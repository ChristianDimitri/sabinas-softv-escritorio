Imports System.Data.SqlClient
Public Class BrwTiposServicios

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmTiposServicios.Show()
    End Sub

    Private Sub consultar()
        If GloClv_TipoServicio > 0 Then
            opcion = "C"
            GloClv_TipoServicio = Me.Clv_calleLabel2.Text
            FrmTiposServicios.Show()
        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If GloClv_TipoServicio > 0 Then
            opcion = "M"
            GloClv_TipoServicio = Me.Clv_calleLabel2.Text
            FrmTiposServicios.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            modificar()
        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If op = 0 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.BusTipServTableAdapter.Connection = CON
                    Me.BusTipServTableAdapter.Fill(Me.NewSofTvDataSet.BusTipServ, New System.Nullable(Of Integer)(CType(Me.TextBox1.Text, Integer)), "", New System.Nullable(Of Integer)(CType(0, Integer)))
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            ElseIf op = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BusTipServTableAdapter.Connection = CON
                    Me.BusTipServTableAdapter.Fill(Me.NewSofTvDataSet.BusTipServ, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)))
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            Else
                Me.BusTipServTableAdapter.Connection = CON
                Me.BusTipServTableAdapter.Fill(Me.NewSofTvDataSet.BusTipServ, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(2, Integer)))
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub


    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub



    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            GloClv_TipoServicio = Me.Clv_calleLabel2.Text
        End If
    End Sub

    Private Sub BrwTiposServicios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Busca(2)
        End If
    End Sub

    Private Sub BrwTiposServicios_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Busca(2)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub



    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub


End Class