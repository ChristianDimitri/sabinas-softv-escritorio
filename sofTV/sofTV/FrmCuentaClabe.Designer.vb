﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCuentaClabe
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label1 As System.Windows.Forms.Label
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCuentaClabe))
        Me.LocalidadBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.tbnombre = New System.Windows.Forms.TextBox()
        Me.tbId = New System.Windows.Forms.TextBox()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.lbContrato = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        ClaveLabel = New System.Windows.Forms.Label()
        CType(Me.LocalidadBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LocalidadBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(45, 90)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(56, 15)
        Label1.TabIndex = 45
        Label1.Text = "Cuenta:"
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ClaveLabel.Location = New System.Drawing.Point(76, 52)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(25, 15)
        ClaveLabel.TabIndex = 43
        ClaveLabel.Text = "ID:"
        AddHandler ClaveLabel.Click, AddressOf Me.ClaveLabel_Click
        '
        'LocalidadBindingNavigator
        '
        Me.LocalidadBindingNavigator.AddNewItem = Nothing
        Me.LocalidadBindingNavigator.CountItem = Nothing
        Me.LocalidadBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.LocalidadBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.LocalidadBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONSERVICIOSBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.LocalidadBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.LocalidadBindingNavigator.MoveFirstItem = Nothing
        Me.LocalidadBindingNavigator.MoveLastItem = Nothing
        Me.LocalidadBindingNavigator.MoveNextItem = Nothing
        Me.LocalidadBindingNavigator.MovePreviousItem = Nothing
        Me.LocalidadBindingNavigator.Name = "LocalidadBindingNavigator"
        Me.LocalidadBindingNavigator.PositionItem = Nothing
        Me.LocalidadBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LocalidadBindingNavigator.Size = New System.Drawing.Size(475, 27)
        Me.LocalidadBindingNavigator.TabIndex = 42
        Me.LocalidadBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(94, 24)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 24)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONSERVICIOSBindingNavigatorSaveItem
        '
        Me.CONSERVICIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSERVICIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSERVICIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Name = "CONSERVICIOSBindingNavigatorSaveItem"
        Me.CONSERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(95, 24)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'tbnombre
        '
        Me.tbnombre.BackColor = System.Drawing.Color.White
        Me.tbnombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbnombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbnombre.Location = New System.Drawing.Point(116, 88)
        Me.tbnombre.MaxLength = 255
        Me.tbnombre.Name = "tbnombre"
        Me.tbnombre.Size = New System.Drawing.Size(322, 21)
        Me.tbnombre.TabIndex = 46
        '
        'tbId
        '
        Me.tbId.BackColor = System.Drawing.Color.White
        Me.tbId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbId.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbId.Location = New System.Drawing.Point(116, 50)
        Me.tbId.MaxLength = 5
        Me.tbId.Name = "tbId"
        Me.tbId.Size = New System.Drawing.Size(100, 21)
        Me.tbId.TabIndex = 44
        '
        'tbContrato
        '
        Me.tbContrato.BackColor = System.Drawing.Color.White
        Me.tbContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContrato.Location = New System.Drawing.Point(169, 124)
        Me.tbContrato.MaxLength = 255
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.ReadOnly = True
        Me.tbContrato.Size = New System.Drawing.Size(119, 21)
        Me.tbContrato.TabIndex = 47
        '
        'lbContrato
        '
        Me.lbContrato.AutoSize = True
        Me.lbContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lbContrato.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lbContrato.Location = New System.Drawing.Point(39, 127)
        Me.lbContrato.Name = "lbContrato"
        Me.lbContrato.Size = New System.Drawing.Size(128, 15)
        Me.lbContrato.TabIndex = 48
        Me.lbContrato.Text = "Contrato Asignado:"
        '
        'FrmCuentaClabe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(475, 166)
        Me.Controls.Add(Me.lbContrato)
        Me.Controls.Add(Me.tbContrato)
        Me.Controls.Add(Me.tbnombre)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.tbId)
        Me.Controls.Add(ClaveLabel)
        Me.Controls.Add(Me.LocalidadBindingNavigator)
        Me.Name = "FrmCuentaClabe"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cuenta Cable"
        CType(Me.LocalidadBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LocalidadBindingNavigator.ResumeLayout(False)
        Me.LocalidadBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LocalidadBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbnombre As System.Windows.Forms.TextBox
    Friend WithEvents tbId As System.Windows.Forms.TextBox
    Friend WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents lbContrato As System.Windows.Forms.Label
End Class
