﻿Public Class BrwIdentificacionUsuario

    Private Sub BrwIdentificacionUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        tbNombre.BackColor = Panel1.BackColor
        tbNombre.ForeColor = Panel1.ForeColor
        buscaIdentificacion(0, "")
    End Sub

    Public Sub buscaIdentificacion(ByVal clv_identificacion As Integer, ByVal nombre As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_identificacion", SqlDbType.Int, clv_identificacion)
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, nombre)
        DataGridView1.DataSource = BaseII.ConsultaDT("ConsultaIdentificacionUsuario")
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        If TextBox1.Text.Trim = "" Then
            Exit Sub
        End If
        buscaIdentificacion(TextBox1.Text, "")
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox2.Text.Trim = "" Then
            Exit Sub
        End If
        buscaIdentificacion(0, TextBox2.Text)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            If DataGridView1.Rows.Count = 0 Then
                Exit Sub
            End If
            lbClave.Text = DataGridView1.SelectedCells(0).Value.ToString
            tbNombre.Text = DataGridView1.SelectedCells(1).Value.ToString
        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        GloClvIdentificacionUsario = 0
        opcion = "N"
        FrmIdentificacionUsuario.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        GloClvIdentificacionUsario = lbClave.Text
        opcion = "C"
        FrmIdentificacionUsuario.Show()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        GloClvIdentificacionUsario = lbClave.Text
        opcion = "M"
        FrmIdentificacionUsuario.Show()
    End Sub
End Class