﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmVendedorContratos

    'Private Sub LenaFolios()
    '    Dim aValor As Long = 0
    '    If IsNumeric(Me.ComboBox1.SelectedValue) = False Then
    '        aValor = 0
    '    Else
    '        aValor = CStr(Me.ComboBox1.SelectedValue)
    '    End If

    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim strSQL As New StringBuilder("EXEC Folio_Disponible " & aValor.ToString & ", '" & CStr(Me.ComboBox2.SelectedValue) & "'")
    '    Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
    '    Dim dataTable As New DataTable
    '    Dim bindingSource As New BindingSource

    '    Try
    '        dataAdapter.Fill(dataTable)
    '        bindingSource.DataSource = dataTable
    '        ComboBox3.DataSource = bindingSource
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    End Try
    '    'Me.ComboBox3.Text = ""
    'End Sub

    Public Sub LenaFolios()
        Try
            ComboBox3.Text = ""
            Dim aValor As Long = 0
            If IsNumeric(Me.ComboBox1.SelectedValue) = False Then
                aValor = 0
            Else
                aValor = CStr(Me.ComboBox1.SelectedValue)
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_VENDEDOR", SqlDbType.BigInt, aValor.ToString)
            BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, CStr(Me.ComboBox2.SelectedValue), 50)
            ComboBox3.DataSource = BaseII.ConsultaDT("Folio_DisponibleCLI")
            ComboBox3.DisplayMember = "Folio"
            ComboBox3.ValueMember = "Folio"
            If ComboBox3.Items.Count >= 1 Then
                ComboBox3.SelectedIndex = 0
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub Llena_Vendedores(oClvUsuario As Long)
        Try
            ComboBox1.Text = ""
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.BigInt, oClvUsuario)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
            ComboBox1.DataSource = BaseII.ConsultaDT("MUESTRAVENDEDORES_2")
            ComboBox1.DisplayMember = "Nombre"
            ComboBox1.ValueMember = "Clv_Vendedor"
            If ComboBox1.Items.Count >= 1 Then
                ComboBox1.SelectedIndex = 0
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.ComboBox1.SelectedValue) = True And Len(Trim(Me.ComboBox1.Text)) > 0 Then
            ComboBox2.Text = ""
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_VENDEDOR", SqlDbType.Int, ComboBox1.SelectedValue)
            ComboBox2.DataSource = BaseII.ConsultaDT("Ultimo_SERIEYFOLIOCLI")
            ComboBox2.DisplayMember = "SERIE"
            ComboBox2.ValueMember = "SERIE"
            If ComboBox2.Items.Count >= 1 Then
                ComboBox2.SelectedIndex = 0
            End If
            'Me.Ultimo_SERIEYFOLIOTableAdapter.Connection = CON
            'Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.DataSetEdgar.Ultimo_SERIEYFOLIO, Me.ComboBox1.SelectedValue)

        Else
             ComboBox2.Text = ""
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_VENDEDOR", SqlDbType.Int, 0)
            ComboBox2.DataSource = BaseII.ConsultaDT("Ultimo_SERIEYFOLIOCLI")
            ComboBox2.DisplayMember = "SERIE"
            ComboBox2.ValueMember = "SERIE"
            'Me.Ultimo_SERIEYFOLIOTableAdapter.Connection = CON
            'Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.DataSetEdgar.Ultimo_SERIEYFOLIO, 0)

        End If
        CON.Dispose()
        CON.Close()
    End Sub

    Private Sub FrmVendedorContratos_Load(sender As Object, e As EventArgs) Handles Me.Load
        Llena_Vendedores(GloClvUsuario)

        If Contrato > 0 Then
            CONSULTAR(Contrato)
            If VALIDARELCONTRATOVENDEDOR(Contrato) = 1 Then
                Panel4.Enabled = False
            Else
                Panel4.Enabled = True
            End If
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged

        If Len(Trim(ComboBox2.Text)) Then
            'Dame_UltimoFolio()
            LenaFolios()
        End If
    End Sub


    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub GUARDAR(oContrato As Long, oClv_Vendedor As Integer, oSerie As String, oFolio As Long)
        Try

        
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_VENDEDOR", SqlDbType.Int, oClv_Vendedor)
            BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, oSerie, 50)
        BaseII.CreateMyParameter("@FOLIO", SqlDbType.BigInt, oFolio)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 400)
        BaseII.ProcedimientoOutPut("GUARDAR_RELCONTRATOVENDEDOR")
        MsgBox(BaseII.dicoPar("@MSJ").ToString)
        Catch ex As Exception

        End Try
    End Sub

    Public Function VALIDARELCONTRATOVENDEDOR(oContrato As Long) As Integer
        Try
            VALIDARELCONTRATOVENDEDOR = 0
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@BND", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("VALIDARELCONTRATOVENDEDOR")
            VALIDARELCONTRATOVENDEDOR = BaseII.dicoPar("@BND").ToString


        Catch ex As Exception

        End Try
    End Function

    Private Sub CONSULTAR(oContrato As Long)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_VENDEDOR", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@NOMBREVENDEDOR", ParameterDirection.Output, SqlDbType.VarChar, 250)
            BaseII.CreateMyParameter("@SERIE", ParameterDirection.Output, SqlDbType.VarChar, 50)
            BaseII.CreateMyParameter("@FOLIO", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("CONSULTA_RELCONTRATOVENDEDOR")
            ComboBox1.Text = BaseII.dicoPar("@NOMBREVENDEDOR").ToString
            ComboBox2.Text = BaseII.dicoPar("@SERIE").ToString
            If CInt(BaseII.dicoPar("@FOLIO").ToString) > 0 Then
                ComboBox3.Text = BaseII.dicoPar("@FOLIO").ToString
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ELIMINAR(oContrato As Long)
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 400)
            BaseII.ProcedimientoOutPut("ELIMINAR_RELCONTRATOVENDEDOR")
            MsgBox(BaseII.dicoPar("@MSJ").ToString)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Contrato > 0 Then
            ELIMINAR(Contrato)
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If ComboBox1.SelectedIndex >= 0 And ComboBox2.SelectedIndex >= 0 And ComboBox3.SelectedIndex >= 0 Then
            GUARDAR(Contrato, ComboBox1.SelectedValue, ComboBox2.SelectedValue, ComboBox3.SelectedValue)
            Me.Close()
        Else
            MsgBox("Seleccione el vendedor, la serie y el folio", vbInformation)
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox3.SelectedIndexChanged

    End Sub
End Class