Imports System.Data.SqlClient
Public Class FrmMetasInd
    Private Bnd As Boolean = False

    Private Sub FrmMetasInd_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraTipServEricTableAdapter.Connection = CON
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)
            Me.MuestraAniosTableAdapter.Connection = CON
            Me.MuestraAniosTableAdapter.Fill(Me.DataSetEric2.MuestraAnios)
            Me.ConGrupoVentasTableAdapter.Connection = CON
            Me.ConGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConGrupoVentas, 0, 3, GloClvUsuario)
            CON.Close()
            Consultar()
            Bnd = True
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
            UspDesactivaBotones(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Consultar()
        Try

            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.ConMetasIndTableAdapter.Connection = CON2
            Me.ConMetasIndTableAdapter.Fill(Me.DataSetEric2.ConMetasInd, CInt(Me.GrupoComboBox.SelectedValue), CInt(Me.NombreComboBox.SelectedValue), CInt(Me.ConceptoComboBox.SelectedValue), CInt(Me.AnioComboBox.SelectedValue))
            CON2.Close()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GrupoComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrupoComboBox.SelectedValueChanged
        
    End Sub


    Private Sub NombreComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NombreComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub

    Private Sub ConceptoComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub


    Private Sub AnioComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AnioComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub

    Private Sub ConMetasIndBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConMetasIndBindingNavigatorSaveItem.Click
        Try
            Dim CON7 As New SqlConnection(MiConexion)
            CON7.Open()
            Me.ConMetasIndTableAdapter.Connection = CON7
            Me.ConMetasIndTableAdapter.Update(Me.DataSetEric2.ConMetasInd)
            CON7.Close()
            MsgBox(mensaje5)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim R As Integer = 0
        Try
            R = MsgBox("Se Eliminar� el Servicio. �Deseas Continuar?", MsgBoxStyle.YesNo, "Atenci�n")
            If R = 6 Then
                Dim CON8 As New SqlConnection(MiConexion)
                CON8.Open()
                Me.ConMetasIndTableAdapter.Connection = CON8
                Me.ConMetasIndTableAdapter.Delete(CInt(Me.Clv_GrupoTextBox.Text), CInt(Me.ClaveTextBox.Text), CInt(Me.Clv_ServicioTextBox.Text), CInt(Me.AnioTextBox.Text))
                CON8.Close()
                MsgBox(mensaje6)
                Consultar()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Bnd = False
        Me.Close()
    End Sub

    Private Sub ConMetasIndDataGridView_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConMetasIndDataGridView.CellLeave
        Try
            Dim CON7 As New SqlConnection(MiConexion)
            CON7.Open()
            Me.ConMetasIndTableAdapter.Connection = CON7
            Me.ConMetasIndTableAdapter.Update(Me.DataSetEric2.ConMetasInd)
            CON7.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Checa()
        Try
            If Me.NombreComboBox.Items.Count = 0 Then
                Bnd = False
                Me.Panel1.Enabled = False
            Else
                Bnd = True
                Me.Panel1.Enabled = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Clv_GrupoTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_GrupoTextBox1.TextChanged
        Try

            Dim CON5 As New SqlConnection(MiConexion)
            CON5.Open()
            Me.MuestraGrupoTableAdapter.Connection = CON5
            Me.MuestraGrupoTableAdapter.Fill(Me.DataSetEric2.MuestraGrupo, CInt(Me.Clv_GrupoTextBox1.Text))
            CON5.Close()
            Checa()
            If Bnd = True Then
                Consultar()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class