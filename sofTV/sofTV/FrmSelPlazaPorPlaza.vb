﻿Imports System.Data.SqlClient
Public Class FrmSelPlazaPorPlaza

    Private Sub FrmSelPlaza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Por si es uno
        'Dim CONar As New SqlConnection(MiConexion)
        'Dim cmd As New SqlCommand()
        'Dim numero As Integer
        'Try
        '    cmd = New SqlCommand()
        '    CONar.Open()
        '    '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
        '    With cmd
        '        .CommandText = "DameNumPlazasUsuario"
        '        .Connection = CONar
        '        .CommandTimeout = 0
        '        .CommandType = CommandType.StoredProcedure

        '        Dim prm As New SqlParameter("@ClaveUsuario", SqlDbType.Int)
        '        prm.Direction = ParameterDirection.Input
        '        prm.Value = GloClvUsuario
        '        .Parameters.Add(prm)

        '        Dim prm1 As New SqlParameter("@numero", SqlDbType.Int)
        '        prm1.Direction = ParameterDirection.Output
        '        prm1.Value = 0
        '        .Parameters.Add(prm1)


        '        Dim ia As Integer = .ExecuteNonQuery()

        '        numero = prm1.Value
        '    End With
        '    CONar.Close()
        'Catch ex As Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
        'If numero = 1 Then
        '    Dim conexion As New SqlConnection(MiConexion)
        '    conexion.Open()
        '    Dim comando As New SqlCommand()
        '    comando.Connection = conexion
        '    comando.CommandText = " exec DameIdentificador"
        '    identificador = comando.ExecuteScalar()
        '    conexion.Close()
        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@Identificador", SqlDbType.BigInt, identificador)
        '    BaseII.CreateMyParameter("@claveusuario", SqlDbType.Int, GloClvUsuario)
        '    BaseII.Inserta("InsertaSeleccionPlazatbl")
        '    llevamealotro()
        '    Exit Sub
        'End If
        'Por si es uni
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        Dim conexion2 As New SqlConnection(MiConexion)
        conexion2.Open()
        Dim comando2 As New SqlCommand()
        comando2.Connection = conexion2
        comando2.CommandText = " exec DameIdentificador"
        identificador = comando2.ExecuteScalar()
        conexion2.Close()
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionPlaza")
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionPlaza")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionPlaza")
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        'Solo un elemento puede estar seleccionado
        Dim AntesDeBorrar As Integer
        AntesDeBorrar = loquehay.SelectedValue

        Me.quitar.PerformClick()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, AntesDeBorrar)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionPlaza")
        llenalistboxs()

        Me.Button6.PerformClick()

    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionPlaza")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionPlaza")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionPlaza")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        EntradasSelCiudad = 0
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            MsgBox("Seleccione al menos un Elemento")
            Exit Sub
        End If

        InsertaTodoEnElMundo()
        execar = True

        'FrmSelCompaniaCartera.Show()
        'FrmSelCompaniaCarteraPorPlaza.Show()
        Me.Close()
    End Sub

    Private Sub InsertaTodoEnElMundo()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, LocClv_session)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("InsertaSeleccionParaCarterasPorPlaza")
    End Sub

    Private Sub llevamealotro()
        FrmSelCompaniaCartera.Show()
        Me.Close()
    End Sub
End Class