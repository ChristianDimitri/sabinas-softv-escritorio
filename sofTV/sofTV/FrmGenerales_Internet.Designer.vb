<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGenerales_Internet
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_IdLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmGenerales_Internet))
        Me.Button2 = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.CONGenerales_InternetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Paq_defaultTextBox = New System.Windows.Forms.TextBox
        Me.Num_dia_habilitarTextBox = New System.Windows.Forms.TextBox
        Me.Clv_IdTextBox = New System.Windows.Forms.TextBox
        Me.CONGenerales_InternetBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.CONGenerales_InternetBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.CONGenerales_InternetTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONGenerales_InternetTableAdapter
        Clv_IdLabel = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.CONGenerales_InternetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONGenerales_InternetBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONGenerales_InternetBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_IdLabel
        '
        Clv_IdLabel.AutoSize = True
        Clv_IdLabel.Location = New System.Drawing.Point(73, 92)
        Clv_IdLabel.Name = "Clv_IdLabel"
        Clv_IdLabel.Size = New System.Drawing.Size(37, 13)
        Clv_IdLabel.TabIndex = 15
        Clv_IdLabel.Text = "Clv Id:"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(399, 280)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(142, 29)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "&CERRAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Paq_defaultTextBox)
        Me.Panel1.Controls.Add(Me.Num_dia_habilitarTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 39)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(529, 225)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(205, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 15)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "Comunidad"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(200, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 15)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Ip Server C4"
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_InternetBindingSource, "Comunidad", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(291, 72)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(168, 21)
        Me.TextBox1.TabIndex = 1
        '
        'CONGenerales_InternetBindingSource
        '
        Me.CONGenerales_InternetBindingSource.DataMember = "CONGenerales_Internet"
        Me.CONGenerales_InternetBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_InternetBindingSource, "IpServer", True))
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(291, 31)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(168, 21)
        Me.TextBox2.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(49, 156)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(236, 15)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Paquete(s) Asignado(s) por Default:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(16, 115)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(269, 15)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Numero de Dìas de Prueba de Paquetes:"
        '
        'Paq_defaultTextBox
        '
        Me.Paq_defaultTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_InternetBindingSource, "paq_default", True))
        Me.Paq_defaultTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Paq_defaultTextBox.Location = New System.Drawing.Point(291, 150)
        Me.Paq_defaultTextBox.Name = "Paq_defaultTextBox"
        Me.Paq_defaultTextBox.Size = New System.Drawing.Size(168, 21)
        Me.Paq_defaultTextBox.TabIndex = 3
        '
        'Num_dia_habilitarTextBox
        '
        Me.Num_dia_habilitarTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_InternetBindingSource, "num_dia_habilitar", True))
        Me.Num_dia_habilitarTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Num_dia_habilitarTextBox.Location = New System.Drawing.Point(291, 109)
        Me.Num_dia_habilitarTextBox.Name = "Num_dia_habilitarTextBox"
        Me.Num_dia_habilitarTextBox.Size = New System.Drawing.Size(90, 21)
        Me.Num_dia_habilitarTextBox.TabIndex = 2
        '
        'Clv_IdTextBox
        '
        Me.Clv_IdTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_InternetBindingSource, "Clv_Id", True))
        Me.Clv_IdTextBox.Location = New System.Drawing.Point(116, 85)
        Me.Clv_IdTextBox.Name = "Clv_IdTextBox"
        Me.Clv_IdTextBox.ReadOnly = True
        Me.Clv_IdTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_IdTextBox.TabIndex = 16
        Me.Clv_IdTextBox.TabStop = False
        '
        'CONGenerales_InternetBindingNavigator
        '
        Me.CONGenerales_InternetBindingNavigator.AddNewItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.BindingSource = Me.CONGenerales_InternetBindingSource
        Me.CONGenerales_InternetBindingNavigator.CountItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONGenerales_InternetBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONGenerales_InternetBindingNavigatorSaveItem})
        Me.CONGenerales_InternetBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONGenerales_InternetBindingNavigator.MoveFirstItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.MoveLastItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.MoveNextItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.MovePreviousItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.Name = "CONGenerales_InternetBindingNavigator"
        Me.CONGenerales_InternetBindingNavigator.PositionItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.Size = New System.Drawing.Size(555, 25)
        Me.CONGenerales_InternetBindingNavigator.TabIndex = 4
        Me.CONGenerales_InternetBindingNavigator.TabStop = True
        Me.CONGenerales_InternetBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(72, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONGenerales_InternetBindingNavigatorSaveItem
        '
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONGenerales_InternetBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Name = "CONGenerales_InternetBindingNavigatorSaveItem"
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Size = New System.Drawing.Size(108, 22)
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'CONGenerales_InternetTableAdapter
        '
        Me.CONGenerales_InternetTableAdapter.ClearBeforeFill = True
        '
        'FrmGenerales_Internet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(555, 324)
        Me.Controls.Add(Me.CONGenerales_InternetBindingNavigator)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Clv_IdLabel)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Clv_IdTextBox)
        Me.Name = "FrmGenerales_Internet"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generales de Internet"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONGenerales_InternetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONGenerales_InternetBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONGenerales_InternetBindingNavigator.ResumeLayout(False)
        Me.CONGenerales_InternetBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Paq_defaultTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Num_dia_habilitarTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_IdTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents CONGenerales_InternetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONGenerales_InternetTableAdapter As sofTV.DataSetEDGARTableAdapters.CONGenerales_InternetTableAdapter
    Friend WithEvents CONGenerales_InternetBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONGenerales_InternetBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
End Class
