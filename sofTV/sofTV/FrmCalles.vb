Imports System.Data.SqlClient
Public Class FrmCalles
    Private locclv_colonia As Integer = 0
    Private clvcoloniatxt As String = Nothing

    Private nomcalle As String = Nothing
    Private bnd1 As Boolean = False
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                nomcalle = Me.NOMBRETextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    If opcion = "N" Then
                        If bnd1 = False Then
                            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se guardo un nueva nueva calle", "", "Se guardo un nueva nueva calle: " + Me.NOMBRETextBox.Text, LocClv_Ciudad)
                            bnd1 = True
                        End If
                    ElseIf opcion = "M" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NOMBRETextBox.Name, nomcalle, Me.NOMBRETextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    'clvcoloniatxt
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Agrego Una Nueva Colonia A La Calle", "Colonia: " + Me.ComboBox1.Text, "Se Agrego Una Nueva Colonia A La Calle: " + Me.NOMBRETextBox.Text, LocClv_Ciudad)
                Case 2
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Una Colonia A La Calle", "Colonia: " + clvcoloniatxt, "Se Elimino Una Colonia A La Calle: " + Me.NOMBRETextBox.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub guardarcalles()


    End Sub

    Private Sub mostrardatos()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCALLESTableAdapter.Connection = CON
            Me.CONCALLESTableAdapter.Fill(Me.NewSofTvDataSet.CONCALLES, New System.Nullable(Of Integer)(CType(GloClv_Calle, Integer)))
            damedatosbitacora()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub BindingNavigatorAddNewItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Public Sub CREAARBOL()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
            Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(GloClv_Calle, Integer)))
            Dim FilaRow As DataRow
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.DAMECOLONIA_CALLE.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                If IsNumeric(Trim(FilaRow("Clv_colonia").ToString())) = True Then
                    Me.TreeView1.Nodes.Add(Trim(FilaRow("Clv_colonia").ToString()), Trim(FilaRow("Colonia").ToString()))
                    Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("Clv_colonia").ToString())
                    I += 1
                End If
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FrmCalles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        Try
            CON.Open()
            colorea(Me, Me.Name)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUECOLONIAS' Puede moverla o quitarla seg�n sea necesario.
            'Me.MUECOLONIASTableAdapter.Connection = CON
            'Me.MUECOLONIASTableAdapter.Fill(Me.NewSofTvDataSet.MUECOLONIAS)
            Me.MUECOLONIASCompaniaTableAdapter.Connection = CON
            Me.MUECOLONIASCompaniaTableAdapter.Fill(Me.DataSetEDGAR.MUECOLONIASCompania, GloIdCompania)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
            Me.MUESTRACALLESTableAdapter.Connection = CON
            Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
            Me.Panel1.Enabled = True
            If opcion = "N" Then
                Me.CONCALLESBindingSource.AddNew()
                Panel2.Enabled = False
            ElseIf opcion = "C" Then
                Me.mostrardatos()
                Me.Panel1.Enabled = False
                Me.Panel2.Enabled = False
                Me.Panel1.Enabled = False
                Me.CREAARBOL()
                llenaComboEstados()
                llenaGrid()
                Panel2.Enabled = False
            ElseIf opcion = "M" Then
                Me.mostrardatos()
                Me.CREAARBOL()
                llenaComboEstados()
                llenaGrid()
            End If
            CON.Close()
            If opcion = "N" Or opcion = "M" Then
                UspDesactivaBotones(Me, Me.Name)
            End If
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.Message)
            CON.CLOSE()
        End Try
    End Sub

    Private Sub ToolStripLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripLabel1.Click
        Me.CONCALLESBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub



    Private Sub CONCALLESBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCALLESBindingNavigatorSaveItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, NOMBRETextBox.Text)
        BaseII.CreateMyParameter("@mismoNombre", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@clv_calle", SqlDbType.VarChar, Clv_CalleTextBox.Text)
        BaseII.ProcedimientoOutPut("ValidaNombreCalle")
        Dim mismoNombre = BaseII.dicoPar("@mismoNombre")
        If mismoNombre = 1 Then
            MsgBox("Ya existe una calle con el mismo nombre.")
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONCALLESBindingSource.EndEdit()
        Me.CONCALLESTableAdapter.Connection = CON
        Me.CONCALLESTableAdapter.Update(Me.NewSofTvDataSet.CONCALLES)
        guardabitacora(0)
        MsgBox("Se Guardo con Ex�to", MsgBoxStyle.Information)
        GloBnd = True
        CON.Close()
        Panel2.Enabled = True
        llenaComboEstados()
        If opcion = "N" Then
            opcion = "M"
        End If
        'Me.Close()
    End Sub


    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub CONCVECAROLDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ComboBox1.Items.Count = 0 Then
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, cbEstado.SelectedValue)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, cbCiudad.SelectedValue)
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, cbLocalidad.SelectedValue)
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, ComboBox1.SelectedValue)
            BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, Clv_CalleTextBox.Text)
            BaseII.CreateMyParameter("@existe", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("InsertaCveCaCol")
            If BaseII.dicoPar("@existe") = 1 Then
                MsgBox("Ya existe una relaci�n con la colonia que se desea agregar.")
            Else
                llenaGrid()
            End If
        Catch ex As Exception

        End Try
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Dim bnd As Boolean = True
        'Try
        '    Me.Validate()
        '    Me.CONCALLESBindingSource.EndEdit()
        '    Me.CONCALLESTableAdapter.Connection = CON
        '    Me.CONCALLESTableAdapter.Update(Me.NewSofTvDataSet.CONCALLES)
        '    guardabitacora(0)
        '    '--MsgBox("Se Guardo con Ex�to", MsgBoxStyle.Information)
        '    Me.CONCVECAROLTableAdapter.Connection = CON
        '    Me.CONCVECAROLTableAdapter.Insert(GloClv_Calle, Me.ComboBox1.SelectedValue, 0, 0, 0, "", "")
        '    Me.Validate()
        '    Me.CONCVECAROLBindingSource.EndEdit()
        '    Me.CONCVECAROLTableAdapter.Connection = CON
        '    Me.CONCVECAROLTableAdapter.Update(Me.NewSofTvDataSet.CONCVECAROL)
        '    If bnd = True Then
        '        CREAARBOL()
        '    End If
        '    guardabitacora(1)
        'Catch ex As System.Exception

        '    bnd = False
        '    Me.CONCVECAROLBindingSource.CancelEdit()
        '    MsgBox("La Colonia Ya Ex�ste en la Lista")
        '    '--System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
        'CON.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, DataGridView1.SelectedCells(6).Value.ToString)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, DataGridView1.SelectedCells(4).Value.ToString)
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, DataGridView1.SelectedCells(2).Value.ToString)
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, DataGridView1.SelectedCells(0).Value.ToString)
            BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, Clv_CalleTextBox.Text)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.ProcedimientoOutPut("ValidaEliminaCveCaCol")
            If BaseII.dicoPar("@error") <> "" Then
                MsgBox(BaseII.dicoPar("@error"))
                Exit Sub
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, DataGridView1.SelectedCells(6).Value.ToString)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, DataGridView1.SelectedCells(4).Value.ToString)
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, DataGridView1.SelectedCells(2).Value.ToString)
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, DataGridView1.SelectedCells(0).Value.ToString)
            BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, Clv_CalleTextBox.Text)
            BaseII.ProcedimientoOutPut("EliminaCveCaCol")
            llenaGrid()
        Catch ex As Exception

        End Try
        'ValidaEliminarRelColoniaCalle()
        'If Len(eMsj) > 0 Then
        '    MsgBox(eMsj, MsgBoxStyle.Information)
        '    Exit Sub
        'End If

        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If locclv_colonia > 0 Then
        '    Me.CONCVECAROLTableAdapter.Connection = CON
        '    Me.CONCVECAROLTableAdapter.Delete(GloClv_Calle, Me.locclv_colonia)
        '    Me.Validate()
        '    Me.CONCVECAROLBindingSource.EndEdit()
        '    Me.CONCVECAROLTableAdapter.Connection = CON
        '    Me.CONCVECAROLTableAdapter.Update(Me.NewSofTvDataSet.CONCVECAROL)
        '    Me.CREAARBOL()
        '    guardabitacora(2)
        '    locclv_colonia = 0
        'Else
        '    MsgBox("Seleccione lo que desea Quitar de la Lista", MsgBoxStyle.Information)
        'End If
        'CON.Close()

    End Sub

    Private Sub ValidaEliminarRelColoniaCalle()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_Calle", SqlDbType.Int, GloClv_Calle)
        BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, Me.locclv_colonia)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("ValidaEliminarRelColoniaCalle")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString
    End Sub


    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        locclv_colonia = e.Node.Tag
        clvcoloniatxt = e.Node.Text
    End Sub

    Private Sub Clv_CalleTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CalleTextBox.TextChanged
        GloClv_Calle = Me.Clv_CalleTextBox.Text
    End Sub

    Private Sub NOMBRETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NOMBRETextBox.TextChanged

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub FrmCalles_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        'If Clv_CalleTextBox.Text = "" Or Clv_CalleTextBox.Text = "0" Then
        '    Exit Sub
        'End If
        'If DataGridView1.Rows.Count = 0 And opcion = "M" Then
        '    e.Cancel = True
        '    MsgBox("Debe que asignar la relaci�n antes de salir.")
        'End If
    End Sub

    Private Sub NOMBRELabel1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub llenaComboEstados()
        Try
            BaseII.limpiaParametros()
            cbEstado.DataSource = BaseII.ConsultaDT("MuestraEstados")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaComboCiudades()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, cbEstado.SelectedValue)
            cbCiudad.DataSource = BaseII.ConsultaDT("MuestraCiudadesEstado2")
            If cbCiudad.Items.Count = 0 Then
                cbCiudad.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaComboLocalidades()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, cbCiudad.SelectedValue)
            cbLocalidad.DataSource = BaseII.ConsultaDT("MuestraLocalidadCiudad")
            If cbLocalidad.Items.Count = 0 Then
                cbLocalidad.Text = ""
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub llenaComboColonias()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, cbLocalidad.SelectedValue)
            ComboBox1.DataSource = BaseII.ConsultaDT("MuestraColoniaLocalidad")
            If ComboBox1.Items.Count = 0 Then
                ComboBox1.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaGrid()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, Clv_CalleTextBox.Text)
            DataGridView1.DataSource = BaseII.ConsultaDT("DameCVECAROL")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub cbEstado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbEstado.SelectedIndexChanged
        llenaComboCiudades()
    End Sub

    Private Sub cbCiudad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCiudad.SelectedIndexChanged
        
        llenaComboLocalidades()
    End Sub

    Private Sub cbLocalidad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbLocalidad.SelectedIndexChanged
       
        llenaComboColonias()
    End Sub
End Class