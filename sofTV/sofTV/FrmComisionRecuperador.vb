﻿Public Class FrmComisionRecuperador

    Private Sub FrmComisionRecuperador_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        colorea(Me, Me.Name)


        llenaTipoRJ()

        If opRanRec = "N" Then
            idRanRec = 0
        End If
        If (opRanRec = "M" Or opRanRec = "C") And idRanRec > 0 Then

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.BigInt, idRanRec)
            BaseII.CreateMyParameter("@TipoRJ", ParameterDirection.Output, SqlDbType.VarChar, 50)
            BaseII.CreateMyParameter("@limiteInf", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@limiteSup", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@pago", ParameterDirection.Output, SqlDbType.Money)
            BaseII.ProcedimientoOutPut("Usp_ConRangosComisionRecuperador")
            ComboBox1.SelectedValue = BaseII.dicoPar("@TipoRJ")
            TextBox1.Text = BaseII.dicoPar("@limiteInf")
            TextBox2.Text = BaseII.dicoPar("@limiteSup")
            TextBox3.Text = BaseII.dicoPar("@pago")

            If opRanRec = "C" Then
                TextBox1.Enabled = False
                TextBox2.Enabled = False
                TextBox3.Enabled = False
                ComboBox1.Enabled = False
                'Button1.Enabled = False
                Button5.Enabled = False
            End If

        End If

    End Sub

    Private Sub llenaTipoRJ()
        Try
            BaseII.limpiaParametros()
            ComboBox1.DataSource = BaseII.ConsultaDT("Usp_MuestraTipoRJ")
            ComboBox1.DisplayMember = "TipoRJ"
            ComboBox1.ValueMember = "idTipoRJ"

            If ComboBox1.Items.Count > 0 Then
                ComboBox1.SelectedIndex = 0
            End If

        Catch ex As Exception

        End Try
    End Sub


    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox1, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TextBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox2, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox3.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox3, Asc(e.KeyChar), "L"))
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        If TextBox1.Text.Length = 0 Then
            MsgBox("Capture el limite inferior")
            Exit Sub
        End If

        If TextBox2.Text.Length = 0 Then
            MsgBox("Capture el limite Superior")
            Exit Sub
        End If

        If TextBox3.Text.Length = 0 Then
            MsgBox("Capture el Pago")
            Exit Sub
        End If

        If CInt(TextBox1.Text) >= CInt(TextBox2.Text) Then
            MsgBox("El limite inferior no puede ser mayor o igual al limite superior")
            Exit Sub
        End If

        Dim Msg As String = Nothing

        If opRanRec = "N" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@TipoRJ", SqlDbType.VarChar, ComboBox1.SelectedValue, 50)
            BaseII.CreateMyParameter("@limiteInf", SqlDbType.Int, TextBox1.Text)
            BaseII.CreateMyParameter("@limiteSup", SqlDbType.Int, TextBox2.Text)
            BaseII.CreateMyParameter("@pago", SqlDbType.Money, TextBox3.Text)
            BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 100)
            BaseII.ProcedimientoOutPut("Usp_NueRangosComisionRecuperador")
            Msg = BaseII.dicoPar("@Msg")
        ElseIf opRanRec = "M" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.BigInt, idRanRec)
            BaseII.CreateMyParameter("@TipoRJ", SqlDbType.VarChar, ComboBox1.SelectedValue, 50)
            BaseII.CreateMyParameter("@limiteInf", SqlDbType.Int, TextBox1.Text)
            BaseII.CreateMyParameter("@limiteSup", SqlDbType.Int, TextBox2.Text)
            BaseII.CreateMyParameter("@pago", SqlDbType.Money, TextBox3.Text)
            BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 100)
            BaseII.ProcedimientoOutPut("Usp_ModRangosComisionRecuperador")
            Msg = BaseII.dicoPar("@Msg")
        End If


        If Msg.Length > 0 Then
            MsgBox(Msg, MsgBoxStyle.Information)
            Exit Sub
        Else
            MsgBox("Guardado con exito", MsgBoxStyle.Information)
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class