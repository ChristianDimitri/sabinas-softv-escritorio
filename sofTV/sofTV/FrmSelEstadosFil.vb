﻿Public Class FrmSelEstadosFil

    Private Sub FrmSelEstadosFil_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Por si es uno
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
        BaseII.CreateMyParameter("@numero", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameNumEstadosCompania")
        Dim numciudades As Integer = BaseII.dicoPar("@numero")
        If numciudades = 1 Then
            'Dim conexion As New SqlConnection(MiConexion)
            'conexion.Open()
            'Dim comando As New SqlCommand()
            'comando.Connection = conexion
            'comando.CommandText = " exec DameIdentificador"
            'identificador = comando.ExecuteScalar()
            'conexion.Close()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.Inserta("InsertaSeleccionEstadotbl")
            Llevamealotro()
            Exit Sub
        End If
        'Fin Por si es uno
        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionEstado")

        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionEstado")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionEstado")
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionEstado")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionEstado")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionEstado")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionEstado")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        bgMovCartera = False
        EntradasSelCiudad = 0
        Me.Close()
    End Sub
    Private Sub Llevamealotro()
        If SelCiudadSinJ = 0 Then
            If varfrmselcompania = "movimientoscartera" Then
                bgMovCartera = True
                Me.Close()
            ElseIf varfrmselcompania = "reporteSuscriptores" Then
                FrmFiltroSuscriptores.Show()
                EntradasSelCiudad = 0
            Else
                FrmSelCiudadJ.Show()
                Me.Close()
            End If
        ElseIf SelCiudadSinJ = 1 Then
            If varfrmselcompania = "movimientoscartera" Then
                Me.Close()
                Exit Sub
            End If
            If varfrmselcompania = "ordenes" Then
                Me.Visible = False
                FrmSelCiudad.Show()
                Me.Close()
                Exit Sub
            End If
            If rMensajes = True Or eBndDeco = True Then
                FrmSelColonia_Rep21.Show()
            End If
            If LocbndPolizaCiudad = True Then
                FrmSelFechas.Show()
            Else
                'FrmSelColonia_Rep21.Show() 'opcion1
                varfrmselcompania = "opcion1varios"
                FrmSelCiudad.Show()

            End If
            
            SelCiudadSinJ = 0
        ElseIf SelCiudadSinJ = 2 Then
            FrmSelCd_Cartera.Show()
            SelCiudadSinJ = 0
        ElseIf SelCiudadSinJ = 3 Then
            eOpVentas = 65
            FrmImprimirComision.Show()
            SelCiudadSinJ = 0
        ElseIf SelCiudadSinJ = 4 Then
            eOpVentas = 66
            FrmImprimirComision.Show()
            SelCiudadSinJ = 0
        End If
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If SelCiudadSinJ = 0 Then
            If varfrmselcompania = "movimientoscartera" Then
                bgMovCartera = True
                Me.Close()
            ElseIf varfrmselcompania = "reporteSuscriptores" Then
                EntradasSelCiudad = 0
                FrmFiltroSuscriptores.Show()
            Else
                FrmSelCiudadJ.Show()
                Me.Close()
            End If
        ElseIf SelCiudadSinJ = 1 Then
            If varfrmselcompania = "movimientoscartera" Then
                Me.Close()
                Exit Sub
            End If
            If varfrmselcompania = "ordenes" Then
                Me.Visible = False
                FrmSelCiudad.Show()
                Me.Close()
                Exit Sub
            End If
            If rMensajes = True Or eBndDeco = True Then
                FrmSelColonia_Rep21.Show()
            End If
            If LocbndPolizaCiudad = True Then
                FrmSelFechas.Show()
            Else
                'FrmSelColonia_Rep21.Show() 'opcion1
                varfrmselcompania = "opcion1varios"
                FrmSelCiudad.Show()

            End If
            
            SelCiudadSinJ = 0
        ElseIf SelCiudadSinJ = 2 Then
            FrmSelCd_Cartera.Show()
            SelCiudadSinJ = 0
        ElseIf SelCiudadSinJ = 3 Then
            eOpVentas = 65
            FrmImprimirComision.Show()
            SelCiudadSinJ = 0
        ElseIf SelCiudadSinJ = 4 Then
            eOpVentas = 66
            FrmImprimirComision.Show()
            SelCiudadSinJ = 0
        End If
        Me.Close()
    End Sub
End Class