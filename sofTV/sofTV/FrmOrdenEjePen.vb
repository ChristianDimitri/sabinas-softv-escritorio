﻿Imports System.Data.SqlClient

Public Class FrmOrdenEjePen


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            OrdenEjecutada = 1
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            OrdenEjecutada = 0
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If CheckBox1.Checked = False And CheckBox2.Checked = False Then
            MsgBox("Seleccione al menos un Status", MsgBoxStyle.Information)
            Exit Sub
        End If
        If CheckBox1.Checked = True And CheckBox2.Checked = True Then
            OrdenEjecutada = 2
        End If
        'op = " "
        'GloClv_tipser2 = 0
        Me.Close()
    End Sub

    Private Sub FrmOrdenEjePen_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class