<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMotivosCancelacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_MOTCANLabel As System.Windows.Forms.Label
        Dim MOTCANLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMotivosCancelacion))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.CONMotivoCancelacionBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONMotivoCancelacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.CONMotivoCancelacionBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer
        Me.Clv_MOTCANTextBox = New System.Windows.Forms.TextBox
        Me.MOTCANTextBox = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.CONMotivoCancelacionTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONMotivoCancelacionTableAdapter
        Clv_MOTCANLabel = New System.Windows.Forms.Label
        MOTCANLabel = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.CONMotivoCancelacionBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONMotivoCancelacionBindingNavigator.SuspendLayout()
        CType(Me.CONMotivoCancelacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_MOTCANLabel
        '
        Clv_MOTCANLabel.AutoSize = True
        Clv_MOTCANLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_MOTCANLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_MOTCANLabel.Location = New System.Drawing.Point(30, 51)
        Clv_MOTCANLabel.Name = "Clv_MOTCANLabel"
        Clv_MOTCANLabel.Size = New System.Drawing.Size(50, 15)
        Clv_MOTCANLabel.TabIndex = 0
        Clv_MOTCANLabel.Text = "Clave :"
        '
        'MOTCANLabel
        '
        MOTCANLabel.AutoSize = True
        MOTCANLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MOTCANLabel.ForeColor = System.Drawing.Color.LightSlateGray
        MOTCANLabel.Location = New System.Drawing.Point(23, 78)
        MOTCANLabel.Name = "MOTCANLabel"
        MOTCANLabel.Size = New System.Drawing.Size(57, 15)
        MOTCANLabel.TabIndex = 2
        MOTCANLabel.Text = "Motivo :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.CONMotivoCancelacionBindingNavigator)
        Me.Panel1.Controls.Add(Me.ToolStripContainer1)
        Me.Panel1.Controls.Add(Clv_MOTCANLabel)
        Me.Panel1.Controls.Add(Me.Clv_MOTCANTextBox)
        Me.Panel1.Controls.Add(MOTCANLabel)
        Me.Panel1.Controls.Add(Me.MOTCANTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(559, 208)
        Me.Panel1.TabIndex = 0
        '
        'CONMotivoCancelacionBindingNavigator
        '
        Me.CONMotivoCancelacionBindingNavigator.AddNewItem = Nothing
        Me.CONMotivoCancelacionBindingNavigator.BindingSource = Me.CONMotivoCancelacionBindingSource
        Me.CONMotivoCancelacionBindingNavigator.CountItem = Nothing
        Me.CONMotivoCancelacionBindingNavigator.DeleteItem = Nothing
        Me.CONMotivoCancelacionBindingNavigator.Dock = System.Windows.Forms.DockStyle.None
        Me.CONMotivoCancelacionBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONMotivoCancelacionBindingNavigatorSaveItem})
        Me.CONMotivoCancelacionBindingNavigator.Location = New System.Drawing.Point(344, 3)
        Me.CONMotivoCancelacionBindingNavigator.MoveFirstItem = Nothing
        Me.CONMotivoCancelacionBindingNavigator.MoveLastItem = Nothing
        Me.CONMotivoCancelacionBindingNavigator.MoveNextItem = Nothing
        Me.CONMotivoCancelacionBindingNavigator.MovePreviousItem = Nothing
        Me.CONMotivoCancelacionBindingNavigator.Name = "CONMotivoCancelacionBindingNavigator"
        Me.CONMotivoCancelacionBindingNavigator.PositionItem = Nothing
        Me.CONMotivoCancelacionBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONMotivoCancelacionBindingNavigator.Size = New System.Drawing.Size(241, 25)
        Me.CONMotivoCancelacionBindingNavigator.TabIndex = 2
        Me.CONMotivoCancelacionBindingNavigator.TabStop = True
        Me.CONMotivoCancelacionBindingNavigator.Text = "BindingNavigator1"
        '
        'CONMotivoCancelacionBindingSource
        '
        Me.CONMotivoCancelacionBindingSource.DataMember = "CONMotivoCancelacion"
        Me.CONMotivoCancelacionBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONMotivoCancelacionBindingNavigatorSaveItem
        '
        Me.CONMotivoCancelacionBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONMotivoCancelacionBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONMotivoCancelacionBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONMotivoCancelacionBindingNavigatorSaveItem.Name = "CONMotivoCancelacionBindingNavigatorSaveItem"
        Me.CONMotivoCancelacionBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.CONMotivoCancelacionBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(431, 0)
        Me.ToolStripContainer1.Location = New System.Drawing.Point(3, 3)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.Size = New System.Drawing.Size(431, 25)
        Me.ToolStripContainer1.TabIndex = 5
        Me.ToolStripContainer1.TabStop = False
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        '
        'ToolStripContainer1.TopToolStripPanel
        '
        Me.ToolStripContainer1.TopToolStripPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        '
        'Clv_MOTCANTextBox
        '
        Me.Clv_MOTCANTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_MOTCANTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONMotivoCancelacionBindingSource, "Clv_MOTCAN", True))
        Me.Clv_MOTCANTextBox.Enabled = False
        Me.Clv_MOTCANTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_MOTCANTextBox.Location = New System.Drawing.Point(86, 51)
        Me.Clv_MOTCANTextBox.Name = "Clv_MOTCANTextBox"
        Me.Clv_MOTCANTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_MOTCANTextBox.TabIndex = 0
        Me.Clv_MOTCANTextBox.TabStop = False
        '
        'MOTCANTextBox
        '
        Me.MOTCANTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MOTCANTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONMotivoCancelacionBindingSource, "MOTCAN", True))
        Me.MOTCANTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MOTCANTextBox.Location = New System.Drawing.Point(86, 78)
        Me.MOTCANTextBox.MaxLength = 200
        Me.MOTCANTextBox.Multiline = True
        Me.MOTCANTextBox.Name = "MOTCANTextBox"
        Me.MOTCANTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.MOTCANTextBox.Size = New System.Drawing.Size(455, 43)
        Me.MOTCANTextBox.TabIndex = 1
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(435, 226)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 3
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONMotivoCancelacionTableAdapter
        '
        Me.CONMotivoCancelacionTableAdapter.ClearBeforeFill = True
        '
        'FrmMotivosCancelacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(583, 273)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmMotivosCancelacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catalogo de Motivos de Cancelación"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONMotivoCancelacionBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONMotivoCancelacionBindingNavigator.ResumeLayout(False)
        Me.CONMotivoCancelacionBindingNavigator.PerformLayout()
        CType(Me.CONMotivoCancelacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONMotivoCancelacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONMotivoCancelacionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONMotivoCancelacionTableAdapter
    Friend WithEvents CONMotivoCancelacionBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONMotivoCancelacionBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_MOTCANTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MOTCANTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
End Class
