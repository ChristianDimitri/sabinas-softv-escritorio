﻿Module ModResize

    'Variable para contener primero el tamaño actual, luego la proporcion en la que cambia
    'Nota: *****************************************************
    'El formulario debe tener la propiedad autoscalemode en None
    Dim SizeAux As SizeF

    Public Sub BeginResize(ByRef Frm As Form)
        'Esta funcion se debe llamar en el evento ResizeBegin
        Frm.SuspendLayout()
        'Guardo el tamaño que tiene actualmente el formulario
        SizeAux = New SizeF(Frm.Size)
    End Sub
    Public Sub EndResize(ByRef Frm As Form)
        'Esta función se debe llamar en el evento ResizeEnd
        Dim WidthAnterior, HeightAnterior As Single
        'Guardo el tamaño que Antes de redimensionarlo
        WidthAnterior = SizeAux.Width
        HeightAnterior = SizeAux.Height
        'Pongo en SizeAux las proporciones de ancho y alto que ha cambiado el formulario
        SizeAux.Height = Frm.Height / SizeAux.Height
        SizeAux.Width = Frm.Width / SizeAux.Width
        'Vuelvo a poner el formulario con el tamaño que tenia antes de redimensionarlo, ya que el procedimiento de redimensionado
        'lo incrementará en la proporcion que le paso como parametros en sizeaux
        Frm.Height = HeightAnterior
        Frm.Width = WidthAnterior
        'Escalo todo el control según las proporciones que van en SizeAux, esto escala todos los objetos pero no escala la fuente
        Frm.Scale(SizeAux)
        'Una vez redimensionado los objetos, redimensiono la letra, proporcionalmente al XProp,YProp

        RedimensionaFuente(Frm, SizeAux.Width, SizeAux.Height)
        frm.ResumeLayout()


    End Sub
    'Private Sub Redimensiona(ByVal Control As Control, ByVal XProp As Single, ByVal YProp As Single)
    '    Dim Ctr As Windows.Forms.Control
    '    Control.Size = New Size(Control.Size.Width * XProp, Control.Size.Height * YProp)
    '    Control.Location = New Point(Control.Location.X * XProp, Control.Location.Y * YProp)
    '    Control.Font = New Font(Control.Font.OriginalFontName, Control.Font.Size * XProp, Control.Font.Style, GraphicsUnit.Point)
    '    If Control.Controls.Count >= 1 Then
    '        For Each Ctr In Control.Controls
    '            Redimensiona(Ctr, XProp, YProp)
    '        Next
    '    End If

    'End Sub
    Private Sub RedimensionaFuente(ByVal Control As Control, ByVal XProp As Single, ByVal YProp As Single)
        'Las fuentes se redimensiona según haya cambiado la proporcion del alto y no según el ancho, pero si te 
        'interesa de que cambie según la proporcion horizontal cambia la siguiente linea
        Dim Prop As Single = YProp
        Dim CtrAux As Windows.Forms.Control

        'Primero redimensiono la fuente del control en si y luego la de sus componentes
        Try
            'controlo el error por si no tiene propiedad fon
            Control.Font = New Font(Control.Font.OriginalFontName, Control.Font.Size * Prop, Control.Font.Style, GraphicsUnit.Point)
        Catch ex As Exception
        End Try
        'Escalo ahora las fuentes de los controles que contiene el objeto
        For Each CtrAux In Control.Controls
            'Si el control es a su vez otro contenedor redimensiono sus controles asociados
            If CtrAux.Controls.Count >= 1 Then
                Redimensionafuente(CtrAux, XProp, YProp)
            Else
                Try
                    'controlo el error por si no tiene propiedad fon
                    CtrAux.Font = New Font(CtrAux.Font.OriginalFontName, CtrAux.Font.Size * Prop, CtrAux.Font.Style, GraphicsUnit.Point)
                Catch ex As Exception
                End Try
            End If
        Next

    End Sub
End Module
