﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwRangosComisionSucursal


    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CveRangoLabel = New System.Windows.Forms.Label()
        Me.RangoInferiorLabel = New System.Windows.Forms.Label()
        Me.RangoSuperiorLabel = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.MuestraCatalogoDeRangosDataGridView = New System.Windows.Forms.DataGridView()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LimiteInferior = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LimiteSuperior = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CveRangoTextBox = New System.Windows.Forms.TextBox()
        Me.RangoInferiorTextBox = New System.Windows.Forms.TextBox()
        Me.RangoSuperiorTextBox = New System.Windows.Forms.TextBox()
        Me.TextBoxPago = New System.Windows.Forms.TextBox()
        CType(Me.MuestraCatalogoDeRangosDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CveRangoLabel
        '
        Me.CveRangoLabel.AutoSize = True
        Me.CveRangoLabel.Location = New System.Drawing.Point(200, 126)
        Me.CveRangoLabel.Name = "CveRangoLabel"
        Me.CveRangoLabel.Size = New System.Drawing.Size(64, 13)
        Me.CveRangoLabel.TabIndex = 13
        Me.CveRangoLabel.Text = "Cve Rango:"
        Me.CveRangoLabel.Visible = False
        '
        'RangoInferiorLabel
        '
        Me.RangoInferiorLabel.AutoSize = True
        Me.RangoInferiorLabel.Location = New System.Drawing.Point(187, 152)
        Me.RangoInferiorLabel.Name = "RangoInferiorLabel"
        Me.RangoInferiorLabel.Size = New System.Drawing.Size(77, 13)
        Me.RangoInferiorLabel.TabIndex = 14
        Me.RangoInferiorLabel.Text = "Rango Inferior:"
        Me.RangoInferiorLabel.Visible = False
        '
        'RangoSuperiorLabel
        '
        Me.RangoSuperiorLabel.AutoSize = True
        Me.RangoSuperiorLabel.Location = New System.Drawing.Point(180, 178)
        Me.RangoSuperiorLabel.Name = "RangoSuperiorLabel"
        Me.RangoSuperiorLabel.Size = New System.Drawing.Size(84, 13)
        Me.RangoSuperiorLabel.TabIndex = 15
        Me.RangoSuperiorLabel.Text = "Rango Superior:"
        Me.RangoSuperiorLabel.Visible = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(463, 32)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&NUEVO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(462, 74)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "&MODIFICAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(462, 202)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'MuestraCatalogoDeRangosDataGridView
        '
        Me.MuestraCatalogoDeRangosDataGridView.AllowUserToAddRows = False
        Me.MuestraCatalogoDeRangosDataGridView.AllowUserToDeleteRows = False
        Me.MuestraCatalogoDeRangosDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MuestraCatalogoDeRangosDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.MuestraCatalogoDeRangosDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.LimiteInferior, Me.LimiteSuperior, Me.Pago})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MuestraCatalogoDeRangosDataGridView.DefaultCellStyle = DataGridViewCellStyle4
        Me.MuestraCatalogoDeRangosDataGridView.Location = New System.Drawing.Point(12, 32)
        Me.MuestraCatalogoDeRangosDataGridView.Name = "MuestraCatalogoDeRangosDataGridView"
        Me.MuestraCatalogoDeRangosDataGridView.ReadOnly = True
        Me.MuestraCatalogoDeRangosDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MuestraCatalogoDeRangosDataGridView.Size = New System.Drawing.Size(431, 206)
        Me.MuestraCatalogoDeRangosDataGridView.TabIndex = 13
        Me.MuestraCatalogoDeRangosDataGridView.TabStop = False
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        '
        'LimiteInferior
        '
        Me.LimiteInferior.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.LimiteInferior.DataPropertyName = "LimiteInferior"
        Me.LimiteInferior.HeaderText = "Limite Inferior %"
        Me.LimiteInferior.MinimumWidth = 10
        Me.LimiteInferior.Name = "LimiteInferior"
        Me.LimiteInferior.ReadOnly = True
        Me.LimiteInferior.Width = 138
        '
        'LimiteSuperior
        '
        Me.LimiteSuperior.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.LimiteSuperior.DataPropertyName = "LimiteSuperior"
        Me.LimiteSuperior.HeaderText = "Limite Superior %"
        Me.LimiteSuperior.Name = "LimiteSuperior"
        Me.LimiteSuperior.ReadOnly = True
        Me.LimiteSuperior.Width = 147
        '
        'Pago
        '
        Me.Pago.DataPropertyName = "Pago"
        Me.Pago.HeaderText = "Pago"
        Me.Pago.Name = "Pago"
        Me.Pago.ReadOnly = True
        '
        'CveRangoTextBox
        '
        Me.CveRangoTextBox.Location = New System.Drawing.Point(270, 123)
        Me.CveRangoTextBox.Name = "CveRangoTextBox"
        Me.CveRangoTextBox.ReadOnly = True
        Me.CveRangoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CveRangoTextBox.TabIndex = 14
        Me.CveRangoTextBox.TabStop = False
        Me.CveRangoTextBox.Visible = False
        '
        'RangoInferiorTextBox
        '
        Me.RangoInferiorTextBox.Location = New System.Drawing.Point(270, 149)
        Me.RangoInferiorTextBox.Name = "RangoInferiorTextBox"
        Me.RangoInferiorTextBox.ReadOnly = True
        Me.RangoInferiorTextBox.Size = New System.Drawing.Size(100, 20)
        Me.RangoInferiorTextBox.TabIndex = 15
        Me.RangoInferiorTextBox.TabStop = False
        Me.RangoInferiorTextBox.Visible = False
        '
        'RangoSuperiorTextBox
        '
        Me.RangoSuperiorTextBox.Location = New System.Drawing.Point(270, 175)
        Me.RangoSuperiorTextBox.Name = "RangoSuperiorTextBox"
        Me.RangoSuperiorTextBox.ReadOnly = True
        Me.RangoSuperiorTextBox.Size = New System.Drawing.Size(100, 20)
        Me.RangoSuperiorTextBox.TabIndex = 16
        Me.RangoSuperiorTextBox.TabStop = False
        Me.RangoSuperiorTextBox.Visible = False
        '
        'TextBoxPago
        '
        Me.TextBoxPago.Location = New System.Drawing.Point(270, 201)
        Me.TextBoxPago.Name = "TextBoxPago"
        Me.TextBoxPago.ReadOnly = True
        Me.TextBoxPago.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxPago.TabIndex = 17
        Me.TextBoxPago.TabStop = False
        Me.TextBoxPago.Visible = False
        '
        'BrwRangosComisionSucursal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(623, 262)
        Me.Controls.Add(Me.MuestraCatalogoDeRangosDataGridView)
        Me.Controls.Add(Me.RangoSuperiorLabel)
        Me.Controls.Add(Me.RangoSuperiorTextBox)
        Me.Controls.Add(Me.RangoInferiorLabel)
        Me.Controls.Add(Me.RangoInferiorTextBox)
        Me.Controls.Add(Me.CveRangoLabel)
        Me.Controls.Add(Me.CveRangoTextBox)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBoxPago)
        Me.Name = "BrwRangosComisionSucursal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Rangos Pago Comisiones del Coordinador de Sucursal"
        CType(Me.MuestraCatalogoDeRangosDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents MuestraCatalogoDeRangosDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents CveRangoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RangoInferiorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RangoSuperiorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CveRangoLabel As System.Windows.Forms.Label
    Friend WithEvents RangoInferiorLabel As System.Windows.Forms.Label
    Friend WithEvents RangoSuperiorLabel As System.Windows.Forms.Label
    Friend WithEvents TextBoxPago As System.Windows.Forms.TextBox
    Friend WithEvents id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LimiteInferior As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LimiteSuperior As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pago As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
