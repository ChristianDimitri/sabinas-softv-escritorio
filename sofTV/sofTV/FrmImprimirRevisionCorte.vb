﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Collections.Generic

Public Class FrmImprimirRevisionCorte

    Private customersByCityReport As ReportDocument
    Dim fecha2 As String
    Dim fecha1 As String

    Private Sub FrmImprimirRevisionCorte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ConfigureCrystalReportsReporteREVCO()
    End Sub

    Private Sub ConfigureCrystalReportsReporteREVCO()
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            reportPath = RutaReportes + "\ReporteREVCO.rpt"
            'mySelectFormula = "Cartera Ejecutiva Del Día: " + fecha_cartera


            'customersByCityReport.Load(reportPath)


            'SetDBLogonForReport(connectionInfo, customersByCityReport)

            ''(@op int
            'customersByCityReport.SetParameterValue(0, fecha_cartera)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Fecha_ini", SqlDbType.DateTime, GloFecha_Ini)
            'BaseII.CreateMyParameter("@fecha_fin", SqlDbType.DateTime, GloFecha_Fin)
            'BaseII.ConsultaDT("Reporte_REVCO")

            Dim listatablas As New List(Of String)

            'listatablas.Add("Cartera_Ejecutiva")
            'listatablas.Add("Servicios")
            listatablas.Add("DatosRevision")

            DS = BaseII.ConsultaDS("Reporte_REVCO", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & LocGloNomEmpresa & "'"
            '' CrystalReportViewer1.ReportSource = customersByCityReport
            'customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"


            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            'CrystalReportViewer1.ShowPrintButton = True
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


End Class