﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic

Public Class FrmOrdSer
    'Cambio 08/09/2016 w
    Private customersByCityReport As ReportDocument
    Private LocFecEje As Boolean = False
    Private LocTec As Boolean = False
    Private LocDet As Boolean = False
    Private Cadena As String
    Private Imprime As Integer = 0
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim bloq As Integer = 0
    Dim dime As Integer
    Dim clvTecnicoDescarga As Integer
    Dim clvBitacoraDescarga As Integer
    Private pasa As Integer = 0
    Private auxTecnicoAnterior As Integer

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try

            'If IsNumeric(gloClave) = True And IsNumeric(GloClv_TipSer) = True Then
            If IsNumeric(gloClave) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.CONORDSERTableAdapter.Connection = CON
                Me.CONORDSERTableAdapter.Fill(Me.NewSofTvDataSet.CONORDSER, gloClave, 0)
                CON.Close()
                CREAARBOL()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub Borra_Cambio_Domicilio_Orden_si_no_guardo()
        Dim Con45 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            Con45.Open()
            With cmd
                .CommandText = "Borra_Cambio_Domicilio_Orden_si_no_guardo"
                .CommandTimeout = 0
                .Connection = Con45
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_orden", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = gloClv_Orden
                .Parameters.Add(prm)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            Con45.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Borra_Camdo_si_no_guardo_orden()
        Dim con50 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            con50.Open()
            With cmd
                .CommandText = "Borra_Camdo_si_no_guardo_orden"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con50

                Dim prm As New SqlParameter("@clv_orden", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = gloClave
                .Parameters.Add(prm)

                Dim i As Integer = cmd.ExecuteNonQuery()

            End With
            con50.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmOrdSer_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        'dim cmd as New SqlClient 
        If GLOCONTRATOSEL > 0 Then
            Me.ContratoTextBox.Text = GLOCONTRATOSEL
            Me.ContratoCompania.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
            GLOCONTRATOSEL = 0
            GloContrato = GLOCONTRATOSEL
            eContratoCompania = 0
            GloClv_TipSer = 0
        End If
        'If GloBndTipSer = True Then
        '    GloBndTipSer = False
        '    Me.ComboBox5.SelectedValue = GloClv_TipSer
        '    Me.ComboBox5.Text = GloNom_TipSer
        '    Me.ComboBox5.FindString(GloNom_TipSer)
        '    Me.ComboBox5.Text = GloNom_TipSer
        '    Me.TextBox2.Text = GloNom_TipSer
        'End If
        If GloBndTrabajo = True Then
            GloBndTrabajo = False
            GloClv_TipSer = 0
            If bndCAMDO = True Then
                bndCAMDO = False
                Borra_Cambio_Domicilio_Orden_si_no_guardo()
            End If
            CON.Open()
            Me.BUSCADetOrdSerTableAdapter.Connection = CON
            Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(gloClv_Orden, Long)))
            CON.Close()
        End If
        If GloBloqueaDetalle = True Then
            GloBloqueaDetalle = False
            Me.BUSCADetOrdSerDataGridView.Enabled = True
        End If

        'Cambios Pablo
        'Activa los combos de Nap y Tap dependiendo de las ordenes
        Dim t As Integer = 0, n As Integer = 0
        For Each dRow As DataGridViewRow In BUSCADetOrdSerDataGridView.Rows
            If dRow.Cells(3).Value.ToString().Contains("IACTV") Or dRow.Cells(3).Value.ToString().Contains("IACNET") Or dRow.Cells(3).Value.ToString().Contains("RIACT") Then
                ComboBoxTap.Visible = True
                CMBLabelTab.Visible = True
                t = 1
            ElseIf t = 0 Then
                ComboBoxTap.Visible = False
                CMBLabelTab.Visible = False
            End If

            If dRow.Cells(3).Value.ToString().Contains("IACFT") Or dRow.Cells(3).Value.ToString().Contains("IACFN") Then
                ComboBoxNap.Visible = True
                CMBLabelNap.Visible = True
                n = 1
            ElseIf n = 0 Then
                ComboBoxNap.Visible = False
                CMBLabelNap.Visible = False
            End If
        Next
        t = 0
        n = 0

        'Fin cambios Pablo


        If eStatusOrdSer = "P" And eActTecnico = False Then
            Me.Tecnico.Enabled = False
            cbCuadrilla.Enabled = False
        Else
            Me.Tecnico.Enabled = True
            cbCuadrilla.Enabled = True
        End If

        checaBitacoraTecnico(gloClave, "O")
        If clvBitacoraDescarga > 0 Then
            Me.Tecnico.SelectedValue = clvTecnicoDescarga
            Me.Tecnico.Enabled = False
        Else
            Me.Tecnico.Enabled = True
        End If

        'ENTRAGA DE APARTATO PARA LAS RECONTRATACIONES
        If eBndEntregaAparato = True Then
            eBndEntregaAparato = False
            GuardaRelOrdenUsuario()
            ModOrdSer(Clv_OrdenTextBox.Text, 0, ContratoTextBox.Text, Fecha_SoliciutudMaskedTextBox.Value, Fec_EjeTextBox1.Text, Visita1TextBox1.Text, Visita2TextBox1.Text, StatusTextBox.Text, Tecnico.SelectedValue, IMPRESACheckBox.Checked, 0, ObsTextBox.Text, "")
            PreEjecutaOrdSer(Clv_OrdenTextBox.Text)
            MsgBox(mensaje5)
            Me.Close()
        End If

        'Guarda el Motivo por el cual ha sido dado de baja un servicio.
        'Eric--------------------------------------------------------------------
        If GloClv_MotCan > 0 Then

            CON.Open()
            Me.InsertMotCanServTableAdapter.Connection = CON
            Me.InsertMotCanServTableAdapter.Fill(Me.DataSetEric.InsertMotCanServ, Me.Clv_OrdenTextBox.Text, GloClv_MotCan)
            CON.Close()
            GloClv_MotCan = 0

            GuardaRelOrdenUsuario()
            'CON.Open()
            Me.Validate()
            Me.CONORDSERBindingSource.EndEdit()


            ModOrdSer(Clv_OrdenTextBox.Text, 0, ContratoTextBox.Text, Fecha_SoliciutudMaskedTextBox.Value, Fec_EjeTextBox1.Text, Visita1TextBox1.Text, Visita2TextBox1.Text, StatusTextBox.Text, Tecnico.SelectedValue, IMPRESACheckBox.Checked, 0, ObsTextBox.Text, "")
            PreEjecutaOrdSer(Clv_OrdenTextBox.Text)

            'Cuadrilla-----------------------------------------------------------------
            If cbCuadrilla.SelectedValue > 0 Then
                NueRelOrdSerCuadrilla(Clv_OrdenTextBox.Text, cbCuadrilla.SelectedValue)
            End If
            '--------------------------------------------------------------------------

            'Me.CONORDSERTableAdapter.Connection = CON
            'Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)
            'Me.PREEJECUTAOrdSerTableAdapter.Connection = CON
            'Me.PREEJECUTAOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.PREEJECUTAOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
            'CON.Close()
            MsgBox(mensaje5)

            'Eric----------------------------------------------
            Dim CONERIC As New SqlConnection(MiConexion)
            Dim eRes As Long = 0
            Dim eMsg As String = Nothing
            CONERIC.Open()
            Me.ChecaOrdSerRetiroTableAdapter.Connection = CONERIC
            Me.ChecaOrdSerRetiroTableAdapter.Fill(Me.DataSetEric.ChecaOrdSerRetiro, CType(Me.Clv_OrdenTextBox.Text, Long), eRes, eMsg)
            CONERIC.Close()
            If eRes > 0 And IdSistema = "SA" Or IdSistema = "VA" Then
                ImprimeOrdSerRetiro(eRes)
            End If
            '------------------------------------------------------

            GloBnd = True
            GloGuardo = False
            If opcion = "N" Then
                CON.Open()
                Me.Imprime_OrdenTableAdapter.Connection = CON
                Me.Imprime_OrdenTableAdapter.Fill(Me.ProcedimientosArnoldo2.Imprime_Orden, Me.Clv_OrdenTextBox.Text, Imprime)
                CON.Close()
                If Imprime = 0 Then
                    ConfigureCrystalReports(0, "")
                ElseIf Imprime = 1 Then
                    MsgBox("La orden es de proceso Automático No se Imprimio", MsgBoxStyle.Information)
                End If
            End If
            CON.Close()
            Me.Close()
        End If
        If bloq = 1 Then
            bloq = 0
            eGloContrato = Me.ContratoTextBox.Text
            FrmBloqueo.Show()
            Me.Panel1.Enabled = False
            Me.Panel3.Enabled = False
            Me.Panel6.Enabled = False
            Me.Panel7.Enabled = False
            Me.Panel8.Enabled = False
        End If
        '----------------------------------------------------------------------------------------
    End Sub


    Private Sub BUSCLIPORCONTRATOXsd(oContrato As Long, oNOMBRE As String, oCALLE As String, oNUMERO As String, ociudad As String, oOP As Integer, oClv_Tipser As Integer, oIdCompania As Integer)
        '@CONTRATO AS BIGINT,@NOMBRE VARCHAR(150),@CALLE VARCHAR(150),@NUMERO VARCHAR(100),@CIUDAD VARCHAR(100),@OP INT,@Clv_TipSer int,@IdCompania int=0
        Try
            Dim TbluspBuscaOrdSer As New DataTable

            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)

            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)

            BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, oNOMBRE, 150)
            BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, oCALLE, 150)
            BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, oNUMERO, 150)
            BaseII.CreateMyParameter("@CIUDAD", SqlDbType.VarChar, ociudad, 100)


            BaseII.CreateMyParameter("@OP", SqlDbType.Int, oOP)
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, oClv_Tipser)
            BaseII.CreateMyParameter("@IdCompania", SqlDbType.Int, oIdCompania)

            TbluspBuscaOrdSer = BaseII.ConsultaDT("BUSCLIPORCONTRATO")
            For Each dr As DataRow In TbluspBuscaOrdSer.Rows
                NOMBRELabel1.Text = dr(1).ToString()
                CALLELabel1.Text = dr(2).ToString() & " #" & dr(4).ToString() & " Colonia: " & dr(3).ToString() & " Ciudad: " & dr(5).ToString()

                If dr(6).ToString() = "1" Then
                    SOLOINTERNETCheckBox.CheckState = CheckState.Checked
                Else
                    SOLOINTERNETCheckBox.CheckState = CheckState.Unchecked
                End If
                If dr(7).ToString() = "1" Then
                    ESHOTELCheckBox.CheckState = CheckState.Checked
                Else
                    ESHOTELCheckBox.CheckState = CheckState.Unchecked
                End If
                LblCompania.Text = dr(8).ToString()
            Next


            'Dim reader As SqlDataReader(TbluspBuscaOrdSer)            

            'Using reader
            '    While reader.Read
            '        DimeTipSer_CualEsPrincipal = reader.GetValue(0)
            '    End While
            'End Using


            'ComboBoxCartera.Text = ""
        Catch ex As Exception

        End Try
    End Sub


    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)

            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                BUSCLIPORCONTRATOXsd(Me.ContratoTextBox.Text, (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(0, Integer)), 0, GloIdCompania)


                num2 = 0
                'Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON
                'Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(0, Integer)), 0)
                CON.Open()
                Me.BuscaBloqueadoTableAdapter.Connection = CON
                Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
                CON.Close()
                CREAARBOL()
                If num2 = 1 Then
                    eGloContrato = Me.ContratoTextBox.Text
                    bloq = 1
                End If
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            CON.Open()
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            CON.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Teléfonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub CREAARBOL11()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If


            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                Me.TreeView1.Nodes.Add(Trim(FilaRow("Servicio").ToString()))
                I += 1
            Next
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ACTIVA(ByVal BND As Boolean)
        'Me.ComboBox4.Enabled = BND
        'Me.BindingNavigatorDeleteItem.Enabled = BND
        RadioButton2.Enabled = BND
        RadioButton3.Enabled = BND
        Me.Tecnico.Enabled = BND
        cbCuadrilla.Enabled = BND
        Me.Fec_EjeTextBox.Enabled = BND
        Me.Visita1TextBox.Enabled = BND
        Me.Visita2TextBox.Enabled = BND
        'SolucionTextBox.Enabled = BND
        Panel2.Enabled = BND
        If BND = True Then
            Me.RadioButton1.Enabled = False
            Me.ContratoTextBox.Enabled = False
            Me.Button1.Enabled = False
            'Me.SplitContainer1.Enabled = False
            'Me.ComboBox3.Enabled = False
            'Me.ProblemaTextBox.Enabled = False
        End If
    End Sub

    Private Sub FrmOrdSer_AutoSizeChanged(sender As Object, e As EventArgs) Handles Me.AutoSizeChanged

    End Sub



    Private Sub FrmOrdSer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If dameStatusOrdenQueja(CInt(Clv_OrdenTextBox.Text), "O") = "P" And opcion = "M" Then
                softv_BorraDescarga(CInt(Clv_OrdenTextBox.Text), "O")
                BorraArticulosAsignados()
            End If
            Dim CON As New SqlConnection(MiConexion)
            Dim error2 As Integer = Nothing

            LocNo_Bitacora = 0
            CON.Open()
            Me.Valida_DetOrdenTableAdapter.Connection = CON
            Me.Valida_DetOrdenTableAdapter.Fill(Me.NewSofTvDataSet.Valida_DetOrden, New System.Nullable(Of Long)(CType(Clv_OrdenTextBox.Text, Long)))
            CON.Close()
            If IsNumeric(Me.ValidacionTextBox.Text) = False Then Me.ValidacionTextBox.Text = 0
            Me.valida()
            'If Me.CONTADORTextBox.Text = 4 Then
            ' GloGuardo = True
            ' End If
            If GloGuardo = True And Me.ValidacionTextBox.Text > 0 Then
                Dim RESP As MsgBoxResult = MsgBoxResult.Yes
                If opcion = "C" Then
                    Exit Sub
                End If
                If Me.StatusTextBox.Text = "P" Then

                    Me.ValidaOrdSerManuales(CLng(Me.Clv_OrdenTextBox.Text))
                    If eRes = 1 Then
                        eRes = 0
                        eMsg = String.Empty
                        Exit Sub
                    End If

                    RESP = MsgBox("Desea Guardar la Orden que Genero", MsgBoxStyle.YesNo)
                    If RESP = MsgBoxResult.No Then
                        GloGuardo = False
                        'GloClv_TipSer = Me.ComboBox5.SelectedValue
                        GloClv_TipSer = 0
                        'GloNom_TipSer = Me.ComboBox5.Text
                        GloNom_TipSer = ""
                        Borra_Camdo_si_no_guardo_orden()
                        CON.Open()
                        Me.CONORDSERTableAdapter.Connection = CON
                        Me.CONORDSERTableAdapter.Delete(gloClave, 0)
                        CON.Close()
                    Else
                        error2 = Checa_si_tiene_camdo(gloClave)

                        If error2 > 0 Then
                            GloBnd = True
                            MsgBox("Se Tiene Que Capturar el Nuevo Domicilio", MsgBoxStyle.Information)
                            CON.Open()
                            Me.CONORDSERTableAdapter.Connection = CON
                            Me.CONORDSERTableAdapter.Delete(gloClave, 0)
                            CON.Close()
                            Exit Sub

                        End If
                    End If
                ElseIf Me.StatusTextBox.Text = "E" And Me.CONTADORTextBox.Text = 4 Then
                    RESP = MsgBox("La Orden ya tiene todos los datos para Ejecutarse ¿ Desea Salir sin Grabar ? ", MsgBoxStyle.YesNo)
                    If RESP = MsgBoxResult.No Then
                        e.Cancel = True
                        Exit Sub
                    End If
                End If
                GloBnd = True
            ElseIf GloGuardo = True And Me.ValidacionTextBox.Text = 0 Then
                GloBnd = True
                GloGuardo = False
                'GloClv_TipSer = Me.ComboBox5.SelectedValue
                'GloNom_TipSer = Me.ComboBox5.Text
                GloClv_TipSer = 0
                GloNom_TipSer = ""
                CON.Open()
                Me.CONORDSERTableAdapter.Connection = CON
                Me.CONORDSERTableAdapter.Delete(gloClave, GloClv_TipSer)
                CON.Close()
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmOrdSer_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave

    End Sub


    Private Sub FrmOrdSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            BindingNavigatorDeleteItem.Enabled = False
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            Dim valor As Integer = 0

            'No paresca "Es Hotel"
            If IdSistema = "LO" Or IdSistema = "YU" Then
                Me.Label13.Visible = False
                Me.ESHOTELCheckBox.Visible = False
            End If

            If EsPreregistro() Then
                RadioButton3.Visible = False
                Label5.Visible = False
                Visita1TextBox.Visible = False
                Fec_EjeTextBox1.Visible = False
                Label11.Visible = False
                Visita1TextBox1.Visible = False
                Visita2TextBox1.Visible = False
                Visita2TextBox.Visible = False
            End If

            CON.Open()
            'GloClv_TipSer = 1000
            'TODO: esta línea de código carga datos en la tabla 'DataSetarnoldo.Dame_fecha_hora_serv' Puede moverla o quitarla según sea necesario.
            Me.Dame_fecha_hora_servTableAdapter.Connection = CON
            Me.Dame_fecha_hora_servTableAdapter.Fill(Me.DataSetarnoldo.Dame_fecha_hora_serv)
            'TODO: esta línea de código carga datos en la tabla 'DataSetarnoldo.Dame_fecha_hora_serv' Puede moverla o quitarla según sea necesario.
            Locclv_folio = 0
            LocNo_Bitacora = 0
            LocValida1 = False
            Bloquea = False

            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
            Me.DameClv_Session_TecnicosTableAdapter.Connection = CON
            Me.DameClv_Session_TecnicosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Tecnicos, clv_sessionTecnico)
            Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
            Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRATECNICOS' Puede moverla o quitarla según sea necesario.
            'Me.MUESTRATECNICOSTableAdapter.Connection = CON
            'Me.MUESTRATECNICOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATECNICOS)
            'Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
            'Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Tecnicos_Almacen, 0)
            CON.Close()

            If opcion = "N" Then
                MuestraRelOrdenesTecnicos(0)
                Me.Button3.Visible = False
                Me.CONORDSERBindingSource.AddNew()
                GloControlaReloj = 0
                Me.StatusTextBox.Text = "P"
                'FrmSelTipServicio.Show()
                Me.Fecha_SoliciutudMaskedTextBox.Text = Now
                ACTIVA(False)
                Panel1.Enabled = True
                Panel7.Enabled = True
                'Lo pUso Eric
                Panel6.Enabled = False
                Me.Panel8.Enabled = False

                'Llena combo de Técnicos


            ElseIf opcion = "C" Then

                If IsNumeric(gloClave) = True Then
                    DameAgenda()
                    MuestraRelOrdenesTecnicos(gloClave)
                    ContratoCompania.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
                    ContratoTextBox.Text = loccontratoordenes
                    GloControlaReloj = 0
                    'Panel1.Enabled = False
                    Panel6.Enabled = False
                    Panel7.Enabled = False
                    Me.Panel3.Enabled = False
                    Bloquea = True
                    BUSCA(gloClave)
                    CREAARBOL()
                    Me.Button1.Enabled = False
                    Me.Panel2.Enabled = False
                    Me.Fecha_SoliciutudMaskedTextBox.Enabled = False
                    Me.CONORDSERBindingNavigator.Enabled = False
                    Me.ContratoTextBox.ReadOnly = True
                    Me.ContratoCompania.ReadOnly = True
                    Me.FolioTextBox.ReadOnly = True
                    Me.Fec_EjeTextBox.ReadOnly = True
                    'Me.GroupBox1.Enabled = True
                    ' Me.TreeView1.Enabled = True
                    Me.Label2.Visible = True 'Etiqueta Visible
                    Me.Label2.Text = "Se generó el número de bitácora: " & Cadena 'Etiqueta Text concatenar 
                    CON.Open()
                    Me.Muestra_no_ordenTableAdapter.Connection = CON
                    Me.Muestra_no_ordenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_no_orden, CInt(gloClave))
                    Me.BUSCADetOrdSerTableAdapter.Connection = CON
                    Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(gloClave, Long)))

                    'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
                    'Eric
                    SP_LLena_Bitacora_Ordenes(GloUsuario, "Consulto el ", gloClave)
                    Me.ValidaSiEsAcometidaTableAdapter.Connection = CON
                    Me.ValidaSiEsAcometidaTableAdapter.Fill(Me.DataSetEric.ValidaSiEsAcometida, CInt(gloClave), eResAco)
                    CON.Close()
                    If (eResAco = 1 And IdSistema = "TO") Or (eResAco = 1 And IdSistema = "SA") Or (eResAco = 1 And IdSistema = "VA") Or (eResAco = 1 And IdSistema = "AG") Then
                        'CON.Open()
                        'Me.ConRelCtePlacaTableAdapter.Connection = CON
                        'Me.ConRelCtePlacaTableAdapter.Fill(Me.DataSetEric.ConRelCtePlaca, Me.ContratoTextBox.Text)
                        'CON.Close()

                        'OBTENEMOS LA PLACA Y TAP DEL CLIENTE EN CASO DE QUE LA TENGA (INICIO) *JUANJO*
                        'uspDameTapCliente(CLng(Me.ContratoTextBox.Text))
                        'OBTENEMOS LA PLACA Y TAP DEL CLIENTE EN CASO DE QUE LA TENGA (INICIO) *JUANJO*
                        DameSectorCliente()
                        Me.Label4.Visible = True
                        Me.PlacaTextBox.Visible = True
                        LBSector.Visible = True
                        ComboBoxSector.Visible = True
                    Else
                        Me.Label4.Visible = False
                        Me.PlacaTextBox.Visible = False
                    End If

                    If eResAco = 1 And IdSistema = "AG" Then
                        'Llena_Tabs_Por_Contrato(Me.ContratoTextBox.Text)

                        'PARA LLENAR EL COMBO DE LOS TAP (INICIO) *JUANJO*
                        uspConsultaTap(CLng(Me.ContratoTextBox.Text), 0, 0)
                        'PARA LLENAR EL COMBO DE LOS TAP (INICIO) *JUANJO*
                        DameSectorCliente()
                        LBSector.Visible = True
                        ComboBoxSector.Visible = True
                        'ConRelClienteTab(Me.ContratoTextBox.Text)
                        'Me.CMBLabelTab.Visible = True
                        ''Me.TextBoxTab.Visible = True
                        'Me.ComboBoxTap.Visible = True
                        Me.CMBLabelTab.Visible = True
                        Me.ComboBoxTap.Visible = True

                        Me.CMBLabelNap.Visible = True
                        Me.ComboBoxNap.Visible = True
                    End If

                    'CONSULTA LA RELACION QUE HAY ENTRE LA ORDEN Y EL USUARIO
                    CON.Open()
                    Me.Consulta_RelOrdenUsuarioTableAdapter.Connection = CON
                    Me.Consulta_RelOrdenUsuarioTableAdapter.Fill(Me.DataSetEric2.Consulta_RelOrdenUsuario, gloClave)
                    CON.Close()
                    Me.Label7.Visible = True
                    Me.GeneroLabel1.Visible = True
                    If Me.StatusTextBox.Text = "E" Then
                        Me.Label12.Visible = True
                        Me.EjecutoLabel1.Visible = True
                    End If
                End If
            ElseIf opcion = "M" Then
                If IsNumeric(gloClave) = True Then
                    MuestraRelOrdenesTecnicos(gloClave)
                    'MuestraRelOrdenesTecnicos(0)
                    DameAgenda()
                    ContratoCompania.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
                    ContratoTextBox.Text = loccontratoordenes
                    'Edgar 04/09/2015
                    'Me.Panel3.Enabled = False
                    Button9.Enabled = False
                    Button2.Enabled = False
                    'Fin
                    ACTIVA(True)
                    Panel1.Enabled = True
                    Panel6.Enabled = True
                    Panel7.Enabled = True
                    BUSCA(gloClave)
                    CREAARBOL()
                    CON.Open()
                    Me.BUSCADetOrdSerTableAdapter.Connection = CON
                    Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(gloClave, Long)))

                    Me.MUESTRATRABAJOSTableAdapter.Connection = CON
                    'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
                    Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, 0)
                    'eric
                    Me.ValidaSiEsAcometidaTableAdapter.Connection = CON
                    Me.ValidaSiEsAcometidaTableAdapter.Fill(Me.DataSetEric.ValidaSiEsAcometida, Me.Clv_OrdenTextBox.Text, eResAco)
                    CON.Close()

                    ValidaDetalleOrden_DetalleGrid()
                    validaEliminarOrden()

                    For Each dRow As DataGridViewRow In BUSCADetOrdSerDataGridView.Rows
                        If dRow.Cells(3).Value.ToString().Contains("IACTV") Or dRow.Cells(3).Value.ToString().Contains("IACNET") Or dRow.Cells(3).Value.ToString().Contains("IAPAG") Or dRow.Cells(3).Value.ToString().Contains("ICABM") Or dRow.Cells(3).Value.ToString().Contains("IANTX") Then
                            Button4.Visible = True
                        End If
                    Next

                    PermiteAgregarRepetidores()

                    If (eResAco = 1 And IdSistema = "TO") Or (eResAco = 1 And IdSistema = "SA") Or (eResAco = 1 And IdSistema = "VA") Or (eResAco = 1 And IdSistema = "AG") Then
                        'CON.Open()
                        'Me.ConRelCtePlacaTableAdapter.Connection = CON
                        'Me.ConRelCtePlacaTableAdapter.Fill(Me.DataSetEric.ConRelCtePlaca, Me.ContratoTextBox.Text)
                        'CON.Close()

                        'OBTENEMOS LA PLACA Y TAP DEL CLIENTE EN CASO DE QUE LA TENGA (INICIO) *JUANJO*
                        uspDameTapCliente(CLng(Me.ContratoTextBox.Text))
                        'OBTENEMOS LA PLACA Y TAP DEL CLIENTE EN CASO DE QUE LA TENGA (INICIO) *JUANJO*
                        DameSectorCliente()
                        Me.Label4.Visible = True
                        Me.PlacaTextBox.Visible = True
                        LBSector.Visible = True
                        ComboBoxSector.Visible = True
                    Else
                        Me.Label4.Visible = False
                        Me.PlacaTextBox.Visible = False
                    End If

                    If eResAco = 1 And IdSistema = "AG" Then
                        'Llena_Tabs_Por_Contrato(Me.ContratoTextBox.Text)

                        'PARA LLENAR EL COMBO DE LOS TAP (INICIO) *JUANJO*
                        uspConsultaTap(CLng(Me.ContratoTextBox.Text), 0, 0)
                        'PARA LLENAR EL COMBO DE LOS TAP (INICIO) *JUANJO*

                        ' ConRelClienteTab(Me.ContratoTextBox.Text)
                        'Me.CMBLabelTab.Visible = True
                        ''Me.TextBoxTab.Visible = True
                        'Me.ComboBoxTap.Visible = True
                        Me.CMBLabelTab.Visible = True
                        Me.ComboBoxTap.Visible = True
                        Me.CMBLabelNap.Visible = True
                        Me.ComboBoxNap.Visible = True
                    End If

                    If Me.StatusTextBox.Text = "E" Then
                        Me.Label2.Visible = True 'Etiqueta Visible
                        Me.Label2.Text = "Se generó el número de bitácora: " & Cadena 'Etiqueta Text concatenar 
                        CON.Open()
                        Me.Muestra_no_ordenTableAdapter.Connection = CON
                        Me.Muestra_no_ordenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_no_orden, CInt(gloClave))
                        Panel1.Enabled = False
                        Panel6.Enabled = False
                        Panel7.Enabled = False
                        Bloquea = True
                        'CONSULTA LA RELACION QUE HAY ENTRE LA ORDEN Y EL USUARIO
                        Me.Consulta_RelOrdenUsuarioTableAdapter.Connection = CON
                        Me.Consulta_RelOrdenUsuarioTableAdapter.Fill(Me.DataSetEric2.Consulta_RelOrdenUsuario, gloClave)
                        CON.Close()
                        Me.Label7.Visible = True
                        Me.GeneroLabel1.Visible = True
                        If Me.StatusTextBox.Text = "E" Then
                            Me.Label12.Visible = True
                            Me.EjecutoLabel1.Visible = True
                        End If

                    ElseIf Me.StatusTextBox.Text = "P" Then
                        CON.Open()
                        Me.ValidarNuevoTableAdapter.Connection = CON
                        Me.ValidarNuevoTableAdapter.Fill(Me.DataSetarnoldo.ValidarNuevo, gloClave, 0, valor)
                        CON.Close()
                        If valor <= 0 Then
                            Panel1.Enabled = True
                            Panel6.Enabled = True
                            Panel7.Enabled = True

                            Bloquea = False
                            Timer1.Enabled = True
                            GloControlaReloj = 1
                        Else
                            Timer1.Enabled = False
                            GloControlaReloj = 0
                            Panel1.Enabled = False
                            Panel6.Enabled = False
                            Panel7.Enabled = False
                            Me.Panel8.Enabled = False
                            Bloquea = True
                            MsgBox("La orden no se puede ejecutar de forma manual ya que este tipo de orden de servicio al cliente se procesa de forma automatica ", MsgBoxStyle.Information)
                        End If

                        Me.StatusTextBox.Text = "E"
                        Me.Fec_EjeTextBox.Enabled = True
                        Me.Visita1TextBox.Enabled = False
                        Me.Visita2TextBox.Enabled = False
                        Me.TextBox1.Visible = False
                        Me.Fec_EjeTextBox.Focus()

                        'lineas de eric

                        If eStatusOrdSer = "P" Then
                            CON.Open()
                            Me.ValidaSiEsAcometidaTableAdapter.Connection = CON
                            Me.ValidaSiEsAcometidaTableAdapter.Fill(Me.DataSetEric.ValidaSiEsAcometida, Me.Clv_OrdenTextBox.Text, eResAco)
                            CON.Close()
                            If (eResAco = 1 And IdSistema = "TO") Or (eResAco = 1 And IdSistema = "SA") Or (eResAco = 1 And IdSistema = "VA") Or (eResAco = 1 And IdSistema = "AG") Then
                                Me.Label4.Visible = True
                                Me.PlacaTextBox.Visible = True
                                LBSector.Visible = True
                                ComboBoxSector.Visible = True
                            Else
                                Me.Label4.Visible = False
                                Me.PlacaTextBox.Visible = False
                            End If

                            If eResAco = 1 And IdSistema = "AG" Then
                                'PARA LLENAR EL COMBO DE LOS TAP (INICIO) *JUANJO*
                                uspConsultaTap(CLng(Me.ContratoTextBox.Text), 0, 0)
                                'PARA LLENAR EL COMBO DE LOS TAP (INICIO) *JUANJO*

                                'Llena_Tabs_Por_Contrato(Me.ContratoTextBox.Text)
                                'ConRelClienteTab(Me.ContratoTextBox.Text)
                                'Me.CMBLabelTab.Visible = True
                                ''Me.TextBoxTab.Visible = True
                                'Me.ComboBoxTap.Visible = True
                                Me.CMBLabelTab.Visible = True
                                Me.ComboBoxTap.Visible = True
                                Me.CMBLabelNap.Visible = True
                                Me.ComboBoxNap.Visible = True
                            End If
                        End If

                        ''Me.Label4.Visible = True'
                        ''Me.Label4.Text = "Ejecución : " 
                        ''If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
                        '' Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
                        ''Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
                        ''Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
                        ''End If

                        'DAMEFechayHoraDelServidor()

                    ElseIf Me.StatusTextBox.Text = "V" Then
                        If IsDate(Me.Visita1TextBox1.Text) = True Then
                            Me.Visita1TextBox.Enabled = False
                        End If
                        If IsDate(Me.Visita2TextBox.Text) = True Then
                            Me.Visita2TextBox.Enabled = False
                        End If

                    End If
                End If

                checaBitacoraTecnico(gloClave, "O")
                If clvBitacoraDescarga > 0 Then
                    Me.Tecnico.SelectedValue = clvTecnicoDescarga
                    Me.Tecnico.Enabled = False
                Else
                    Me.Tecnico.Enabled = True
                End If

            End If

            'Me.CONDetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.CONDetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
            If opcion = "M" Or opcion = "C" Then
                Dim cone As New SqlClient.SqlConnection(MiConexion)
                cone.Open()
                NUM = 0
                num2 = 0
                Me.BuscaBloqueadoTableAdapter.Connection = cone
                Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, loccontratoordenes, CInt(NUM), CInt(num2))
                cone.Close()
                If num2 = 1 Then
                    'eGloContrato = Contrato
                    bloq = 1
                End If
                ' Me.ContratoTextBox.Text = loccontratoordenes
                ChecaRelOrdenUsuario(gloClave)
                'Llena combo de Técnicos
                CONOrdSerFechas(gloClave)

                'OBTENEMOS LA PLACA Y TAP DEL CLIENTE EN CASO DE QUE LA TENGA (INICIO) *JUANJO*
                uspDameTapCliente(CLng(Me.ContratoTextBox.Text))
                'OBTENEMOS LA PLACA Y TAP DEL CLIENTE EN CASO DE QUE LA TENGA (INICIO) *JUANJO*
            End If
            If opcion = "N" Or opcion = "M" Then
                UspDesactivaBotones(Me, Me.Name)
            End If
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
            If opcion = "M" Then
                Button9.Enabled = False
                Button2.Enabled = False
                ValidaDetalleOrden_Primer()
            End If
            Me.BackColor = Panel7.BackColor

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

        If EsPreregistro() And Tecnico.Items.Count <> 0 And opcion = "M" Then
            Tecnico.SelectedIndex = 0
        End If
    End Sub

    Private Sub PermiteAgregarRepetidores()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_orden", SqlDbType.BigInt, gloClave)
        BaseII.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("PreguntaRepetidorInternet")
        If CBool(BaseII.dicoPar("@bnd").ToString) Then
            btnAgregarRepetidor.Visible = True
        Else
            btnAgregarRepetidor.Visible = False
        End If
    End Sub
    Private Sub DameSectorCliente()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoTextBox.Text)
            ComboBoxSector.DataSource = BaseII.ConsultaDT("DameSectorCliente")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.BorraMotivoCanServTableAdapter.Connection = CON
        'Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, Me.Clv_OrdenTextBox.Text, 0, 0, 0, 1)
        'CON.Close()
        Me.CONORDSERBindingSource.CancelEdit()
        GloBnd = True
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, Me.ContratoTextBox.Text)
        BaseII.CreateMyParameter("@clv_orden", SqlDbType.Int, Clv_OrdenTextBox.Text)
        BaseII.Inserta("BorraInformacionZonaNorte")
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click

        If validaEliminarOrden() = 1 Then
            GloBnd = True


            Dim CON As New SqlConnection(MiConexion)

            'RAPAR y RETCA
            GuardaMovSist(0, GloUsuario, gloClave)

            Borra_Orden(gloClave)
            'CON.Open()
            'Me.CONORDSERTableAdapter.Connection = CON
            'Me.CONORDSERTableAdapter.Delete(gloClave, 0)
            'CON.Close()
            'MsgBox(mensaje6)
            'GloBnd = True

            'Bitácora del sistema
            bitsist(GloUsuario, ContratoTextBox.Text, "Softv", Me.Name, "Se eliminó orden de servicio", "", gloClave.ToString(), "AG")
            MsgBox("Se a borrado la orden correctamente ", vbInformation)
            Me.Close()
        Else
            MsgBox("No tiene permisos para borrar la orden", vbInformation)
        End If
    End Sub
    Private Sub Dime_Si_Graba()
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Dim comando As New SqlCommand
        Dim reader As SqlDataReader
        If IsDate(Me.Visita2TextBox.Text) = True Then
            conlidia.Open()
            With comando
                .Connection = conlidia
                .CommandText = "Exec Dimesigrabaord " & CStr(gloClave) & "," & "0" & ",'" & CStr(Me.Visita2TextBox.Text) & "'"
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        ' Process SprocResults datareader here.
                        Me.TextBox4.Text = reader.GetValue(0)
                    End While
                End Using
            End With
            conlidia.Close()
            dime = Me.TextBox4.Text
        Else
            dime = 0
        End If
    End Sub
    Private Function Checa_si_tiene_camdo(ByVal clv_orden As Long) As Integer
        Dim con60 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim error1 As Integer = Nothing
        Try
            con60.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "Checa_si_tiene_camdo"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con60

                Dim prm As New SqlParameter("@clv_orden", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_orden
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@error", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim i As Integer = cmd.ExecuteNonQuery()

                error1 = prm1.Value

            End With
            con60.Close()
            Checa_si_tiene_camdo = error1
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function

    Private Sub CONORDSERBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONORDSERBindingNavigatorSaveItem.Click

        If IsNumeric(ContratoTextBox.Text) And opcion = "N" Then
            pasa = 0
            uspContratoServ()
        Else
            pasa = 1
        End If

        If pasa = 1 Then

            If opcion = "M" Then
                BUSCADetOrdSerDataGridView.EndEdit()
                If ValidaBorraDetalleOrden_DetalleGrid() = False Then
                    MsgBox("Se tiene que recibír por los menos un aparato para grabar la orden", MsgBoxStyle.Information, "Información")
                    Exit Sub
                End If
                Try


                    'If IsNumeric(Clv_TecnicoTextBox.Text) = False Then Clv_TecnicoTextBox.Text = 0
                    Dim LocMsj As String = SP_ValidaGuardaOrdSerAparatos(CLng(Clv_OrdenTextBox.Text), opcion, StatusTextBox.Text, 0, Tecnico.SelectedValue)

                    If LocMsj.Length > 0 Then
                        MsgBox(LocMsj, MsgBoxStyle.Information, "Información")
                        Exit Sub
                    End If

                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

                Try
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoTextBox.Text)
                    BaseII.CreateMyParameter("@clv_orden", SqlDbType.BigInt, Clv_OrdenTextBox.Text)
                    BaseII.CreateMyParameter("@tieneCoordenadas", ParameterDirection.Output, SqlDbType.Int)
                    BaseII.ProcedimientoOutPut("sp_verificaCoordenadas")
                    If BaseII.dicoPar("@tieneCoordenadas") = 0 Then
                        MsgBox("No se asignaron las coordenadas, no se puede ejecutar la orden.")
                        Exit Sub
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

                Try
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@clv_orden", SqlDbType.BigInt, Clv_OrdenTextBox.Text)
                    BaseII.CreateMyParameter("@tieneBimProveedor", ParameterDirection.Output, SqlDbType.Int)
                    BaseII.ProcedimientoOutPut("sp_verificaBimProveedor")
                    If BaseII.dicoPar("@tieneBimProveedor") = 0 Then
                        MsgBox("No se asigno proveedor, no se puede ejecutar la orden.")
                        Exit Sub
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                'Que siempre no vamos a validar, se va a poder ejecutar sin capturar los datos
                'Try
                '    BaseII.limpiaParametros()
                '    BaseII.CreateMyParameter("@contrato", SqlDbType.Int, ContratoTextBox.Text)
                '    BaseII.CreateMyParameter("@clv_orden", SqlDbType.BigInt, Clv_OrdenTextBox.Text)
                '    BaseII.CreateMyParameter("@tieneInformacionZonaNorte", ParameterDirection.Output, SqlDbType.Int)
                '    BaseII.ProcedimientoOutPut("VerificaInformacionZonaNorte")
                '    If BaseII.dicoPar("@tieneInformacionZonaNorte") = 0 Then
                '        MsgBox("No se asignó la información de instalación de Cobertura de Zona Norte. No se puede ejecutar la orden.")
                '        Exit Sub
                '    End If
                'Catch ex As Exception
                '    MsgBox(ex.Message)
                'End Try

            End If

            Dim CON As New SqlConnection(MiConexion)
            Dim error2 As Integer = Nothing
            'CON.Open()

            If BUSCADetOrdSerDataGridView.RowCount = 0 Then
                MessageBox.Show("No hay conceptos en el detalle de la orden.")
                Exit Sub
            End If

            'If Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value ), 1, 5) = "CAPAR" Then
            '    If GloGuardo = False Then
            '        MsgBox("Seleccione el nuevo aparato digital", MsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If


            If eResAco = 1 And opcion <> "N" And IdSistema = "AG" Then
                If Me.TextBoxTab.Text.Length = 0 Then
                    'MsgBox("Se requiere capturar la Tab.", MsgBoxStyle.Information)
                    'Exit Sub
                    Me.TextBoxTab.Text = ""
                End If
                If ComboBoxTap.Visible Then
                    NueRelClienteTab(Me.ContratoTextBox.Text, Me.ComboBoxTap.Text, "Tap", ComboBoxTap.SelectedValue)
                End If
                If ComboBoxNap.Visible Then
                    NueRelClienteTab(Me.ContratoTextBox.Text, Me.ComboBoxNap.Text, "Nap", ComboBoxNap.SelectedValue)
                End If
            End If

            If opcion <> "N" Then
                If Me.RadioButton3.Checked = True Then
                    'CON.Open()
                    If Me.Visita1TextBox.Text.Trim.Length > 0 Then
                        Dime_Si_Graba()
                    ElseIf Me.Visita2TextBox.Text.Trim.Length > 0 Then
                        'Me.DimesigrabaordTableAdapter.Connection = CON
                        'Me.DimesigrabaordTableAdapter.Fill(Me.DataSetLidia.dimesigrabaord, gloClave, 0, Me.Visita2TextBox.Text)
                        Dime_Si_Graba()
                    End If
                    ' CON.Close()
                Else
                    'CON.Open()
                    'Me.DimesigrabaordTableAdapter.Connection = CON
                    'Me.DimesigrabaordTableAdapter.Fill(Me.DataSetLidia.dimesigrabaord, gloClave, 0, Me.Fec_EjeTextBox.Text)
                    Dime_Si_Graba()
                    ' CON.Close()
                End If
            End If
            If dime = "2" Or dime = "0" Or opcion = "N" Then
                Try

                    ValidaOrdSerManuales(CLng(Me.Clv_OrdenTextBox.Text))
                    If eRes = 1 Then
                        MsgBox(eMsg, MsgBoxStyle.Information)
                        eRes = 0
                        eMsg = String.Empty
                        Exit Sub
                    End If

                    If IsNumeric(Me.ContratoTextBox.Text) = True Then
                        'STORED PROCEDURE Q VERIFICA SI TIENE MAS SERVICIOS EL CLIENTE
                        CON.Open()
                        Me.Valida_DetOrdenTableAdapter.Connection = CON
                        Me.Valida_DetOrdenTableAdapter.Fill(Me.NewSofTvDataSet.Valida_DetOrden, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
                        CON.Close()
                        If Me.ValidacionTextBox.Text = 0 Then
                            MsgBox("Se Requiere tener datos en el Detalle de la Orden")
                            Exit Sub
                        End If

                        error2 = Checa_si_tiene_camdo(CLng(Me.Clv_OrdenTextBox.Text))

                        If error2 > 0 Then
                            MsgBox("Se Requiere Que Capture El Nuevo Domicilio", MsgBoxStyle.Information)
                            Exit Sub
                        End If


                        'AQUI VA TU CODIGO ERIC 2 DE ABRIL DE 2008
                        'If Me.StatusTextBox.Text = "P" And GLOTRABAJO = "CCABM" Then
                        ' If Len(Trim(Tecnico.Text)) = 0 Or IsNumeric(Tecnico.SelectedValue) = False Then
                        'MsgBox("Se requiere que Seleccione el Técnico por favor", MsgBoxStyle.Information)
                        'Exit Sub
                        'End If
                        If Me.StatusTextBox.Text <> "V" Then
                            Me.valida()
                            If Me.RadioButton1.Checked = False Then
                                If Me.CONTADORTextBox.Text = 1 Then
                                    'If GloClv_TipSer = 2 Then
                                    MsgBox("Se Requiere que Asigne el Cablemodem", MsgBoxStyle.Information)
                                    Exit Sub
                                ElseIf Me.CONTADORTextBox.Text = 2 Then
                                    'ElseIf GloClv_TipSer = 3 Then
                                    MsgBox("Se Requiere que Asigne el Aparato ", MsgBoxStyle.Information)
                                    Exit Sub
                                    'End If
                                ElseIf Me.CONTADORTextBox.Text = 3 Then
                                    MsgBox("Se Requiere Que Asigne el ATA", MsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                        End If
                        If Me.StatusTextBox.Text = "E" Then
                            SP_LLena_Bitacora_Ordenes(GloUsuario, "Se Ejecuto el ", gloClave)
                            Dim Pasa As Integer = 0
                            If IsDate(Mid(Me.Fec_EjeTextBox.Text, 1, 10)) = True Then
                                Dim Fecha As Date = FormatDateTime(Mid(Me.Fec_EjeTextBox.Text, 1, 10), DateFormat.ShortDate)
                                If DateValue(Fecha) >= DateValue(FormatDateTime(Me.Fecha_SoliciutudMaskedTextBox.Value, DateFormat.ShortDate)) And DateValue(Fecha) <= DateValue(FormatDateTime(Me.FechaDateTimePicker.Value, DateFormat.ShortDate)) Then
                                    Pasa = 1
                                Else
                                    MsgBox("La Fecha de la Ejecución no puede ser Menor a la Fecha de Solicitud ni Mayor a la Fecha Actual ", MsgBoxStyle.Information)
                                    Me.Fec_EjeTextBox.Clear()
                                    Exit Sub
                                End If
                            Else
                                MsgBox("La Fecha de Ejecución es Invalida")
                                Me.Fec_EjeTextBox.Clear()
                                Exit Sub
                            End If
                            If IsDate(Me.Fec_EjeTextBox.Text) = False Then
                                MsgBox("Se Requiere que Capture la Fecha y la Hora de Ejecución de Forma Correcta por Favor", MsgBoxStyle.Information)
                                Exit Sub
                            End If
                            If Len(Trim(Tecnico.Text)) = 0 Or IsNumeric(Tecnico.SelectedValue) = False Then
                                MsgBox("Se requiere que Seleccione el Técnico por favor", MsgBoxStyle.Information)
                                Exit Sub
                            End If


                            BorraDetalleOrden_DetalleGrid()
                            'SI SE TRATA DE RETIRO DE EQUIPO, PREGUNTA EL NUMERO DE APARATOS POR ENTREGAR
                            'DimeQueAparatosEntrega(gloClave)

                            'If eBndDig = True Or eBndNet = True Then
                            'FrmEntregaAparato.Show()
                            '    Exit Sub
                            'End If
                            '-----------------------------------------------------------------------------

                        ElseIf Me.StatusTextBox.Text = "V" Then
                            'Visita1
                            SP_LLena_Bitacora_Ordenes(GloUsuario, "Se puso en Visita la ", gloClave)
                            Dim Pasa As Integer = 0
                            If IsDate(Mid(Me.Visita1TextBox.Text, 1, 10)) = True Then
                                Dim Fecha As Date = Mid(Me.Visita1TextBox.Text, 1, 10)
                                If DateValue(Fecha) >= DateValue(Me.Fecha_SoliciutudMaskedTextBox.Value) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                                    Pasa = 1
                                Else
                                    MsgBox("La Fecha de la Visita no puede ser Menor a la Fecha de Solicitud ni Mayor a la Fecha Actual ", MsgBoxStyle.Information)
                                    Me.Visita1TextBox.Clear()
                                    Exit Sub
                                End If
                            Else
                                MsgBox("La Fecha de Ejecución es Invalida")
                                Me.Visita1TextBox.Clear()
                                Exit Sub
                            End If
                            If IsDate(Me.Visita1TextBox.Text) = False Then
                                MsgBox("Se requiere que capture la fecha y la hora de Visita de forma correcta por favor", MsgBoxStyle.Information)
                                Exit Sub
                            End If
                            'Visita1                    
                            'Visita2
                            If IsDate(Mid(Me.Visita2TextBox.Text, 1, 10)) = True Then
                                Dim Fecha As Date = Mid(Me.Visita2TextBox.Text, 1, 10)
                                If DateValue(Fecha) >= DateValue(Me.Fecha_SoliciutudMaskedTextBox.Value) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                                    Pasa = 1
                                Else
                                    MsgBox("La Fecha de la Visita no puede ser Menor a la Fecha de Solicitud ni mayor a la Fecha Actual ", MsgBoxStyle.Information)
                                    Me.Visita2TextBox.Clear()
                                    Exit Sub
                                End If
                            Else
                                'MsgBox("La Fecha de Ejecución es Invalida")
                                Me.Visita2TextBox.Clear()
                            End If
                            If IsDate(Me.Visita2TextBox.Text) = False Then
                                'MsgBox("Se requiere que capture la fecha y la hora de Visita de forma correcta por favor", MsgBoxStyle.Information)
                                Me.Visita2TextBox.Clear()
                            End If
                            'Visita1                    
                        End If


                        If LocValida1 = True Then
                            CON.Open()
                            Me.Dame_FolioTableAdapter.Connection = CON
                            Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, gloClave, 1, Locclv_folio)
                            Me.Inserta_Bitacora_tecTableAdapter.Connection = CON
                            Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, clv_sessionTecnico, CLng(gloClave), Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
                            Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Connection = CON
                            Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Orden, LocNo_Bitacora, CLng(gloClave))
                            Me.Inserta_RelCobraDescTableAdapter.Connection = CON
                            Me.Inserta_RelCobraDescTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_RelCobraDesc, LocNo_Bitacora, "O")
                            CON.Close()
                            LocValida1 = False
                        End If

                        If bndCCABM = True Then
                            bndCCABM = False
                            CON.Open()
                            Me.Cambia_Tipo_cablemodemTableAdapter.Connection = CON
                            Me.Cambia_Tipo_cablemodemTableAdapter.Fill(Me.ProcedimientosArnoldo2.Cambia_Tipo_cablemodem, CLng(Me.Clv_OrdenTextBox.Text), LoctipoCablemdm)
                            Me.Guarda_Comentario_CCABMTableAdapter.Connection = CON
                            Me.Guarda_Comentario_CCABMTableAdapter.Fill(Me.ProcedimientosArnoldo2.Guarda_Comentario_CCABM, CLng(Me.Clv_OrdenTextBox.Text), LoctipoCablemdm)
                            CON.Close()
                        End If

                        'Eric
                        eRes = 0
                        eMsg = ""
                        CON.Open()
                        Me.ChecaMotivoCanServTableAdapter.Connection = CON
                        Me.ChecaMotivoCanServTableAdapter.Fill(Me.DataSetEric.ChecaMotivoCanServ, Me.Clv_OrdenTextBox.Text, eRes, eMsg)
                        CON.Close()
                        If eRes = 1 Then

                            'If opcion = "M" And GloClv_TipSer = 1 Then
                            '    GloClv_MotCan = 0
                            '    FrmMotCan.Show()
                            '    Exit Sub
                            'End If

                            'If (opcion = "N" And GloClv_TipSer = 2) Or (opcion = "N" And GloClv_TipSer = 3) Then
                            '    GloClv_MotCan = 0
                            '    FrmMotCan.Show()
                            '    Exit Sub
                            'End If

                            If (opcion = "N") Then
                                GloClv_MotCan = 0
                                FrmMotCan.Show()
                                Exit Sub
                            End If


                        End If
                        '---------------------------------------

                        ValidaCajasAsignadas()
                        If Bnd_Orden = True And Me.RadioButton3.Checked = False Then
                            MsgBox("No Se Puede Ejecutar La Orden Hasta No Ser Asignados Todos Los Aparatos")
                            Bnd_Orden = False
                            Exit Sub
                        End If

                        'Eric
                        If (eResAco = 1 And opcion <> "N" And IdSistema = "TO") Or (eResAco = 1 And opcion <> "N" And IdSistema = "SA") Or (eResAco = 1 And opcion <> "N" And IdSistema = "VA") Or (eResAco = 1 And opcion <> "N" And IdSistema = "AG") Then
                            'If Me.ComboBoxTap.SelectedValue = 0 Then
                            '    MsgBox("¡Capture el Tap al que pertenecerá el Cliente!", MsgBoxStyle.Information)
                            '    Exit Sub
                            'End If

                            'If Me.PlacaTextBox.Text.Length > 0 Then
                            'CON.Open()
                            'Me.ConRelCtePlacaTableAdapter.Connection = CON
                            'Me.ConRelCtePlacaTableAdapter.Insert(Me.ContratoTextBox.Text, Me.PlacaTextBox.Text, Me.Clv_OrdenTextBox.Text)
                            'CON.Close()
                            'ALMACENA LA PLACA Y TAP DEL CLIENTE (INCIO) *JUANJO*
                            uspGuardaPlacaTapCliente(CLng(Me.ContratoTextBox.Text), CInt(Me.ComboBoxTap.SelectedValue), Me.PlacaTextBox.Text, CLng(Me.Clv_OrdenTextBox.Text))
                            ModificaSectorCliente()
                            'ALMACENA LA PLACA Y TAP DEL CLIENTE (FIN) *JUANJO*
                            GuardaRelOrdenUsuario()
                            'CON.Open()
                            Me.Validate()
                            Me.CONORDSERBindingSource.EndEdit()



                            ModOrdSer(Clv_OrdenTextBox.Text, 0, ContratoTextBox.Text, Fecha_SoliciutudMaskedTextBox.Value, Fec_EjeTextBox1.Text, Visita1TextBox1.Text, Visita2TextBox1.Text, StatusTextBox.Text, Tecnico.SelectedValue, IMPRESACheckBox.Checked, 0, ObsTextBox.Text, "")
                            PreEjecutaOrdSer(Clv_OrdenTextBox.Text)
                            SP_GuardaOrdSerAparatos(Clv_OrdenTextBox.Text, opcion, StatusTextBox.Text, 0)

                            'Cuadrilla-----------------------------------------------------------------
                            If cbCuadrilla.SelectedValue > 0 Then
                                NueRelOrdSerCuadrilla(Clv_OrdenTextBox.Text, cbCuadrilla.SelectedValue)
                            End If
                            '--------------------------------------------------------------------------

                            'Mostrar ip dada por el provisonamiento
                            If StatusTextBox.Text = "E" And RequeireEsperarIp() Then
                                Dim frm1 As New FrmMostrarIpProvisionamientoOrden
                                frm1.ContratoIpOrden = ContratoTextBox.Text
                                frm1.ShowDialog()
                            End If

                            'Vamos a validar que si es de instalación haya hecho las cosas bien, de otro modo 
                            BaseII.limpiaParametros()
                            BaseII.CreateMyParameter("@clv_orden", SqlDbType.Int, Clv_OrdenTextBox.Text)
                            BaseII.CreateMyParameter("@muestraMensaje", ParameterDirection.Output, SqlDbType.Int)
                            BaseII.ProcedimientoOutPut("ValidaInstalacionCorrecta")
                            If BaseII.dicoPar("@muestraMensaje") = 1 Then
                                MsgBox("Se produjo un error en el proceso de ejecución. Vuelva a repetir el proceso.")
                            End If
                            'Me.CONORDSERTableAdapter.Connection = CON
                            'Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)
                            'Me.PREEJECUTAOrdSerTableAdapter.Connection = CON
                            'Me.PREEJECUTAOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.PREEJECUTAOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
                            'CON.Close()
                            MsgBox(mensaje5)
                            GloBnd = True
                            GloGuardo = False
                            If opcion = "N" And IdSistema <> "VA" Then
                                SP_LLena_Bitacora_Ordenes(GloUsuario, "Se Genero la ", gloClave)
                                CON.Open()
                                Me.Imprime_OrdenTableAdapter.Connection = CON
                                Me.Imprime_OrdenTableAdapter.Fill(Me.ProcedimientosArnoldo2.Imprime_Orden, Me.Clv_OrdenTextBox.Text, Imprime)
                                CON.Close()
                                If Imprime = 0 Then
                                    ConfigureCrystalReports(0, "")
                                ElseIf Imprime = 1 Then
                                    MsgBox("La orden es de proceso Automático No se Imprimio", MsgBoxStyle.Information)
                                End If
                            End If

                            Me.Close()
                            'Else
                            '    MsgBox("Captura # de Placa.", , "Atención")
                            'End If

                        Else
                            GuardaRelOrdenUsuario()
                            'CON.Open()
                            Me.Validate()
                            Me.CONORDSERBindingSource.EndEdit()


                            ModOrdSer(Clv_OrdenTextBox.Text, 0, ContratoTextBox.Text, Fecha_SoliciutudMaskedTextBox.Value, Fec_EjeTextBox1.Text, Visita1TextBox1.Text, Visita2TextBox1.Text, StatusTextBox.Text, Tecnico.SelectedValue, IMPRESACheckBox.Checked, 0, ObsTextBox.Text, "")
                            PreEjecutaOrdSer(Clv_OrdenTextBox.Text)
                            SP_GuardaOrdSerAparatos(Clv_OrdenTextBox.Text, opcion, StatusTextBox.Text, 0)

                            'Cuadrilla-----------------------------------------------------------------
                            If cbCuadrilla.SelectedValue > 0 Then
                                NueRelOrdSerCuadrilla(Clv_OrdenTextBox.Text, cbCuadrilla.SelectedValue)
                            End If
                            '--------------------------------------------------------------------------

                            ''Mostrar ip dada por el provisonamiento
                            'If StatusTextBox.Text = "E" And RequeireEsperarIp() Then
                            '    Dim frm1 As New FrmMostrarIpProvisionamientoOrden
                            '    frm1.ContratoIpOrden = ContratoTextBox.Text
                            '    frm1.ShowDialog()                               

                            'End If

                            'Me.CONORDSERTableAdapter.Connection = CON
                            'Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)
                            'Me.PREEJECUTAOrdSerTableAdapter.Connection = CON
                            'Me.PREEJECUTAOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.PREEJECUTAOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
                            'CON.Close()
                            MsgBox(mensaje5)

                            'Eric----------------------------------------------
                            Dim CONERIC As New SqlConnection(MiConexion)
                            Dim eRes As Long = 0
                            Dim eMsg As String = Nothing


                            CONERIC.Open()
                            Me.ChecaOrdSerRetiroTableAdapter.Connection = CONERIC
                            Me.ChecaOrdSerRetiroTableAdapter.Fill(Me.DataSetEric.ChecaOrdSerRetiro, CType(Me.Clv_OrdenTextBox.Text, Long), eRes, eMsg)
                            CONERIC.Close()
                            If (eRes > 0 And IdSistema = "SA") Or (eRes > 0 And IdSistema = "VA") Then
                                ImprimeOrdSerRetiro(eRes)
                            End If

                            '------------------------------------------------------

                            GloBnd = True
                            GloGuardo = False
                            If opcion = "N" Then
                                SP_LLena_Bitacora_Ordenes(GloUsuario, "Se Genero la ", gloClave)
                                CON.Open()
                                Me.Imprime_OrdenTableAdapter.Connection = CON
                                Me.Imprime_OrdenTableAdapter.Fill(Me.ProcedimientosArnoldo2.Imprime_Orden, Me.Clv_OrdenTextBox.Text, Imprime)
                                CON.Close()
                                If Imprime = 0 And IdSistema <> "VA" Then
                                    ConfigureCrystalReports(0, "")
                                ElseIf Imprime = 1 Then
                                    MsgBox("La orden es de proceso Automático No se Imprimio", MsgBoxStyle.Information)
                                End If
                            End If
                            Me.Close()
                        End If



                    Else
                        MsgBox(mensaje7)
                    End If

                    'CON.Close()
                Catch ex As System.Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try
            Else
                MsgBox("No Se Puede Grabar,la Fecha de Ejecución No puede ser de Meses Anteriores ", MsgBoxStyle.Information)
                Me.TextBox4.Clear()
            End If
        ElseIf pasa = 0 Then
            MsgBox("El cliente no tiene contratado el servicio, seleccione otro tipo por favor", MsgBoxStyle.Information, "Tipo Servicio")
        End If
    End Sub

    Private Sub ModificaSectorCliente()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_sector", SqlDbType.Int, ComboBoxSector.SelectedValue)
            BaseII.CreateMyParameter("@clv_orden", SqlDbType.BigInt, Clv_OrdenTextBox.Text)
            BaseII.Inserta("ModificaSectorOrden")
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try


            If dameStatusOrdenQueja(CInt(Me.Clv_OrdenTextBox.Text), "O") = "P" And opcion = "M" Then
                Dim res = MsgBox("¿Deseas salir sin guardar la Descarga de Material?", MsgBoxStyle.YesNo)
                If res = MsgBoxResult.Yes Then
                    softv_BorraDescarga(CInt(Me.Clv_OrdenTextBox.Text), "O")
                Else
                    Exit Sub
                End If
            End If

            If opcion = "N" Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.Grabar_det_ordenTableAdapter.Connection = CON
                Me.Grabar_det_ordenTableAdapter.Fill(Me.DataSetarnoldo.grabar_det_orden, CInt(Me.Clv_OrdenTextBox.Text))
                CON.Close()
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, Me.ContratoTextBox.Text)
            BaseII.CreateMyParameter("@clv_orden", SqlDbType.Int, Clv_OrdenTextBox.Text)
            BaseII.Inserta("BorraInformacionZonaNorte")
            Me.Close()
        Catch ex As Exception

        End Try
    End Sub



    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.RadioButton2.Checked = True Then
            Me.StatusTextBox.Text = "E"
            GloControlaReloj = 1
            Me.Fec_EjeTextBox.Enabled = True
            Me.Visita1TextBox.Enabled = False
            Me.Visita2TextBox.Enabled = False
            Me.Fec_EjeTextBox.Focus()
            ValidaDetalleOrden()
            '           Me.TextBox1.Visible = False
            ''Me.Label4.Visible = True
            ''Me.Label4.Text = "Ejecución : "
        End If

        'If Me.StatusTextBox.Text <> "E" Then
        '    Me.StatusTextBox.Text = "E"
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '    Me.Label4.Visible = True
        '    Me.Label4.Text = "Ejecución : "
        '    If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
        '        Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
        '        Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
        '        Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
        '    End If
        'ElseIf Me.StatusTextBox.Text = "E" Then
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '    Me.Label4.Visible = True
        '    Me.Label4.Text = "Ejecución : "
        'End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If Me.RadioButton3.Checked = True Then
            GloControlaReloj = 0
            Me.Panel5.BackColor = Color.WhiteSmoke
            Me.Panel6.BackColor = Color.WhiteSmoke
            Me.StatusTextBox.Text = "V"
            '    Me.TextBox1.Visible = False
            Me.Fec_EjeTextBox.Enabled = False
            Me.Visita1TextBox.Enabled = True
            Me.Visita2TextBox.Enabled = True
            Me.Visita1TextBox.Focus()
            ''Me.Label4.Visible = True
            ''Me.Label4.Text = "Visita : "
        End If
        'Me.StatusTextBox.Text = "V"
        'If Me.StatusTextBox.Text <> "V" Then
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '    Me.Label4.Visible = True
        '    Me.StatusTextBox.Text = "V"
        '    Me.Label4.Text = "Visita : "
        '    If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
        '        Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
        '        Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
        '        Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
        '    End If
        'ElseIf Me.StatusTextBox.Text = "V" Then
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '   
        '    Me.Label4.Visible = True
        '    Me.Label4.Text = "Visita : "
        'End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GLOCONTRATOSEL = 0
        'op = 3
        GloClv_TipSer = 0
        FrmSelCliente.Show()
    End Sub

    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            Dim CON As New SqlConnection(MiConexion)


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0


            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            'If IdSistema = "AG" Then

            reportPath = RutaReportes + "\ReporteOrdenes.rpt"
            'ElseIf IdSistema = "TO" Then
            '    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            'ElseIf IdSistema = "SA" Then
            '    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            'ElseIf IdSistema = "VA" Then
            '    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            'ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
            '    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            'End If




            'If IdSistema <> "AG" Then
            '    customersByCityReport.Load(reportPath)
            '    SetDBLogonForReport(connectionInfo, customersByCityReport)

            '    '@Clv_TipSer int
            '    customersByCityReport.SetParameterValue(0, 0)
            '    ',@op1 smallint
            '    customersByCityReport.SetParameterValue(1, 1)
            '    ',@op2 smallint
            '    customersByCityReport.SetParameterValue(2, 0)
            '    ',@op3 smallint
            '    customersByCityReport.SetParameterValue(3, 0)
            '    ',@op4 smallint,
            '    customersByCityReport.SetParameterValue(4, 0)
            '    '@op5 smallint
            '    customersByCityReport.SetParameterValue(5, 0)
            '    ',@StatusPen bit
            '    customersByCityReport.SetParameterValue(6, 0)
            '    ',@StatusEje bit
            '    customersByCityReport.SetParameterValue(7, 0)
            '    ',@StatusVis bit,
            '    customersByCityReport.SetParameterValue(8, 0)
            '    '@Clv_OrdenIni bigint
            '    customersByCityReport.SetParameterValue(9, CLng(Me.Clv_OrdenTextBox.Text))
            '    ',@Clv_OrdenFin bigint
            '    customersByCityReport.SetParameterValue(10, CLng(Me.Clv_OrdenTextBox.Text))
            '    ',@Fec1Ini Datetime
            '    customersByCityReport.SetParameterValue(11, "01/01/1900")
            '    ',@Fec1Fin Datetime,
            '    customersByCityReport.SetParameterValue(12, "01/01/1900")
            '    '@Fec2Ini Datetime
            '    customersByCityReport.SetParameterValue(13, "01/01/1900")
            '    ',@Fec2Fin Datetime
            '    customersByCityReport.SetParameterValue(14, "01/01/1900")
            '    ',@Clv_Trabajo int
            '    customersByCityReport.SetParameterValue(15, 0)
            '    ',@Clv_Colonia int
            '    customersByCityReport.SetParameterValue(16, 0)
            '    ',@OpOrden int
            '    customersByCityReport.SetParameterValue(17, OpOrdenar)
            'Else
            Dim listatablas As New List(Of String)
            Dim DS As New DataSet
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CObj(0))
            BaseII.CreateMyParameter("@op1", SqlDbType.SmallInt, CShort(1))
            BaseII.CreateMyParameter("@op2", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op3", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op4", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op5", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.BigInt, CLng(Me.Clv_OrdenTextBox.Text))
            BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.BigInt, CLng(Me.Clv_OrdenTextBox.Text))
            BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, CLng(OpOrdenar))
            BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, CLng(GloClvUsuario))
            'listatablas.Add("ReporteOrdSer")
            'listatablas.Add("Comentarios_DetalleOrden")
            'listatablas.Add("DameDatosGenerales_2")
            'listatablas.Add("DetOrdSer")
            'listatablas.Add("Trabajos")
            'listatablas.Add("ClientesConElMismoPoste")
            'DS = BaseII.ConsultaDS("ReporteOrdSer", listatablas)
            listatablas.Add("ReporteOrdSer;1")
            listatablas.Add("Comentarios_DetalleOrden")
            listatablas.Add("DameDatosGenerales_2;1")
            listatablas.Add("DetOrdSer")
            listatablas.Add("Trabajos")
            listatablas.Add("ClientesConElMismoPoste")
            DS = BaseII.ConsultaDS("ReporteOrdSer", listatablas)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(DS)

            mySelectFormula = "Orden " & GloNom_TipSer
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"

            CON.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora, a)
            CON.Close()

            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora

                Dim ps As System.Drawing.Printing.PrinterSettings
                ps = New Printing.PrinterSettings
                ps.Copies = 1
                ps.Duplex = Printing.Duplex.Default
                ps.Collate = False

                Dim pageSet As System.Drawing.Printing.PageSettings
                pageSet = New Printing.PageSettings

                customersByCityReport.PrintToPrinter(ps, pageSet, False)


                'customersByCityReport.PrintToPrinter(1, True, 0, 0)
            End If

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.Clv_TecnicoTextBox.Text = Me.ComboBox1.SelectedValue
    End Sub

    Private Sub ContratoTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContratoTextBox.GotFocus
        Me.ContratoTextBox.SelectionStart = 0
        Me.ContratoTextBox.SelectionLength = Len(Me.ContratoTextBox.Text)
    End Sub

    Private Sub ContratoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ContratoTextBox.KeyPress
        e.KeyChar = Chr(ValidaKey(ContratoTextBox, Asc(LCase(e.KeyChar)), "N"))
    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            GloContratoord = CLng(Me.ContratoTextBox.Text)
            GloContrato = CLng(Me.ContratoTextBox.Text)
        End If
        Me.BUSCACLIENTES(0)

    End Sub

    Private Sub Clv_OrdenTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_OrdenTextBox.TextChanged
        gloClave = Me.Clv_OrdenTextBox.Text
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = True Then
            dimebitacora()
        End If
    End Sub


    Private Sub StatusTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusTextBox.TextChanged
        If Me.StatusTextBox.Text = "P" Then
            If Me.RadioButton1.Checked = False Then Me.RadioButton1.Checked = True
        ElseIf Me.StatusTextBox.Text = "E" Then
            If Me.RadioButton2.Checked = False Then
                Me.RadioButton2.Checked = True
                Me.TextBox1.Visible = False
                'Me.Fecha_EjecucionMaskedTextBox.Visible = True
                ''Me.Label4.Visible = True
                ''Me.Label4.Text = "Ejecución : "
            End If
        ElseIf Me.StatusTextBox.Text = "V" Then
            If Me.RadioButton3.Checked = False Then
                Me.RadioButton3.Checked = True
                Me.TextBox1.Visible = False

                'Me.Fecha_EjecucionMaskedTextBox.Visible = True
                '' Me.Label4.Visible = True
                '' Me.Label4.Text = "Visita : "
            End If

        End If
    End Sub



    Private Sub Fecha_SoliciutudMaskedTextBox_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_SoliciutudMaskedTextBox.ValueChanged
        If Me.StatusTextBox.Text = "P" Then
            'If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
            ' Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
            ' Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
            ' Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
            'End If
        End If
    End Sub

    Private Sub ComboBox5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        'If Me.ComboBox5.SelectedValue <> Nothing Then
        '    GloClv_TipSer = Me.ComboBox5.SelectedValue
        '    Me.TextBox2.Text = Me.ComboBox5.Text
        '    GloNom_TipSer = Me.ComboBox5.Text
        'End If
    End Sub


    Private Sub Fec_EjeTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fec_EjeTextBox1.TextChanged
        Me.Fec_EjeTextBox.Text = Me.Fec_EjeTextBox1.Text
    End Sub

    Private Sub Visita1TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visita1TextBox1.TextChanged
        Me.Visita1TextBox.Text = Me.Visita1TextBox1.Text
    End Sub

    Private Sub Visita2TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visita2TextBox1.TextChanged
        Me.Visita2TextBox.Text = Me.Visita2TextBox1.Text
    End Sub

    Private Sub Fec_EjeTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Fec_EjeTextBox.GotFocus
        Fec_EjeTextBox.SelectionStart = 0
        Fec_EjeTextBox.SelectionLength = Len(Fec_EjeTextBox.Text)
    End Sub



    Private Sub Fec_EjeTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Fec_EjeTextBox.TextChanged
        If IsDate(Mid(Me.Fec_EjeTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Fec_EjeTextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Fec_EjeTextBox.Clear()
            End If
        End If
        If IsDate(Me.Fec_EjeTextBox.Text) = True Then
            Me.Fec_EjeTextBox1.Text = Me.Fec_EjeTextBox.Text
            Panel5.BackColor = Color.WhiteSmoke
            If Len(Trim(Me.Fec_EjeTextBox1.Text)) = 10 Then
                LocFecEje = True
            End If
        Else
            LocFecEje = False
        End If
    End Sub

    Private Sub Visita1TextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita1TextBox.GotFocus
        Visita1TextBox.SelectionStart = 0
        Visita1TextBox.SelectionLength = Len(Visita1TextBox.Text)
    End Sub


    Private Sub Visita1TextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita1TextBox.TextChanged
        If IsDate(Mid(Me.Visita1TextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Visita1TextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Visita1TextBox.Clear()
            End If
        End If
        If IsDate(Me.Visita1TextBox.Text) = True Then
            Me.Visita1TextBox1.Text = Me.Visita1TextBox.Text
        End If
    End Sub

    Private Sub Visita2TextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita2TextBox.GotFocus
        Visita2TextBox.SelectionStart = 0
        Visita2TextBox.SelectionLength = Len(Visita2TextBox.Text)
    End Sub


    Private Sub Visita2TextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita2TextBox.TextChanged
        If IsDate(Mid(Me.Visita2TextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Visita2TextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Visita2TextBox.Clear()
            End If
        End If
        If IsDate(Me.Visita2TextBox.Text) = True Then
            Me.Visita2TextBox1.Text = Me.Visita2TextBox.Text
        End If
    End Sub

    Private Sub Button9_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click

        Dim clavedetordSer As Long = 0
        If Me.StatusTextBox.Text = "P" Then
            If IsNumeric(Me.ContratoTextBox.Text) = False Then
                MsgBox(mensaje7)
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Validate()
            Me.CONORDSERBindingSource.EndEdit()
            Me.CONORDSERTableAdapter.Connection = CON
            Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)

            'Este no Me.CONDetOrdSerTableAdapter.Insert(Me.Clv_OrdenTextBox.Text, Me.Clv_TrabajoTextBox.Text, Me.ObsTextBox1.Text, Me.SeRealizaCheckBox.Checked, clavedetordSer)
            gloClv_Orden = Me.Clv_OrdenTextBox.Text
            Contrato = Me.ContratoTextBox.Text
            CON.Close()
            FrmDetOrSer.Show()
            ValidaDetalleOrden()

        Else
            MsgBox("Solo se puede agregar Servicios al Cliente cuando esta con Status de Pendiente", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'BUSCADetOrdSerDataGridView.SelectedRows(0).Cells("ContratoCompania").Value.ToString()
        'If IsNumeric(BUSCADetOrdSerDataGridView.Item(0, BUSCADetOrdSerDataGridView.SelectedRows.Item).Value) = True Then
        '    Me.ClaveTextBox.Text = BUSCADetOrdSerDataGridView.Item(0, e.RowIndex).Value
        'End If
        If opcion = "N" Then
            If IsNumeric(Me.ClaveTextBox.Text) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.BorraMotivoCanServTableAdapter.Connection = CON
                Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, Me.Clv_OrdenTextBox.Text, 0, Me.ClaveTextBox.Text, 0, 2)
                Me.CONDetOrdSerTableAdapter.Connection = CON
                Me.CONDetOrdSerTableAdapter.Delete(Me.ClaveTextBox.Text)
                Me.BUSCADetOrdSerTableAdapter.Connection = CON
                Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))

                CON.Close()
                ValidaDetalleOrden()
                Me.ClaveTextBox.Text = 0

            End If
        ElseIf opcion = "M" Then

            Dim Fila As Integer = 0
            Fila = BUSCADetOrdSerDataGridView.CurrentRow.Index
            GloDetClave = BUSCADetOrdSerDataGridView.Item(0, Fila).Value
            Me.ClaveTextBox.Text = GloDetClave
            Contrato = Me.ContratoTextBox.Text
            gloClv_Orden = Me.Clv_OrdenTextBox.Text
            GLOTRABAJO = RTrim(LTrim(Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 6)))
            GLONOMTRABAJO = BUSCADetOrdSerDataGridView.Item(3, Fila).Value
            dame_clv_tipser(GLOTRABAJO)
            BUSCADetOrdSerDataGridView.Item(6, Fila).Value = 1


        End If
    End Sub
    Private Sub dame_clv_tipser(ByVal clv_txt As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        cmd = New SqlClient.SqlCommand()
        Try
            If GloClv_TipSer > 0 Then
                GloClv_TipSer = 0
            End If
            CON.Open()
            With cmd
                .CommandText = "Dame_tipo_servicio_trabajo"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON

                Dim prm As New SqlParameter("@clv_txt", SqlDbType.VarChar, 10)
                Dim prm1 As New SqlParameter("@clv_tipser", SqlDbType.Int)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Output

                prm.Value = clv_txt
                prm1.Value = 0

                .Parameters.Add(prm)
                .Parameters.Add(prm1)

                Dim i As Integer = cmd.ExecuteNonQuery()

                GloClv_TipSer = prm1.Value
            End With
            CON.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Actuliza_Detalle()
        Dim CON As New SqlConnection(MiConexion)
        Try

            CON.Open()
            Me.BUSCADetOrdSerTableAdapter.Connection = CON
            Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))

        Catch ex As Exception
        Finally
            CON.Close()
        End Try
    End Sub


    Private Sub BUSCADetOrdSerDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles BUSCADetOrdSerDataGridView.CellClick
        Try


            Dim Pasa As Integer = 0
            If GloControlaReloj = 0 Then
                Pasa = 0
            Else
                If LocTec = True And LocFecEje = True Then
                    Pasa = 0
                Else
                    Pasa = 1
                End If
            End If
            Pasa = 0
            Locclv_tec = Me.Tecnico.SelectedValue
            ''dame_clv_tipser(
            If opcion = "M" Then
                Button2.Enabled = False
            End If
            If Pasa = 0 Then

                If IsNumeric(BUSCADetOrdSerDataGridView.Item(0, e.RowIndex).Value) = True Then
                    GloDetClave = BUSCADetOrdSerDataGridView.Item(0, e.RowIndex).Value
                    Me.ClaveTextBox.Text = GloDetClave
                    Contrato = Me.ContratoTextBox.Text
                    gloClv_Orden = Me.Clv_OrdenTextBox.Text
                    GLOTRABAJO = RTrim(LTrim(Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 6)))
                    GLONOMTRABAJO = BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value
                    dame_clv_tipser(GLOTRABAJO)
                    If e.ColumnIndex <> 4 Then
                        If Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RANTE" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RCABL" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RCONT" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RAPAR" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RELNB" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RANTX" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RRADI" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RETCA" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RAPAG" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RROUT" Then
                            Button2.Enabled = True
                        End If
                        Exit Sub
                    End If
                    If e.ColumnIndex = 6 Then
                        If Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RANTE") = False And _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RCABL") = False And _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RCONT") = False And _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RAPAR") = False And _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RELNB") = False And _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RANTX") = False And _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RRADI") = False And _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RAPAR") = False And _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RAPAG") = False And _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RETCA") = False Then
                            BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value = 0
                        End If
                        Exit Sub
                    End If

                    If Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "CAMDO" Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "CADIG" Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "CANET" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmCAMDO.Show()
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "CONEX" Then

                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmCONEX.Show()
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "CEXTE" Then

                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmCEXTE.Show()
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "CANEX" Then

                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmCANEX.Show()
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "BCABM" Then

                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmRelCablemodemClientes.Show()
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "BAPAR" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmRelCablemodemClientesDigital.Show()
                        'CONTRATACIONES O RECONTRATACIONES NET
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "ICAB" Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RICAB" Then
                        If Me.StatusTextBox.Text = "P" Then
                            Me.BUSCADetOrdSerDataGridView.Enabled = False
                            'FrmRelCablemodemClientes.Show()
                            FrmICABMAsigna.Show()
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.BUSCADetOrdSerDataGridView.Enabled = False
                            'GloClv_TipSer = 2
                            FrmICABMAsigna.Show()
                        End If
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("ECABL") = True Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("ECONT") = True Then
                        Dim FrM1 As New FormIAPARATOS_SELECCION
                        FrM1.ShowDialog()
                        Actuliza_Detalle()
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RANTE" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RCABL" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RCONT" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RAPAR" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RELNB" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RANTX" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RAPAG" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RRADI" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RETCA" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RROUT" Then
                        Button2.Enabled = True
                        Dim FrM1 As New FormIAPARATOS_SELECCION
                        FrM1.ShowDialog()
                        Actuliza_Detalle()
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("CANTE") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("CALNB") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "CAPAR" Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("CANTX") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("CRADI") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("CROUT") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("CCABM") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("CAPAG") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("CAPAF") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "CAPRO" Then
                        'ANTENAS , LNB , TARJETAS 
                        If Me.StatusTextBox.Text = "E" Then
                            'Me.BUSCADetOrdSerDataGridView.Enabled = False
                            'GloClv_TipSer = 2
                            Dim FRM1 As New FormIAPARATOS
                            FRM1.ShowDialog()
                            Actuliza_Detalle()
                        End If
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("CAPSG") = True Then
                        'ANTENAS , LNB , TARJETAS 
                        If Me.StatusTextBox.Text = "E" Then
                            'Me.BUSCADetOrdSerDataGridView.Enabled = False
                            'GloClv_TipSer = 2
                            Dim FRM1 As New FormIAPARATOSDoble
                            FRM1.ShowDialog()
                            Actuliza_Detalle()
                        End If
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("IANTE") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("INLNB") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("IAPAR") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RIAPAR") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("IANTX") Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("IRADI") Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("IROUT") Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("ICABM") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("RIAPG") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("IREPE") = True Or _
                        Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("IAPAG") = True Then
                        'ANTENAS , LNB , TARJETAS 
                        If Me.StatusTextBox.Text = "E" Then
                            'Me.BUSCADetOrdSerDataGridView.Enabled = False
                            'GloClv_TipSer = 2
                            Dim FRM1 As New FormIAPARATOS
                            FRM1.ShowDialog()
                            'If GLOTRABAJO = "IANTX" And GloBndCoordenadas Then
                            '    Dim FRM2 As New FrmCoordenadas()
                            '    FRM2.ShowDialog()
                            'End If
                            Actuliza_Detalle()
                        End If
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("REVCO") = True Then
                        If Me.StatusTextBox.Text = "E" Then
                            Me.BUSCADetOrdSerDataGridView.Enabled = False
                            Dim FRMR As New FormREVCO
                            FRMR.ShowDialog()
                        End If
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 6) = "ICABMT" Then
                        If Me.StatusTextBox.Text = "P" Then
                            Me.BUSCADetOrdSerDataGridView.Enabled = False
                            'FrmRelCablemodemClientes.Show()
                            FrmICABMAsigna.Show()
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.BUSCADetOrdSerDataGridView.Enabled = False
                            'GloClv_TipSer = 2
                            FrmICABMAsigna.Show()
                        End If
                        'CONTRATACIONES O RECONTRATACIONES DIG
                        'ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value ), 1, 5) = "IAPAR" Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value ), 1, 5) = "RIAPA" Then
                        '    If Me.StatusTextBox.Text = "P" Then
                        '        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        '        'FrmRelCablemodemClientesDigital.Show()
                        '        FrmIAPARAsigna.Show()
                        '    ElseIf Me.StatusTextBox.Text = "E" Then
                        '        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        '        'GloClv_TipSer = 3
                        '        FrmIAPARAsigna.Show()
                        '    End If
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "ICAJA" Then
                        If opcion = "N" Then
                            MsgBox("Proceda a guardar la orden antes de asignar una Caja Digital", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        ContratoCajasDig = Me.ContratoTextBox.Text
                        'Dim RES = MsgBox("Primero debe seleccionar la marca de la Caja Digital", MsgBoxStyle.OkCancel)
                        'If RES = MsgBoxResult.Ok Then

                        If Me.StatusTextBox.Text = "P" Then
                            Me.BUSCADetOrdSerDataGridView.Enabled = False
                            OpICAJA = "E"
                            FrmSelCajasDisponiblesvb.Show()
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.BUSCADetOrdSerDataGridView.Enabled = False
                            OpICAJA = "P"
                            FrmSelCajasDisponiblesvb.Show()
                        End If
                        'Else
                        '    Exit Sub
                        'End If
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "CCAJA" Then
                        If Me.StatusTextBox.Text = "P" Then
                            MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.BUSCADetOrdSerDataGridView.Enabled = False
                            FrmIAPARAsigna.Show()
                        End If
                        'ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "CCABM" Then
                        '    If Me.StatusTextBox.Text = "P" Then
                        '        MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                        '    ElseIf Me.StatusTextBox.Text = "E" Then
                        '        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        '        FrmICABMAsigna.Show()
                        '    End If
                        'ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value ), 1, 5) = "CAPAR" Then
                        '    If Me.StatusTextBox.Text = "P" Then
                        '        MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                        '    ElseIf Me.StatusTextBox.Text = "E" Then
                        '        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        '        FrmIAPARAsigna.Show()
                        '    End If
                    ElseIf (Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "IPAQU" Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "BPAQU" Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "DPAQU" Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RPAQU") Then
                        'If Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmrRelPaquetesdelCliente.Show()
                        'End If
                    ElseIf (Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 6)) = "IPAQUT" Then
                        'If Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmrRelPaquetesdelCliente.Show()
                        'End If
                    ElseIf (Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "IPAQD" Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "BPAQD" Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "DPAQD" Or Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "RPAQD") Then
                        'If Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmrRelPaquetesdelClienteDigital.Show()
                        'End If
                    ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5).Contains("POSIN") = True Then
                        FrmInformacionZonaNorte.contrato = Me.ContratoTextBox.Text
                        GloBloqueaDetalle = True
                        'Checamos si es consulta o para modificar
                        If Not Me.CONORDSERBindingNavigator.Enabled Then
                            Me.BUSCADetOrdSerDataGridView.Enabled = False
                            FrmInformacionZonaNorte.opcion = "Consulta"
                            FrmInformacionZonaNorte.ShowDialog()
                        Else 'If Me.StatusTextBox.Text = "E" Then
                            FrmInformacionZonaNorte.opcion = "Modificar"
                            FrmInformacionZonaNorte.ShowDialog()
                        End If
                    ElseIf (Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, e.RowIndex).Value), 1, 5) = "PRERE") Then
                        'If Me.StatusTextBox.Text = "E" Then
                        'Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmMuestraCoordenadas.ShowDialog()

                    End If
                End If
            Else
                MsgBox("Primero Capture los Datos Solicitados ", MsgBoxStyle.Information)
            End If
        Catch ex As Exception

        End Try
    End Sub




    Private Sub valida()
        Dim OP As Integer = 0
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'If GloClv_TipSer = 2 Then
            '    OP = 0
            'ElseIf GloClv_TipSer = 3 Then
            '    OP = 1
            'End If
            OP = 0
            Me.ValidaTrabajosTableAdapter.Connection = CON
            Me.ValidaTrabajosTableAdapter.Fill(Me.NewSofTvDataSet.ValidaTrabajos, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)), New System.Nullable(Of Integer)(CType(OP, Integer)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If GloControlaReloj = 1 Then
            If LocFecEje = False Then
                If Me.Panel5.BackColor = Color.WhiteSmoke Then
                    Me.Panel5.BackColor = Color.Gold
                    If Me.Fec_EjeTextBox.Focused = False Then
                        Me.Fec_EjeTextBox.Focus()
                    End If
                Else
                    Me.Panel5.BackColor = Color.WhiteSmoke
                End If
            ElseIf LocTec = False Then
                If Me.Panel6.BackColor = Color.WhiteSmoke Then
                    Me.Panel6.BackColor = Color.Gold
                    If Me.Tecnico.Focused = False Then
                        Me.Tecnico.Focus()
                    End If
                Else
                    Me.Panel6.BackColor = Color.WhiteSmoke
                End If
            End If
        End If
    End Sub

    Private Sub Tecnico_DropDown(sender As Object, e As EventArgs) Handles Tecnico.DropDown
        If IsNumeric(Tecnico.SelectedValue) Then
            auxTecnicoAnterior = Tecnico.SelectedValue
        End If
    End Sub

    Private Sub Tecnico_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Tecnico.SelectedIndexChanged
        If Len(Trim(Tecnico.Text)) > 0 And IsNumeric(Tecnico.SelectedValue) = True Then
            LocTec = True
            Panel6.BackColor = Color.WhiteSmoke
            MuestraRelOrdSerCuadrilla(gloClave, Me.Tecnico.SelectedValue)
        Else
            LocTec = False
        End If

        If ValidaArticulosAsignados() And IsNumeric(Tecnico.SelectedValue) And Tecnico.SelectedValue <> auxTecnicoAnterior And (auxTecnicoAnterior > 0 And IsNumeric(auxTecnicoAnterior)) Then
            If MsgBox("Si cambia de técnico, necesitará asignar los aparatos nuevamente. " + vbNewLine + " ¿Desea continuar?", MsgBoxStyle.YesNo) <> 6 Then
                Tecnico.SelectedValue = auxTecnicoAnterior
            Else
                BorraArticulosAsignados()
            End If
        End If
    End Sub

    Private Sub Tecnico_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tecnico.TextChanged
        If Len(Trim(Tecnico.Text)) > 0 And IsNumeric(Tecnico.SelectedValue) = True Then
            LocTec = True
            Panel6.BackColor = Color.WhiteSmoke
        Else
            LocTec = False
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.Tecnico.SelectedValue) = True Then
            Locclv_tec = Me.Tecnico.SelectedValue
            gLOVERgUARDA = 0
            If Me.CONORDSERBindingNavigator.Enabled = False Then
                gLOVERgUARDA = 1
            End If
            SoftvNew.Opcion = opcion
            'Me.DameClv_Session_TecnicosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Tecnicos, clv_sessionTecnico)
            SoftvNew.Module1.GloTipoUsuario = GloTipoUsuario
            SoftvNew.Module1.GloClaveMenus = GloClaveMenus
            'SoftvNew.Module1.MiConexion = MiConexion
            Dim frm As New SoftvNew.FrmDescargaMaterialTec()
            frm.Clv_Orden = CLng(Me.Clv_OrdenTextBox.Text)
            frm.IdTecnico = Locclv_tec
            frm.tipoDescarga = "O"
            frm.RiContrato = ContratoTextBox.Text
            If IsNumeric(FolioTextBox.Text) = True Then
                If CLng(FolioTextBox.Text) > 0 Then
                    frm.IdBitacora = FolioTextBox.Text
                End If
            End If
            frm.ShowDialog()
            'FrmDescargaMaterialTec.Show()
        Else
            MsgBox("Seleccione el Tecnico por favor")
        End If
    End Sub



    Private Sub dimebitacora()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Label3.Visible = True
            Me.FolioTextBox.Visible = True
            Me.DimeSiTieneunaBitacoraTableAdapter.Connection = CON
            Me.DimeSiTieneunaBitacoraTableAdapter.Fill(Me.DataSetEdgarRev2.DimeSiTieneunaBitacora, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ESHOTELLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ImprimeOrdSerRetiro(ByVal ClvOrden As Long)

        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim Titulo As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            If IdSistema = "AG" Then

                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, 1)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(9, ClvOrden)
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(10, ClvOrden)
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(11, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(15, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(16, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)





            mySelectFormula = "Orden " & GloNom_TipSer
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            CON.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora, a)
            CON.Close()
            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(2, True, 1, 1)
            End If
            '--SetDBLogonForReport(connectionInfo)

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GuardaRelOrdenUsuario()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.NueRelOrdenUsuarioTableAdapter.Connection = CON
        Me.NueRelOrdenUsuarioTableAdapter.Fill(Me.DataSetEric.NueRelOrdenUsuario, CLng(Me.Clv_OrdenTextBox.Text), GloClvUsuario, Me.StatusTextBox.Text)
        CON.Close()
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub




    Private Sub BUSCADetOrdSerDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles BUSCADetOrdSerDataGridView.CellContentClick

    End Sub

    Private Sub ChecaRelOrdenUsuario(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaRelOrdenUsuario", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
        Finally
            conexion.Close()
            conexion.Dispose()

        End Try
    End Sub

    Private Sub ConRelClienteTab(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelClienteTab", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Tab", SqlDbType.VarChar, 50)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Me.TextBoxTab.Text = parametro2.Value.ToString
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    'Private Sub Llena_Tabs_Por_Contrato(ByVal Contrato As Long)
    '    Dim con As New SqlConnection(MiConexion)
    '    Dim str As New StringBuilder

    '    str.Append("Exec DameTaps_PorContrato ")
    '    str.Append(CStr(Contrato))

    '    Dim dataAdapter As New SqlDataAdapter(str.ToString(), con)
    '    Dim dataTable As New DataTable
    '    Dim bindingSource As New BindingSource

    '    Try

    '        con.Open()
    '        dataAdapter.Fill(dataTable)
    '        bindingSource.DataSource = dataTable
    '        Me.ComboBoxTap.DataSource = bindingSource

    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '        con.Close()
    '        con.Dispose()
    '    End Try
    'End Sub


    Private Sub NueRelClienteTab(ByVal Contrato As Long, ByVal Tab As String, ByVal NT As String, ByVal IdTap As Integer)

        Dim procedimiento As String
        If NT = "Tap" Then
            procedimiento = "GuardarRelClienteTap"
        End If
        If NT = "Nap" Then
            procedimiento = "GuardarRelClienteNap"
        End If
        Dim conexion As New SqlConnection(MiConexion)
        'Dim comando As New SqlCommand("NueRelClienteTab", conexion)
        Dim comando As New SqlCommand(procedimiento, conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@TAP", SqlDbType.VarChar, 50)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Tab
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@IDTAP", SqlDbType.VarChar, 50)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = IdTap
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub



    Private Sub MuestraRelOrdenesTecnicos(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraRelOrdenesTecnicos ")
        strSQL.Append(CStr(Clv_Orden))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.Tecnico.DataSource = bindingSource


        Catch ex As Exception
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Function EsPreregistro() As Boolean
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_orden", SqlDbType.BigInt, gloClave)
        BaseII.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("EsPreRegistro")
        Return CBool(BaseII.dicoPar("@bnd").ToString)

    End Function

    Private Function RequeireEsperarIp() As Boolean
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_orden", SqlDbType.BigInt, gloClave)
        BaseII.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("RequeireEsperarIp")
        Return CBool(BaseII.dicoPar("@bnd").ToString)

    End Function


    Private Sub ValidaOrdSerManuales(ByVal Clv_Orden As Long)
        Try


            Dim conexion As New SqlConnection(MiConexion)
            Dim comando As New SqlCommand("ValidaOrdSerManuales", conexion)
            comando.CommandType = CommandType.StoredProcedure

            Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = Clv_Orden
            comando.Parameters.Add(parametro)

            Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
            parametro2.Direction = ParameterDirection.Output
            comando.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
            parametro3.Direction = ParameterDirection.Output
            comando.Parameters.Add(parametro3)

            Try
                eRes = 0
                eMsg = String.Empty
                conexion.Open()
                comando.ExecuteNonQuery()
                eRes = parametro2.Value
                eMsg = parametro3.Value
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Finally
                conexion.Close()
                conexion.Dispose()
            End Try
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ModOrdSer(ByVal Clv_Orden As Long, ByVal Clv_TipSer As Integer, ByVal Contrato As Long, ByVal Fec_Sol As String, ByVal Fec_Eje As String, ByVal Visita1 As String, ByVal Visita2 As String, ByVal Status As Char, ByVal Clv_Tecnico As Integer, ByVal Impresa As Boolean, ByVal Clv_Factura As String, ByVal Obs As String, ByVal ListaDeArticulos As Char)

        If Fec_Sol.Length = 0 Then Fec_Sol = "01/01/1900"
        If Fec_Eje.Length = 0 Then Fec_Eje = "01/01/1900"
        If Visita1.Length = 0 Then Visita1 = "01/01/1900"
        If Visita2.Length = 0 Then Visita2 = "01/01/1900"
        If Clv_Factura.Length = 0 Then Clv_Factura = 0

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MODORDSER", conexion)
        Dim reader As SqlDataReader

        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Contrato
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Fec_Sol", SqlDbType.DateTime)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = DateTime.Parse(Fec_Sol)
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Fec_Eje", SqlDbType.DateTime)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = DateTime.Parse(Fec_Eje)
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Visita1", SqlDbType.DateTime)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = DateTime.Parse(Visita1)
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Visita2", SqlDbType.DateTime)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = DateTime.Parse(Visita2)
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = Status
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Clv_Tecnico", SqlDbType.Int)
        parametro9.Direction = ParameterDirection.Input
        parametro9.Value = Clv_Tecnico
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Impresa", SqlDbType.Bit)
        parametro10.Direction = ParameterDirection.Input
        parametro10.Value = Impresa
        comando.Parameters.Add(parametro10)

        Dim parametro11 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro11.Direction = ParameterDirection.Input
        parametro11.Value = CLng(Clv_Factura)
        comando.Parameters.Add(parametro11)

        Dim parametro12 As New SqlParameter("@Obs", SqlDbType.VarChar, 255)
        parametro12.Direction = ParameterDirection.Input
        parametro12.Value = Obs
        comando.Parameters.Add(parametro12)

        Dim parametro13 As New SqlParameter("@ListaDeArticulos", SqlDbType.VarChar, 1)
        parametro13.Direction = ParameterDirection.Input
        parametro13.Value = ListaDeArticulos
        comando.Parameters.Add(parametro13)

        Dim parametro14 As New SqlParameter("@msj", SqlDbType.VarChar, 150)
        parametro14.Direction = ParameterDirection.Output
        parametro14.Value = ""
        comando.Parameters.Add(parametro14)

        Try
            conexion.Open()

            reader = comando.ExecuteReader

            While (reader.Read())
                eMsj = ""
                eMsj = reader(0).ToString()
                If eMsj.Length > 0 Then
                    MessageBox.Show(eMsj, "¡Atención!")
                End If
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Private Sub PreEjecutaOrdSer(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("PREEJECUTAOrdSer", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try



    End Sub

    Private Sub DimeQueAparatosEntrega(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DimeQueAparatosEntrega", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Try

            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                eBndDig = Boolean.Parse(reader(0).ToString)
                eBndNet = Boolean.Parse(reader(1).ToString)
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub GuardaMovSist(ByVal Op As Integer, ByVal Usuario As String, ByVal Clv_Orden As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("GuardaMovSist", conexion)
        comando.CommandTimeout = 0
        comando.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@OP", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Op
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@USUARIO", SqlDbType.VarChar, 5)
        par2.Direction = ParameterDirection.Input
        par2.Value = Usuario
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@CLV_ORDSER", SqlDbType.BigInt)
        par3.Direction = ParameterDirection.Input
        par3.Value = Clv_Orden
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub checaBitacoraTecnico(ByVal prmClvOrden As Integer, ByVal prmTipoDescarga As String)
        Dim CON As New SqlConnection(Globals.DataAccess.GlobalConectionString)
        Dim CMD As New SqlCommand("checaBitacoraTecnico", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@clvOrden", prmClvOrden)
        CMD.Parameters.AddWithValue("@tipoDescarga", prmTipoDescarga)

        Dim READER As SqlDataReader

        Try
            CON.Open()

            READER = CMD.ExecuteReader()
            If READER.HasRows Then
                While (READER.Read)
                    clvTecnicoDescarga = READER(0).ToString
                    clvBitacoraDescarga = READER(1).ToString
                End While
            Else
                clvTecnicoDescarga = 0
                clvBitacoraDescarga = 0
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub uspConsultaTap(ByVal prmContrato As Long, ByVal prmClvCalle As Integer, ByVal prmClvColonia As Integer)
        'Dim CON As New SqlConnection(MiConexion)
        'Dim STR As New StringBuilder

        'STR.Append("EXEC uspConsultaTap ")
        'STR.Append(CStr(prmContrato) & ", ")
        'STR.Append(CStr(prmClvCalle) & ", ")
        'STR.Append(CStr(prmClvColonia))

        'Dim DT As New DataTable
        'Dim DA As New SqlDataAdapter(STR.ToString, CON)

        'Try
        '    CON.Open()
        '    DA.Fill(DT)
        '    Me.ComboBoxTap.DataSource = DT
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Information)
        'Finally
        '    CON.Close()
        '    CON.Dispose()
        'End Try

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(Me.ContratoTextBox.Text))
        BaseII.CreateMyParameter("@clvCalle", SqlDbType.Int, prmClvCalle)
        BaseII.CreateMyParameter("@clvColonia", SqlDbType.Int, prmClvColonia)
        ComboBoxTap.DataSource = BaseII.ConsultaDT("ObtieneTap")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(Me.ContratoTextBox.Text))
        BaseII.CreateMyParameter("@clvCalle", SqlDbType.Int, prmClvCalle)
        BaseII.CreateMyParameter("@clvColonia", SqlDbType.Int, prmClvColonia)
        ComboBoxNap.DataSource = BaseII.ConsultaDT("ObtieneNap")

        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(Me.ContratoTextBox.Text))
        'BaseII.CreateMyParameter("@EsCoaxial", ParameterDirection.Output, SqlDbType.Bit)
        'BaseII.ProcedimientoOutPut("EsCoaxial")
        'EsCoaxial = CBool(BaseII.dicoPar("@EsCoaxial").ToString)

        'If EsCoaxial Then
        '    CMBLabelTab.Text = "TAP"
        'Else
        '    CMBLabelTab.Text = "NAP"
        'End If
    End Sub

    Private Sub uspDameTapCliente(ByVal prmContrato As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("uspDamePlacaTapCliente", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = prmContrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@clvTap", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@placa", SqlDbType.VarChar, 50)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            Me.ComboBoxTap.SelectedValue = CInt(PRM2.Value)
            Me.PlacaTextBox.Text = CStr(PRM3.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub uspGuardaPlacaTapCliente(ByVal prmContrato As Long, ByVal prmClvTap As Integer, ByVal prmPlaca As String, ByVal prmClvOrden As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("uspGuardaPlacaTapCliente", CON)
        CMD.CommandType = CommandType.StoredProcedure

        If IsNumeric(prmClvTap) = False Then
            prmClvTap = 0
        End If

        Dim PRM1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = prmContrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@clvTap", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = prmClvTap
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@placa", SqlDbType.VarChar, 250)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = prmPlaca
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@clvOrden", SqlDbType.BigInt)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = prmClvOrden
        CMD.Parameters.Add(PRM4)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ValidaCajasAsignadas()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("VALIDADECODERS", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@CLV_ORDEN", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = gloClave
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@BND_ORDEN", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Bnd_Orden = parametro2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub DAMEFechayHoraDelServidor()
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        dTable = BaseII.ConsultaDT("DAMEFechayHoraDelServidor")
        If dTable.Rows.Count = 0 Then Exit Sub
        Fec_EjeTextBox.Text = dTable.Rows(0)(0).ToString
        'Fec_EjeTextBox.Enabled = False
    End Sub

    Private Sub CONOrdSerFechas(ByVal CLV_ORDEN As Integer)
        dtpFechaEjecucionReal.Visible = False
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.Int, CLV_ORDEN)
        dTable = BaseII.ConsultaDT("CONOrdSerFechas")
        If dTable.Rows.Count = 0 Then Exit Sub
        dtpFechaEjecucionReal.Value = dTable.Rows(0)(1).ToString
        dtpFechaEjecucionReal.Visible = True
    End Sub


    Private Sub uspContratoServ()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(Me.ContratoTextBox.Text))
        BaseII.CreateMyParameter("@TipoServ", SqlDbType.BigInt, eClv_TipSer)
        BaseII.CreateMyParameter("@pasa", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("uspContratoServ")
        pasa = CInt(BaseII.dicoPar("@pasa").ToString)

    End Sub

    Private Sub ContratoCompania_TextChanged(sender As System.Object, e As System.EventArgs) Handles ContratoCompania.TextChanged

    End Sub

    Private Sub ContratoCompania_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles ContratoCompania.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If ContratoCompania.Text.Length = 0 Then
                Exit Sub
            End If
            Try
                Dim conexion As New SqlConnection(MiConexion)
                Dim array As String() = ContratoCompania.Text.Trim.Split("-")
                Dim comando As New SqlCommand()
                conexion.Open()
                comando.Connection = conexion
                comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + array(0) + " and idcompania=" + array(1) + " and idcompania in (select idcompania from Rel_Usuario_Compania where Clave=" + GloClvUsuario.ToString() + ")"
                ContratoTextBox.Text = comando.ExecuteScalar().ToString()
                GloContrato = ContratoTextBox.Text
                conexion.Close()
            Catch ex As Exception
                MsgBox("Contrato no válido. Vuelve a intentarlo.")
            End Try
        End If
    End Sub

    Private Sub ContratoCompania_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles ContratoCompania.KeyDown
        'If Asc(e.KeyChar) = 13 Then
        '    If IsNumeric(ContratoCompania.Text) = False Then
        '        Exit Sub
        '    End If
        '    Try
        '        Dim conexion As New SqlConnection(MiConexion)
        '        Dim comando As New SqlCommand()
        '        conexion.Open()
        '        comando.Connection = conexion
        '        comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + ContratoCompania.Text.Trim() + " and idcompania=" + GloIdCompania
        '        ContratoTextBox.Text = comando.ExecuteScalar().ToString()

        '    Catch ex As Exception

        '    End Try
        'End If
    End Sub
    Private Sub DameAgenda()
        Try
            Dim conexion As New SqlConnection(MiConexion)
            Dim comando As New SqlCommand()
            conexion.Open()
            comando.Connection = conexion
            comando.CommandText = "exec DameCitaOrdenQueja " + gloClave.ToString + ",1"
            Dim reader As SqlDataReader = comando.ExecuteReader()
            reader.Read()
            If Not reader(0).ToString = "Orden sin agendar." Then
                tbTecnico.Text = reader(0).ToString
                tbTurno.Text = reader(1).ToString
                tbfecha.Text = reader(2).ToString
                tbobservaciones.Text = reader(3).ToString
                'sinagendar.Visible = False
                sinagendar.Text = reader(4).ToString
            Else
                'sinagendar.Visible = True
                sinagendar.Text = "Sin Agendar"
            End If
            reader.Close()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub SP_GuardaOrdSerAparatos(oClv_Orden As Long, oOpcion As String, oStatus As String, oOp2 As Integer)
        '@CLV_ORDEN BIGINT=0,	@OPCION VARCHAR(10)='',	@STATUS VARCHAR(10)='',	@OP2 INT        
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@OPCION", SqlDbType.VarChar, oOpcion, 10)
        BaseII.CreateMyParameter("@STATUS", SqlDbType.VarChar, oStatus, 10)
        BaseII.CreateMyParameter("@OP2", SqlDbType.Int, oOp2)
        BaseII.Inserta("SP_GuardaOrdSerAparatos")
    End Sub

    Private Sub BUSCADetOrdSerDataGridView_CurrentCellChanged(sender As Object, e As System.EventArgs) Handles BUSCADetOrdSerDataGridView.CurrentCellChanged
        ValidaDetalleOrden()
    End Sub

    Private Sub BUSCADetOrdSerDataGridView_Leave(sender As Object, e As System.EventArgs) Handles BUSCADetOrdSerDataGridView.Leave
        ValidaDetalleOrden()
    End Sub

    Private Sub ValidaDetalleOrden_Primer()
        Try
            If BUSCADetOrdSerDataGridView.RowCount > 0 Then
                Dim Fila As Integer = 0
                Fila = 0

                GloDetClave = BUSCADetOrdSerDataGridView.Item(0, Fila).Value
                Me.ClaveTextBox.Text = GloDetClave
                Contrato = Me.ContratoTextBox.Text
                gloClv_Orden = Me.Clv_OrdenTextBox.Text
                GLOTRABAJO = RTrim(LTrim(Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 6)))
                GLONOMTRABAJO = BUSCADetOrdSerDataGridView.Item(3, Fila).Value
                dame_clv_tipser(GLOTRABAJO)
                If opcion = "M" Then
                    Button2.Enabled = False
                End If

                If Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTE" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCABL" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCONT" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAR" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RELNB" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTX" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAG" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RRADI" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RETCA" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RROUT" Then
                    Button2.Text = "&Recibí"
                    Button2.Enabled = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ValidaDetalleOrden()
        Try

            Dim Fila As Integer = 0
            Fila = BUSCADetOrdSerDataGridView.CurrentRow.Index

            GloDetClave = BUSCADetOrdSerDataGridView.Item(0, Fila).Value
            Me.ClaveTextBox.Text = GloDetClave
            Contrato = Me.ContratoTextBox.Text
            gloClv_Orden = Me.Clv_OrdenTextBox.Text
            GLOTRABAJO = RTrim(LTrim(Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 6)))
            GLONOMTRABAJO = BUSCADetOrdSerDataGridView.Item(3, Fila).Value
            dame_clv_tipser(GLOTRABAJO)
            If opcion = "M" Then
                Button2.Enabled = False
                Button2.Text = "&Eliminar"
            End If

            If Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTE" Or _
                                Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCABL" Or _
                                Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCONT" Or _
                                Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAR" Or _
                                Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RELNB" Or _
                                Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTX" Or _
                                Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAG" Or _
                                Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RRADI" Or _
                                Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RETCA" Or _
                                Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RROUT" Then
                Button2.Text = "&Recibí"
                Button2.Enabled = True
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub ValidaDetalleOrden_DetalleGrid()
        Try

            Dim Fila As Integer = 0
            Fila = BUSCADetOrdSerDataGridView.CurrentRow.Index

            For Fila = 0 To BUSCADetOrdSerDataGridView.RowCount - 1

                GloDetClave = BUSCADetOrdSerDataGridView.Item(0, Fila).Value
                Me.ClaveTextBox.Text = GloDetClave
                Contrato = Me.ContratoTextBox.Text
                gloClv_Orden = Me.Clv_OrdenTextBox.Text
                GLOTRABAJO = RTrim(LTrim(Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 6)))
                GLONOMTRABAJO = BUSCADetOrdSerDataGridView.Item(3, Fila).Value
                BUSCADetOrdSerDataGridView.Item(6, Fila).ReadOnly = True

                If Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTE" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCABL" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCONT" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAR" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RELNB" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTX" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAG" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RRADI" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RETCA" Or _
                                    Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RROUT" Then
                    BUSCADetOrdSerDataGridView.Item(6, Fila).ReadOnly = False
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SP_InsertaTbl_NoEntregados(oClv_Orden As Long, oClave As Long)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.Int, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.Inserta("SP_InsertaTbl_NoEntregados")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BorraDetalleOrden_DetalleGrid()
        Try

            Dim Fila As Integer = 0


            For Fila = 0 To BUSCADetOrdSerDataGridView.RowCount - 1

                GloDetClave = BUSCADetOrdSerDataGridView.Item(0, Fila).Value
                Me.ClaveTextBox.Text = GloDetClave
                Contrato = Me.ContratoTextBox.Text
                gloClv_Orden = Me.Clv_OrdenTextBox.Text
                GLOTRABAJO = RTrim(LTrim(Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 6)))
                GLONOMTRABAJO = BUSCADetOrdSerDataGridView.Item(3, Fila).Value
                BUSCADetOrdSerDataGridView.Item(6, Fila).ReadOnly = True


                If Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTE" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCABL" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCONT" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAR" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RELNB" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTX" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAG" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RRADI" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RETCA" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RROUT" Then

                    If BUSCADetOrdSerDataGridView.Item(6, Fila).Value = 0 Or BUSCADetOrdSerDataGridView.Item(6, Fila).Value = False Then
                        If IsNumeric(Me.ClaveTextBox.Text) = True Then

                            SP_InsertaTbl_NoEntregados(gloClv_Orden, GloDetClave)


                        End If
                    End If
                End If
            Next

            For Fila = 0 To BUSCADetOrdSerDataGridView.RowCount - 1

                GloDetClave = BUSCADetOrdSerDataGridView.Item(0, Fila).Value
                Me.ClaveTextBox.Text = GloDetClave
                Contrato = Me.ContratoTextBox.Text
                gloClv_Orden = Me.Clv_OrdenTextBox.Text
                GLOTRABAJO = RTrim(LTrim(Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 6)))
                GLONOMTRABAJO = BUSCADetOrdSerDataGridView.Item(3, Fila).Value
                BUSCADetOrdSerDataGridView.Item(6, Fila).ReadOnly = True
                If Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTE" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCABL" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCONT" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAR" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RELNB" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTX" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAG" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RRADI" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RETCA" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RROUT" Then

                    If BUSCADetOrdSerDataGridView.Item(6, Fila).Value = 0 Or BUSCADetOrdSerDataGridView.Item(6, Fila).Value = False Then
                        If IsNumeric(Me.ClaveTextBox.Text) = True Then


                            Dim CON As New SqlConnection(MiConexion)
                            CON.Open()
                            Me.BorraMotivoCanServTableAdapter.Connection = CON
                            Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, Me.Clv_OrdenTextBox.Text, 0, Me.ClaveTextBox.Text, 0, 2)
                            'Me.CONDetOrdSerTableAdapter.Connection = CON
                            'Me.CONDetOrdSerTableAdapter.Delete(Me.ClaveTextBox.Text)
                            CON.Close()
                            Me.ClaveTextBox.Text = 0
                        End If
                    End If
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Function ValidaBorraDetalleOrden_DetalleGrid() As Boolean
        Try
            ValidaBorraDetalleOrden_DetalleGrid = True
            Dim Fila As Integer = 0
            Dim CuantosSi As Integer = 0
            Dim CuantosNo As Integer = 0
            Dim DimeSiesRetiro As Integer = 0

            For Fila = 0 To BUSCADetOrdSerDataGridView.RowCount - 1

                If Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTE" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCABL" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RCONT" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAR" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RELNB" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RANTX" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RAPAG" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RRADI" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RETCA" Or _
                                   Mid(Trim(BUSCADetOrdSerDataGridView.Item(3, Fila).Value), 1, 5) = "RROUT" Then
                    DimeSiesRetiro = 1
                    If BUSCADetOrdSerDataGridView.Item(6, Fila).Value = 1 Or BUSCADetOrdSerDataGridView.Item(6, Fila).Value = True Then
                        CuantosSi += 1
                    Else
                        CuantosNo += 1
                    End If
                End If
            Next

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_orden", SqlDbType.BigInt, Clv_OrdenTextBox.Text)
            BaseII.CreateMyParameter("@CuantoExtraviados", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("CuantoExtraviados")
            CuantosSi = CuantosSi + CInt(BaseII.dicoPar("@CuantoExtraviados").ToString)

            If DimeSiesRetiro = 1 Then


                If CuantosSi = 0 Then
                    ValidaBorraDetalleOrden_DetalleGrid = False
                End If

            End If

        Catch ex As Exception

        End Try
    End Function

    Private Sub BUSCADetOrdSerDataGridView_LostFocus(sender As Object, e As System.EventArgs) Handles BUSCADetOrdSerDataGridView.LostFocus

    End Sub

    Private Sub BUSCADetOrdSerDataGridView_RowLeave(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles BUSCADetOrdSerDataGridView.RowLeave
        ValidaDetalleOrden()
    End Sub

    Public Sub SP_LLena_Bitacora_Ordenes(oClvTxtUsuario As String, oDescripcionMov As String, oclv_orden As Long)
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClvTxtUsuario", SqlDbType.VarChar, oClvTxtUsuario, 6)
            BaseII.CreateMyParameter("@DescripcionMov", SqlDbType.VarChar, oDescripcionMov, 450)
            BaseII.CreateMyParameter("@clv_orden", SqlDbType.Int, oclv_orden)
            BaseII.ProcedimientoOutPut("SP_LLena_Bitacora_Ordenes")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Public Function SP_ValidaGuardaOrdSerAparatos(oClv_Orden As Long, oOpcion As String, oStatus As String, oOp2 As Integer, oClv_Tecnico As Long) As String
        SP_ValidaGuardaOrdSerAparatos = ""
        '@CLV_ORDEN BIGINT=0,	@OPCION VARCHAR(10)='',	@STATUS VARCHAR(10)='',	@OP2 INT,	@Msj varchar(400)='' output
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@OPCION", SqlDbType.VarChar, oOpcion, 10)
        BaseII.CreateMyParameter("@STATUS", SqlDbType.VarChar, oStatus, 10)
        BaseII.CreateMyParameter("@OP2", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 400)
        BaseII.CreateMyParameter("@Clv_Tecnico", SqlDbType.BigInt, oClv_Tecnico)
        BaseII.ProcedimientoOutPut("SP_ValidaGuardaOrdSerAparatos")
        SP_ValidaGuardaOrdSerAparatos = BaseII.dicoPar("@Msj").ToString
    End Function

    Private Function validaEliminarOrden() As Integer
        validaEliminarOrden = 0
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.VarChar, GloUsuario)
        BaseII.CreateMyParameter("@activo", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("sp_validaEliminarOrden")
        If BaseII.dicoPar("@activo") = 1 Then
            BindingNavigatorDeleteItem.Enabled = True
            validaEliminarOrden = 1
        End If
        Return validaEliminarOrden
    End Function

    Private Function ValidaArticulosAsignados() As Boolean
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.BigInt, gloClv_Orden)
        BaseII.CreateMyParameter("@BND_ORDEN", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("sp_ValidaArticulosAsignados")
        Return CBool(BaseII.dicoPar("@BND_ORDEN").ToString)

    End Function

    Private Sub Borra_Orden(oClv_Orden As Long)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.Inserta("Borra_Orden")
    End Sub


    Private Sub BorraArticulosAsignados()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.BigInt, gloClv_Orden)
        BaseII.Inserta("sp_BorraArticulosAsignados")
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BUSCADetOrdSerTableAdapter.Connection = CON
        Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(gloClave, Long)))

        CON.Close()
    End Sub


    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        FrmCoordenadasCliente.ShowDialog()
    End Sub

    Private Sub MuestraRelOrdSerCuadrilla(ByVal Clv_Orden As Long, ByVal Clv_Tecnico As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraRelOrdSerCuadrilla " + Clv_Orden.ToString() + ", " + Clv_Tecnico.ToString())


        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cbCuadrilla.DataSource = bindingSource
        Catch ex As Exception
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
    Private Sub NueRelOrdSerCuadrilla(ByVal Clv_Orden As Long, ByVal Clv_Tecnico As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelOrdSerCuadrilla", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Orden
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Tecnico", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Tecnico
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally

            conexion.Close()
            conexion.Dispose()

        End Try

    End Sub


    Private Sub btnAgregarRepetidor_Click(sender As Object, e As EventArgs) Handles btnAgregarRepetidor.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_orden", SqlDbType.BigInt, gloClave)
        BaseII.Inserta("AgregaRepetidorInternet")
        Actuliza_Detalle()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        FrmRoboAparatos.clv_ordenRobo = Clv_OrdenTextBox.Text
        FrmRoboAparatos.contratoRobo = ContratoTextBox.Text
        FrmRoboAparatos.ShowDialog()
    End Sub
End Class
'prueba check in 
