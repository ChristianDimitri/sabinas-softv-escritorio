﻿Public Class FrmEntreCalles

    Public CallePrincipal As Integer = 0
    Public UbicacionCallePrincipal As Char = ""
    Public ContratoEntreCalles As Integer = 0
    Public clv_sesionEntreCalles As Integer = 0
    Public clv_coloniaEntreCalles As Integer = 0





    Private Sub FrmEntreCalles_Load(sender As Object, e As EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Button1.ForeColor = Button5.ForeColor
        Button1.BackColor = Button5.BackColor
        Button2.ForeColor = Button5.ForeColor
        Button2.BackColor = Button5.BackColor

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, clv_coloniaEntreCalles)
        cbNorte.DataSource = BaseII.ConsultaDT("MuestraCalleColoniaSelecciona")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, clv_coloniaEntreCalles)
        cbSur.DataSource = BaseII.ConsultaDT("MuestraCalleColoniaSelecciona")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, clv_coloniaEntreCalles)
        cbEste.DataSource = BaseII.ConsultaDT("MuestraCalleColoniaSelecciona")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, clv_coloniaEntreCalles)
        cbOeste.DataSource = BaseII.ConsultaDT("MuestraCalleColoniaSelecciona")

        If OpcionCli = "C" Then
            GroupBox1.Enabled = False
            GroupBoxCoordenadas.Enabled = False
        End If
        llenaEntrecalles()

        If ContratoEntreCalles = 0 Then
            GroupBoxCoordenadas.Visible = False
            Button3.Visible = False
        Else
            llenaCoordenadasCliente()
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim aux As Int16 = 0
        If Not (RadioNorte.Checked Or RadioSur.Checked Or RadioEste.Checked Or RadioOeste.Checked) Then
            MsgBox("Falta señalar la calle en la que se encuentra la casa del cliente", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If ((RadioNorte.Checked Or RadioSur.Checked) And (LbEste.Text.Length = 0 And LbOeste.Text.Length = 0)) Or ((RadioEste.Checked Or RadioOeste.Checked) And (LbNorte.Text.Length = 0 And LbSur.Text.Length = 0)) Then
            MsgBox("Debe de captutar por lo menos una calle perpendicular a la calle del cliente", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If tbReferencia.Text.Length = 0 Then
            MsgBox("Debe de captutar la referencia del domicilio", MsgBoxStyle.Exclamation)
            Exit Sub
        End If



        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoEntreCalles)
        BaseII.CreateMyParameter("@CalleNorte", SqlDbType.Int, cbNorte.SelectedValue)
        BaseII.CreateMyParameter("@CalleSur", SqlDbType.Int, cbSur.SelectedValue)
        BaseII.CreateMyParameter("@CalleEste", SqlDbType.Int, cbEste.SelectedValue)
        BaseII.CreateMyParameter("@CalleOeste", SqlDbType.Int, cbOeste.SelectedValue)
        BaseII.CreateMyParameter("@Casa", SqlDbType.VarChar, UbicacionCallePrincipal, 5)
        BaseII.CreateMyParameter("@Clv_sesion", SqlDbType.BigInt, clv_sesionEntreCalles)
        BaseII.CreateMyParameter("@referencia", SqlDbType.VarChar, tbReferencia.Text, 150)
        BaseII.Inserta("InsertaEntrecalles")

        MsgBox("Guardado correctamente")

        Dim auxEntreCallesParaCliente As String = ""
        If UbicacionCallePrincipal <> "N" And cbNorte.SelectedValue > 0 Then
            auxEntreCallesParaCliente += cbNorte.Text + ", "
        End If
        If UbicacionCallePrincipal <> "S" And cbSur.SelectedValue > 0 Then
            auxEntreCallesParaCliente += cbSur.Text + ", "
        End If
        If UbicacionCallePrincipal <> "E" And cbEste.SelectedValue > 0 Then
            auxEntreCallesParaCliente += cbEste.Text + ", "
        End If
        If UbicacionCallePrincipal <> "O" And cbOeste.SelectedValue > 0 Then
            auxEntreCallesParaCliente += cbOeste.Text + ", "
        End If

        'auxEntreCallesParaCliente += tbReferencia.Text

        FrmClientes.ENTRECALLESTextBox.Text = auxEntreCallesParaCliente
        If ContratoEntreCalles = 0 Then
            bitsist(GloUsuario, clv_sesionEntreCalles, "Softv", "Entrecalles", "Nuevo de entrecalles para clv_sesion", clv_sesionEntreCalles.ToString(), "", "AG")
        Else
            bitsist(GloUsuario, ContratoEntreCalles, "Softv", "Entrecalles", "Cambio de entrecalles del contrato", "", "", "AG")
        End If

        Me.Close()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub llenaEntrecalles()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoEntreCalles)
        BaseII.CreateMyParameter("@CalleNorte", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@CalleSur", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@CalleEste", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@CalleOeste", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@casa", ParameterDirection.Output, SqlDbType.VarChar, 5)
        BaseII.CreateMyParameter("@Clv_sesion", SqlDbType.BigInt, clv_sesionEntreCalles)
        BaseII.CreateMyParameter("@referencia", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("DameEntrecalles")

        cbNorte.SelectedValue = BaseII.dicoPar("@CalleNorte").ToString()
        cbSur.SelectedValue = BaseII.dicoPar("@CalleSur").ToString()
        cbEste.SelectedValue = BaseII.dicoPar("@CalleEste").ToString()
        cbOeste.SelectedValue = BaseII.dicoPar("@CalleOeste").ToString()
        UbicacionCallePrincipal = BaseII.dicoPar("@casa").ToString()
        tbReferencia.Text = BaseII.dicoPar("@referencia").ToString()

        If UbicacionCallePrincipal = "N" Then
            RadioNorte.Checked = True
        ElseIf UbicacionCallePrincipal = "S" Then
            RadioSur.Checked = True
        ElseIf UbicacionCallePrincipal = "E" Then
            RadioEste.Checked = True
        ElseIf UbicacionCallePrincipal = "O" Then
            RadioOeste.Checked = True
        End If

    End Sub


    Private Sub RadioNorte_CheckedChanged(sender As Object, e As EventArgs) Handles RadioNorte.CheckedChanged
        If RadioNorte.Checked Then
            PictureBox2.Visible = True
            PictureBox2.Location = New System.Drawing.Point(621, 159)
            cbNorte.SelectedValue = CallePrincipal
            UbicacionCallePrincipal = "N"
        Else
            cbNorte.SelectedValue = 0
        End If
    End Sub

    Private Sub RadioSur_CheckedChanged(sender As Object, e As EventArgs) Handles RadioSur.CheckedChanged
        If RadioSur.Checked Then
            PictureBox2.Visible = True
            PictureBox2.Location = New System.Drawing.Point(621, 234)
            cbSur.SelectedValue = CallePrincipal
            UbicacionCallePrincipal = "S"
        Else
            cbSur.SelectedValue = 0
        End If
    End Sub

    Private Sub RadioEste_CheckedChanged(sender As Object, e As EventArgs) Handles RadioEste.CheckedChanged
        If RadioEste.Checked Then
            PictureBox2.Visible = True
            PictureBox2.Location = New System.Drawing.Point(712, 194)
            cbEste.SelectedValue = CallePrincipal
            UbicacionCallePrincipal = "E"
        Else
            cbEste.SelectedValue = 0
        End If
    End Sub

    Private Sub RadioOeste_CheckedChanged(sender As Object, e As EventArgs) Handles RadioOeste.CheckedChanged
        If RadioOeste.Checked Then
            PictureBox2.Visible = True
            PictureBox2.Location = New System.Drawing.Point(530, 194)
            cbOeste.SelectedValue = CallePrincipal
            UbicacionCallePrincipal = "O"
        Else
            cbOeste.SelectedValue = 0
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Not (IsNumeric(tbLatDecimal.Text) And IsNumeric(tbLongDecimal.Text)) Then
            MsgBox("Captura las coordenadas", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, ContratoEntreCalles)
            BaseII.CreateMyParameter("@latitud", SqlDbType.VarChar, tbLatDecimal.Text, 50)
            BaseII.CreateMyParameter("@longitud", SqlDbType.VarChar, tbLongDecimal.Text, 50)
            BaseII.Inserta("SP_GuardaCoordenadas")
            MsgBox("Guardado correctamente")
            bitsist(GloUsuario, ContratoEntreCalles, "Softv", "Entrecalles", "Cambio de coordenadas", "", tbLatDecimal.Text + "," + tbLongDecimal.Text, "AG")

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub llenaCoordenadasCliente()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoEntreCalles)
        BaseII.CreateMyParameter("@latitud", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@longitud", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("SP_DameCoordenadas")

        tbLatDecimal.Text = BaseII.dicoPar("@latitud").ToString()
        tbLongDecimal.Text = BaseII.dicoPar("@longitud").ToString()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If tbLatDecimal.Text <> "0" And tbLongDecimal.Text <> "0" Then
            Dim sInfo As ProcessStartInfo = New ProcessStartInfo("https://www.google.com.mx/maps/place//@" + tbLatDecimal.Text.Trim + "," + tbLongDecimal.Text.Trim + ",17z/data=!4m2!3m1!1s0x0:0x0")
            Process.Start(sInfo)
        End If
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub cbNorte_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbNorte.SelectedValueChanged
        If cbNorte.SelectedValue > 0 Then
            LbNorte.Text = cbNorte.Text
        Else
            LbNorte.Text = ""
        End If

    End Sub

    Private Sub cbSur_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbSur.SelectedValueChanged
        If cbSur.SelectedValue > 0 Then
            LbSur.Text = cbSur.Text
        Else
            LbSur.Text = ""
        End If
    End Sub

    Private Sub cbEste_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbEste.SelectedValueChanged
        If cbEste.SelectedValue > 0 Then
            LbEste.Text = cbEste.Text
        Else
            LbEste.Text = ""
        End If
    End Sub

    Private Sub cbOeste_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbOeste.SelectedValueChanged
        If cbOeste.SelectedValue > 0 Then
            LbOeste.Text = cbOeste.Text
        Else
            LbOeste.Text = ""
        End If
    End Sub
End Class