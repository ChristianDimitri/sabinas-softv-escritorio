Imports System.Data.SqlClient
Imports System.Text
Public Class FrmSelTrabajo_Rep
    Dim bndreporteTec As Boolean = True

    Private Sub FrmSelTrabajo_Rep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        Me.MuestraSelecciona_TrabajoTmpNuevoTableAdapter.Connection = CON
        Me.MuestraSelecciona_TrabajoTmpNuevoTableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TrabajoTmpNuevo, LocClv_session, CLng(Me.ComboBox4.SelectedValue))
        Me.MuestraSelecciona_Trabajo_tmpConsultaTableAdapter.Connection = CON
        Me.MuestraSelecciona_Trabajo_tmpConsultaTableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_Trabajo_tmpConsulta, LocClv_session)
        CON.Close()
        'MuestraTipOrdQuejaPrincipal()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Insertauno_Seleccion_TrabajoTableAdapter.Connection = CON
        Me.Insertauno_Seleccion_TrabajoTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_Trabajo, LocClv_session, CLng(Me.ListBox1.SelectedValue))
        CON.Close()
        Muestra(LocClv_session)
    End Sub
    Private Sub Muestra(ByVal clv_session As Long)
        Dim CON1 As New SqlConnection(MiConexion)
        CON1.Open()
        Me.MuestraSelecciona_TrabajoConsultaTableAdapter.Connection = CON1
        Me.MuestraSelecciona_TrabajoConsultaTableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TrabajoConsulta, clv_session)
        Me.MuestraSelecciona_Trabajo_tmpConsultaTableAdapter.Connection = CON1
        Me.MuestraSelecciona_Trabajo_tmpConsultaTableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_Trabajo_tmpConsulta, LocClv_session)
        CON1.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTOSeleccion_trabajoTableAdapter.Connection = CON
        Me.InsertaTOSeleccion_trabajoTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTOSeleccion_trabajo, LocClv_session)
        CON.Close()
        Muestra(LocClv_session)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Insertauno_Seleccion_Trabajo_tmpTableAdapter.Connection = CON
        Me.Insertauno_Seleccion_Trabajo_tmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_Trabajo_tmp, LocClv_session, CLng(Me.ListBox2.SelectedValue))
        CON.Close()
        Muestra(LocClv_session)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTOSeleccion_trabajo_tmpTableAdapter.Connection = CON
        Me.InsertaTOSeleccion_trabajo_tmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTOSeleccion_trabajo_tmp, LocClv_session)
        CON.Close()
        Muestra(LocClv_session)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim cont As Integer = 0
        cont = Me.ListBox2.Items.Count
        If cont = 0 Then
            MsgBox("Seleccione Al Menos un Trabajo", MsgBoxStyle.Information)
        ElseIf cont > 0 Then
            FrmSelFechas2.Show()
            bndreportetec = False
            Me.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If bndreportetec = True Then
            Me.Borra_Seleccion_trabajoTableAdapter.Connection = CON
            Me.Borra_Seleccion_trabajoTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_Seleccion_trabajo, LocClv_session)
        End If
        Me.MuestraSelecciona_TrabajoTmpNuevoTableAdapter.Connection = CON
        Me.MuestraSelecciona_TrabajoTmpNuevoTableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TrabajoTmpNuevo, LocClv_session, CLng(Me.ComboBox4.SelectedValue))
        Muestra(LocClv_session)
        CON.Close()
    End Sub



    'Private Sub MuestraTipOrdQuejaPrincipal()
    '    Dim con As New SqlConnection(MiConexion)
    '    Dim str As New StringBuilder

    '    str.Append("Exec MuestraTipOrdQuejaPrincipal ")


    '    Dim dataadapter As New SqlDataAdapter(str.ToString, con)
    '    Dim datatable As New DataTable
    '    Dim binding As New BindingSource

    '    Try
    '        con.Open()
    '        dataadapter.Fill(datatable)
    '        binding.DataSource = datatable
    '        Me.ComboBox1.DataSource = binding

    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '        con.Close()
    '        con.Dispose()
    '    End Try

    'End Sub

    'Private Sub MuestraSelecciona_QuejaTmpNuevo(ByVal clv_tipserv As Integer)

    '    Dim con As New SqlConnection(MiConexion)
    '    Dim str As New StringBuilder

    '    str.Append("Exec MuestraSelecciona_QuejaTmpNuevo ")
    '    str.Append(CStr(clv_tipserv))

    '    Dim dataadapter As New SqlDataAdapter(str.ToString, con)
    '    Dim datatable As New DataTable
    '    Dim binding As New BindingSource

    '    Try
    '        con.Open()
    '        dataadapter.Fill(datatable)
    '        binding.DataSource = datatable

    '    Catch ex As Exception

    '    End Try

    'End Sub

    'Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
    '    If Me.ComboBox1.Text = "Ordenes" Then
    '        TextBox1.Text = "S"
    '    ElseIf Me.ComboBox1.Text = "Quejas" Then
    '        TextBox1.Text = "Q"
    '    ElseIf Me.ComboBox1.Text = "Ambas" Then
    '        TextBox1.Text = ""
    '    End If
    'End Sub

    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged

    End Sub

    Private Sub ListBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox2.SelectedIndexChanged

    End Sub
End Class