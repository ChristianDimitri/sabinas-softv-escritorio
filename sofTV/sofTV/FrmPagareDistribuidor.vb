﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Xml
Imports System.Net
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Public Class FrmPagareDistribuidor

    Private Sub FrmDocumentosCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Button1.BackColor = Button2.BackColor
        Button1.ForeColor = Button2.ForeColor
        Button5.BackColor = Button2.BackColor
        Button5.ForeColor = Button2.ForeColor
        muestraArchivo()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim ofd As New OpenFileDialog()
        ofd.Title = "Selecciona un archivo."
        ofd.Filter = "Files|*.pdf;*.jpg;*.png"
        ofd.FilterIndex = 1
        ofd.RestoreDirectory = True
        If ofd.ShowDialog() = DialogResult.OK Then
            Dim fi As FileInfo = New FileInfo(ofd.FileName)
            If ofd.FileName.Contains(".pdf") Then
                pdfViewer.Visible = True
                PictureBox1.Visible = False
                pdfViewer.Navigate(ofd.FileName)
                tbNombre.Text = ofd.FileName
            Else
                If fi.Length > 1000000 Then
                    MsgBox("Imagen muy grande, no soportada por el sistema")
                    Exit Sub
                End If
                PictureBox1.Visible = True
                pdfViewer.Visible = False
                tbNombre.Text = ofd.FileName
                PictureBox1.Image = Image.FromFile(ofd.FileName)
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If tbNombre.Text = "" Then
            MsgBox("Debe seleccionar un archivo.")
            Exit Sub
        End If
        Try
            'Dim argbit As Byte() = File.ReadAllBytes((tbNombre.Text))
            'Dim arch As Archivo = New Archivo()
            'arch.imagen = argbit
            'Dim xsSubmit As System.Xml.Serialization.XmlSerializer = New System.Xml.Serialization.XmlSerializer(arch.GetType())
            'Dim sww As StringWriter = New StringWriter()
            'Dim writer As XmlWriter = XmlWriter.Create(sww)
            'xsSubmit.Serialize(writer, arch)
            'Dim xml As String = sww.ToString()
            'Fin Prueba 2
            'MsgBox(xmlstring)
            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()
            BaseII.limpiaParametros()

            If pdfViewer.Visible Then
                Dim fInfo As New FileInfo(tbNombre.Text)
                Dim numBytes As Long = fInfo.Length
                Dim fStream As New FileStream(tbNombre.Text, FileMode.Open, FileAccess.Read)
                Dim br As New BinaryReader(fStream)
                Dim data As Byte() = br.ReadBytes(CInt(numBytes))
                br.Close()
                fStream.Close()
                BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, GloClvPlazaAux)
                BaseII.CreateMyParameter("@archivo", SqlDbType.Image, data)
                BaseII.Inserta("GuardaDocumentoPdfPagare")
            Else
                PictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
                BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, GloClvPlazaAux)
                BaseII.CreateMyParameter("@archivo", SqlDbType.Image, ms.GetBuffer())
                BaseII.Inserta("GuardaDocumentoImagePagare")
            End If

            MsgBox("Documento guardado con éxito.")
            tbNombre.Text = ""
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
    Public Class Archivo

        Public imagen As Byte()
    End Class


    Public Class imagen
        Public imagen As Image
    End Class


    Private Sub muestraArchivo()
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim comando As New SqlCommand()
        comando.Connection = conexion
        comando.CommandText = "select Archivo,Tipo from DocumentosClientes.dbo.tbl_distribuidorPagare where clv_plaza=" + GloClvPlazaAux.ToString()
        Dim reader As SqlDataReader = comando.ExecuteReader
        If reader.Read Then
            If reader.GetValue(1).ToString = "image" Then
                Dim by(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
                reader.GetBytes(0, 0, by, 0, by.Length)
                Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(by)
                PictureBox1.Image = Image.FromStream(ms)
                reader.Close()
                PictureBox1.Visible = True
                pdfViewer.Visible = False
            Else
                Dim contents(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
                'contents = reader.GetBytes(0, 0, contents, 0, contents.Length)
                contents = reader.GetValue(0)
                Dim fileName As String = System.IO.Path.GetTempFileName() + ".pdf"
                File.WriteAllBytes(fileName, contents)
                reader.Close()
                PictureBox1.Visible = False
                pdfViewer.Visible = True
                pdfViewer.Navigate(fileName)
            End If
        Else
            PictureBox1.Visible = True
            pdfViewer.Visible = False
            PictureBox1.Image = sofTV.My.Resources.Resources.filenotfound4041
        End If
        conexion.Close()

    End Sub






End Class
