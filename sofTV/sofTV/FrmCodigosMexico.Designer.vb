<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCodigosMexico
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_CodigoLabel As System.Windows.Forms.Label
        Dim PoblacionLabel As System.Windows.Forms.Label
        Dim EstadoLabel As System.Windows.Forms.Label
        Dim CentralLabel As System.Windows.Forms.Label
        Dim PreSuscripcionLabel As System.Windows.Forms.Label
        Dim RegionLabel As System.Windows.Forms.Label
        Dim ASLLabel As System.Windows.Forms.Label
        Dim CLDLabel As System.Windows.Forms.Label
        Dim SerieLabel As System.Windows.Forms.Label
        Dim NumeracionInicialLabel As System.Windows.Forms.Label
        Dim NumeracionFinalLabel As System.Windows.Forms.Label
        Dim InicialLabel As System.Windows.Forms.Label
        Dim FinalLabel As System.Windows.Forms.Label
        Dim OcupacionLabel As System.Windows.Forms.Label
        Dim TipoDeRedLabel As System.Windows.Forms.Label
        Dim ModalidadLabel As System.Windows.Forms.Label
        Dim EstatusLabel As System.Windows.Forms.Label
        Dim RazonSocialLabel As System.Windows.Forms.Label
        Dim IdOperadorLabel As System.Windows.Forms.Label
        Dim MunicipioLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCodigosMexico))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Consulta_CodigosMexicoBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Consulta_CodigosMexicoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_CodigosMexicoBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_CodigoTextBox = New System.Windows.Forms.TextBox()
        Me.PoblacionTextBox = New System.Windows.Forms.TextBox()
        Me.EstadoTextBox = New System.Windows.Forms.TextBox()
        Me.CentralTextBox = New System.Windows.Forms.TextBox()
        Me.PreSuscripcionTextBox = New System.Windows.Forms.TextBox()
        Me.RegionTextBox = New System.Windows.Forms.TextBox()
        Me.ASLTextBox = New System.Windows.Forms.TextBox()
        Me.CLDTextBox = New System.Windows.Forms.TextBox()
        Me.SerieTextBox = New System.Windows.Forms.TextBox()
        Me.NumeracionInicialTextBox = New System.Windows.Forms.TextBox()
        Me.NumeracionFinalTextBox = New System.Windows.Forms.TextBox()
        Me.InicialTextBox = New System.Windows.Forms.TextBox()
        Me.FinalTextBox = New System.Windows.Forms.TextBox()
        Me.OcupacionTextBox = New System.Windows.Forms.TextBox()
        Me.TipoDeRedTextBox = New System.Windows.Forms.TextBox()
        Me.ModalidadTextBox = New System.Windows.Forms.TextBox()
        Me.EstatusTextBox = New System.Windows.Forms.TextBox()
        Me.RazonSocialTextBox = New System.Windows.Forms.TextBox()
        Me.IdOperadorTextBox = New System.Windows.Forms.TextBox()
        Me.MunicipioTextBox = New System.Windows.Forms.TextBox()
        Me.Consulta_CodigosMexicoTableAdapter = New sofTV.DataSetLidia2TableAdapters.consulta_CodigosMexicoTableAdapter()
        Clv_CodigoLabel = New System.Windows.Forms.Label()
        PoblacionLabel = New System.Windows.Forms.Label()
        EstadoLabel = New System.Windows.Forms.Label()
        CentralLabel = New System.Windows.Forms.Label()
        PreSuscripcionLabel = New System.Windows.Forms.Label()
        RegionLabel = New System.Windows.Forms.Label()
        ASLLabel = New System.Windows.Forms.Label()
        CLDLabel = New System.Windows.Forms.Label()
        SerieLabel = New System.Windows.Forms.Label()
        NumeracionInicialLabel = New System.Windows.Forms.Label()
        NumeracionFinalLabel = New System.Windows.Forms.Label()
        InicialLabel = New System.Windows.Forms.Label()
        FinalLabel = New System.Windows.Forms.Label()
        OcupacionLabel = New System.Windows.Forms.Label()
        TipoDeRedLabel = New System.Windows.Forms.Label()
        ModalidadLabel = New System.Windows.Forms.Label()
        EstatusLabel = New System.Windows.Forms.Label()
        RazonSocialLabel = New System.Windows.Forms.Label()
        IdOperadorLabel = New System.Windows.Forms.Label()
        MunicipioLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.Consulta_CodigosMexicoBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_CodigosMexicoBindingNavigator.SuspendLayout()
        CType(Me.Consulta_CodigosMexicoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_CodigoLabel
        '
        Clv_CodigoLabel.AutoSize = True
        Clv_CodigoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CodigoLabel.Location = New System.Drawing.Point(63, 64)
        Clv_CodigoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_CodigoLabel.Name = "Clv_CodigoLabel"
        Clv_CodigoLabel.Size = New System.Drawing.Size(96, 18)
        Clv_CodigoLabel.TabIndex = 0
        Clv_CodigoLabel.Text = "Clv Codigo:"
        '
        'PoblacionLabel
        '
        PoblacionLabel.AutoSize = True
        PoblacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PoblacionLabel.Location = New System.Drawing.Point(68, 96)
        PoblacionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        PoblacionLabel.Name = "PoblacionLabel"
        PoblacionLabel.Size = New System.Drawing.Size(88, 18)
        PoblacionLabel.TabIndex = 2
        PoblacionLabel.Text = "Poblacion:"
        '
        'EstadoLabel
        '
        EstadoLabel.AutoSize = True
        EstadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EstadoLabel.Location = New System.Drawing.Point(95, 161)
        EstadoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        EstadoLabel.Name = "EstadoLabel"
        EstadoLabel.Size = New System.Drawing.Size(66, 18)
        EstadoLabel.TabIndex = 4
        EstadoLabel.Text = "Estado:"
        '
        'CentralLabel
        '
        CentralLabel.AutoSize = True
        CentralLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CentralLabel.Location = New System.Drawing.Point(92, 191)
        CentralLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CentralLabel.Name = "CentralLabel"
        CentralLabel.Size = New System.Drawing.Size(67, 18)
        CentralLabel.TabIndex = 6
        CentralLabel.Text = "Central:"
        '
        'PreSuscripcionLabel
        '
        PreSuscripcionLabel.AutoSize = True
        PreSuscripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PreSuscripcionLabel.Location = New System.Drawing.Point(19, 223)
        PreSuscripcionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        PreSuscripcionLabel.Name = "PreSuscripcionLabel"
        PreSuscripcionLabel.Size = New System.Drawing.Size(133, 18)
        PreSuscripcionLabel.TabIndex = 8
        PreSuscripcionLabel.Text = "Pre Suscripcion:"
        '
        'RegionLabel
        '
        RegionLabel.AutoSize = True
        RegionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RegionLabel.Location = New System.Drawing.Point(92, 257)
        RegionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        RegionLabel.Name = "RegionLabel"
        RegionLabel.Size = New System.Drawing.Size(66, 18)
        RegionLabel.TabIndex = 10
        RegionLabel.Text = "Region:"
        '
        'ASLLabel
        '
        ASLLabel.AutoSize = True
        ASLLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ASLLabel.Location = New System.Drawing.Point(120, 290)
        ASLLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ASLLabel.Name = "ASLLabel"
        ASLLabel.Size = New System.Drawing.Size(43, 18)
        ASLLabel.TabIndex = 12
        ASLLabel.Text = "ASL:"
        '
        'CLDLabel
        '
        CLDLabel.AutoSize = True
        CLDLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CLDLabel.Location = New System.Drawing.Point(117, 322)
        CLDLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CLDLabel.Name = "CLDLabel"
        CLDLabel.Size = New System.Drawing.Size(46, 18)
        CLDLabel.TabIndex = 14
        CLDLabel.Text = "CLD:"
        '
        'SerieLabel
        '
        SerieLabel.AutoSize = True
        SerieLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SerieLabel.Location = New System.Drawing.Point(108, 351)
        SerieLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        SerieLabel.Name = "SerieLabel"
        SerieLabel.Size = New System.Drawing.Size(52, 18)
        SerieLabel.TabIndex = 16
        SerieLabel.Text = "Serie:"
        '
        'NumeracionInicialLabel
        '
        NumeracionInicialLabel.AutoSize = True
        NumeracionInicialLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NumeracionInicialLabel.Location = New System.Drawing.Point(485, 65)
        NumeracionInicialLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NumeracionInicialLabel.Name = "NumeracionInicialLabel"
        NumeracionInicialLabel.Size = New System.Drawing.Size(152, 18)
        NumeracionInicialLabel.TabIndex = 18
        NumeracionInicialLabel.Text = "Numeracion Inicial:"
        '
        'NumeracionFinalLabel
        '
        NumeracionFinalLabel.AutoSize = True
        NumeracionFinalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NumeracionFinalLabel.Location = New System.Drawing.Point(495, 94)
        NumeracionFinalLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NumeracionFinalLabel.Name = "NumeracionFinalLabel"
        NumeracionFinalLabel.Size = New System.Drawing.Size(145, 18)
        NumeracionFinalLabel.TabIndex = 20
        NumeracionFinalLabel.Text = "Numeracion Final:"
        '
        'InicialLabel
        '
        InicialLabel.AutoSize = True
        InicialLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        InicialLabel.Location = New System.Drawing.Point(595, 126)
        InicialLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        InicialLabel.Name = "InicialLabel"
        InicialLabel.Size = New System.Drawing.Size(56, 18)
        InicialLabel.TabIndex = 22
        InicialLabel.Text = "Inicial:"
        '
        'FinalLabel
        '
        FinalLabel.AutoSize = True
        FinalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FinalLabel.Location = New System.Drawing.Point(604, 158)
        FinalLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        FinalLabel.Name = "FinalLabel"
        FinalLabel.Size = New System.Drawing.Size(49, 18)
        FinalLabel.TabIndex = 24
        FinalLabel.Text = "Final:"
        '
        'OcupacionLabel
        '
        OcupacionLabel.AutoSize = True
        OcupacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        OcupacionLabel.Location = New System.Drawing.Point(556, 193)
        OcupacionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        OcupacionLabel.Name = "OcupacionLabel"
        OcupacionLabel.Size = New System.Drawing.Size(94, 18)
        OcupacionLabel.TabIndex = 26
        OcupacionLabel.Text = "Ocupacion:"
        '
        'TipoDeRedLabel
        '
        TipoDeRedLabel.AutoSize = True
        TipoDeRedLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipoDeRedLabel.Location = New System.Drawing.Point(540, 225)
        TipoDeRedLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        TipoDeRedLabel.Name = "TipoDeRedLabel"
        TipoDeRedLabel.Size = New System.Drawing.Size(107, 18)
        TipoDeRedLabel.TabIndex = 28
        TipoDeRedLabel.Text = "Tipo De Red:"
        '
        'ModalidadLabel
        '
        ModalidadLabel.AutoSize = True
        ModalidadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ModalidadLabel.Location = New System.Drawing.Point(556, 257)
        ModalidadLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ModalidadLabel.Name = "ModalidadLabel"
        ModalidadLabel.Size = New System.Drawing.Size(90, 18)
        ModalidadLabel.TabIndex = 30
        ModalidadLabel.Text = "Modalidad:"
        '
        'EstatusLabel
        '
        EstatusLabel.AutoSize = True
        EstatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EstatusLabel.Location = New System.Drawing.Point(584, 289)
        EstatusLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        EstatusLabel.Name = "EstatusLabel"
        EstatusLabel.Size = New System.Drawing.Size(70, 18)
        EstatusLabel.TabIndex = 32
        EstatusLabel.Text = "Estatus:"
        '
        'RazonSocialLabel
        '
        RazonSocialLabel.AutoSize = True
        RazonSocialLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RazonSocialLabel.Location = New System.Drawing.Point(533, 321)
        RazonSocialLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        RazonSocialLabel.Name = "RazonSocialLabel"
        RazonSocialLabel.Size = New System.Drawing.Size(114, 18)
        RazonSocialLabel.TabIndex = 34
        RazonSocialLabel.Text = "Razon Social:"
        '
        'IdOperadorLabel
        '
        IdOperadorLabel.AutoSize = True
        IdOperadorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        IdOperadorLabel.Location = New System.Drawing.Point(545, 353)
        IdOperadorLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        IdOperadorLabel.Name = "IdOperadorLabel"
        IdOperadorLabel.Size = New System.Drawing.Size(102, 18)
        IdOperadorLabel.TabIndex = 36
        IdOperadorLabel.Text = "Id Operador:"
        '
        'MunicipioLabel
        '
        MunicipioLabel.AutoSize = True
        MunicipioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MunicipioLabel.Location = New System.Drawing.Point(69, 128)
        MunicipioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        MunicipioLabel.Name = "MunicipioLabel"
        MunicipioLabel.Size = New System.Drawing.Size(85, 18)
        MunicipioLabel.TabIndex = 38
        MunicipioLabel.Text = "Municipio:"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(847, 409)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(193, 41)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Consulta_CodigosMexicoBindingNavigator)
        Me.Panel1.Controls.Add(Clv_CodigoLabel)
        Me.Panel1.Controls.Add(Me.Clv_CodigoTextBox)
        Me.Panel1.Controls.Add(PoblacionLabel)
        Me.Panel1.Controls.Add(Me.PoblacionTextBox)
        Me.Panel1.Controls.Add(EstadoLabel)
        Me.Panel1.Controls.Add(Me.EstadoTextBox)
        Me.Panel1.Controls.Add(CentralLabel)
        Me.Panel1.Controls.Add(Me.CentralTextBox)
        Me.Panel1.Controls.Add(PreSuscripcionLabel)
        Me.Panel1.Controls.Add(Me.PreSuscripcionTextBox)
        Me.Panel1.Controls.Add(RegionLabel)
        Me.Panel1.Controls.Add(Me.RegionTextBox)
        Me.Panel1.Controls.Add(ASLLabel)
        Me.Panel1.Controls.Add(Me.ASLTextBox)
        Me.Panel1.Controls.Add(CLDLabel)
        Me.Panel1.Controls.Add(Me.CLDTextBox)
        Me.Panel1.Controls.Add(SerieLabel)
        Me.Panel1.Controls.Add(Me.SerieTextBox)
        Me.Panel1.Controls.Add(NumeracionInicialLabel)
        Me.Panel1.Controls.Add(Me.NumeracionInicialTextBox)
        Me.Panel1.Controls.Add(NumeracionFinalLabel)
        Me.Panel1.Controls.Add(Me.NumeracionFinalTextBox)
        Me.Panel1.Controls.Add(InicialLabel)
        Me.Panel1.Controls.Add(Me.InicialTextBox)
        Me.Panel1.Controls.Add(FinalLabel)
        Me.Panel1.Controls.Add(Me.FinalTextBox)
        Me.Panel1.Controls.Add(OcupacionLabel)
        Me.Panel1.Controls.Add(Me.OcupacionTextBox)
        Me.Panel1.Controls.Add(TipoDeRedLabel)
        Me.Panel1.Controls.Add(Me.TipoDeRedTextBox)
        Me.Panel1.Controls.Add(ModalidadLabel)
        Me.Panel1.Controls.Add(Me.ModalidadTextBox)
        Me.Panel1.Controls.Add(EstatusLabel)
        Me.Panel1.Controls.Add(Me.EstatusTextBox)
        Me.Panel1.Controls.Add(RazonSocialLabel)
        Me.Panel1.Controls.Add(Me.RazonSocialTextBox)
        Me.Panel1.Controls.Add(IdOperadorLabel)
        Me.Panel1.Controls.Add(Me.IdOperadorTextBox)
        Me.Panel1.Controls.Add(MunicipioLabel)
        Me.Panel1.Controls.Add(Me.MunicipioTextBox)
        Me.Panel1.Location = New System.Drawing.Point(0, 2)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1056, 399)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'Consulta_CodigosMexicoBindingNavigator
        '
        Me.Consulta_CodigosMexicoBindingNavigator.AddNewItem = Nothing
        Me.Consulta_CodigosMexicoBindingNavigator.BindingSource = Me.Consulta_CodigosMexicoBindingSource
        Me.Consulta_CodigosMexicoBindingNavigator.CountItem = Nothing
        Me.Consulta_CodigosMexicoBindingNavigator.DeleteItem = Nothing
        Me.Consulta_CodigosMexicoBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_CodigosMexicoBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.Consulta_CodigosMexicoBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_CodigosMexicoBindingNavigatorSaveItem})
        Me.Consulta_CodigosMexicoBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_CodigosMexicoBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_CodigosMexicoBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_CodigosMexicoBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_CodigosMexicoBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_CodigosMexicoBindingNavigator.Name = "Consulta_CodigosMexicoBindingNavigator"
        Me.Consulta_CodigosMexicoBindingNavigator.PositionItem = Nothing
        Me.Consulta_CodigosMexicoBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Consulta_CodigosMexicoBindingNavigator.Size = New System.Drawing.Size(1056, 27)
        Me.Consulta_CodigosMexicoBindingNavigator.TabIndex = 20
        Me.Consulta_CodigosMexicoBindingNavigator.TabStop = True
        Me.Consulta_CodigosMexicoBindingNavigator.Text = "BindingNavigator1"
        '
        'Consulta_CodigosMexicoBindingSource
        '
        Me.Consulta_CodigosMexicoBindingSource.DataMember = "consulta_CodigosMexico"
        Me.Consulta_CodigosMexicoBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(94, 24)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'Consulta_CodigosMexicoBindingNavigatorSaveItem
        '
        Me.Consulta_CodigosMexicoBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_CodigosMexicoBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_CodigosMexicoBindingNavigatorSaveItem.Name = "Consulta_CodigosMexicoBindingNavigatorSaveItem"
        Me.Consulta_CodigosMexicoBindingNavigatorSaveItem.Size = New System.Drawing.Size(138, 24)
        Me.Consulta_CodigosMexicoBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'Clv_CodigoTextBox
        '
        Me.Clv_CodigoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Clv_Codigo", True))
        Me.Clv_CodigoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CodigoTextBox.Location = New System.Drawing.Point(176, 57)
        Me.Clv_CodigoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_CodigoTextBox.Name = "Clv_CodigoTextBox"
        Me.Clv_CodigoTextBox.ReadOnly = True
        Me.Clv_CodigoTextBox.Size = New System.Drawing.Size(132, 24)
        Me.Clv_CodigoTextBox.TabIndex = 1
        Me.Clv_CodigoTextBox.TabStop = False
        '
        'PoblacionTextBox
        '
        Me.PoblacionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.PoblacionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Poblacion", True))
        Me.PoblacionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PoblacionTextBox.Location = New System.Drawing.Point(176, 89)
        Me.PoblacionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PoblacionTextBox.Name = "PoblacionTextBox"
        Me.PoblacionTextBox.Size = New System.Drawing.Size(288, 24)
        Me.PoblacionTextBox.TabIndex = 1
        '
        'EstadoTextBox
        '
        Me.EstadoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.EstadoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Estado", True))
        Me.EstadoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EstadoTextBox.Location = New System.Drawing.Point(176, 154)
        Me.EstadoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.EstadoTextBox.Name = "EstadoTextBox"
        Me.EstadoTextBox.Size = New System.Drawing.Size(288, 24)
        Me.EstadoTextBox.TabIndex = 3
        '
        'CentralTextBox
        '
        Me.CentralTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CentralTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Central", True))
        Me.CentralTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CentralTextBox.Location = New System.Drawing.Point(176, 186)
        Me.CentralTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CentralTextBox.Name = "CentralTextBox"
        Me.CentralTextBox.Size = New System.Drawing.Size(288, 24)
        Me.CentralTextBox.TabIndex = 4
        '
        'PreSuscripcionTextBox
        '
        Me.PreSuscripcionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.PreSuscripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "PreSuscripcion", True))
        Me.PreSuscripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PreSuscripcionTextBox.Location = New System.Drawing.Point(176, 218)
        Me.PreSuscripcionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PreSuscripcionTextBox.Name = "PreSuscripcionTextBox"
        Me.PreSuscripcionTextBox.Size = New System.Drawing.Size(288, 24)
        Me.PreSuscripcionTextBox.TabIndex = 5
        '
        'RegionTextBox
        '
        Me.RegionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RegionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Region", True))
        Me.RegionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RegionTextBox.Location = New System.Drawing.Point(176, 250)
        Me.RegionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RegionTextBox.Name = "RegionTextBox"
        Me.RegionTextBox.Size = New System.Drawing.Size(288, 24)
        Me.RegionTextBox.TabIndex = 6
        '
        'ASLTextBox
        '
        Me.ASLTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ASLTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "ASL", True))
        Me.ASLTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ASLTextBox.Location = New System.Drawing.Point(176, 282)
        Me.ASLTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ASLTextBox.Name = "ASLTextBox"
        Me.ASLTextBox.Size = New System.Drawing.Size(288, 24)
        Me.ASLTextBox.TabIndex = 7
        '
        'CLDTextBox
        '
        Me.CLDTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CLDTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "CLD", True))
        Me.CLDTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CLDTextBox.Location = New System.Drawing.Point(176, 314)
        Me.CLDTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CLDTextBox.Name = "CLDTextBox"
        Me.CLDTextBox.Size = New System.Drawing.Size(288, 24)
        Me.CLDTextBox.TabIndex = 8
        '
        'SerieTextBox
        '
        Me.SerieTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SerieTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Serie", True))
        Me.SerieTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieTextBox.Location = New System.Drawing.Point(176, 346)
        Me.SerieTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SerieTextBox.Name = "SerieTextBox"
        Me.SerieTextBox.Size = New System.Drawing.Size(288, 24)
        Me.SerieTextBox.TabIndex = 9
        '
        'NumeracionInicialTextBox
        '
        Me.NumeracionInicialTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NumeracionInicialTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "NumeracionInicial", True))
        Me.NumeracionInicialTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumeracionInicialTextBox.Location = New System.Drawing.Point(669, 58)
        Me.NumeracionInicialTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumeracionInicialTextBox.Name = "NumeracionInicialTextBox"
        Me.NumeracionInicialTextBox.Size = New System.Drawing.Size(288, 24)
        Me.NumeracionInicialTextBox.TabIndex = 10
        '
        'NumeracionFinalTextBox
        '
        Me.NumeracionFinalTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NumeracionFinalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "NumeracionFinal", True))
        Me.NumeracionFinalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumeracionFinalTextBox.Location = New System.Drawing.Point(669, 90)
        Me.NumeracionFinalTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumeracionFinalTextBox.Name = "NumeracionFinalTextBox"
        Me.NumeracionFinalTextBox.Size = New System.Drawing.Size(288, 24)
        Me.NumeracionFinalTextBox.TabIndex = 11
        '
        'InicialTextBox
        '
        Me.InicialTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.InicialTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Inicial", True))
        Me.InicialTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InicialTextBox.Location = New System.Drawing.Point(669, 122)
        Me.InicialTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.InicialTextBox.Name = "InicialTextBox"
        Me.InicialTextBox.Size = New System.Drawing.Size(288, 24)
        Me.InicialTextBox.TabIndex = 12
        '
        'FinalTextBox
        '
        Me.FinalTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FinalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Final", True))
        Me.FinalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FinalTextBox.Location = New System.Drawing.Point(669, 154)
        Me.FinalTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FinalTextBox.Name = "FinalTextBox"
        Me.FinalTextBox.Size = New System.Drawing.Size(288, 24)
        Me.FinalTextBox.TabIndex = 13
        '
        'OcupacionTextBox
        '
        Me.OcupacionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcupacionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Ocupacion", True))
        Me.OcupacionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OcupacionTextBox.Location = New System.Drawing.Point(669, 186)
        Me.OcupacionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.OcupacionTextBox.Name = "OcupacionTextBox"
        Me.OcupacionTextBox.Size = New System.Drawing.Size(288, 24)
        Me.OcupacionTextBox.TabIndex = 14
        '
        'TipoDeRedTextBox
        '
        Me.TipoDeRedTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TipoDeRedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "TipoDeRed", True))
        Me.TipoDeRedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoDeRedTextBox.Location = New System.Drawing.Point(669, 218)
        Me.TipoDeRedTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TipoDeRedTextBox.Name = "TipoDeRedTextBox"
        Me.TipoDeRedTextBox.Size = New System.Drawing.Size(288, 24)
        Me.TipoDeRedTextBox.TabIndex = 15
        '
        'ModalidadTextBox
        '
        Me.ModalidadTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ModalidadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Modalidad", True))
        Me.ModalidadTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModalidadTextBox.Location = New System.Drawing.Point(669, 250)
        Me.ModalidadTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ModalidadTextBox.Name = "ModalidadTextBox"
        Me.ModalidadTextBox.Size = New System.Drawing.Size(288, 24)
        Me.ModalidadTextBox.TabIndex = 16
        '
        'EstatusTextBox
        '
        Me.EstatusTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.EstatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Estatus", True))
        Me.EstatusTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EstatusTextBox.Location = New System.Drawing.Point(669, 282)
        Me.EstatusTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.EstatusTextBox.MaxLength = 1
        Me.EstatusTextBox.Name = "EstatusTextBox"
        Me.EstatusTextBox.Size = New System.Drawing.Size(288, 24)
        Me.EstatusTextBox.TabIndex = 17
        '
        'RazonSocialTextBox
        '
        Me.RazonSocialTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RazonSocialTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "RazonSocial", True))
        Me.RazonSocialTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RazonSocialTextBox.Location = New System.Drawing.Point(669, 314)
        Me.RazonSocialTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RazonSocialTextBox.Name = "RazonSocialTextBox"
        Me.RazonSocialTextBox.Size = New System.Drawing.Size(288, 24)
        Me.RazonSocialTextBox.TabIndex = 18
        '
        'IdOperadorTextBox
        '
        Me.IdOperadorTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.IdOperadorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "IdOperador", True))
        Me.IdOperadorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdOperadorTextBox.Location = New System.Drawing.Point(669, 346)
        Me.IdOperadorTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.IdOperadorTextBox.Name = "IdOperadorTextBox"
        Me.IdOperadorTextBox.Size = New System.Drawing.Size(288, 24)
        Me.IdOperadorTextBox.TabIndex = 19
        '
        'MunicipioTextBox
        '
        Me.MunicipioTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.MunicipioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_CodigosMexicoBindingSource, "Municipio", True))
        Me.MunicipioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MunicipioTextBox.Location = New System.Drawing.Point(176, 121)
        Me.MunicipioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MunicipioTextBox.Name = "MunicipioTextBox"
        Me.MunicipioTextBox.Size = New System.Drawing.Size(288, 24)
        Me.MunicipioTextBox.TabIndex = 2
        '
        'Consulta_CodigosMexicoTableAdapter
        '
        Me.Consulta_CodigosMexicoTableAdapter.ClearBeforeFill = True
        '
        'FrmCodigosMexico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1056, 455)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmCodigosMexico"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogos de Códigos de México"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Consulta_CodigosMexicoBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_CodigosMexicoBindingNavigator.ResumeLayout(False)
        Me.Consulta_CodigosMexicoBindingNavigator.PerformLayout()
        CType(Me.Consulta_CodigosMexicoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents Consulta_CodigosMexicoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_CodigosMexicoTableAdapter As sofTV.DataSetLidia2TableAdapters.consulta_CodigosMexicoTableAdapter
    Friend WithEvents Clv_CodigoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PoblacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EstadoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CentralTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PreSuscripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RegionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ASLTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CLDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SerieTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeracionInicialTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeracionFinalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents InicialTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FinalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents OcupacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TipoDeRedTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ModalidadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EstatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RazonSocialTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IdOperadorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MunicipioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Consulta_CodigosMexicoBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_CodigosMexicoBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
End Class
