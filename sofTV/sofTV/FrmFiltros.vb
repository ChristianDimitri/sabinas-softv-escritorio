﻿Imports System.Data.SqlClient
Public Class FrmFiltros
    Dim NumDistri, NumPlaza, NumEstado, NumMunicipio As Integer
    Private Sub FrmFiltros_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        contarAccesos()
        habiliarFiltros()

    End Sub

    Private Sub contarAccesos()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
        BaseII.CreateMyParameter("@NumDistri", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@NumPlaza", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@NumEstado", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@NumMunicipio", ParameterDirection.Output, SqlDbType.Int)

        BaseII.ProcedimientoOutPut("AccesoFiltros")
        NumDistri = BaseII.dicoPar("@NumDistri").ToString()
        NumPlaza = BaseII.dicoPar("@NumPlaza").ToString()
        NumEstado = BaseII.dicoPar("@NumEstado").ToString()
        NumMunicipio = BaseII.dicoPar("@NumMunicipio").ToString()
    End Sub
    Private Sub habiliarFiltros()
        If NumDistri = 1 Then
            CheckBoxDistri.Enabled = False
            CheckBoxDistri.Checked = True
        End If
        If NumPlaza = 1 Then
            CheckBoxPlaza.Enabled = False
            CheckBoxPlaza.Checked = True
        End If
        If NumEstado = 1 Then
            CheckBoxEstado.Enabled = False
            CheckBoxEstado.Checked = True
        End If
        If NumMunicipio = 1 Then
            CheckBoxMun.Enabled = False
            CheckBoxMun.Checked = True
        End If
    End Sub

    Private Sub Button34_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button34.Click
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim comando As New SqlCommand()
        comando.Connection = conexion
        comando.CommandText = "exec DameIdentificador"
        identificador = comando.ExecuteScalar()
        conexion.Close()
        If CheckBoxDistri.Checked And CheckBoxDistri.Enabled = True Then
            'Abre el filtro
            FrmSelPlaza.ShowDialog()
        ElseIf CheckBoxDistri.Checked = False And CheckBoxDistri.Enabled = True Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@clvusuario", SqlDbType.Int, identificador)
            BaseII.Inserta("InsertAutoDistribuidor")
        End If
        If CheckBoxPlaza.Checked And CheckBoxPlaza.Enabled = True Then
            'Abre el filtro
            FrmSelCompaniaCartera.ShowDialog()
        ElseIf CheckBoxPlaza.Checked = False And CheckBoxPlaza.Enabled = True Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@clvusuario", SqlDbType.Int, identificador)
            BaseII.Inserta("InsertAutoPlaza")
        End If
        If CheckBoxEstado.Checked And CheckBoxEstado.Enabled = True Then
            'Abre el filtro
            FrmSelEstadosFil.ShowDialog()
        ElseIf CheckBoxEstado.Checked = False And CheckBoxEstado.Enabled = True Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@clvusuario", SqlDbType.Int, identificador)
            BaseII.Inserta("InsertAutoEstado")
        End If
        If CheckBoxMun.Checked And CheckBoxMun.Enabled = True Then
            'Abre el filtro
            FrmSelCiudadJ.ShowDialog()
        ElseIf CheckBoxMun.Checked = False And CheckBoxMun.Enabled = True Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@clvusuario", SqlDbType.Int, identificador)
            BaseII.Inserta("InsertAutoCiudad")
        End If
        If CheckBoxLoc.Checked And CheckBoxLoc.Enabled = True Then
            'Abre el filtro
            'Pendiente
        ElseIf CheckBoxLoc.Checked = False And CheckBoxLoc.Enabled = True Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.Inserta("InsertAutoLocalidad")
        End If
        If CheckBoxCol.Checked And CheckBoxCol.Enabled = True Then
            'Abre el filtro
            FrmSelColoniaJ.ShowDialog()
        ElseIf CheckBoxCol.Checked = False And CheckBoxCol.Enabled = True Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.Inserta("InsertAutoColonia")
        End If
        If CheckBoxCal.Checked And CheckBoxCal.Enabled = True Then
            'Abre el filtro
            FrmSelCalleJ.ShowDialog()
        ElseIf CheckBoxCal.Checked = False And CheckBoxCal.Enabled = True Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.Inserta("InsertAutoCalle")
        End If
        If CheckBoxServicio.Checked And CheckBoxServicio.Enabled = True Then
            'Abre el filtro
            FrmServicios.ShowDialog()
        ElseIf CheckBoxServicio.Checked = False And CheckBoxServicio.Enabled = True Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.Inserta("InsertAutoServicio")
        End If
    End Sub
End Class