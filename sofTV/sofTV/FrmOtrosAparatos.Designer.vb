﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmOtrosAparatos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label9 As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Dim Label31 As System.Windows.Forms.Label
        Dim Label34 As System.Windows.Forms.Label
        Dim Label35 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmOtrosAparatos))
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Me.TBMarcaAparato = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.CMBLabelAparato = New System.Windows.Forms.Label()
        Me.CMBLabel53 = New System.Windows.Forms.Label()
        Me.LblMac = New System.Windows.Forms.TextBox()
        Me.BindingNavigator4 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton12 = New System.Windows.Forms.ToolStripButton()
        Me.TBObs = New System.Windows.Forms.TextBox()
        Me.CMBoxStatus = New System.Windows.Forms.ComboBox()
        Me.TBBaja = New System.Windows.Forms.TextBox()
        Me.CBSeRenta = New System.Windows.Forms.CheckBox()
        Me.CMBTextBox28 = New System.Windows.Forms.TextBox()
        Me.TBSuspension = New System.Windows.Forms.TextBox()
        Me.TBInstalacion = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ButtonMap = New System.Windows.Forms.Button()
        Me.PanelUbicacion = New System.Windows.Forms.Panel()
        Me.TbLongitud = New System.Windows.Forms.TextBox()
        Me.TbLatitud = New System.Windows.Forms.TextBox()
        Label9 = New System.Windows.Forms.Label()
        Label30 = New System.Windows.Forms.Label()
        Label31 = New System.Windows.Forms.Label()
        Label34 = New System.Windows.Forms.Label()
        Label35 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        CType(Me.BindingNavigator4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.PanelUbicacion.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(397, 140)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(40, 15)
        Label9.TabIndex = 88
        Label9.Text = "Baja:"
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.ForeColor = System.Drawing.Color.LightSlateGray
        Label30.Location = New System.Drawing.Point(425, 170)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(74, 15)
        Label30.TabIndex = 81
        Label30.Text = "Se Renta :"
        '
        'Label31
        '
        Label31.AutoSize = True
        Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label31.ForeColor = System.Drawing.Color.LightSlateGray
        Label31.Location = New System.Drawing.Point(14, 100)
        Label31.Name = "Label31"
        Label31.Size = New System.Drawing.Size(109, 15)
        Label31.TabIndex = 78
        Label31.Text = "Observaciones :"
        '
        'Label34
        '
        Label34.AutoSize = True
        Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label34.ForeColor = System.Drawing.Color.LightSlateGray
        Label34.Location = New System.Drawing.Point(351, 113)
        Label34.Name = "Label34"
        Label34.Size = New System.Drawing.Size(86, 15)
        Label34.TabIndex = 75
        Label34.Text = "Suspención:"
        '
        'Label35
        '
        Label35.AutoSize = True
        Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label35.ForeColor = System.Drawing.Color.LightSlateGray
        Label35.Location = New System.Drawing.Point(362, 87)
        Label35.Name = "Label35"
        Label35.Size = New System.Drawing.Size(75, 15)
        Label35.TabIndex = 73
        Label35.Text = "Activación:"
        '
        'TBMarcaAparato
        '
        Me.TBMarcaAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBMarcaAparato.Location = New System.Drawing.Point(140, 76)
        Me.TBMarcaAparato.Name = "TBMarcaAparato"
        Me.TBMarcaAparato.ReadOnly = True
        Me.TBMarcaAparato.Size = New System.Drawing.Size(191, 21)
        Me.TBMarcaAparato.TabIndex = 95
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label56.Location = New System.Drawing.Point(10, 77)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(116, 15)
        Me.Label56.TabIndex = 94
        Me.Label56.Text = "Modelo Aparato :"
        '
        'CMBLabelAparato
        '
        Me.CMBLabelAparato.AutoSize = True
        Me.CMBLabelAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabelAparato.ForeColor = System.Drawing.Color.OrangeRed
        Me.CMBLabelAparato.Location = New System.Drawing.Point(12, 17)
        Me.CMBLabelAparato.Name = "CMBLabelAparato"
        Me.CMBLabelAparato.Size = New System.Drawing.Size(126, 18)
        Me.CMBLabelAparato.TabIndex = 91
        Me.CMBLabelAparato.Text = "Datos Aparato :"
        Me.CMBLabelAparato.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBLabel53
        '
        Me.CMBLabel53.AutoSize = True
        Me.CMBLabel53.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel53.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBLabel53.Location = New System.Drawing.Point(13, 49)
        Me.CMBLabel53.Name = "CMBLabel53"
        Me.CMBLabel53.Size = New System.Drawing.Size(108, 15)
        Me.CMBLabel53.TabIndex = 90
        Me.CMBLabel53.Text = "Status Aparato :"
        '
        'LblMac
        '
        Me.LblMac.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LblMac.CausesValidation = False
        Me.LblMac.Enabled = False
        Me.LblMac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMac.Location = New System.Drawing.Point(163, 17)
        Me.LblMac.Multiline = True
        Me.LblMac.Name = "LblMac"
        Me.LblMac.ReadOnly = True
        Me.LblMac.Size = New System.Drawing.Size(231, 20)
        Me.LblMac.TabIndex = 89
        '
        'BindingNavigator4
        '
        Me.BindingNavigator4.AddNewItem = Nothing
        Me.BindingNavigator4.CountItem = Nothing
        Me.BindingNavigator4.DeleteItem = Nothing
        Me.BindingNavigator4.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator9, Me.ToolStripSeparator10, Me.ToolStripButton12})
        Me.BindingNavigator4.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator4.MoveFirstItem = Nothing
        Me.BindingNavigator4.MoveLastItem = Nothing
        Me.BindingNavigator4.MoveNextItem = Nothing
        Me.BindingNavigator4.MovePreviousItem = Nothing
        Me.BindingNavigator4.Name = "BindingNavigator4"
        Me.BindingNavigator4.PositionItem = Nothing
        Me.BindingNavigator4.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BindingNavigator4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator4.Size = New System.Drawing.Size(555, 25)
        Me.BindingNavigator4.TabIndex = 80
        Me.BindingNavigator4.TabStop = True
        Me.BindingNavigator4.Text = "BindingNavigator4"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton12
        '
        Me.ToolStripButton12.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton12.Image = CType(resources.GetObject("ToolStripButton12.Image"), System.Drawing.Image)
        Me.ToolStripButton12.Name = "ToolStripButton12"
        Me.ToolStripButton12.Size = New System.Drawing.Size(88, 22)
        Me.ToolStripButton12.Text = "GUA&RDAR"
        '
        'TBObs
        '
        Me.TBObs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBObs.Location = New System.Drawing.Point(20, 118)
        Me.TBObs.MaxLength = 250
        Me.TBObs.Multiline = True
        Me.TBObs.Name = "TBObs"
        Me.TBObs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TBObs.Size = New System.Drawing.Size(304, 161)
        Me.TBObs.TabIndex = 79
        '
        'CMBoxStatus
        '
        Me.CMBoxStatus.Enabled = False
        Me.CMBoxStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CMBoxStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBoxStatus.FormattingEnabled = True
        Me.CMBoxStatus.Location = New System.Drawing.Point(143, 45)
        Me.CMBoxStatus.Name = "CMBoxStatus"
        Me.CMBoxStatus.Size = New System.Drawing.Size(145, 23)
        Me.CMBoxStatus.TabIndex = 84
        Me.CMBoxStatus.TabStop = False
        '
        'TBBaja
        '
        Me.TBBaja.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBBaja.CausesValidation = False
        Me.TBBaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBBaja.Location = New System.Drawing.Point(443, 138)
        Me.TBBaja.Name = "TBBaja"
        Me.TBBaja.ReadOnly = True
        Me.TBBaja.Size = New System.Drawing.Size(100, 21)
        Me.TBBaja.TabIndex = 87
        Me.TBBaja.TabStop = False
        '
        'CBSeRenta
        '
        Me.CBSeRenta.Location = New System.Drawing.Point(505, 167)
        Me.CBSeRenta.Name = "CBSeRenta"
        Me.CBSeRenta.Size = New System.Drawing.Size(16, 24)
        Me.CBSeRenta.TabIndex = 83
        Me.CBSeRenta.TabStop = False
        '
        'CMBTextBox28
        '
        Me.CMBTextBox28.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox28.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox28.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox28.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox28.Location = New System.Drawing.Point(358, 53)
        Me.CMBTextBox28.Name = "CMBTextBox28"
        Me.CMBTextBox28.ReadOnly = True
        Me.CMBTextBox28.Size = New System.Drawing.Size(189, 19)
        Me.CMBTextBox28.TabIndex = 82
        Me.CMBTextBox28.TabStop = False
        Me.CMBTextBox28.Text = "Fechas de "
        Me.CMBTextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TBSuspension
        '
        Me.TBSuspension.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBSuspension.CausesValidation = False
        Me.TBSuspension.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBSuspension.Location = New System.Drawing.Point(443, 111)
        Me.TBSuspension.Name = "TBSuspension"
        Me.TBSuspension.ReadOnly = True
        Me.TBSuspension.Size = New System.Drawing.Size(100, 21)
        Me.TBSuspension.TabIndex = 76
        Me.TBSuspension.TabStop = False
        '
        'TBInstalacion
        '
        Me.TBInstalacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBInstalacion.CausesValidation = False
        Me.TBInstalacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBInstalacion.Location = New System.Drawing.Point(443, 85)
        Me.TBInstalacion.Name = "TBInstalacion"
        Me.TBInstalacion.ReadOnly = True
        Me.TBInstalacion.Size = New System.Drawing.Size(100, 21)
        Me.TBInstalacion.TabIndex = 74
        Me.TBInstalacion.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.PanelUbicacion)
        Me.Panel1.Controls.Add(Me.TBObs)
        Me.Panel1.Controls.Add(Me.TBMarcaAparato)
        Me.Panel1.Controls.Add(Label9)
        Me.Panel1.Controls.Add(Label31)
        Me.Panel1.Controls.Add(Me.TBBaja)
        Me.Panel1.Controls.Add(Me.Label56)
        Me.Panel1.Controls.Add(Me.CMBTextBox28)
        Me.Panel1.Controls.Add(Label34)
        Me.Panel1.Controls.Add(Label30)
        Me.Panel1.Controls.Add(Me.TBSuspension)
        Me.Panel1.Controls.Add(Me.CBSeRenta)
        Me.Panel1.Controls.Add(Label35)
        Me.Panel1.Controls.Add(Me.CMBoxStatus)
        Me.Panel1.Controls.Add(Me.TBInstalacion)
        Me.Panel1.Controls.Add(Me.CMBLabelAparato)
        Me.Panel1.Controls.Add(Me.LblMac)
        Me.Panel1.Controls.Add(Me.CMBLabel53)
        Me.Panel1.Location = New System.Drawing.Point(0, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(548, 297)
        Me.Panel1.TabIndex = 96
        '
        'ButtonMap
        '
        Me.ButtonMap.BackColor = System.Drawing.Color.DarkOrange
        Me.ButtonMap.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonMap.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonMap.ForeColor = System.Drawing.Color.White
        Me.ButtonMap.Location = New System.Drawing.Point(45, 71)
        Me.ButtonMap.Name = "ButtonMap"
        Me.ButtonMap.Size = New System.Drawing.Size(99, 26)
        Me.ButtonMap.TabIndex = 105
        Me.ButtonMap.Text = "Ver mapa"
        Me.ButtonMap.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(12, 42)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(67, 15)
        Label1.TabIndex = 108
        Label1.Text = "Longitud:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(23, 17)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(55, 15)
        Label2.TabIndex = 107
        Label2.Text = "Latitud:"
        '
        'PanelUbicacion
        '
        Me.PanelUbicacion.Controls.Add(Me.TbLongitud)
        Me.PanelUbicacion.Controls.Add(Me.TbLatitud)
        Me.PanelUbicacion.Controls.Add(Me.ButtonMap)
        Me.PanelUbicacion.Controls.Add(Label1)
        Me.PanelUbicacion.Controls.Add(Label2)
        Me.PanelUbicacion.Location = New System.Drawing.Point(358, 194)
        Me.PanelUbicacion.Name = "PanelUbicacion"
        Me.PanelUbicacion.Size = New System.Drawing.Size(191, 100)
        Me.PanelUbicacion.TabIndex = 109
        '
        'TbLongitud
        '
        Me.TbLongitud.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TbLongitud.CausesValidation = False
        Me.TbLongitud.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbLongitud.Location = New System.Drawing.Point(84, 39)
        Me.TbLongitud.Name = "TbLongitud"
        Me.TbLongitud.ReadOnly = True
        Me.TbLongitud.Size = New System.Drawing.Size(100, 21)
        Me.TbLongitud.TabIndex = 110
        Me.TbLongitud.TabStop = False
        '
        'TbLatitud
        '
        Me.TbLatitud.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TbLatitud.CausesValidation = False
        Me.TbLatitud.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbLatitud.Location = New System.Drawing.Point(84, 13)
        Me.TbLatitud.Name = "TbLatitud"
        Me.TbLatitud.ReadOnly = True
        Me.TbLatitud.Size = New System.Drawing.Size(100, 21)
        Me.TbLatitud.TabIndex = 109
        Me.TbLatitud.TabStop = False
        '
        'FrmOtrosAparatos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(555, 337)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.BindingNavigator4)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Location = New System.Drawing.Point(445, 330)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmOtrosAparatos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "FrmOtrosAparatos"
        CType(Me.BindingNavigator4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator4.ResumeLayout(False)
        Me.BindingNavigator4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.PanelUbicacion.ResumeLayout(False)
        Me.PanelUbicacion.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TBMarcaAparato As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents CMBLabelAparato As System.Windows.Forms.Label
    Friend WithEvents CMBLabel53 As System.Windows.Forms.Label
    Friend WithEvents LblMac As System.Windows.Forms.TextBox
    Friend WithEvents BindingNavigator4 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton12 As System.Windows.Forms.ToolStripButton
    Friend WithEvents TBObs As System.Windows.Forms.TextBox
    Friend WithEvents CMBoxStatus As System.Windows.Forms.ComboBox
    Friend WithEvents TBBaja As System.Windows.Forms.TextBox
    Friend WithEvents CBSeRenta As System.Windows.Forms.CheckBox
    Friend WithEvents CMBTextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents TBSuspension As System.Windows.Forms.TextBox
    Friend WithEvents TBInstalacion As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ButtonMap As System.Windows.Forms.Button
    Friend WithEvents PanelUbicacion As System.Windows.Forms.Panel
    Friend WithEvents TbLongitud As System.Windows.Forms.TextBox
    Friend WithEvents TbLatitud As System.Windows.Forms.TextBox
End Class
