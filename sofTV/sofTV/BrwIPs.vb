﻿Public Class BrwIPs

    Dim Id As Long = 0
    Dim opcion As Integer = 0

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmRedes.Show()
    End Sub

    Private Sub BrwRedes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        If origenForm = "IpRed" Then

            opcion = 1
            Button2.Enabled = False
            Button6.Enabled = False

            If OpcAccion = "C" Then
                Button6.Enabled = True
            ElseIf OpcAccion = "M" Then
                Button2.Enabled = True
            ElseIf OpcAccion = "N" Then
                Button2.Enabled = True
                Button6.Enabled = True
            End If

        ElseIf origenForm = "menu" Then
            iD_redSelec = 0
            Button2.Enabled = True
            Button6.Enabled = True
        End If

        llenaGrid()

    End Sub


    Public Sub llenaGrid()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdRed", SqlDbType.BigInt, iD_redSelec) '0
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, opcion)
        BaseII.CreateMyParameter("@IdIP", SqlDbType.BigInt, 0) '0
        dgvIps.DataSource = BaseII.ConsultaDT("GetList_CatalogoIP")

    End Sub


    Public Sub llenaGrid_idIP()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdRed", SqlDbType.BigInt, iD_redSelec) '0
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@IdIP", SqlDbType.BigInt, 0) '0
        dgvIps.DataSource = BaseII.ConsultaDT("GetList_CatalogoIP")

    End Sub

    Private Sub BuscaRedBtn_Click(sender As Object, e As EventArgs) Handles BuscaRedBtn.Click
        'Dim red As String
        'red = buscaRedTxt.Text
        '  buscarIpEnCatalogo(red)

        buscarIpEnCatalogo()

    End Sub


    Public Sub buscarIpEnCatalogo()

        Dim red As String
        red = buscaRedTxt.Text

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdRed", SqlDbType.BigInt, iD_redSelec) '0
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, opcion) '0
        BaseII.CreateMyParameter("@IpRed", SqlDbType.VarChar, red.ToString())

        dgvIps.DataSource = BaseII.ConsultaDT("BuscarIp_CatalogoIP")
        buscaRedTxt.Text = ""

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click

        OpcAccion = "C"
        seleccionarIp()

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click

        OpcAccion = "M"
        seleccionarIp()

    End Sub

    Private Sub seleccionarIp()

        Dim rowActual As Long
        '  iD_redSelec = 0
        If dgvIps.SelectedRows.Count = 1 Then
            rowActual = dgvIps.SelectedRows(0).Index
        End If

        If dgvIps.SelectedRows.Count = 1 Then
            Id = dgvIps.Rows(rowActual).Cells(0).Value
            iD_ip = dgvIps.Rows(rowActual).Cells(0).Value '   iD_redSelec

            FrmIPs.Show()
        Else
            MessageBox.Show("Seleccione una IP")
        End If

    End Sub


    Private Sub buscaRedTxt_TextChanged(sender As Object, e As EventArgs) Handles buscaRedTxt.TextChanged

    End Sub

    Private Sub buscaRedTxt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles buscaRedTxt.KeyPress

        If Asc(e.KeyChar) = 13 Then
            buscarIpEnCatalogo()
        End If

    End Sub



End Class