<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCamServCte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim Fecha_SolLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Fecha_SolLabel1 = New System.Windows.Forms.Label()
        Me.ConCambioServClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.StatusLabel2 = New System.Windows.Forms.Label()
        Me.Servicio_ActualLabel2 = New System.Windows.Forms.Label()
        Me.Servicio_AnteriorLabel2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ConceptoLabel2 = New System.Windows.Forms.Label()
        Me.ContratoLabel2 = New System.Windows.Forms.Label()
        Me.NombreLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ConCambioServClienteDataGridView = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ComboBoxCiudad = New System.Windows.Forms.ComboBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.RealizarTextBox = New System.Windows.Forms.TextBox()
        Me.ConCambioServClienteTableAdapter = New sofTV.DataSetEricTableAdapters.ConCambioServClienteTableAdapter()
        Me.BorCambioServClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorCambioServClienteTableAdapter = New sofTV.DataSetEricTableAdapters.BorCambioServClienteTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        ContratoLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        ConceptoLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        Fecha_SolLabel = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ConCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConCambioServClienteDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.Location = New System.Drawing.Point(7, 42)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(65, 15)
        ContratoLabel.TabIndex = 13
        ContratoLabel.Text = "Contrato:"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.Location = New System.Drawing.Point(7, 86)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(62, 15)
        NombreLabel.TabIndex = 14
        NombreLabel.Text = "Nombre:"
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConceptoLabel.Location = New System.Drawing.Point(7, 139)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(172, 15)
        ConceptoLabel.TabIndex = 15
        ConceptoLabel.Text = "Tipo de Servicio Anterior :"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel.Location = New System.Drawing.Point(7, 350)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(67, 15)
        StatusLabel.TabIndex = 18
        StatusLabel.Text = "Proceso :"
        '
        'Fecha_SolLabel
        '
        Fecha_SolLabel.AutoSize = True
        Fecha_SolLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_SolLabel.Location = New System.Drawing.Point(7, 409)
        Fecha_SolLabel.Name = "Fecha_SolLabel"
        Fecha_SolLabel.Size = New System.Drawing.Size(134, 15)
        Fecha_SolLabel.TabIndex = 12
        Fecha_SolLabel.Text = "Fecha de Solicitud :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(-8, -3)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBoxCompanias)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel2.Controls.Add(Me.ConCambioServClienteDataGridView)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ComboBoxCiudad)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button2)
        Me.SplitContainer1.Size = New System.Drawing.Size(872, 712)
        Me.SplitContainer1.SplitterDistance = 273
        Me.SplitContainer1.TabIndex = 0
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(28, 51)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(239, 24)
        Me.ComboBoxCompanias.TabIndex = 99
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(25, 32)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(47, 16)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Plaza"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(25, 163)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 16)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Nombre"
        '
        'Button6
        '
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(28, 210)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(80, 27)
        Me.Button6.TabIndex = 11
        Me.Button6.Text = "Buscar"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(28, 182)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(214, 22)
        Me.TextBox2.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Contrato"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Fecha_SolLabel1)
        Me.Panel1.Controls.Add(Fecha_SolLabel)
        Me.Panel1.Controls.Add(StatusLabel)
        Me.Panel1.Controls.Add(Me.StatusLabel2)
        Me.Panel1.Controls.Add(Me.Servicio_ActualLabel2)
        Me.Panel1.Controls.Add(ConceptoLabel)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.Servicio_AnteriorLabel2)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.ConceptoLabel2)
        Me.Panel1.Controls.Add(Me.ContratoLabel2)
        Me.Panel1.Controls.Add(Me.NombreLabel2)
        Me.Panel1.Location = New System.Drawing.Point(17, 253)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(255, 458)
        Me.Panel1.TabIndex = 8
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(21, 257)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(12, 16)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(10, 231)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(161, 15)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Tipo de Servicio Actual :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(10, 186)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 15)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Servicio Anterior :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(10, 280)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 15)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Servicio Actual :"
        '
        'Fecha_SolLabel1
        '
        Me.Fecha_SolLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Fecha_Sol", True))
        Me.Fecha_SolLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_SolLabel1.ForeColor = System.Drawing.Color.White
        Me.Fecha_SolLabel1.Location = New System.Drawing.Point(10, 424)
        Me.Fecha_SolLabel1.Name = "Fecha_SolLabel1"
        Me.Fecha_SolLabel1.Size = New System.Drawing.Size(240, 23)
        Me.Fecha_SolLabel1.TabIndex = 13
        '
        'ConCambioServClienteBindingSource
        '
        Me.ConCambioServClienteBindingSource.DataMember = "ConCambioServCliente"
        Me.ConCambioServClienteBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.EnforceConstraints = False
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'StatusLabel2
        '
        Me.StatusLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Status", True))
        Me.StatusLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusLabel2.ForeColor = System.Drawing.Color.White
        Me.StatusLabel2.Location = New System.Drawing.Point(10, 370)
        Me.StatusLabel2.Name = "StatusLabel2"
        Me.StatusLabel2.Size = New System.Drawing.Size(240, 23)
        Me.StatusLabel2.TabIndex = 12
        '
        'Servicio_ActualLabel2
        '
        Me.Servicio_ActualLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Servicio_Actual", True))
        Me.Servicio_ActualLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Servicio_ActualLabel2.ForeColor = System.Drawing.Color.White
        Me.Servicio_ActualLabel2.Location = New System.Drawing.Point(13, 295)
        Me.Servicio_ActualLabel2.Name = "Servicio_ActualLabel2"
        Me.Servicio_ActualLabel2.Size = New System.Drawing.Size(240, 23)
        Me.Servicio_ActualLabel2.TabIndex = 10
        '
        'Servicio_AnteriorLabel2
        '
        Me.Servicio_AnteriorLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Servicio_Anterior", True))
        Me.Servicio_AnteriorLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Servicio_AnteriorLabel2.ForeColor = System.Drawing.Color.White
        Me.Servicio_AnteriorLabel2.Location = New System.Drawing.Point(10, 201)
        Me.Servicio_AnteriorLabel2.Name = "Servicio_AnteriorLabel2"
        Me.Servicio_AnteriorLabel2.Size = New System.Drawing.Size(240, 23)
        Me.Servicio_AnteriorLabel2.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(13, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(147, 20)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Datos del Cliente"
        '
        'ConceptoLabel2
        '
        Me.ConceptoLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Concepto", True))
        Me.ConceptoLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoLabel2.ForeColor = System.Drawing.Color.White
        Me.ConceptoLabel2.Location = New System.Drawing.Point(10, 154)
        Me.ConceptoLabel2.Name = "ConceptoLabel2"
        Me.ConceptoLabel2.Size = New System.Drawing.Size(240, 23)
        Me.ConceptoLabel2.TabIndex = 6
        '
        'ContratoLabel2
        '
        Me.ContratoLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Contrato", True))
        Me.ContratoLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoLabel2.ForeColor = System.Drawing.Color.White
        Me.ContratoLabel2.Location = New System.Drawing.Point(10, 62)
        Me.ContratoLabel2.Name = "ContratoLabel2"
        Me.ContratoLabel2.Size = New System.Drawing.Size(100, 23)
        Me.ContratoLabel2.TabIndex = 2
        '
        'NombreLabel2
        '
        Me.NombreLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Nombre", True))
        Me.NombreLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreLabel2.ForeColor = System.Drawing.Color.White
        Me.NombreLabel2.Location = New System.Drawing.Point(10, 101)
        Me.NombreLabel2.Name = "NombreLabel2"
        Me.NombreLabel2.Size = New System.Drawing.Size(240, 38)
        Me.NombreLabel2.TabIndex = 4
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.CMBLabel1.Location = New System.Drawing.Point(37, 6)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(168, 20)
        Me.CMBLabel1.TabIndex = 2
        Me.CMBLabel1.Text = "Buscar Cliente Por :"
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(28, 127)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(80, 27)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "Buscar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(28, 99)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(142, 22)
        Me.TextBox1.TabIndex = 3
        '
        'ConCambioServClienteDataGridView
        '
        Me.ConCambioServClienteDataGridView.AllowUserToAddRows = False
        Me.ConCambioServClienteDataGridView.AllowUserToDeleteRows = False
        Me.ConCambioServClienteDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConCambioServClienteDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.ConCambioServClienteDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11, Me.Column12, Me.Column13, Me.Column14, Me.Column15, Me.Column16})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConCambioServClienteDataGridView.DefaultCellStyle = DataGridViewCellStyle4
        Me.ConCambioServClienteDataGridView.GridColor = System.Drawing.Color.WhiteSmoke
        Me.ConCambioServClienteDataGridView.Location = New System.Drawing.Point(0, 3)
        Me.ConCambioServClienteDataGridView.Name = "ConCambioServClienteDataGridView"
        Me.ConCambioServClienteDataGridView.ReadOnly = True
        Me.ConCambioServClienteDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConCambioServClienteDataGridView.Size = New System.Drawing.Size(591, 709)
        Me.ConCambioServClienteDataGridView.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "Clave"
        Me.Column1.HeaderText = "Clave"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "NumContrato"
        Me.Column2.HeaderText = "Contrato"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "ContratoNet"
        Me.Column3.HeaderText = "ContratoNet"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Visible = False
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "Nombre"
        Me.Column4.HeaderText = "Nombre"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 280
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "Clv_TipSer"
        Me.Column5.HeaderText = "Clv_TipSer"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Visible = False
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "Concepto"
        Me.Column6.HeaderText = "Concepto"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Visible = False
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "Clv_ServOld"
        Me.Column7.HeaderText = "Clv_ServOld"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Visible = False
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "Servicio_Anterior"
        Me.Column8.HeaderText = "Servicio_Anterior"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Visible = False
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "Clv_ServNew"
        Me.Column9.HeaderText = "CLv_ServNew"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Visible = False
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "Servicio_Actual"
        Me.Column10.HeaderText = "Servicio_Actual"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Visible = False
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "Fecha_Sol"
        Me.Column11.HeaderText = "Fecha_Sol"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Visible = False
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "Realizar"
        Me.Column12.HeaderText = "Realizar"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Visible = False
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "Status"
        Me.Column13.HeaderText = "Proceso"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Width = 150
        '
        'Column14
        '
        Me.Column14.DataPropertyName = "Clv_Txt"
        Me.Column14.HeaderText = "Clv_Txt"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        Me.Column14.Visible = False
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "Monto"
        Me.Column15.HeaderText = "Monto"
        Me.Column15.Name = "Column15"
        Me.Column15.ReadOnly = True
        Me.Column15.Visible = False
        '
        'Column16
        '
        Me.Column16.DataPropertyName = "Clv_Factura"
        Me.Column16.HeaderText = "Clv_Factura"
        Me.Column16.Name = "Column16"
        Me.Column16.ReadOnly = True
        Me.Column16.Visible = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(423, 299)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&NUEVO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(423, 341)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "&ELIMINAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(356, 163)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(78, 16)
        Me.Label9.TabIndex = 207
        Me.Label9.Text = "Compañía"
        Me.Label9.Visible = False
        '
        'ComboBoxCiudad
        '
        Me.ComboBoxCiudad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCiudad.FormattingEnabled = True
        Me.ComboBoxCiudad.Location = New System.Drawing.Point(235, 199)
        Me.ComboBoxCiudad.Name = "ComboBoxCiudad"
        Me.ComboBoxCiudad.Size = New System.Drawing.Size(236, 23)
        Me.ComboBoxCiudad.TabIndex = 206
        Me.ComboBoxCiudad.Visible = False
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(870, 673)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(136, 36)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "&SALIR"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(923, 70)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(10, 20)
        Me.ClaveTextBox.TabIndex = 2
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(939, 70)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_TipSerTextBox.TabIndex = 4
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'RealizarTextBox
        '
        Me.RealizarTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Realizar", True))
        Me.RealizarTextBox.Location = New System.Drawing.Point(955, 70)
        Me.RealizarTextBox.Name = "RealizarTextBox"
        Me.RealizarTextBox.ReadOnly = True
        Me.RealizarTextBox.Size = New System.Drawing.Size(10, 20)
        Me.RealizarTextBox.TabIndex = 6
        Me.RealizarTextBox.TabStop = False
        '
        'ConCambioServClienteTableAdapter
        '
        Me.ConCambioServClienteTableAdapter.ClearBeforeFill = True
        '
        'BorCambioServClienteBindingSource
        '
        Me.BorCambioServClienteBindingSource.DataMember = "BorCambioServCliente"
        Me.BorCambioServClienteBindingSource.DataSource = Me.DataSetEric
        '
        'BorCambioServClienteTableAdapter
        '
        Me.BorCambioServClienteTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(870, 11)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "&NUEVO"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(870, 56)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(136, 36)
        Me.Button7.TabIndex = 8
        Me.Button7.Text = "&ELIMINAR"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'BrwCamServCte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 740)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.RealizarTextBox)
        Me.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "BrwCamServCte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Servicio"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConCambioServClienteDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConCambioServClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConCambioServClienteTableAdapter As sofTV.DataSetEricTableAdapters.ConCambioServClienteTableAdapter
    Friend WithEvents ConCambioServClienteDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RealizarTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusLabel2 As System.Windows.Forms.Label
    Friend WithEvents Servicio_ActualLabel2 As System.Windows.Forms.Label
    Friend WithEvents ConceptoLabel2 As System.Windows.Forms.Label
    Friend WithEvents ContratoLabel2 As System.Windows.Forms.Label
    Friend WithEvents NombreLabel2 As System.Windows.Forms.Label
    Friend WithEvents Fecha_SolLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents BorCambioServClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorCambioServClienteTableAdapter As sofTV.DataSetEricTableAdapters.BorCambioServClienteTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Servicio_AnteriorLabel2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
