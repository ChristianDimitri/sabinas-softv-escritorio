﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwProspectos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.tbApellidoM = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbApellidoP = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbNumero = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbCalle = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbProspecto = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ContratoCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Apellido_Paterno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Apellido_Materno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CIUDAD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Localidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COLONIA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CALLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NUMERO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELEFONO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcCONTRATO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Email = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.lblColonias = New System.Windows.Forms.Label()
        Me.comboColonias = New System.Windows.Forms.ComboBox()
        Me.ComboBoxCompania = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tbMail = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbTelefono = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbDireccion = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tbnombreM = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.Color.DarkOrange
        Me.Button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.ForeColor = System.Drawing.Color.Black
        Me.Button14.Location = New System.Drawing.Point(170, 427)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(91, 26)
        Me.Button14.TabIndex = 232
        Me.Button14.Text = "&Buscar"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'tbApellidoM
        '
        Me.tbApellidoM.BackColor = System.Drawing.Color.LightGray
        Me.tbApellidoM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbApellidoM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbApellidoM.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbApellidoM.Location = New System.Drawing.Point(13, 246)
        Me.tbApellidoM.Name = "tbApellidoM"
        Me.tbApellidoM.Size = New System.Drawing.Size(250, 24)
        Me.tbApellidoM.TabIndex = 227
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 226)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(124, 15)
        Me.Label1.TabIndex = 229
        Me.Label1.Text = "Apellido Materno :"
        '
        'tbApellidoP
        '
        Me.tbApellidoP.BackColor = System.Drawing.Color.LightGray
        Me.tbApellidoP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbApellidoP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbApellidoP.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbApellidoP.Location = New System.Drawing.Point(13, 202)
        Me.tbApellidoP.Name = "tbApellidoP"
        Me.tbApellidoP.Size = New System.Drawing.Size(250, 24)
        Me.tbApellidoP.TabIndex = 226
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(12, 181)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(121, 15)
        Me.Label12.TabIndex = 228
        Me.Label12.Text = "Apellido Paterno :"
        '
        'tbNombre
        '
        Me.tbNombre.BackColor = System.Drawing.Color.LightGray
        Me.tbNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombre.Location = New System.Drawing.Point(13, 155)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(250, 24)
        Me.tbNombre.TabIndex = 224
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 137)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 15)
        Me.Label2.TabIndex = 223
        Me.Label2.Text = "Nombre :"
        '
        'tbNumero
        '
        Me.tbNumero.BackColor = System.Drawing.Color.LightGray
        Me.tbNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNumero.Location = New System.Drawing.Point(15, 342)
        Me.tbNumero.Name = "tbNumero"
        Me.tbNumero.Size = New System.Drawing.Size(248, 24)
        Me.tbNumero.TabIndex = 220
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(15, 323)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 15)
        Me.Label5.TabIndex = 214
        Me.Label5.Text = "Numero :"
        '
        'tbCalle
        '
        Me.tbCalle.BackColor = System.Drawing.Color.LightGray
        Me.tbCalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbCalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCalle.Location = New System.Drawing.Point(15, 297)
        Me.tbCalle.Name = "tbCalle"
        Me.tbCalle.Size = New System.Drawing.Size(248, 24)
        Me.tbCalle.TabIndex = 219
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 278)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 15)
        Me.Label4.TabIndex = 213
        Me.Label4.Text = "Calle :"
        '
        'tbProspecto
        '
        Me.tbProspecto.BackColor = System.Drawing.Color.LightGray
        Me.tbProspecto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbProspecto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbProspecto.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbProspecto.Location = New System.Drawing.Point(16, 109)
        Me.tbProspecto.Name = "tbProspecto"
        Me.tbProspecto.Size = New System.Drawing.Size(124, 24)
        Me.tbProspecto.TabIndex = 217
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 87)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(113, 15)
        Me.Label3.TabIndex = 212
        Me.Label3.Text = "Núm. Prospecto:"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ContratoCompania, Me.Nombre, Me.Apellido_Paterno, Me.Apellido_Materno, Me.Estado, Me.CIUDAD, Me.Localidad, Me.COLONIA, Me.CALLE, Me.NUMERO, Me.TELEFONO, Me.gcCONTRATO, Me.Email})
        Me.DataGridView1.Location = New System.Drawing.Point(273, 12)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(600, 699)
        Me.DataGridView1.TabIndex = 236
        Me.DataGridView1.TabStop = False
        '
        'ContratoCompania
        '
        Me.ContratoCompania.DataPropertyName = "Clv_Prospecto"
        Me.ContratoCompania.HeaderText = "Prospecto"
        Me.ContratoCompania.Name = "ContratoCompania"
        Me.ContratoCompania.ReadOnly = True
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        '
        'Apellido_Paterno
        '
        Me.Apellido_Paterno.DataPropertyName = "ApellidoPat"
        Me.Apellido_Paterno.HeaderText = "Apellido Paterno"
        Me.Apellido_Paterno.Name = "Apellido_Paterno"
        Me.Apellido_Paterno.ReadOnly = True
        '
        'Apellido_Materno
        '
        Me.Apellido_Materno.DataPropertyName = "ApellidoMat"
        Me.Apellido_Materno.HeaderText = "Apellido Materno"
        Me.Apellido_Materno.Name = "Apellido_Materno"
        Me.Apellido_Materno.ReadOnly = True
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        '
        'CIUDAD
        '
        Me.CIUDAD.DataPropertyName = "Ciudad"
        Me.CIUDAD.HeaderText = "Ciudad"
        Me.CIUDAD.Name = "CIUDAD"
        Me.CIUDAD.ReadOnly = True
        '
        'Localidad
        '
        Me.Localidad.DataPropertyName = "Localidad"
        Me.Localidad.HeaderText = "Localidad"
        Me.Localidad.Name = "Localidad"
        Me.Localidad.ReadOnly = True
        '
        'COLONIA
        '
        Me.COLONIA.DataPropertyName = "Colonia"
        Me.COLONIA.HeaderText = "Colonia"
        Me.COLONIA.Name = "COLONIA"
        Me.COLONIA.ReadOnly = True
        '
        'CALLE
        '
        Me.CALLE.DataPropertyName = "Calle"
        Me.CALLE.HeaderText = "Calle"
        Me.CALLE.Name = "CALLE"
        Me.CALLE.ReadOnly = True
        '
        'NUMERO
        '
        Me.NUMERO.DataPropertyName = "Numero"
        Me.NUMERO.HeaderText = "Numero"
        Me.NUMERO.Name = "NUMERO"
        Me.NUMERO.ReadOnly = True
        '
        'TELEFONO
        '
        Me.TELEFONO.DataPropertyName = "Telefono"
        Me.TELEFONO.HeaderText = "Telefono"
        Me.TELEFONO.Name = "TELEFONO"
        Me.TELEFONO.ReadOnly = True
        '
        'gcCONTRATO
        '
        Me.gcCONTRATO.DataPropertyName = "Contrato"
        Me.gcCONTRATO.HeaderText = "noCONTRATO"
        Me.gcCONTRATO.Name = "gcCONTRATO"
        Me.gcCONTRATO.ReadOnly = True
        Me.gcCONTRATO.Visible = False
        Me.gcCONTRATO.Width = 5
        '
        'Email
        '
        Me.Email.DataPropertyName = "Email"
        Me.Email.HeaderText = "Email"
        Me.Email.Name = "Email"
        Me.Email.ReadOnly = True
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Orange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(884, 98)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(120, 36)
        Me.Button4.TabIndex = 239
        Me.Button4.Text = "&MODIFICAR"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(884, 56)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(120, 36)
        Me.Button3.TabIndex = 238
        Me.Button3.Text = "&CONSULTAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(884, 14)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 36)
        Me.Button2.TabIndex = 237
        Me.Button2.Text = "&NUEVO"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(884, 673)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(120, 36)
        Me.Button5.TabIndex = 240
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'lblColonias
        '
        Me.lblColonias.AutoSize = True
        Me.lblColonias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColonias.Location = New System.Drawing.Point(14, 375)
        Me.lblColonias.Name = "lblColonias"
        Me.lblColonias.Size = New System.Drawing.Size(64, 15)
        Me.lblColonias.TabIndex = 230
        Me.lblColonias.Text = "Colonia :"
        '
        'comboColonias
        '
        Me.comboColonias.BackColor = System.Drawing.Color.LightGray
        Me.comboColonias.DisplayMember = "nombre"
        Me.comboColonias.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.comboColonias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboColonias.FormattingEnabled = True
        Me.comboColonias.Location = New System.Drawing.Point(13, 394)
        Me.comboColonias.Name = "comboColonias"
        Me.comboColonias.Size = New System.Drawing.Size(248, 24)
        Me.comboColonias.TabIndex = 216
        Me.comboColonias.ValueMember = "clvColonia"
        '
        'ComboBoxCompania
        '
        Me.ComboBoxCompania.BackColor = System.Drawing.Color.LightGray
        Me.ComboBoxCompania.DisplayMember = "nombre"
        Me.ComboBoxCompania.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBoxCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompania.FormattingEnabled = True
        Me.ComboBoxCompania.Location = New System.Drawing.Point(15, 54)
        Me.ComboBoxCompania.Name = "ComboBoxCompania"
        Me.ComboBoxCompania.Size = New System.Drawing.Size(248, 24)
        Me.ComboBoxCompania.TabIndex = 242
        Me.ComboBoxCompania.ValueMember = "clvColonia"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(14, 33)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 15)
        Me.Label7.TabIndex = 243
        Me.Label7.Text = "Plaza :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 11)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(184, 18)
        Me.Label8.TabIndex = 244
        Me.Label8.Text = "Buscar Prospecto por :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tbMail)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.tbTelefono)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.tbDireccion)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.tbnombreM)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 460)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(252, 251)
        Me.GroupBox1.TabIndex = 245
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Información de prospecto"
        '
        'tbMail
        '
        Me.tbMail.BackColor = System.Drawing.Color.LightGray
        Me.tbMail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbMail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbMail.Enabled = False
        Me.tbMail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMail.Location = New System.Drawing.Point(6, 221)
        Me.tbMail.Name = "tbMail"
        Me.tbMail.Size = New System.Drawing.Size(240, 21)
        Me.tbMail.TabIndex = 252
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(15, 203)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 15)
        Me.Label11.TabIndex = 251
        Me.Label11.Text = "Email :"
        '
        'tbTelefono
        '
        Me.tbTelefono.BackColor = System.Drawing.Color.LightGray
        Me.tbTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbTelefono.Enabled = False
        Me.tbTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTelefono.Location = New System.Drawing.Point(6, 174)
        Me.tbTelefono.Name = "tbTelefono"
        Me.tbTelefono.Size = New System.Drawing.Size(240, 21)
        Me.tbTelefono.TabIndex = 250
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(15, 156)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(71, 15)
        Me.Label10.TabIndex = 249
        Me.Label10.Text = "Teléfono :"
        '
        'tbDireccion
        '
        Me.tbDireccion.BackColor = System.Drawing.Color.LightGray
        Me.tbDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbDireccion.Enabled = False
        Me.tbDireccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDireccion.Location = New System.Drawing.Point(7, 98)
        Me.tbDireccion.Multiline = True
        Me.tbDireccion.Name = "tbDireccion"
        Me.tbDireccion.Size = New System.Drawing.Size(240, 55)
        Me.tbDireccion.TabIndex = 248
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(16, 80)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(76, 15)
        Me.Label9.TabIndex = 247
        Me.Label9.Text = "Dirección :"
        '
        'tbnombreM
        '
        Me.tbnombreM.BackColor = System.Drawing.Color.LightGray
        Me.tbnombreM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbnombreM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbnombreM.Enabled = False
        Me.tbnombreM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbnombreM.Location = New System.Drawing.Point(6, 40)
        Me.tbnombreM.Multiline = True
        Me.tbnombreM.Name = "tbnombreM"
        Me.tbnombreM.Size = New System.Drawing.Size(240, 37)
        Me.tbnombreM.TabIndex = 246
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(15, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 15)
        Me.Label6.TabIndex = 224
        Me.Label6.Text = "Nombre :"
        '
        'BrwProspectos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.ComboBoxCompania)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.comboColonias)
        Me.Controls.Add(Me.lblColonias)
        Me.Controls.Add(Me.tbApellidoM)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbApellidoP)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.tbNombre)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbNumero)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tbCalle)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tbProspecto)
        Me.Controls.Add(Me.Label3)
        Me.Name = "BrwProspectos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Buscar Prospecto"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents tbApellidoM As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbApellidoP As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbNumero As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbCalle As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbProspecto As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents lblColonias As System.Windows.Forms.Label
    Friend WithEvents comboColonias As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxCompania As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tbnombreM As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbMail As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tbDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ContratoCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Apellido_Paterno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Apellido_Materno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CIUDAD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Localidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COLONIA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CALLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NUMERO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELEFONO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gcCONTRATO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Email As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
