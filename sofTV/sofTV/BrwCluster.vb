﻿Imports System.IO
Imports System.Data.SqlClient
Public Class BrwCluster

    Private Sub BrwCluster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenadgvcluster(1, "", "")
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub llenadgvcluster(ByVal opcion As Integer, ByVal clave As String, ByVal descripcion As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, opcion)
        BaseII.CreateMyParameter("@clave", SqlDbType.VarChar, clave)
        BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, descripcion)
        BaseII.CreateMyParameter("@clv_cluster", SqlDbType.Int, 0)
        dgvcluster.DataSource = BaseII.ConsultaDT("MuestraCluster")
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        llenadgvcluster(2, tbclave.Text, "")
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        llenadgvcluster(3, "", tbdescripcion.Text)
    End Sub

    Private Sub dgvcluster_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvcluster.SelectionChanged
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim comando As New SqlCommand()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "MuestraCluster"

        Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
        p4.Direction = ParameterDirection.Input
        p4.Value = 4
        comando.Parameters.Add(p4)
        Dim p3 As New SqlParameter("@clave", SqlDbType.VarChar)
        p3.Direction = ParameterDirection.Input
        p3.Value = dgvcluster.CurrentRow.Cells(0).Value
        comando.Parameters.Add(p3)
        Dim p2 As New SqlParameter("@descripcion", SqlDbType.VarChar)
        p2.Direction = ParameterDirection.Input
        p2.Value = dgvcluster.CurrentRow.Cells(0).Value
        comando.Parameters.Add(p2)
        Dim p1 As New SqlParameter("@clv_cluster", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = dgvcluster.CurrentRow.Cells(0).Value
        comando.Parameters.Add(p1)
        Dim reader As SqlDataReader = comando.ExecuteReader()
        If reader.Read() Then
            lbclv_cluster.Text = reader(0).ToString
            Clv_TxtLabel1.Text = reader(1).ToString
            DescripcionLabel1.Text = reader(2).ToString
        End If
        reader.Close()
        conexion.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        opcion = "N"
        FrmCluster.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If dgvcluster.Rows.Count = 0 Then
            Exit Sub
        End If
        opcion = "C"
        gloclv_cluster = CInt(lbclv_cluster.Text)
        FrmCluster.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If dgvcluster.Rows.Count = 0 Then
            Exit Sub
        End If
        opcion = "M"
        gloclv_cluster = lbclv_cluster.Text
        FrmCluster.Show()
    End Sub

    Private Sub BrwCluster_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        llenadgvcluster(1, "", "")
    End Sub
End Class