﻿Imports System.Data.SqlClient
Public Class FrmSelPlazaXML


    Private Sub FrmSelPlaza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'Por si es uno
        'DocPlazas.DocumentElement.RemoveAll()
        If GloOpFiltrosXML = "CarteraDistribuidor" Then
            agregartodo.Visible = False
            quitartodo.Visible = False
            CheckBox1.Checked = True
            CheckBox1.Enabled = False
        Else
            agregartodo.Visible = True
            quitartodo.Visible = True
            CheckBox1.Checked = False
            CheckBox1.Enabled = True
        End If
        CargarElementosPlazaXML()
        llenalistboxs()
        If loquehay.Items.Count = 1 Then
            llevamealotro()
            Me.Close()
        End If
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Plaza") = loquehay.SelectedValue Then
                elem.SetAttribute("Seleccion", 1)
            End If
        Next
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Plaza") = seleccion.SelectedValue Then
                elem.SetAttribute("Seleccion", 0)
            End If
        Next
        llenalistboxs()

    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        llenalistboxs()

    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos

            elem.SetAttribute("Seleccion", 0)

        Next
        llenalistboxs()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        EntradasSelCiudad = 0
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        PasarTodoXML = CheckBox1.Checked
        If GloOpFiltrosXML = "VariosDesconectados" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaListado" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaOrdenes" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "VariosAlCorriente" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "VariosContrataciones" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCortesia" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCiudad" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "AtencionTelefonica" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "PendientesDeRealizar" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ResumenOrdenReporte" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "DetalleOrdenes" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ListadoQuejas" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ListaCumpleanos" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "EnviarSMS" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "QuejasConcetoDetallado" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "InterfazDigital" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "InstalacionesEfectuadas" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "bitacoradepruebas" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "Hoteles" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "Permanencia" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "BitacoraEdoCuenta" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ListadoActividadesTecnico" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "AgentaTecnico" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "reporteSuscriptoresBruno" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "sindocumentos" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "MensajesVarios" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "Carteras" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "QuejasPorIdentificador" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "CarteraDistribuidor" Then
            If seleccion.Items.Count > 1 Then
                MsgBox("Para esta opción solo se puede seleccionar un distribuidor.")
                Exit Sub
            End If
            PasarTodoXML = True
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "OrdenesZonaNorte" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ResumenClientesCobertura" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "CuentasClabe" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "BitacoraDatalogic" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ResumenCajas" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ReporteToken" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "PruebasDeInternet" Then
            FrmSelCompaniaXML.Show()
        End If
        Me.Close()
    End Sub
    Private Sub llevamealotro()
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        If GloOpFiltrosXML = "VariosDesconectados" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaListado" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaOrdenes" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "VariosAlCorriente" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "VariosContrataciones" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCortesia" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCiudad" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "AtencionTelefonica" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "PendientesDeRealizar" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ResumenOrdenReporte" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "DetalleOrdenes" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ListadoQuejas" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ListaCumpleanos" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "EnviarSMS" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "QuejasConcetoDetallado" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "InterfazDigital" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "InstalacionesEfectuadas" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "bitacoradepruebas" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "Hoteles" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "Permanencia" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "BitacoraEdoCuenta" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ListadoActividadesTecnico" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "AgentaTecnico" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "reporteSuscriptoresBruno" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "sindocumentos" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "MensajesVarios" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "Carteras" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "CarteraDistribuidor" Then
            If seleccion.Items.Count > 1 Then
                MsgBox("Para esta opción solo se puede seleccionar un distribuidor.")
                Exit Sub
            End If
            PasarTodoXML = True
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "QuejasPorIdentificador" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "OrdenesZonaNorte" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ResumenClientesCobertura" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "CuentasClabe" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "BitacoraDatalogic" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ResumenCajas" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "ReporteToken" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "PruebasDeInternet" Then
            FrmSelCompaniaXML.Show()
        End If
    End Sub

    Private Sub llenalistboxs()
        Dim dt As New DataTable
        dt.Columns.Add("Clv_Plaza", Type.GetType("System.String"))
        dt.Columns.Add("nombre", Type.GetType("System.String"))

        Dim dt2 As New DataTable
        dt2.Columns.Add("Clv_Plaza", Type.GetType("System.String"))
        dt2.Columns.Add("nombre", Type.GetType("System.String"))

        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Seleccion") = 0 Then
                dt.Rows.Add(elem.GetAttribute("Clv_Plaza"), elem.GetAttribute("nombre"))
            End If
            If elem.GetAttribute("Seleccion") = 1 Then
                dt2.Rows.Add(elem.GetAttribute("Clv_Plaza"), elem.GetAttribute("nombre"))
            End If
        Next
        loquehay.DataSource = New BindingSource(dt, Nothing)
        seleccion.DataSource = New BindingSource(dt2, Nothing)
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        PasarTodoXML = CheckBox1.Checked
    End Sub
End Class