﻿Imports System.Collections.Generic
Imports System.Data.SqlClient
Public Class FrmCambioDeTipoDeServicio
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedValue = 0

            End If
            GloIdCompania = 0
            
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub LLENA_cajas()

        Dim dTable As New DataTable
        Dim i As Integer = 0
        BaseII.limpiaParametros()
        Me.ComboBox1.DataSource = BaseII.ConsultaDT("SP_MUESTRATipoCaja")
        Me.ComboBox1.ValueMember = "NOARTICULO"
        Me.ComboBox1.DisplayMember = "CAJA"

        'DataGridView1.Columns.Item(7).Visible = False
    End Sub

    Private Sub MuestraServiciosEric(ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Op As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        dTable = BaseII.ConsultaDT("MuestraServiciosEric")
        If Clv_TipSer = 1 Then
            cbServicioTv.DataSource = dTable
        Else
            cbServicioDig.DataSource = dTable
        End If
    End Sub

    Private Sub MUESTRAClientesCambioDeTipoDeServicio(ByVal Op As Integer, ByVal Clv_TipSer As Integer, ByVal Contrato As Integer, ByVal Nombre As String)
        Dim dSet As New DataSet

        Dim l As New List(Of String)
        l.Add("Consulta")
        l.Add("Mensaje")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Nombre, 250)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
        dSet = BaseII.ConsultaDS("MUESTRAClientesCambioDeTipoDeServicio", l)


        eClv_Servicio = 0
        eClv_Servicio = dSet.Tables("Consulta").Rows(0)(2).ToString
        eMsj = ""
        eMsj = dSet.Tables("Mensaje").Rows(0)(0).ToString


        gbServicioDig.Enabled = False
        gbServicioTv.Enabled = False

        If eMsj.Length = 0 Then
            If cbDeTvADig.Checked = True Then
                gbServicioDig.Enabled = True
            Else
                gbServicioTv.Enabled = True
            End If
            bnGuardar.Enabled = True
        Else
            MessageBox.Show(eMsj)
            Limpiar()
        End If

    End Sub

    Private Sub NUECambioDeTipoDeServicio(ByVal Contrato As Integer, ByVal Clv_Usuario As String, ByVal DeTvADig As Boolean, ByVal DeDigATv As Boolean, ByVal Clv_ServicioTV As Integer, ByVal TvSinPago As Integer, ByVal TvConPago As Integer, ByVal Clv_ServicioDig As Integer, ByVal NumeroDeCajas As Integer, ByVal ExtensionesAnalogas As Integer, ByVal OCLVSESSION As Long, ByVal oModelo As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, Clv_Usuario, 10)
        BaseII.CreateMyParameter("@DeTvADig", SqlDbType.Bit, DeTvADig)
        BaseII.CreateMyParameter("@DeDigATv", SqlDbType.Bit, DeDigATv)
        BaseII.CreateMyParameter("@Clv_ServicioTV", SqlDbType.Int, Clv_ServicioTV)
        BaseII.CreateMyParameter("@TvSinPago", SqlDbType.Int, TvSinPago)
        BaseII.CreateMyParameter("@TvConPago", SqlDbType.Int, TvConPago)
        BaseII.CreateMyParameter("@Clv_ServicioDig", SqlDbType.Int, Clv_ServicioDig)
        BaseII.CreateMyParameter("@NumeroDeCajas", SqlDbType.Int, NumeroDeCajas)
        BaseII.CreateMyParameter("@ExtensionesAnalogas", SqlDbType.Int, ExtensionesAnalogas)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, OCLVSESSION)
        BaseII.CreateMyParameter("@ModeloCaja", SqlDbType.Int, oModelo)
        BaseII.ProcedimientoOutPut("NUECambioDeTipoDeServicio")
        eMsj = ""
        eMsj = BaseII.dicoPar("@Msj").ToString

        eRefrescar = True
        MessageBox.Show(eMsj)
        Limpiar()

    End Sub

    Private Sub Limpiar()
        eContrato = 0
        eClv_TipSer = 1
        eClv_Servicio = 0
        tbContrato.Clear()
        nudCajas.Value = 1
        nudExtensiones.Value = 0
        nudTvSinPago.Value = 0
        nudTvConPago.Value = 0
        gbServicioDig.Enabled = False
        gbServicioTv.Enabled = False
        bnGuardar.Enabled = False
        cbDeTvADig.Checked = True
    End Sub

    Private Sub FrmCambioDeTipoDeServicio_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmCambioDeTipoDeServicio_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Llena_companias()
        Limpiar()
        MuestraServiciosEric(1, 0, 0)
        MuestraServiciosEric(3, 0, 0)
        LLENA_cajas()
    End Sub

    Private Sub bnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnBuscar.Click
        eContrato = 0
        eContratoCompania = 0
        Dim FrmMuestaCambio As New FrmMUESTRAClientesCambioDeTipoDeServicio
        'FrmMUESTRAClientesCambioDeTipoDeServicio.Show()
        FrmMuestaCambio.ShowDialog()
        If eContrato > 0 Then
            tbContrato.Text = eContrato
            tbContratoCompania.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
            eContrato = 0
            'eContratoCompania = 0
            DameClv_Session()
            MuestraServiciosEric(1, 0, 0)
            MuestraServiciosEric(3, 0, 0)
            If cbDeTvADig.Checked = True Then
                MUESTRAClientesCambioDeTipoDeServicio(1, 1, eContratoCompania, "")
            Else
                MUESTRAClientesCambioDeTipoDeServicio(1, 3, eContratoCompania, "")
            End If
        End If
    End Sub

    Private Sub bnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnGuardar.Click

        If cbDeTvADig.Checked = True Then
            If ComboBox1.SelectedIndex >= 1 Then
                LocNoArticuloCajas = ComboBox1.SelectedValue

            Else
                LocNoArticuloCajas = 0
                MsgBox("Seleccione un modelo de caja por favor ", MsgBoxStyle.Information, "Importante")
                Exit Sub
            End If

            Dim reSPUESTA As MsgBoxResult = MsgBoxResult.No
            reSPUESTA = MsgBox(" Este proceso realizara varias afectaciones y si se procesa no habrá marcha atrás. ¿ Deseas realizar el proceso ahora ?", MsgBoxStyle.YesNo, "Importante")
            If reSPUESTA = vbYes Then
                NUECambioDeTipoDeServicio(tbContrato.Text, GloUsuario, cbDeTvADig.Checked, cbDeDigATv.Checked, eClv_Servicio, 0, 0, cbServicioDig.SelectedValue, nudCajas.Value, nudExtensiones.Value, eClv_Session, LocNoArticuloCajas)
                Me.Close()
            End If
        Else

            BndCambioServicio = "C"
            eContrato = tbContrato.Text
            BndValRecon = 0
            Dim frmaparatosrecibir As New FormAparatosRecibir
            frmaparatosrecibir.ShowDialog()
            BndCambioServicio = "R"

            If BndValRecon = 1 Then 'Autorizados si no nel
                Dim resul As String = SP_VALIDA_APARATOS_RECON(eClv_Session, BndCambioServicio)
                If resul.Length > 0 Then
                    MsgBox(resul)
                    BndValRecon = 0
                Else
                    BndValRecon = 1 'Autorizados si no nel
                    NUECambioDeTipoDeServicio(tbContrato.Text, GloUsuario, cbDeTvADig.Checked, cbDeDigATv.Checked, cbServicioTv.SelectedValue, nudTvSinPago.Value, nudTvConPago.Value, eClv_Servicio, 0, 0, eClv_Session, 0)
                    Me.Close()
                End If

            End If
        End If


    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbDeTvADig_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDeTvADig.CheckedChanged
        eClv_TipSer = 1
        'If tbContrato.Text.Length = 0 Then Exit Sub
        'If IsNumeric(tbContrato.Text) = False Then Exit Sub
        Try
            If tbContratoCompania.Text.Length = 0 Then
                Exit Sub
            End If
            Dim array As String() = tbContratoCompania.Text.Trim.Split("-")
            GloIdCompania = array(1).Trim
            If cbDeTvADig.Checked = True Then
                MUESTRAClientesCambioDeTipoDeServicio(1, 1, array(0).Trim, "")
            Else
                MUESTRAClientesCambioDeTipoDeServicio(1, 3, array(0).Trim, "")
            End If
        Catch ex As Exception
            MsgBox("Contrato inválido. Inténtalo nuevamente.")
        End Try
    End Sub

    Private Sub cbDeDigATv_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDeDigATv.CheckedChanged
        eClv_TipSer = 3
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        Try
            If tbContratoCompania.Text.Length = 0 Then
                Exit Sub
            End If
            Dim array As String() = tbContratoCompania.Text.Trim.Split("-")
            GloIdCompania = array(1).Trim
            If cbDeTvADig.Checked = True Then
                MUESTRAClientesCambioDeTipoDeServicio(1, 1, array(0).Trim, "")
            Else
                MUESTRAClientesCambioDeTipoDeServicio(1, 3, array(0).Trim, "")
            End If
        Catch ex As Exception
            MsgBox("Contrato inválido. Inténtalo nuevamente.")
        End Try
        
    End Sub

    Private Sub tbContrato_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tbContrato.KeyDown, tbContratoCompania.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbContratoCompania.Text.Length = 0 Then Exit Sub
        DameClv_Session()
        Try
            Dim CONE As New SqlConnection(MiConexion)
            CONE.Open()
            Dim comando As New SqlCommand()
            comando.Connection = CONE
            Dim array As String() = tbContratoCompania.Text.Trim.Split("-")
            GloIdCompania = array(1).Trim
            comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + array(0).Trim + " and idcompania=" + GloIdCompania.ToString() + " and idcompania in (select idcompania from Rel_Usuario_Compania where Clave=" + GloClvUsuario.ToString() + ")"
            tbContrato.Text = comando.ExecuteScalar.ToString()
            'CONE.Close()
            'CONE.Open()
            CONE.Close()
            MuestraServiciosEric(1, 0, 0)
            MuestraServiciosEric(3, 0, 0)
            If cbDeTvADig.Checked = True Then
                MUESTRAClientesCambioDeTipoDeServicio(1, 1, array(0).Trim, "")
            Else
                MUESTRAClientesCambioDeTipoDeServicio(1, 3, array(0).Trim, "")
            End If
        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub tbContrato_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbContrato.TextChanged

    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            Limpiar()
            MuestraServiciosEric(1, 0, 0)
            MuestraServiciosEric(3, 0, 0)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tbContratoCompania_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbContratoCompania.TextChanged

    End Sub
End Class
