﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormIAPARATOSDoble
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.BtnAceptar = New System.Windows.Forms.Button()
        Me.ComboBoxPorAsignar = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBoxAparatosDisponibles = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblEstadoAparato = New System.Windows.Forms.Label()
        Me.CMBoxEstadoAparato = New System.Windows.Forms.ComboBox()
        Me.ComboBoxCables = New System.Windows.Forms.ComboBox()
        Me.LabCables = New System.Windows.Forms.Label()
        Me.ComboBoxControlRemoto = New System.Windows.Forms.ComboBox()
        Me.LabControlRemoto = New System.Windows.Forms.Label()
        Me.CMBPanelAccesorios = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxWan = New System.Windows.Forms.TextBox()
        Me.LabelWan = New System.Windows.Forms.Label()
        Me.CMBoxEstadoAparato2 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBoxPorAsignar2 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CMBPanelAccesorios.SuspendLayout
        Me.SuspendLayout
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(308, 278)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 32
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = false
        '
        'BtnAceptar
        '
        Me.BtnAceptar.BackColor = System.Drawing.SystemColors.Control
        Me.BtnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BtnAceptar.ForeColor = System.Drawing.Color.Black
        Me.BtnAceptar.Location = New System.Drawing.Point(166, 278)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(136, 33)
        Me.BtnAceptar.TabIndex = 41
        Me.BtnAceptar.Text = "&ACEPTAR"
        Me.BtnAceptar.UseVisualStyleBackColor = false
        '
        'ComboBoxPorAsignar
        '
        Me.ComboBoxPorAsignar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ComboBoxPorAsignar.FormattingEnabled = true
        Me.ComboBoxPorAsignar.Location = New System.Drawing.Point(14, 52)
        Me.ComboBoxPorAsignar.Name = "ComboBoxPorAsignar"
        Me.ComboBoxPorAsignar.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxPorAsignar.TabIndex = 40
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(11, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(167, 16)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Aparatos actual de TV:"
        '
        'ComboBoxAparatosDisponibles
        '
        Me.ComboBoxAparatosDisponibles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ComboBoxAparatosDisponibles.FormattingEnabled = true
        Me.ComboBoxAparatosDisponibles.Location = New System.Drawing.Point(14, 210)
        Me.ComboBoxAparatosDisponibles.Name = "ComboBoxAparatosDisponibles"
        Me.ComboBoxAparatosDisponibles.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxAparatosDisponibles.TabIndex = 43
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(12, 191)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(178, 16)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "Seleccione el Aparato   :"
        '
        'LblEstadoAparato
        '
        Me.LblEstadoAparato.AutoSize = true
        Me.LblEstadoAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LblEstadoAparato.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LblEstadoAparato.Location = New System.Drawing.Point(15, 85)
        Me.LblEstadoAparato.Name = "LblEstadoAparato"
        Me.LblEstadoAparato.Size = New System.Drawing.Size(257, 16)
        Me.LblEstadoAparato.TabIndex = 44
        Me.LblEstadoAparato.Text = "Seleccione el Estado del Aparato   :"
        Me.LblEstadoAparato.Visible = false
        '
        'CMBoxEstadoAparato
        '
        Me.CMBoxEstadoAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CMBoxEstadoAparato.FormattingEnabled = true
        Me.CMBoxEstadoAparato.Location = New System.Drawing.Point(274, 82)
        Me.CMBoxEstadoAparato.Name = "CMBoxEstadoAparato"
        Me.CMBoxEstadoAparato.Size = New System.Drawing.Size(170, 24)
        Me.CMBoxEstadoAparato.TabIndex = 45
        '
        'ComboBoxCables
        '
        Me.ComboBoxCables.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ComboBoxCables.FormattingEnabled = true
        Me.ComboBoxCables.Location = New System.Drawing.Point(16, 39)
        Me.ComboBoxCables.Name = "ComboBoxCables"
        Me.ComboBoxCables.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxCables.TabIndex = 47
        '
        'LabCables
        '
        Me.LabCables.AutoSize = true
        Me.LabCables.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabCables.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabCables.Location = New System.Drawing.Point(8, 20)
        Me.LabCables.Name = "LabCables"
        Me.LabCables.Size = New System.Drawing.Size(160, 16)
        Me.LabCables.TabIndex = 46
        Me.LabCables.Text = "Seleccione el Cable  :"
        '
        'ComboBoxControlRemoto
        '
        Me.ComboBoxControlRemoto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ComboBoxControlRemoto.FormattingEnabled = true
        Me.ComboBoxControlRemoto.Location = New System.Drawing.Point(16, 85)
        Me.ComboBoxControlRemoto.Name = "ComboBoxControlRemoto"
        Me.ComboBoxControlRemoto.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxControlRemoto.TabIndex = 49
        '
        'LabControlRemoto
        '
        Me.LabControlRemoto.AutoSize = true
        Me.LabControlRemoto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabControlRemoto.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabControlRemoto.Location = New System.Drawing.Point(9, 66)
        Me.LabControlRemoto.Name = "LabControlRemoto"
        Me.LabControlRemoto.Size = New System.Drawing.Size(226, 16)
        Me.LabControlRemoto.TabIndex = 48
        Me.LabControlRemoto.Text = "Seleccione el Control Remoto  :"
        '
        'CMBPanelAccesorios
        '
        Me.CMBPanelAccesorios.Controls.Add(Me.Label2)
        Me.CMBPanelAccesorios.Controls.Add(Me.LabCables)
        Me.CMBPanelAccesorios.Controls.Add(Me.ComboBoxCables)
        Me.CMBPanelAccesorios.Controls.Add(Me.ComboBoxControlRemoto)
        Me.CMBPanelAccesorios.Controls.Add(Me.LabControlRemoto)
        Me.CMBPanelAccesorios.Location = New System.Drawing.Point(12, 342)
        Me.CMBPanelAccesorios.Name = "CMBPanelAccesorios"
        Me.CMBPanelAccesorios.Size = New System.Drawing.Size(460, 126)
        Me.CMBPanelAccesorios.TabIndex = 50
        Me.CMBPanelAccesorios.Visible = false
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 20)
        Me.Label2.TabIndex = 50
        Me.Label2.Text = "Accesorios"
        '
        'TextBoxWan
        '
        Me.TextBoxWan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxWan.Location = New System.Drawing.Point(274, 244)
        Me.TextBoxWan.Name = "TextBoxWan"
        Me.TextBoxWan.Size = New System.Drawing.Size(170, 22)
        Me.TextBoxWan.TabIndex = 60
        '
        'LabelWan
        '
        Me.LabelWan.AutoSize = true
        Me.LabelWan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelWan.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelWan.Location = New System.Drawing.Point(16, 250)
        Me.LabelWan.Name = "LabelWan"
        Me.LabelWan.Size = New System.Drawing.Size(153, 16)
        Me.LabelWan.TabIndex = 59
        Me.LabelWan.Text = "Dirección MAC WAN:"
        '
        'CMBoxEstadoAparato2
        '
        Me.CMBoxEstadoAparato2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CMBoxEstadoAparato2.FormattingEnabled = true
        Me.CMBoxEstadoAparato2.Location = New System.Drawing.Point(274, 160)
        Me.CMBoxEstadoAparato2.Name = "CMBoxEstadoAparato2"
        Me.CMBoxEstadoAparato2.Size = New System.Drawing.Size(170, 24)
        Me.CMBoxEstadoAparato2.TabIndex = 64
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(15, 163)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(257, 16)
        Me.Label3.TabIndex = 63
        Me.Label3.Text = "Seleccione el Estado del Aparato   :"
        Me.Label3.Visible = false
        '
        'ComboBoxPorAsignar2
        '
        Me.ComboBoxPorAsignar2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ComboBoxPorAsignar2.FormattingEnabled = true
        Me.ComboBoxPorAsignar2.Location = New System.Drawing.Point(14, 130)
        Me.ComboBoxPorAsignar2.Name = "ComboBoxPorAsignar2"
        Me.ComboBoxPorAsignar2.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxPorAsignar2.TabIndex = 62
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(11, 111)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(190, 16)
        Me.Label5.TabIndex = 61
        Me.Label5.Text = "Aparato actual de Internet:"
        '
        'FormIAPARATOSDoble
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(467, 331)
        Me.Controls.Add(Me.CMBoxEstadoAparato2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboBoxPorAsignar2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TextBoxWan)
        Me.Controls.Add(Me.LabelWan)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.CMBPanelAccesorios)
        Me.Controls.Add(Me.CMBoxEstadoAparato)
        Me.Controls.Add(Me.LblEstadoAparato)
        Me.Controls.Add(Me.ComboBoxAparatosDisponibles)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxPorAsignar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button5)
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "FormIAPARATOSDoble"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TopMost = true
        Me.CMBPanelAccesorios.ResumeLayout(false)
        Me.CMBPanelAccesorios.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents BtnAceptar As System.Windows.Forms.Button
    Friend WithEvents ComboBoxPorAsignar As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxAparatosDisponibles As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LblEstadoAparato As System.Windows.Forms.Label
    Friend WithEvents CMBoxEstadoAparato As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxCables As System.Windows.Forms.ComboBox
    Friend WithEvents LabCables As System.Windows.Forms.Label
    Friend WithEvents ComboBoxControlRemoto As System.Windows.Forms.ComboBox
    Friend WithEvents LabControlRemoto As System.Windows.Forms.Label
    Friend WithEvents CMBPanelAccesorios As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBoxWan As System.Windows.Forms.TextBox
    Friend WithEvents LabelWan As System.Windows.Forms.Label
    Friend WithEvents CMBoxEstadoAparato2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxPorAsignar2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
