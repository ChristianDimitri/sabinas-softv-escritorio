Imports System.Data.SqlClient
Public Class BrwCajas
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmCatalogoCajas.Show()

    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            gloClave = Me.Clv_calleLabel2.Text
            FrmCatalogoCajas.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            gloClave = Me.Clv_calleLabel2.Text
            FrmCatalogoCajas.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub



    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            gloClave = Me.Clv_calleLabel2.Text
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            gloClave = Me.Clv_calleLabel2.Text
            modificar()
        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub Busca(ByVal op As Integer)

        Try
            BaseII.limpiaParametros()
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If op = 0 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.BUSCACatalogoCajasTableAdapter.Connection = CON
                    BaseII.CreateMyParameter("@Clave", SqlDbType.Int, (CType(Me.TextBox1.Text, Integer)))
                    BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    DataGridView1.DataSource = BaseII.ConsultaDT("BUSCACatalogoCajas")
                    'Me.BUSCACatalogoCajasTableAdapter.Fill(Me.DataSetEDGAR.BUSCACatalogoCajas, New System.Nullable(Of Integer)(CType(Me.TextBox1.Text, Integer)), "", New System.Nullable(Of Integer)(CType(0, Integer)), GloIdCompania)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            ElseIf op = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    BaseII.CreateMyParameter("@Clave", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, Me.TextBox2.Text)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 1)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    DataGridView1.DataSource = BaseII.ConsultaDT("BUSCACatalogoCajas")
                    'Me.BUSCACatalogoCajasTableAdapter.Connection = CON
                    'Me.BUSCACatalogoCajasTableAdapter.Fill(Me.DataSetEDGAR.BUSCACatalogoCajas, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)), GloIdCompania)

                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 2 Then
                'If Len(Trim(Me.TextBox2.Text)) > 0 Then
                BaseII.CreateMyParameter("@Clave", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 2)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                DataGridView1.DataSource = BaseII.ConsultaDT("BUSCACatalogoCajas")
                'Me.BUSCACatalogoCajasTableAdapter.Connection = CON
                'Me.BUSCACatalogoCajasTableAdapter.Fill(Me.DataSetEDGAR.BUSCACatalogoCajas, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)), GloIdCompania)

                'Else
                '    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                'End If
            Else
                BaseII.CreateMyParameter("@Clave", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 3)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                DataGridView1.DataSource = BaseII.ConsultaDT("BUSCACatalogoCajas")
                'Me.BUSCACatalogoCajasTableAdapter.Connection = CON
                'Me.BUSCACatalogoCajasTableAdapter.Fill(Me.DataSetEDGAR.BUSCACatalogoCajas, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(2, Integer)), GloIdCompania)
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            gloClave = Me.Clv_calleLabel2.Text
        End If
    End Sub

    Private Sub BrwCajas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Busca(2)
        End If
        GloIdCompania = ComboBoxCompanias.SelectedValue
    End Sub


    Private Sub BrwCajas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        ' Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        Llena_companias()
        GloIdCompania = ComboBoxCompanias.SelectedValue
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Busca(2)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            Busca(3)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            If DataGridView1.Rows.Count > 0 Then
                Me.Clv_calleLabel2.Text = DataGridView1.SelectedCells(0).Value.ToString
                CMBNombreTextBox.Text = DataGridView1.SelectedCells(1).Value.ToString
            End If
        Catch ex As Exception

        End Try
        
    End Sub
End Class