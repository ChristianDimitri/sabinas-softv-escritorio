﻿Imports System.Data.SqlClient
Public Class FrmHub
    Private Desc_sector As String = Nothing
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing

    Private Sub damedatosbitacora()
        Try
            If eOpcion = "M" Then
                Desc_sector = Me.DescripcionTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub guarda_bitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    If eOpcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Nuevo HUB", " ", "Nuevo HUB: " + Me.Clv_TxtTextBox.Text, LocClv_Ciudad)
                    ElseIf eOpcion = "M" Then
                        'Desc_sector = Me.DescripcionTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DescripcionTextBox.Name, Desc_sector, Me.DescripcionTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Nueva Colonia Para un HUB", " ", "Nueva una Colonia Para El HUB: " + Me.NombreComboBox.Text, LocClv_Ciudad)
                Case 2
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Elimino Colonia Para un HUB", " ", "Elimino una Colonia Para El HUB: " + Me.Clv_ColoniaTextBox.Text, LocClv_Ciudad)
                Case 3
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino un HUB", " ", "Se Elimino el HUB: " + Me.Clv_TxtTextBox.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub FrmSectore_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraColoniaSec1TableAdapter.Connection = CON
        Me.MuestraColoniaSec1TableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec1, 0, 0, 0)

        If eOpcion = "N" Then
            Me.ToolStripButton2.Enabled = False
            Me.NombreComboBox.Enabled = False
            ComboBox3.Enabled = False
            ComboBoxCiudad.Enabled = False
            ComboBoxEstado.Enabled = False
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
        End If

        If eOpcion = "C" Then
            Me.ConSector1TableAdapter.Connection = CON
            Me.ConSector1TableAdapter.Fill(Me.DataSetEric.ConSector1, eClv_Sector, "", "", 3)
            Me.ConRelSectorColonia1TableAdapter.Connection = CON
            Me.ConRelSectorColonia1TableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia1, eClv_Sector)
            Me.BindingNavigator1.Enabled = False
            Me.Clv_TxtTextBox.ReadOnly = True
            Me.DescripcionTextBox.ReadOnly = True
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
            Me.NombreComboBox.Enabled = False
            ObtieneColoniasHub()
        End If

        If eOpcion = "M" Then
            Me.ConSector1TableAdapter.Connection = CON
            Me.ConSector1TableAdapter.Fill(Me.DataSetEric.ConSector1, eClv_Sector, "", "", 3)
            Me.ConRelSectorColonia1TableAdapter.Connection = CON
            Me.ConRelSectorColonia1TableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia1, eClv_Sector)
            Me.Clv_TxtTextBox.Enabled = False
            damedatosbitacora()
            ObtieneColoniasHub()
        End If

        LlenaComboEstado()
        LlenaComboCiudad()
        LlenaComboLocalidad()

        CON.Close()
        If eOpcion = "N" Or eOpcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)

    End Sub

    Private Sub ObtieneColoniasHub()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Sector", SqlDbType.Int, eClv_Sector)
        ConRelSectorColoniaDataGridView.DataSource = BaseII.ConsultaDT("ObtieneColoniasHub")
    End Sub

    Private Sub LlenaComboLocalidad()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
            ComboBox3.DataSource = BaseII.ConsultaDT("MuestraLocalidadCiudad")
            If ComboBox3.Items.Count = 0 Then
                ComboBox3.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LlenaComboEstado()
        Try
            BaseII.limpiaParametros()
            ComboBoxEstado.DataSource = BaseII.ConsultaDT("MuestraEstados")
            If ComboBox3.Items.Count = 0 Then
                ComboBox3.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LlenaComboCiudad()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, ComboBoxEstado.SelectedValue)
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("MuestraCiudadesEstado2")
            If ComboBoxCiudad.Items.Count = 0 Then
                ComboBoxCiudad.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LlenaComboColonia()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, ComboBox3.SelectedValue)
            NombreComboBox.DataSource = BaseII.ConsultaDT("MuestraColoniaLocalidad")
            If NombreComboBox.Items.Count = 0 Then
                NombreComboBox.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click

        eRes = 0
        eMsg = ""

        If Me.Clv_TxtTextBox.Text.Length = 0 Then
            MsgBox("Captura la Clave.", , "Atención")
            Exit Sub
        End If
        If Me.DescripcionTextBox.Text.Length = 0 Then
            MsgBox("Captura la Descripción.", , "Atención")
            Exit Sub
        End If



        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If eOpcion = "M" Then

                Me.ModSector1TableAdapter.Connection = CON
                Me.ModSector1TableAdapter.Fill(Me.DataSetEric.ModSector1, eClv_Sector, Me.Clv_TxtTextBox.Text, Me.DescripcionTextBox.Text, eRes, eMsg)
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    MsgBox(mensaje5)
                    guarda_bitacora(0)
                End If
            End If


            If eOpcion = "N" Then
                Me.NueSector1TableAdapter.Connection = CON
                Me.NueSector1TableAdapter.Fill(Me.DataSetEric.NueSector1, Me.Clv_TxtTextBox.Text, Me.DescripcionTextBox.Text, eRes, eMsg, eClv_Sector)
                If eRes = 1 Then
                    MsgBox(eMsg)

                Else
                    Me.ConSector1TableAdapter.Connection = CON
                    Me.ConSector1TableAdapter.Fill(Me.DataSetEric.ConSector1, eClv_Sector, "", "", 3)
                    Me.ConRelSectorColonia1TableAdapter.Connection = CON
                    Me.ConRelSectorColonia1TableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia1, eClv_Sector)
                    Me.BindingNavigator1.Enabled = False
                    Me.Clv_TxtTextBox.ReadOnly = True
                    Me.DescripcionTextBox.ReadOnly = True
                    Me.NombreComboBox.Enabled = True
                    Me.Button1.Enabled = True
                    Me.Button2.Enabled = True
                    eOpcion = "M"
                    MsgBox(mensaje5)
                    guarda_bitacora(0)
                End If
            End If


            CON.Close()
        Catch

        End Try
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click

        eRes = 0
        eMsg = ""
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorSector1TableAdapter.Connection = CON
            Me.BorSector1TableAdapter.Fill(Me.DataSetEric.BorSector1, Me.Clv_SectorTextBox.Text, eRes, eMsg)
            If eRes = 1 Then
                MsgBox(eMsg)
            Else
                guarda_bitacora(3)
                Me.ConRelSectorColonia1TableAdapter.Connection = CON
                Me.ConRelSectorColonia1TableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia1, eClv_Sector)
                MsgBox(mensaje6)
                Me.Close()
            End If
            CON.Close()
        Catch

        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Agregar()
    End Sub



    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If Me.ConRelSectorColoniaDataGridView.RowCount > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'Me.BorRelSectorColonia1TableAdapter.Connection = CON
            'Me.BorRelSectorColonia1TableAdapter.Fill(Me.DataSetEric.BorRelSectorColonia1, Me.Clv_ColoniaTextBox.Text, eRes, eMsg)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_RelSecCol", SqlDbType.BigInt, ConRelSectorColoniaDataGridView.SelectedCells(0).Value)
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.BigInt, ConRelSectorColoniaDataGridView.SelectedCells(4).Value)
            BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.ProcedimientoOutPut("BorRelHubColonia")
            If BaseII.dicoPar("@Res").ToString = "1" Then
                MsgBox(BaseII.dicoPar("@Msg").ToString)
            Else
                'Me.ConRelSectorColonia1TableAdapter.Connection = CON
                'Me.ConRelSectorColonia1TableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia1, eClv_Sector)
                ObtieneColoniasHub()
                guarda_bitacora(2)
            End If
            CON.Close()
        Else
            MsgBox("Seleccione una Colonia a Eliminar", , "Atención")
        End If
    End Sub

    Private Sub NombreComboBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            Agregar()
        End If
    End Sub

    Private Sub Agregar()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        eRes = 0
        eMsg = ""
        'Me.NueRelSectorColonia1TableAdapter.Connection = CON
        'Me.NueRelSectorColonia1TableAdapter.Fill(Me.DataSetEric.NueRelSectorColonia1, eClv_Sector, CLng(Me.NombreComboBox.SelectedValue), eRes, eMsg)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Sector", SqlDbType.BigInt, eClv_Sector)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.BigInt, NombreComboBox.SelectedValue)
        BaseII.CreateMyParameter("@Clv_Localidad", SqlDbType.BigInt, ComboBox3.SelectedValue)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.BigInt, ComboBoxCiudad.SelectedValue)
        BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.BigInt)
        BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 500)
        BaseII.ProcedimientoOutPut("NueRelHubColonia")
        If BaseII.dicoPar("@Res").ToString = "1" Then
            MsgBox(BaseII.dicoPar("@Msg").ToString)
        Else
            'Me.ConRelSectorColonia1TableAdapter.Connection = CON
            'Me.ConRelSectorColonia1TableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia1, eClv_Sector)
            ObtieneColoniasHub()
            guarda_bitacora(1)
        End If
        CON.Close()
    End Sub

    Private Sub NombreComboBox_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreComboBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Agregar()
        End If
    End Sub

    Private Sub ComboBoxEstado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxEstado.SelectedIndexChanged
        LlenaComboCiudad()

    End Sub

    Private Sub ComboBoxCiudad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxCiudad.SelectedIndexChanged
        LlenaComboLocalidad()
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox3.SelectedIndexChanged
        LlenaComboColonia()
    End Sub
End Class