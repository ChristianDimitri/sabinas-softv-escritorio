<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPoliza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_llave_polizaLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPoliza))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Consulta_Genera_PolizaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Genera_PolizaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Genera_PolizaTableAdapter()
        Me.Consulta_Genera_PolizaBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_Genera_PolizaDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.GeneraPolizaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GeneraPolizaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.GeneraPolizaTableAdapter()
        Me.Clv_llave_polizaTextBox = New System.Windows.Forms.TextBox()
        Clv_llave_polizaLabel = New System.Windows.Forms.Label()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Genera_PolizaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Genera_PolizaBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_Genera_PolizaBindingNavigator.SuspendLayout()
        CType(Me.Consulta_Genera_PolizaDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GeneraPolizaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_llave_polizaLabel
        '
        Clv_llave_polizaLabel.AutoSize = True
        Clv_llave_polizaLabel.Location = New System.Drawing.Point(957, 103)
        Clv_llave_polizaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_llave_polizaLabel.Name = "Clv_llave_polizaLabel"
        Clv_llave_polizaLabel.Size = New System.Drawing.Size(103, 17)
        Clv_llave_polizaLabel.TabIndex = 29
        Clv_llave_polizaLabel.Text = "clv llave poliza:"
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_Genera_PolizaBindingSource
        '
        Me.Consulta_Genera_PolizaBindingSource.DataMember = "Consulta_Genera_Poliza"
        Me.Consulta_Genera_PolizaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Genera_PolizaTableAdapter
        '
        Me.Consulta_Genera_PolizaTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Genera_PolizaBindingNavigator
        '
        Me.Consulta_Genera_PolizaBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.Consulta_Genera_PolizaBindingNavigator.BindingSource = Me.Consulta_Genera_PolizaBindingSource
        Me.Consulta_Genera_PolizaBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.Consulta_Genera_PolizaBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_Genera_PolizaBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.Consulta_Genera_PolizaBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.Consulta_Genera_PolizaBindingNavigatorSaveItem})
        Me.Consulta_Genera_PolizaBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_Genera_PolizaBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.Consulta_Genera_PolizaBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.Consulta_Genera_PolizaBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.Consulta_Genera_PolizaBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.Consulta_Genera_PolizaBindingNavigator.Name = "Consulta_Genera_PolizaBindingNavigator"
        Me.Consulta_Genera_PolizaBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.Consulta_Genera_PolizaBindingNavigator.Size = New System.Drawing.Size(1355, 27)
        Me.Consulta_Genera_PolizaBindingNavigator.TabIndex = 0
        Me.Consulta_Genera_PolizaBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorAddNewItem.Text = "Agregar nuevo"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(48, 24)
        Me.BindingNavigatorCountItem.Text = "de {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Número total de elementos"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveFirstItem.Text = "Mover primero"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMovePreviousItem.Text = "Mover anterior"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Posición"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(65, 27)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Posición actual"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveNextItem.Text = "Mover siguiente"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveLastItem.Text = "Mover último"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'Consulta_Genera_PolizaBindingNavigatorSaveItem
        '
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.Enabled = False
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_Genera_PolizaBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.Name = "Consulta_Genera_PolizaBindingNavigatorSaveItem"
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.Size = New System.Drawing.Size(24, 24)
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Consulta_Genera_PolizaDataGridView
        '
        Me.Consulta_Genera_PolizaDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Consulta_Genera_PolizaDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Consulta_Genera_PolizaDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6})
        Me.Consulta_Genera_PolizaDataGridView.DataSource = Me.Consulta_Genera_PolizaBindingSource
        Me.Consulta_Genera_PolizaDataGridView.Location = New System.Drawing.Point(-185, 0)
        Me.Consulta_Genera_PolizaDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Consulta_Genera_PolizaDataGridView.Name = "Consulta_Genera_PolizaDataGridView"
        Me.Consulta_Genera_PolizaDataGridView.Size = New System.Drawing.Size(1317, 400)
        Me.Consulta_Genera_PolizaDataGridView.TabIndex = 2
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_llave"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clv_llave"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Cuenta"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Cuenta"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Descripcion"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Descripción"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 300
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Parcial"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Parcial"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Debe"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Debe"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Haber"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Haber"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Consulta_Genera_PolizaDataGridView)
        Me.Panel1.Location = New System.Drawing.Point(61, 206)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1132, 404)
        Me.Panel1.TabIndex = 3
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Button5.Location = New System.Drawing.Point(1111, 801)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 28
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'GeneraPolizaBindingSource
        '
        Me.GeneraPolizaBindingSource.DataMember = "GeneraPoliza"
        Me.GeneraPolizaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'GeneraPolizaTableAdapter
        '
        Me.GeneraPolizaTableAdapter.ClearBeforeFill = True
        '
        'Clv_llave_polizaTextBox
        '
        Me.Clv_llave_polizaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GeneraPolizaBindingSource, "clv_llave_poliza", True))
        Me.Clv_llave_polizaTextBox.Location = New System.Drawing.Point(1071, 100)
        Me.Clv_llave_polizaTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_llave_polizaTextBox.Name = "Clv_llave_polizaTextBox"
        Me.Clv_llave_polizaTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clv_llave_polizaTextBox.TabIndex = 30
        '
        'FrmPoliza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 903)
        Me.Controls.Add(Clv_llave_polizaLabel)
        Me.Controls.Add(Me.Clv_llave_polizaTextBox)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Consulta_Genera_PolizaBindingNavigator)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmPoliza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Poliza"
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Genera_PolizaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Genera_PolizaBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_Genera_PolizaBindingNavigator.ResumeLayout(False)
        Me.Consulta_Genera_PolizaBindingNavigator.PerformLayout()
        CType(Me.Consulta_Genera_PolizaDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GeneraPolizaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Consulta_Genera_PolizaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Genera_PolizaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Genera_PolizaTableAdapter
    Friend WithEvents Consulta_Genera_PolizaBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Consulta_Genera_PolizaBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_Genera_PolizaDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents GeneraPolizaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GeneraPolizaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.GeneraPolizaTableAdapter
    Friend WithEvents Clv_llave_polizaTextBox As System.Windows.Forms.TextBox
End Class
