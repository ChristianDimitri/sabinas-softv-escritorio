﻿Public Class FrmHistorialPago

    Private Sub FrmHistorialReimpresion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Try
            BaseII.limpiaParametros()
            cbTipo.DataSource = BaseII.ConsultaDT("MUESTRATIPOFACTURA_Historial")
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
            UspDesactivaBotones(Me, Me.Name)
            lbNombre.ForeColor = lbSerie.ForeColor
            lbNombre.BackColor = lbSerie.BackColor
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cbTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipo.SelectedIndexChanged
        Try
            BuscaFactura("tipo", "", 0, "01/01/1900")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BuscaFactura(op As String, serie As String, folio As Integer, fecha As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@op", SqlDbType.VarChar, op)
        BaseII.CreateMyParameter("@serie", SqlDbType.VarChar, serie)
        BaseII.CreateMyParameter("@folio", SqlDbType.BigInt, folio)
        BaseII.CreateMyParameter("@fecha", SqlDbType.VarChar, fecha)
        BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, cbTipo.SelectedValue)
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CInt(Module1.Contrato))
        DataGridView1.DataSource = BaseII.ConsultaDT("BuscaFacturasHistorial")
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        If tbFolio.Text.Trim.Length > 0 Or tbSerie.Text.Trim.Length > 0 Then
            If tbFolio.Text.Trim.Length > 0 And IsNumeric(tbFolio.Text) Then
                BuscaFactura("seriefolio", tbSerie.Text, tbFolio.Text, "01/01/1900")
            Else
                BuscaFactura("seriefolio", tbSerie.Text, 0, "01/01/1900")
            End If
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If tbFecha.Text.Trim.Length > 0 Then
            Try
                BuscaFactura("fecha", "", 0, tbFecha.Text)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub SplitContainer1_Panel2_Paint(sender As Object, e As PaintEventArgs) Handles SplitContainer1.Panel2.Paint

    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            lbSerie.Text = DataGridView1.SelectedCells(2).Value.ToString
            lbFolio.Text = DataGridView1.SelectedCells(3).Value.ToString
            lbFecha.Text = DataGridView1.SelectedCells(4).Value.ToString
            lbContrato.Text = DataGridView1.SelectedCells(5).Value.ToString
            lbImporte.Text = DataGridView1.SelectedCells(6).Value.ToString
            lbNombre.Text = DataGridView1.SelectedCells(7).Value.ToString
            lbStatus.Text = DataGridView1.SelectedCells(1).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        If DataGridView1.SelectedCells(8).Value.ToString = "N" Then
            gloClvNota = DataGridView1.SelectedCells(0).Value
            LocGloOpRep = 3
            FrmImprimirFac.Show()
        Else

            GloClv_Factura = DataGridView1.SelectedCells(0).Value
            LocGloOpRep = 0
            FrmImprimirFac.Show()
        End If
    End Sub
End Class