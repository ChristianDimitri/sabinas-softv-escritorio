Imports System.Data.SqlClient
Public Class FrmMuestraContrato


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Locbndguardar = True
        eBndEntraDire = True
        OpCoincidencias = 2
        Me.Close()
    End Sub

    Private Sub FrmMuestraContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraNombreContrato(eNombre)
    End Sub

    Private Sub MuestraNombreContrato(ByVal Nombre As String)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraNombreContratoTableAdapter.Connection = CON
            Me.MuestraNombreContratoTableAdapter.Fill(Me.DataSetEric2.MuestraNombreContrato, Nombre)
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Locbndguardar = False
        eBndEntraDire = False
        Me.Close()
    End Sub
End Class