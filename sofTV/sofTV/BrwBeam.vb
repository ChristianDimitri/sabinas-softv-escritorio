﻿Public Class BrwBeam
    Public actualiza As Boolean = False
    Private Sub BrwBeam_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            colorea(Me, Me.Name)
            tbNombre.BackColor = Panel1.BackColor
            tbNombre.ForeColor = lbClave.ForeColor
            buscaCobertura("")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub buscaCobertura(ByVal nombre As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, nombre)
        DataGridView1.DataSource = BaseII.ConsultaDT("BuscaCobertura")
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            If DataGridView1.Rows.Count > 0 Then
                lbClave.Text = DataGridView1.SelectedCells(0).Value.ToString
                tbNombre.Text = DataGridView1.SelectedCells(1).Value.ToString
            End If
        Catch ex As Exception

        End Try
       
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        opcion = "N"
        FrmBeam.idCobertura = 0
        FrmBeam.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        opcion = "C"
        FrmBeam.idCobertura = DataGridView1.SelectedCells(0).Value
        FrmBeam.Show()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        opcion = "M"
        FrmBeam.idCobertura = DataGridView1.SelectedCells(0).Value
        FrmBeam.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox2.Text.Trim.Length > 0 Then
            buscaCobertura(TextBox2.Text)
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwBeam_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        If actualiza Then
            buscaCobertura("")
            actualiza = False
        End If
    End Sub
End Class