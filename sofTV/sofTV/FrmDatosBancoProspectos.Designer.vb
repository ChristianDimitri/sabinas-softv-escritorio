<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDatosBancoProspectos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim CLV_BANCOLabel As System.Windows.Forms.Label
        Dim CUENTA_BANCOLabel As System.Windows.Forms.Label
        Dim TIPO_CUENTALabel As System.Windows.Forms.Label
        Dim VENCIMIENTOLabel As System.Windows.Forms.Label
        Dim CODIGOSEGURIDADLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmDatosBancoProspectos))
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.CONRELCLIBANCOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONRELCLIBANCOTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONRELCLIBANCOTableAdapter
        Me.CONRELCLIBANCOBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.CONRELCLIBANCOBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.ContratoTextBox = New System.Windows.Forms.TextBox
        Me.CODIGOSEGURIDADTextBox = New System.Windows.Forms.TextBox
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox
        Me.TIPODECUENTA = New System.Windows.Forms.ComboBox
        Me.MUESTRATIPOSDECUENTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.MUESTRABANCOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRABANCOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRABANCOSTableAdapter
        Me.MUESTRATIPOSDECUENTATableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOSDECUENTATableAdapter
        Me.MaskedTextBox1 = New System.Windows.Forms.MaskedTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.LblConfCuentaBanc = New System.Windows.Forms.Label
        Me.TxtConfCuentaBanc = New System.Windows.Forms.TextBox
        Me.TxtConfirmacionCodigo = New System.Windows.Forms.TextBox
        Me.LblConfiCodSeg = New System.Windows.Forms.Label
        Me.CUENTA_BANCOTextBox = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        ContratoLabel = New System.Windows.Forms.Label
        CLV_BANCOLabel = New System.Windows.Forms.Label
        CUENTA_BANCOLabel = New System.Windows.Forms.Label
        TIPO_CUENTALabel = New System.Windows.Forms.Label
        VENCIMIENTOLabel = New System.Windows.Forms.Label
        CODIGOSEGURIDADLabel = New System.Windows.Forms.Label
        NOMBRELabel = New System.Windows.Forms.Label
        Label1 = New System.Windows.Forms.Label
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRELCLIBANCOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRELCLIBANCOBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONRELCLIBANCOBindingNavigator.SuspendLayout()
        CType(Me.MUESTRATIPOSDECUENTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRABANCOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.Location = New System.Drawing.Point(132, 3)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(65, 15)
        ContratoLabel.TabIndex = 2
        ContratoLabel.Text = "Contrato:"
        '
        'CLV_BANCOLabel
        '
        CLV_BANCOLabel.AutoSize = True
        CLV_BANCOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CLV_BANCOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CLV_BANCOLabel.Location = New System.Drawing.Point(75, 53)
        CLV_BANCOLabel.Name = "CLV_BANCOLabel"
        CLV_BANCOLabel.Size = New System.Drawing.Size(55, 15)
        CLV_BANCOLabel.TabIndex = 4
        CLV_BANCOLabel.Text = "Banco :"
        '
        'CUENTA_BANCOLabel
        '
        CUENTA_BANCOLabel.AutoSize = True
        CUENTA_BANCOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CUENTA_BANCOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CUENTA_BANCOLabel.Location = New System.Drawing.Point(9, 83)
        CUENTA_BANCOLabel.Name = "CUENTA_BANCOLabel"
        CUENTA_BANCOLabel.Size = New System.Drawing.Size(121, 15)
        CUENTA_BANCOLabel.TabIndex = 6
        CUENTA_BANCOLabel.Text = "Cuenta Bancaria :"
        '
        'TIPO_CUENTALabel
        '
        TIPO_CUENTALabel.AutoSize = True
        TIPO_CUENTALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TIPO_CUENTALabel.ForeColor = System.Drawing.Color.LightSlateGray
        TIPO_CUENTALabel.Location = New System.Drawing.Point(18, 146)
        TIPO_CUENTALabel.Name = "TIPO_CUENTALabel"
        TIPO_CUENTALabel.Size = New System.Drawing.Size(112, 15)
        TIPO_CUENTALabel.TabIndex = 8
        TIPO_CUENTALabel.Text = "Tipo de Cuenta :"
        '
        'VENCIMIENTOLabel
        '
        VENCIMIENTOLabel.AutoSize = True
        VENCIMIENTOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        VENCIMIENTOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        VENCIMIENTOLabel.Location = New System.Drawing.Point(36, 171)
        VENCIMIENTOLabel.Name = "VENCIMIENTOLabel"
        VENCIMIENTOLabel.Size = New System.Drawing.Size(94, 15)
        VENCIMIENTOLabel.TabIndex = 10
        VENCIMIENTOLabel.Text = "Vencimiento :"
        '
        'CODIGOSEGURIDADLabel
        '
        CODIGOSEGURIDADLabel.AutoSize = True
        CODIGOSEGURIDADLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CODIGOSEGURIDADLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CODIGOSEGURIDADLabel.Location = New System.Drawing.Point(36, 197)
        CODIGOSEGURIDADLabel.Name = "CODIGOSEGURIDADLabel"
        CODIGOSEGURIDADLabel.Size = New System.Drawing.Size(150, 15)
        CODIGOSEGURIDADLabel.TabIndex = 12
        CODIGOSEGURIDADLabel.Text = "Codigo de Seguridad :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.ForeColor = System.Drawing.Color.LightSlateGray
        NOMBRELabel.Location = New System.Drawing.Point(64, 23)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(66, 15)
        NOMBRELabel.TabIndex = 14
        NOMBRELabel.Text = "Nombre :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(192, 171)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(151, 15)
        Label1.TabIndex = 17
        Label1.Text = "Mes / Año Ejem. 01/07"
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONRELCLIBANCOBindingSource
        '
        Me.CONRELCLIBANCOBindingSource.DataMember = "CONRELCLIBANCO"
        Me.CONRELCLIBANCOBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONRELCLIBANCOTableAdapter
        '
        Me.CONRELCLIBANCOTableAdapter.ClearBeforeFill = True
        '
        'CONRELCLIBANCOBindingNavigator
        '
        Me.CONRELCLIBANCOBindingNavigator.AddNewItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.BindingSource = Me.CONRELCLIBANCOBindingSource
        Me.CONRELCLIBANCOBindingNavigator.CountItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONRELCLIBANCOBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONRELCLIBANCOBindingNavigatorSaveItem})
        Me.CONRELCLIBANCOBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONRELCLIBANCOBindingNavigator.MoveFirstItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.MoveLastItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.MoveNextItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.MovePreviousItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.Name = "CONRELCLIBANCOBindingNavigator"
        Me.CONRELCLIBANCOBindingNavigator.PositionItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONRELCLIBANCOBindingNavigator.Size = New System.Drawing.Size(609, 25)
        Me.CONRELCLIBANCOBindingNavigator.TabIndex = 6
        Me.CONRELCLIBANCOBindingNavigator.TabStop = True
        Me.CONRELCLIBANCOBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONRELCLIBANCOBindingNavigatorSaveItem
        '
        Me.CONRELCLIBANCOBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONRELCLIBANCOBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONRELCLIBANCOBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONRELCLIBANCOBindingNavigatorSaveItem.Name = "CONRELCLIBANCOBindingNavigatorSaveItem"
        Me.CONRELCLIBANCOBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.CONRELCLIBANCOBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONRELCLIBANCOBindingSource, "Contrato", True))
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(203, 0)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ContratoTextBox.TabIndex = 3
        Me.ContratoTextBox.TabStop = False
        '
        'CODIGOSEGURIDADTextBox
        '
        Me.CODIGOSEGURIDADTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CODIGOSEGURIDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONRELCLIBANCOBindingSource, "CODIGOSEGURIDAD", True))
        Me.CODIGOSEGURIDADTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CODIGOSEGURIDADTextBox.Location = New System.Drawing.Point(191, 194)
        Me.CODIGOSEGURIDADTextBox.MaxLength = 7
        Me.CODIGOSEGURIDADTextBox.Name = "CODIGOSEGURIDADTextBox"
        Me.CODIGOSEGURIDADTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.CODIGOSEGURIDADTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CODIGOSEGURIDADTextBox.TabIndex = 6
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONRELCLIBANCOBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(136, 23)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(395, 21)
        Me.NOMBRETextBox.TabIndex = 0
        '
        'TIPODECUENTA
        '
        Me.TIPODECUENTA.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONRELCLIBANCOBindingSource, "TIPO_CUENTA", True))
        Me.TIPODECUENTA.DataSource = Me.MUESTRATIPOSDECUENTABindingSource
        Me.TIPODECUENTA.DisplayMember = "NOMBRE"
        Me.TIPODECUENTA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TIPODECUENTA.FormattingEnabled = True
        Me.TIPODECUENTA.Location = New System.Drawing.Point(136, 143)
        Me.TIPODECUENTA.Name = "TIPODECUENTA"
        Me.TIPODECUENTA.Size = New System.Drawing.Size(240, 23)
        Me.TIPODECUENTA.TabIndex = 4
        Me.TIPODECUENTA.ValueMember = "NOMBRE"
        '
        'MUESTRATIPOSDECUENTABindingSource
        '
        Me.MUESTRATIPOSDECUENTABindingSource.DataMember = "MUESTRATIPOSDECUENTA"
        Me.MUESTRATIPOSDECUENTABindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(451, 296)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 9
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONRELCLIBANCOBindingSource, "CLV_BANCO", True))
        Me.ComboBox1.DataSource = Me.MUESTRABANCOSBindingSource
        Me.ComboBox1.DisplayMember = "nombre"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(136, 50)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(257, 24)
        Me.ComboBox1.TabIndex = 1
        Me.ComboBox1.ValueMember = "Clave"
        '
        'MUESTRABANCOSBindingSource
        '
        Me.MUESTRABANCOSBindingSource.DataMember = "MUESTRABANCOS"
        Me.MUESTRABANCOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRABANCOSTableAdapter
        '
        Me.MUESTRABANCOSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOSDECUENTATableAdapter
        '
        Me.MUESTRATIPOSDECUENTATableAdapter.ClearBeforeFill = True
        '
        'MaskedTextBox1
        '
        Me.MaskedTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONRELCLIBANCOBindingSource, "VENCIMIENTO", True))
        Me.MaskedTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaskedTextBox1.Location = New System.Drawing.Point(136, 171)
        Me.MaskedTextBox1.Mask = "99/99"
        Me.MaskedTextBox1.Name = "MaskedTextBox1"
        Me.MaskedTextBox1.Size = New System.Drawing.Size(50, 21)
        Me.MaskedTextBox1.TabIndex = 5
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.LblConfCuentaBanc)
        Me.Panel1.Controls.Add(Me.TxtConfCuentaBanc)
        Me.Panel1.Controls.Add(Me.TxtConfirmacionCodigo)
        Me.Panel1.Controls.Add(Me.LblConfiCodSeg)
        Me.Panel1.Controls.Add(Me.CUENTA_BANCOTextBox)
        Me.Panel1.Controls.Add(Me.MaskedTextBox1)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Label1)
        Me.Panel1.Controls.Add(Me.TIPODECUENTA)
        Me.Panel1.Controls.Add(CLV_BANCOLabel)
        Me.Panel1.Controls.Add(CUENTA_BANCOLabel)
        Me.Panel1.Controls.Add(TIPO_CUENTALabel)
        Me.Panel1.Controls.Add(VENCIMIENTOLabel)
        Me.Panel1.Controls.Add(CODIGOSEGURIDADLabel)
        Me.Panel1.Controls.Add(Me.CODIGOSEGURIDADTextBox)
        Me.Panel1.Controls.Add(NOMBRELabel)
        Me.Panel1.Controls.Add(Me.NOMBRETextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 36)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(557, 250)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'LblConfCuentaBanc
        '
        Me.LblConfCuentaBanc.AutoSize = True
        Me.LblConfCuentaBanc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblConfCuentaBanc.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LblConfCuentaBanc.Location = New System.Drawing.Point(9, 110)
        Me.LblConfCuentaBanc.Name = "LblConfCuentaBanc"
        Me.LblConfCuentaBanc.Size = New System.Drawing.Size(183, 15)
        Me.LblConfCuentaBanc.TabIndex = 20
        Me.LblConfCuentaBanc.Text = "Confirme Cuenta Bancaria :"
        Me.LblConfCuentaBanc.Visible = False
        '
        'TxtConfCuentaBanc
        '
        Me.TxtConfCuentaBanc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtConfCuentaBanc.Location = New System.Drawing.Point(198, 109)
        Me.TxtConfCuentaBanc.MaxLength = 16
        Me.TxtConfCuentaBanc.Name = "TxtConfCuentaBanc"
        Me.TxtConfCuentaBanc.Size = New System.Drawing.Size(217, 21)
        Me.TxtConfCuentaBanc.TabIndex = 3
        Me.TxtConfCuentaBanc.Visible = False
        '
        'TxtConfirmacionCodigo
        '
        Me.TxtConfirmacionCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtConfirmacionCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtConfirmacionCodigo.Location = New System.Drawing.Point(254, 226)
        Me.TxtConfirmacionCodigo.MaxLength = 7
        Me.TxtConfirmacionCodigo.Name = "TxtConfirmacionCodigo"
        Me.TxtConfirmacionCodigo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TxtConfirmacionCodigo.Size = New System.Drawing.Size(100, 21)
        Me.TxtConfirmacionCodigo.TabIndex = 7
        Me.TxtConfirmacionCodigo.Visible = False
        '
        'LblConfiCodSeg
        '
        Me.LblConfiCodSeg.AutoSize = True
        Me.LblConfiCodSeg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblConfiCodSeg.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LblConfiCodSeg.Location = New System.Drawing.Point(36, 227)
        Me.LblConfiCodSeg.Name = "LblConfiCodSeg"
        Me.LblConfiCodSeg.Size = New System.Drawing.Size(212, 15)
        Me.LblConfiCodSeg.TabIndex = 18
        Me.LblConfiCodSeg.Text = "Confirme Código de Seguridad :"
        Me.LblConfiCodSeg.Visible = False
        '
        'CUENTA_BANCOTextBox
        '
        Me.CUENTA_BANCOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONRELCLIBANCOBindingSource, "CUENTA_BANCO", True))
        Me.CUENTA_BANCOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CUENTA_BANCOTextBox.Location = New System.Drawing.Point(136, 80)
        Me.CUENTA_BANCOTextBox.MaxLength = 16
        Me.CUENTA_BANCOTextBox.Name = "CUENTA_BANCOTextBox"
        Me.CUENTA_BANCOTextBox.Size = New System.Drawing.Size(240, 21)
        Me.CUENTA_BANCOTextBox.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(298, 296)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 33)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "&IMPRIMIR"
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'FrmDatosBancoProspectos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(609, 337)
        Me.Controls.Add(Me.CONRELCLIBANCOBindingNavigator)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(ContratoLabel)
        Me.Controls.Add(Me.ContratoTextBox)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmDatosBancoProspectos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos Bancarios del Prospecto"
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRELCLIBANCOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRELCLIBANCOBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONRELCLIBANCOBindingNavigator.ResumeLayout(False)
        Me.CONRELCLIBANCOBindingNavigator.PerformLayout()
        CType(Me.MUESTRATIPOSDECUENTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRABANCOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONRELCLIBANCOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRELCLIBANCOTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONRELCLIBANCOTableAdapter
    Friend WithEvents CONRELCLIBANCOBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONRELCLIBANCOBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CODIGOSEGURIDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents TIPODECUENTA As System.Windows.Forms.ComboBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRABANCOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRABANCOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRABANCOSTableAdapter
    Friend WithEvents MUESTRATIPOSDECUENTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATIPOSDECUENTATableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOSDECUENTATableAdapter
    Friend WithEvents MaskedTextBox1 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CUENTA_BANCOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TxtConfCuentaBanc As System.Windows.Forms.TextBox
    Friend WithEvents TxtConfirmacionCodigo As System.Windows.Forms.TextBox
    Friend WithEvents LblConfCuentaBanc As System.Windows.Forms.Label
    Friend WithEvents LblConfiCodSeg As System.Windows.Forms.Label
End Class
