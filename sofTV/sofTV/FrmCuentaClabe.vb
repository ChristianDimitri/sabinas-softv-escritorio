﻿Public Class FrmCuentaClabe

    Private Sub ClaveLabel_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub FrmCuentaClabe_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            BindingNavigatorDeleteItem.Enabled = False
            tbContrato.Visible = False
            lbContrato.Visible = False
        ElseIf opcion = "M" Then
            tbId.Text = gloClave.ToString
            llenaDatos()
        ElseIf opcion = "C" Then
            tbId.Text = gloClave.ToString
            BindingNavigatorDeleteItem.Enabled = False
            CONSERVICIOSBindingNavigatorSaveItem.Enabled = False
            llenaDatos()
            tbnombre.Enabled = False
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub llenaDatos()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.Int, tbId.Text)
            BaseII.CreateMyParameter("@clabe", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.CreateMyParameter("@contrato", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.ProcedimientoOutPut("ObtieneDatosClabe")
            tbnombre.Text = BaseII.dicoPar("@clabe").ToString
            tbContrato.Text = BaseII.dicoPar("@contrato").ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        If tbnombre.Text.Trim = "" Then
            MsgBox("Debe ingresar la cuenta Clabe para continuar.")
            Exit Sub
        End If
        If tbId.Text.Trim = "" Or Not IsNumeric(tbId.Text) Or tbId.Text <= 0 Then
            MsgBox("Debe ingresar el ID, teniendo en cuenta que debe ser un número entero positivo.")
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.Int, tbId.Text)
            BaseII.CreateMyParameter("@clabe", SqlDbType.VarChar, tbnombre.Text)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@op", SqlDbType.VarChar, opcion)
            BaseII.ProcedimientoOutPut("GuardaClabe")
            If BaseII.dicoPar("@error") = 1 Then
                MsgBox("Ya existe un registro con alguno de los datos que se están ingresando. Vuelve a intentarlo.")
            Else
                MsgBox("Cuenta Clabe guardada con éxito.")
                Locbndactualiza = True
                Me.Close()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.Int, tbId.Text)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("EliminaClabe")
            If BaseII.dicoPar("@error") = 1 Then
                MsgBox("La Cuenta Clabe ya está registrada a un contrato.")
            Else
                MsgBox("Cuenta Clabe eliminada con éxito.")
                Locbndactualiza = True
                Me.Close()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub
End Class
