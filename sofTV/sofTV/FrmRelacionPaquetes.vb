﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Public Class FrmRelacionPaquetes
    Dim Clv_servicioPrinLoc As Integer

    Private Sub FrmRelacionPaquetes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        llenaServiciosPrincipales()

        UspDesactivaBotones(Me, Me.Name)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub llenaServiciosPrincipales()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.VarChar, GloUsuario, 10)
        DataGridView1.DataSource = BaseII.ConsultaDT("DameServiciosPrincipalesDigital")
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Clv_servicioPrinLoc = DataGridView1.SelectedRows(0).Cells("Clv_Servicio").Value.ToString()

            DameServiciosAdicionalesRelPaquetes()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DameServiciosAdicionalesRelPaquetes()
        If Not IsNumeric(Clv_servicioPrinLoc) Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicioPrincipal", SqlDbType.BigInt, Clv_servicioPrinLoc)


        Dim DS As New DataSet
        DS.Clear()
        Dim listatablas As New List(Of String)
        listatablas.Add("SinAsignar")
        listatablas.Add("Asignada")

        DS = BaseII.ConsultaDS("DameServiciosAdicionalesRelPrincipal", listatablas)

        ComboBoxServiciosAdicionales.DataSource = DS.Tables.Item("SinAsignar")
        ComboBoxServiciosAdicionales.DisplayMember = "Descripcion"
        ComboBoxServiciosAdicionales.ValueMember = "Clv_Servicio"
        If ComboBoxServiciosAdicionales.Items.Count = 0 Then
            ComboBoxServiciosAdicionales.Text = ""
        End If

        DataGridView2.DataSource = DS.Tables.Item("Asignada")
    End Sub

    Private Sub BtnAgregar_Click(sender As Object, e As EventArgs) Handles BtnAgregar.Click
        If Not IsNumeric(ComboBoxServiciosAdicionales.SelectedValue) Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicioPrincipal", SqlDbType.BigInt, Clv_servicioPrinLoc)
        BaseII.CreateMyParameter("@clv_servicioAdicional", SqlDbType.BigInt, ComboBoxServiciosAdicionales.SelectedValue)
        BaseII.Inserta("InsertaRelPaquetePrincipalAdicional")
        DameServiciosAdicionalesRelPaquetes()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Not IsNumeric(DataGridView2.CurrentRow.Cells(0).Value.ToString) Then

            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicioPrincipal", SqlDbType.BigInt, Clv_servicioPrinLoc)
        BaseII.CreateMyParameter("@clv_servicioAdicional", SqlDbType.BigInt, DataGridView2.CurrentRow.Cells(0).Value.ToString)
        BaseII.Inserta("EliminaRelPaquetePrincipalAdicional")
        DameServiciosAdicionalesRelPaquetes()
    End Sub


    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.Close()
    End Sub
End Class