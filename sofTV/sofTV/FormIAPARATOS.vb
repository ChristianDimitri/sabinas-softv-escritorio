﻿Imports System.Data.SqlClient

Public Class FormIAPARATOS
    Private Aparato815 As Boolean = False 'Se usaba para pedir el nodeo y equipo de cliente, ahora se usa para pedir mac wan

    Private Sub SP_CONSULTAIAPARATOS(ByVal oClv_Orden As Long, oClave As Long)
        Try

        
        '@Clv_Orden Bigint=0,@Clave Bigint=0,@Contratonet bigint =0 OUTPUT,@Clv_Aparato bigint=0 OUTPUT
            Dim oContratonet As Long = 0
            Dim oClv_Aparato As Long = 0
            Dim oStatus As String = ""

            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@Contratonet", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@Clv_Aparato", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@Status", ParameterDirection.Output, SqlDbType.VarChar, 1)
            BaseII.ProcedimientoOutPut("SP_CONSULTAIAPARATOS")
            oContratonet = BaseII.dicoPar("@Contratonet").ToString
            oClv_Aparato = BaseII.dicoPar("@Clv_Aparato").ToString
            oStatus = BaseII.dicoPar("@Status").ToString
            If oStatus.Length > 0 Then
                CMBoxEstadoAparato.SelectedValue = oStatus
            End If
            If oContratonet > 0 Then
                ComboBoxPorAsignar.SelectedValue = oContratonet
            End If
            If oClv_Aparato > 0 Then
                ComboBoxAparatosDisponibles.SelectedValue = oClv_Aparato
            End If
            If oClv_Aparato > 0 Then
                ComboBoxAparatosDisponibles.SelectedValue = oClv_Aparato
                consultaMacWanPorOrden(oClv_Orden, oClave, oClv_Aparato)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub consultaMacWanPorOrden(ByVal oClv_Orden As Long, oClave As Long, oClv_Aparato As Long)
        'If Aparato815 Then
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
        BaseII.CreateMyParameter("@MacWan", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("consultaMacWanPorOrden")
        TextBoxWan.Text = BaseII.dicoPar("@MacWan").ToString
        ' End If

    End Sub

    Private Sub Llena_AparatosporAsignar(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)

            ComboBoxPorAsignar.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxPorAsignar.DisplayMember = "Descripcion"
            ComboBoxPorAsignar.ValueMember = "ContratoAnt"

            If ComboBoxPorAsignar.Items.Count > 0 Then
                ComboBoxPorAsignar.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_AparatosporAsignarCables(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)

            ComboBoxPorAsignar.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxPorAsignar.DisplayMember = "Descripcion"
            ComboBoxPorAsignar.ValueMember = "ContratoAnt"

            If ComboBoxPorAsignar.Items.Count > 0 Then
                ComboBoxPorAsignar.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_AparatosporAsignarControlRemoto(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)

            ComboBoxPorAsignar.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxPorAsignar.DisplayMember = "Descripcion"
            ComboBoxPorAsignar.ValueMember = "ContratoAnt"

            If ComboBoxPorAsignar.Items.Count > 0 Then
                ComboBoxPorAsignar.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_EstadoAparato(oTrabajo As String)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            If oTrabajo = "CAPRO" Then
                CMBoxEstadoAparato.DataSource = BaseII.ConsultaDT("SP_StatusAparatosRobados")
            Else
                CMBoxEstadoAparato.DataSource = BaseII.ConsultaDT("SP_StatusAparatos")
            End If

            CMBoxEstadoAparato.DisplayMember = "Concepto"
            CMBoxEstadoAparato.ValueMember = "Clv_StatusCableModem"

            If CMBoxEstadoAparato.Items.Count > 0 Then
                CMBoxEstadoAparato.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""


        Catch ex As Exception

        End Try
    End Sub

    Private Sub llena_TipoAparato(oTrabajo As String)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_orden", SqlDbType.BigInt, gloClv_Orden)
            BaseII.CreateMyParameter("@idFibra", SqlDbType.BigInt, ComboBoxPorAsignar.SelectedValue)
            BaseII.CreateMyParameter("@trabajo", SqlDbType.VarChar, oTrabajo, 10)
            ComboBoxTipoAparato.DataSource = BaseII.ConsultaDT("SP_DameLosPosiblesTiposAparato")
            ComboBoxTipoAparato.DisplayMember = "concepto"
            ComboBoxTipoAparato.ValueMember = "TipoAparato"

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_AparatosporDisponiblesCables(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            ComboBoxCables.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxCables.DisplayMember = "Descripcion"
            ComboBoxCables.ValueMember = "ContratoAnt"

            If ComboBoxCables.Items.Count > 0 Then
                ComboBoxCables.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_AparatosporDisponiblesControlRemoto(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            ComboBoxControlRemoto.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxControlRemoto.DisplayMember = "Descripcion"
            ComboBoxControlRemoto.ValueMember = "ContratoAnt"

            If ComboBoxControlRemoto.Items.Count > 0 Then
                ComboBoxControlRemoto.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_AparatosporDisponibles(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, ComboBoxTipoAparato.SelectedValue, 10)
            ComboBoxAparatosDisponibles.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxAparatosDisponibles.DisplayMember = "Descripcion"
            ComboBoxAparatosDisponibles.ValueMember = "ContratoAnt"

            If ComboBoxAparatosDisponibles.Items.Count > 0 Then
                ComboBoxAparatosDisponibles.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SP_GuardaIAPARATOS(oClave As Long, oTrabajo As String, oClv_Orden As Long, oContratonet As Long, oClv_Aparato As Long, oOpcion As String, oStatus As String)
        '@Clave bigint=0,@Trabajo varchar(10)='',@Clv_Orden bigint=0,@Contratonet Bigint=0,@Clv_Aparato bigint=0,@Opcion varchar(10)=''
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Trabajo", SqlDbType.VarChar, oTrabajo, 10)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.VarChar, oOpcion, 10)
        BaseII.CreateMyParameter("@Status", SqlDbType.VarChar, oStatus, 1)
        BaseII.Inserta("SP_GuardaIAPARATOS")
    End Sub

    Private Sub SP_GuardaMacWan(oClave As Long, oClv_Orden As Long, oClv_Aparato As Long, oMacLan As String, oMacWan As String)
        If Aparato815 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
            BaseII.CreateMyParameter("@MacLan", SqlDbType.VarChar, oMacLan, 50)
            BaseII.CreateMyParameter("@MacWan", SqlDbType.VarChar, oMacWan, 50)
            BaseII.Inserta("SP_GuardaMacWan")
        End If

    End Sub

    Private Sub CONCCABMBindingNavigatorSaveItem_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub FormIAPARATOS_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Aparato815 = False
        Llena_AparatosporAsignar(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, 0, gloClv_Orden, GloDetClave)
        Llena_AparatosporDisponibles(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
        Llena_EstadoAparato(GLOTRABAJO)
        llena_TipoAparato(GLOTRABAJO)
        Me.Text = GLONOMTRABAJO

        SP_CONSULTAIAPARATOS(gloClv_Orden, GloDetClave)
        LblEstadoAparato.Visible = False
        CMBoxEstadoAparato.Visible = False
        CMBPanelAccesorios.Visible = False

        If GLOTRABAJO = "CANTE" Or GLOTRABAJO = "CALNB" Or GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPRO" Then
            LblEstadoAparato.Visible = True
            CMBoxEstadoAparato.Visible = True
            If GLOTRABAJO = "CANTE" Then
                Label4.Text = "Antena asignada actualmente"
                LblEstadoAparato.Text = "Seleccione el estado de la antena"
                Label1.Text = "Seleccione la antena a instalar"

            ElseIf GLOTRABAJO = "CALNB" Then
                Label4.Text = "LNB asignado actualmente"
                LblEstadoAparato.Text = "Seleccione el estado del LNB"
                Label1.Text = "Seleccione el LNB a instalar"
            ElseIf GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPRO" Then
                Label4.Text = "Setupbox asignado actualmente"
                LblEstadoAparato.Text = "Seleccione el estado del Setupbox"
                Label1.Text = "Seleccione el Setupbox a instalar"
                'CMBPanelAccesorios.Visible = True
                'Cables
                'Llena_AparatosporAsignarCables(opcion & "CA", GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, 0, gloClv_Orden, GloDetClave)
                Llena_AparatosporDisponiblesCables(opcion & "CA", GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
                'ControlRemoto
                'Llena_AparatosporAsignarControlRemoto(opcion & "CO", GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, 0, gloClv_Orden, GloDetClave)
                Llena_AparatosporDisponiblesControlRemoto(opcion & "CO", GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
            End If


        End If

        If GLOTRABAJO = "CANTX" Or GLOTRABAJO = "CRADI" Or GLOTRABAJO = "CCABM" Or GLOTRABAJO = "CROUT" Or GLOTRABAJO = "CAPAG" Or GLOTRABAJO = "CAPAF" Then
            LblEstadoAparato.Visible = True
            CMBoxEstadoAparato.Visible = True
            If GLOTRABAJO = "CANTX" Then
                Label4.Text = "Antena asignada actualmente"
                LblEstadoAparato.Text = "Seleccione el estado de la antena"
                Label1.Text = "Seleccione la antena a instalar"
            ElseIf GLOTRABAJO = "CRADI" Then
                Label4.Text = "Radio asignado actualmente"
                LblEstadoAparato.Text = "Seleccione el estado del Radio"
                Label1.Text = "Seleccione el Radio a instalar"
            ElseIf GLOTRABAJO = "CCABM" Then
                Label4.Text = "Cablemódem asignado actualmente"
                LblEstadoAparato.Text = "Seleccione el estado del Cablemódem"
                Label1.Text = "Seleccione el Cablemódem a instalar"
            ElseIf GLOTRABAJO = "CROUT" Then
                Label4.Text = "Router asignado actualmente"
                LblEstadoAparato.Text = "Seleccione el estado del Router"
                Label1.Text = "Seleccione el Router a instalar"
            ElseIf GLOTRABAJO = "CROUT" Then
                Label4.Text = "Aparato asignado actualmente"
                LblEstadoAparato.Text = "Seleccione el estado del Aparato"
                Label1.Text = "Seleccione el Aparato a instalar"
            End If
        End If

        If GLOTRABAJO = "IAPAG" Or GLOTRABAJO = "CAPAG" Or GLOTRABAJO = "CAPAF" Then
            LbTipoAparato.Visible = True
            ComboBoxTipoAparato.Visible = True
        End If

        If GLOTRABAJO = "IANTX" Or GLOTRABAJO = "CANTX" Then
            Aparato815 = True
        ElseIf GLOTRABAJO = "IAPAG" Or GLOTRABAJO = "CAPAG" Or GLOTRABAJO = "CAPAF" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idFibra", SqlDbType.BigInt, ComboBoxPorAsignar.SelectedValue)
            BaseII.CreateMyParameter("@BND", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("checaSiEsAparato815")
            If CBool(BaseII.dicoPar("@BND").ToString) Then
                Aparato815 = True
            End If
        Else
            Aparato815 = False
        End If

        If Aparato815 Then
            'LabelNodo.Visible = True
            'LabelEquipo815.Visible = True
            'cbNodo.Visible = True
            'cbEquipo815.Visible = True

            'Dame_NododeRed()
            'Dame_EquiposCliente()
            LabelWan.Visible = True
            TextBoxWan.Visible = True
        Else
            'LabelNodo.Visible = False
            'LabelEquipo815.Visible = False
            'cbNodo.Visible = False
            'cbEquipo815.Visible = False
            LabelWan.Visible = False
            TextBoxWan.Visible = False
        End If

        If opcion = "M" Or opcion = "C" Then
            If FrmOrdSer.Panel6.Enabled = False Then
                ComboBoxAparatosDisponibles.Enabled = False
                ComboBoxPorAsignar.Enabled = False
                BtnAceptar.Enabled = False
                CMBoxEstadoAparato.Enabled = False
            End If
        End If


    End Sub

    Private Sub BtnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles BtnAceptar.Click
        If GLOTRABAJO = "CANTE" Or GLOTRABAJO = "CALNB" Or GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CCABM" Or GLOTRABAJO = "CANTX" Or GLOTRABAJO = "CRADI" Or GLOTRABAJO = "CROUT" Or GLOTRABAJO = "CAPRO" Then
            If CMBoxEstadoAparato.SelectedIndex = -1 Then
                MsgBox("Seleccione el Estado del Aparato a Cambiar ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxAparatosDisponibles.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Cambiar ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxPorAsignar.SelectedIndex = -1 Then
                MsgBox("Seleccione el Nuevo Aparato ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            'If GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPRO" Then
            '    If ComboBoxCables.SelectedIndex = -1 Then
            '        MsgBox("Seleccione el Accesorio Cable ", MsgBoxStyle.Information, "Información")
            '        Exit Sub
            '    End If
            '    If ComboBoxControlRemoto.SelectedIndex = -1 Then
            '        MsgBox("Seleccione el Accesorio Control Remoto ", MsgBoxStyle.Information, "Información")
            '        Exit Sub
            '    End If
            'End If

        Else
            If ComboBoxAparatosDisponibles.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Instalar", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxPorAsignar.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
        End If

        If Aparato815 Then
            'If (cbEquipo815.SelectedValue = 0 Or cbNodo.SelectedValue = 0) Then
            '    MsgBox("Seleccione el nodo o equipo del cliente ", MsgBoxStyle.Information, "Información")
            '    Exit Sub
            'End If

            If TextBoxWan.Text.Length = 0 Then
                MsgBox("Falta la dirección MAC WAN", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If

            If TextBoxWan.Text.Length <> 12 Then
                MsgBox("Formato incorrecto de la dirección MAC WAN", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If

            If TextBoxWan.Text = ComboBoxAparatosDisponibles.Text Then
                MsgBox("La dirección MAC LAN y MAC WAN no pueder ser iguales", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If

            If Not validaWAN() Then
                MsgBox("Ya existe esa MAC WAN registrada con otro equipo", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
        End If

        SP_GuardaIAPARATOS(GloDetClave, GLOTRABAJO, gloClv_Orden, ComboBoxPorAsignar.SelectedValue, ComboBoxAparatosDisponibles.SelectedValue, opcion, CMBoxEstadoAparato.SelectedValue)
        'If GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPRO" Then
        '    SP_GuardaIAPARATOS(GloDetClave, "ECABL", gloClv_Orden, ComboBoxPorAsignar.SelectedValue, ComboBoxCables.SelectedValue, opcion & "CA", "")
        '    SP_GuardaIAPARATOS(GloDetClave, "ECONT", gloClv_Orden, ComboBoxPorAsignar.SelectedValue, ComboBoxControlRemoto.SelectedValue, opcion & "CO", "")
        'End If
        If Aparato815 Then
            'NueNodoEquipCli()
            SP_GuardaMacWan(GloDetClave, gloClv_Orden, ComboBoxAparatosDisponibles.SelectedValue, ComboBoxAparatosDisponibles.Text, TextBoxWan.Text)

        End If


        Me.Close()
        GloBndCoordenadas = True
    End Sub

    Private Function validaWAN() As Boolean
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, ComboBoxAparatosDisponibles.SelectedValue)
        BaseII.CreateMyParameter("@MacWan", SqlDbType.VarChar, TextBoxWan.Text, 50)
        BaseII.CreateMyParameter("@correcta", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("validaWAN")
        Return CBool(BaseII.dicoPar("@correcta").ToString)
    End Function

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Me.Close()
        GloBndCoordenadas = False
    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub

    Private Sub ComboBoxTipoAparato_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxTipoAparato.SelectedValueChanged
        Llena_AparatosporDisponibles(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
       
    End Sub

    Private Sub Dame_NododeRed()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, gloClv_Orden)
            cbNodo.DataSource = BaseII.ConsultaDT("Dame_NododeRed")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Dame_EquiposCliente()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, gloClv_Orden)
            cbEquipo815.DataSource = BaseII.ConsultaDT("Dame_EquiposCliente")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub NueNodoEquipCli()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@NodoRed", SqlDbType.BigInt, cbNodo.SelectedValue)
        BaseII.CreateMyParameter("@EquipCli", SqlDbType.BigInt, cbEquipo815.SelectedValue)
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, GloDetClave)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, gloClv_Orden)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, ComboBoxPorAsignar.SelectedValue)
        BaseII.Inserta("NueNodoEquipCli")
    End Sub

    Private Sub ComboBoxAparatosDisponibles_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxAparatosDisponibles.SelectedValueChanged
        If IsNumeric(ComboBoxAparatosDisponibles.SelectedValue) Then
            If ComboBoxAparatosDisponibles.SelectedValue > 0 Then
                consultaMacWanPorOrden(gloClv_Orden, GloDetClave, ComboBoxAparatosDisponibles.SelectedValue)
            End If
        End If
    End Sub
End Class