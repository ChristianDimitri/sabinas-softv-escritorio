﻿Public Class FrmIdentificacionUsuario

    Private Sub CONVENDEDORESBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles CONVENDEDORESBindingNavigatorSaveItem.Click
        If tbNombre.Text.Trim = "" Then
            MsgBox("Es necesario llenar el nombre.")
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_identificacion", SqlDbType.Int, tbClave.Text)
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, tbNombre.Text)
        BaseII.CreateMyParameter("@agregaCompanias", SqlDbType.Bit, CBool(ckAgregar.Checked))
        BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("GuardaIdentificacionUsuario")
        If BaseII.dicoPar("@error") = 1 Then
            MsgBox("El nombre que intenta registrar ya existe, inténtelo nuevamente.")
            Exit Sub
        Else
            MsgBox("Guardado con éxito.")
            Me.Close()
            BrwIdentificacionUsuario.buscaIdentificacion(0, "")
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmIdentificacionUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        tbClave.Text = GloClvIdentificacionUsario.ToString
        If opcion = "N" Then
            BindingNavigatorDeleteItem.Enabled = False
        ElseIf opcion = "M" Then
            llenaDatos()
            BindingNavigatorDeleteItem.Enabled = True
        ElseIf opcion = "C" Then
            llenaDatos()
            CONVENDEDORESBindingNavigatorSaveItem.Enabled = False
            BindingNavigatorDeleteItem.Enabled = False
            ckAgregar.Enabled = False
            tbNombre.Enabled = False
        End If
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_identificacion", SqlDbType.Int, tbClave.Text)
        BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("EliminaIdentificacionUsuario")
        If BaseII.dicoPar("@error") = 1 Then
            MsgBox("Ya existe un usuario relacionado con esta identificación. No se puede eliminar.")
        Else
            MsgBox("Eliminado exitosamente.")
            Me.Close()
            BrwIdentificacionUsuario.buscaIdentificacion(0, "")
        End If
    End Sub

    Private Sub llenaDatos()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_identificacion", SqlDbType.Int, tbClave.Text)
        BaseII.CreateMyParameter("@nombre", ParameterDirection.Output, SqlDbType.VarChar, 100)
        BaseII.CreateMyParameter("@agregarCompanias", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("ObtieneIdentificacionUsuario")
        tbNombre.Text = BaseII.dicoPar("@nombre")
        ckAgregar.Checked = BaseII.dicoPar("@agregarCompanias")
    End Sub
End Class