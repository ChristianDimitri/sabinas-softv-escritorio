﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConceptosPoliza
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.PosicionText = New System.Windows.Forms.TextBox()
        Me.ConceptoText = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.IdConceptoText = New System.Windows.Forms.TextBox()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.IdGrupoCombo = New System.Windows.Forms.ComboBox()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.Clv_ServicioComboBox = New System.Windows.Forms.ComboBox()
        Me.CuentaTextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel53 = New System.Windows.Forms.Label()
        Me.IdProgramadaTextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel66 = New System.Windows.Forms.Label()
        Me.ActivoCheckBox = New System.Windows.Forms.CheckBox()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.gbxConcepto = New System.Windows.Forms.GroupBox()
        Me.cmbCuidadPoliza = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SubcuentaTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbxConcepto.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(27, 83)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(80, 16)
        Me.CMBLabel2.TabIndex = 22
        Me.CMBLabel2.Text = "Posición : "
        '
        'PosicionText
        '
        Me.PosicionText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PosicionText.Location = New System.Drawing.Point(113, 82)
        Me.PosicionText.Name = "PosicionText"
        Me.PosicionText.Size = New System.Drawing.Size(124, 21)
        Me.PosicionText.TabIndex = 21
        '
        'ConceptoText
        '
        Me.ConceptoText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoText.Location = New System.Drawing.Point(113, 56)
        Me.ConceptoText.Name = "ConceptoText"
        Me.ConceptoText.Size = New System.Drawing.Size(551, 21)
        Me.ConceptoText.TabIndex = 20
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(21, 57)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(86, 16)
        Me.CMBLabel1.TabIndex = 19
        Me.CMBLabel1.Text = "Concepto : "
        '
        'IdConceptoText
        '
        Me.IdConceptoText.Enabled = False
        Me.IdConceptoText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdConceptoText.Location = New System.Drawing.Point(113, 30)
        Me.IdConceptoText.Name = "IdConceptoText"
        Me.IdConceptoText.Size = New System.Drawing.Size(124, 21)
        Me.IdConceptoText.TabIndex = 17
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(47, 31)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(60, 16)
        Me.CMBLabel5.TabIndex = 18
        Me.CMBLabel5.Text = "Clave : "
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(49, 109)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(58, 16)
        Me.CMBLabel3.TabIndex = 38
        Me.CMBLabel3.Text = "Grupo :"
        '
        'IdGrupoCombo
        '
        Me.IdGrupoCombo.DisplayMember = "CONCEPTO"
        Me.IdGrupoCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdGrupoCombo.FormattingEnabled = True
        Me.IdGrupoCombo.Location = New System.Drawing.Point(113, 108)
        Me.IdGrupoCombo.Name = "IdGrupoCombo"
        Me.IdGrupoCombo.Size = New System.Drawing.Size(450, 23)
        Me.IdGrupoCombo.TabIndex = 37
        Me.IdGrupoCombo.ValueMember = "IdGrupo"
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(34, 136)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(73, 16)
        Me.CMBLabel4.TabIndex = 40
        Me.CMBLabel4.Text = "Servicio :"
        '
        'Clv_ServicioComboBox
        '
        Me.Clv_ServicioComboBox.DisplayMember = "Descripcion"
        Me.Clv_ServicioComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ServicioComboBox.FormattingEnabled = True
        Me.Clv_ServicioComboBox.Location = New System.Drawing.Point(113, 135)
        Me.Clv_ServicioComboBox.Name = "Clv_ServicioComboBox"
        Me.Clv_ServicioComboBox.Size = New System.Drawing.Size(450, 23)
        Me.Clv_ServicioComboBox.TabIndex = 39
        Me.Clv_ServicioComboBox.ValueMember = "Clv_Servicio"
        '
        'CuentaTextBox
        '
        Me.CuentaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CuentaTextBox.Location = New System.Drawing.Point(112, 195)
        Me.CuentaTextBox.Name = "CuentaTextBox"
        Me.CuentaTextBox.Size = New System.Drawing.Size(154, 21)
        Me.CuentaTextBox.TabIndex = 42
        '
        'CMBLabel53
        '
        Me.CMBLabel53.AutoSize = True
        Me.CMBLabel53.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel53.Location = New System.Drawing.Point(43, 200)
        Me.CMBLabel53.Name = "CMBLabel53"
        Me.CMBLabel53.Size = New System.Drawing.Size(68, 16)
        Me.CMBLabel53.TabIndex = 41
        Me.CMBLabel53.Text = "Cuenta : "
        '
        'IdProgramadaTextBox
        '
        Me.IdProgramadaTextBox.Enabled = False
        Me.IdProgramadaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdProgramadaTextBox.Location = New System.Drawing.Point(113, 249)
        Me.IdProgramadaTextBox.Name = "IdProgramadaTextBox"
        Me.IdProgramadaTextBox.Size = New System.Drawing.Size(154, 21)
        Me.IdProgramadaTextBox.TabIndex = 44
        '
        'CMBLabel66
        '
        Me.CMBLabel66.AutoSize = True
        Me.CMBLabel66.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel66.Location = New System.Drawing.Point(74, 252)
        Me.CMBLabel66.Name = "CMBLabel66"
        Me.CMBLabel66.Size = New System.Drawing.Size(35, 16)
        Me.CMBLabel66.TabIndex = 43
        Me.CMBLabel66.Text = "ID : "
        '
        'ActivoCheckBox
        '
        Me.ActivoCheckBox.AutoSize = True
        Me.ActivoCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivoCheckBox.Location = New System.Drawing.Point(113, 276)
        Me.ActivoCheckBox.Name = "ActivoCheckBox"
        Me.ActivoCheckBox.Size = New System.Drawing.Size(70, 20)
        Me.ActivoCheckBox.TabIndex = 45
        Me.ActivoCheckBox.Text = "Activo"
        Me.ActivoCheckBox.UseVisualStyleBackColor = True
        '
        'ExitButton
        '
        Me.ExitButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitButton.Location = New System.Drawing.Point(568, 354)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(140, 35)
        Me.ExitButton.TabIndex = 47
        Me.ExitButton.Text = "&Cancelar"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'SaveButton
        '
        Me.SaveButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.Location = New System.Drawing.Point(396, 354)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(142, 35)
        Me.SaveButton.TabIndex = 46
        Me.SaveButton.Text = "&Guardar"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'gbxConcepto
        '
        Me.gbxConcepto.Controls.Add(Me.cmbCuidadPoliza)
        Me.gbxConcepto.Controls.Add(Me.Label2)
        Me.gbxConcepto.Controls.Add(Me.SubcuentaTextBox)
        Me.gbxConcepto.Controls.Add(Me.Label1)
        Me.gbxConcepto.Controls.Add(Me.IdConceptoText)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel5)
        Me.gbxConcepto.Controls.Add(Me.CuentaTextBox)
        Me.gbxConcepto.Controls.Add(Me.IdGrupoCombo)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel1)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel2)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel53)
        Me.gbxConcepto.Controls.Add(Me.ActivoCheckBox)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel66)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel3)
        Me.gbxConcepto.Controls.Add(Me.ConceptoText)
        Me.gbxConcepto.Controls.Add(Me.PosicionText)
        Me.gbxConcepto.Controls.Add(Me.Clv_ServicioComboBox)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel4)
        Me.gbxConcepto.Controls.Add(Me.IdProgramadaTextBox)
        Me.gbxConcepto.Location = New System.Drawing.Point(12, 12)
        Me.gbxConcepto.Name = "gbxConcepto"
        Me.gbxConcepto.Size = New System.Drawing.Size(696, 302)
        Me.gbxConcepto.TabIndex = 48
        Me.gbxConcepto.TabStop = False
        '
        'cmbCuidadPoliza
        '
        Me.cmbCuidadPoliza.DisplayMember = "Nombre"
        Me.cmbCuidadPoliza.FormattingEnabled = True
        Me.cmbCuidadPoliza.Location = New System.Drawing.Point(114, 166)
        Me.cmbCuidadPoliza.Name = "cmbCuidadPoliza"
        Me.cmbCuidadPoliza.Size = New System.Drawing.Size(250, 21)
        Me.cmbCuidadPoliza.TabIndex = 49
        Me.cmbCuidadPoliza.ValueMember = "Clv_Ciudad"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(42, 173)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 16)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "Ciudad :"
        '
        'SubcuentaTextBox
        '
        Me.SubcuentaTextBox.Location = New System.Drawing.Point(113, 223)
        Me.SubcuentaTextBox.Name = "SubcuentaTextBox"
        Me.SubcuentaTextBox.Size = New System.Drawing.Size(154, 20)
        Me.SubcuentaTextBox.TabIndex = 47
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(21, 226)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 16)
        Me.Label1.TabIndex = 46
        Me.Label1.Text = "Subcuenta:"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmConceptosPoliza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(726, 412)
        Me.Controls.Add(Me.gbxConcepto)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.SaveButton)
        Me.MaximizeBox = False
        Me.Name = "FrmConceptosPoliza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Captura el Concepto"
        Me.gbxConcepto.ResumeLayout(False)
        Me.gbxConcepto.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents PosicionText As System.Windows.Forms.TextBox
    Friend WithEvents ConceptoText As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents IdConceptoText As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents IdGrupoCombo As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents Clv_ServicioComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CuentaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel53 As System.Windows.Forms.Label
    Friend WithEvents IdProgramadaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel66 As System.Windows.Forms.Label
    Friend WithEvents ActivoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents SaveButton As System.Windows.Forms.Button
    Friend WithEvents gbxConcepto As System.Windows.Forms.GroupBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbCuidadPoliza As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents SubcuentaTextBox As System.Windows.Forms.TextBox
End Class
