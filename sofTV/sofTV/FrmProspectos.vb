﻿Imports System.Data.SqlClient

Public Class FrmProspectos

    Private Sub FrmProspectos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ColoreaFrm(Me)

        Llena_estados()

        If opcion = "M" Or opcion = "C" Then
            ConsultarProspecto()
            If opcion = "C" Then
                TextBoxPnombre.Enabled = False
                TextBoxSnombre.Enabled = False
                TextBoxApeMa.Enabled = False
                TextBoxApePa.Enabled = False
                TextBoxTel.Enabled = False
                TextBoxCel.Enabled = False
                ComboBoxCalles.Enabled = False
                TextBoxNext.Enabled = False
                TextBoxNint.Enabled = False
                ComboBoxColonias.Enabled = False
                ComboBoxCompanias.Enabled = False
                ComboBoxLocalidad.Enabled = False
                ComboBoxEstados.Enabled = False
                TextBoxCorreo.Enabled = False
                ComboBoxCiudad.Enabled = False
            End If
        End If


    End Sub
    Private Sub ConsultarProspecto()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Prospecto", SqlDbType.Int, ClvProspecto)
            Dim dt As New DataTable
            dt = BaseII.ConsultaDT("[CONSULTAPROSPECTOS]")

            TextBoxClave.Text = ClvProspecto
            TextBoxPnombre.Text = dt.Rows(0)(0).ToString
            TextBoxSnombre.Text = dt.Rows(0)(1).ToString
            TextBoxApeMa.Text = dt.Rows(0)(2).ToString
            TextBoxApePa.Text = dt.Rows(0)(3).ToString
            TextBoxTel.Text = dt.Rows(0)(4).ToString
            TextBoxCel.Text = dt.Rows(0)(5).ToString
            ComboBoxEstados.SelectedValue = dt.Rows(0)(12).ToString
            ComboBoxCiudad.SelectedValue = dt.Rows(0)(14).ToString
            ComboBoxCompanias.SelectedValue = dt.Rows(0)(10).ToString
            ComboBoxLocalidad.SelectedValue = dt.Rows(0)(11).ToString
            ComboBoxColonias.SelectedValue = dt.Rows(0)(9).ToString
            ComboBoxCalles.SelectedValue = dt.Rows(0)(6).ToString
            TextBoxNext.Text = dt.Rows(0)(7).ToString
            TextBoxNint.Text = dt.Rows(0)(8).ToString

            TextBoxCorreo.Text = dt.Rows(0)(13).ToString


        Catch ex As Exception

        End Try
    End Sub
    Private Sub Llena_estados()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@claveUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxEstados.DataSource = BaseII.ConsultaDT("[MuestraEstadosPorUsuario]")
            ComboBoxEstados.DisplayMember = "Nombre"
            ComboBoxEstados.ValueMember = "Clv_Estado"
            llenaCiudades()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ComboBoxEstados_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBoxEstados.SelectedIndexChanged
        If IsNumeric(ComboBoxEstados.SelectedValue) Then
            llenaCiudades()
        End If
    End Sub

    Private Sub llenaCiudades()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, ComboBoxEstados.SelectedValue)
            BaseII.CreateMyParameter("@claveUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("[MuestraCiudadesPorEstado]")
            ComboBoxCiudad.DisplayMember = "Nombre"
            ComboBoxCiudad.ValueMember = "Clv_Ciudad"
            Llena_companias()
            Llena_localidades()
        Catch ex As Exception

        End Try

    End Sub


    Private Sub ComboBoxCiudad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCiudad.SelectedIndexChanged
        If IsNumeric(ComboBoxCiudad.SelectedValue) Then
            Llena_companias()
            Llena_localidades()
        End If
    End Sub


    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClvCiudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
            BaseII.CreateMyParameter("@claveUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("[MuestraCompaniasPorCiudad]")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"
            ComboBoxCompanias.SelectedIndex = 0
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_localidades()
        Try
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@ClvCiudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
            ComboBoxLocalidad.DataSource = BaseII.ConsultaDT("[MUESTRALOCALIDAD_PorCiudad]")
            ComboBoxLocalidad.DisplayMember = "Nombre"
            ComboBoxLocalidad.ValueMember = "Clv_Localidad"
            Llena_colonias()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxLocalidad_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBoxLocalidad.SelectedIndexChanged
        If IsNumeric(ComboBoxLocalidad.SelectedValue) Then
            Llena_colonias()
        End If
    End Sub

    Private Sub Llena_colonias()
        Try
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, ComboBoxLocalidad.SelectedValue)
            ComboBoxColonias.DataSource = BaseII.ConsultaDT("[MuestraColoniaLocalidad]")
            ComboBoxColonias.DisplayMember = "Nombre"
            ComboBoxColonias.ValueMember = "Clv_Colonia"
            ComboBoxColonias.SelectedIndex = 0
            Llena_calles()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxColonias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxColonias.SelectedIndexChanged
        If IsNumeric(ComboBoxColonias.SelectedValue) Then
            Llena_calles()
        End If
    End Sub

    Private Sub Llena_calles()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, ComboBoxColonias.SelectedValue)
            ComboBoxCalles.DataSource = BaseII.ConsultaDT("[MuestraCalleColonia]")
            ComboBoxCalles.DisplayMember = "NOMBRE"
            ComboBoxCalles.ValueMember = "Clv_Calle"

            If ComboBoxCalles.Items.Count = 0 Then
                ComboBoxCalles.Text = ""
            Else
                ComboBoxCalles.SelectedIndex = 0
            End If
            If CapturaCalle And opcion = "N" Then
                ComboBoxCalles.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub BindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorSaveItem.Click
        Dim clv_calle As Integer
        If opcion = "M" Then
            If TextBoxTel.Text.Length <> 10 Then
                MsgBox("El número telefónico debe ser de 10 dígitos.")
                Exit Sub
            End If
            If TextBoxClave.Text > 0 And Len(TextBoxPnombre.Text) > 0 And Len(TextBoxTel.Text) > 0 And Len(TextBoxNext.Text) > 0 And ((ComboBoxCalles.SelectedValue = Nothing And CapturaCalle = True) Or ComboBoxCalles.SelectedValue > 0) Then
                If CapturaCalle And ComboBoxCalles.Text = "" Then
                    MsgBox("Debe seleccionar la calle o registrar una nueva para poder continuar.")
                    Exit Sub
                End If
                If CapturaCalle And ComboBoxCalles.Text = "" Then
                    MsgBox("Debe seleccionar la calle o registrar una nueva para poder continuar.")
                    Exit Sub
                End If
                'Validamos si se pueden capturar las calles desde la pantalla y procedemos a guardarla
                If CapturaCalle And ComboBoxCalles.SelectedValue = Nothing Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.Int, ComboBoxEstados.SelectedValue)
                    BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
                    BaseII.CreateMyParameter("@Clv_Localidad", SqlDbType.Int, ComboBoxLocalidad.SelectedValue)
                    BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, ComboBoxColonias.SelectedValue)
                    BaseII.CreateMyParameter("@calleNombre", SqlDbType.VarChar, ComboBoxCalles.Text)
                    BaseII.CreateMyParameter("@clv_calle", ParameterDirection.Output, SqlDbType.Int)
                    BaseII.ProcedimientoOutPut("sp_guardarCalleNuevaCliente")
                    clv_calle = BaseII.dicoPar("@clv_calle")
                Else
                    clv_calle = ComboBoxCalles.SelectedValue
                End If
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Prospecto", SqlDbType.BigInt, TextBoxClave.Text)
                BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, TextBoxPnombre.Text, 100)
                BaseII.CreateMyParameter("@Segundo_Nombre", SqlDbType.VarChar, TextBoxSnombre.Text, 100)
                BaseII.CreateMyParameter("@ApellidoPat", SqlDbType.VarChar, TextBoxApePa.Text, 100)
                BaseII.CreateMyParameter("@ApellidoMat", SqlDbType.VarChar, TextBoxApeMa.Text, 100)
                BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, TextBoxTel.Text, 20)
                BaseII.CreateMyParameter("@Celular", SqlDbType.VarChar, TextBoxCel.Text, 20)
                BaseII.CreateMyParameter("@Clv_Calle", SqlDbType.Int, clv_calle)
                BaseII.CreateMyParameter("@Numero", SqlDbType.VarChar, TextBoxNext.Text, 15)
                BaseII.CreateMyParameter("@NumeroInt", SqlDbType.VarChar, TextBoxNint.Text, 15)
                BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, ComboBoxColonias.SelectedValue)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                BaseII.CreateMyParameter("@Clv_Localidad", SqlDbType.Int, ComboBoxLocalidad.SelectedValue)
                BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.Int, ComboBoxEstados.SelectedValue)
                BaseII.CreateMyParameter("@Email", SqlDbType.VarChar, TextBoxCorreo.Text, 50)
                BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
                BaseII.Inserta("UpdatePROSPECTOS")
                MsgBox("Datos actualizados correctamente.")
                Me.Close()
                BrwProspectos.bndActivate = 1
            Else
                MsgBox("Faltan datos por completar.")
            End If

        ElseIf opcion = "N" Then
            If TextBoxTel.Text.Length <> 10 Then
                MsgBox("El número telefónico debe ser de 10 dígitos.")
                Exit Sub
            End If
            If Len(TextBoxPnombre.Text) > 0 And Len(TextBoxTel.Text) > 0 And Len(TextBoxNext.Text) > 0 And ((ComboBoxCalles.SelectedValue = Nothing And CapturaCalle = True) Or ComboBoxCalles.SelectedValue > 0) Then
                If CapturaCalle And ComboBoxCalles.Text = "" Then
                    MsgBox("Debe seleccionar la calle o registrar una nueva para poder continuar.")
                    Exit Sub
                End If
                'Validamos si se pueden capturar las calles desde la pantalla y procedemos a guardarla
                If CapturaCalle And ComboBoxCalles.SelectedValue = Nothing Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.Int, ComboBoxEstados.SelectedValue)
                    BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
                    BaseII.CreateMyParameter("@Clv_Localidad", SqlDbType.Int, ComboBoxLocalidad.SelectedValue)
                    BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, ComboBoxColonias.SelectedValue)
                    BaseII.CreateMyParameter("@calleNombre", SqlDbType.VarChar, ComboBoxCalles.Text)
                    BaseII.CreateMyParameter("@clv_calle", ParameterDirection.Output, SqlDbType.Int)
                    BaseII.ProcedimientoOutPut("sp_guardarCalleNuevaCliente")
                    clv_calle = BaseII.dicoPar("@clv_calle")
                Else
                    clv_calle = ComboBoxCalles.SelectedValue
                End If
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Prospecto", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, TextBoxPnombre.Text, 100)
                BaseII.CreateMyParameter("@Segundo_Nombre", SqlDbType.VarChar, TextBoxSnombre.Text, 100)
                BaseII.CreateMyParameter("@ApellidoPat", SqlDbType.VarChar, TextBoxApePa.Text, 100)
                BaseII.CreateMyParameter("@ApellidoMat", SqlDbType.VarChar, TextBoxApeMa.Text, 100)
                BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, TextBoxTel.Text, 20)
                BaseII.CreateMyParameter("@Celular", SqlDbType.VarChar, TextBoxCel.Text, 20)
                BaseII.CreateMyParameter("@Clv_Calle", SqlDbType.Int, clv_calle)
                BaseII.CreateMyParameter("@Numero", SqlDbType.VarChar, TextBoxNext.Text, 15)
                BaseII.CreateMyParameter("@NumeroInt", SqlDbType.VarChar, TextBoxNint.Text, 15)
                BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, ComboBoxColonias.SelectedValue)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                BaseII.CreateMyParameter("@Clv_Localidad", SqlDbType.Int, ComboBoxLocalidad.SelectedValue)
                BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.Int, ComboBoxEstados.SelectedValue)
                BaseII.CreateMyParameter("@Email", SqlDbType.VarChar, TextBoxCorreo.Text, 50)
                BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
                If GloTipoUsuario = 53 Then
                    BaseII.CreateMyParameter("@callCenter", SqlDbType.Int, 1)
                Else
                    BaseII.CreateMyParameter("@callCenter", SqlDbType.Int, 0)
                End If
                BaseII.ProcedimientoOutPut("InsertaPROSPECTOS")
                TextBoxClave.Text = BaseII.dicoPar("@Clv_Prospecto")
                If TextBoxClave.Text > 0 Then
                    MsgBox("Prospecto almacenado correctamente.")
                    Me.Close()
                    BrwProspectos.bndActivate = 1
                End If
            Else
                MsgBox("Faltan datos por completar.")
            End If

        End If

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdCompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
            BaseII.CreateMyParameter("@permiteCapturarNuevo", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("sp_permiteCapturarCalles")
            CapturaCalle = BaseII.dicoPar("@permiteCapturarNuevo")
        Catch ex As Exception

        End Try
    End Sub
End Class
