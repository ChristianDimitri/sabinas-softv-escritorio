﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormReporteFolios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Serie = New System.Windows.Forms.ComboBox()
        Me.Vendedor = New System.Windows.Forms.ComboBox()
        Me.Txt_inicio = New System.Windows.Forms.TextBox()
        Me.Txt_fin = New System.Windows.Forms.TextBox()
        Me.Cancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Rockwell", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(61, 25)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(146, 19)
        Me.CMBLabel4.TabIndex = 7
        Me.CMBLabel4.Text = "Reporte de Folios"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Rockwell", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(79, 164)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(107, 15)
        Me.CMBLabel3.TabIndex = 14
        Me.CMBLabel3.Text = "Rango de Folios"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Rockwell", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(50, 113)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(47, 15)
        Me.CMBLabel2.TabIndex = 13
        Me.CMBLabel2.Text = "Serie :"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Rockwell", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(50, 71)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(75, 15)
        Me.CMBLabel1.TabIndex = 12
        Me.CMBLabel1.Text = "Vendedor :"
        '
        'Serie
        '
        Me.Serie.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Serie.DisplayMember = "SERIE"
        Me.Serie.FormattingEnabled = True
        Me.Serie.Location = New System.Drawing.Point(46, 131)
        Me.Serie.Name = "Serie"
        Me.Serie.Size = New System.Drawing.Size(105, 21)
        Me.Serie.TabIndex = 10
        Me.Serie.ValueMember = "SERIE"
        '
        'Vendedor
        '
        Me.Vendedor.DisplayMember = "Nombre"
        Me.Vendedor.FormattingEnabled = True
        Me.Vendedor.Location = New System.Drawing.Point(46, 87)
        Me.Vendedor.Name = "Vendedor"
        Me.Vendedor.Size = New System.Drawing.Size(193, 21)
        Me.Vendedor.TabIndex = 9
        Me.Vendedor.ValueMember = "Clv_Vendedor"
        '
        'Txt_inicio
        '
        Me.Txt_inicio.Location = New System.Drawing.Point(46, 180)
        Me.Txt_inicio.Name = "Txt_inicio"
        Me.Txt_inicio.Size = New System.Drawing.Size(79, 20)
        Me.Txt_inicio.TabIndex = 15
        '
        'Txt_fin
        '
        Me.Txt_fin.Location = New System.Drawing.Point(160, 180)
        Me.Txt_fin.Name = "Txt_fin"
        Me.Txt_fin.Size = New System.Drawing.Size(79, 20)
        Me.Txt_fin.TabIndex = 16
        '
        'Cancelar
        '
        Me.Cancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancelar.Location = New System.Drawing.Point(169, 229)
        Me.Cancelar.Name = "Cancelar"
        Me.Cancelar.Size = New System.Drawing.Size(84, 39)
        Me.Cancelar.TabIndex = 18
        Me.Cancelar.Text = "&CANCELAR"
        Me.Cancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(41, 229)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(84, 39)
        Me.btnAceptar.TabIndex = 19
        Me.btnAceptar.Text = "&ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'FormReporteFolios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 293)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.Cancelar)
        Me.Controls.Add(Me.Txt_fin)
        Me.Controls.Add(Me.Txt_inicio)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Serie)
        Me.Controls.Add(Me.Vendedor)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Name = "FormReporteFolios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte Folios"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    'Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    'Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Serie As System.Windows.Forms.ComboBox
    Friend WithEvents Vendedor As System.Windows.Forms.ComboBox
    Friend WithEvents Txt_inicio As System.Windows.Forms.TextBox
    Friend WithEvents Txt_fin As System.Windows.Forms.TextBox
    Friend WithEvents Cancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
End Class
