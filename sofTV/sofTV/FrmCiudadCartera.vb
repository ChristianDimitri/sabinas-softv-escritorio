﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic

Imports NPOI.HSSF.UserModel
Imports NPOI.SS.UserModel
Imports NPOI.SS.Util
Imports NPOI.HSSF.Util
Imports NPOI.POIFS.FileSystem
Imports NPOI.HPSF
Imports NPOI.XSSF.UserModel
Imports NPOI.XSSF.Util
Imports System.Text
Imports System.IO
Imports System.Web.Security
Public Class FrmCiudadCartera
    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Titulo As String = Nothing
    Dim LocClv_Cartera As Long = 0
    Dim LocClv_ReporteCiudad As Long = 0
    Private Sub FrmCiudadCartera_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        cbTipSer.DataSource = BaseII.ConsultaDT("MuestraTipSerPrincipal")

    End Sub

    'Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs)
    '    dtpFin.MinDate = dtpIni.Value
    'End Sub

    'Private Sub dtpFin_ValueChanged(sender As Object, e As EventArgs)
    '    dtpIni.MaxDate = dtpFin.Value
    'End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub cbTipSer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipSer.SelectedIndexChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, cbTipSer.SelectedValue)
            cbFecha.DataSource = BaseII.ConsultaDT("ObtieneFechaReportes")
            GloClv_TipSer = cbTipSer.SelectedValue
        Catch ex As Exception
            GloClv_TipSer = 0
        End Try
    End Sub

    Private Sub cbFecha_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbFecha.SelectedIndexChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdReporteCiudad", SqlDbType.BigInt, cbFecha.SelectedValue)
            BaseII.CreateMyParameter("@Clv_Cartera", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@Clv_ReporteCiudad", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("SP_DAMEReportesCiudadCartera")
            LocClv_Cartera = BaseII.dicoPar("@Clv_Cartera").ToString
            LocClv_ReporteCiudad = BaseII.dicoPar("@Clv_ReporteCiudad").ToString
            If RadioButton1.Checked = True Then
                ReporteCartera()
            Else
                GeneraReporteCiudad()
            End If
        Catch ex As Exception
            LocClv_Cartera = 0
            LocClv_ReporteCiudad = 0
        End Try
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then


            Button2.Visible = True
            Button3.Visible = False
            ReporteCartera()
        Else
            Button2.Visible = False
            Button3.Visible = False
            GeneraReporteCiudad()
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton1.Checked = True Then


            Button2.Visible = True
            Button3.Visible = False
            ReporteCartera()
        Else
            Button2.Visible = False
            Button3.Visible = False
            GeneraReporteCiudad()
        End If
    End Sub

    Private Function SELECCIONACARTERA(ByVal CLV_CARTERA As Integer, ByVal CLV_TIPSER As Integer, ByVal SUCURSAL As String) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("CatalogoConceptosCartera")
        tableNameList.Add("DetCartera")
        tableNameList.Add("SeleccionaCartera")
        tableNameList.Add("Servicios")
        tableNameList.Add("Encabezados")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, CLV_CARTERA)
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.VarChar, SUCURSAL, 150)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        Return BaseII.ConsultaDS("SELECCIONACARTERA", tableNameList)
    End Function

    Private Function SELECCIONACARTERADIG(ByVal CLV_CARTERA As Integer, ByVal CLV_TIPSER As Integer, ByVal SUCURSAL As String) As DataSet
        Dim tableNameList As New List(Of String)
        'tableNameList.Add("CatalogoConceptosCartera")
        'tableNameList.Add("DetCartera")
        ''tableNameList.Add("Encabezados")
        'tableNameList.Add("SeleccionaCartera;1")
        'tableNameList.Add("Servicios")
        tableNameList.Add("CatalogoConceptosCartera")
        tableNameList.Add("DetCartera")
        tableNameList.Add("SeleccionaCartera;1")
        tableNameList.Add("Servicios")
        tableNameList.Add("Encabezados")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, CLV_CARTERA)
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.VarChar, SUCURSAL, 150)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        Return BaseII.ConsultaDS("SELECCIONACARTERA", tableNameList)
    End Function
    Private Sub ReporteCartera()
        Try
            GloClv_TipSer = cbTipSer.SelectedValue

            If GloClv_TipSer = 3 Then
                customersByCityReport = New ReportDocument
                Dim connectionInfo As New ConnectionInfo
                Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
                Dim dSet As New DataSet
                dSet = SELECCIONACARTERADIG(LocClv_Cartera, GloClv_TipSer, GloSucursal)
                rDocument.Load(RutaReportes + "\CarterasDig.rpt")
                rDocument.SetDataSource(dSet)
                Dim subtitutlo As String = "Estado de Cartera de TV Digital"
                'inicio nuevo titulo
                Dim con As New SqlConnection(MiConexion)
                con.Open()
                Dim comando As New SqlCommand()
                comando.Connection = con
                comando.CommandText = "exec DamePlazasTituloReporteCartera " + Str(LocClv_Cartera)
                Dim companias As String = "Distribuidor: "
                Dim reader As SqlDataReader = comando.ExecuteReader()
                Dim contador As Integer = 0
                While reader.Read()
                    If contador = 0 Then
                        companias = companias + reader(0).ToString
                        contador = contador + 1
                    Else
                        companias = companias + ", " + reader(0).ToString
                    End If
                End While
                reader.Close()
                comando.Dispose()

                con.Close()
                'Fin Título nuevo
                rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
                rDocument.DataDefinition.FormulaFields("Sucursal").Text = "'" & subtitutlo & "'"
                rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & companias & "'"
                CrystalReportViewer1.ShowGroupTreeButton = False
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = rDocument
                rDocument = Nothing
            Else

                Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
                Dim dSet As New DataSet
                dSet = SELECCIONACARTERA(LocClv_Cartera, GloClv_TipSer, GloSucursal)
                rDocument.Load(RutaReportes + "\CarterasInt.rpt")
                rDocument.SetDataSource(dSet)
                'inicio nuevo titulo
                Dim con As New SqlConnection(MiConexion)
                con.Open()
                Dim comando As New SqlCommand()
                comando.Connection = con
                Dim subtitutlo As String = "Estado de Cartera de Internet"
                comando.CommandText = "exec DamePlazasTituloReporteCartera " + Str(LocClv_Cartera)
                Dim companias As String = "Distribuidor: "
                Dim reader As SqlDataReader = comando.ExecuteReader()
                Dim contador As Integer = 0
                While reader.Read()
                    If contador = 0 Then
                        companias = companias + reader(0).ToString
                        contador = contador + 1
                    Else
                        companias = companias + ", " + reader(0).ToString
                    End If
                End While
                reader.Close()
                comando.Dispose()

                con.Close()
                'Fin Título nuevo
                rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
                rDocument.DataDefinition.FormulaFields("Sucursal").Text = "'" & subtitutlo & "'"
                rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & companias & "'"
                CrystalReportViewer1.ShowGroupTreeButton = False
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = rDocument
                rDocument = Nothing

            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            If LocClv_Cartera = 0 Then
                Exit Sub
            End If
            Dim rutaFile As String = ""
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, LocClv_Cartera)
            Dim dtServicios = BaseII.ConsultaDT("SELECCIONACARTERAServiciosExcel")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, LocClv_Cartera)
            Dim dt = BaseII.ConsultaDT("SELECCIONACARTERAExcel")
            Dim wb As New XSSFWorkbook()
            Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()
            If (result = DialogResult.OK) Then
                rutaFile = Me.FolderBrowserDialog1.SelectedPath.ToString
                If File.Exists(rutaFile + "\Cartera_" + Str(LocClv_Cartera) + ".xlsx") Then
                    File.Delete(rutaFile + "\Cartera_" + Str(LocClv_Cartera) + ".xlsx")
                End If
                Dim sheet1 As ISheet = wb.CreateSheet(Str(LocClv_Cartera))
                Dim fileOut = New FileStream(rutaFile + "\Cartera_" + Str(LocClv_Cartera) + ".xlsx", FileMode.Create, FileAccess.ReadWrite)


                Dim rowNew As IRow = sheet1.CreateRow(0)
                Dim c As Integer = 1
                rowNew.CreateCell(0).SetCellValue("")

                For Each row As DataRow In dtServicios.Rows
                    If row(1).GetType().ToString = "System.Int32" Then
                        rowNew.CreateCell(c).SetCellValue(CInt(row(1).ToString))
                    ElseIf row(1).GetType().ToString = "System.Int16" Then
                        rowNew.CreateCell(c).SetCellValue(CInt(row(1).ToString))
                    ElseIf row(1).GetType().ToString = "System.Decimal" Then
                        rowNew.CreateCell(c).SetCellValue(CDbl(row(1).ToString))
                    Else
                        rowNew.CreateCell(c).SetCellValue(row(1).ToString)
                    End If

                    c = c + 2
                Next

                Dim f As Integer = 1
                c = 0
                Dim aux As Integer = 0
                Dim rowNew2 As IRow

                For Each row As DataRow In dt.Rows
                    If aux <> CInt(row(1).ToString) Then
                        c = 0
                        aux = CInt(row(1).ToString)
                        rowNew2 = sheet1.CreateRow(f)
                        rowNew2.CreateCell(c).SetCellValue(row(0).ToString)
                        f = f + 1
                        c = c + 1
                    End If
                    rowNew2.CreateCell(c).SetCellValue(row(4).ToString)
                    c = c + 1
                    rowNew2.CreateCell(c).SetCellValue(row(5).ToString)
                    c = c + 1
                Next

                sheet1.AutoSizeColumn(0)

                wb.Write(fileOut)
                fileOut.Close()
                MsgBox("Archivo generado exitosamente.")
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub GeneraReporteCiudad()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing
            Dim nuevo As String = Nothing
            Dim bndcol As Integer = 0
            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing
            Dim OpOrdenar As String = "0"

            'contrato
            OpOrdenar = "1"
            nuevo = "_2.rpt"

            Dim bndmotcan As Integer = 0


            mySelectFormula = "Resumen de Clientes por Ciudad."
            reportPath = RutaReportes + "\ReporteCiudad" + nuevo



            Dim DS As New DataSet
            DS.Clear()

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_ReporteCiudad", SqlDbType.BigInt, LocClv_ReporteCiudad)


            Dim listaTablas As New List(Of String)

            listaTablas.Add("CALLES")
            listaTablas.Add("CIUDADES")
            listaTablas.Add("CLIENTES")
            listaTablas.Add("COLONIAS")
            If bndcol <> 1 Then
                listaTablas.Add("MotivoCancelacion")
            End If
            listaTablas.Add("Reporte_TiposCliente_Ciudad;1")
            'listaTablas.Add("PARAMETROS")


            DS = BaseII.ConsultaDS("SP_ImprimeReportePorCiudadGrabado", listaTablas)

            ' reportPath = RutaReportes + "\ReporteCiudad_2.rpt"


            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)




            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait



            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)




            'MANDAMOS LOS PARÁMETROS QUE REQUIERE EL REPORTE (INICIO)
            customersByCityReport.SetParameterValue("@clv_Session", 0)
            customersByCityReport.SetParameterValue("@op", GloClv_TipSer)
            customersByCityReport.SetParameterValue("@conectado", True)
            customersByCityReport.SetParameterValue("@baja", True)
            customersByCityReport.SetParameterValue("@Insta", True)
            customersByCityReport.SetParameterValue("@Desconect", True)
            customersByCityReport.SetParameterValue("@Susp", True)
            customersByCityReport.SetParameterValue("@Fuera", True)
            customersByCityReport.SetParameterValue("@DescTmp", True)
            customersByCityReport.SetParameterValue("@Orden", 1)
            customersByCityReport.SetParameterValue("@Habilita", 0)
            customersByCityReport.SetParameterValue("@periodo1", False)
            customersByCityReport.SetParameterValue("@periodo2", False)
            'MANDAMOS LOS PARÁMETROS QUE REQUIERE EL REPORTE (FIN)

            Dim companias As String = "Plazas: "

            Dim ciudades As String = "Distribuidores: "


            Subformula = GloSucursal
            companias = "Todas las Plazas"
            ciudades = "Todas los Distribuidores"
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Plazas").Text = "'" & companias & "'"



            'customersByCityReport = Nothing


            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

    End Sub
End Class