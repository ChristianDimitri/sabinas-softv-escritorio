﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSucursales2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label14 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim SerieLabel1 As System.Windows.Forms.Label
        Dim Impresora_TarjetasLabel As System.Windows.Forms.Label
        Dim Impresora_ContratosLabel As System.Windows.Forms.Label
        Dim Clv_SucursalLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim IPLabel As System.Windows.Forms.Label
        Dim ImpresoraLabel As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label23 As System.Windows.Forms.Label
        Dim Label24 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label26 As System.Windows.Forms.Label
        Dim Label27 As System.Windows.Forms.Label
        Dim Label29 As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Dim Label31 As System.Windows.Forms.Label
        Dim Label32 As System.Windows.Forms.Label
        Dim Label33 As System.Windows.Forms.Label
        Dim Label34 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSucursales2))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CboxDom = New System.Windows.Forms.CheckBox()
        Me.ButtonAgregar = New System.Windows.Forms.Button()
        Me.ButtonEliminar = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.UltimoFolioUsadoTextBox = New System.Windows.Forms.TextBox()
        Me.SerieTextBox = New System.Windows.Forms.TextBox()
        Me.SerieFDTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_EquivalenteTextBox = New System.Windows.Forms.TextBox()
        Me.No_folioTextBox = New System.Windows.Forms.TextBox()
        Me.SerieTextBox1 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.FolioFDTextbox = New System.Windows.Forms.TextBox()
        Me.tbContacto = New System.Windows.Forms.TextBox()
        Me.tbReferencia = New System.Windows.Forms.TextBox()
        Me.tbHorario = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TxtIdCompania = New System.Windows.Forms.TextBox()
        Me.tbEmail = New System.Windows.Forms.TextBox()
        Me.TxtPais = New System.Windows.Forms.TextBox()
        Me.tbfax = New System.Windows.Forms.TextBox()
        Me.txtLocalidad = New System.Windows.Forms.TextBox()
        Me.tbentrecalles = New System.Windows.Forms.TextBox()
        Me.TxtNumInt = New System.Windows.Forms.TextBox()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.txtCP = New System.Windows.Forms.TextBox()
        Me.txtNumero = New System.Windows.Forms.TextBox()
        Me.txtMunicipio = New System.Windows.Forms.TextBox()
        Me.txtColonia = New System.Windows.Forms.TextBox()
        Me.txtCalle = New System.Windows.Forms.TextBox()
        Me.txtCiudad = New System.Windows.Forms.TextBox()
        Me.MatrizChck = New System.Windows.Forms.CheckBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Ciudadcombo = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ConsultaImpresoraSucursalBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Impresora_TarjetasTextBox = New System.Windows.Forms.TextBox()
        Me.Impresora_ContratosTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CONSUCURSALESBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONSUCURSALESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSUCURSALESBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_SucursalTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.IPTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraTextBox = New System.Windows.Forms.TextBox()
        Me.Consulta_Generales_FacturasGlobalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.ConsultaImpresoraSucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Impresora_SucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Impresora_SucursalTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Consulta_Impresora_SucursalTableAdapter()
        Me.Inserta_impresora_sucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_impresora_sucursalTableAdapter = New sofTV.DataSetarnoldoTableAdapters.inserta_impresora_sucursalTableAdapter()
        Me.Borra_Impresora_SucursalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Impresora_SucursalesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Borra_Impresora_SucursalesTableAdapter()
        Me.Consulta_Generales_FacturasGlobalesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Consulta_Generales_FacturasGlobalesTableAdapter()
        Me.Inserta_Generales_FacturaGlobalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Generales_FacturaGlobalTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Generales_FacturaGlobalTableAdapter()
        Me.Borra_Generales_FacturasGlobalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Generales_FacturasGlobalesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Borra_Generales_FacturasGlobalesTableAdapter()
        Me.Inserta_impresora_sucursalBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_impresora_sucursalTableAdapter1 = New sofTV.ProcedimientosArnoldo2TableAdapters.inserta_impresora_sucursalTableAdapter()
        Me.Consulta_Impresora_SucursalTableAdapter1 = New sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Impresora_SucursalTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CONSUCURSALESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONSUCURSALESTableAdapter()
        Label14 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        SerieLabel1 = New System.Windows.Forms.Label()
        Impresora_TarjetasLabel = New System.Windows.Forms.Label()
        Impresora_ContratosLabel = New System.Windows.Forms.Label()
        Clv_SucursalLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        IPLabel = New System.Windows.Forms.Label()
        ImpresoraLabel = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label21 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label23 = New System.Windows.Forms.Label()
        Label24 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label26 = New System.Windows.Forms.Label()
        Label27 = New System.Windows.Forms.Label()
        Label29 = New System.Windows.Forms.Label()
        Label30 = New System.Windows.Forms.Label()
        Label31 = New System.Windows.Forms.Label()
        Label32 = New System.Windows.Forms.Label()
        Label33 = New System.Windows.Forms.Label()
        Label34 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ConsultaImpresoraSucursalBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSUCURSALESBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONSUCURSALESBindingNavigator.SuspendLayout()
        CType(Me.CONSUCURSALESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaImpresoraSucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_impresora_sucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Impresora_SucursalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Generales_FacturaGlobalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_impresora_sucursalBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Cursor = System.Windows.Forms.Cursors.Default
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(727, 62)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(88, 15)
        Label14.TabIndex = 39
        Label14.Text = "Telefono(s) :"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Cursor = System.Windows.Forms.Cursors.Default
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(727, 17)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(41, 15)
        Label13.TabIndex = 37
        Label13.Text = "C.P. :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Cursor = System.Windows.Forms.Cursors.Default
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Label12.Location = New System.Drawing.Point(371, 17)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(83, 15)
        Label12.TabIndex = 35
        Label12.Text = "No Exterior:"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Cursor = System.Windows.Forms.Cursors.Default
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(13, 146)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(56, 15)
        Label11.TabIndex = 33
        Label11.Text = "Ciudad:"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Cursor = System.Windows.Forms.Cursors.Default
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(13, 104)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(74, 15)
        Label10.TabIndex = 31
        Label10.Text = "Municipio:"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Cursor = System.Windows.Forms.Cursors.Default
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(13, 62)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(60, 15)
        Label9.TabIndex = 29
        Label9.Text = "Colonia:"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Cursor = System.Windows.Forms.Cursors.Default
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(13, 18)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(44, 15)
        Label8.TabIndex = 27
        Label8.Text = "Calle:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(539, 43)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(125, 15)
        Label2.TabIndex = 29
        Label2.Text = "Impresora Tickets:"
        '
        'SerieLabel1
        '
        SerieLabel1.AutoSize = True
        SerieLabel1.Cursor = System.Windows.Forms.Cursors.Default
        SerieLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        SerieLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        SerieLabel1.Location = New System.Drawing.Point(521, 188)
        SerieLabel1.Name = "SerieLabel1"
        SerieLabel1.Size = New System.Drawing.Size(143, 15)
        SerieLabel1.TabIndex = 25
        SerieLabel1.Text = "Serie Factura Global:"
        SerieLabel1.Visible = False
        '
        'Impresora_TarjetasLabel
        '
        Impresora_TarjetasLabel.AutoSize = True
        Impresora_TarjetasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Impresora_TarjetasLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Impresora_TarjetasLabel.Location = New System.Drawing.Point(71, 148)
        Impresora_TarjetasLabel.Name = "Impresora_TarjetasLabel"
        Impresora_TarjetasLabel.Size = New System.Drawing.Size(132, 15)
        Impresora_TarjetasLabel.TabIndex = 22
        Impresora_TarjetasLabel.Text = "Impresora Tarjetas:"
        '
        'Impresora_ContratosLabel
        '
        Impresora_ContratosLabel.AutoSize = True
        Impresora_ContratosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Impresora_ContratosLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Impresora_ContratosLabel.Location = New System.Drawing.Point(62, 172)
        Impresora_ContratosLabel.Name = "Impresora_ContratosLabel"
        Impresora_ContratosLabel.Size = New System.Drawing.Size(141, 15)
        Impresora_ContratosLabel.TabIndex = 24
        Impresora_ContratosLabel.Text = "Impresora Contratos:"
        '
        'Clv_SucursalLabel
        '
        Clv_SucursalLabel.AutoSize = True
        Clv_SucursalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_SucursalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_SucursalLabel.Location = New System.Drawing.Point(153, 42)
        Clv_SucursalLabel.Name = "Clv_SucursalLabel"
        Clv_SucursalLabel.Size = New System.Drawing.Size(50, 15)
        Clv_SucursalLabel.TabIndex = 0
        Clv_SucursalLabel.Text = "Clave :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(112, 69)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(91, 15)
        NombreLabel.TabIndex = 2
        NombreLabel.Text = "Descripción :"
        '
        'IPLabel
        '
        IPLabel.AutoSize = True
        IPLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        IPLabel.ForeColor = System.Drawing.Color.LightSlateGray
        IPLabel.Location = New System.Drawing.Point(179, 96)
        IPLabel.Name = "IPLabel"
        IPLabel.Size = New System.Drawing.Size(24, 15)
        IPLabel.TabIndex = 4
        IPLabel.Text = "IP:"
        '
        'ImpresoraLabel
        '
        ImpresoraLabel.AutoSize = True
        ImpresoraLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImpresoraLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ImpresoraLabel.Location = New System.Drawing.Point(11, 123)
        ImpresoraLabel.Name = "ImpresoraLabel"
        ImpresoraLabel.Size = New System.Drawing.Size(192, 15)
        ImpresoraLabel.TabIndex = 6
        ImpresoraLabel.Text = "Impresora Facturas Fiscales:"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(528, 70)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(138, 15)
        Label15.TabIndex = 114
        Label15.Text = "Horario de Atención:"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(527, 95)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(142, 15)
        Label16.TabIndex = 115
        Label16.Text = "Referencia Bancaria:"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Label17.Location = New System.Drawing.Point(597, 122)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(67, 15)
        Label17.TabIndex = 116
        Label17.Text = "Contácto:"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.LightSlateGray
        Label19.Location = New System.Drawing.Point(20, 33)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(43, 15)
        Label19.TabIndex = 197
        Label19.Text = "Plaza"
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Label20.Location = New System.Drawing.Point(782, 33)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(41, 15)
        Label20.TabIndex = 43
        Label20.Text = "Serie"
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label21.ForeColor = System.Drawing.Color.LightSlateGray
        Label21.Location = New System.Drawing.Point(861, 33)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(39, 15)
        Label21.TabIndex = 44
        Label21.Text = "Folio"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label22.ForeColor = System.Drawing.Color.LightSlateGray
        Label22.Location = New System.Drawing.Point(551, 33)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(39, 15)
        Label22.TabIndex = 12
        Label22.Text = "Folio"
        '
        'Label23
        '
        Label23.AutoSize = True
        Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label23.ForeColor = System.Drawing.Color.LightSlateGray
        Label23.Location = New System.Drawing.Point(633, 33)
        Label23.Name = "Label23"
        Label23.Size = New System.Drawing.Size(41, 15)
        Label23.TabIndex = 41
        Label23.Text = "Serie"
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.ForeColor = System.Drawing.Color.LightSlateGray
        Label24.Location = New System.Drawing.Point(708, 33)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(39, 15)
        Label24.TabIndex = 42
        Label24.Text = "Folio"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.ForeColor = System.Drawing.Color.LightSlateGray
        Label25.Location = New System.Drawing.Point(477, 33)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(41, 15)
        Label25.TabIndex = 10
        Label25.Text = "Serie"
        '
        'Label26
        '
        Label26.AutoSize = True
        Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label26.ForeColor = System.Drawing.Color.LightSlateGray
        Label26.Location = New System.Drawing.Point(397, 33)
        Label26.Name = "Label26"
        Label26.Size = New System.Drawing.Size(67, 15)
        Label26.TabIndex = 8
        Label26.Text = "Clave Eq."
        '
        'Label27
        '
        Label27.AutoSize = True
        Label27.Cursor = System.Windows.Forms.Cursors.Default
        Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label27.ForeColor = System.Drawing.Color.LightSlateGray
        Label27.Location = New System.Drawing.Point(646, 18)
        Label27.Name = "Label27"
        Label27.Size = New System.Drawing.Size(101, 15)
        Label27.TabIndex = 25
        Label27.Text = "Factura Global"
        '
        'Label29
        '
        Label29.AutoSize = True
        Label29.Cursor = System.Windows.Forms.Cursors.Default
        Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label29.ForeColor = System.Drawing.Color.LightSlateGray
        Label29.Location = New System.Drawing.Point(570, 18)
        Label29.Name = "Label29"
        Label29.Size = New System.Drawing.Size(79, 15)
        Label29.TabIndex = 115
        Label29.Text = "No Interior:"
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.ForeColor = System.Drawing.Color.LightSlateGray
        Label30.Location = New System.Drawing.Point(371, 61)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(91, 15)
        Label30.TabIndex = 116
        Label30.Text = "Entre calles :"
        '
        'Label31
        '
        Label31.AutoSize = True
        Label31.Cursor = System.Windows.Forms.Cursors.Default
        Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label31.ForeColor = System.Drawing.Color.LightSlateGray
        Label31.Location = New System.Drawing.Point(397, 166)
        Label31.Name = "Label31"
        Label31.Size = New System.Drawing.Size(74, 15)
        Label31.TabIndex = 119
        Label31.Text = "Localidad:"
        Label31.Visible = False
        '
        'Label32
        '
        Label32.AutoSize = True
        Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label32.ForeColor = System.Drawing.Color.LightSlateGray
        Label32.Location = New System.Drawing.Point(727, 106)
        Label32.Name = "Label32"
        Label32.Size = New System.Drawing.Size(38, 15)
        Label32.TabIndex = 120
        Label32.Text = "Fax :"
        '
        'Label33
        '
        Label33.AutoSize = True
        Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label33.ForeColor = System.Drawing.Color.LightSlateGray
        Label33.Location = New System.Drawing.Point(727, 146)
        Label33.Name = "Label33"
        Label33.Size = New System.Drawing.Size(52, 15)
        Label33.TabIndex = 124
        Label33.Text = "Email :"
        '
        'Label34
        '
        Label34.AutoSize = True
        Label34.Cursor = System.Windows.Forms.Cursors.Default
        Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label34.ForeColor = System.Drawing.Color.LightSlateGray
        Label34.Location = New System.Drawing.Point(371, 104)
        Label34.Name = "Label34"
        Label34.Size = New System.Drawing.Size(39, 15)
        Label34.TabIndex = 123
        Label34.Text = "Pais:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.tbContacto)
        Me.Panel1.Controls.Add(Me.tbReferencia)
        Me.Panel1.Controls.Add(Me.tbHorario)
        Me.Panel1.Controls.Add(Label17)
        Me.Panel1.Controls.Add(Label16)
        Me.Panel1.Controls.Add(Label15)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.MatrizChck)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.Ciudadcombo)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(SerieLabel1)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Impresora_TarjetasLabel)
        Me.Panel1.Controls.Add(Me.Impresora_TarjetasTextBox)
        Me.Panel1.Controls.Add(Impresora_ContratosLabel)
        Me.Panel1.Controls.Add(Me.Impresora_ContratosTextBox)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.CONSUCURSALESBindingNavigator)
        Me.Panel1.Controls.Add(Clv_SucursalLabel)
        Me.Panel1.Controls.Add(Me.Clv_SucursalTextBox)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Controls.Add(IPLabel)
        Me.Panel1.Controls.Add(Me.IPTextBox)
        Me.Panel1.Controls.Add(ImpresoraLabel)
        Me.Panel1.Controls.Add(Me.ImpresoraTextBox)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1091, 759)
        Me.Panel1.TabIndex = 20
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CboxDom)
        Me.GroupBox2.Controls.Add(Me.ButtonAgregar)
        Me.GroupBox2.Controls.Add(Me.ButtonEliminar)
        Me.GroupBox2.Controls.Add(Me.DataGridView1)
        Me.GroupBox2.Controls.Add(Label19)
        Me.GroupBox2.Controls.Add(Me.ComboBoxCompanias)
        Me.GroupBox2.Controls.Add(Label20)
        Me.GroupBox2.Controls.Add(Me.UltimoFolioUsadoTextBox)
        Me.GroupBox2.Controls.Add(Label21)
        Me.GroupBox2.Controls.Add(Label22)
        Me.GroupBox2.Controls.Add(Label23)
        Me.GroupBox2.Controls.Add(Me.SerieTextBox)
        Me.GroupBox2.Controls.Add(Me.SerieFDTextBox)
        Me.GroupBox2.Controls.Add(Label24)
        Me.GroupBox2.Controls.Add(Label25)
        Me.GroupBox2.Controls.Add(Me.Clv_EquivalenteTextBox)
        Me.GroupBox2.Controls.Add(Label26)
        Me.GroupBox2.Controls.Add(Me.No_folioTextBox)
        Me.GroupBox2.Controls.Add(Me.SerieTextBox1)
        Me.GroupBox2.Controls.Add(Label27)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Controls.Add(Me.FolioFDTextbox)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.GroupBox2.Location = New System.Drawing.Point(6, 214)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(963, 294)
        Me.GroupBox2.TabIndex = 122
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Series y Folios por Compañia"
        '
        'CboxDom
        '
        Me.CboxDom.AutoSize = True
        Me.CboxDom.Location = New System.Drawing.Point(400, 82)
        Me.CboxDom.Name = "CboxDom"
        Me.CboxDom.Size = New System.Drawing.Size(239, 19)
        Me.CboxDom.TabIndex = 203
        Me.CboxDom.Text = "Mismo Domicilio de la Compañia"
        Me.CboxDom.UseVisualStyleBackColor = True
        '
        'ButtonAgregar
        '
        Me.ButtonAgregar.Location = New System.Drawing.Point(705, 78)
        Me.ButtonAgregar.Name = "ButtonAgregar"
        Me.ButtonAgregar.Size = New System.Drawing.Size(114, 23)
        Me.ButtonAgregar.TabIndex = 202
        Me.ButtonAgregar.Text = "A&gregar"
        Me.ButtonAgregar.UseVisualStyleBackColor = True
        '
        'ButtonEliminar
        '
        Me.ButtonEliminar.Location = New System.Drawing.Point(825, 78)
        Me.ButtonEliminar.Name = "ButtonEliminar"
        Me.ButtonEliminar.Size = New System.Drawing.Size(114, 23)
        Me.ButtonEliminar.TabIndex = 199
        Me.ButtonEliminar.Text = "E&liminar"
        Me.ButtonEliminar.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(16, 107)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(921, 181)
        Me.DataGridView1.TabIndex = 198
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(16, 51)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(372, 23)
        Me.ComboBoxCompanias.TabIndex = 196
        '
        'UltimoFolioUsadoTextBox
        '
        Me.UltimoFolioUsadoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.UltimoFolioUsadoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltimoFolioUsadoTextBox.Location = New System.Drawing.Point(554, 51)
        Me.UltimoFolioUsadoTextBox.Name = "UltimoFolioUsadoTextBox"
        Me.UltimoFolioUsadoTextBox.Size = New System.Drawing.Size(71, 21)
        Me.UltimoFolioUsadoTextBox.TabIndex = 5
        '
        'SerieTextBox
        '
        Me.SerieTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SerieTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieTextBox.Location = New System.Drawing.Point(477, 51)
        Me.SerieTextBox.Name = "SerieTextBox"
        Me.SerieTextBox.Size = New System.Drawing.Size(71, 21)
        Me.SerieTextBox.TabIndex = 4
        '
        'SerieFDTextBox
        '
        Me.SerieFDTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieFDTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.SerieFDTextBox.Location = New System.Drawing.Point(785, 51)
        Me.SerieFDTextBox.Name = "SerieFDTextBox"
        Me.SerieFDTextBox.Size = New System.Drawing.Size(71, 21)
        Me.SerieFDTextBox.TabIndex = 32
        '
        'Clv_EquivalenteTextBox
        '
        Me.Clv_EquivalenteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_EquivalenteTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_EquivalenteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_EquivalenteTextBox.Location = New System.Drawing.Point(400, 51)
        Me.Clv_EquivalenteTextBox.Name = "Clv_EquivalenteTextBox"
        Me.Clv_EquivalenteTextBox.Size = New System.Drawing.Size(71, 21)
        Me.Clv_EquivalenteTextBox.TabIndex = 3
        '
        'No_folioTextBox
        '
        Me.No_folioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.No_folioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.No_folioTextBox.Location = New System.Drawing.Point(708, 51)
        Me.No_folioTextBox.Name = "No_folioTextBox"
        Me.No_folioTextBox.Size = New System.Drawing.Size(71, 21)
        Me.No_folioTextBox.TabIndex = 10
        '
        'SerieTextBox1
        '
        Me.SerieTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.SerieTextBox1.Location = New System.Drawing.Point(631, 51)
        Me.SerieTextBox1.Name = "SerieTextBox1"
        Me.SerieTextBox1.Size = New System.Drawing.Size(71, 21)
        Me.SerieTextBox1.TabIndex = 9
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label28.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label28.Location = New System.Drawing.Point(799, 18)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(101, 15)
        Me.Label28.TabIndex = 31
        Me.Label28.Text = "Factura Digital"
        '
        'FolioFDTextbox
        '
        Me.FolioFDTextbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FolioFDTextbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.FolioFDTextbox.Location = New System.Drawing.Point(864, 51)
        Me.FolioFDTextbox.Name = "FolioFDTextbox"
        Me.FolioFDTextbox.Size = New System.Drawing.Size(73, 21)
        Me.FolioFDTextbox.TabIndex = 34
        '
        'tbContacto
        '
        Me.tbContacto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbContacto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tbContacto.Location = New System.Drawing.Point(672, 121)
        Me.tbContacto.Name = "tbContacto"
        Me.tbContacto.Size = New System.Drawing.Size(294, 21)
        Me.tbContacto.TabIndex = 120
        '
        'tbReferencia
        '
        Me.tbReferencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbReferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tbReferencia.Location = New System.Drawing.Point(672, 94)
        Me.tbReferencia.Name = "tbReferencia"
        Me.tbReferencia.Size = New System.Drawing.Size(294, 21)
        Me.tbReferencia.TabIndex = 119
        '
        'tbHorario
        '
        Me.tbHorario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbHorario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tbHorario.Location = New System.Drawing.Point(672, 67)
        Me.tbHorario.Name = "tbHorario"
        Me.tbHorario.Size = New System.Drawing.Size(294, 21)
        Me.tbHorario.TabIndex = 118
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TxtIdCompania)
        Me.GroupBox1.Controls.Add(Me.tbEmail)
        Me.GroupBox1.Controls.Add(Label33)
        Me.GroupBox1.Controls.Add(Label34)
        Me.GroupBox1.Controls.Add(Me.TxtPais)
        Me.GroupBox1.Controls.Add(Me.tbfax)
        Me.GroupBox1.Controls.Add(Label32)
        Me.GroupBox1.Controls.Add(Me.txtLocalidad)
        Me.GroupBox1.Controls.Add(Me.tbentrecalles)
        Me.GroupBox1.Controls.Add(Label31)
        Me.GroupBox1.Controls.Add(Label30)
        Me.GroupBox1.Controls.Add(Label29)
        Me.GroupBox1.Controls.Add(Me.TxtNumInt)
        Me.GroupBox1.Controls.Add(Label14)
        Me.GroupBox1.Controls.Add(Me.txtTelefono)
        Me.GroupBox1.Controls.Add(Label13)
        Me.GroupBox1.Controls.Add(Me.txtCP)
        Me.GroupBox1.Controls.Add(Label12)
        Me.GroupBox1.Controls.Add(Me.txtNumero)
        Me.GroupBox1.Controls.Add(Label11)
        Me.GroupBox1.Controls.Add(Label10)
        Me.GroupBox1.Controls.Add(Me.txtMunicipio)
        Me.GroupBox1.Controls.Add(Label9)
        Me.GroupBox1.Controls.Add(Me.txtColonia)
        Me.GroupBox1.Controls.Add(Label8)
        Me.GroupBox1.Controls.Add(Me.txtCalle)
        Me.GroupBox1.Controls.Add(Me.txtCiudad)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.GroupBox1.Location = New System.Drawing.Point(6, 514)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(963, 197)
        Me.GroupBox1.TabIndex = 40
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Dirección Sucursal"
        '
        'TxtIdCompania
        '
        Me.TxtIdCompania.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtIdCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TxtIdCompania.Location = New System.Drawing.Point(932, 12)
        Me.TxtIdCompania.Name = "TxtIdCompania"
        Me.TxtIdCompania.Size = New System.Drawing.Size(25, 21)
        Me.TxtIdCompania.TabIndex = 126
        '
        'tbEmail
        '
        Me.tbEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tbEmail.Location = New System.Drawing.Point(730, 164)
        Me.tbEmail.Name = "tbEmail"
        Me.tbEmail.Size = New System.Drawing.Size(185, 21)
        Me.tbEmail.TabIndex = 121
        '
        'TxtPais
        '
        Me.TxtPais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtPais.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TxtPais.Location = New System.Drawing.Point(374, 122)
        Me.TxtPais.Name = "TxtPais"
        Me.TxtPais.Size = New System.Drawing.Size(350, 21)
        Me.TxtPais.TabIndex = 122
        '
        'tbfax
        '
        Me.tbfax.BackColor = System.Drawing.Color.White
        Me.tbfax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbfax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbfax.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbfax.Location = New System.Drawing.Point(730, 122)
        Me.tbfax.MaxLength = 255
        Me.tbfax.Name = "tbfax"
        Me.tbfax.Size = New System.Drawing.Size(185, 21)
        Me.tbfax.TabIndex = 121
        '
        'txtLocalidad
        '
        Me.txtLocalidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLocalidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtLocalidad.Location = New System.Drawing.Point(477, 164)
        Me.txtLocalidad.Name = "txtLocalidad"
        Me.txtLocalidad.Size = New System.Drawing.Size(93, 21)
        Me.txtLocalidad.TabIndex = 118
        Me.txtLocalidad.Visible = False
        '
        'tbentrecalles
        '
        Me.tbentrecalles.BackColor = System.Drawing.Color.White
        Me.tbentrecalles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbentrecalles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbentrecalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbentrecalles.Location = New System.Drawing.Point(374, 80)
        Me.tbentrecalles.MaxLength = 255
        Me.tbentrecalles.Name = "tbentrecalles"
        Me.tbentrecalles.Size = New System.Drawing.Size(350, 21)
        Me.tbentrecalles.TabIndex = 117
        '
        'TxtNumInt
        '
        Me.TxtNumInt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtNumInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TxtNumInt.Location = New System.Drawing.Point(570, 36)
        Me.TxtNumInt.Name = "TxtNumInt"
        Me.TxtNumInt.Size = New System.Drawing.Size(154, 21)
        Me.TxtNumInt.TabIndex = 114
        '
        'txtTelefono
        '
        Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtTelefono.Location = New System.Drawing.Point(730, 80)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(185, 21)
        Me.txtTelefono.TabIndex = 5
        '
        'txtCP
        '
        Me.txtCP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtCP.Location = New System.Drawing.Point(730, 36)
        Me.txtCP.Name = "txtCP"
        Me.txtCP.Size = New System.Drawing.Size(185, 21)
        Me.txtCP.TabIndex = 3
        '
        'txtNumero
        '
        Me.txtNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtNumero.Location = New System.Drawing.Point(374, 36)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(190, 21)
        Me.txtNumero.TabIndex = 1
        '
        'txtMunicipio
        '
        Me.txtMunicipio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMunicipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtMunicipio.Location = New System.Drawing.Point(16, 122)
        Me.txtMunicipio.Name = "txtMunicipio"
        Me.txtMunicipio.Size = New System.Drawing.Size(308, 21)
        Me.txtMunicipio.TabIndex = 4
        '
        'txtColonia
        '
        Me.txtColonia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtColonia.Location = New System.Drawing.Point(16, 80)
        Me.txtColonia.Name = "txtColonia"
        Me.txtColonia.Size = New System.Drawing.Size(339, 21)
        Me.txtColonia.TabIndex = 2
        '
        'txtCalle
        '
        Me.txtCalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtCalle.Location = New System.Drawing.Point(16, 36)
        Me.txtCalle.Name = "txtCalle"
        Me.txtCalle.Size = New System.Drawing.Size(339, 21)
        Me.txtCalle.TabIndex = 0
        '
        'txtCiudad
        '
        Me.txtCiudad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtCiudad.Location = New System.Drawing.Point(16, 164)
        Me.txtCiudad.Name = "txtCiudad"
        Me.txtCiudad.Size = New System.Drawing.Size(308, 21)
        Me.txtCiudad.TabIndex = 6
        '
        'MatrizChck
        '
        Me.MatrizChck.AutoSize = True
        Me.MatrizChck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MatrizChck.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MatrizChck.ForeColor = System.Drawing.Color.Blue
        Me.MatrizChck.Location = New System.Drawing.Point(546, 153)
        Me.MatrizChck.Name = "MatrizChck"
        Me.MatrizChck.Size = New System.Drawing.Size(99, 22)
        Me.MatrizChck.TabIndex = 12
        Me.MatrizChck.Text = "Es Matriz"
        Me.MatrizChck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MatrizChck.UseVisualStyleBackColor = True
        Me.MatrizChck.Visible = False
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(672, 185)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(96, 23)
        Me.ComboBox1.TabIndex = 11
        Me.ComboBox1.Visible = False
        '
        'Ciudadcombo
        '
        Me.Ciudadcombo.DisplayMember = "Clv_ciudad"
        Me.Ciudadcombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ciudadcombo.FormattingEnabled = True
        Me.Ciudadcombo.Location = New System.Drawing.Point(756, 148)
        Me.Ciudadcombo.Name = "Ciudadcombo"
        Me.Ciudadcombo.Size = New System.Drawing.Size(134, 23)
        Me.Ciudadcombo.TabIndex = 11
        Me.Ciudadcombo.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(685, 152)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 15)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Ciudad:"
        Me.Label3.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaImpresoraSucursalBindingSource1, "Impresora_Tickets", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TextBox1.Location = New System.Drawing.Point(672, 37)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(294, 21)
        Me.TextBox1.TabIndex = 8
        '
        'ConsultaImpresoraSucursalBindingSource1
        '
        Me.ConsultaImpresoraSucursalBindingSource1.DataMember = "Consulta_Impresora_Sucursal"
        Me.ConsultaImpresoraSucursalBindingSource1.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(833, 717)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 113
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Impresora_TarjetasTextBox
        '
        Me.Impresora_TarjetasTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Impresora_TarjetasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaImpresoraSucursalBindingSource1, "Impresora_Tarjetas", True))
        Me.Impresora_TarjetasTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Impresora_TarjetasTextBox.Location = New System.Drawing.Point(209, 146)
        Me.Impresora_TarjetasTextBox.Name = "Impresora_TarjetasTextBox"
        Me.Impresora_TarjetasTextBox.Size = New System.Drawing.Size(294, 21)
        Me.Impresora_TarjetasTextBox.TabIndex = 6
        '
        'Impresora_ContratosTextBox
        '
        Me.Impresora_ContratosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Impresora_ContratosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaImpresoraSucursalBindingSource1, "Impresora_Contratos", True))
        Me.Impresora_ContratosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Impresora_ContratosTextBox.Location = New System.Drawing.Point(209, 171)
        Me.Impresora_ContratosTextBox.Name = "Impresora_ContratosTextBox"
        Me.Impresora_ContratosTextBox.Size = New System.Drawing.Size(294, 21)
        Me.Impresora_ContratosTextBox.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(315, 98)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(179, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = " Nota : Ejemplo 192.168.(""  60 "") .25"
        '
        'CONSUCURSALESBindingNavigator
        '
        Me.CONSUCURSALESBindingNavigator.AddNewItem = Nothing
        Me.CONSUCURSALESBindingNavigator.BindingSource = Me.CONSUCURSALESBindingSource
        Me.CONSUCURSALESBindingNavigator.CountItem = Nothing
        Me.CONSUCURSALESBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONSUCURSALESBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONSUCURSALESBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONSUCURSALESBindingNavigatorSaveItem})
        Me.CONSUCURSALESBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONSUCURSALESBindingNavigator.MoveFirstItem = Nothing
        Me.CONSUCURSALESBindingNavigator.MoveLastItem = Nothing
        Me.CONSUCURSALESBindingNavigator.MoveNextItem = Nothing
        Me.CONSUCURSALESBindingNavigator.MovePreviousItem = Nothing
        Me.CONSUCURSALESBindingNavigator.Name = "CONSUCURSALESBindingNavigator"
        Me.CONSUCURSALESBindingNavigator.PositionItem = Nothing
        Me.CONSUCURSALESBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONSUCURSALESBindingNavigator.Size = New System.Drawing.Size(1091, 27)
        Me.CONSUCURSALESBindingNavigator.TabIndex = 10
        Me.CONSUCURSALESBindingNavigator.TabStop = True
        Me.CONSUCURSALESBindingNavigator.Text = "BindingNavigator1"
        '
        'CONSUCURSALESBindingSource
        '
        Me.CONSUCURSALESBindingSource.DataMember = "CONSUCURSALES"
        Me.CONSUCURSALESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.EnforceConstraints = False
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(94, 24)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 24)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONSUCURSALESBindingNavigatorSaveItem
        '
        Me.CONSUCURSALESBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSUCURSALESBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSUCURSALESBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSUCURSALESBindingNavigatorSaveItem.Name = "CONSUCURSALESBindingNavigatorSaveItem"
        Me.CONSUCURSALESBindingNavigatorSaveItem.Size = New System.Drawing.Size(95, 24)
        Me.CONSUCURSALESBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_SucursalTextBox
        '
        Me.Clv_SucursalTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_SucursalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Clv_Sucursal", True))
        Me.Clv_SucursalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_SucursalTextBox.Location = New System.Drawing.Point(209, 40)
        Me.Clv_SucursalTextBox.Name = "Clv_SucursalTextBox"
        Me.Clv_SucursalTextBox.ReadOnly = True
        Me.Clv_SucursalTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_SucursalTextBox.TabIndex = 10
        Me.Clv_SucursalTextBox.TabStop = False
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(209, 67)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(294, 21)
        Me.NombreTextBox.TabIndex = 0
        '
        'IPTextBox
        '
        Me.IPTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.IPTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.IPTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "IP", True))
        Me.IPTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IPTextBox.Location = New System.Drawing.Point(209, 94)
        Me.IPTextBox.Name = "IPTextBox"
        Me.IPTextBox.Size = New System.Drawing.Size(100, 21)
        Me.IPTextBox.TabIndex = 1
        '
        'ImpresoraTextBox
        '
        Me.ImpresoraTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ImpresoraTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ImpresoraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Impresora", True))
        Me.ImpresoraTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImpresoraTextBox.Location = New System.Drawing.Point(209, 121)
        Me.ImpresoraTextBox.Name = "ImpresoraTextBox"
        Me.ImpresoraTextBox.Size = New System.Drawing.Size(294, 21)
        Me.ImpresoraTextBox.TabIndex = 2
        '
        'Consulta_Generales_FacturasGlobalesBindingSource
        '
        Me.Consulta_Generales_FacturasGlobalesBindingSource.DataMember = "Consulta_Generales_FacturasGlobales"
        Me.Consulta_Generales_FacturasGlobalesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConsultaImpresoraSucursalBindingSource
        '
        Me.ConsultaImpresoraSucursalBindingSource.DataMember = "Consulta_Impresora_Sucursal"
        Me.ConsultaImpresoraSucursalBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Impresora_SucursalBindingSource
        '
        Me.Consulta_Impresora_SucursalBindingSource.DataMember = "Consulta_Impresora_Sucursal"
        Me.Consulta_Impresora_SucursalBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Consulta_Impresora_SucursalTableAdapter
        '
        Me.Consulta_Impresora_SucursalTableAdapter.ClearBeforeFill = True
        '
        'Inserta_impresora_sucursalBindingSource
        '
        Me.Inserta_impresora_sucursalBindingSource.DataMember = "inserta_impresora_sucursal"
        Me.Inserta_impresora_sucursalBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_impresora_sucursalTableAdapter
        '
        Me.Inserta_impresora_sucursalTableAdapter.ClearBeforeFill = True
        '
        'Borra_Impresora_SucursalesBindingSource
        '
        Me.Borra_Impresora_SucursalesBindingSource.DataMember = "Borra_Impresora_Sucursales"
        Me.Borra_Impresora_SucursalesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Borra_Impresora_SucursalesTableAdapter
        '
        Me.Borra_Impresora_SucursalesTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Generales_FacturasGlobalesTableAdapter
        '
        Me.Consulta_Generales_FacturasGlobalesTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Generales_FacturaGlobalBindingSource
        '
        Me.Inserta_Generales_FacturaGlobalBindingSource.DataMember = "Inserta_Generales_FacturaGlobal"
        Me.Inserta_Generales_FacturaGlobalBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Generales_FacturaGlobalTableAdapter
        '
        Me.Inserta_Generales_FacturaGlobalTableAdapter.ClearBeforeFill = True
        '
        'Borra_Generales_FacturasGlobalesBindingSource
        '
        Me.Borra_Generales_FacturasGlobalesBindingSource.DataMember = "Borra_Generales_FacturasGlobales"
        Me.Borra_Generales_FacturasGlobalesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Borra_Generales_FacturasGlobalesTableAdapter
        '
        Me.Borra_Generales_FacturasGlobalesTableAdapter.ClearBeforeFill = True
        '
        'Inserta_impresora_sucursalBindingSource1
        '
        Me.Inserta_impresora_sucursalBindingSource1.DataMember = "inserta_impresora_sucursal"
        Me.Inserta_impresora_sucursalBindingSource1.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_impresora_sucursalTableAdapter1
        '
        Me.Inserta_impresora_sucursalTableAdapter1.ClearBeforeFill = True
        '
        'Consulta_Impresora_SucursalTableAdapter1
        '
        Me.Consulta_Impresora_SucursalTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'CONSUCURSALESTableAdapter
        '
        Me.CONSUCURSALESTableAdapter.ClearBeforeFill = True
        '
        'FrmSucursales2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1091, 759)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmSucursales2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Sucursales"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ConsultaImpresoraSucursalBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSUCURSALESBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONSUCURSALESBindingNavigator.ResumeLayout(False)
        Me.CONSUCURSALESBindingNavigator.PerformLayout()
        CType(Me.CONSUCURSALESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaImpresoraSucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_impresora_sucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Impresora_SucursalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Generales_FacturaGlobalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_impresora_sucursalBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents txtCP As System.Windows.Forms.TextBox
    Friend WithEvents txtNumero As System.Windows.Forms.TextBox
    Friend WithEvents txtCiudad As System.Windows.Forms.TextBox
    Friend WithEvents txtMunicipio As System.Windows.Forms.TextBox
    Friend WithEvents txtColonia As System.Windows.Forms.TextBox
    Friend WithEvents txtCalle As System.Windows.Forms.TextBox
    Friend WithEvents MatrizChck As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents SerieFDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Ciudadcombo As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Impresora_TarjetasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Impresora_ContratosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CONSUCURSALESBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSUCURSALESBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_SucursalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ProcedimientosArnoldo2 As Softv.ProcedimientosArnoldo2
    Friend WithEvents Consulta_Generales_FacturasGlobalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As Softv.DataSetarnoldo
    Friend WithEvents ConsultaImpresoraSucursalBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents CONSUCURSALESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewSofTvDataSet As Softv.NewSofTvDataSet
    Friend WithEvents ConsultaImpresoraSucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Impresora_SucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Impresora_SucursalTableAdapter As Softv.DataSetarnoldoTableAdapters.Consulta_Impresora_SucursalTableAdapter
    Friend WithEvents Inserta_impresora_sucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_impresora_sucursalTableAdapter As Softv.DataSetarnoldoTableAdapters.inserta_impresora_sucursalTableAdapter
    Friend WithEvents Borra_Impresora_SucursalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Impresora_SucursalesTableAdapter As Softv.DataSetarnoldoTableAdapters.Borra_Impresora_SucursalesTableAdapter
    Friend WithEvents Consulta_Generales_FacturasGlobalesTableAdapter As Softv.DataSetarnoldoTableAdapters.Consulta_Generales_FacturasGlobalesTableAdapter
    Friend WithEvents Inserta_Generales_FacturaGlobalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Generales_FacturaGlobalTableAdapter As Softv.DataSetarnoldoTableAdapters.Inserta_Generales_FacturaGlobalTableAdapter
    Friend WithEvents Borra_Generales_FacturasGlobalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Generales_FacturasGlobalesTableAdapter As Softv.DataSetarnoldoTableAdapters.Borra_Generales_FacturasGlobalesTableAdapter
    Friend WithEvents Inserta_impresora_sucursalBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_impresora_sucursalTableAdapter1 As Softv.ProcedimientosArnoldo2TableAdapters.inserta_impresora_sucursalTableAdapter
    Friend WithEvents Consulta_Impresora_SucursalTableAdapter1 As Softv.ProcedimientosArnoldo2TableAdapters.Consulta_Impresora_SucursalTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CONSUCURSALESTableAdapter As Softv.NewSofTvDataSetTableAdapters.CONSUCURSALESTableAdapter
    Friend WithEvents tbEmail As System.Windows.Forms.TextBox
    Friend WithEvents tbContacto As System.Windows.Forms.TextBox
    Friend WithEvents tbReferencia As System.Windows.Forms.TextBox
    Friend WithEvents tbHorario As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CboxDom As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonAgregar As System.Windows.Forms.Button
    Friend WithEvents ButtonEliminar As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents UltimoFolioUsadoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SerieTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_EquivalenteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents No_folioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SerieTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents FolioFDTextbox As System.Windows.Forms.TextBox
    Friend WithEvents TxtIdCompania As System.Windows.Forms.TextBox
    Friend WithEvents TxtPais As System.Windows.Forms.TextBox
    Friend WithEvents tbfax As System.Windows.Forms.TextBox
    Friend WithEvents txtLocalidad As System.Windows.Forms.TextBox
    Friend WithEvents tbentrecalles As System.Windows.Forms.TextBox
    Friend WithEvents TxtNumInt As System.Windows.Forms.TextBox
End Class
