﻿Public Class FrmAgrupaTrabajos

    Private Sub FrmAgrupaTrabajos_Load(sender As Object, e As EventArgs) Handles Me.Load
        If OpAT = "M" Then
            Button1.Text = "Modificar"
            TextBox1.Text = GloDescAgruTrabajo
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If TextBox1.Text.Length > 0 Then
                If OpAT = "M" Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@clave", SqlDbType.Int, GloClvAgruTrabajo)
                    BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, TextBox1.Text, 500)
                    BaseII.Inserta("Usp_ModificaAgrupaTrabajos")

                    MsgBox("Modificado con exitó", MsgBoxStyle.Information)
                ElseIf OpAT = "N" Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, TextBox1.Text, 250)
                    BaseII.Inserta("Usp_NuevoAgrupaTrabajos")

                    MsgBox("Guardado con exitó", MsgBoxStyle.Information)
                End If
                Me.Close()
            Else
                MsgBox("Captura una Descripción", MsgBoxStyle.Information)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.Close()
    End Sub
End Class