<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCtrl_ServiciosCli
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label1 As System.Windows.Forms.Label
        Dim RespuestaLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCtrl_ServiciosCli))
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.Muestra_ClientesTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_ClientesTelTableAdapter = New sofTV.DataSetLidia2TableAdapters.Muestra_ClientesTelTableAdapter()
        Me.MUESTRACONTTELBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTTELTableAdapter = New sofTV.DataSetLidia2TableAdapters.MUESTRACONTTELTableAdapter()
        Me.MUESTRAPAQ_ADICBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAPAQ_ADICTableAdapter = New sofTV.DataSetLidia2TableAdapters.MUESTRAPAQ_ADICTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.BindingNavigator4 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigator2 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem1 = New System.Windows.Forms.ToolStripButton()
        Me.GuardarToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton8 = New System.Windows.Forms.ToolStripButton()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.ComboBox12 = New System.Windows.Forms.ComboBox()
        Me.ComboBox14 = New System.Windows.Forms.ComboBox()
        Me.MUESTRADIGITALDELCLIBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MACCABLEMODEMTextBox = New System.Windows.Forms.TextBox()
        Me.CONTRATONETTextBox2 = New System.Windows.Forms.TextBox()
        Me.RespuestaTextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel32 = New System.Windows.Forms.Label()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.CMBLabel29 = New System.Windows.Forms.Label()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.MUESTRACABLEMODEMSDELCLIBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMSDELCLITableAdapter1 = New sofTV.DataSetLidia2TableAdapters.MUESTRACABLEMODEMSDELCLITableAdapter()
        Me.MUESTRACONTNETBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTNETTableAdapter1 = New sofTV.DataSetLidia2TableAdapters.MUESTRACONTNETTableAdapter()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.BindingNavigator3 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton7 = New System.Windows.Forms.ToolStripButton()
        Me.tbSiteId = New System.Windows.Forms.TextBox()
        Me.tbPassword = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        RespuestaLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_ClientesTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTTELBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAPAQ_ADICBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.BindingNavigator4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator4.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator2.SuspendLayout()
        Me.Panel10.SuspendLayout()
        CType(Me.MUESTRADIGITALDELCLIBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTNETBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.BindingNavigator3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.DarkRed
        Label1.Location = New System.Drawing.Point(29, 461)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(153, 18)
        Label1.TabIndex = 1
        Label1.Text = "Servicios al Cliente"
        '
        'RespuestaLabel
        '
        RespuestaLabel.AutoSize = True
        RespuestaLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        RespuestaLabel.Location = New System.Drawing.Point(140, 157)
        RespuestaLabel.Name = "RespuestaLabel"
        RespuestaLabel.Size = New System.Drawing.Size(61, 13)
        RespuestaLabel.TabIndex = 8
        RespuestaLabel.Text = "Respuesta:"
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.White
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(12, 85)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(415, 227)
        Me.TreeView1.TabIndex = 0
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Muestra_ClientesTelBindingSource
        '
        Me.Muestra_ClientesTelBindingSource.DataMember = "Muestra_ClientesTel"
        Me.Muestra_ClientesTelBindingSource.DataSource = Me.DataSetLidia2
        '
        'Muestra_ClientesTelTableAdapter
        '
        Me.Muestra_ClientesTelTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTTELBindingSource
        '
        Me.MUESTRACONTTELBindingSource.DataMember = "MUESTRACONTTEL"
        Me.MUESTRACONTTELBindingSource.DataSource = Me.DataSetLidia2
        '
        'MUESTRACONTTELTableAdapter
        '
        Me.MUESTRACONTTELTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAPAQ_ADICBindingSource
        '
        Me.MUESTRAPAQ_ADICBindingSource.DataMember = "MUESTRAPAQ_ADIC"
        Me.MUESTRAPAQ_ADICBindingSource.DataSource = Me.DataSetLidia2
        '
        'MUESTRAPAQ_ADICTableAdapter
        '
        Me.MUESTRAPAQ_ADICTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(169, 413)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(87, 44)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Digital"
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(340, 147)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(87, 36)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Internet"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(340, 227)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(87, 37)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "Telefonía"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(22, 405)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(87, 44)
        Me.Button4.TabIndex = 5
        Me.Button4.Text = "TV"
        Me.Button4.UseVisualStyleBackColor = False
        Me.Button4.Visible = False
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.BindingNavigator4)
        Me.Panel4.Controls.Add(Me.BindingNavigator1)
        Me.Panel4.Controls.Add(Me.BindingNavigator2)
        Me.Panel4.Location = New System.Drawing.Point(3, 1)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(424, 78)
        Me.Panel4.TabIndex = 27
        Me.Panel4.Visible = False
        '
        'BindingNavigator4
        '
        Me.BindingNavigator4.AddNewItem = Nothing
        Me.BindingNavigator4.CountItem = Nothing
        Me.BindingNavigator4.DeleteItem = Me.ToolStripButton3
        Me.BindingNavigator4.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton3, Me.ToolStripButton4})
        Me.BindingNavigator4.Location = New System.Drawing.Point(0, 51)
        Me.BindingNavigator4.MoveFirstItem = Nothing
        Me.BindingNavigator4.MoveLastItem = Nothing
        Me.BindingNavigator4.MoveNextItem = Nothing
        Me.BindingNavigator4.MovePreviousItem = Nothing
        Me.BindingNavigator4.Name = "BindingNavigator4"
        Me.BindingNavigator4.PositionItem = Nothing
        Me.BindingNavigator4.Size = New System.Drawing.Size(424, 25)
        Me.BindingNavigator4.TabIndex = 7
        Me.BindingNavigator4.Text = "BindingNavigator4"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton3.Size = New System.Drawing.Size(178, 22)
        Me.ToolStripButton3.Text = "&Eliminar Servicios Digitales"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(179, 22)
        Me.ToolStripButton4.Text = "&Agregar Servicios Digitales"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Me.ToolStripButton1
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 26)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.Size = New System.Drawing.Size(424, 25)
        Me.BindingNavigator1.TabIndex = 6
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton1.Size = New System.Drawing.Size(176, 22)
        Me.ToolStripButton1.Text = "Eliminar Paquete &Adicional"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(177, 22)
        Me.ToolStripButton2.Text = "Agregar Paquete &Adicional"
        '
        'BindingNavigator2
        '
        Me.BindingNavigator2.AddNewItem = Nothing
        Me.BindingNavigator2.CountItem = Nothing
        Me.BindingNavigator2.DeleteItem = Me.BindingNavigatorDeleteItem1
        Me.BindingNavigator2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem1, Me.GuardarToolStripButton1, Me.ToolStripButton8})
        Me.BindingNavigator2.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator2.MoveFirstItem = Nothing
        Me.BindingNavigator2.MoveLastItem = Nothing
        Me.BindingNavigator2.MoveNextItem = Nothing
        Me.BindingNavigator2.MovePreviousItem = Nothing
        Me.BindingNavigator2.Name = "BindingNavigator2"
        Me.BindingNavigator2.PositionItem = Nothing
        Me.BindingNavigator2.Size = New System.Drawing.Size(424, 26)
        Me.BindingNavigator2.TabIndex = 5
        Me.BindingNavigator2.Text = "Telefonía"
        '
        'BindingNavigatorDeleteItem1
        '
        Me.BindingNavigatorDeleteItem1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem1.Image = CType(resources.GetObject("BindingNavigatorDeleteItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem1.Name = "BindingNavigatorDeleteItem1"
        Me.BindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem1.Size = New System.Drawing.Size(150, 23)
        Me.BindingNavigatorDeleteItem1.Text = "Eliminar Plan &Tarifario"
        '
        'GuardarToolStripButton1
        '
        Me.GuardarToolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.GuardarToolStripButton1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GuardarToolStripButton1.Image = CType(resources.GetObject("GuardarToolStripButton1.Image"), System.Drawing.Image)
        Me.GuardarToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.GuardarToolStripButton1.Name = "GuardarToolStripButton1"
        Me.GuardarToolStripButton1.Size = New System.Drawing.Size(151, 23)
        Me.GuardarToolStripButton1.Text = "Agregar Plan &Tarifario"
        '
        'ToolStripButton8
        '
        Me.ToolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton8.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton8.ForeColor = System.Drawing.Color.DarkRed
        Me.ToolStripButton8.Image = CType(resources.GetObject("ToolStripButton8.Image"), System.Drawing.Image)
        Me.ToolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton8.Name = "ToolStripButton8"
        Me.ToolStripButton8.Size = New System.Drawing.Size(90, 23)
        Me.ToolStripButton8.Text = "Telefonía"
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.ComboBox12)
        Me.Panel10.Controls.Add(Me.ComboBox14)
        Me.Panel10.Controls.Add(Me.MACCABLEMODEMTextBox)
        Me.Panel10.Controls.Add(Me.CONTRATONETTextBox2)
        Me.Panel10.Controls.Add(RespuestaLabel)
        Me.Panel10.Controls.Add(Me.RespuestaTextBox)
        Me.Panel10.Controls.Add(Me.CMBLabel32)
        Me.Panel10.Controls.Add(Me.Button14)
        Me.Panel10.Controls.Add(Me.CMBLabel29)
        Me.Panel10.Controls.Add(Me.Button13)
        Me.Panel10.Enabled = False
        Me.Panel10.Location = New System.Drawing.Point(168, 461)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(366, 239)
        Me.Panel10.TabIndex = 24
        Me.Panel10.TabStop = True
        Me.Panel10.Visible = False
        '
        'ComboBox12
        '
        Me.ComboBox12.DisplayMember = "Descripcion"
        Me.ComboBox12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox12.FormattingEnabled = True
        Me.ComboBox12.Location = New System.Drawing.Point(25, 164)
        Me.ComboBox12.Name = "ComboBox12"
        Me.ComboBox12.Size = New System.Drawing.Size(246, 23)
        Me.ComboBox12.TabIndex = 24
        Me.ComboBox12.ValueMember = "Clv_Servicio"
        '
        'ComboBox14
        '
        Me.ComboBox14.DataSource = Me.MUESTRADIGITALDELCLIBindingSource
        Me.ComboBox14.DisplayMember = "MACCABLEMODEM"
        Me.ComboBox14.Enabled = False
        Me.ComboBox14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox14.ForeColor = System.Drawing.Color.Red
        Me.ComboBox14.FormattingEnabled = True
        Me.ComboBox14.Location = New System.Drawing.Point(25, 81)
        Me.ComboBox14.Name = "ComboBox14"
        Me.ComboBox14.Size = New System.Drawing.Size(246, 24)
        Me.ComboBox14.TabIndex = 23
        Me.ComboBox14.ValueMember = "CONTRATONET"
        '
        'MUESTRADIGITALDELCLIBindingSource
        '
        Me.MUESTRADIGITALDELCLIBindingSource.DataMember = "MUESTRADIGITALDELCLI"
        '
        'MACCABLEMODEMTextBox
        '
        Me.MACCABLEMODEMTextBox.Location = New System.Drawing.Point(143, 85)
        Me.MACCABLEMODEMTextBox.Name = "MACCABLEMODEMTextBox"
        Me.MACCABLEMODEMTextBox.Size = New System.Drawing.Size(100, 20)
        Me.MACCABLEMODEMTextBox.TabIndex = 11
        Me.MACCABLEMODEMTextBox.TabStop = False
        '
        'CONTRATONETTextBox2
        '
        Me.CONTRATONETTextBox2.Location = New System.Drawing.Point(25, 85)
        Me.CONTRATONETTextBox2.Name = "CONTRATONETTextBox2"
        Me.CONTRATONETTextBox2.Size = New System.Drawing.Size(100, 20)
        Me.CONTRATONETTextBox2.TabIndex = 10
        Me.CONTRATONETTextBox2.TabStop = False
        '
        'RespuestaTextBox
        '
        Me.RespuestaTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.RespuestaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RespuestaTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.RespuestaTextBox.Location = New System.Drawing.Point(143, 169)
        Me.RespuestaTextBox.Name = "RespuestaTextBox"
        Me.RespuestaTextBox.Size = New System.Drawing.Size(100, 13)
        Me.RespuestaTextBox.TabIndex = 9
        Me.RespuestaTextBox.TabStop = False
        '
        'CMBLabel32
        '
        Me.CMBLabel32.AutoSize = True
        Me.CMBLabel32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel32.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBLabel32.Location = New System.Drawing.Point(4, 16)
        Me.CMBLabel32.Name = "CMBLabel32"
        Me.CMBLabel32.Size = New System.Drawing.Size(365, 16)
        Me.CMBLabel32.TabIndex = 8
        Me.CMBLabel32.Text = "Seleccione la Tarjeta que le va Asignar el Paquete:"
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.Color.DarkOrange
        Me.Button14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.ForeColor = System.Drawing.Color.Black
        Me.Button14.Location = New System.Drawing.Point(161, 193)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(139, 33)
        Me.Button14.TabIndex = 26
        Me.Button14.Text = "C&ANCELAR"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'CMBLabel29
        '
        Me.CMBLabel29.AutoSize = True
        Me.CMBLabel29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel29.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBLabel29.Location = New System.Drawing.Point(10, 123)
        Me.CMBLabel29.Name = "CMBLabel29"
        Me.CMBLabel29.Size = New System.Drawing.Size(243, 16)
        Me.CMBLabel29.TabIndex = 4
        Me.CMBLabel29.Text = "Seleccione el Paquete a Asignar :"
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.DarkOrange
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.Black
        Me.Button13.Location = New System.Drawing.Point(16, 193)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(139, 33)
        Me.Button13.TabIndex = 25
        Me.Button13.Text = "G&UARDAR"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'MUESTRACABLEMODEMSDELCLIBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLIBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI"
        Me.MUESTRACABLEMODEMSDELCLIBindingSource.DataSource = Me.DataSetLidia2
        '
        'MUESTRACABLEMODEMSDELCLITableAdapter1
        '
        Me.MUESTRACABLEMODEMSDELCLITableAdapter1.ClearBeforeFill = True
        '
        'MUESTRACONTNETBindingSource1
        '
        Me.MUESTRACONTNETBindingSource1.DataMember = "MUESTRACONTNET"
        Me.MUESTRACONTNETBindingSource1.DataSource = Me.DataSetLidia2
        '
        'MUESTRACONTNETTableAdapter1
        '
        Me.MUESTRACONTNETTableAdapter1.ClearBeforeFill = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(7, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(253, 29)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Servicios de Internet"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(-15, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(481, 78)
        Me.Panel1.TabIndex = 29
        Me.Panel1.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.BindingNavigator3)
        Me.Panel2.Location = New System.Drawing.Point(18, 29)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(424, 28)
        Me.Panel2.TabIndex = 8
        '
        'BindingNavigator3
        '
        Me.BindingNavigator3.AddNewItem = Nothing
        Me.BindingNavigator3.CountItem = Nothing
        Me.BindingNavigator3.DeleteItem = Nothing
        Me.BindingNavigator3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton5, Me.ToolStripButton6, Me.ToolStripButton7})
        Me.BindingNavigator3.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator3.MoveFirstItem = Nothing
        Me.BindingNavigator3.MoveLastItem = Nothing
        Me.BindingNavigator3.MoveNextItem = Nothing
        Me.BindingNavigator3.MovePreviousItem = Nothing
        Me.BindingNavigator3.Name = "BindingNavigator3"
        Me.BindingNavigator3.PositionItem = Nothing
        Me.BindingNavigator3.Size = New System.Drawing.Size(424, 26)
        Me.BindingNavigator3.TabIndex = 7
        Me.BindingNavigator3.Text = "BindingNavigator3"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton5.Size = New System.Drawing.Size(123, 23)
        Me.ToolStripButton5.Text = "Eliminar  &Servicio"
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(124, 23)
        Me.ToolStripButton6.Text = "Agrega&r Servicio "
        '
        'ToolStripButton7
        '
        Me.ToolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton7.ForeColor = System.Drawing.Color.DarkRed
        Me.ToolStripButton7.Image = CType(resources.GetObject("ToolStripButton7.Image"), System.Drawing.Image)
        Me.ToolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton7.Name = "ToolStripButton7"
        Me.ToolStripButton7.Size = New System.Drawing.Size(81, 23)
        Me.ToolStripButton7.Text = "Internet"
        '
        'tbSiteId
        '
        Me.tbSiteId.BackColor = System.Drawing.Color.White
        Me.tbSiteId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbSiteId.CausesValidation = False
        Me.tbSiteId.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSiteId.Location = New System.Drawing.Point(63, 314)
        Me.tbSiteId.Name = "tbSiteId"
        Me.tbSiteId.Size = New System.Drawing.Size(155, 21)
        Me.tbSiteId.TabIndex = 32
        Me.tbSiteId.TabStop = False
        Me.tbSiteId.Visible = False
        '
        'tbPassword
        '
        Me.tbPassword.BackColor = System.Drawing.Color.White
        Me.tbPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbPassword.CausesValidation = False
        Me.tbPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPassword.Location = New System.Drawing.Point(297, 314)
        Me.tbPassword.Name = "tbPassword"
        Me.tbPassword.Size = New System.Drawing.Size(130, 21)
        Me.tbPassword.TabIndex = 33
        Me.tbPassword.TabStop = False
        Me.tbPassword.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(220, 318)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 15)
        Me.Label3.TabIndex = 34
        Me.Label3.Text = "Password:"
        Me.Label3.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(12, 318)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 15)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "SiteId:"
        Me.Label4.Visible = False
        '
        'FrmCtrl_ServiciosCli
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(437, 349)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tbPassword)
        Me.Controls.Add(Me.tbSiteId)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel10)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Location = New System.Drawing.Point(0, 330)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmCtrl_ServiciosCli"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Servicios del Cliente"
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_ClientesTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTTELBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAPAQ_ADICBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.BindingNavigator4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator4.ResumeLayout(False)
        Me.BindingNavigator4.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator2.ResumeLayout(False)
        Me.BindingNavigator2.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.MUESTRADIGITALDELCLIBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTNETBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.BindingNavigator3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator3.ResumeLayout(False)
        Me.BindingNavigator3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRACABLEMODEMSDELCLITableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLITableAdapter
    Friend WithEvents MUESTRACONTNETBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTNETTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter
    Friend WithEvents MUESTRADIGITALDELCLIBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRADIGITALDELCLITableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRADIGITALDELCLITableAdapter
    Friend WithEvents MUESTRACONTDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIGTableAdapter
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents Muestra_ClientesTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_ClientesTelTableAdapter As sofTV.DataSetLidia2TableAdapters.Muestra_ClientesTelTableAdapter
    Friend WithEvents MUESTRACONTTELBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTTELTableAdapter As sofTV.DataSetLidia2TableAdapters.MUESTRACONTTELTableAdapter
    Friend WithEvents MUESTRAPAQ_ADICBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAPAQ_ADICTableAdapter As sofTV.DataSetLidia2TableAdapters.MUESTRAPAQ_ADICTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigator2 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents GuardarToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox12 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox14 As System.Windows.Forms.ComboBox
    Friend WithEvents MACCABLEMODEMTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONTRATONETTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents RespuestaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel32 As System.Windows.Forms.Label
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel29 As System.Windows.Forms.Label
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents MUESTRACABLEMODEMSDELCLIBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMSDELCLITableAdapter1 As sofTV.DataSetLidia2TableAdapters.MUESTRACABLEMODEMSDELCLITableAdapter
    Friend WithEvents MUESTRACONTNETBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTNETTableAdapter1 As sofTV.DataSetLidia2TableAdapters.MUESTRACONTNETTableAdapter
    Friend WithEvents BindingNavigator4 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents BindingNavigator3 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton8 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Public WithEvents tbSiteId As System.Windows.Forms.TextBox
    Public WithEvents tbPassword As System.Windows.Forms.TextBox
End Class
