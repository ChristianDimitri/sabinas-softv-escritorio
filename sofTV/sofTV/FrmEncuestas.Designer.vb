<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEncuestas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEncuestas))
        Me.TreeViewIzq = New System.Windows.Forms.TreeView()
        Me.TreeViewDer = New System.Windows.Forms.TreeView()
        Me.ButtonInsertarUno = New System.Windows.Forms.Button()
        Me.ButtonInsertarTodos = New System.Windows.Forms.Button()
        Me.ButtonEliminarUno = New System.Windows.Forms.Button()
        Me.ButtonEliminarTodos = New System.Windows.Forms.Button()
        Me.TextBoxNombre = New System.Windows.Forms.TextBox()
        Me.TextBoxDescripcion = New System.Windows.Forms.TextBox()
        Me.CheckBoxActiva = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BindingNavigatorEncuestas = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.TSBEliminar = New System.Windows.Forms.ToolStripButton()
        Me.TSBGuardar = New System.Windows.Forms.ToolStripButton()
        Me.TSBNuevo = New System.Windows.Forms.ToolStripButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ButtonSalir = New System.Windows.Forms.Button()
        CType(Me.BindingNavigatorEncuestas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigatorEncuestas.SuspendLayout()
        Me.SuspendLayout()
        '
        'TreeViewIzq
        '
        Me.TreeViewIzq.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewIzq.Location = New System.Drawing.Point(39, 215)
        Me.TreeViewIzq.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeViewIzq.Name = "TreeViewIzq"
        Me.TreeViewIzq.Size = New System.Drawing.Size(572, 564)
        Me.TreeViewIzq.TabIndex = 0
        '
        'TreeViewDer
        '
        Me.TreeViewDer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewDer.Location = New System.Drawing.Point(753, 215)
        Me.TreeViewDer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeViewDer.Name = "TreeViewDer"
        Me.TreeViewDer.Size = New System.Drawing.Size(572, 564)
        Me.TreeViewDer.TabIndex = 1
        '
        'ButtonInsertarUno
        '
        Me.ButtonInsertarUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonInsertarUno.Location = New System.Drawing.Point(641, 391)
        Me.ButtonInsertarUno.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonInsertarUno.Name = "ButtonInsertarUno"
        Me.ButtonInsertarUno.Size = New System.Drawing.Size(89, 28)
        Me.ButtonInsertarUno.TabIndex = 2
        Me.ButtonInsertarUno.Text = ">"
        Me.ButtonInsertarUno.UseVisualStyleBackColor = True
        '
        'ButtonInsertarTodos
        '
        Me.ButtonInsertarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonInsertarTodos.Location = New System.Drawing.Point(641, 427)
        Me.ButtonInsertarTodos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonInsertarTodos.Name = "ButtonInsertarTodos"
        Me.ButtonInsertarTodos.Size = New System.Drawing.Size(89, 28)
        Me.ButtonInsertarTodos.TabIndex = 3
        Me.ButtonInsertarTodos.Text = ">>"
        Me.ButtonInsertarTodos.UseVisualStyleBackColor = True
        '
        'ButtonEliminarUno
        '
        Me.ButtonEliminarUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminarUno.Location = New System.Drawing.Point(641, 517)
        Me.ButtonEliminarUno.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonEliminarUno.Name = "ButtonEliminarUno"
        Me.ButtonEliminarUno.Size = New System.Drawing.Size(89, 28)
        Me.ButtonEliminarUno.TabIndex = 4
        Me.ButtonEliminarUno.Text = "<"
        Me.ButtonEliminarUno.UseVisualStyleBackColor = True
        '
        'ButtonEliminarTodos
        '
        Me.ButtonEliminarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminarTodos.Location = New System.Drawing.Point(641, 553)
        Me.ButtonEliminarTodos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonEliminarTodos.Name = "ButtonEliminarTodos"
        Me.ButtonEliminarTodos.Size = New System.Drawing.Size(89, 28)
        Me.ButtonEliminarTodos.TabIndex = 5
        Me.ButtonEliminarTodos.Text = "<<"
        Me.ButtonEliminarTodos.UseVisualStyleBackColor = True
        '
        'TextBoxNombre
        '
        Me.TextBoxNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNombre.Location = New System.Drawing.Point(501, 59)
        Me.TextBoxNombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNombre.MaxLength = 50
        Me.TextBoxNombre.Name = "TextBoxNombre"
        Me.TextBoxNombre.Size = New System.Drawing.Size(309, 24)
        Me.TextBoxNombre.TabIndex = 6
        '
        'TextBoxDescripcion
        '
        Me.TextBoxDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxDescripcion.Location = New System.Drawing.Point(501, 91)
        Me.TextBoxDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxDescripcion.MaxLength = 250
        Me.TextBoxDescripcion.Name = "TextBoxDescripcion"
        Me.TextBoxDescripcion.Size = New System.Drawing.Size(628, 24)
        Me.TextBoxDescripcion.TabIndex = 7
        '
        'CheckBoxActiva
        '
        Me.CheckBoxActiva.AutoSize = True
        Me.CheckBoxActiva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxActiva.Location = New System.Drawing.Point(433, 138)
        Me.CheckBoxActiva.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBoxActiva.Name = "CheckBoxActiva"
        Me.CheckBoxActiva.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBoxActiva.Size = New System.Drawing.Size(75, 22)
        Me.CheckBoxActiva.TabIndex = 8
        Me.CheckBoxActiva.Text = "Activa"
        Me.CheckBoxActiva.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(416, 66)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 18)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Nombre"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(383, 98)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 18)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Descripción"
        '
        'BindingNavigatorEncuestas
        '
        Me.BindingNavigatorEncuestas.AddNewItem = Nothing
        Me.BindingNavigatorEncuestas.CountItem = Nothing
        Me.BindingNavigatorEncuestas.DeleteItem = Nothing
        Me.BindingNavigatorEncuestas.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorEncuestas.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.BindingNavigatorEncuestas.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSBEliminar, Me.TSBGuardar, Me.TSBNuevo})
        Me.BindingNavigatorEncuestas.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigatorEncuestas.MoveFirstItem = Nothing
        Me.BindingNavigatorEncuestas.MoveLastItem = Nothing
        Me.BindingNavigatorEncuestas.MoveNextItem = Nothing
        Me.BindingNavigatorEncuestas.MovePreviousItem = Nothing
        Me.BindingNavigatorEncuestas.Name = "BindingNavigatorEncuestas"
        Me.BindingNavigatorEncuestas.PositionItem = Nothing
        Me.BindingNavigatorEncuestas.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigatorEncuestas.Size = New System.Drawing.Size(1355, 27)
        Me.BindingNavigatorEncuestas.TabIndex = 27
        Me.BindingNavigatorEncuestas.Text = "BindingNavigator1"
        '
        'TSBEliminar
        '
        Me.TSBEliminar.Image = CType(resources.GetObject("TSBEliminar.Image"), System.Drawing.Image)
        Me.TSBEliminar.Name = "TSBEliminar"
        Me.TSBEliminar.RightToLeftAutoMirrorImage = True
        Me.TSBEliminar.Size = New System.Drawing.Size(110, 24)
        Me.TSBEliminar.Text = "&ELIMINAR"
        '
        'TSBGuardar
        '
        Me.TSBGuardar.Image = CType(resources.GetObject("TSBGuardar.Image"), System.Drawing.Image)
        Me.TSBGuardar.Name = "TSBGuardar"
        Me.TSBGuardar.Size = New System.Drawing.Size(107, 24)
        Me.TSBGuardar.Text = "&GUARDAR"
        '
        'TSBNuevo
        '
        Me.TSBNuevo.Image = CType(resources.GetObject("TSBNuevo.Image"), System.Drawing.Image)
        Me.TSBNuevo.Name = "TSBNuevo"
        Me.TSBNuevo.RightToLeftAutoMirrorImage = True
        Me.TSBNuevo.Size = New System.Drawing.Size(86, 24)
        Me.TSBNuevo.Text = "&NUEVO"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(35, 193)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(174, 18)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "Preguntas Sin Asignar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(749, 193)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(166, 18)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "Preguntas Asignadas"
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(1157, 844)
        Me.ButtonSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(181, 44)
        Me.ButtonSalir.TabIndex = 30
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'FrmEncuestas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1355, 903)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.BindingNavigatorEncuestas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CheckBoxActiva)
        Me.Controls.Add(Me.TextBoxDescripcion)
        Me.Controls.Add(Me.TextBoxNombre)
        Me.Controls.Add(Me.ButtonEliminarTodos)
        Me.Controls.Add(Me.ButtonEliminarUno)
        Me.Controls.Add(Me.ButtonInsertarTodos)
        Me.Controls.Add(Me.ButtonInsertarUno)
        Me.Controls.Add(Me.TreeViewDer)
        Me.Controls.Add(Me.TreeViewIzq)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmEncuestas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Encuestas"
        CType(Me.BindingNavigatorEncuestas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigatorEncuestas.ResumeLayout(False)
        Me.BindingNavigatorEncuestas.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TreeViewIzq As System.Windows.Forms.TreeView
    Friend WithEvents TreeViewDer As System.Windows.Forms.TreeView
    Friend WithEvents ButtonInsertarUno As System.Windows.Forms.Button
    Friend WithEvents ButtonInsertarTodos As System.Windows.Forms.Button
    Friend WithEvents ButtonEliminarUno As System.Windows.Forms.Button
    Friend WithEvents ButtonEliminarTodos As System.Windows.Forms.Button
    Friend WithEvents TextBoxNombre As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxActiva As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BindingNavigatorEncuestas As System.Windows.Forms.BindingNavigator
    Friend WithEvents TSBEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSBGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSBNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
End Class
