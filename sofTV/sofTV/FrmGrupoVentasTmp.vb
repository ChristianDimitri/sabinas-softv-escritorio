﻿Public Class FrmGrupoVentasTmp

    Private Sub bnAceptar_Click(sender As Object, e As EventArgs) Handles bnAceptar.Click


        If lbDerecha.Items.Count = 0 Then
            MessageBox.Show("Selecciona la menos un elemento")
        End If

        If opRep = 8 Then
            FrmTipServTmp.Show()
            Me.Close()
        End If


    End Sub

    Private Sub FrmGrupoVentasTmp_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        llenar_lb(0)
        llenar_lb(1)

    End Sub

    Private Sub llenar_lb(opcion As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CType(opcion, Integer))
        BaseII.CreateMyParameter("@IdSession", SqlDbType.BigInt, CType(IdSessionRep, Long))
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, CType(identificador, Integer))

        If opcion = 0 Then
            lbIzquierda.DataSource = BaseII.ConsultaDT("CONtbl_GrupoVentasTmp")
        Else
            lbDerecha.DataSource = BaseII.ConsultaDT("CONtbl_GrupoVentasTmp")
        End If


    End Sub


    Private Sub button1_Click(sender As Object, e As EventArgs) Handles button1.Click

        Nue(0, lbIzquierda.SelectedValue)
        llenar_lb(0)
        llenar_lb(1)

    End Sub

    Private Sub Nue(opcion As Integer, Clv_Grupo As Integer)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CType(opcion, Integer))
        BaseII.CreateMyParameter("@IdSession", SqlDbType.BigInt, CType(IdSessionRep, Long))
        BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CType(Clv_Grupo, Integer))
        BaseII.ConsultaDT("NUEtbl_GrupoVentasTmp")

    End Sub



    Private Sub button2_Click(sender As Object, e As EventArgs) Handles button2.Click
        Nue(1, 0)
        llenar_lb(0)
        llenar_lb(1)
    End Sub

    Private Sub button3_Click(sender As Object, e As EventArgs) Handles button3.Click
        Bor(0, IdSessionRep, lbDerecha.SelectedValue)
        llenar_lb(0)
        llenar_lb(1)
    End Sub

    Private Sub Bor(opcion As Integer, idSession As Long, Clv_Grupo As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CType(opcion, Integer))
        BaseII.CreateMyParameter("@IdSession", SqlDbType.BigInt, CType(IdSessionRep, Long))
        BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CType(Clv_Grupo, Integer))
        BaseII.ConsultaDT("BORtbl_GrupoVentasTmp")
    End Sub

    Private Sub button4_Click(sender As Object, e As EventArgs) Handles button4.Click
        Bor(1, IdSessionRep, 0)
        llenar_lb(0)
        llenar_lb(1)
    End Sub

    Private Sub bnSalir_Click(sender As Object, e As EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class