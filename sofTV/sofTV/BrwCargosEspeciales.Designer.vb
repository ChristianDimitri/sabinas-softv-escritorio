<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCargosEspeciales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ClienteLabel As System.Windows.Forms.Label
        Dim MontoLabel As System.Windows.Forms.Label
        Dim Se_cobroLabel As System.Windows.Forms.Label
        Dim Clv_cobroLabel2 As System.Windows.Forms.Label
        Dim Fecha_cobroLabel As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.LabelFechaF = New System.Windows.Forms.Label()
        Me.LabelFecha = New System.Windows.Forms.Label()
        Me.LabelSeCobro = New System.Windows.Forms.Label()
        Me.LabelMonto = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LabelContrato = New System.Windows.Forms.Label()
        Me.LabelClave = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Clv_Cobro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Se_Cobro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Motivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha_Cobro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha_Fin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        ClienteLabel = New System.Windows.Forms.Label()
        MontoLabel = New System.Windows.Forms.Label()
        Se_cobroLabel = New System.Windows.Forms.Label()
        Clv_cobroLabel2 = New System.Windows.Forms.Label()
        Fecha_cobroLabel = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Me.CMBPanel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ClienteLabel
        '
        ClienteLabel.AutoSize = True
        ClienteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClienteLabel.Location = New System.Drawing.Point(32, 92)
        ClienteLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ClienteLabel.Name = "ClienteLabel"
        ClienteLabel.Size = New System.Drawing.Size(65, 18)
        ClienteLabel.TabIndex = 0
        ClienteLabel.Text = "Cliente:"
        '
        'MontoLabel
        '
        MontoLabel.AutoSize = True
        MontoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MontoLabel.Location = New System.Drawing.Point(40, 130)
        MontoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        MontoLabel.Name = "MontoLabel"
        MontoLabel.Size = New System.Drawing.Size(61, 18)
        MontoLabel.TabIndex = 4
        MontoLabel.Text = "Monto:"
        '
        'Se_cobroLabel
        '
        Se_cobroLabel.AutoSize = True
        Se_cobroLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Se_cobroLabel.Location = New System.Drawing.Point(16, 170)
        Se_cobroLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Se_cobroLabel.Name = "Se_cobroLabel"
        Se_cobroLabel.Size = New System.Drawing.Size(77, 18)
        Se_cobroLabel.TabIndex = 6
        Se_cobroLabel.Text = "Aplicado:"
        '
        'Clv_cobroLabel2
        '
        Clv_cobroLabel2.AutoSize = True
        Clv_cobroLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_cobroLabel2.Location = New System.Drawing.Point(45, 63)
        Clv_cobroLabel2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_cobroLabel2.Name = "Clv_cobroLabel2"
        Clv_cobroLabel2.Size = New System.Drawing.Size(55, 18)
        Clv_cobroLabel2.TabIndex = 428
        Clv_cobroLabel2.Text = "Clave:"
        '
        'Fecha_cobroLabel
        '
        Fecha_cobroLabel.AutoSize = True
        Fecha_cobroLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Fecha_cobroLabel.Location = New System.Drawing.Point(16, 209)
        Fecha_cobroLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Fecha_cobroLabel.Name = "Fecha_cobroLabel"
        Fecha_cobroLabel.Size = New System.Drawing.Size(156, 18)
        Fecha_cobroLabel.TabIndex = 429
        Fecha_cobroLabel.Text = "Fecha Generación: "
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label3.Location = New System.Drawing.Point(17, 260)
        Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(146, 18)
        Label3.TabIndex = 431
        Label3.Text = "Fecha Aplicación: "
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(1171, 857)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 11
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CMBPanel1
        '
        Me.CMBPanel1.Controls.Add(Me.LabelFechaF)
        Me.CMBPanel1.Controls.Add(Label3)
        Me.CMBPanel1.Controls.Add(Me.LabelFecha)
        Me.CMBPanel1.Controls.Add(Fecha_cobroLabel)
        Me.CMBPanel1.Controls.Add(Me.LabelSeCobro)
        Me.CMBPanel1.Controls.Add(Clv_cobroLabel2)
        Me.CMBPanel1.Controls.Add(Me.LabelMonto)
        Me.CMBPanel1.Controls.Add(Me.Label1)
        Me.CMBPanel1.Controls.Add(ClienteLabel)
        Me.CMBPanel1.Controls.Add(Me.LabelContrato)
        Me.CMBPanel1.Controls.Add(Me.LabelClave)
        Me.CMBPanel1.Controls.Add(MontoLabel)
        Me.CMBPanel1.Controls.Add(Se_cobroLabel)
        Me.CMBPanel1.Location = New System.Drawing.Point(4, 345)
        Me.CMBPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(392, 384)
        Me.CMBPanel1.TabIndex = 422
        '
        'LabelFechaF
        '
        Me.LabelFechaF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFechaF.Location = New System.Drawing.Point(21, 278)
        Me.LabelFechaF.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelFechaF.Name = "LabelFechaF"
        Me.LabelFechaF.Size = New System.Drawing.Size(257, 25)
        Me.LabelFechaF.TabIndex = 430
        Me.LabelFechaF.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelFecha
        '
        Me.LabelFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFecha.Location = New System.Drawing.Point(20, 228)
        Me.LabelFecha.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelFecha.Name = "LabelFecha"
        Me.LabelFecha.Size = New System.Drawing.Size(257, 25)
        Me.LabelFecha.TabIndex = 6
        Me.LabelFecha.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelSeCobro
        '
        Me.LabelSeCobro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSeCobro.Location = New System.Drawing.Point(115, 164)
        Me.LabelSeCobro.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelSeCobro.Name = "LabelSeCobro"
        Me.LabelSeCobro.Size = New System.Drawing.Size(257, 25)
        Me.LabelSeCobro.TabIndex = 5
        Me.LabelSeCobro.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelMonto
        '
        Me.LabelMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMonto.Location = New System.Drawing.Point(115, 124)
        Me.LabelMonto.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelMonto.Name = "LabelMonto"
        Me.LabelMonto.Size = New System.Drawing.Size(257, 25)
        Me.LabelMonto.TabIndex = 4
        Me.LabelMonto.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 12)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(247, 24)
        Me.Label1.TabIndex = 427
        Me.Label1.Text = "Datos Del Cargo Especial"
        '
        'LabelContrato
        '
        Me.LabelContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelContrato.Location = New System.Drawing.Point(115, 86)
        Me.LabelContrato.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelContrato.Name = "LabelContrato"
        Me.LabelContrato.Size = New System.Drawing.Size(257, 25)
        Me.LabelContrato.TabIndex = 2
        Me.LabelContrato.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelClave
        '
        Me.LabelClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelClave.Location = New System.Drawing.Point(115, 57)
        Me.LabelClave.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelClave.Name = "LabelClave"
        Me.LabelClave.Size = New System.Drawing.Size(257, 25)
        Me.LabelClave.TabIndex = 1
        Me.LabelClave.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.DataGridView1)
        Me.Panel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(429, 15)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(720, 882)
        Me.Panel2.TabIndex = 423
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Cobro, Me.Clv_Servicio, Me.Cliente, Me.Monto, Me.Se_Cobro, Me.Motivo, Me.Fecha_Cobro, Me.Fecha_Fin})
        Me.DataGridView1.Location = New System.Drawing.Point(4, 4)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(712, 875)
        Me.DataGridView1.TabIndex = 0
        '
        'Clv_Cobro
        '
        Me.Clv_Cobro.DataPropertyName = "Clv_Cobro"
        Me.Clv_Cobro.HeaderText = "Clave"
        Me.Clv_Cobro.Name = "Clv_Cobro"
        Me.Clv_Cobro.ReadOnly = True
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.ReadOnly = True
        Me.Clv_Servicio.Visible = False
        '
        'Cliente
        '
        Me.Cliente.DataPropertyName = "Cliente"
        Me.Cliente.HeaderText = "Contrato"
        Me.Cliente.Name = "Cliente"
        Me.Cliente.ReadOnly = True
        '
        'Monto
        '
        Me.Monto.DataPropertyName = "Monto"
        Me.Monto.HeaderText = "Monto"
        Me.Monto.Name = "Monto"
        Me.Monto.ReadOnly = True
        '
        'Se_Cobro
        '
        Me.Se_Cobro.DataPropertyName = "Se_Cobro"
        Me.Se_Cobro.HeaderText = "Aplicado"
        Me.Se_Cobro.Name = "Se_Cobro"
        Me.Se_Cobro.ReadOnly = True
        '
        'Motivo
        '
        Me.Motivo.DataPropertyName = "Motivo"
        Me.Motivo.HeaderText = "Motivo"
        Me.Motivo.Name = "Motivo"
        Me.Motivo.ReadOnly = True
        Me.Motivo.Visible = False
        '
        'Fecha_Cobro
        '
        Me.Fecha_Cobro.DataPropertyName = "Fecha_Cobro"
        Me.Fecha_Cobro.HeaderText = "Fecha Generación"
        Me.Fecha_Cobro.Name = "Fecha_Cobro"
        Me.Fecha_Cobro.ReadOnly = True
        '
        'Fecha_Fin
        '
        Me.Fecha_Fin.DataPropertyName = "FechaAplicacion"
        Me.Fecha_Fin.HeaderText = "Fecha Aplicación"
        Me.Fecha_Fin.Name = "Fecha_Fin"
        Me.Fecha_Fin.ReadOnly = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(1171, 15)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 41)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "&NUEVO"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(1171, 74)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 41)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "&CONSULTAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(1171, 142)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(181, 41)
        Me.Button3.TabIndex = 10
        Me.Button3.Text = "&MODIFICAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(28, 84)
        Me.RadioButton1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(112, 24)
        Me.RadioButton1.TabIndex = 3
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Aplicadas"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(176, 84)
        Me.RadioButton2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(141, 24)
        Me.RadioButton2.TabIndex = 2
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "No Aplicadas"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Checked = True
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.Location = New System.Drawing.Point(29, 116)
        Me.RadioButton3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(87, 24)
        Me.RadioButton3.TabIndex = 1
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Ambos"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(24, 30)
        Me.CMBLabel2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(231, 25)
        Me.CMBLabel2.TabIndex = 431
        Me.CMBLabel2.Text = "Opciones para buscar:"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(12, 57)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(296, 24)
        Me.TextBox2.TabIndex = 4
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(9, 22)
        Me.CMBLabel4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(74, 18)
        Me.CMBLabel4.TabIndex = 437
        Me.CMBLabel4.Text = "Contrato"
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(12, 90)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(117, 28)
        Me.Button6.TabIndex = 5
        Me.Button6.Text = "&Buscar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.CMBLabel4)
        Me.Panel3.Controls.Add(Me.TextBox2)
        Me.Panel3.Controls.Add(Me.Button6)
        Me.Panel3.Controls.Add(Me.CMBPanel1)
        Me.Panel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel3.Location = New System.Drawing.Point(16, 161)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(409, 737)
        Me.Panel3.TabIndex = 439
        '
        'BrwCargosEspeciales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1365, 923)
        Me.Controls.Add(Me.RadioButton3)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.RadioButton2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Button5)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwCargosEspeciales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Busca Cargos Especiales"
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents LabelFecha As System.Windows.Forms.Label
    Friend WithEvents LabelSeCobro As System.Windows.Forms.Label
    Friend WithEvents LabelMonto As System.Windows.Forms.Label
    Friend WithEvents LabelContrato As System.Windows.Forms.Label
    Friend WithEvents LabelClave As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents LabelFechaF As System.Windows.Forms.Label
    Friend WithEvents Clv_Cobro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Se_Cobro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Motivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha_Cobro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha_Fin As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
