﻿Public Class FrmMostrarIpProvisionamientoOrden
    Public ContratoIpOrden As Integer
    Dim cont As Integer = 0
    Dim contBtnSalir As Integer = 0
    Private Sub FrmMostrarIpProvisionamientoOrden_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Timer1.Start()
        Timer2.Start()
        ConsultaIpPorContrato()
    End Sub

    Private Sub ConsultaIpPorContrato()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoIpOrden)
        BaseII.CreateMyParameter("@ip", ParameterDirection.Output, SqlDbType.VarChar, 20)
        BaseII.ProcedimientoOutPut("ConsultaIpPorContrato")
        TextBox1.Text = BaseII.dicoPar("@ip").ToString
        If TextBox1.Text.Length > 0 Then
            Timer1.Stop()
            Timer2.Stop()
            BtnAceptar.Enabled = True
            ProgressBar1.Value = 100
        End If

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        cont = 0
        ConsultaIpPorContrato()
        If btnSalir.Visible = False Then
            contBtnSalir = contBtnSalir + 10
            If contBtnSalir >= 60 Then
                btnSalir.Visible = True
            End If
        End If

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        cont = cont + 10
        ProgressBar1.Value = cont
    End Sub
End Class