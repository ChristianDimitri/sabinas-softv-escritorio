<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmVendedores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_VendedorLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim DomicilioLabel As System.Windows.Forms.Label
        Dim ColoniaLabel As System.Windows.Forms.Label
        Dim FechaIngresoLabel As System.Windows.Forms.Label
        Dim FechaSalidaLabel As System.Windows.Forms.Label
        Dim ActivoLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmVendedores))
        Me.Clv_VendedorTextBox = New System.Windows.Forms.TextBox()
        Me.CONVENDEDORESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New sofTV.NewsoftvDataSet1()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.DomicilioTextBox = New System.Windows.Forms.TextBox()
        Me.ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.ActivoCheckBox = New System.Windows.Forms.CheckBox()
        Me.CONVENDEDORESBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONVENDEDORESBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer()
        Me.cbTecnico = New System.Windows.Forms.ComboBox()
        Me.cbRecuperador = New System.Windows.Forms.CheckBox()
        Me.cbCapacitacion = New System.Windows.Forms.CheckBox()
        Me.btnDocumentos = New System.Windows.Forms.Button()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.cbCobroADomicilio = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBoxLogin = New System.Windows.Forms.TextBox()
        Me.TextBoxPasaporte = New System.Windows.Forms.TextBox()
        Me.FechaSalidaTextBox = New System.Windows.Forms.TextBox()
        Me.FechaIngresoTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CONVENDEDORESTableAdapter = New sofTV.NewsoftvDataSet1TableAdapters.CONVENDEDORESTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Clv_VendedorLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        DomicilioLabel = New System.Windows.Forms.Label()
        ColoniaLabel = New System.Windows.Forms.Label()
        FechaIngresoLabel = New System.Windows.Forms.Label()
        FechaSalidaLabel = New System.Windows.Forms.Label()
        ActivoLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        CType(Me.CONVENDEDORESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONVENDEDORESBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONVENDEDORESBindingNavigator.SuspendLayout()
        Me.ToolStripContainer1.ContentPanel.SuspendLayout()
        Me.ToolStripContainer1.TopToolStripPanel.SuspendLayout()
        Me.ToolStripContainer1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_VendedorLabel
        '
        Clv_VendedorLabel.AutoSize = True
        Clv_VendedorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_VendedorLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_VendedorLabel.Location = New System.Drawing.Point(41, 10)
        Clv_VendedorLabel.Name = "Clv_VendedorLabel"
        Clv_VendedorLabel.Size = New System.Drawing.Size(115, 15)
        Clv_VendedorLabel.TabIndex = 0
        Clv_VendedorLabel.Text = "Clave Vendedor :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(90, 65)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(66, 15)
        NombreLabel.TabIndex = 2
        NombreLabel.Text = "Nombre :"
        '
        'DomicilioLabel
        '
        DomicilioLabel.AutoSize = True
        DomicilioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DomicilioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DomicilioLabel.Location = New System.Drawing.Point(80, 92)
        DomicilioLabel.Name = "DomicilioLabel"
        DomicilioLabel.Size = New System.Drawing.Size(76, 15)
        DomicilioLabel.TabIndex = 4
        DomicilioLabel.Text = "Domicilio :"
        '
        'ColoniaLabel
        '
        ColoniaLabel.AutoSize = True
        ColoniaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ColoniaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ColoniaLabel.Location = New System.Drawing.Point(88, 164)
        ColoniaLabel.Name = "ColoniaLabel"
        ColoniaLabel.Size = New System.Drawing.Size(64, 15)
        ColoniaLabel.TabIndex = 6
        ColoniaLabel.Text = "Colonia :"
        '
        'FechaIngresoLabel
        '
        FechaIngresoLabel.AutoSize = True
        FechaIngresoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaIngresoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaIngresoLabel.Location = New System.Drawing.Point(701, 68)
        FechaIngresoLabel.Name = "FechaIngresoLabel"
        FechaIngresoLabel.Size = New System.Drawing.Size(106, 15)
        FechaIngresoLabel.TabIndex = 8
        FechaIngresoLabel.Text = "Fecha Ingreso :"
        AddHandler FechaIngresoLabel.Click, AddressOf Me.FechaIngresoLabel_Click
        '
        'FechaSalidaLabel
        '
        FechaSalidaLabel.AutoSize = True
        FechaSalidaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaSalidaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaSalidaLabel.Location = New System.Drawing.Point(708, 95)
        FechaSalidaLabel.Name = "FechaSalidaLabel"
        FechaSalidaLabel.Size = New System.Drawing.Size(99, 15)
        FechaSalidaLabel.TabIndex = 10
        FechaSalidaLabel.Text = "Fecha Salida :"
        '
        'ActivoLabel
        '
        ActivoLabel.AutoSize = True
        ActivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ActivoLabel.ForeColor = System.Drawing.Color.Red
        ActivoLabel.Location = New System.Drawing.Point(752, 39)
        ActivoLabel.Name = "ActivoLabel"
        ActivoLabel.Size = New System.Drawing.Size(55, 16)
        ActivoLabel.TabIndex = 12
        ActivoLabel.Text = "Activo:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(74, 68)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(88, 15)
        Label1.TabIndex = 7
        Label1.Text = "Contraseña :"
        AddHandler Label1.Click, AddressOf Me.Label1_Click
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Label3.Location = New System.Drawing.Point(111, 39)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(51, 15)
        Label3.TabIndex = 11
        Label3.Text = "Login :"
        AddHandler Label3.Click, AddressOf Me.Label3_Click
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(583, 15)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(90, 15)
        Label2.TabIndex = 103
        Label2.Text = "Distribuidor :"
        Label2.Visible = False
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.Red
        Label4.Location = New System.Drawing.Point(708, 215)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(103, 16)
        Label4.TabIndex = 206
        Label4.Text = "Capacitación:"
        Label4.Visible = False
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(276, 39)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(65, 15)
        Label5.TabIndex = 435
        Label5.Text = "Tecnico :"
        '
        'Clv_VendedorTextBox
        '
        Me.Clv_VendedorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_VendedorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONVENDEDORESBindingSource, "Clv_Vendedor", True))
        Me.Clv_VendedorTextBox.Enabled = False
        Me.Clv_VendedorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_VendedorTextBox.Location = New System.Drawing.Point(162, 10)
        Me.Clv_VendedorTextBox.Name = "Clv_VendedorTextBox"
        Me.Clv_VendedorTextBox.Size = New System.Drawing.Size(79, 21)
        Me.Clv_VendedorTextBox.TabIndex = 100
        Me.Clv_VendedorTextBox.TabStop = False
        '
        'CONVENDEDORESBindingSource
        '
        Me.CONVENDEDORESBindingSource.DataMember = "CONVENDEDORES"
        Me.CONVENDEDORESBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONVENDEDORESBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(162, 63)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(484, 21)
        Me.NombreTextBox.TabIndex = 0
        '
        'DomicilioTextBox
        '
        Me.DomicilioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DomicilioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONVENDEDORESBindingSource, "Domicilio", True))
        Me.DomicilioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DomicilioTextBox.Location = New System.Drawing.Point(162, 90)
        Me.DomicilioTextBox.MaxLength = 250
        Me.DomicilioTextBox.Multiline = True
        Me.DomicilioTextBox.Name = "DomicilioTextBox"
        Me.DomicilioTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DomicilioTextBox.Size = New System.Drawing.Size(484, 66)
        Me.DomicilioTextBox.TabIndex = 1
        '
        'ColoniaTextBox
        '
        Me.ColoniaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONVENDEDORESBindingSource, "Colonia", True))
        Me.ColoniaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ColoniaTextBox.Location = New System.Drawing.Point(162, 162)
        Me.ColoniaTextBox.Name = "ColoniaTextBox"
        Me.ColoniaTextBox.Size = New System.Drawing.Size(484, 21)
        Me.ColoniaTextBox.TabIndex = 2
        '
        'ActivoCheckBox
        '
        Me.ActivoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONVENDEDORESBindingSource, "Activo", True))
        Me.ActivoCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivoCheckBox.Location = New System.Drawing.Point(813, 35)
        Me.ActivoCheckBox.Name = "ActivoCheckBox"
        Me.ActivoCheckBox.Size = New System.Drawing.Size(81, 24)
        Me.ActivoCheckBox.TabIndex = 3
        '
        'CONVENDEDORESBindingNavigator
        '
        Me.CONVENDEDORESBindingNavigator.AddNewItem = Nothing
        Me.CONVENDEDORESBindingNavigator.BindingSource = Me.CONVENDEDORESBindingSource
        Me.CONVENDEDORESBindingNavigator.CountItem = Nothing
        Me.CONVENDEDORESBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONVENDEDORESBindingNavigator.Dock = System.Windows.Forms.DockStyle.None
        Me.CONVENDEDORESBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONVENDEDORESBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CONVENDEDORESBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.CONVENDEDORESBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONVENDEDORESBindingNavigator.MoveFirstItem = Nothing
        Me.CONVENDEDORESBindingNavigator.MoveLastItem = Nothing
        Me.CONVENDEDORESBindingNavigator.MoveNextItem = Nothing
        Me.CONVENDEDORESBindingNavigator.MovePreviousItem = Nothing
        Me.CONVENDEDORESBindingNavigator.Name = "CONVENDEDORESBindingNavigator"
        Me.CONVENDEDORESBindingNavigator.PositionItem = Nothing
        Me.CONVENDEDORESBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.CONVENDEDORESBindingNavigator.Size = New System.Drawing.Size(244, 27)
        Me.CONVENDEDORESBindingNavigator.TabIndex = 160
        Me.CONVENDEDORESBindingNavigator.TabStop = True
        Me.CONVENDEDORESBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(94, 24)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONVENDEDORESBindingNavigatorSaveItem
        '
        Me.CONVENDEDORESBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONVENDEDORESBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONVENDEDORESBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONVENDEDORESBindingNavigatorSaveItem.Name = "CONVENDEDORESBindingNavigatorSaveItem"
        Me.CONVENDEDORESBindingNavigatorSaveItem.Size = New System.Drawing.Size(138, 24)
        Me.CONVENDEDORESBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'ContentPanel
        '
        Me.ContentPanel.Size = New System.Drawing.Size(121, 150)
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.AutoScroll = True
        Me.ToolStripContainer1.ContentPanel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Label5)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.cbTecnico)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.cbRecuperador)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.cbCapacitacion)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Label4)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.btnDocumentos)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.ComboBoxCompanias)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Label2)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.cbCobroADomicilio)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.GroupBox1)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.FechaSalidaTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.FechaIngresoTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Clv_VendedorLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.Clv_VendedorTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(NombreLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.NombreTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(DomicilioLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.ActivoCheckBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.DomicilioTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(ActivoLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(ColoniaLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.ColoniaTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(FechaSalidaLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(FechaIngresoLabel)
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(992, 341)
        Me.ToolStripContainer1.Location = New System.Drawing.Point(12, 12)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.Size = New System.Drawing.Size(992, 368)
        Me.ToolStripContainer1.TabIndex = 2
        Me.ToolStripContainer1.TabStop = False
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        '
        'ToolStripContainer1.TopToolStripPanel
        '
        Me.ToolStripContainer1.TopToolStripPanel.Controls.Add(Me.CONVENDEDORESBindingNavigator)
        Me.ToolStripContainer1.TopToolStripPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        '
        'cbTecnico
        '
        Me.cbTecnico.DisplayMember = "Nombre"
        Me.cbTecnico.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTecnico.FormattingEnabled = True
        Me.cbTecnico.Location = New System.Drawing.Point(345, 36)
        Me.cbTecnico.Name = "cbTecnico"
        Me.cbTecnico.Size = New System.Drawing.Size(372, 23)
        Me.cbTecnico.TabIndex = 434
        Me.cbTecnico.ValueMember = "clv_Tecnico"
        '
        'cbRecuperador
        '
        Me.cbRecuperador.AutoSize = True
        Me.cbRecuperador.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbRecuperador.Location = New System.Drawing.Point(162, 38)
        Me.cbRecuperador.Name = "cbRecuperador"
        Me.cbRecuperador.Size = New System.Drawing.Size(109, 19)
        Me.cbRecuperador.TabIndex = 207
        Me.cbRecuperador.Text = "Recuperador"
        Me.cbRecuperador.UseVisualStyleBackColor = True
        '
        'cbCapacitacion
        '
        Me.cbCapacitacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCapacitacion.Location = New System.Drawing.Point(813, 211)
        Me.cbCapacitacion.Name = "cbCapacitacion"
        Me.cbCapacitacion.Size = New System.Drawing.Size(81, 24)
        Me.cbCapacitacion.TabIndex = 205
        Me.cbCapacitacion.Visible = False
        '
        'btnDocumentos
        '
        Me.btnDocumentos.BackColor = System.Drawing.Color.DarkOrange
        Me.btnDocumentos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDocumentos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDocumentos.ForeColor = System.Drawing.Color.Black
        Me.btnDocumentos.Location = New System.Drawing.Point(732, 154)
        Me.btnDocumentos.Name = "btnDocumentos"
        Me.btnDocumentos.Size = New System.Drawing.Size(146, 25)
        Me.btnDocumentos.TabIndex = 204
        Me.btnDocumentos.Text = "Documentos"
        Me.btnDocumentos.UseVisualStyleBackColor = False
        Me.btnDocumentos.Visible = False
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONVENDEDORESBindingSource, "idcompania", True))
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(679, 10)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(50, 23)
        Me.ComboBoxCompanias.TabIndex = 203
        Me.ComboBoxCompanias.Visible = False
        '
        'cbCobroADomicilio
        '
        Me.cbCobroADomicilio.AutoSize = True
        Me.cbCobroADomicilio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCobroADomicilio.Location = New System.Drawing.Point(162, 189)
        Me.cbCobroADomicilio.Name = "cbCobroADomicilio"
        Me.cbCobroADomicilio.Size = New System.Drawing.Size(141, 19)
        Me.cbCobroADomicilio.TabIndex = 102
        Me.cbCobroADomicilio.Text = "Cobro a Domicilio"
        Me.cbCobroADomicilio.UseVisualStyleBackColor = True
        Me.cbCobroADomicilio.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Label3)
        Me.GroupBox1.Controls.Add(Me.TextBoxLogin)
        Me.GroupBox1.Controls.Add(Label1)
        Me.GroupBox1.Controls.Add(Me.TextBoxPasaporte)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(162, 211)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(323, 129)
        Me.GroupBox1.TabIndex = 101
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Acceso Web"
        Me.GroupBox1.Visible = False
        '
        'TextBoxLogin
        '
        Me.TextBoxLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxLogin.Location = New System.Drawing.Point(168, 37)
        Me.TextBoxLogin.MaxLength = 8
        Me.TextBoxLogin.Name = "TextBoxLogin"
        Me.TextBoxLogin.Size = New System.Drawing.Size(100, 22)
        Me.TextBoxLogin.TabIndex = 10
        '
        'TextBoxPasaporte
        '
        Me.TextBoxPasaporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxPasaporte.Location = New System.Drawing.Point(168, 66)
        Me.TextBoxPasaporte.MaxLength = 8
        Me.TextBoxPasaporte.Name = "TextBoxPasaporte"
        Me.TextBoxPasaporte.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBoxPasaporte.Size = New System.Drawing.Size(100, 22)
        Me.TextBoxPasaporte.TabIndex = 0
        '
        'FechaSalidaTextBox
        '
        Me.FechaSalidaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FechaSalidaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONVENDEDORESBindingSource, "FechaSalida", True))
        Me.FechaSalidaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaSalidaTextBox.Location = New System.Drawing.Point(813, 95)
        Me.FechaSalidaTextBox.Name = "FechaSalidaTextBox"
        Me.FechaSalidaTextBox.Size = New System.Drawing.Size(100, 21)
        Me.FechaSalidaTextBox.TabIndex = 5
        '
        'FechaIngresoTextBox
        '
        Me.FechaIngresoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FechaIngresoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONVENDEDORESBindingSource, "FechaIngreso", True))
        Me.FechaIngresoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaIngresoTextBox.Location = New System.Drawing.Point(813, 67)
        Me.FechaIngresoTextBox.Name = "FechaIngresoTextBox"
        Me.FechaIngresoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.FechaIngresoTextBox.TabIndex = 4
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(868, 390)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 170
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONVENDEDORESTableAdapter
        '
        Me.CONVENDEDORESTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmVendedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(1016, 435)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.Name = "FrmVendedores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Vendedores"
        CType(Me.CONVENDEDORESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONVENDEDORESBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONVENDEDORESBindingNavigator.ResumeLayout(False)
        Me.CONVENDEDORESBindingNavigator.PerformLayout()
        Me.ToolStripContainer1.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer1.ContentPanel.PerformLayout()
        Me.ToolStripContainer1.TopToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer1.TopToolStripPanel.PerformLayout()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewsoftvDataSet1 As sofTV.NewsoftvDataSet1
    Friend WithEvents CONVENDEDORESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONVENDEDORESTableAdapter As sofTV.NewsoftvDataSet1TableAdapters.CONVENDEDORESTableAdapter
    Friend WithEvents CONVENDEDORESBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONVENDEDORESBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_VendedorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DomicilioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ActivoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents BottomToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents TopToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents RightToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents LeftToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ContentPanel As System.Windows.Forms.ToolStripContentPanel
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents FechaSalidaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaIngresoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxPasaporte As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxLogin As System.Windows.Forms.TextBox
    Friend WithEvents cbCobroADomicilio As System.Windows.Forms.CheckBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents btnDocumentos As System.Windows.Forms.Button
    Friend WithEvents cbCapacitacion As System.Windows.Forms.CheckBox
    Friend WithEvents cbRecuperador As System.Windows.Forms.CheckBox
    Friend WithEvents cbTecnico As System.Windows.Forms.ComboBox
End Class
