Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic

Imports NPOI.HSSF.UserModel
Imports NPOI.SS.UserModel
Imports NPOI.SS.Util
Imports NPOI.HSSF.Util
Imports NPOI.POIFS.FileSystem
Imports NPOI.HPSF
Imports NPOI.XSSF.UserModel
Imports NPOI.XSSF.Util
Imports System.Text
Imports System.IO


Public Class FrmCarterasPorPlaza
    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Titulo As String = Nothing

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            End If
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            If GloClv_TipSer > 0 And GLOClv_Cartera > 0 Then
                customersByCityReport = New ReportDocument
                Dim connectionInfo As New ConnectionInfo
                '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
                '    "=True;User ID=DeSistema;Password=1975huli")

                'connectionInfo.ServerName = GloServerName
                'connectionInfo.DatabaseName = GloDatabaseName
                'connectionInfo.UserID = GloUserID
                'connectionInfo.Password = GloPassword

                Dim mySelectFormula As String = Titulo
                Dim ciudades1 As String = Nothing
                ciudades1 = " Ciudad(es): " + LocCiudades

                'Dim OpOrdenar As String = "0"
                'If Me.RadioButton1.Checked = True Then
                '    OpOrdenar = "0"
                'ElseIf Me.RadioButton2.Checked = True Then
                '    OpOrdenar = "1"
                'ElseIf Me.RadioButton3.Checked = True Then
                '    OpOrdenar = "2"
                'End If

                Dim reportPath As String = Nothing
                If Me.ComboPeriodo.SelectedValue = 0 Then
                    Select Case op
                        Case 4
                            reportPath = RutaReportes + "\ValorResumenCarterasrespaldo.rpt"
                        Case Else
                            If GloClv_TipSer <> 3 Then
                                reportPath = RutaReportes + "\Carteras.rpt"
                            Else
                                If IdSistema = "SA" Or IdSistema = "VA" Then
                                    reportPath = RutaReportes + "\Carteras.rpt"
                                Else
                                    reportPath = RutaReportes + "\CarterasDig.rpt"
                                End If

                            End If
                    End Select
                Else
                    Select Case op
                        Case 4
                            reportPath = RutaReportes + "\ValorResumenCarterasrespaldo.rpt"
                        Case Else
                            If GloClv_TipSer <> 3 Then
                                reportPath = RutaReportes + "\CarterasPorPeriodo_02.rpt"
                            Else
                                If IdSistema = "SA" Or IdSistema = "VA" Then
                                    reportPath = RutaReportes + "\CarterasPorPeriodo_02.rpt"
                                Else
                                    reportPath = RutaReportes + "\CarteraDig_PorPeriodo.rpt"
                                End If

                            End If
                    End Select
                End If

                Dim DS As New DataSet
                DS.Clear()

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.BigInt, GLOClv_Cartera)
                BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, GloClv_TipSer)

                Dim listatablas As New List(Of String)


                'DS = BaseII.ConsultaDS("ReporteCarteraPorPeriodo", listatablas)

                'customersByCityReport.Load(reportPath)
                'SetDBReport(DS, customersByCityReport)



                Dim loc_tipser As Integer = 1
                If IsNumeric(Me.ComboBox1.SelectedValue) = True Then loc_tipser = Me.ComboBox1.SelectedValue
                If Me.ComboPeriodo.SelectedValue = 0 Then
                    Select Case op
                        Case 4
                            Select Case loc_tipser
                                Case 1
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                                Case 2
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                                Case 3
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                            End Select

                        Case Else

                            Select Case loc_tipser
                                Case 1
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text
                                Case 2
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text
                                Case 3
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text
                            End Select

                    End Select
                Else
                    Select Case op
                        Case 4
                            Select Case loc_tipser
                                Case 1
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                                Case 2
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                                Case 3
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                            End Select

                        Case Else

                            Select Case loc_tipser
                                Case 1
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text & " del " & Me.ComboPeriodo.Text
                                Case 2
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text & " del " & Me.ComboPeriodo.Text
                                Case 3
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text & " del " & Me.ComboPeriodo.Text
                            End Select

                    End Select
                End If

                'Select Case op
                '    Case 4, 6
                'customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                '    Case Else
                'customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                'End Select
                If Me.ComboPeriodo.SelectedValue = 0 Then
                    Select Case op
                        Case 4
                            listatablas.Add("SeleccionaCartera")
                            listatablas.Add("CatalogoConceptosCartera")
                            listatablas.Add("DetCartera")
                            listatablas.Add("Servicios")
                            listatablas.Add("TipServ")
                            listatablas.Add("VistaCatalogoConceptosCartera")
                            DS = BaseII.ConsultaDS("SeleccionaCartera", listatablas)
                        Case Else
                            If GloClv_TipSer <> 3 Then
                                listatablas.Add("ReporteCartera")
                                listatablas.Add("Servicios")
                                DS = BaseII.ConsultaDS("ReporteCartera", listatablas)
                            Else
                                If IdSistema = "SA" Or IdSistema = "VA" Then
                                    listatablas.Add("ReporteCartera")
                                    listatablas.Add("Servicios")
                                    DS = BaseII.ConsultaDS("ReporteCartera", listatablas)
                                Else
                                    listatablas.Add("SeleccionaCartera")
                                    listatablas.Add("CatalogoConceptosCartera")
                                    listatablas.Add("DetCartera")
                                    listatablas.Add("Servicios")
                                    listatablas.Add("TipServ")
                                    listatablas.Add("VistaCatalogoConceptosCartera")
                                    DS = BaseII.ConsultaDS("SeleccionaCartera", listatablas)
                                End If

                            End If
                    End Select
                Else
                    Select Case op
                        Case 4
                            listatablas.Add("SeleccionaCartera")
                            listatablas.Add("CatalogoConceptosCartera")
                            listatablas.Add("DetCartera")
                            listatablas.Add("Servicios")
                            listatablas.Add("TipServ")
                            listatablas.Add("VistaCatalogoConceptosCartera")
                            DS = BaseII.ConsultaDS("SeleccionaCartera", listatablas)
                        Case Else
                            If GloClv_TipSer <> 3 Then
                                listatablas.Add("ReporteCarteraPorPeriodo")
                                listatablas.Add("Servicios")
                                DS = BaseII.ConsultaDS("ReporteCarteraPorPeriodo", listatablas)
                            Else
                                If IdSistema = "SA" Or IdSistema = "VA" Then
                                    listatablas.Add("ReporteCarteraPorPeriodo")
                                    listatablas.Add("Servicios")
                                    DS = BaseII.ConsultaDS("ReporteCarteraPorPeriodo", listatablas)
                                Else
                                    listatablas.Add("CatalogoConceptosCartera_PorPeriodo")
                                    listatablas.Add("DetCartera_PorPeriodo")
                                    listatablas.Add("PorPeriodo_SeleccionaCartera")
                                    listatablas.Add("Servicios")
                                    DS = BaseII.ConsultaDS("PorPeriodo_SeleccionaCartera", listatablas)

                                End If

                            End If
                    End Select
                End If
                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
                customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & ciudades1 & "'"
                'customersByCityReport.DataDefinition.FormulaFields(3).Text = "'" & op & "'"

                '--SetDBLogonForReport(connectionInfo)
                CrystalReportViewer1.ShowGroupTreeButton = False
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = customersByCityReport
                SetDBLogonForReport2(connectionInfo)
                customersByCityReport = Nothing
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmCarteras_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)

        Dim resp As MsgBoxResult = MsgBoxResult.Yes

        If execar = True Then
            execar = False
            If op = 0 Then
                Genera_Cartera(LocClv_session, GloClv_TipSer, 0, GloUsuario)
                ReporteCartera()
                EntradasSelCiudad = 0
            ElseIf op = 1 Then
                ReporteCartera()
            Else
                GENERAMICARTERA()
            End If



            '    execar = False
            '    If Me.ComboPeriodo.SelectedValue = 0 Then

            '        If op <> 4 Then
            '            ReporteCartera()
            '        Else
            '            GENERAMICARTERA()
            '        End If
            '    Else
            '        GENERAMICARTERA_PorPeriodo()
            '    End If
            'ElseIf execar = False Then
            '    'borra la session de las tablas
            '    'If IdSistema = "AG" Then
            '    '    If glosessioncar > 0 Then
            '    '        glosessioncar = 0
            '    '        Me.Borra_sessionCarteraTableAdapter.Connection = CON
            '    '        Me.Borra_sessionCarteraTableAdapter.Fill(Me.Procedimientosarnoldo4.Borra_sessionCartera, glosessioncar)
            '    '    End If
            '    'End If
        End If

        'If GloBndGenCartera = True Then
        '    GloBndGenCartera = False
        '    ConfigureCrystalReports(op, "")



        If GloBndGenCartera_Pregunta = True Then
            GloBndGenCartera_Pregunta = False
            resp = MsgBox("�Deseas guardar la Cartera?", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.No Then
                'CON.Open()
                'Me.BORRACARTERASTableAdapter.Connection = CON
                'Me.BORRACARTERASTableAdapter.Fill(Me.DataSetEDGAR.BORRACARTERAS, GLOClv_Cartera, GloClv_TipSer)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Cartera", SqlDbType.Int, GLOClv_Cartera)
                BaseII.CreateMyParameter("@Clv_Tipser", SqlDbType.Int, GloClv_TipSer)
                BaseII.Inserta("BORRACARTERASPorPlaza")
                'CON.Close()
            End If
        End If





        'End If

    End Sub

    Private Sub FrmCarteras_CausesValidationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.CausesValidationChanged

    End Sub

    Private Sub FrmCarteras_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GloClv_TipSer = 0
    End Sub


    Private Sub FrmCarteras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEdgarRev2.PorPerido_Seleccione' Puede moverla o quitarla seg�n sea necesario.

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        colorea(Me, Me.Name)
        Llena_companias()

        Me.Text = "Estados De Cartera Por Distribuidor"
        If IsNumeric(ComboBox1.SelectedValue) = True Then
            GloClv_TipSer = Me.ComboBox1.SelectedValue
        Else
            GloClv_TipSer = glotiposervicioppal
        End If
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEDGAR.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Try
            'GloClv_TipSer = 1
            Me.PorPerido_SeleccioneTableAdapter.Connection = CON
            Me.PorPerido_SeleccioneTableAdapter.Fill(Me.DataSetEdgarRev2.PorPerido_Seleccione)
            Me.ComboPeriodo.SelectedIndex = 0
            gloPorClv_Periodo = 0
            Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
            Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.DataSetEDGAR.MuestraTipSerPrincipal)
            If IsNumeric(ComboBox1.SelectedValue) = True Then
                Me.Rep_CarterasTableAdapter.Connection = CON
                Me.Rep_CarterasTableAdapter.Fill(Me.DataSetEDGAR.Rep_Carteras, Me.ComboBox1.SelectedValue, 0)
                'ConfigureCrystalReports(op, "")
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
        'If IdSistema = "AG" Or IdSistema = "VA" Then
        '    Me.Label1.Visible = True
        '    Me.ComboPeriodo.Visible = True
        'End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try

            If IsNumeric(ComboBox1.SelectedValue) = True Then
                GloClv_TipSer = Me.ComboBox1.SelectedValue
                Me.Rep_CarterasTableAdapter.Connection = CON
                Me.Rep_CarterasTableAdapter.Fill(Me.DataSetEDGAR.Rep_Carteras, Me.ComboBox1.SelectedValue, 0)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub Rep_CarterasDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Rep_CarterasDataGridView.CellClick
        'If Me.ComboBoxCompanias.SelectedIndex <= 0 Then
        '    MsgBox("Seleccione la Compa��a", MsgBoxStyle.Information)
        '    Exit Sub
        'End If
        varfrmselcompania = "cartera"
        GloOpFiltrosXML = "CarteraDistribuidor"
        If Me.Rep_CarterasDataGridView.SelectedCells(0).Value = 0 Then
            op = 0
            'FrmGeneraCarteras.Show()
            GlovienedeCartera = 1
            LocClv_session = DAMESclv_Sessionporfavor()
            'FrmSelCd_CarteraPorPlaza.Show()
            FrmSelPlazaXML.Show()
            'FrmSelCompaniaCartera.Show()
        ElseIf Me.Rep_CarterasDataGridView.SelectedCells(0).Value = 1 Then
            op = 1
            'FrmSelCuidadesCartera.Show()
            BrwCarterasPorPlaza.Show()
        ElseIf Me.Rep_CarterasDataGridView.SelectedCells(0).Value = 4 Then
            op = 4
            FrmSelCuidadesCartera.Show()
            'BrwCarteras.Show()
        End If
    End Sub

    Private Sub Rep_CarterasDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Rep_CarterasDataGridView.CellContentClick

    End Sub

    Private Sub GENERAMICARTERA()
        Dim comando As SqlClient.SqlCommand
        Dim resp As MsgBoxResult = MsgBoxResult.Yes
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GloClv_TipSer <> 3 Then
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Genera_Cartera_Por_Periodo"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                    Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                    Dim prm5 As New SqlParameter("@idcompania", SqlDbType.Int)
                    prm.Direction = ParameterDirection.Output
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm5.Direction = ParameterDirection.Input
                    prm.Value = 0
                    prm1.Value = glosessioncar
                    prm2.Value = GloClv_TipSer
                    prm3.Value = 0
                    prm4.Value = GloUsuario
                    prm5.Value = identificador


                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    GLOClv_Cartera = prm.Value
                End With
            Else
                If IdSistema = "SA" Or IdSistema = "VA" Then
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_Cartera "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario

                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                Else
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_CarteraDIG "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0

                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session2", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        Dim prm5 As New SqlParameter("@idcompania", SqlDbType.Int)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm5.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario
                        prm5.Value = identificador
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        .Parameters.Add(prm5)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                End If
            End If
            Me.Dame_ciudad_carteraTableAdapter.Connection = CON
            Me.Dame_ciudad_carteraTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_ciudad_cartera, glosessioncar, LocCiudades)
            ConfigureCrystalReports(op, "")
            'If IdSistema = "AG" Then
            '    Me.Borra_sessionCarteraTableAdapter.Connection = CON
            '    Me.Borra_sessionCarteraTableAdapter.Fill(Me.Procedimientosarnoldo4.Borra_sessionCartera, glosessioncar)
            'End If
            resp = MsgBox("Deseas Guardar la Cartera ", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.No Then
                Me.BORRACARTERASTableAdapter.Connection = CON
                Me.BORRACARTERASTableAdapter.Fill(Me.DataSetEDGAR.BORRACARTERAS, GLOClv_Cartera, GloClv_TipSer)
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GENERAMICARTERA_PorPeriodo()
        Dim comando As SqlClient.SqlCommand
        Dim resp As MsgBoxResult = MsgBoxResult.Yes
        Try

            If Me.ComboPeriodo.SelectedValue >= 1 Then
                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                Dim comando2 As SqlClient.SqlCommand
                comando2 = New SqlClient.SqlCommand
                With comando2
                    .Connection = CON2
                    .CommandText = "PorPeriodo_GrabaPeriodoTmp "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_Periodo", SqlDbType.Int)


                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input

                    prm1.Value = glosessioncar
                    prm2.Value = Me.ComboPeriodo.SelectedValue


                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    Dim i As Integer = comando2.ExecuteNonQuery()
                End With
                CON2.Close()
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If GloClv_TipSer <> 3 Then
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Genera_Cartera_Por_Periodo "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                    Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                    prm.Direction = ParameterDirection.Output
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm.Value = 0
                    prm1.Value = glosessioncar
                    prm2.Value = GloClv_TipSer
                    prm3.Value = 0
                    prm4.Value = GloUsuario

                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    GLOClv_Cartera = prm.Value
                End With
            Else
                If IdSistema = "SA" Then
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_Cartera_Por_Periodo "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario

                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                Else
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_CarteraDIG_PorPeriodo "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0

                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session2", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        Dim prm5 As New SqlParameter("@idcompania", SqlDbType.Int)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm5.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario
                        prm5.Value = identificador
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        .Parameters.Add(prm5)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                End If
            End If
            Me.Dame_ciudad_carteraTableAdapter.Connection = CON
            Me.Dame_ciudad_carteraTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_ciudad_cartera, glosessioncar, LocCiudades)
            ConfigureCrystalReports(op, "")
            'If IdSistema = "AG" Then
            '    Me.Borra_sessionCarteraTableAdapter.Connection = CON
            '    Me.Borra_sessionCarteraTableAdapter.Fill(Me.Procedimientosarnoldo4.Borra_sessionCartera, glosessioncar)
            'End If
            CON.Close()
            resp = MsgBox("Deseas Guardar la Cartera ", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.No Then
                'Me.BORRACARTERASTableAdapter.Connection = CON
                'Me.BORRACARTERASTableAdapter.Fill(Me.DataSetEDGAR.BORRACARTERAS, GLOClv_Cartera, GloClv_TipSer)
                Dim CON3 As New SqlConnection(MiConexion)
                CON3.Open()
                Dim comando3 As SqlClient.SqlCommand
                comando3 = New SqlClient.SqlCommand
                With comando3
                    .Connection = CON3
                    .CommandText = "PorPeriodo_BORRACARTERAS"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm1 As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_Tipser", SqlDbType.Int)


                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input

                    prm1.Value = GLOClv_Cartera
                    prm2.Value = GloClv_TipSer


                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    Dim i As Integer = comando3.ExecuteNonQuery()
                End With
                CON3.Close()
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub ComboPeriodo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboPeriodo.SelectedIndexChanged
        If IsNumeric(Me.ComboPeriodo.SelectedValue) = True Then
            gloPorClv_Periodo = Me.ComboPeriodo.SelectedValue
        Else
            gloPorClv_Periodo = 0
        End If
    End Sub

    Private Sub Genera_Cartera(ByVal CLV_SESSION As Integer, ByVal CLV_TIPSER As Integer, ByVal TIPOCARTERA As Integer, ByVal CLV_USUARIO As String)

        Dim comando As SqlClient.SqlCommand
        Dim resp As MsgBoxResult = MsgBoxResult.Yes
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If CLV_TIPSER = 3 Then
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Genera_CarteraDIG_PorPeriodoPorPlaza "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    ' Create a SqlParameter for each parameter in the stored procedure.

                    Dim prm1 As New SqlParameter("@clv_session2", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                    Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                    Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm5 As New SqlParameter("@idcompania", SqlDbType.Int)
                    Dim prm6 As New SqlParameter("@CompaniasXml", SqlDbType.Xml)
                    Dim prm7 As New SqlParameter("@CiudadesXml", SqlDbType.Xml)
                    Dim prm8 As New SqlParameter("@LocalidadesXml", SqlDbType.Xml)
                    Dim prm9 As New SqlParameter("@PeriodosXml", SqlDbType.Xml)
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm.Direction = ParameterDirection.Output
                    prm5.Direction = ParameterDirection.Input
                    prm6.Direction = ParameterDirection.Input
                    prm7.Direction = ParameterDirection.Input
                    prm8.Direction = ParameterDirection.Input
                    prm9.Direction = ParameterDirection.Input
                    prm1.Value = LocClv_session
                    prm2.Value = GloClv_TipSer
                    prm3.Value = 0
                    prm4.Value = GloUsuario
                    prm5.Value = identificador
                    prm.Value = 1
                    prm6.Value = DocCompanias.InnerXml
                    prm7.Value = "" 'DocCiudades.InnerXml
                    prm8.Value = "" 'DocLocalidades.InnerXml
                    prm9.Value = "" 'DocPeriodos.InnerXml
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    GLOClv_Cartera = prm.Value
                End With
                GloBndGenCartera_Pregunta = True

            Else

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
                BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
                BaseII.CreateMyParameter("@TIPOCARTERA", SqlDbType.Int, TIPOCARTERA)
                BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, CLV_USUARIO, 150)
                BaseII.CreateMyParameter("@CLV_CARTERA", System.Data.ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, identificador)
                BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
                BaseII.CreateMyParameter("@LocalidadesXml", SqlDbType.Xml, DocLocalidades.InnerXml)
                BaseII.CreateMyParameter("@CiudadesXml", SqlDbType.Xml, DocCiudades.InnerXml)
                BaseII.CreateMyParameter("@PeriodosXml", SqlDbType.Xml, DocPeriodos.InnerXml)
                BaseII.ProcedimientoOutPut("Genera_Cartera_Por_PeriodoPorPlaza")
                GLOClv_Cartera = CInt(BaseII.dicoPar("@CLV_CARTERA").ToString)
                GloBndGenCartera_Pregunta = True

            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Function SELECCIONACARTERA(ByVal CLV_CARTERA As Integer, ByVal CLV_TIPSER As Integer, ByVal SUCURSAL As String) As DataSet
        'Dim tableNameList As New List(Of String)
        'tableNameList.Add("CatalogoConceptosCartera")
        'tableNameList.Add("DetCartera")
        'tableNameList.Add("SeleccionaCartera")
        'tableNameList.Add("Servicios")
        'tableNameList.Add("Encabezados")
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, CLV_CARTERA)
        'BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
        'BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.VarChar, SUCURSAL, 150)
        'BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        'Return BaseII.ConsultaDS("SELECCIONACARTERA", tableNameList)
        Dim tableNameList As New List(Of String)
        tableNameList.Add("CatalogoConceptosCartera")
        tableNameList.Add("DetCartera")
        'tableNameList.Add("Encabezados")
        tableNameList.Add("SeleccionaCartera;1")
        tableNameList.Add("Servicios")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, CLV_CARTERA)
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.VarChar, SUCURSAL, 150)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        Return BaseII.ConsultaDS("SELECCIONACARTERAPorPlaza", tableNameList)
    End Function

    Private Function SELECCIONACARTERADIG(ByVal CLV_CARTERA As Integer, ByVal CLV_TIPSER As Integer, ByVal SUCURSAL As String) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("CatalogoConceptosCartera")
        tableNameList.Add("DetCartera")
        'tableNameList.Add("Encabezados")
        tableNameList.Add("SeleccionaCartera;1")
        tableNameList.Add("Servicios")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, CLV_CARTERA)
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.VarChar, SUCURSAL, 150)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        Return BaseII.ConsultaDS("SELECCIONACARTERAPorPlaza", tableNameList)
    End Function

    Private Sub ReporteCartera()
        Try
            If GloClv_TipSer = 3 Then
                customersByCityReport = New ReportDocument
                Dim connectionInfo As New ConnectionInfo
                Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
                Dim dSet As New DataSet
                dSet = SELECCIONACARTERADIG(GLOClv_Cartera, GloClv_TipSer, GloSucursal)
                rDocument.Load(RutaReportes + "\CarterasDigPorPlaza.rpt")
                rDocument.SetDataSource(dSet)
                Dim subtitutlo As String = "Estado de Cartera de TV Digital"
                'inicio nuevo titulo
                Dim con As New SqlConnection(MiConexion)
                con.Open()
                Dim comando As New SqlCommand()
                comando.Connection = con
                comando.CommandText = "exec DamePlazasTituloReporteCarteraPorPlaza " + Str(GLOClv_Cartera)
                Dim companias As String = "Distribuidor: "
                Dim reader As SqlDataReader = comando.ExecuteReader()
                Dim contador As Integer = 0
                While reader.Read()
                    If contador = 0 Then
                        companias = companias + reader(0).ToString
                        contador = contador + 1
                    Else
                        companias = companias + ", " + reader(0).ToString
                    End If
                End While
                reader.Close()
                comando.Dispose()

                con.Close()
                'Fin T�tulo nuevo
                rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
                rDocument.DataDefinition.FormulaFields("Sucursal").Text = "'" & subtitutlo & "'"
                rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & companias & "'"
                CrystalReportViewer1.ShowGroupTreeButton = False
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = rDocument
                rDocument = Nothing
            Else

                Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
                Dim dSet As New DataSet
                dSet = SELECCIONACARTERA(GLOClv_Cartera, GloClv_TipSer, GloSucursal)
                rDocument.Load(RutaReportes + "\CarterasIntPorPlaza.rpt")
                rDocument.SetDataSource(dSet)
                Dim subtitutlo As String = "Estado de Cartera de Internet"
                'inicio nuevo titulo
                Dim con As New SqlConnection(MiConexion)
                con.Open()
                Dim comando As New SqlCommand()
                comando.Connection = con
                comando.CommandText = "exec DamePlazasTituloReporteCarteraPorPlaza " + Str(GLOClv_Cartera)
                Dim companias As String = "Distribuidor: "
                Dim reader As SqlDataReader = comando.ExecuteReader()
                Dim contador As Integer = 0
                While reader.Read()
                    If contador = 0 Then
                        companias = companias + reader(0).ToString
                        contador = contador + 1
                    Else
                        companias = companias + ", " + reader(0).ToString
                    End If
                End While
                reader.Close()
                comando.Dispose()

                con.Close()
                'Fin T�tulo nuevo
                rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
                rDocument.DataDefinition.FormulaFields("Sucursal").Text = "'" & subtitutlo & "'"
                rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & companias & "'"
                CrystalReportViewer1.ShowGroupTreeButton = False
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = rDocument
                rDocument = Nothing

            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Dim locclv_cartera As Integer
            Dim locCompaniaCartera As String
            Dim rutaFile As String = ""
            If GLOClv_Cartera = 0 Then
                Exit Sub
            End If
            'se trae la coleccion de carteras de la cartera maestra
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, GLOClv_Cartera)
            Dim dtCarteras = BaseII.ConsultaDT("SELECCIONACARTERASCarteraMaestra")

            Dim wb As New XSSFWorkbook()
            Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()
            If (result <> DialogResult.OK) Then
                Exit Sub
            End If

            rutaFile = Me.FolderBrowserDialog1.SelectedPath.ToString
            If File.Exists(rutaFile + "\CarteraPorPlaza_" + Str(GLOClv_Cartera) + ".xlsx") Then
                File.Delete(rutaFile + "\CarteraPorPlaza_" + Str(GLOClv_Cartera) + ".xlsx")
            End If
            Dim fileOut = New FileStream(rutaFile + "\CarteraPorPlaza_" + Str(GLOClv_Cartera) + ".xlsx", FileMode.Create, FileAccess.ReadWrite)

            For Each rowCartera As DataRow In dtCarteras.Rows
                locclv_cartera = CInt(rowCartera(0).ToString)
                locCompaniaCartera = rowCartera(1).ToString

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, locclv_cartera)
                Dim dtServicios = BaseII.ConsultaDT("SELECCIONACARTERAServiciosExcel")
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, locclv_cartera)
                Dim dt = BaseII.ConsultaDT("SELECCIONACARTERAExcel")


                Dim sheet1 As ISheet = wb.CreateSheet(locCompaniaCartera + "-" + Str(locclv_cartera))



                Dim rowNew As IRow = sheet1.CreateRow(0)
                Dim c As Integer = 1
                rowNew.CreateCell(0).SetCellValue("")

                For Each row As DataRow In dtServicios.Rows
                    If row(1).GetType().ToString = "System.Int32" Then
                        rowNew.CreateCell(c).SetCellValue(CInt(row(1).ToString))
                    ElseIf row(1).GetType().ToString = "System.Int16" Then
                        rowNew.CreateCell(c).SetCellValue(CInt(row(1).ToString))
                    ElseIf row(1).GetType().ToString = "System.Decimal" Then
                        rowNew.CreateCell(c).SetCellValue(CDbl(row(1).ToString))
                    Else
                        rowNew.CreateCell(c).SetCellValue(row(1).ToString)
                    End If

                    c = c + 2
                Next

                Dim f As Integer = 1
                c = 0
                Dim aux As Integer = 0
                Dim rowNew2 As IRow

                For Each row As DataRow In dt.Rows
                    If aux <> CInt(row(1).ToString) Then
                        c = 0
                        aux = CInt(row(1).ToString)
                        rowNew2 = sheet1.CreateRow(f)
                        rowNew2.CreateCell(c).SetCellValue(row(0).ToString)
                        f = f + 1
                        c = c + 1
                    End If
                    rowNew2.CreateCell(c).SetCellValue(row(4).ToString)
                    c = c + 1
                    rowNew2.CreateCell(c).SetCellValue(row(5).ToString)
                    c = c + 1
                Next

                sheet1.AutoSizeColumn(0)
            Next

            wb.Write(fileOut)
            fileOut.Close()
            MsgBox("Archivo generado exitosamente.")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
End Class