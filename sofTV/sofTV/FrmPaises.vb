Imports System.Data.SqlClient
Public Class FrmPaises
    Private error1 As Integer = Nothing
    'Public KeyAscii As Short
    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub validacion(ByVal tel As String)
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            con.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "Validacion_paises"
                .CommandTimeout = 0
                .Connection = con
                .CommandType = CommandType.StoredProcedure

                Dim prm0 As New SqlParameter("@clv_pais", SqlDbType.Int)
                Dim prm As New SqlParameter("@clv_lada", SqlDbType.VarChar, 25)
                Dim prm1 As New SqlParameter("@error", SqlDbType.Int)

                prm0.Direction = ParameterDirection.Input
                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Output

                prm0.Value = CLng(Me.Clv_CiudadTextBox.Text)
                prm.Value = tel
                prm1.Value = 0

                .Parameters.Add(prm0)
                .Parameters.Add(prm)
                .Parameters.Add(prm1)

                Dim i As Integer = cmd.ExecuteNonQuery()
                error1 = prm1.Value
            End With
            con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Consulta_PaisesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_PaisesBindingNavigatorSaveItem.Click
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        If Me.TextBox2.Text = "" Then
            Me.TextBox2.Text = 0
        Else
            Me.TextBox2.Text = CDec(Me.TextBox2.Text)
        End If
        If Me.NombreTextBox.Text = "" Then
            MsgBox("Es Necesario Capturar un Nombre del Pais ", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox1.Text = "" Then
            MsgBox("Es Necesario Capturar la Lada del Pais", MsgBoxStyle.Information)
            Exit Sub

        Else

            validacion(Me.TextBox1.Text)

            If error1 = 0 Then
                Me.Validate()
                Me.Consulta_PaisesBindingSource.EndEdit()
                cone.Open()
                Me.Consulta_PaisesTableAdapter.Connection = cone
                Me.Consulta_PaisesTableAdapter.Update(Me.DataSetLidia2.Consulta_Paises)
                cone.Close()
                MsgBox("Se Ha Guardado con �xito ", MsgBoxStyle.Information)
                bec_bnd = True
                If opcion = "N" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Bot�n Guardar", "Se Agreg� Pais", "clave del Pais :" + CStr(Me.Clv_CiudadTextBox.Text), GloCiudad)
                ElseIf opcion = "M" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Bot�n Guardar", "Se Modific� Pais", "clave del Pais :" + CStr(Me.Clv_CiudadTextBox.Text), GloCiudad)
                End If
                Me.Close()
            ElseIf error1 = 1 Then
                MsgBox("Esa Lada Ya esta Asignada A Otro Pais", MsgBoxStyle.Information)
            End If

        End If
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Me.Consulta_PaisesTableAdapter.Delete(Me.Clv_CiudadTextBox.Text)
        bec_bnd = True
        MsgBox("Se Ha Borrado con �xito", MsgBoxStyle.Information)
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Bot�n Eliminar", "Se elimin� Pais", "clave del Pais :" + CStr(Me.Clv_CiudadTextBox.Text), GloCiudad)
        Me.Close()
    End Sub

    Private Sub FrmPaises_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim conLidia As New SqlClient.SqlConnection(MiConexion)
        If opcion = "N" Then
            Me.Consulta_PaisesBindingSource.AddNew()
            Me.BindingNavigatorDeleteItem.Enabled = False
        ElseIf opcion = "M" Then
            conLidia.Open()
            Me.Consulta_PaisesTableAdapter.Connection = conLidia
            Me.Consulta_PaisesTableAdapter.Fill(Me.DataSetLidia2.Consulta_Paises, Clv_Pais)
            conLidia.Close()
        ElseIf opcion = "C" Then
            Me.Panel1.Enabled = False
            Me.Consulta_PaisesBindingNavigator.Enabled = False
            conLidia.Open()
            Me.Consulta_PaisesTableAdapter.Connection = conLidia
            Me.Consulta_PaisesTableAdapter.Fill(Me.DataSetLidia2.Consulta_Paises, Clv_Pais)
            conLidia.Close()
        End If
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub
End Class