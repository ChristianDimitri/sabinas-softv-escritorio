﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmIdentificacionUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmIdentificacionUsuario))
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Me.CONVENDEDORESBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONVENDEDORESBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.tbClave = New System.Windows.Forms.TextBox()
        Me.ckAgregar = New System.Windows.Forms.CheckBox()
        Me.Button5 = New System.Windows.Forms.Button()
        NombreLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.CONVENDEDORESBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONVENDEDORESBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'CONVENDEDORESBindingNavigator
        '
        Me.CONVENDEDORESBindingNavigator.AddNewItem = Nothing
        Me.CONVENDEDORESBindingNavigator.CountItem = Nothing
        Me.CONVENDEDORESBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONVENDEDORESBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONVENDEDORESBindingNavigatorSaveItem})
        Me.CONVENDEDORESBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONVENDEDORESBindingNavigator.MoveFirstItem = Nothing
        Me.CONVENDEDORESBindingNavigator.MoveLastItem = Nothing
        Me.CONVENDEDORESBindingNavigator.MoveNextItem = Nothing
        Me.CONVENDEDORESBindingNavigator.MovePreviousItem = Nothing
        Me.CONVENDEDORESBindingNavigator.Name = "CONVENDEDORESBindingNavigator"
        Me.CONVENDEDORESBindingNavigator.PositionItem = Nothing
        Me.CONVENDEDORESBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.CONVENDEDORESBindingNavigator.Size = New System.Drawing.Size(517, 25)
        Me.CONVENDEDORESBindingNavigator.TabIndex = 161
        Me.CONVENDEDORESBindingNavigator.TabStop = True
        Me.CONVENDEDORESBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONVENDEDORESBindingNavigatorSaveItem
        '
        Me.CONVENDEDORESBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CONVENDEDORESBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONVENDEDORESBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONVENDEDORESBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONVENDEDORESBindingNavigatorSaveItem.Name = "CONVENDEDORESBindingNavigatorSaveItem"
        Me.CONVENDEDORESBindingNavigatorSaveItem.Size = New System.Drawing.Size(89, 22)
        Me.CONVENDEDORESBindingNavigatorSaveItem.Text = "&Guardar"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(65, 76)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(66, 15)
        NombreLabel.TabIndex = 163
        NombreLabel.Text = "Nombre :"
        '
        'tbNombre
        '
        Me.tbNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombre.Location = New System.Drawing.Point(137, 74)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(286, 21)
        Me.tbNombre.TabIndex = 162
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(65, 49)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(50, 15)
        Label1.TabIndex = 165
        Label1.Text = "Clave :"
        '
        'tbClave
        '
        Me.tbClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbClave.Enabled = False
        Me.tbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbClave.Location = New System.Drawing.Point(137, 47)
        Me.tbClave.Name = "tbClave"
        Me.tbClave.Size = New System.Drawing.Size(114, 21)
        Me.tbClave.TabIndex = 164
        '
        'ckAgregar
        '
        Me.ckAgregar.AutoSize = True
        Me.ckAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ckAgregar.ForeColor = System.Drawing.Color.LightSlateGray
        Me.ckAgregar.Location = New System.Drawing.Point(137, 101)
        Me.ckAgregar.Name = "ckAgregar"
        Me.ckAgregar.Size = New System.Drawing.Size(237, 19)
        Me.ckAgregar.TabIndex = 166
        Me.ckAgregar.Text = "Agregar plazas automáticamente"
        Me.ckAgregar.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(391, 126)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(114, 33)
        Me.Button5.TabIndex = 171
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'FrmIdentificacionUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(517, 171)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.ckAgregar)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.tbClave)
        Me.Controls.Add(NombreLabel)
        Me.Controls.Add(Me.tbNombre)
        Me.Controls.Add(Me.CONVENDEDORESBindingNavigator)
        Me.Name = "FrmIdentificacionUsuario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Identificación Usuario"
        CType(Me.CONVENDEDORESBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONVENDEDORESBindingNavigator.ResumeLayout(False)
        Me.CONVENDEDORESBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CONVENDEDORESBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONVENDEDORESBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents tbClave As System.Windows.Forms.TextBox
    Friend WithEvents ckAgregar As System.Windows.Forms.CheckBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
