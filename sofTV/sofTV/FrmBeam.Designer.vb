﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBeam
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmBeam))
        Dim Label1 As System.Windows.Forms.Label
        Me.CoberturaBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.tbClave = New System.Windows.Forms.TextBox()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.ckbZonaNorte = New System.Windows.Forms.CheckBox()
        DescripcionLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.CoberturaBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CoberturaBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(59, 88)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(62, 15)
        DescripcionLabel.TabIndex = 18
        DescripcionLabel.Text = "Nombre:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(75, 52)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(46, 15)
        Label2.TabIndex = 19
        Label2.Text = "Clave:"
        '
        'CoberturaBindingNavigator
        '
        Me.CoberturaBindingNavigator.AddNewItem = Nothing
        Me.CoberturaBindingNavigator.CountItem = Nothing
        Me.CoberturaBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CoberturaBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONSERVICIOSBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.CoberturaBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CoberturaBindingNavigator.MoveFirstItem = Nothing
        Me.CoberturaBindingNavigator.MoveLastItem = Nothing
        Me.CoberturaBindingNavigator.MoveNextItem = Nothing
        Me.CoberturaBindingNavigator.MovePreviousItem = Nothing
        Me.CoberturaBindingNavigator.Name = "CoberturaBindingNavigator"
        Me.CoberturaBindingNavigator.PositionItem = Nothing
        Me.CoberturaBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CoberturaBindingNavigator.Size = New System.Drawing.Size(484, 25)
        Me.CoberturaBindingNavigator.TabIndex = 13
        Me.CoberturaBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONSERVICIOSBindingNavigatorSaveItem
        '
        Me.CONSERVICIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSERVICIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSERVICIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Name = "CONSERVICIOSBindingNavigatorSaveItem"
        Me.CONSERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'tbClave
        '
        Me.tbClave.Enabled = False
        Me.tbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbClave.Location = New System.Drawing.Point(127, 47)
        Me.tbClave.Name = "tbClave"
        Me.tbClave.Size = New System.Drawing.Size(97, 21)
        Me.tbClave.TabIndex = 16
        '
        'tbNombre
        '
        Me.tbNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombre.Location = New System.Drawing.Point(127, 82)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(268, 21)
        Me.tbNombre.TabIndex = 17
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(39, 116)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(82, 15)
        Label1.TabIndex = 20
        Label1.Text = "Zona Norte:"
        '
        'ckbZonaNorte
        '
        Me.ckbZonaNorte.AutoSize = True
        Me.ckbZonaNorte.Location = New System.Drawing.Point(127, 119)
        Me.ckbZonaNorte.Name = "ckbZonaNorte"
        Me.ckbZonaNorte.Size = New System.Drawing.Size(15, 14)
        Me.ckbZonaNorte.TabIndex = 21
        Me.ckbZonaNorte.UseVisualStyleBackColor = True
        '
        'FrmBeam
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 148)
        Me.Controls.Add(Me.ckbZonaNorte)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Label2)
        Me.Controls.Add(DescripcionLabel)
        Me.Controls.Add(Me.tbNombre)
        Me.Controls.Add(Me.tbClave)
        Me.Controls.Add(Me.CoberturaBindingNavigator)
        Me.Name = "FrmBeam"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cobertura"
        CType(Me.CoberturaBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CoberturaBindingNavigator.ResumeLayout(False)
        Me.CoberturaBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CoberturaBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbClave As System.Windows.Forms.TextBox
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents ckbZonaNorte As System.Windows.Forms.CheckBox
End Class
