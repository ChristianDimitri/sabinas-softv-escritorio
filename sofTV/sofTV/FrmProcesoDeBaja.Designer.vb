<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmProcesoDeBaja
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButtonBuscar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ButtonInsertar = New System.Windows.Forms.Button()
        Me.ButtonEliminar = New System.Windows.Forms.Button()
        Me.TextBoxContrato = New System.Windows.Forms.TextBox()
        Me.TreeViewActivos = New System.Windows.Forms.TreeView()
        Me.TreeViewPreBajas = New System.Windows.Forms.TreeView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxSoloInternet = New System.Windows.Forms.CheckBox()
        Me.LabelCelular = New System.Windows.Forms.Label()
        Me.LabelTelefono = New System.Windows.Forms.Label()
        Me.LabelCiudad = New System.Windows.Forms.Label()
        Me.LabelColonia = New System.Windows.Forms.Label()
        Me.LabelNumero = New System.Windows.Forms.Label()
        Me.LabelCalle = New System.Windows.Forms.Label()
        Me.LabelNombre = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ComboBoxMotCan = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PanelMotCan = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.ComboBoxTipSer = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.PanelMotCan.SuspendLayout()
        Me.SuspendLayout()
        '
        'ButtonBuscar
        '
        Me.ButtonBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBuscar.Location = New System.Drawing.Point(605, 91)
        Me.ButtonBuscar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBuscar.Name = "ButtonBuscar"
        Me.ButtonBuscar.Size = New System.Drawing.Size(57, 28)
        Me.ButtonBuscar.TabIndex = 0
        Me.ButtonBuscar.Text = "..."
        Me.ButtonBuscar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(68, 25)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(133, 23)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Nombre: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'ButtonInsertar
        '
        Me.ButtonInsertar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonInsertar.Location = New System.Drawing.Point(632, 501)
        Me.ButtonInsertar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonInsertar.Name = "ButtonInsertar"
        Me.ButtonInsertar.Size = New System.Drawing.Size(81, 37)
        Me.ButtonInsertar.TabIndex = 2
        Me.ButtonInsertar.Text = ">"
        Me.ButtonInsertar.UseVisualStyleBackColor = True
        '
        'ButtonEliminar
        '
        Me.ButtonEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminar.Location = New System.Drawing.Point(632, 571)
        Me.ButtonEliminar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonEliminar.Name = "ButtonEliminar"
        Me.ButtonEliminar.Size = New System.Drawing.Size(81, 37)
        Me.ButtonEliminar.TabIndex = 3
        Me.ButtonEliminar.Text = "<"
        Me.ButtonEliminar.UseVisualStyleBackColor = True
        '
        'TextBoxContrato
        '
        Me.TextBoxContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxContrato.Location = New System.Drawing.Point(464, 94)
        Me.TextBoxContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxContrato.Name = "TextBoxContrato"
        Me.TextBoxContrato.Size = New System.Drawing.Size(132, 24)
        Me.TextBoxContrato.TabIndex = 4
        '
        'TreeViewActivos
        '
        Me.TreeViewActivos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewActivos.Location = New System.Drawing.Point(48, 389)
        Me.TreeViewActivos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeViewActivos.Name = "TreeViewActivos"
        Me.TreeViewActivos.Size = New System.Drawing.Size(575, 338)
        Me.TreeViewActivos.TabIndex = 5
        '
        'TreeViewPreBajas
        '
        Me.TreeViewPreBajas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewPreBajas.Location = New System.Drawing.Point(721, 389)
        Me.TreeViewPreBajas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeViewPreBajas.Name = "TreeViewPreBajas"
        Me.TreeViewPreBajas.Size = New System.Drawing.Size(575, 338)
        Me.TreeViewPreBajas.TabIndex = 6
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBoxSoloInternet)
        Me.GroupBox1.Controls.Add(Me.LabelCelular)
        Me.GroupBox1.Controls.Add(Me.LabelTelefono)
        Me.GroupBox1.Controls.Add(Me.LabelCiudad)
        Me.GroupBox1.Controls.Add(Me.LabelColonia)
        Me.GroupBox1.Controls.Add(Me.LabelNumero)
        Me.GroupBox1.Controls.Add(Me.LabelCalle)
        Me.GroupBox1.Controls.Add(Me.LabelNombre)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(264, 139)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(824, 214)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'CheckBoxSoloInternet
        '
        Me.CheckBoxSoloInternet.AutoSize = True
        Me.CheckBoxSoloInternet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxSoloInternet.Enabled = False
        Me.CheckBoxSoloInternet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxSoloInternet.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBoxSoloInternet.Location = New System.Drawing.Point(625, 25)
        Me.CheckBoxSoloInternet.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBoxSoloInternet.Name = "CheckBoxSoloInternet"
        Me.CheckBoxSoloInternet.Size = New System.Drawing.Size(126, 22)
        Me.CheckBoxSoloInternet.TabIndex = 16
        Me.CheckBoxSoloInternet.Text = "Sólo Internet"
        Me.CheckBoxSoloInternet.UseVisualStyleBackColor = True
        '
        'LabelCelular
        '
        Me.LabelCelular.Location = New System.Drawing.Point(200, 142)
        Me.LabelCelular.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelCelular.Name = "LabelCelular"
        Me.LabelCelular.Size = New System.Drawing.Size(400, 23)
        Me.LabelCelular.TabIndex = 15
        Me.LabelCelular.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelTelefono
        '
        Me.LabelTelefono.Location = New System.Drawing.Point(200, 165)
        Me.LabelTelefono.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelTelefono.Name = "LabelTelefono"
        Me.LabelTelefono.Size = New System.Drawing.Size(400, 23)
        Me.LabelTelefono.TabIndex = 14
        Me.LabelTelefono.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCiudad
        '
        Me.LabelCiudad.Location = New System.Drawing.Point(200, 118)
        Me.LabelCiudad.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelCiudad.Name = "LabelCiudad"
        Me.LabelCiudad.Size = New System.Drawing.Size(400, 23)
        Me.LabelCiudad.TabIndex = 13
        Me.LabelCiudad.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelColonia
        '
        Me.LabelColonia.Location = New System.Drawing.Point(200, 95)
        Me.LabelColonia.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelColonia.Name = "LabelColonia"
        Me.LabelColonia.Size = New System.Drawing.Size(400, 23)
        Me.LabelColonia.TabIndex = 12
        Me.LabelColonia.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNumero
        '
        Me.LabelNumero.Location = New System.Drawing.Point(200, 71)
        Me.LabelNumero.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelNumero.Name = "LabelNumero"
        Me.LabelNumero.Size = New System.Drawing.Size(400, 23)
        Me.LabelNumero.TabIndex = 11
        Me.LabelNumero.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCalle
        '
        Me.LabelCalle.Location = New System.Drawing.Point(200, 48)
        Me.LabelCalle.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelCalle.Name = "LabelCalle"
        Me.LabelCalle.Size = New System.Drawing.Size(400, 23)
        Me.LabelCalle.TabIndex = 10
        Me.LabelCalle.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNombre
        '
        Me.LabelNombre.Location = New System.Drawing.Point(200, 25)
        Me.LabelNombre.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelNombre.Name = "LabelNombre"
        Me.LabelNombre.Size = New System.Drawing.Size(400, 23)
        Me.LabelNombre.TabIndex = 9
        Me.LabelNombre.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label7.Location = New System.Drawing.Point(68, 142)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(133, 23)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Celular: "
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(68, 165)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(133, 23)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Teléfono: "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(68, 118)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(133, 23)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Ciudad: "
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(68, 95)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(133, 23)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Colonia: "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(68, 71)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(135, 23)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "#: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(68, 48)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(133, 23)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Calle: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(349, 91)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(107, 28)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Contrato:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ComboBoxMotCan
        '
        Me.ComboBoxMotCan.DisplayMember = "MotCan"
        Me.ComboBoxMotCan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxMotCan.FormattingEnabled = True
        Me.ComboBoxMotCan.Location = New System.Drawing.Point(89, 65)
        Me.ComboBoxMotCan.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxMotCan.Name = "ComboBoxMotCan"
        Me.ComboBoxMotCan.Size = New System.Drawing.Size(360, 28)
        Me.ComboBoxMotCan.TabIndex = 18
        Me.ComboBoxMotCan.ValueMember = "Clv_MotCan"
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(85, 34)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(349, 28)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Motivos de Cancelación"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(1157, 63)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 44)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'PanelMotCan
        '
        Me.PanelMotCan.Controls.Add(Me.Button4)
        Me.PanelMotCan.Controls.Add(Me.Button2)
        Me.PanelMotCan.Controls.Add(Me.ComboBoxMotCan)
        Me.PanelMotCan.Controls.Add(Me.Label8)
        Me.PanelMotCan.Location = New System.Drawing.Point(417, 454)
        Me.PanelMotCan.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PanelMotCan.Name = "PanelMotCan"
        Me.PanelMotCan.Size = New System.Drawing.Size(527, 204)
        Me.PanelMotCan.TabIndex = 28
        Me.PanelMotCan.Visible = False
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(303, 132)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(181, 44)
        Me.Button4.TabIndex = 30
        Me.Button4.Text = "&CANCELAR"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(52, 132)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 44)
        Me.Button2.TabIndex = 29
        Me.Button2.Text = "&ACEPTAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'ComboBoxTipSer
        '
        Me.ComboBoxTipSer.DisplayMember = "Concepto"
        Me.ComboBoxTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxTipSer.FormattingEnabled = True
        Me.ComboBoxTipSer.Location = New System.Drawing.Point(464, 57)
        Me.ComboBoxTipSer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxTipSer.Name = "ComboBoxTipSer"
        Me.ComboBoxTipSer.Size = New System.Drawing.Size(344, 28)
        Me.ComboBoxTipSer.TabIndex = 30
        Me.ComboBoxTipSer.ValueMember = "Clv_TipSer"
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(276, 57)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(180, 28)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Tipo de Servicio:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(44, 357)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(580, 28)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "Servicios Activos"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(717, 357)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(580, 28)
        Me.Label12.TabIndex = 32
        Me.Label12.Text = "Servicios a Baja"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(1157, 15)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(181, 44)
        Me.Button3.TabIndex = 33
        Me.Button3.Text = "&PROCESAR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'FrmProcesoDeBaja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1355, 811)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.ComboBoxTipSer)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.PanelMotCan)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TreeViewPreBajas)
        Me.Controls.Add(Me.TreeViewActivos)
        Me.Controls.Add(Me.TextBoxContrato)
        Me.Controls.Add(Me.ButtonEliminar)
        Me.Controls.Add(Me.ButtonInsertar)
        Me.Controls.Add(Me.ButtonBuscar)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmProcesoDeBaja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Proceso de Cancelación de Servicios"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.PanelMotCan.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButtonBuscar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ButtonInsertar As System.Windows.Forms.Button
    Friend WithEvents ButtonEliminar As System.Windows.Forms.Button
    Friend WithEvents TextBoxContrato As System.Windows.Forms.TextBox
    Friend WithEvents TreeViewActivos As System.Windows.Forms.TreeView
    Friend WithEvents TreeViewPreBajas As System.Windows.Forms.TreeView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxSoloInternet As System.Windows.Forms.CheckBox
    Friend WithEvents LabelCelular As System.Windows.Forms.Label
    Friend WithEvents LabelTelefono As System.Windows.Forms.Label
    Friend WithEvents LabelCiudad As System.Windows.Forms.Label
    Friend WithEvents LabelColonia As System.Windows.Forms.Label
    Friend WithEvents LabelNumero As System.Windows.Forms.Label
    Friend WithEvents LabelCalle As System.Windows.Forms.Label
    Friend WithEvents LabelNombre As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxMotCan As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PanelMotCan As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ComboBoxTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
End Class
