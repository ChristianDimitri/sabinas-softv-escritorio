﻿Imports System.Data.SqlClient
Imports sofTV.BAL
Imports System.Collections.Generic
Public Class FrmSucursales2
    Public RFC As String = Nothing
    Public LocIdCompania As Integer = 0
    Public eFila As Integer = 0
    Private Descripcion As String = Nothing
    Private Ip As String = Nothing
    Private Impresora As String = Nothing

    Private Sub FrmSucursales2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Llena_Estructura_Grid()
        'Llena_Combo()
        'If GloActivarCFD = 1 Then
        '    ''no se usa ya SP_Mizar_MuestraRFC()
        '    Label6.Visible = True
        '    ComboBox1.Visible = True
        '    RFC_CFD()
        'End If

        If opcion = "N" Then
            Me.CONSUCURSALESBindingSource.AddNew()
            Llena_Companias()
            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            Clv_SucursalTextBox.Text = gloClave.ToString
            BUSCA(gloClave)
            consultaDatosGeneralesSucursal(gloClave, 0)
            Llena_Companias()

            '  damecompaniasucursal()
            llenaSucursal() '??
            LLena_DetalleSuc(gloClave) '---
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            Clv_SucursalTextBox.Text = gloClave.ToString
            BUSCA(gloClave)
            consultaDatosGeneralesSucursal(gloClave, 0)
            Llena_Companias()

            '   damecompaniasucursal()
            LLena_DetalleSuc(gloClave) '---
            If NombreTextBox.Text = "Puntos Web" Then
                SerieTextBox1.ReadOnly = True
                No_folioTextBox.ReadOnly = True
            End If
            llenaSucursal() '??
            ' ComboEjemplo.Enabled = False
        End If
    End Sub

    Private Sub Llena_Estructura_Grid()

        DataGridView1.Columns.Add("Clvid", "Clvid")
        DataGridView1.Columns.Item("Clvid").Width = 5
        DataGridView1.Columns.Item("Clvid").ReadOnly = True

        DataGridView1.Columns.Add("id_compania", "id_compania")
        DataGridView1.Columns.Item("id_compania").Width = 5
        DataGridView1.Columns.Item("id_compania").ReadOnly = True

        DataGridView1.Columns.Add("razon_social", "razon_social")
        DataGridView1.Columns.Item("razon_social").Width = 260
        DataGridView1.Columns.Item("razon_social").ReadOnly = True

        DataGridView1.Columns.Add("ClvEq", "ClvEq")
        DataGridView1.Columns.Item("ClvEq").Width = 70
        DataGridView1.Columns.Add("Serie", "Serie")
        DataGridView1.Columns.Item("Serie").Width = 70
        DataGridView1.Columns.Add("Folio", "Folio")
        DataGridView1.Columns.Item("Folio").Width = 70
        DataGridView1.Columns.Add("SerieG", "SerieG")
        DataGridView1.Columns.Item("SerieG").Width = 70
        DataGridView1.Columns.Add("FolioG", "FolioG")
        DataGridView1.Columns.Item("FolioG").Width = 70
        DataGridView1.Columns.Add("SerieD", "SerieD")
        DataGridView1.Columns.Item("SerieD").Width = 70
        DataGridView1.Columns.Add("FolioD", "FolioD")
        DataGridView1.Columns.Item("FolioD").Width = 70

        Dim chk As New DataGridViewCheckBoxColumn()
        DataGridView1.Columns.Add(chk)
        chk.HeaderText = "Mismo Dom. Compañia"
        chk.Name = "DomCompania"
        DataGridView1.Columns.Item("DomCompania").Width = 100
        DataGridView1.Columns.Item("DomCompania").ReadOnly = True

        DataGridView1.Columns.Add("Rfc", "Rfc")
        DataGridView1.Columns.Item("Rfc").Width = 0

        'Compania a eliminar 
        DataGridView1.Columns.Add("eliminar", "eliminar")
        DataGridView1.Columns.Item("eliminar").Width = 260
        DataGridView1.Columns.Item("eliminar").ReadOnly = True
        DataGridView1.Columns.Item("eliminar").Visible = False

    End Sub


    Private Sub llenaSucursal()
        Try
            Dim comando As New SqlCommand()
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            comando.Connection = conexion
            comando.CommandText = "exec CONSUCURSALES " + gloClave.ToString
            Dim reader As SqlDataReader = comando.ExecuteReader()
            reader.Read()
            Clv_SucursalTextBox.Text = reader(0).ToString
            NombreTextBox.Text = reader(1).ToString
            IPTextBox.Text = reader(2).ToString
            ImpresoraTextBox.Text = reader(3).ToString
            '  Clv_EquivalenteTextBoxOrig.Text = reader(4).ToString '---
            '  SerieTextBoxOriginal.Text = reader(5).ToString '--
            '   UltimoFolioUsadoTextBoxOrig.Text = reader(6).ToString '--
            reader.Close()
            conexion.Close()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub damecompaniasucursal()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
        BaseII.ProcedimientoOutPut("DameCompaniaSucursal")
        Dim res As Integer = BaseII.dicoPar("@idcompania")
        If res = 0 Then
            ComboBoxCompanias.SelectedValue = 0
        Else
            ComboBoxCompanias.SelectedValue = res
        End If
        'Parte de serial
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
        'BaseII.CreateMyParameter("@serie", ParameterDirection.Output, SqlDbType.VarChar, 15)
        'BaseII.ProcedimientoOutPut("DameSerieSucursal")
        'SerieTextBoxOriginal.Text = BaseII.dicoPar("@serie")
    End Sub
    Private Sub ModificaCompaniaSucursal()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
        BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
        BaseII.Inserta("ModificaCompaniaSucursal")
    End Sub
    'Private Sub Llena_CompaniasEjemplo()
    '    Try
    '        BaseII.limpiaParametros()
    '        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
    '        ComboEjemplo.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
    '        ComboEjemplo.DisplayMember = "razon_social"
    '        ComboEjemplo.ValueMember = "id_compania"
    '        If ComboEjemplo.Items.Count > 0 Then
    '            ComboEjemplo.SelectedIndex = 0

    '        End If
    '        GloIdCompania = 0
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Private Sub Llena_Companias()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                Dim i As Integer = 0
                For i = 0 To ComboBoxCompanias.Items.Count - 2
                    DataGridView1.Rows.Add()
                Next
            End If

            GloIdCompania = 0
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub RFC_CFD()
        Dim conn As New SqlConnection(MiConexion)
        Try

            conn.Open()
            Dim comando As New SqlClient.SqlCommand("Dame_Serire_Folio_RFC", conn)
            comando.CommandType = CommandType.StoredProcedure
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            'Agrego el parámetro
            'Adaptador.SelectCommand.Parameters.Add("@RFC", SqlDbType.VarChar, 50).Value = eRFC

            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "Dame_Serire_Folio_RFC")
            Bs.DataSource = Dataset.Tables("Dame_Serire_Folio_RFC")

            'dgvResultadosClasificacion.DataSource = Bs
            ComboBox1.DataSource = Bs
            ComboBox1.DisplayMember = "Serie_Folio"
            ComboBox1.ValueMember = "Clave"

            'If ComboBox1.Items.Count > 0 Then
            '    ComboBox1.SelectedIndex = 0
            'End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conn.Close()
        End Try
    End Sub
    Public Sub Guarda_Rel_Ciudad_Ciudad(ByVal eClv_Sucursal As Integer, ByVal eClv_Ciudad As Integer, ByVal eClave_SerieFolio As String, ByVal eMatriz As Boolean, ByVal oId_Compania As Long, ByVal oRfc As String)

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Insetra_Rel_Sucursal_Ciudad"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm1 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                prm1.Value = eClv_Sucursal
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@Clave_SerieFolio", SqlDbType.VarChar, 50)
                prm3.Value = eClave_SerieFolio
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Matriz", SqlDbType.Bit)
                prm4.Value = eMatriz
                .Parameters.Add(prm4)



                Dim i As Integer = .ExecuteNonQuery()
                'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con éxito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With
            CON80.Close()
        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Sub
    Private Sub Muestra_Rel_Ciudad_Ciudad(ByVal eClv_Sucursal As Integer)

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                '.CommandText = "Muestra_Rel_Sucursal_Ciudad"
                '.CommandType = CommandType.StoredProcedure
                '.Connection = CON80
                '.CommandTimeout = 0
                .CommandText = "DimeSiMatriz"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0
                Dim prm2 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                prm2.Value = eClv_Sucursal
                .Parameters.Add(prm2)


                'Dim prm3 As New SqlParameter("@Clave_SerieFolio", SqlDbType.VarChar, 50)
                'prm3.Direction = ParameterDirection.Output
                'prm3.Value = 0
                '.Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Matriz", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)


                Dim i As Integer = .ExecuteNonQuery()
                'Ciudadcmb.Items.Add(prm1.Value)
                'Ciudadcmb.SelectedItem = prm1.Value
                'Dame_RFC_Por_Ciudad(prm1.Value)
                'RFC_CFD(RFC)
                '



                If GloActivarCFD = 1 Then
                    Dim CON1 As New SqlClient.SqlConnection(MiConexion)
                    Dim CMD1 As New SqlClient.SqlCommand()
                    CMD1 = New SqlClient.SqlCommand()
                    CON1.Open()
                    With CMD1
                        .CommandText = "Muestra_Rel_Sucursal_Ciudad"
                        .CommandType = CommandType.StoredProcedure
                        .Connection = CON80
                        .CommandTimeout = 0

                        Dim prm21 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                        prm21.Value = eClv_Sucursal
                        .Parameters.Add(prm21)


                        Dim prm31 As New SqlParameter("@Clave_SerieFolio", SqlDbType.VarChar, 50)
                        prm31.Direction = ParameterDirection.Output
                        prm31.Value = 0
                        .Parameters.Add(prm31)

                        Dim prm41 As New SqlParameter("@Matriz", SqlDbType.Bit)
                        prm41.Direction = ParameterDirection.Output
                        prm41.Value = 0
                        .Parameters.Add(prm41)


                        Dim j As Integer = .ExecuteNonQuery()
                        ComboBox1.SelectedValue = prm31.Value

                    End With

                End If

                MatrizChck.CheckState = CheckState.Unchecked

                If prm4.Value = 1 Then MatrizChck.CheckState = CheckState.Checked

                'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con éxito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With

        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            'System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Sub


    'Public Function Valida_Serie(ByVal LocSerie As String, LocClv_Sucursal As Integer) As Integer

    '    Dim CON80 As New SqlClient.SqlConnection(MiConexion)
    '    Dim CMD As New SqlClient.SqlCommand()
    '    Valida_Serie = 0
    '    Try
    '        CMD = New SqlClient.SqlCommand()
    '        CON80.Open()
    '        With CMD
    '            .CommandText = "Valida_Serie"
    '            .CommandType = CommandType.StoredProcedure
    '            .Connection = CON80
    '            .CommandTimeout = 0

    '            Dim prm2 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
    '            prm2.Value = LocClv_Sucursal
    '            .Parameters.Add(prm2)

    '            Dim prm1 As New SqlParameter("@SERIE", SqlDbType.VarChar, 10)
    '            prm1.Direction = ParameterDirection.Input
    '            prm1.Value = LocSerie
    '            .Parameters.Add(prm1)

    '            Dim prm3 As New SqlParameter("@Bnd", SqlDbType.Int)
    '            prm3.Direction = ParameterDirection.Output
    '            prm3.Value = 0
    '            .Parameters.Add(prm3)

    '            Dim i As Integer = .ExecuteNonQuery()
    '            Valida_Serie = prm3.Value
    '        End With

    '    Catch ex As Exception
    '        If CON80.State = ConnectionState.Open Then CON80.Close()
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    Finally
    '        CON80.Close()
    '    End Try
    'End Function


    Public Function Valida_Sucursal_Matriz(ByVal eClv_Sucursal As Integer, ByVal eClv_Ciudad As Integer) As Integer

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Valida_Sucursal_Matriz = 0
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Valida_Sucursal_Matriz"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm2 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                prm2.Value = eClv_Sucursal
                .Parameters.Add(prm2)

                Dim prm1 As New SqlParameter("@idcompania", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = ComboBoxCompanias.SelectedValue
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@Bnd", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim i As Integer = .ExecuteNonQuery()
                Valida_Sucursal_Matriz = prm3.Value
            End With

        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
        Return Valida_Sucursal_Matriz
    End Function
    Private Sub Dame_RFC_Por_Ciudad(ByVal eClv_Ciudad As Integer)

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        RFC = ""
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Dame_RFC_Por_Ciudad"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm2 As New SqlParameter("@Clv_Ciudad", SqlDbType.Int)
                prm2.Value = eClv_Ciudad
                .Parameters.Add(prm2)

                Dim prm1 As New SqlParameter("@RFC", SqlDbType.VarChar, 50)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)



                Dim i As Integer = .ExecuteNonQuery()
                RFC = prm1.Value

                'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con éxito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With

        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Sub

    Private Sub CONSUCURSALESBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSUCURSALESBindingNavigatorSaveItem.Click
        If IsNumeric(Ciudadcombo.SelectedItem) = False And Ciudadcombo.Items.Count > 0 Then
            MsgBox("Seleccione una Ciudad de la Lista ", vbInformation)
            Exit Sub
        End If
        If Ciudadcombo.SelectedItem <= 0 And Ciudadcombo.Items.Count > 0 Then
            MsgBox("Seleccione una Ciudad de la Lista ", vbInformation)
            Exit Sub
        End If
        'If GloActivarCFD = 1 Then
        '    If ComboBox1.SelectedIndex < 0 Then
        '        MsgBox("Seleccione la Series y Folio de la Lista ", vbInformation)
        '        Exit Sub
        '    End If
        'End If

        If ComboBox1.SelectedIndex <= 0 And ComboBox1.Items.Count > 0 Then 'agregado
            MsgBox("Seleccione la Series y Folio de la Lista ", vbInformation)
            Exit Sub
        End If

        'If SerieTextBoxOriginal.Text.Length > 5 Then
        If SerieTextBox.Text.Length > 5 Then
            MsgBox("La Serie no puede exceder de 5 caracteres.")
            Exit Sub
        End If
        If MatrizChck.CheckState = CheckState.Checked Then
            If Valida_Sucursal_Matriz(Clv_SucursalTextBox.Text, Ciudadcombo.SelectedItem) = 1 Then
                'MatrizChck.CheckState = CheckState.Unchecked
                MsgBox("Ya existe una sucursal que es la Matríz de la Plaza seleccionado ", vbInformation)
                Exit Sub
            End If
        End If


        If IsNumeric(Clv_SucursalTextBox.Text) = False Then Clv_SucursalTextBox.Text = 0
        '   If Valida_Serie(SerieTextBoxOriginal.Text, Clv_SucursalTextBox.Text) = 1 Then
        'If Valida_Serie(SerieTextBox.Text, Clv_SucursalTextBox.Text) = 1 Then
        '    'MatrizChck.CheckState = CheckState.Unchecked
        '    MsgBox("Ya existe una serie ", vbInformation)
        '    Exit Sub
        'End If

        If Me.txtCalle.Text.Length = 0 And Me.txtCalle.Enabled = True Then
            MsgBox("¡Capture la Calle de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtNumero.Text.Length = 0 And Me.txtNumero.Enabled = True Then
            MsgBox("¡Capture el Número de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtColonia.Text.Length = 0 And Me.txtColonia.Enabled = True Then
            MsgBox("¡Capture el Barrio de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtCP.Text.Length = 0 And txtCP.Enabled = True And Not IsNumeric(txtCP.Text) Then
            MsgBox("¡Capture el CP de la Sucursal!. Recuerde que debe ser un valor numérico" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtMunicipio.Text.Length = 0 And txtMunicipio.Enabled = True Then
            MsgBox("¡Capture el Municipio de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtCiudad.Text.Length = 0 And txtCiudad.Enabled = True Then
            MsgBox("¡Capture la Ciudad de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub

        End If

        Dim locerror As Integer
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'If IsNumeric(Me.No_folioTextBoxOriginal.Text) = False Then Me.No_folioTextBoxOriginal.Text = 0
            If IsNumeric(Me.No_folioTextBox.Text) = False Then Me.No_folioTextBox.Text = 0
            'Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
            'Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)
            locerror = 0
            If locerror = 0 Then
                If IsNumeric(Me.UltimoFolioUsadoTextBox.Text) = False Then Me.UltimoFolioUsadoTextBox.Text = 0
                Me.Validate()
                Me.CONSUCURSALESBindingSource.EndEdit()
                Me.CONSUCURSALESTableAdapter.Connection = CON
                Me.CONSUCURSALESTableAdapter.Update(Me.NewSofTvDataSet.CONSUCURSALES)
                If opcion = "M" Then

                    If Me.TxtIdCompania.Text = "" Then
                        Me.TxtIdCompania.Text = "0"
                    End If

                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
                    BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, NombreTextBox.Text)
                    BaseII.CreateMyParameter("@IP", SqlDbType.VarChar, IPTextBox.Text)
                    BaseII.CreateMyParameter("@Impresora", SqlDbType.VarChar, ImpresoraTextBox.Text)
                    'BaseII.CreateMyParameter("@Clv_Equivalente", SqlDbType.VarChar, Clv_EquivalenteTextBox.Text)
                    ' BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, SerieTextBox.Text)
                    ' BaseII.CreateMyParameter("@UltimoFolioUsado", SqlDbType.BigInt, UltimoFolioUsadoTextBox.Text)
                    BaseII.Inserta("MODSUCURSALES")
                End If
                If opcion = "N" Then
                    If Me.TxtIdCompania.Text = "" Then
                        Me.TxtIdCompania.Text = "0"
                    End If
                   
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Clv_Sucursal", ParameterDirection.Output, SqlDbType.Int)
                    BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, NombreTextBox.Text)
                    BaseII.CreateMyParameter("@IP", SqlDbType.VarChar, IPTextBox.Text)
                    BaseII.CreateMyParameter("@Impresora", SqlDbType.VarChar, ImpresoraTextBox.Text)
                    '  BaseII.CreateMyParameter("@Clv_Equivalente", SqlDbType.VarChar, Clv_EquivalenteTextBox.Text)
                    ' BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, SerieTextBox.Text)
                    ' BaseII.CreateMyParameter("@UltimoFolioUsado", SqlDbType.BigInt, UltimoFolioUsadoTextBox.Text)
                    BaseII.ProcedimientoOutPut("NUESUCURSALES")
                    Clv_SucursalTextBox.Text = BaseII.dicoPar("@Clv_Sucursal")


                End If
                Me.Inserta_impresora_sucursalTableAdapter1.Connection = CON
                Me.Inserta_impresora_sucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.inserta_impresora_sucursal, CInt(Me.Clv_SucursalTextBox.Text), Me.Impresora_ContratosTextBox.Text, Me.Impresora_TarjetasTextBox.Text, Me.TextBox1.Text)
                'Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
                'Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)

                'BaseII.limpiaParametros()
                'BaseII.CreateMyParameter("@serie", SqlDbType.VarChar, SerieTextBox.Text)
                'BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
                'BaseII.Inserta("InsertaSerieSucursal")

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
                If MatrizChck.Checked = True Then
                    BaseII.CreateMyParameter("@matriz", SqlDbType.Int, 1)
                Else
                    BaseII.CreateMyParameter("@matriz", SqlDbType.Int, 2)
                End If
                BaseII.Inserta("InsertaMatrizSucursal")

                If MatrizChck.Checked = True Then
                    Me.txtCalle.Text = ""
                    Me.txtNumero.Text = ""
                    Me.txtColonia.Text = ""
                    Me.txtCP.Text = 0
                    Me.txtMunicipio.Text = ""
                    Me.txtCiudad.Text = ""
                    Me.txtTelefono.Text = ""
                    Me.TxtNumInt.Text = ""
                    Me.tbentrecalles.Text = ""
                    Me.tbfax.Text = ""
                    Me.TxtPais.Text = ""
                    Me.tbEmail.Text = ""

                    insertaDatosGeneralesSucursal(
                        Clv_SucursalTextBox.Text, Me.txtCalle.Text, Me.txtNumero.Text, Me.TxtNumInt.Text,
                        Me.txtCP.Text, Me.txtColonia.Text, Me.tbentrecalles.Text, Me.txtTelefono.Text, Me.txtMunicipio.Text, Me.txtCiudad.Text,
                        Me.tbfax.Text, Me.TxtPais.Text, Me.tbEmail.Text, Me.CboxDom.Checked,
                        CInt(Me.TxtIdCompania.Text))   '@horario ,@referencia ,@contacto '  CInt(Me.txtCP.Text),


                    'insertaDatosGeneralesSucursal(Clv_SucursalTextBox.Text, Me.txtCalle.Text, Me.txtNumero.Text, Me.TxtNumInt.Text, CInt(Me.txtCP.Text), Me.txtColonia.Text, Me.tbentrecalles.Text, _
                    ' Me.txtTelefono.Text, Me.txtMunicipio.Text, Me.txtCiudad.Text, Me.tbfax.Text, Me.TxtPais.Text, Me.tbEmail.Text, Me.CboxDom.Checked, CInt(Me.TxtIdCompania.Text))   '@horario ,@referencia ,@contacto 

                ElseIf MatrizChck.Checked = False Then
                    insertaDatosGeneralesSucursal(
                        Clv_SucursalTextBox.Text,
                        Me.txtCalle.Text,
                        Me.txtNumero.Text,
                        Me.TxtNumInt.Text,
                        Me.txtCP.Text,
                        Me.txtColonia.Text,
                        Me.tbentrecalles.Text,
                        Me.txtTelefono.Text, Me.txtMunicipio.Text, Me.txtCiudad.Text,
                        Me.tbfax.Text, Me.TxtPais.Text, Me.tbEmail.Text, Me.CboxDom.Checked,
                        CInt(Me.TxtIdCompania.Text))  '@horario ,@referencia ,@contacto '  CInt(Me.txtCP.Text),

                    ' insertaDatosGeneralesSucursal(Clv_SucursalTextBox.Text, Me.txtCalle.Text, Me.txtNumero.Text, Me.TxtNumInt.Text, CInt(Me.txtCP.Text), Me.txtColonia.Text, Me.tbentrecalles.Text, _
                    'Me.txtTelefono.Text, Me.txtMunicipio.Text, Me.txtCiudad.Text, Me.tbfax.Text, Me.TxtPais.Text, Me.tbEmail.Text, Me.CboxDom.Checked, CInt(Me.TxtIdCompania.Text))   '@horario ,@referencia ,@contacto 

                End If
                'If IsNumeric(Ciudadcmb.SelectedValue) = True Then
                '    If Ciudadcmb.SelectedValue > 0 Then
                'If MatrizChck.CheckState = CheckState.Checked Then
                LocIdCompania = 0
                'Guarda_Rel_Ciudad_Ciudad(Clv_SucursalTextBox.Text, gloClave, ComboBox1.SelectedValue, MatrizChck.Checked, LocIdCompania, "")

                '  ModificaCompaniaSucursal() '** CHECAR SI SE NECESITA

                'Else
                '    LocIdCompania = 0
                '    If ComboBoxRFC.SelectedIndex > 0 Then
                '        If IsNumeric(ComboBoxRFC.SelectedValue) = True Then
                '            LocIdCompania = ComboBoxRFC.SelectedValue
                '        End If
                '    End If
                '    Guarda_Rel_Ciudad_Ciudad(Clv_SucursalTextBox.Text, Ciudadcmb.SelectedValue, ComboBox1.SelectedValue, False, LocIdCompania)
                'End If

                '    End If
                'End If

                Guarda_Detalle()
                bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Creación Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
                MsgBox(mensaje5)
                GloBnd = True
                CON.Close()
                Me.Close()
            Else
                MsgBox("La Serie de Factura Global ya Existe no se puede Guardar", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            'MsgBox("La Serie ya Existe no se Puede Guardar")
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub Guarda_Detalle()
        Dim oClv_Sucursa As Integer, oid_compania As Integer, oClvEq As String, oSerie As String, oFolio As Integer, oSerieG As String, FolioG As Integer, oSerieD As String, FolioD As Integer, oRfc As String

        oClv_Sucursa = Clv_SucursalTextBox.Text

        Dim i As Integer = 0
        For i = 0 To DataGridView1.RowCount - 1
            '  If CStr(DataGridView1.Item(0, i).Value) <> "" Then  ' fila vacía
            If CStr(DataGridView1.Item(12, i).Value) <> "" Then 'todas las filas con valor
                If IsNumeric(DataGridView1.Item(1, i).Value) = True Then oid_compania = DataGridView1.Item(1, i).Value Else oid_compania = 0
                oClvEq = DataGridView1.Item(3, i).Value
                oSerie = DataGridView1.Item(4, i).Value
                If IsNumeric(DataGridView1.Item(5, i).Value) = True Then oFolio = DataGridView1.Item(5, i).Value Else oFolio = 0
                oSerieG = DataGridView1.Item(6, i).Value
                If IsNumeric(DataGridView1.Item(7, i).Value) = True Then FolioG = DataGridView1.Item(7, i).Value Else FolioG = 0
                oSerieD = DataGridView1.Item(8, i).Value
                If IsNumeric(DataGridView1.Item(9, i).Value) = True Then FolioD = DataGridView1.Item(9, i).Value Else FolioD = 0

                Dim aEliminar As Integer = DataGridView1.Item(12, i).Value

                SP_GUARDARRel_SucursalCompania(Clv_SucursalTextBox.Text, oid_compania, oClvEq, oSerie, oFolio, oSerieG, FolioG, oSerieD, FolioD, "", aEliminar)
            End If
        Next
    End Sub


    Public Sub SP_GUARDARRel_SucursalCompania(ByVal oClv_Sucursa As Integer, oid_compania As Integer, oClvEq As String, oSerie As String, oFolio As Integer, oSerieG As String, FolioG As Integer, oSerieD As String, FolioD As Integer, oRfc As String, aEliminar As Integer)
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            '@Clv_Sucursal int,
            '@id_compania Int,	
            '@ClvEq Varchar(50),
            '@Serie Varchar(50),
            '@Folio int,
            '@SerieG Varchar(50),
            '@FolioG int,
            '@SerieD Varchar(50),
            '@FolioD int,
            '@Rfc Varchar(50)
            BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, oClv_Sucursa)
            BaseII.CreateMyParameter("@id_compania", SqlDbType.Int, oid_compania)
            BaseII.CreateMyParameter("@ClvEq", SqlDbType.VarChar, oClvEq, 50)
            BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, oSerie, 50)
            BaseII.CreateMyParameter("@Folio", SqlDbType.VarChar, oFolio)
            BaseII.CreateMyParameter("@SerieG", SqlDbType.VarChar, oSerieG, 50)
            BaseII.CreateMyParameter("@FolioG", SqlDbType.VarChar, FolioG)
            BaseII.CreateMyParameter("@SerieD", SqlDbType.VarChar, oSerieD, 50)
            BaseII.CreateMyParameter("@FolioD", SqlDbType.VarChar, FolioD)
            BaseII.CreateMyParameter("@Rfc", SqlDbType.VarChar, oRfc)
            BaseII.CreateMyParameter("@aEliminar", SqlDbType.Int, aEliminar)
            BaseII.Inserta("SP_GUARDARRel_SucursalCompania")
            'MsgBox("Se guardo con éxito", MsgBoxStyle.Information, )
            'ComboBoxCartera.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONSUCURSALESTableAdapter.Connection = CON
            Me.CONSUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.CONSUCURSALES, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            Me.Consulta_Impresora_SucursalTableAdapter1.Connection = CON
            Me.Consulta_Impresora_SucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.Consulta_Impresora_Sucursal, CLAVE)
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Connection = CON
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Consulta_Generales_FacturasGlobales, CLAVE)
            CON.Close()

            Muestra_Rel_Ciudad_Ciudad(CLAVE)
            'If (pos) > 0 Then
            '    Ciudadcmb.SelectedValue = pos
            'End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Clv_SucursalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SucursalTextBox.TextChanged
        gloClave = Me.Clv_SucursalTextBox.Text
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click

        ValidaEliminarRelCompaniasucursal(2)
        If Len(eMsj) > 0 Then
            MsgBox(eMsj, MsgBoxStyle.Information)
            Descripcion = NombreTextBox.Text
            Ip = IPTextBox.Text
            Impresora = ImpresoraTextBox.Text
            Exit Sub
        End If

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONSUCURSALESTableAdapter.Connection = CON
        Me.CONSUCURSALESTableAdapter.Delete(gloClave)
        Me.Borra_Impresora_SucursalesTableAdapter.Connection = CON
        Me.Borra_Impresora_SucursalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Impresora_Sucursales, gloClave)
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Connection = CON
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Generales_FacturasGlobales, gloClave)
        bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Eliminó Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
        CON.Close()
        GloBnd = True
        Me.Close()

    End Sub





    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub


    Private Sub insertaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmCalle As String, ByVal prmNumero As String, ByVal prmNumInt As String, _
         ByVal prmCP As String, ByVal prmColonia As String, ByVal prmEntreCalles As String, ByVal prmTelefono As String, ByVal prmMunicipio As String, ByVal prmCiudad As String, _
        ByVal prmFax As String, ByVal prmPais As String, ByVal prmEmail As String, ByVal prmDomCompania As String, ByVal prmIdCompania As Integer)

        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal) 'clvSucursal
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, prmCalle, 250) 'calle
            BaseII.CreateMyParameter("@numero", SqlDbType.VarChar, prmNumero, 100) '@numero
            BaseII.CreateMyParameter("@numint", SqlDbType.VarChar, prmNumInt, 100)  '@numint  ''
            Try
                prmCP = CInt(prmCP)
            Catch ex As Exception
                '  MsgBox(ex.Message)
                prmCP = 0
            End Try
            BaseII.CreateMyParameter("@cp", SqlDbType.Int, prmCP) 'cp           
            BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, prmColonia, 250) 'colonia
            BaseII.CreateMyParameter("@entrecalles", SqlDbType.VarChar, prmEntreCalles, 250)   'entrecalles ''
            BaseII.CreateMyParameter("@telefono", SqlDbType.VarChar, prmTelefono, 250) 'telefono
            BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, prmMunicipio, 250) 'municipio
            BaseII.CreateMyParameter("@ciudad", SqlDbType.VarChar, prmCiudad, 250) 'ciudad
            BaseII.CreateMyParameter("@fax", SqlDbType.VarChar, prmFax, 250)  'fax ''
            BaseII.CreateMyParameter("@pais", SqlDbType.VarChar, prmPais, 250)  'pais ''
            BaseII.CreateMyParameter("@email", SqlDbType.VarChar, tbEmail.Text)
            BaseII.CreateMyParameter("@DomCompania", SqlDbType.VarChar, prmDomCompania, 250)  'DomCompania ''
            BaseII.CreateMyParameter("@idCompania", SqlDbType.VarChar, prmIdCompania, 250)  'idCompania ''

            'Nuevos datos
            BaseII.CreateMyParameter("@horario", SqlDbType.VarChar, tbHorario.Text) 'horario
            BaseII.CreateMyParameter("@referencia", SqlDbType.VarChar, tbReferencia.Text) 'referencia
            BaseII.CreateMyParameter("@contacto", SqlDbType.VarChar, tbContacto.Text) 'contacto

            BaseII.Inserta("uspInsertaTblRelSucursalDatosGenerales")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub consultaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmClvFactura As Integer)

        Dim dtDatosGenerales As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
        BaseII.CreateMyParameter("@clvFactura", SqlDbType.Int, prmClvFactura)
        dtDatosGenerales = BaseII.ConsultaDT("uspConsultaTblRelSucursalDatosGenerales")


        If dtDatosGenerales.Rows.Count > 0 Then
            ' Me.TxtIdCompania.Text = dtDatosGenerales.Rows(0)("calle").ToString
            Me.txtCalle.Text = dtDatosGenerales.Rows(0)("calle").ToString
            Me.txtNumero.Text = dtDatosGenerales.Rows(0)("numero").ToString
            Me.TxtNumInt.Text = dtDatosGenerales.Rows(0)("numint").ToString
            Me.txtColonia.Text = dtDatosGenerales.Rows(0)("colonia").ToString
            Me.txtCP.Text = dtDatosGenerales.Rows(0)("cp").ToString
            Me.tbentrecalles.Text = dtDatosGenerales.Rows(0)("entrecalles").ToString
            Me.txtMunicipio.Text = dtDatosGenerales.Rows(0)("municipio").ToString
            Me.txtCiudad.Text = dtDatosGenerales.Rows(0)("ciudad").ToString
            Me.tbfax.Text = dtDatosGenerales.Rows(0)("fax").ToString
            Me.TxtPais.Text = dtDatosGenerales.Rows(0)("pais").ToString
            Me.txtTelefono.Text = dtDatosGenerales.Rows(0)("telefono").ToString
            tbHorario.Text = dtDatosGenerales.Rows(0)("horario").ToString
            tbContacto.Text = dtDatosGenerales.Rows(0)("contacto").ToString
            tbEmail.Text = dtDatosGenerales.Rows(0)("email").ToString
            tbReferencia.Text = dtDatosGenerales.Rows(0)("referenciabancaria").ToString

            TxtNumInt.Text = dtDatosGenerales.Rows(0)("numInt").ToString
            tbentrecalles.Text = dtDatosGenerales.Rows(0)("entrecalles").ToString
            TxtPais.Text = dtDatosGenerales.Rows(0)("pais").ToString
            tbfax.Text = dtDatosGenerales.Rows(0)("fax").ToString

            Me.TxtIdCompania.Text = dtDatosGenerales.Rows(0)("idCompania").ToString
            If dtDatosGenerales.Rows(0)("idCompania").ToString <> "" Then
                Me.ComboBoxCompanias.SelectedValue = dtDatosGenerales.Rows(0)("idCompania").ToString
            End If
            Me.ComboBoxCompanias.SelectedValue = 1
            Me.CboxDom.Checked = dtDatosGenerales.Rows(0)("DomCompania")

        End If
    End Sub

    Private Sub MatrizChck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MatrizChck.CheckedChanged
        If MatrizChck.Checked = True Then
            GroupBox1.Enabled = False
        ElseIf MatrizChck.Checked = False Then
            GroupBox1.Enabled = True
        End If
    End Sub

    'Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboEjemplo.SelectedIndexChanged
    '    Llena_Ciudades()
    'End Sub
    'Private Sub Llena_Ciudades()

    'End Sub

    Private Sub txtCP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCP.KeyPress
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtTelefono_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefono.KeyPress
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged_1(sender As Object, e As EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        If ComboBoxCompanias.SelectedIndex > 0 Then
            consultaCompaniaSucursal(gloClave, 0)
            If TxtIdCompania.Text = "" Then
                TxtIdCompania.Text = "0"
            End If
            If CboxDom.Enabled = True Then
                If ComboBoxCompanias.SelectedValue = TxtIdCompania.Text Then
                    CboxDom.Checked = True
                Else
                    CboxDom.Checked = False
                End If
            End If
        End If
    End Sub

    Private Sub consultaCompaniaSucursal(ByVal prmClvSucursal As Integer, ByVal prmClvFactura As Integer)

        Dim dtDatosGenerales As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
        BaseII.CreateMyParameter("@clvFactura", SqlDbType.Int, prmClvFactura)
        dtDatosGenerales = BaseII.ConsultaDT("uspConsultaTblRelSucursalDatosGenerales")


        If dtDatosGenerales.Rows.Count > 0 Then
            Me.TxtIdCompania.Text = dtDatosGenerales.Rows(0)("idCompania").ToString
            'Me.ComboBoxCompanias.SelectedValue = dtDatosGenerales.Rows(0)("idCompania").ToString
            'Me.CboxDom.Checked = dtDatosGenerales.Rows(0)("DomCompania")
        End If

    End Sub

    Private Sub ButtonAgregar_Click(sender As Object, e As EventArgs) Handles ButtonAgregar.Click
        If ComboBoxCompanias.SelectedIndex <= 0 Then
            MsgBox("Seleccione la Compañía", MsgBoxStyle.Information, "Importante")
            Exit Sub
        End If

        If Len(Clv_EquivalenteTextBox.Text) = 0 Or (Len(SerieTextBox.Text) = 0 Or IsNumeric(SerieTextBox.Text) = True) Or (Len(SerieTextBox1.Text) = 0 Or IsNumeric(SerieTextBox.Text) = True) Or IsNumeric(UltimoFolioUsadoTextBox.Text) = False Or IsNumeric(No_folioTextBox.Text) = False Then

            MsgBox("Ingrese todos los campos antes de agregar", MsgBoxStyle.Information, "Importante")
            Exit Sub
        End If
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim Bnd As Boolean = False
        For i = 0 To DataGridView1.RowCount - 1
            If CStr(DataGridView1.Item(1, i).Value) = ComboBoxCompanias.SelectedValue.ToString Then
                MsgBox("Ya existe la compañía en la lista")

                Clv_EquivalenteTextBox.Text = ""
                SerieTextBox.Text = ""
                UltimoFolioUsadoTextBox.Text = ""
                SerieTextBox1.Text = ""
                No_folioTextBox.Text = ""
                SerieFDTextBox.Text = ""
                FolioFDTextbox.Text = ""
                Exit Sub
            End If
        Next

        For i = 0 To DataGridView1.RowCount - 1
            If CStr(DataGridView1.Item(1, i).Value) = ComboBoxCompanias.SelectedValue.ToString Then
                MsgBox("Ya existe la compañía en la lista")
                Exit For
            End If
            If CStr(DataGridView1.Item(0, i).Value) = "" Then
                j = i
                Bnd = True
                Exit For
            End If
        Next
        If Bnd = True Then


            DataGridView1.Item(0, j).Value = 0
            DataGridView1.Item(1, j).Value = ComboBoxCompanias.SelectedValue
            DataGridView1.Item(2, j).Value = CStr(ComboBoxCompanias.Text)
            DataGridView1.Item(3, j).Value = CStr(Clv_EquivalenteTextBox.Text)
            DataGridView1.Item(4, j).Value = CStr(SerieTextBox.Text)
            DataGridView1.Item(5, j).Value = CStr(UltimoFolioUsadoTextBox.Text)
            DataGridView1.Item(6, j).Value = CStr(SerieTextBox1.Text)
            DataGridView1.Item(7, j).Value = CStr(No_folioTextBox.Text)
            DataGridView1.Item(8, j).Value = CStr(SerieFDTextBox.Text)
            DataGridView1.Item(9, j).Value = CStr(FolioFDTextbox.Text)
            DataGridView1.Item(12, j).Value = 0

            If ComboBoxCompanias.SelectedValue = Me.TxtIdCompania.Text Then
                DataGridView1.Item(10, j).Value = CboxDom.Checked
            End If

        End If

        'Clv_EquivalenteTextBoxOrig.Text = ""
        'SerieTextBoxOriginal.Text = ""
        'UltimoFolioUsadoTextBoxOrig.Text = ""
        'SerieTextBox1orig.Text = ""
        'No_folioTextBoxOriginal.Text = ""
        'SerieFDTextBox.Text = ""
        'FolioFDTextboxOriginal.Text = ""
        Clv_EquivalenteTextBox.Text = ""
        SerieTextBox.Text = ""
        UltimoFolioUsadoTextBox.Text = ""
        SerieTextBox1.Text = ""
        No_folioTextBox.Text = ""
        SerieFDTextBox.Text = ""
        FolioFDTextbox.Text = ""

        If CboxDom.Checked = True Then
            CboxDom.Enabled = False
        End If

    End Sub

    Private Sub LLena_DetalleSuc(oClv_Sucursal As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("SP_CONSULTARel_SucursalCompania", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Clv_Sucursal", oClv_Sucursal)


        Dim READER As SqlDataReader

        Try
            CON.Open()
            Dim J As Integer = 0
            READER = CMD.ExecuteReader()
            While (READER.Read)
                DataGridView1.Item(0, J).Value = READER(0).ToString()
                DataGridView1.Item(1, J).Value = READER(1).ToString()
                DataGridView1.Item(2, J).Value = READER(2).ToString()
                DataGridView1.Item(3, J).Value = READER(3).ToString()
                DataGridView1.Item(4, J).Value = READER(4).ToString()
                DataGridView1.Item(5, J).Value = READER(5).ToString()
                DataGridView1.Item(6, J).Value = READER(6).ToString()
                DataGridView1.Item(7, J).Value = READER(7).ToString()
                DataGridView1.Item(8, J).Value = READER(8).ToString()
                DataGridView1.Item(9, J).Value = READER(9).ToString()
                DataGridView1.Item(10, J).Value = READER(10).ToString()
                DataGridView1.Item(12, J).Value = 0

                J += 1
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub


    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub ButtonEliminar_Click(sender As Object, e As EventArgs) Handles ButtonEliminar.Click


        Dim companiaEliminar As Integer = DataGridView1.Rows(eFila).Cells(0).Value
        Dim idCompaniaEliminar As Integer = 0

        If DataGridView1.Rows.Count > 0 Then
            idCompaniaEliminar = DataGridView1.Rows(eFila).Cells(1).Value ' Columna id_compania
            MsgBox(idCompaniaEliminar)
        End If

        'ValidaEliminarRelCompaniasucursal(1)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvsucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, idCompaniaEliminar)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("ValidaEliminarRelCompaniasucursal")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString

        If eMsj.Length = 0 Then
            DataGridView1.Item(12, eFila).Value = 1
            If idCompaniaEliminar = TxtIdCompania.Text Then
                CboxDom.Enabled = True
                CboxDom.Checked = False
            End If
        End If

        If Len(eMsj) > 0 Then
            MsgBox(eMsj, MsgBoxStyle.Information)
            Exit Sub
        End If

        'DataGridView1.Item(0, eFila).Value = ""
        'DataGridView1.Item(1, eFila).Value = ""
        'DataGridView1.Item(2, eFila).Value = ""
        'DataGridView1.Item(3, eFila).Value = ""
        'DataGridView1.Item(4, eFila).Value = ""
        'DataGridView1.Item(5, eFila).Value = ""
        'DataGridView1.Item(6, eFila).Value = ""
        'DataGridView1.Item(7, eFila).Value = ""
        'DataGridView1.Item(8, eFila).Value = ""
        'DataGridView1.Item(9, eFila).Value = ""
        'DataGridView1.Item(10, eFila).Value = False

        DataGridView1.Rows(eFila).Visible = False
        DataGridView1.Item(10, eFila).Value = False

    End Sub

    Private Sub ValidaEliminarRelCompaniasucursal(ByVal Op As Integer)
        Dim IdCompañia As Integer = 0

        If DataGridView1.Rows.Count > 0 Then
            IdCompañia = DataGridView1.SelectedCells(1).Value.ToString
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvsucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, IdCompañia)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("ValidaEliminarRelCompaniasucursal")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString

        If eMsj.Length = 0 Then
            If IdCompañia = TxtIdCompania.Text Then
                CboxDom.Enabled = True
                CboxDom.Checked = False
            End If
        End If
    End Sub

    Private Function Valida_Serie(p1 As String, p2 As String) As Integer
        Throw New NotImplementedException
    End Function


    Private Sub llenacampos()

        TxtIdCompania.Text = "0"
        txtCalle.Text = ""
        tbentrecalles.Text = ""
        txtNumero.Text = ""
        TxtNumInt.Text = ""
        txtTelefono.Text = ""
        txtColonia.Text = ""
        tbfax.Text = ""
        txtCP.Text = ""
        tbemail.Text = ""
        txtLocalidad.Text = ""
        txtMunicipio.Text = ""
        txtCiudad.Text = ""
        TxtPais.Text = ""

        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim comando As New SqlCommand()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "DameDatosCompaniaLong"
        Dim p1 As New SqlParameter("@idcompania", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = GloIdCompania
        comando.Parameters.Add(p1)

        Dim reader As SqlDataReader = comando.ExecuteReader()
        reader.Read()
        TxtIdCompania.Text = reader(0).ToString
        txtCalle.Text = reader(3).ToString
        tbentrecalles.Text = reader(4).ToString
        txtNumero.Text = reader(5).ToString
        TxtNumInt.Text = reader(6).ToString
        txtTelefono.Text = reader(7).ToString
        txtColonia.Text = reader(8).ToString
        tbfax.Text = reader(9).ToString
        txtCP.Text = reader(10).ToString
        tbemail.Text = reader(11).ToString
        txtLocalidad.Text = reader(12).ToString
        txtMunicipio.Text = reader(13).ToString
        txtCiudad.Text = reader(14).ToString
        TxtPais.Text = reader(15).ToString
        reader.Close()
        conexion.Close()

    End Sub


    Private Sub CboxDom_CheckedChanged(sender As Object, e As EventArgs) Handles CboxDom.CheckedChanged

        If ComboBoxCompanias.SelectedValue > 0 Then
            If CboxDom.Checked = True Then
                GloIdCompania = ComboBoxCompanias.SelectedValue
                llenacampos()
                GroupBox1.Enabled = False
            ElseIf CboxDom.Checked = False Then
                txtCalle.Text = ""
                tbentrecalles.Text = ""
                txtNumero.Text = ""
                TxtNumInt.Text = ""
                txtTelefono.Text = ""
                txtColonia.Text = ""
                tbfax.Text = ""
                txtCP.Text = ""
                tbEmail.Text = ""
                txtLocalidad.Text = ""
                txtMunicipio.Text = ""
                txtCiudad.Text = ""
                TxtPais.Text = ""
                TxtIdCompania.Text = "0"
                GroupBox1.Enabled = True
            End If
   
        End If

    End Sub

   

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        eFila = e.RowIndex
    End Sub

    Private Sub DataGridView1_RowValidated(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.RowValidated
        eFila = e.RowIndex
    End Sub

    Private Sub DataGridView1_RowValidating(sender As Object, e As DataGridViewCellCancelEventArgs) Handles DataGridView1.RowValidating
        eFila = e.RowIndex
    End Sub
  

End Class