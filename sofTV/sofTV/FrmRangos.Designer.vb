<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRangos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBRangoInferiorLabel As System.Windows.Forms.Label
        Dim CMBRangoSuperiorLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRangos))
        Me.ConCatalogoDeRangosBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ConCatalogoDeRangosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ConCatalogoDeRangosBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CveRangoTextBox = New System.Windows.Forms.TextBox()
        Me.RangoInferiorTextBox = New System.Windows.Forms.TextBox()
        Me.RangoSuperiorTextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.ConCatalogoDeRangosTableAdapter = New sofTV.DataSetEDGARTableAdapters.ConCatalogoDeRangosTableAdapter()
        Me.ValidaRangosAEliminarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaRangosAEliminarTableAdapter = New sofTV.DataSetEDGARTableAdapters.ValidaRangosAEliminarTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        CMBRangoInferiorLabel = New System.Windows.Forms.Label()
        CMBRangoSuperiorLabel = New System.Windows.Forms.Label()
        CType(Me.ConCatalogoDeRangosBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConCatalogoDeRangosBindingNavigator.SuspendLayout()
        CType(Me.ConCatalogoDeRangosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaRangosAEliminarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBRangoInferiorLabel
        '
        CMBRangoInferiorLabel.AutoSize = True
        CMBRangoInferiorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBRangoInferiorLabel.Location = New System.Drawing.Point(27, 76)
        CMBRangoInferiorLabel.Name = "CMBRangoInferiorLabel"
        CMBRangoInferiorLabel.Size = New System.Drawing.Size(103, 15)
        CMBRangoInferiorLabel.TabIndex = 4
        CMBRangoInferiorLabel.Text = "Rango Inferior:"
        '
        'CMBRangoSuperiorLabel
        '
        CMBRangoSuperiorLabel.AutoSize = True
        CMBRangoSuperiorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBRangoSuperiorLabel.Location = New System.Drawing.Point(16, 105)
        CMBRangoSuperiorLabel.Name = "CMBRangoSuperiorLabel"
        CMBRangoSuperiorLabel.Size = New System.Drawing.Size(112, 15)
        CMBRangoSuperiorLabel.TabIndex = 6
        CMBRangoSuperiorLabel.Text = "Rango Superior:"
        '
        'ConCatalogoDeRangosBindingNavigator
        '
        Me.ConCatalogoDeRangosBindingNavigator.AddNewItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.BindingSource = Me.ConCatalogoDeRangosBindingSource
        Me.ConCatalogoDeRangosBindingNavigator.CountItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ConCatalogoDeRangosBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConCatalogoDeRangosBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConCatalogoDeRangosBindingNavigatorSaveItem})
        Me.ConCatalogoDeRangosBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConCatalogoDeRangosBindingNavigator.MoveFirstItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.MoveLastItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.MoveNextItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.MovePreviousItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.Name = "ConCatalogoDeRangosBindingNavigator"
        Me.ConCatalogoDeRangosBindingNavigator.PositionItem = Nothing
        Me.ConCatalogoDeRangosBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConCatalogoDeRangosBindingNavigator.Size = New System.Drawing.Size(483, 25)
        Me.ConCatalogoDeRangosBindingNavigator.TabIndex = 120
        Me.ConCatalogoDeRangosBindingNavigator.TabStop = True
        Me.ConCatalogoDeRangosBindingNavigator.Text = "BindingNavigator1"
        '
        'ConCatalogoDeRangosBindingSource
        '
        Me.ConCatalogoDeRangosBindingSource.DataMember = "ConCatalogoDeRangos"
        Me.ConCatalogoDeRangosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(88, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConCatalogoDeRangosBindingNavigatorSaveItem
        '
        Me.ConCatalogoDeRangosBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConCatalogoDeRangosBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConCatalogoDeRangosBindingNavigatorSaveItem.Name = "ConCatalogoDeRangosBindingNavigatorSaveItem"
        Me.ConCatalogoDeRangosBindingNavigatorSaveItem.Size = New System.Drawing.Size(88, 22)
        Me.ConCatalogoDeRangosBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'CveRangoTextBox
        '
        Me.CveRangoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCatalogoDeRangosBindingSource, "CveRango", True))
        Me.CveRangoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CveRangoTextBox.Location = New System.Drawing.Point(371, 136)
        Me.CveRangoTextBox.Name = "CveRangoTextBox"
        Me.CveRangoTextBox.ReadOnly = True
        Me.CveRangoTextBox.Size = New System.Drawing.Size(10, 21)
        Me.CveRangoTextBox.TabIndex = 3
        Me.CveRangoTextBox.TabStop = False
        '
        'RangoInferiorTextBox
        '
        Me.RangoInferiorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCatalogoDeRangosBindingSource, "RangoInferior", True))
        Me.RangoInferiorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RangoInferiorTextBox.Location = New System.Drawing.Point(143, 73)
        Me.RangoInferiorTextBox.Name = "RangoInferiorTextBox"
        Me.RangoInferiorTextBox.Size = New System.Drawing.Size(226, 21)
        Me.RangoInferiorTextBox.TabIndex = 100
        '
        'RangoSuperiorTextBox
        '
        Me.RangoSuperiorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCatalogoDeRangosBindingSource, "RangoSuperior", True))
        Me.RangoSuperiorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RangoSuperiorTextBox.Location = New System.Drawing.Point(143, 100)
        Me.RangoSuperiorTextBox.Name = "RangoSuperiorTextBox"
        Me.RangoSuperiorTextBox.Size = New System.Drawing.Size(226, 21)
        Me.RangoSuperiorTextBox.TabIndex = 101
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(335, 127)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 130
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'ConCatalogoDeRangosTableAdapter
        '
        Me.ConCatalogoDeRangosTableAdapter.ClearBeforeFill = True
        '
        'ValidaRangosAEliminarBindingSource
        '
        Me.ValidaRangosAEliminarBindingSource.DataMember = "ValidaRangosAEliminar"
        Me.ValidaRangosAEliminarBindingSource.DataSource = Me.DataSetEDGAR
        '
        'ValidaRangosAEliminarTableAdapter
        '
        Me.ValidaRangosAEliminarTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(38, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 15)
        Me.Label1.TabIndex = 207
        Me.Label1.Text = "Distribuidor :"
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.ConCatalogoDeRangosBindingSource, "idcompania", True))
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(143, 39)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(328, 23)
        Me.ComboBoxCompanias.TabIndex = 206
        '
        'FrmRangos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(483, 175)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxCompanias)
        Me.Controls.Add(Me.RangoSuperiorTextBox)
        Me.Controls.Add(CMBRangoSuperiorLabel)
        Me.Controls.Add(Me.RangoInferiorTextBox)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(CMBRangoInferiorLabel)
        Me.Controls.Add(Me.ConCatalogoDeRangosBindingNavigator)
        Me.Controls.Add(Me.CveRangoTextBox)
        Me.Name = "FrmRangos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Rangos"
        CType(Me.ConCatalogoDeRangosBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConCatalogoDeRangosBindingNavigator.ResumeLayout(False)
        Me.ConCatalogoDeRangosBindingNavigator.PerformLayout()
        CType(Me.ConCatalogoDeRangosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaRangosAEliminarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents ConCatalogoDeRangosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConCatalogoDeRangosTableAdapter As sofTV.DataSetEDGARTableAdapters.ConCatalogoDeRangosTableAdapter
    Friend WithEvents ConCatalogoDeRangosBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConCatalogoDeRangosBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CveRangoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RangoInferiorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RangoSuperiorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ValidaRangosAEliminarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaRangosAEliminarTableAdapter As sofTV.DataSetEDGARTableAdapters.ValidaRangosAEliminarTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
End Class
