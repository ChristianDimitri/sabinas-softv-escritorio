<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmNotasdeCredito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_NotadeCreditoLabel As System.Windows.Forms.Label
        Dim SerieLabel As System.Windows.Forms.Label
        Dim Nota_CreditoLabel As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim FacturaLabel As System.Windows.Forms.Label
        Dim Fecha_deGeneracionLabel As System.Windows.Forms.Label
        Dim Usuario_CapturaLabel As System.Windows.Forms.Label
        Dim Usuario_AutorizoLabel As System.Windows.Forms.Label
        Dim Fecha_CaducidadLabel As System.Windows.Forms.Label
        Dim MontoLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim ObservacionesLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmNotasdeCredito))
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.Consulta_NotaCreditoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_NotaCreditoTableAdapter = New sofTV.DataSetLidiaTableAdapters.Consulta_NotaCreditoTableAdapter()
        Me.Consulta_NotaCreditoBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_NotadeCreditoTextBox = New System.Windows.Forms.TextBox()
        Me.SerieTextBox = New System.Windows.Forms.TextBox()
        Me.Nota_CreditoTextBox = New System.Windows.Forms.TextBox()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.FacturaTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_deGeneracionDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Usuario_CapturaTextBox = New System.Windows.Forms.TextBox()
        Me.Usuario_AutorizoTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_CaducidadDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.MontoTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.ObservacionesTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.Dame_fecha_hora_servBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_fecha_hora_servTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_fecha_hora_servTableAdapter()
        Clv_NotadeCreditoLabel = New System.Windows.Forms.Label()
        SerieLabel = New System.Windows.Forms.Label()
        Nota_CreditoLabel = New System.Windows.Forms.Label()
        ContratoLabel = New System.Windows.Forms.Label()
        FacturaLabel = New System.Windows.Forms.Label()
        Fecha_deGeneracionLabel = New System.Windows.Forms.Label()
        Usuario_CapturaLabel = New System.Windows.Forms.Label()
        Usuario_AutorizoLabel = New System.Windows.Forms.Label()
        Fecha_CaducidadLabel = New System.Windows.Forms.Label()
        MontoLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        ObservacionesLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_NotaCreditoBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_NotaCreditoBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_fecha_hora_servBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_NotadeCreditoLabel
        '
        Clv_NotadeCreditoLabel.AutoSize = True
        Clv_NotadeCreditoLabel.Location = New System.Drawing.Point(108, 682)
        Clv_NotadeCreditoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_NotadeCreditoLabel.Name = "Clv_NotadeCreditoLabel"
        Clv_NotadeCreditoLabel.Size = New System.Drawing.Size(130, 17)
        Clv_NotadeCreditoLabel.TabIndex = 1
        Clv_NotadeCreditoLabel.Text = "Clv Notade Credito:"
        '
        'SerieLabel
        '
        SerieLabel.AutoSize = True
        SerieLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SerieLabel.Location = New System.Drawing.Point(151, 14)
        SerieLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        SerieLabel.Name = "SerieLabel"
        SerieLabel.Size = New System.Drawing.Size(52, 18)
        SerieLabel.TabIndex = 3
        SerieLabel.Text = "Serie:"
        '
        'Nota_CreditoLabel
        '
        Nota_CreditoLabel.AutoSize = True
        Nota_CreditoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Nota_CreditoLabel.Location = New System.Drawing.Point(89, 41)
        Nota_CreditoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Nota_CreditoLabel.Name = "Nota_CreditoLabel"
        Nota_CreditoLabel.Size = New System.Drawing.Size(109, 18)
        Nota_CreditoLabel.TabIndex = 5
        Nota_CreditoLabel.Text = "Nota Credito:"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.Location = New System.Drawing.Point(124, 97)
        ContratoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(79, 18)
        ContratoLabel.TabIndex = 7
        ContratoLabel.Text = "Contrato:"
        '
        'FacturaLabel
        '
        FacturaLabel.AutoSize = True
        FacturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FacturaLabel.Location = New System.Drawing.Point(132, 129)
        FacturaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        FacturaLabel.Name = "FacturaLabel"
        FacturaLabel.Size = New System.Drawing.Size(70, 18)
        FacturaLabel.TabIndex = 9
        FacturaLabel.Text = "Factura:"
        '
        'Fecha_deGeneracionLabel
        '
        Fecha_deGeneracionLabel.AutoSize = True
        Fecha_deGeneracionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_deGeneracionLabel.Location = New System.Drawing.Point(13, 162)
        Fecha_deGeneracionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Fecha_deGeneracionLabel.Name = "Fecha_deGeneracionLabel"
        Fecha_deGeneracionLabel.Size = New System.Drawing.Size(174, 18)
        Fecha_deGeneracionLabel.TabIndex = 11
        Fecha_deGeneracionLabel.Text = "Fecha de Generacion:"
        '
        'Usuario_CapturaLabel
        '
        Usuario_CapturaLabel.AutoSize = True
        Usuario_CapturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Usuario_CapturaLabel.Location = New System.Drawing.Point(57, 193)
        Usuario_CapturaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Usuario_CapturaLabel.Name = "Usuario_CapturaLabel"
        Usuario_CapturaLabel.Size = New System.Drawing.Size(136, 18)
        Usuario_CapturaLabel.TabIndex = 13
        Usuario_CapturaLabel.Text = "Usuario Captura:"
        '
        'Usuario_AutorizoLabel
        '
        Usuario_AutorizoLabel.AutoSize = True
        Usuario_AutorizoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Usuario_AutorizoLabel.Location = New System.Drawing.Point(55, 225)
        Usuario_AutorizoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Usuario_AutorizoLabel.Name = "Usuario_AutorizoLabel"
        Usuario_AutorizoLabel.Size = New System.Drawing.Size(140, 18)
        Usuario_AutorizoLabel.TabIndex = 15
        Usuario_AutorizoLabel.Text = "Usuario Autorizo:"
        '
        'Fecha_CaducidadLabel
        '
        Fecha_CaducidadLabel.AutoSize = True
        Fecha_CaducidadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_CaducidadLabel.Location = New System.Drawing.Point(48, 258)
        Fecha_CaducidadLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Fecha_CaducidadLabel.Name = "Fecha_CaducidadLabel"
        Fecha_CaducidadLabel.Size = New System.Drawing.Size(143, 18)
        Fecha_CaducidadLabel.TabIndex = 17
        Fecha_CaducidadLabel.Text = "Fecha Caducidad:"
        '
        'MontoLabel
        '
        MontoLabel.AutoSize = True
        MontoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MontoLabel.Location = New System.Drawing.Point(143, 289)
        MontoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        MontoLabel.Name = "MontoLabel"
        MontoLabel.Size = New System.Drawing.Size(61, 18)
        MontoLabel.TabIndex = 19
        MontoLabel.Text = "Monto:"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel.Location = New System.Drawing.Point(143, 321)
        StatusLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(61, 18)
        StatusLabel.TabIndex = 21
        StatusLabel.Text = "Status:"
        '
        'ObservacionesLabel
        '
        ObservacionesLabel.AutoSize = True
        ObservacionesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObservacionesLabel.Location = New System.Drawing.Point(71, 353)
        ObservacionesLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ObservacionesLabel.Name = "ObservacionesLabel"
        ObservacionesLabel.Size = New System.Drawing.Size(126, 18)
        ObservacionesLabel.TabIndex = 23
        ObservacionesLabel.Text = "Observaciones:"
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_NotaCreditoBindingSource
        '
        Me.Consulta_NotaCreditoBindingSource.DataMember = "Consulta_NotaCredito"
        Me.Consulta_NotaCreditoBindingSource.DataSource = Me.DataSetLidia
        '
        'Consulta_NotaCreditoTableAdapter
        '
        Me.Consulta_NotaCreditoTableAdapter.ClearBeforeFill = True
        '
        'Consulta_NotaCreditoBindingNavigator
        '
        Me.Consulta_NotaCreditoBindingNavigator.AddNewItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.BindingSource = Me.Consulta_NotaCreditoBindingSource
        Me.Consulta_NotaCreditoBindingNavigator.CountItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_NotaCreditoBindingNavigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_NotaCreditoBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.Consulta_NotaCreditoBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_NotaCreditoBindingNavigatorSaveItem})
        Me.Consulta_NotaCreditoBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_NotaCreditoBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.Name = "Consulta_NotaCreditoBindingNavigator"
        Me.Consulta_NotaCreditoBindingNavigator.PositionItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.Size = New System.Drawing.Size(1006, 27)
        Me.Consulta_NotaCreditoBindingNavigator.TabIndex = 0
        Me.Consulta_NotaCreditoBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(91, 24)
        Me.BindingNavigatorDeleteItem.Text = "Cancelar"
        '
        'Consulta_NotaCreditoBindingNavigatorSaveItem
        '
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_NotaCreditoBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem.Name = "Consulta_NotaCreditoBindingNavigatorSaveItem"
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem.Size = New System.Drawing.Size(131, 24)
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Clv_NotadeCreditoTextBox
        '
        Me.Clv_NotadeCreditoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Clv_NotadeCredito", True))
        Me.Clv_NotadeCreditoTextBox.Location = New System.Drawing.Point(267, 678)
        Me.Clv_NotadeCreditoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_NotadeCreditoTextBox.Name = "Clv_NotadeCreditoTextBox"
        Me.Clv_NotadeCreditoTextBox.Size = New System.Drawing.Size(265, 22)
        Me.Clv_NotadeCreditoTextBox.TabIndex = 2
        '
        'SerieTextBox
        '
        Me.SerieTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Serie", True))
        Me.SerieTextBox.Location = New System.Drawing.Point(252, 7)
        Me.SerieTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SerieTextBox.Name = "SerieTextBox"
        Me.SerieTextBox.ReadOnly = True
        Me.SerieTextBox.Size = New System.Drawing.Size(265, 22)
        Me.SerieTextBox.TabIndex = 4
        '
        'Nota_CreditoTextBox
        '
        Me.Nota_CreditoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Nota_Credito", True))
        Me.Nota_CreditoTextBox.Location = New System.Drawing.Point(252, 39)
        Me.Nota_CreditoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Nota_CreditoTextBox.Name = "Nota_CreditoTextBox"
        Me.Nota_CreditoTextBox.ReadOnly = True
        Me.Nota_CreditoTextBox.Size = New System.Drawing.Size(265, 22)
        Me.Nota_CreditoTextBox.TabIndex = 6
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Contrato", True))
        Me.ContratoTextBox.Location = New System.Drawing.Point(252, 96)
        Me.ContratoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(151, 22)
        Me.ContratoTextBox.TabIndex = 8
        '
        'FacturaTextBox
        '
        Me.FacturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Factura", True))
        Me.FacturaTextBox.Location = New System.Drawing.Point(252, 128)
        Me.FacturaTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FacturaTextBox.Name = "FacturaTextBox"
        Me.FacturaTextBox.Size = New System.Drawing.Size(151, 22)
        Me.FacturaTextBox.TabIndex = 10
        '
        'Fecha_deGeneracionDateTimePicker
        '
        Me.Fecha_deGeneracionDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Consulta_NotaCreditoBindingSource, "Fecha_deGeneracion", True))
        Me.Fecha_deGeneracionDateTimePicker.Location = New System.Drawing.Point(252, 160)
        Me.Fecha_deGeneracionDateTimePicker.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Fecha_deGeneracionDateTimePicker.Name = "Fecha_deGeneracionDateTimePicker"
        Me.Fecha_deGeneracionDateTimePicker.Size = New System.Drawing.Size(293, 22)
        Me.Fecha_deGeneracionDateTimePicker.TabIndex = 12
        '
        'Usuario_CapturaTextBox
        '
        Me.Usuario_CapturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Usuario_Captura", True))
        Me.Usuario_CapturaTextBox.Location = New System.Drawing.Point(252, 192)
        Me.Usuario_CapturaTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Usuario_CapturaTextBox.Name = "Usuario_CapturaTextBox"
        Me.Usuario_CapturaTextBox.Size = New System.Drawing.Size(265, 22)
        Me.Usuario_CapturaTextBox.TabIndex = 14
        '
        'Usuario_AutorizoTextBox
        '
        Me.Usuario_AutorizoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Usuario_Autorizo", True))
        Me.Usuario_AutorizoTextBox.Location = New System.Drawing.Point(252, 224)
        Me.Usuario_AutorizoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Usuario_AutorizoTextBox.Name = "Usuario_AutorizoTextBox"
        Me.Usuario_AutorizoTextBox.Size = New System.Drawing.Size(265, 22)
        Me.Usuario_AutorizoTextBox.TabIndex = 16
        '
        'Fecha_CaducidadDateTimePicker
        '
        Me.Fecha_CaducidadDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Consulta_NotaCreditoBindingSource, "Fecha_Caducidad", True))
        Me.Fecha_CaducidadDateTimePicker.Location = New System.Drawing.Point(252, 256)
        Me.Fecha_CaducidadDateTimePicker.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Fecha_CaducidadDateTimePicker.Name = "Fecha_CaducidadDateTimePicker"
        Me.Fecha_CaducidadDateTimePicker.Size = New System.Drawing.Size(265, 22)
        Me.Fecha_CaducidadDateTimePicker.TabIndex = 18
        '
        'MontoTextBox
        '
        Me.MontoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Monto", True))
        Me.MontoTextBox.Location = New System.Drawing.Point(252, 288)
        Me.MontoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MontoTextBox.Name = "MontoTextBox"
        Me.MontoTextBox.Size = New System.Drawing.Size(265, 22)
        Me.MontoTextBox.TabIndex = 20
        '
        'StatusTextBox
        '
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(252, 320)
        Me.StatusTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(265, 22)
        Me.StatusTextBox.TabIndex = 22
        '
        'ObservacionesTextBox
        '
        Me.ObservacionesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Observaciones", True))
        Me.ObservacionesTextBox.Location = New System.Drawing.Point(252, 352)
        Me.ObservacionesTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ObservacionesTextBox.Multiline = True
        Me.ObservacionesTextBox.Name = "ObservacionesTextBox"
        Me.ObservacionesTextBox.Size = New System.Drawing.Size(652, 93)
        Me.ObservacionesTextBox.TabIndex = 24
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Nota_CreditoTextBox)
        Me.Panel1.Controls.Add(Me.ObservacionesTextBox)
        Me.Panel1.Controls.Add(ObservacionesLabel)
        Me.Panel1.Controls.Add(SerieLabel)
        Me.Panel1.Controls.Add(Me.StatusTextBox)
        Me.Panel1.Controls.Add(Me.SerieTextBox)
        Me.Panel1.Controls.Add(StatusLabel)
        Me.Panel1.Controls.Add(Nota_CreditoLabel)
        Me.Panel1.Controls.Add(Me.MontoTextBox)
        Me.Panel1.Controls.Add(MontoLabel)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Me.Fecha_CaducidadDateTimePicker)
        Me.Panel1.Controls.Add(Me.ContratoTextBox)
        Me.Panel1.Controls.Add(Fecha_CaducidadLabel)
        Me.Panel1.Controls.Add(FacturaLabel)
        Me.Panel1.Controls.Add(Me.Usuario_AutorizoTextBox)
        Me.Panel1.Controls.Add(Me.FacturaTextBox)
        Me.Panel1.Controls.Add(Usuario_AutorizoLabel)
        Me.Panel1.Controls.Add(Fecha_deGeneracionLabel)
        Me.Panel1.Controls.Add(Me.Usuario_CapturaTextBox)
        Me.Panel1.Controls.Add(Me.Fecha_deGeneracionDateTimePicker)
        Me.Panel1.Controls.Add(Usuario_CapturaLabel)
        Me.Panel1.Location = New System.Drawing.Point(23, 50)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(979, 484)
        Me.Panel1.TabIndex = 25
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(773, 555)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(228, 42)
        Me.Button1.TabIndex = 26
        Me.Button1.Text = "SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_fecha_hora_servBindingSource
        '
        Me.Dame_fecha_hora_servBindingSource.DataMember = "Dame_fecha_hora_serv"
        Me.Dame_fecha_hora_servBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Dame_fecha_hora_servTableAdapter
        '
        Me.Dame_fecha_hora_servTableAdapter.ClearBeforeFill = True
        '
        'FrmNotasdeCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1027, 623)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Clv_NotadeCreditoLabel)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Consulta_NotaCreditoBindingNavigator)
        Me.Controls.Add(Me.Clv_NotadeCreditoTextBox)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmNotasdeCredito"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Notas de Crédito"
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_NotaCreditoBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_NotaCreditoBindingNavigator.ResumeLayout(False)
        Me.Consulta_NotaCreditoBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_fecha_hora_servBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents Consulta_NotaCreditoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_NotaCreditoTableAdapter As sofTV.DataSetLidiaTableAdapters.Consulta_NotaCreditoTableAdapter
    Friend WithEvents Consulta_NotaCreditoBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_NotaCreditoBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_NotadeCreditoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SerieTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Nota_CreditoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FacturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_deGeneracionDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Usuario_CapturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Usuario_AutorizoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_CaducidadDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents MontoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ObservacionesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Dame_fecha_hora_servBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_fecha_hora_servTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_fecha_hora_servTableAdapter
End Class
