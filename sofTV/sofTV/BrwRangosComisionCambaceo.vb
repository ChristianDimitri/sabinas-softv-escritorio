﻿Public Class BrwRangosComisionCambaceo

    Private Sub BrwRangosComisionCambaceo_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        llenaRangos()
    End Sub

    Private Sub BrwRangosComisionCambaceo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaRangos()

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub llenaRangos()
        BaseII.limpiaParametros()
        MuestraCatalogoDeRangosDataGridView.DataSource = BaseII.ConsultaDT("MuestraRangosComisionCambaceo")
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        eOpcion = "N"
        Dim frm As FrmRangosComisionCambaceo = New FrmRangosComisionCambaceo
        frm.ShowDialog()
    End Sub

    Private Sub MuestraCatalogoDeRangosDataGridView_SelectionChanged(sender As Object, e As EventArgs) Handles MuestraCatalogoDeRangosDataGridView.SelectionChanged
        CveRangoTextBox.Text = MuestraCatalogoDeRangosDataGridView.CurrentRow.Cells(0).Value
        RangoInferiorTextBox.Text = MuestraCatalogoDeRangosDataGridView.CurrentRow.Cells(1).Value
        RangoSuperiorTextBox.Text = MuestraCatalogoDeRangosDataGridView.CurrentRow.Cells(2).Value
        TextBoxPago.Text = MuestraCatalogoDeRangosDataGridView.CurrentRow.Cells(3).Value
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Me.MuestraCatalogoDeRangosDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eCveRango = Me.CveRangoTextBox.Text
            eRangoInferior = Me.RangoInferiorTextBox.Text
            eRangoSuperior = Me.RangoSuperiorTextBox.Text
            ePrecio = TextBoxPago.Text
            Dim frm As FrmRangosComisionCambaceo = New FrmRangosComisionCambaceo
            frm.ShowDialog()
        Else
            MsgBox("No Existen Registros para Modificar.", , "Error")
        End If
    End Sub
End Class