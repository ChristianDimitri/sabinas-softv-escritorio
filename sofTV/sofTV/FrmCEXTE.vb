Imports System.Data.SqlClient
Public Class FrmCEXTE

    Private Sub Agregar(ByVal Clave As Long, ByVal Clv_Orden As Long, ByVal Contrato As Long, ByVal ExtAdic As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MODCEXTE", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clave", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clave
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Orden
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Contrato
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@ExtAdic", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = ExtAdic
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Borrar(ByVal Clave As Long, ByVal Clv_Orden As Long, ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BORCEXTE", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clave", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clave
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Orden
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Contrato
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Consultar(ByVal Clave As Long, ByVal Clv_Orden As Long, ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CONCEXTE", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clave", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clave
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Orden
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Contrato
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@ExtAdic", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            Me.ExtAdicTextBox.Text = parametro4.Value.ToString
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub CONCONEXBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCONEXBindingNavigatorSaveItem.Click
        If IsNumeric(Me.ExtAdicTextBox.Text) = False Then Me.ExtAdicTextBox.Text = 0
        Agregar(GloDetClave, gloClv_Orden, Contrato, CInt(Me.ExtAdicTextBox.Text))
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Borrar(GloDetClave, gloClv_Orden, Contrato)
        Me.Close()
    End Sub

    Private Sub FrmCEXTE_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GloBloqueaDetalle = True
    End Sub

    Private Sub FrmCEXTE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.ExtAdicLabel.ForeColor = Color.Black
        Consultar(GloDetClave, gloClv_Orden, Contrato)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class