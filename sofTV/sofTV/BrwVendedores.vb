Imports System.Data.SqlClient
Public Class BrwVendedores
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                GloIdCompania = ComboBoxCompanias.SelectedValue
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaDistribuidores()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@clvusuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxDistribuidor.DataSource = BaseII.ConsultaDT("Muestra_PlazasPorUsuario")
            ComboBoxDistribuidor.DisplayMember = "Nombre"
            ComboBoxDistribuidor.ValueMember = "Clv_Plaza"

            If ComboBoxDistribuidor.Items.Count > 0 Then
                ComboBoxDistribuidor.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmVendedores.Show()
    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            gloClave = Me.Clv_calleLabel2.Text
            FrmVendedores.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            gloClave = Me.Clv_calleLabel2.Text
            FrmVendedores.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            modificar()
        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try

            If op = 0 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.BUSCAVENDEDORESTableAdapter.Connection = CON
                    Me.BUSCAVENDEDORESTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAVENDEDORES, 0, Me.TextBox1.Text, "", GloClvUsuario, ComboBoxDistribuidor.SelectedValue)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            ElseIf op = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSCAVENDEDORESTableAdapter.Connection = CON
                    Me.BUSCAVENDEDORESTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAVENDEDORES, 1, 0, Me.TextBox2.Text, GloClvUsuario, ComboBoxDistribuidor.SelectedValue)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 3 Then

                Me.BUSCAVENDEDORESTableAdapter.Connection = CON
                Me.BUSCAVENDEDORESTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAVENDEDORES, 3, 0, "", GloClvUsuario, ComboBoxDistribuidor.SelectedValue)

            Else

                Me.BUSCAVENDEDORESTableAdapter.Connection = CON
                Me.BUSCAVENDEDORESTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAVENDEDORES, 2, 0, "", GloClvUsuario, ComboBoxDistribuidor.SelectedValue)
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()

        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            gloClave = Me.Clv_calleLabel2.Text
        End If
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub

    Private Sub BrwVendedores_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Busca(2)
        End If
    End Sub

    Private Sub BrwVendedores_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaDistribuidores()
        'Llena_companias()
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Busca(2)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Busca(3)
    End Sub

    Private Sub ComboBoxDistribuidor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxDistribuidor.SelectedIndexChanged
        Busca(3)
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class