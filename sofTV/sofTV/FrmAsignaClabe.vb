﻿Public Class FrmAsignaClabe
    Dim yaTiene As Integer
    Private Sub FrmAsignaClabe_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@yaTiene", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@clabe", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@id", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("YaTieneClabe")
        If BaseII.dicoPar("@yaTiene") = 1 Then
            yaTiene = 1
            tbClabe.Visible = True
            cbClabe.Visible = False
            tbClabe.Text = BaseII.dicoPar("@clabe").ToString
        Else
            yaTiene = 0
            tbClabe.Visible = False
            cbClabe.Visible = True
            BaseII.limpiaParametros()
            cbClabe.DataSource = BaseII.ConsultaDT("ObtieneClabeAsignar")
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If yaTiene = 0 Then
            If cbClabe.Items.Count = 0 Or cbClabe.SelectedValue = 0 Then
                Exit Sub
            End If
            Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@id", SqlDbType.Int, cbClabe.SelectedValue)
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
                BaseII.Inserta("AsignaClabe")
                'FrmClientes.tbClabe.Visible = True
                'FrmClientes.tbClabe.Text = cbClabe.Text
                'FrmClientes.Label62.Visible = True
                bitsist(GloUsuario, Contrato, "Softv", "Clientes", "Asignación de Cuenta Clabe", "", "Cuenta clabe asignada: " + cbClabe.Text, "AG")
                Me.Close()
            Catch ex As Exception

            End Try
        Else
            Me.Close()
        End If
    End Sub
End Class