﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelCompaniaCarteraPorPlaza
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.seleccion = New System.Windows.Forms.ListBox()
        Me.quitartodo = New System.Windows.Forms.Button()
        Me.agregartodo = New System.Windows.Forms.Button()
        Me.quitar = New System.Windows.Forms.Button()
        Me.agregar = New System.Windows.Forms.Button()
        Me.loquehay = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBoxPlaza = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(537, 399)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 41)
        Me.Button6.TabIndex = 129
        Me.Button6.Text = "ACEPTAR"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(739, 399)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 128
        Me.Button5.Text = "SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(43, 25)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(195, 20)
        Me.Label1.TabIndex = 124
        Me.Label1.Text = "Selecciona las Plazas"
        '
        'seleccion
        '
        Me.seleccion.DisplayMember = "razon_social"
        Me.seleccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.seleccion.FormattingEnabled = True
        Me.seleccion.ItemHeight = 20
        Me.seleccion.Location = New System.Drawing.Point(587, 65)
        Me.seleccion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.seleccion.Name = "seleccion"
        Me.seleccion.Size = New System.Drawing.Size(332, 304)
        Me.seleccion.TabIndex = 123
        Me.seleccion.ValueMember = "idcompania"
        '
        'quitartodo
        '
        Me.quitartodo.BackColor = System.Drawing.Color.DarkRed
        Me.quitartodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.quitartodo.ForeColor = System.Drawing.Color.White
        Me.quitartodo.Location = New System.Drawing.Point(388, 265)
        Me.quitartodo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.quitartodo.Name = "quitartodo"
        Me.quitartodo.Size = New System.Drawing.Size(191, 37)
        Me.quitartodo.TabIndex = 122
        Me.quitartodo.Text = "<< Quitar To&do "
        Me.quitartodo.UseVisualStyleBackColor = False
        '
        'agregartodo
        '
        Me.agregartodo.BackColor = System.Drawing.Color.DarkRed
        Me.agregartodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.agregartodo.ForeColor = System.Drawing.Color.White
        Me.agregartodo.Location = New System.Drawing.Point(388, 174)
        Me.agregartodo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.agregartodo.Name = "agregartodo"
        Me.agregartodo.Size = New System.Drawing.Size(191, 37)
        Me.agregartodo.TabIndex = 121
        Me.agregartodo.Text = "Agregar &Todo >>"
        Me.agregartodo.UseVisualStyleBackColor = False
        '
        'quitar
        '
        Me.quitar.BackColor = System.Drawing.Color.DarkRed
        Me.quitar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.quitar.ForeColor = System.Drawing.Color.White
        Me.quitar.Location = New System.Drawing.Point(388, 219)
        Me.quitar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.quitar.Name = "quitar"
        Me.quitar.Size = New System.Drawing.Size(191, 37)
        Me.quitar.TabIndex = 120
        Me.quitar.Text = "< &Quitar"
        Me.quitar.UseVisualStyleBackColor = False
        '
        'agregar
        '
        Me.agregar.BackColor = System.Drawing.Color.DarkRed
        Me.agregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.agregar.ForeColor = System.Drawing.Color.White
        Me.agregar.Location = New System.Drawing.Point(388, 129)
        Me.agregar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.agregar.Name = "agregar"
        Me.agregar.Size = New System.Drawing.Size(191, 37)
        Me.agregar.TabIndex = 119
        Me.agregar.Text = "&Agregar >"
        Me.agregar.UseVisualStyleBackColor = False
        '
        'loquehay
        '
        Me.loquehay.DisplayMember = "razon_social"
        Me.loquehay.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.loquehay.FormattingEnabled = True
        Me.loquehay.ItemHeight = 20
        Me.loquehay.Location = New System.Drawing.Point(40, 65)
        Me.loquehay.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.loquehay.Name = "loquehay"
        Me.loquehay.Size = New System.Drawing.Size(332, 304)
        Me.loquehay.TabIndex = 0
        Me.loquehay.ValueMember = "idcompania"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(43, 80)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 20)
        Me.Label2.TabIndex = 130
        Me.Label2.Text = "Plaza"
        Me.Label2.Visible = False
        '
        'ComboBoxPlaza
        '
        Me.ComboBoxPlaza.DisplayMember = "Nombre"
        Me.ComboBoxPlaza.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxPlaza.FormattingEnabled = True
        Me.ComboBoxPlaza.Location = New System.Drawing.Point(113, 65)
        Me.ComboBoxPlaza.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxPlaza.Name = "ComboBoxPlaza"
        Me.ComboBoxPlaza.Size = New System.Drawing.Size(448, 33)
        Me.ComboBoxPlaza.TabIndex = 131
        Me.ComboBoxPlaza.ValueMember = "Clv_Plaza"
        Me.ComboBoxPlaza.Visible = False
        '
        'FrmSelCompaniaCarteraPorPlaza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(955, 466)
        Me.Controls.Add(Me.ComboBoxPlaza)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.seleccion)
        Me.Controls.Add(Me.quitartodo)
        Me.Controls.Add(Me.agregartodo)
        Me.Controls.Add(Me.quitar)
        Me.Controls.Add(Me.agregar)
        Me.Controls.Add(Me.loquehay)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelCompaniaCarteraPorPlaza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección Plazas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents seleccion As System.Windows.Forms.ListBox
    Friend WithEvents quitartodo As System.Windows.Forms.Button
    Friend WithEvents agregartodo As System.Windows.Forms.Button
    Friend WithEvents quitar As System.Windows.Forms.Button
    Friend WithEvents agregar As System.Windows.Forms.Button
    Friend WithEvents loquehay As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxPlaza As System.Windows.Forms.ComboBox
End Class
