﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRentaPromociones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label10 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ComboBoxServicios = New System.Windows.Forms.ComboBox()
        Me.tbAdicional1 = New System.Windows.Forms.TextBox()
        Me.TextBoxPrincipal = New System.Windows.Forms.TextBox()
        Me.tbAdicional2 = New System.Windows.Forms.TextBox()
        Me.cbModelo = New System.Windows.Forms.ComboBox()
        Me.AgregarServicio = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Modelo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdArticulo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Principal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Adicional1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Adicional2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.ComboBoxTipSer = New System.Windows.Forms.ComboBox()
        Label10 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(41, 92)
        Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(79, 18)
        Label10.TabIndex = 72
        Label10.Text = "Servicio :"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(667, 156)
        Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(104, 18)
        Label16.TabIndex = 88
        Label16.Text = "Adicional #2 "
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Label12.Location = New System.Drawing.Point(336, 156)
        Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(78, 18)
        Label12.TabIndex = 86
        Label12.Text = "Principal "
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(505, 158)
        Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(104, 18)
        Label14.TabIndex = 87
        Label14.Text = "Adicional #1 "
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(41, 156)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(64, 18)
        Label1.TabIndex = 89
        Label1.Text = "Modelo"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(39, 21)
        Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(140, 18)
        Label7.TabIndex = 96
        Label7.Text = "Tipo de Servicio :"
        '
        'ComboBoxServicios
        '
        Me.ComboBoxServicios.DisplayMember = "Descripcion"
        Me.ComboBoxServicios.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxServicios.FormattingEnabled = True
        Me.ComboBoxServicios.Location = New System.Drawing.Point(43, 114)
        Me.ComboBoxServicios.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxServicios.Name = "ComboBoxServicios"
        Me.ComboBoxServicios.Size = New System.Drawing.Size(436, 30)
        Me.ComboBoxServicios.TabIndex = 71
        Me.ComboBoxServicios.ValueMember = "Clv_Servicio"
        '
        'tbAdicional1
        '
        Me.tbAdicional1.BackColor = System.Drawing.Color.White
        Me.tbAdicional1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbAdicional1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbAdicional1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbAdicional1.Location = New System.Drawing.Point(505, 181)
        Me.tbAdicional1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbAdicional1.MaxLength = 255
        Me.tbAdicional1.Name = "tbAdicional1"
        Me.tbAdicional1.Size = New System.Drawing.Size(157, 24)
        Me.tbAdicional1.TabIndex = 84
        '
        'TextBoxPrincipal
        '
        Me.TextBoxPrincipal.BackColor = System.Drawing.Color.White
        Me.TextBoxPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxPrincipal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxPrincipal.Location = New System.Drawing.Point(340, 181)
        Me.TextBoxPrincipal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxPrincipal.MaxLength = 255
        Me.TextBoxPrincipal.Name = "TextBoxPrincipal"
        Me.TextBoxPrincipal.Size = New System.Drawing.Size(157, 24)
        Me.TextBoxPrincipal.TabIndex = 81
        '
        'tbAdicional2
        '
        Me.tbAdicional2.BackColor = System.Drawing.Color.White
        Me.tbAdicional2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbAdicional2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbAdicional2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbAdicional2.Location = New System.Drawing.Point(671, 181)
        Me.tbAdicional2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbAdicional2.MaxLength = 255
        Me.tbAdicional2.Name = "tbAdicional2"
        Me.tbAdicional2.Size = New System.Drawing.Size(157, 24)
        Me.tbAdicional2.TabIndex = 85
        '
        'cbModelo
        '
        Me.cbModelo.DisplayMember = "Nombre"
        Me.cbModelo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbModelo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbModelo.FormattingEnabled = True
        Me.cbModelo.Location = New System.Drawing.Point(43, 178)
        Me.cbModelo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbModelo.Name = "cbModelo"
        Me.cbModelo.Size = New System.Drawing.Size(281, 26)
        Me.cbModelo.TabIndex = 90
        Me.cbModelo.ValueMember = "IdArticulo"
        '
        'AgregarServicio
        '
        Me.AgregarServicio.BackColor = System.Drawing.Color.DarkOrange
        Me.AgregarServicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AgregarServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AgregarServicio.ForeColor = System.Drawing.Color.Black
        Me.AgregarServicio.Location = New System.Drawing.Point(843, 172)
        Me.AgregarServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.AgregarServicio.Name = "AgregarServicio"
        Me.AgregarServicio.Size = New System.Drawing.Size(111, 34)
        Me.AgregarServicio.TabIndex = 91
        Me.AgregarServicio.Text = "AGREGAR"
        Me.AgregarServicio.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Modelo, Me.IdArticulo, Me.Principal, Me.Adicional1, Me.Adicional2})
        Me.DataGridView1.Location = New System.Drawing.Point(45, 233)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(783, 135)
        Me.DataGridView1.TabIndex = 92
        '
        'Modelo
        '
        Me.Modelo.DataPropertyName = "Nombre"
        Me.Modelo.HeaderText = "Modelo"
        Me.Modelo.Name = "Modelo"
        Me.Modelo.ReadOnly = True
        Me.Modelo.Width = 220
        '
        'IdArticulo
        '
        Me.IdArticulo.DataPropertyName = "IdArticulo"
        Me.IdArticulo.HeaderText = "IdArticulo"
        Me.IdArticulo.Name = "IdArticulo"
        Me.IdArticulo.ReadOnly = True
        Me.IdArticulo.Visible = False
        '
        'Principal
        '
        Me.Principal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Principal.DataPropertyName = "Principal"
        Me.Principal.HeaderText = "Principal"
        Me.Principal.Name = "Principal"
        Me.Principal.ReadOnly = True
        '
        'Adicional1
        '
        Me.Adicional1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Adicional1.DataPropertyName = "Adicional1"
        Me.Adicional1.HeaderText = "Adicional #1"
        Me.Adicional1.Name = "Adicional1"
        Me.Adicional1.ReadOnly = True
        '
        'Adicional2
        '
        Me.Adicional2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Adicional2.DataPropertyName = "Adicional2"
        Me.Adicional2.HeaderText = "Adicional #2"
        Me.Adicional2.Name = "Adicional2"
        Me.Adicional2.ReadOnly = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(843, 233)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(111, 34)
        Me.Button1.TabIndex = 93
        Me.Button1.Text = "ELIMINAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(843, 383)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(111, 34)
        Me.Button2.TabIndex = 94
        Me.Button2.Text = "SALIR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'ComboBoxTipSer
        '
        Me.ComboBoxTipSer.DisplayMember = "Concepto"
        Me.ComboBoxTipSer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxTipSer.FormattingEnabled = True
        Me.ComboBoxTipSer.Location = New System.Drawing.Point(43, 43)
        Me.ComboBoxTipSer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxTipSer.Name = "ComboBoxTipSer"
        Me.ComboBoxTipSer.Size = New System.Drawing.Size(436, 30)
        Me.ComboBoxTipSer.TabIndex = 95
        Me.ComboBoxTipSer.ValueMember = "Clv_TipSerPrincipal"
        '
        'FrmRentaPromociones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(976, 431)
        Me.Controls.Add(Me.ComboBoxTipSer)
        Me.Controls.Add(Label7)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.AgregarServicio)
        Me.Controls.Add(Me.cbModelo)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.tbAdicional1)
        Me.Controls.Add(Me.TextBoxPrincipal)
        Me.Controls.Add(Label16)
        Me.Controls.Add(Me.tbAdicional2)
        Me.Controls.Add(Label12)
        Me.Controls.Add(Label14)
        Me.Controls.Add(Label10)
        Me.Controls.Add(Me.ComboBoxServicios)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmRentaPromociones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Costo de Renta STB de Promoción"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ComboBoxServicios As System.Windows.Forms.ComboBox
    Friend WithEvents tbAdicional1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents tbAdicional2 As System.Windows.Forms.TextBox
    Friend WithEvents cbModelo As System.Windows.Forms.ComboBox
    Friend WithEvents AgregarServicio As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Modelo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdArticulo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Principal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Adicional1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Adicional2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComboBoxTipSer As System.Windows.Forms.ComboBox
End Class
