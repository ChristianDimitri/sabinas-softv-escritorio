﻿Imports System.Data.SqlClient
Public Class FrmEstados

    Private Sub FrmEstados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If OpcionEstado = "N" Then
            BindingNavigatorDeleteItem.Enabled = False
        ElseIf OpcionEstado = "C" Then
            tbclvestado.Enabled = False
            tbnombre.Enabled = False
            CONSERVICIOSBindingNavigator.Enabled = False
            LlenaDatosGenerales()
        ElseIf OpcionEstado = "M" Then
            BindingNavigatorDeleteItem.Enabled = True
            LlenaDatosGenerales()
        End If
    End Sub
    Private Sub LlenaDatosGenerales()
        Try
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            Dim comando As New SqlCommand()
            comando.Connection = conexion
            comando.CommandText = "exec DameDatosEstado " + GloEstado.ToString
            Dim reader As SqlDataReader = comando.ExecuteReader()
            If reader.Read() Then
                tbclvestado.Text = reader(0).ToString
                tbnombre.Text = reader(1).ToString
            End If
            reader.Close()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, tbnombre.Text)
        BaseII.CreateMyParameter("@mismoNombre", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.VarChar, tbclvestado.Text)
        BaseII.ProcedimientoOutPut("ValidaNombreEstado")
        Dim mismoNombre = BaseII.dicoPar("@mismoNombre")
        If mismoNombre = 1 Then
            MsgBox("Ya existe un estado con el mismo nombre.")
            Exit Sub
        End If
        If GloEstado <> 0 Then
            GloEstado = tbclvestado.Text
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_estado", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@clv_estadomod", SqlDbType.Int, GloEstado)
            BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, tbnombre.Text.Trim)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
            BaseII.ProcedimientoOutPut("ABCEstados")
            If GloEstado = 0 Then
                tbclvestado.Text = BaseII.dicoPar("@clv_estado").ToString
                GloEstado = tbclvestado.Text
            End If
            MsgBox("Datos guardados correctamente.")
            BrwEstados.entraActivate = 1
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_estado", ParameterDirection.Output, SqlDbType.Int, GloEstado)
        BaseII.CreateMyParameter("@clv_estadomod", SqlDbType.Int, GloEstado)
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.ProcedimientoOutPut("ABCEstados")
        Dim res As Integer = BaseII.dicoPar("@clv_estado")
        If res = 999 Then
            MsgBox("No se puede eliminar, ya hay clientes registrados en el estado o aún tiene relacion con alguna ciudad.")
        ElseIf res = -1 Then
            MsgBox("Registro eliminado exitosamente.")
            BrwEstados.entraActivate = 1
            Me.Close()
        End If
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub
End Class