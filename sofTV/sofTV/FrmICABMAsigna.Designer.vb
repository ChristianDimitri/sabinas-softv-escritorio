<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmICABMAsigna
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CLV_CABLEMODEMLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmICABMAsigna))
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.CONICABMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.Clv_OrdenTextBox = New System.Windows.Forms.TextBox()
        Me.ContratonetTextBox = New System.Windows.Forms.TextBox()
        Me.Contratonet1 = New System.Windows.Forms.TextBox()
        Me.MacCablemodem1 = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.MacCableModemTextBox = New System.Windows.Forms.TextBox()
        Me.CLV_CABLEMODEMTextBox = New System.Windows.Forms.TextBox()
        Me.CONICABMBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONICABMBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.NoFuncionaCheckBox = New System.Windows.Forms.CheckBox()
        Me.CONCCABMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CLV_CABLEMODEMNewTextBox = New System.Windows.Forms.TextBox()
        Me.MaccablemodemNew = New System.Windows.Forms.TextBox()
        Me.Contratonet2 = New System.Windows.Forms.TextBox()
        Me.MacCablemodem2 = New System.Windows.Forms.TextBox()
        Me.CONCCABMBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONCCABMBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.MUESTRACABLEMODEMSdisponiblesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONICABMTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONICABMTableAdapter()
        Me.MUESTRACABLEMODEMS_disponiblesTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMS_disponiblesTableAdapter()
        Me.MUESTRAICAMporSOLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAICAM_porSOLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRAICAM_porSOLTableAdapter()
        Me.MUESTRACONTNETBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTNETTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter()
        Me.CONCCABMTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCCABMTableAdapter()
        Me.MUESTRACABLEMODEMSDELCLIporOpcionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter()
        Me.NUECCABMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUECCABMTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUECCABMTableAdapter()
        Me.BORDetOrdSer_INTELIGENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSer_INTELIGENTETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter()
        Me.BORCCABMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORCCABMTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORCCABMTableAdapter()
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.MUESTRACONTTELBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTTELTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.MUESTRACONTTELTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CLV_CABLEMODEMLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        CType(Me.CONICABMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CONICABMBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONICABMBindingNavigator.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.CONCCABMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONCCABMBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCCABMBindingNavigator.SuspendLayout()
        CType(Me.MUESTRACABLEMODEMSdisponiblesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAICAMporSOLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTNETBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIporOpcionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUECCABMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORCCABMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTTELBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CLV_CABLEMODEMLabel
        '
        CLV_CABLEMODEMLabel.AutoSize = True
        CLV_CABLEMODEMLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CLV_CABLEMODEMLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CLV_CABLEMODEMLabel.Location = New System.Drawing.Point(29, 103)
        CLV_CABLEMODEMLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CLV_CABLEMODEMLabel.Name = "CLV_CABLEMODEMLabel"
        CLV_CABLEMODEMLabel.Size = New System.Drawing.Size(175, 18)
        CLV_CABLEMODEMLabel.TabIndex = 24
        CLV_CABLEMODEMLabel.Text = "Cablemodem Nuevo : "
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(29, 44)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Label1.Size = New System.Drawing.Size(93, 18)
        Label1.TabIndex = 29
        Label1.Text = "Asignar a  :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(29, 44)
        Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label3.Name = "Label3"
        Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Label3.Size = New System.Drawing.Size(173, 18)
        Label3.TabIndex = 29
        Label3.Text = "Cablemodem Actual  :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(29, 176)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Label2.Size = New System.Drawing.Size(170, 18)
        Label2.TabIndex = 34
        Label2.Text = "Cablemodem Nuevo :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(12, 14)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(232, 20)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Por Asignar Cablemodems"
        '
        'TreeView2
        '
        Me.TreeView2.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView2.ForeColor = System.Drawing.Color.Navy
        Me.TreeView2.Location = New System.Drawing.Point(16, 37)
        Me.TreeView2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(404, 322)
        Me.TreeView2.TabIndex = 15
        Me.TreeView2.TabStop = False
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONICABMBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(171, 165)
        Me.ClaveTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(132, 22)
        Me.ClaveTextBox.TabIndex = 19
        Me.ClaveTextBox.TabStop = False
        '
        'CONICABMBindingSource
        '
        Me.CONICABMBindingSource.DataMember = "CONICABM"
        Me.CONICABMBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Clv_OrdenTextBox
        '
        Me.Clv_OrdenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONICABMBindingSource, "Clv_Orden", True))
        Me.Clv_OrdenTextBox.Location = New System.Drawing.Point(153, 165)
        Me.Clv_OrdenTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_OrdenTextBox.Name = "Clv_OrdenTextBox"
        Me.Clv_OrdenTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clv_OrdenTextBox.TabIndex = 21
        Me.Clv_OrdenTextBox.TabStop = False
        '
        'ContratonetTextBox
        '
        Me.ContratonetTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONICABMBindingSource, "Contratonet", True))
        Me.ContratonetTextBox.Location = New System.Drawing.Point(183, 165)
        Me.ContratonetTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ContratonetTextBox.Name = "ContratonetTextBox"
        Me.ContratonetTextBox.Size = New System.Drawing.Size(132, 22)
        Me.ContratonetTextBox.TabIndex = 23
        Me.ContratonetTextBox.TabStop = False
        '
        'Contratonet1
        '
        Me.Contratonet1.Location = New System.Drawing.Point(153, 165)
        Me.Contratonet1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Contratonet1.Name = "Contratonet1"
        Me.Contratonet1.Size = New System.Drawing.Size(148, 22)
        Me.Contratonet1.TabIndex = 26
        Me.Contratonet1.TabStop = False
        '
        'MacCablemodem1
        '
        Me.MacCablemodem1.BackColor = System.Drawing.Color.Gainsboro
        Me.MacCablemodem1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MacCablemodem1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MacCablemodem1.ForeColor = System.Drawing.Color.DarkRed
        Me.MacCablemodem1.Location = New System.Drawing.Point(153, 44)
        Me.MacCablemodem1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MacCablemodem1.Name = "MacCablemodem1"
        Me.MacCablemodem1.Size = New System.Drawing.Size(319, 23)
        Me.MacCablemodem1.TabIndex = 27
        Me.MacCablemodem1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.MacCableModemTextBox)
        Me.Panel1.Controls.Add(Me.CLV_CABLEMODEMTextBox)
        Me.Panel1.Controls.Add(Me.Contratonet1)
        Me.Panel1.Controls.Add(Label1)
        Me.Panel1.Controls.Add(Me.ContratonetTextBox)
        Me.Panel1.Controls.Add(Me.Clv_OrdenTextBox)
        Me.Panel1.Controls.Add(Me.MacCablemodem1)
        Me.Panel1.Controls.Add(Me.ClaveTextBox)
        Me.Panel1.Controls.Add(CLV_CABLEMODEMLabel)
        Me.Panel1.Controls.Add(Me.CONICABMBindingNavigator)
        Me.Panel1.Location = New System.Drawing.Point(440, 41)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel1.Size = New System.Drawing.Size(575, 219)
        Me.Panel1.TabIndex = 29
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.Control
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(324, 153)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(231, 37)
        Me.Button3.TabIndex = 40
        Me.Button3.TabStop = False
        Me.Button3.Text = "CABLEMODEM PROPIO"
        Me.Button3.UseVisualStyleBackColor = False
        Me.Button3.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(17, 153)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(299, 37)
        Me.Button2.TabIndex = 38
        Me.Button2.TabStop = False
        Me.Button2.Text = "CABLEMODEMS DISPONIBLES"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'MacCableModemTextBox
        '
        Me.MacCableModemTextBox.BackColor = System.Drawing.Color.Gainsboro
        Me.MacCableModemTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MacCableModemTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONICABMBindingSource, "MacCableModem", True))
        Me.MacCableModemTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MacCableModemTextBox.ForeColor = System.Drawing.Color.DarkRed
        Me.MacCableModemTextBox.Location = New System.Drawing.Point(239, 98)
        Me.MacCableModemTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MacCableModemTextBox.Name = "MacCableModemTextBox"
        Me.MacCableModemTextBox.Size = New System.Drawing.Size(299, 23)
        Me.MacCableModemTextBox.TabIndex = 39
        Me.MacCableModemTextBox.TabStop = False
        '
        'CLV_CABLEMODEMTextBox
        '
        Me.CLV_CABLEMODEMTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONICABMBindingSource, "CLV_CABLEMODEM", True))
        Me.CLV_CABLEMODEMTextBox.Location = New System.Drawing.Point(153, 165)
        Me.CLV_CABLEMODEMTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CLV_CABLEMODEMTextBox.Name = "CLV_CABLEMODEMTextBox"
        Me.CLV_CABLEMODEMTextBox.Size = New System.Drawing.Size(132, 22)
        Me.CLV_CABLEMODEMTextBox.TabIndex = 30
        Me.CLV_CABLEMODEMTextBox.TabStop = False
        '
        'CONICABMBindingNavigator
        '
        Me.CONICABMBindingNavigator.AddNewItem = Nothing
        Me.CONICABMBindingNavigator.BindingSource = Me.CONICABMBindingSource
        Me.CONICABMBindingNavigator.CountItem = Nothing
        Me.CONICABMBindingNavigator.DeleteItem = Nothing
        Me.CONICABMBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONICABMBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONICABMBindingNavigatorSaveItem})
        Me.CONICABMBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONICABMBindingNavigator.MoveFirstItem = Nothing
        Me.CONICABMBindingNavigator.MoveLastItem = Nothing
        Me.CONICABMBindingNavigator.MoveNextItem = Nothing
        Me.CONICABMBindingNavigator.MovePreviousItem = Nothing
        Me.CONICABMBindingNavigator.Name = "CONICABMBindingNavigator"
        Me.CONICABMBindingNavigator.PositionItem = Nothing
        Me.CONICABMBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONICABMBindingNavigator.Size = New System.Drawing.Size(575, 28)
        Me.CONICABMBindingNavigator.TabIndex = 18
        Me.CONICABMBindingNavigator.Text = "BindingNavigator1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(203, 25)
        Me.ToolStripButton1.Text = "BORRAR Cablemodem"
        '
        'CONICABMBindingNavigatorSaveItem
        '
        Me.CONICABMBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONICABMBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONICABMBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONICABMBindingNavigatorSaveItem.Name = "CONICABMBindingNavigatorSaveItem"
        Me.CONICABMBindingNavigatorSaveItem.Size = New System.Drawing.Size(112, 25)
        Me.CONICABMBindingNavigatorSaveItem.Text = "&ACEPTAR"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(833, 374)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.Button4)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.CheckBox1)
        Me.Panel2.Controls.Add(Me.NoFuncionaCheckBox)
        Me.Panel2.Controls.Add(Me.CLV_CABLEMODEMNewTextBox)
        Me.Panel2.Controls.Add(Me.MaccablemodemNew)
        Me.Panel2.Controls.Add(Label2)
        Me.Panel2.Controls.Add(Me.Contratonet2)
        Me.Panel2.Controls.Add(Label3)
        Me.Panel2.Controls.Add(Me.MacCablemodem2)
        Me.Panel2.Controls.Add(Me.CONCCABMBindingNavigator)
        Me.Panel2.Location = New System.Drawing.Point(440, 37)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel2.Size = New System.Drawing.Size(587, 322)
        Me.Panel2.TabIndex = 31
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.SystemColors.Control
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(324, 255)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(231, 37)
        Me.Button4.TabIndex = 41
        Me.Button4.TabStop = False
        Me.Button4.Text = "CABLEMODEM PROPIO"
        Me.Button4.UseVisualStyleBackColor = False
        Me.Button4.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(17, 255)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(299, 37)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&CABLEMODEMS DISPONIBLES"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBox1.Location = New System.Drawing.Point(249, 91)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(132, 22)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Esta Dañado "
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'NoFuncionaCheckBox
        '
        Me.NoFuncionaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONCCABMBindingSource, "NoFunciona", True))
        Me.NoFuncionaCheckBox.Location = New System.Drawing.Point(52, 261)
        Me.NoFuncionaCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NoFuncionaCheckBox.Name = "NoFuncionaCheckBox"
        Me.NoFuncionaCheckBox.Size = New System.Drawing.Size(93, 30)
        Me.NoFuncionaCheckBox.TabIndex = 39
        Me.NoFuncionaCheckBox.TabStop = False
        '
        'CONCCABMBindingSource
        '
        Me.CONCCABMBindingSource.DataMember = "CONCCABM"
        Me.CONCCABMBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CLV_CABLEMODEMNewTextBox
        '
        Me.CLV_CABLEMODEMNewTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCCABMBindingSource, "CLV_CABLEMODEMNew", True))
        Me.CLV_CABLEMODEMNewTextBox.Location = New System.Drawing.Point(92, 261)
        Me.CLV_CABLEMODEMNewTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CLV_CABLEMODEMNewTextBox.Name = "CLV_CABLEMODEMNewTextBox"
        Me.CLV_CABLEMODEMNewTextBox.Size = New System.Drawing.Size(132, 22)
        Me.CLV_CABLEMODEMNewTextBox.TabIndex = 36
        Me.CLV_CABLEMODEMNewTextBox.TabStop = False
        '
        'MaccablemodemNew
        '
        Me.MaccablemodemNew.BackColor = System.Drawing.Color.Gainsboro
        Me.MaccablemodemNew.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MaccablemodemNew.CausesValidation = False
        Me.MaccablemodemNew.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCCABMBindingSource, "Maccablemodem2", True))
        Me.MaccablemodemNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaccablemodemNew.ForeColor = System.Drawing.Color.DarkRed
        Me.MaccablemodemNew.Location = New System.Drawing.Point(231, 177)
        Me.MaccablemodemNew.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaccablemodemNew.Name = "MaccablemodemNew"
        Me.MaccablemodemNew.ReadOnly = True
        Me.MaccablemodemNew.Size = New System.Drawing.Size(319, 23)
        Me.MaccablemodemNew.TabIndex = 1
        '
        'Contratonet2
        '
        Me.Contratonet2.Location = New System.Drawing.Point(81, 261)
        Me.Contratonet2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Contratonet2.Name = "Contratonet2"
        Me.Contratonet2.Size = New System.Drawing.Size(63, 22)
        Me.Contratonet2.TabIndex = 32
        Me.Contratonet2.TabStop = False
        '
        'MacCablemodem2
        '
        Me.MacCablemodem2.BackColor = System.Drawing.Color.Gainsboro
        Me.MacCablemodem2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MacCablemodem2.CausesValidation = False
        Me.MacCablemodem2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCCABMBindingSource, "Maccablemodem1", True))
        Me.MacCablemodem2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MacCablemodem2.ForeColor = System.Drawing.Color.DarkRed
        Me.MacCablemodem2.Location = New System.Drawing.Point(231, 44)
        Me.MacCablemodem2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MacCablemodem2.Name = "MacCablemodem2"
        Me.MacCablemodem2.ReadOnly = True
        Me.MacCablemodem2.Size = New System.Drawing.Size(319, 23)
        Me.MacCablemodem2.TabIndex = 100
        Me.MacCablemodem2.TabStop = False
        '
        'CONCCABMBindingNavigator
        '
        Me.CONCCABMBindingNavigator.AddNewItem = Nothing
        Me.CONCCABMBindingNavigator.BindingSource = Me.CONCCABMBindingSource
        Me.CONCCABMBindingNavigator.CountItem = Nothing
        Me.CONCCABMBindingNavigator.DeleteItem = Nothing
        Me.CONCCABMBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONCCABMBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONCCABMBindingNavigatorSaveItem})
        Me.CONCCABMBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCCABMBindingNavigator.MoveFirstItem = Nothing
        Me.CONCCABMBindingNavigator.MoveLastItem = Nothing
        Me.CONCCABMBindingNavigator.MoveNextItem = Nothing
        Me.CONCCABMBindingNavigator.MovePreviousItem = Nothing
        Me.CONCCABMBindingNavigator.Name = "CONCCABMBindingNavigator"
        Me.CONCCABMBindingNavigator.PositionItem = Nothing
        Me.CONCCABMBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONCCABMBindingNavigator.Size = New System.Drawing.Size(587, 28)
        Me.CONCCABMBindingNavigator.TabIndex = 3
        Me.CONCCABMBindingNavigator.TabStop = True
        Me.CONCCABMBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(333, 25)
        Me.BindingNavigatorDeleteItem.Text = "&BORRAR CABLEMODEM ASIGNADO"
        '
        'CONCCABMBindingNavigatorSaveItem
        '
        Me.CONCCABMBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCCABMBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCCABMBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCCABMBindingNavigatorSaveItem.Name = "CONCCABMBindingNavigatorSaveItem"
        Me.CONCCABMBindingNavigatorSaveItem.Size = New System.Drawing.Size(112, 25)
        Me.CONCCABMBindingNavigatorSaveItem.Text = "&ACEPTAR"
        '
        'MUESTRACABLEMODEMSdisponiblesBindingSource
        '
        Me.MUESTRACABLEMODEMSdisponiblesBindingSource.DataMember = "MUESTRACABLEMODEMS_disponibles"
        Me.MUESTRACABLEMODEMSdisponiblesBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONICABMTableAdapter
        '
        Me.CONICABMTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACABLEMODEMS_disponiblesTableAdapter
        '
        Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAICAMporSOLBindingSource
        '
        Me.MUESTRAICAMporSOLBindingSource.DataMember = "MUESTRAICAM_porSOL"
        Me.MUESTRAICAMporSOLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRAICAM_porSOLTableAdapter
        '
        Me.MUESTRAICAM_porSOLTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTNETBindingSource
        '
        Me.MUESTRACONTNETBindingSource.DataMember = "MUESTRACONTNET"
        Me.MUESTRACONTNETBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACONTNETTableAdapter
        '
        Me.MUESTRACONTNETTableAdapter.ClearBeforeFill = True
        '
        'CONCCABMTableAdapter
        '
        Me.CONCCABMTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACABLEMODEMSDELCLIporOpcionBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLIporOpcionBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI_porOpcion"
        Me.MUESTRACABLEMODEMSDELCLIporOpcionBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.ClearBeforeFill = True
        '
        'NUECCABMBindingSource
        '
        Me.NUECCABMBindingSource.DataMember = "NUECCABM"
        Me.NUECCABMBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUECCABMTableAdapter
        '
        Me.NUECCABMTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSer_INTELIGENTEBindingSource
        '
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataMember = "BORDetOrdSer_INTELIGENTE"
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSer_INTELIGENTETableAdapter
        '
        Me.BORDetOrdSer_INTELIGENTETableAdapter.ClearBeforeFill = True
        '
        'BORCCABMBindingSource
        '
        Me.BORCCABMBindingSource.DataMember = "BORCCABM"
        Me.BORCCABMBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORCCABMTableAdapter
        '
        Me.BORCCABMTableAdapter.ClearBeforeFill = True
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRACONTTELBindingSource
        '
        Me.MUESTRACONTTELBindingSource.DataMember = "MUESTRACONTTEL"
        Me.MUESTRACONTTELBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'MUESTRACONTTELTableAdapter
        '
        Me.MUESTRACONTTELTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmICABMAsigna
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1053, 428)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TreeView2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmICABMAsigna"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asiganción de Cablemodems"
        CType(Me.CONICABMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONICABMBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONICABMBindingNavigator.ResumeLayout(False)
        Me.CONICABMBindingNavigator.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.CONCCABMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONCCABMBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCCABMBindingNavigator.ResumeLayout(False)
        Me.CONCCABMBindingNavigator.PerformLayout()
        CType(Me.MUESTRACABLEMODEMSdisponiblesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAICAMporSOLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTNETBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIporOpcionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUECCABMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORCCABMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTTELBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONICABMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONICABMTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONICABMTableAdapter
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_OrdenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratonetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Contratonet1 As System.Windows.Forms.TextBox
    Friend WithEvents MacCablemodem1 As System.Windows.Forms.TextBox
    Friend WithEvents MUESTRACABLEMODEMSdisponiblesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMS_disponiblesTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMS_disponiblesTableAdapter
    Friend WithEvents MUESTRACONTNETBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAICAMporSOLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAICAM_porSOLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAICAM_porSOLTableAdapter
    Friend WithEvents MUESTRACONTNETTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents CONICABMBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONICABMBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CLV_CABLEMODEMTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents MacCablemodem2 As System.Windows.Forms.TextBox
    Friend WithEvents Contratonet2 As System.Windows.Forms.TextBox
    Friend WithEvents MaccablemodemNew As System.Windows.Forms.TextBox
    Friend WithEvents CLV_CABLEMODEMNewTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CONCCABMBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents CONCCABMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONCCABMBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONCCABMTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCCABMTableAdapter
    Friend WithEvents NoFuncionaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents MacCableModemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MUESTRACABLEMODEMSDELCLIporOpcionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter
    Friend WithEvents NUECCABMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUECCABMTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUECCABMTableAdapter
    Friend WithEvents BORDetOrdSer_INTELIGENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSer_INTELIGENTETableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter
    Friend WithEvents BORCCABMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORCCABMTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORCCABMTableAdapter
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents MUESTRACONTTELBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTTELTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.MUESTRACONTTELTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
