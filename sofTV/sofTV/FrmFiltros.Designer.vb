﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFiltros
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label30 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Me.CheckBoxPlaza = New System.Windows.Forms.CheckBox()
        Me.CheckBoxDistri = New System.Windows.Forms.CheckBox()
        Me.CheckBoxEstado = New System.Windows.Forms.CheckBox()
        Me.CheckBoxMun = New System.Windows.Forms.CheckBox()
        Me.CheckBoxLoc = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCol = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCal = New System.Windows.Forms.CheckBox()
        Me.CheckBoxTCliente = New System.Windows.Forms.CheckBox()
        Me.CheckBoxServicio = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPer = New System.Windows.Forms.CheckBox()
        Me.Button34 = New System.Windows.Forms.Button()
        Label30 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.ForeColor = System.Drawing.Color.LightSlateGray
        Label30.Location = New System.Drawing.Point(48, 18)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(86, 15)
        Label30.TabIndex = 47
        Label30.Text = "Distribuidor:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(48, 46)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(47, 15)
        Label1.TabIndex = 48
        Label1.Text = "Plaza:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(48, 74)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(55, 15)
        Label2.TabIndex = 49
        Label2.Text = "Estado:"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(48, 100)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(56, 15)
        Label3.TabIndex = 50
        Label3.Text = "Ciudad:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(48, 127)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(70, 15)
        Label4.TabIndex = 51
        Label4.Text = "Localidad"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(48, 153)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(56, 15)
        Label5.TabIndex = 52
        Label5.Text = "Colonia"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(48, 231)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(108, 15)
        Label6.TabIndex = 55
        Label6.Text = "Tipo de Cliente:"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(48, 205)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(65, 15)
        Label7.TabIndex = 54
        Label7.Text = "Servicios"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(48, 178)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(44, 15)
        Label8.TabIndex = 53
        Label8.Text = "Calle:"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(48, 261)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(68, 15)
        Label9.TabIndex = 56
        Label9.Text = "Periodos:"
        '
        'CheckBoxPlaza
        '
        Me.CheckBoxPlaza.Location = New System.Drawing.Point(161, 45)
        Me.CheckBoxPlaza.Name = "CheckBoxPlaza"
        Me.CheckBoxPlaza.Size = New System.Drawing.Size(26, 18)
        Me.CheckBoxPlaza.TabIndex = 57
        Me.CheckBoxPlaza.TabStop = False
        '
        'CheckBoxDistri
        '
        Me.CheckBoxDistri.Location = New System.Drawing.Point(161, 15)
        Me.CheckBoxDistri.Name = "CheckBoxDistri"
        Me.CheckBoxDistri.Size = New System.Drawing.Size(16, 24)
        Me.CheckBoxDistri.TabIndex = 58
        Me.CheckBoxDistri.TabStop = False
        '
        'CheckBoxEstado
        '
        Me.CheckBoxEstado.Location = New System.Drawing.Point(161, 71)
        Me.CheckBoxEstado.Name = "CheckBoxEstado"
        Me.CheckBoxEstado.Size = New System.Drawing.Size(16, 24)
        Me.CheckBoxEstado.TabIndex = 59
        Me.CheckBoxEstado.TabStop = False
        '
        'CheckBoxMun
        '
        Me.CheckBoxMun.Location = New System.Drawing.Point(161, 97)
        Me.CheckBoxMun.Name = "CheckBoxMun"
        Me.CheckBoxMun.Size = New System.Drawing.Size(16, 24)
        Me.CheckBoxMun.TabIndex = 60
        Me.CheckBoxMun.TabStop = False
        '
        'CheckBoxLoc
        '
        Me.CheckBoxLoc.Location = New System.Drawing.Point(161, 124)
        Me.CheckBoxLoc.Name = "CheckBoxLoc"
        Me.CheckBoxLoc.Size = New System.Drawing.Size(16, 24)
        Me.CheckBoxLoc.TabIndex = 61
        Me.CheckBoxLoc.TabStop = False
        '
        'CheckBoxCol
        '
        Me.CheckBoxCol.Location = New System.Drawing.Point(161, 150)
        Me.CheckBoxCol.Name = "CheckBoxCol"
        Me.CheckBoxCol.Size = New System.Drawing.Size(16, 24)
        Me.CheckBoxCol.TabIndex = 62
        Me.CheckBoxCol.TabStop = False
        '
        'CheckBoxCal
        '
        Me.CheckBoxCal.Location = New System.Drawing.Point(161, 175)
        Me.CheckBoxCal.Name = "CheckBoxCal"
        Me.CheckBoxCal.Size = New System.Drawing.Size(16, 24)
        Me.CheckBoxCal.TabIndex = 63
        Me.CheckBoxCal.TabStop = False
        '
        'CheckBoxTCliente
        '
        Me.CheckBoxTCliente.Location = New System.Drawing.Point(161, 228)
        Me.CheckBoxTCliente.Name = "CheckBoxTCliente"
        Me.CheckBoxTCliente.Size = New System.Drawing.Size(16, 24)
        Me.CheckBoxTCliente.TabIndex = 64
        Me.CheckBoxTCliente.TabStop = False
        '
        'CheckBoxServicio
        '
        Me.CheckBoxServicio.Location = New System.Drawing.Point(161, 202)
        Me.CheckBoxServicio.Name = "CheckBoxServicio"
        Me.CheckBoxServicio.Size = New System.Drawing.Size(16, 24)
        Me.CheckBoxServicio.TabIndex = 65
        Me.CheckBoxServicio.TabStop = False
        '
        'CheckBoxPer
        '
        Me.CheckBoxPer.Location = New System.Drawing.Point(161, 258)
        Me.CheckBoxPer.Name = "CheckBoxPer"
        Me.CheckBoxPer.Size = New System.Drawing.Size(16, 24)
        Me.CheckBoxPer.TabIndex = 66
        Me.CheckBoxPer.TabStop = False
        '
        'Button34
        '
        Me.Button34.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button34.Location = New System.Drawing.Point(48, 298)
        Me.Button34.Name = "Button34"
        Me.Button34.Size = New System.Drawing.Size(139, 39)
        Me.Button34.TabIndex = 67
        Me.Button34.Text = "Aceptar"
        Me.Button34.UseVisualStyleBackColor = True
        '
        'FrmFiltros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(246, 350)
        Me.Controls.Add(Me.Button34)
        Me.Controls.Add(Me.CheckBoxPer)
        Me.Controls.Add(Me.CheckBoxServicio)
        Me.Controls.Add(Me.CheckBoxTCliente)
        Me.Controls.Add(Me.CheckBoxCal)
        Me.Controls.Add(Me.CheckBoxCol)
        Me.Controls.Add(Me.CheckBoxLoc)
        Me.Controls.Add(Me.CheckBoxMun)
        Me.Controls.Add(Me.CheckBoxEstado)
        Me.Controls.Add(Me.CheckBoxDistri)
        Me.Controls.Add(Me.CheckBoxPlaza)
        Me.Controls.Add(Label9)
        Me.Controls.Add(Label6)
        Me.Controls.Add(Label7)
        Me.Controls.Add(Label8)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Label30)
        Me.Name = "FrmFiltros"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona los filtros"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CheckBoxPlaza As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxDistri As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxEstado As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxMun As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxLoc As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCol As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCal As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxTCliente As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxServicio As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPer As System.Windows.Forms.CheckBox
    Friend WithEvents Button34 As System.Windows.Forms.Button
End Class
