﻿Public Class FrmOpcionReporteDatalogic

    Private Sub rbContrato_CheckedChanged(sender As Object, e As EventArgs) Handles rbContrato.CheckedChanged
        If rbContrato.Checked Then
            GroupBox2.Enabled = True
        Else
            tbContrato.Text = ""
            GroupBox2.Enabled = False
        End If
    End Sub

    Private Sub FrmOpcionReporteDatalogic_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If rbContrato.Checked And tbContrato.Text = "" Then
            MsgBox("Debe ingresar el contrato")
            Exit Sub
        End If
        Try
            If rbContrato.Checked Then
                'opcion = "contrato"
                GloOp = 1
                Dim array() As String = tbContrato.Text.Split("-")
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contratoCompania", SqlDbType.Int, array(0))
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, array(1))
                BaseII.CreateMyParameter("@contrato", ParameterDirection.Output, SqlDbType.Int)
                BaseII.ProcedimientoOutPut("sp_dameContrato")
                Contrato = BaseII.dicoPar("@contrato")
                FrmSelFechasPPE.Show()
                op = tbContrato.Text
            Else
                'opcion = "plaza"
                GloOp = 2
                FrmSelPlazaXML.Show()
            End If
        Catch ex As Exception
            Exit Sub
        End Try
        Me.Close()
    End Sub
End Class