﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambiosServicio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BtnBuscarCliente = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxtDireccion = New System.Windows.Forms.TextBox()
        Me.TxtNombre = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtContrato = New System.Windows.Forms.TextBox()
        Me.GridServiciosDelCliente = New System.Windows.Forms.DataGridView()
        Me.colContrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colContratoNet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClvTipSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClvServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClvCableModem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMacCableModem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.GridMismoServicio = New System.Windows.Forms.DataGridView()
        Me.colClvServicio1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMonto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblCamServ = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblComboCajas = New System.Windows.Forms.Label()
        Me.CajasCombo = New System.Windows.Forms.NumericUpDown()
        Me.dgrCombo = New System.Windows.Forms.DataGridView()
        Me.ColClv_Descuento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColClv_TipoCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDescripcion2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColClv_TipSer4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.PnlPorTipoServicio = New System.Windows.Forms.Panel()
        Me.UpDownTVS2 = New System.Windows.Forms.NumericUpDown()
        Me.LblTvs2 = New System.Windows.Forms.Label()
        Me.LblTvs = New System.Windows.Forms.Label()
        Me.GridDiferenteTipoServicio = New System.Windows.Forms.DataGridView()
        Me.colClv_Servicio3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescripcion1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClv_TipSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColMonto2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpDownTVS = New System.Windows.Forms.NumericUpDown()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblServicio = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.BtnGuardar = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.GridServiciosDelCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.GridMismoServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CajasCombo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgrCombo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlPorTipoServicio.SuspendLayout()
        CType(Me.UpDownTVS2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridDiferenteTipoServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UpDownTVS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BtnBuscarCliente)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TxtDireccion)
        Me.GroupBox1.Controls.Add(Me.TxtNombre)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TxtContrato)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(16, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(589, 225)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'BtnBuscarCliente
        '
        Me.BtnBuscarCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBuscarCliente.Location = New System.Drawing.Point(521, 47)
        Me.BtnBuscarCliente.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BtnBuscarCliente.Name = "BtnBuscarCliente"
        Me.BtnBuscarCliente.Size = New System.Drawing.Size(52, 28)
        Me.BtnBuscarCliente.TabIndex = 6
        Me.BtnBuscarCliente.Text = "..."
        Me.BtnBuscarCliente.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(11, 127)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 18)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Dirección :"
        '
        'TxtDireccion
        '
        Me.TxtDireccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDireccion.Location = New System.Drawing.Point(120, 117)
        Me.TxtDireccion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtDireccion.Multiline = True
        Me.TxtDireccion.Name = "TxtDireccion"
        Me.TxtDireccion.Size = New System.Drawing.Size(452, 83)
        Me.TxtDireccion.TabIndex = 4
        '
        'TxtNombre
        '
        Me.TxtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtNombre.Location = New System.Drawing.Point(120, 82)
        Me.TxtNombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtNombre.Name = "TxtNombre"
        Me.TxtNombre.Size = New System.Drawing.Size(452, 26)
        Me.TxtNombre.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(24, 92)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 18)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Nombre :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(20, 52)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 18)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Contrato :"
        '
        'TxtContrato
        '
        Me.TxtContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtContrato.Location = New System.Drawing.Point(120, 48)
        Me.TxtContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtContrato.Name = "TxtContrato"
        Me.TxtContrato.Size = New System.Drawing.Size(380, 26)
        Me.TxtContrato.TabIndex = 0
        '
        'GridServiciosDelCliente
        '
        Me.GridServiciosDelCliente.AllowUserToAddRows = False
        Me.GridServiciosDelCliente.AllowUserToDeleteRows = False
        Me.GridServiciosDelCliente.BackgroundColor = System.Drawing.SystemColors.Control
        Me.GridServiciosDelCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridServiciosDelCliente.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colContrato, Me.colContratoNet, Me.colClvTipSer, Me.colDescripcion, Me.colClvServicio, Me.colClvCableModem, Me.colMacCableModem})
        Me.GridServiciosDelCliente.Location = New System.Drawing.Point(12, 81)
        Me.GridServiciosDelCliente.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GridServiciosDelCliente.Name = "GridServiciosDelCliente"
        Me.GridServiciosDelCliente.ReadOnly = True
        Me.GridServiciosDelCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridServiciosDelCliente.Size = New System.Drawing.Size(573, 443)
        Me.GridServiciosDelCliente.TabIndex = 1
        '
        'colContrato
        '
        Me.colContrato.DataPropertyName = "Contrato"
        Me.colContrato.HeaderText = "Contrato"
        Me.colContrato.Name = "colContrato"
        Me.colContrato.ReadOnly = True
        Me.colContrato.Visible = False
        '
        'colContratoNet
        '
        Me.colContratoNet.DataPropertyName = "ContratoNet"
        Me.colContratoNet.HeaderText = "ContratoNet"
        Me.colContratoNet.Name = "colContratoNet"
        Me.colContratoNet.ReadOnly = True
        Me.colContratoNet.Visible = False
        '
        'colClvTipSer
        '
        Me.colClvTipSer.DataPropertyName = "Clv_TipSer"
        Me.colClvTipSer.HeaderText = "Clave Tipo Servicio"
        Me.colClvTipSer.Name = "colClvTipSer"
        Me.colClvTipSer.ReadOnly = True
        Me.colClvTipSer.Visible = False
        '
        'colDescripcion
        '
        Me.colDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colDescripcion.DataPropertyName = "Descripcion"
        Me.colDescripcion.HeaderText = "Descripción"
        Me.colDescripcion.Name = "colDescripcion"
        Me.colDescripcion.ReadOnly = True
        '
        'colClvServicio
        '
        Me.colClvServicio.DataPropertyName = "Clv_Servicio"
        Me.colClvServicio.HeaderText = "Clave Servicio"
        Me.colClvServicio.Name = "colClvServicio"
        Me.colClvServicio.ReadOnly = True
        Me.colClvServicio.Visible = False
        '
        'colClvCableModem
        '
        Me.colClvCableModem.DataPropertyName = "Clv_CableModem"
        Me.colClvCableModem.HeaderText = "Clave CableModem"
        Me.colClvCableModem.Name = "colClvCableModem"
        Me.colClvCableModem.ReadOnly = True
        Me.colClvCableModem.Visible = False
        '
        'colMacCableModem
        '
        Me.colMacCableModem.DataPropertyName = "MacCableModem"
        Me.colMacCableModem.HeaderText = "Aparato"
        Me.colMacCableModem.Name = "colMacCableModem"
        Me.colMacCableModem.ReadOnly = True
        Me.colMacCableModem.Width = 140
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Enabled = False
        Me.RadioButton1.Location = New System.Drawing.Point(11, 49)
        Me.RadioButton1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(232, 29)
        Me.RadioButton1.TabIndex = 3
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Cambio de Servicios"
        Me.RadioButton1.UseVisualStyleBackColor = True
        Me.RadioButton1.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Panel2)
        Me.GroupBox2.Controls.Add(Me.lblCamServ)
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Controls.Add(Me.RadioButton3)
        Me.GroupBox2.Controls.Add(Me.PnlPorTipoServicio)
        Me.GroupBox2.Controls.Add(Me.RadioButton2)
        Me.GroupBox2.Controls.Add(Me.RadioButton1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(613, 15)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(715, 775)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones de Cambio de Servicio"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.GridMismoServicio)
        Me.Panel2.Location = New System.Drawing.Point(5, 78)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(708, 226)
        Me.Panel2.TabIndex = 13
        Me.Panel2.Visible = False
        '
        'GridMismoServicio
        '
        Me.GridMismoServicio.AllowUserToAddRows = False
        Me.GridMismoServicio.AllowUserToDeleteRows = False
        Me.GridMismoServicio.BackgroundColor = System.Drawing.SystemColors.Control
        Me.GridMismoServicio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridMismoServicio.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colClvServicio1, Me.Descripcion, Me.colMonto})
        Me.GridMismoServicio.Enabled = False
        Me.GridMismoServicio.Location = New System.Drawing.Point(3, 9)
        Me.GridMismoServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GridMismoServicio.Name = "GridMismoServicio"
        Me.GridMismoServicio.ReadOnly = True
        Me.GridMismoServicio.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridMismoServicio.Size = New System.Drawing.Size(699, 194)
        Me.GridMismoServicio.TabIndex = 5
        '
        'colClvServicio1
        '
        Me.colClvServicio1.DataPropertyName = "Clv_Servicio"
        Me.colClvServicio1.HeaderText = "Clave de Servicio"
        Me.colClvServicio1.Name = "colClvServicio1"
        Me.colClvServicio1.ReadOnly = True
        Me.colClvServicio1.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'colMonto
        '
        Me.colMonto.DataPropertyName = "Monto"
        Me.colMonto.HeaderText = "Monto"
        Me.colMonto.Name = "colMonto"
        Me.colMonto.ReadOnly = True
        '
        'lblCamServ
        '
        Me.lblCamServ.AutoSize = True
        Me.lblCamServ.Enabled = False
        Me.lblCamServ.Location = New System.Drawing.Point(263, 52)
        Me.lblCamServ.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCamServ.Name = "lblCamServ"
        Me.lblCamServ.Size = New System.Drawing.Size(30, 25)
        Me.lblCamServ.TabIndex = 12
        Me.lblCamServ.Text = "..."
        Me.lblCamServ.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblComboCajas)
        Me.Panel1.Controls.Add(Me.CajasCombo)
        Me.Panel1.Controls.Add(Me.dgrCombo)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(8, 587)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(699, 182)
        Me.Panel1.TabIndex = 11
        Me.Panel1.Visible = False
        '
        'lblComboCajas
        '
        Me.lblComboCajas.AutoSize = True
        Me.lblComboCajas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComboCajas.Location = New System.Drawing.Point(24, 6)
        Me.lblComboCajas.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblComboCajas.Name = "lblComboCajas"
        Me.lblComboCajas.Size = New System.Drawing.Size(40, 18)
        Me.lblComboCajas.TabIndex = 9
        Me.lblComboCajas.Text = "No. "
        '
        'CajasCombo
        '
        Me.CajasCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CajasCombo.Location = New System.Drawing.Point(243, 4)
        Me.CajasCombo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CajasCombo.Name = "CajasCombo"
        Me.CajasCombo.Size = New System.Drawing.Size(100, 24)
        Me.CajasCombo.TabIndex = 10
        Me.CajasCombo.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'dgrCombo
        '
        Me.dgrCombo.AllowUserToAddRows = False
        Me.dgrCombo.AllowUserToDeleteRows = False
        Me.dgrCombo.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgrCombo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgrCombo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColClv_Descuento, Me.ColClv_TipoCliente, Me.ColDescripcion2, Me.ColClv_TipSer4})
        Me.dgrCombo.Location = New System.Drawing.Point(8, 36)
        Me.dgrCombo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgrCombo.Name = "dgrCombo"
        Me.dgrCombo.ReadOnly = True
        Me.dgrCombo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgrCombo.Size = New System.Drawing.Size(687, 142)
        Me.dgrCombo.TabIndex = 6
        '
        'ColClv_Descuento
        '
        Me.ColClv_Descuento.DataPropertyName = "Clv_Descuento"
        Me.ColClv_Descuento.HeaderText = "Clave Descuento"
        Me.ColClv_Descuento.Name = "ColClv_Descuento"
        Me.ColClv_Descuento.ReadOnly = True
        Me.ColClv_Descuento.Visible = False
        '
        'ColClv_TipoCliente
        '
        Me.ColClv_TipoCliente.DataPropertyName = "Clv_TipoCliente"
        Me.ColClv_TipoCliente.HeaderText = "Clvave Tipo Cliente"
        Me.ColClv_TipoCliente.Name = "ColClv_TipoCliente"
        Me.ColClv_TipoCliente.ReadOnly = True
        Me.ColClv_TipoCliente.Visible = False
        '
        'ColDescripcion2
        '
        Me.ColDescripcion2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColDescripcion2.DataPropertyName = "Descripcion"
        Me.ColDescripcion2.HeaderText = "Descripción"
        Me.ColDescripcion2.Name = "ColDescripcion2"
        Me.ColDescripcion2.ReadOnly = True
        '
        'ColClv_TipSer4
        '
        Me.ColClv_TipSer4.DataPropertyName = "Clv_TipSer"
        Me.ColClv_TipSer4.HeaderText = "Clave Tipo Servicio"
        Me.ColClv_TipSer4.Name = "ColClv_TipSer4"
        Me.ColClv_TipSer4.ReadOnly = True
        Me.ColClv_TipSer4.Visible = False
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Enabled = False
        Me.RadioButton3.Location = New System.Drawing.Point(8, 550)
        Me.RadioButton3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(228, 29)
        Me.RadioButton3.TabIndex = 10
        Me.RadioButton3.Text = "CAMBIO A COMBO"
        Me.RadioButton3.UseVisualStyleBackColor = True
        Me.RadioButton3.Visible = False
        '
        'PnlPorTipoServicio
        '
        Me.PnlPorTipoServicio.Controls.Add(Me.UpDownTVS2)
        Me.PnlPorTipoServicio.Controls.Add(Me.LblTvs2)
        Me.PnlPorTipoServicio.Controls.Add(Me.LblTvs)
        Me.PnlPorTipoServicio.Controls.Add(Me.GridDiferenteTipoServicio)
        Me.PnlPorTipoServicio.Controls.Add(Me.UpDownTVS)
        Me.PnlPorTipoServicio.Enabled = False
        Me.PnlPorTipoServicio.Location = New System.Drawing.Point(8, 341)
        Me.PnlPorTipoServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PnlPorTipoServicio.Name = "PnlPorTipoServicio"
        Me.PnlPorTipoServicio.Size = New System.Drawing.Size(699, 182)
        Me.PnlPorTipoServicio.TabIndex = 9
        '
        'UpDownTVS2
        '
        Me.UpDownTVS2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UpDownTVS2.Location = New System.Drawing.Point(592, 5)
        Me.UpDownTVS2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.UpDownTVS2.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.UpDownTVS2.Name = "UpDownTVS2"
        Me.UpDownTVS2.Size = New System.Drawing.Size(100, 24)
        Me.UpDownTVS2.TabIndex = 11
        Me.UpDownTVS2.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'LblTvs2
        '
        Me.LblTvs2.AutoSize = True
        Me.LblTvs2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTvs2.Location = New System.Drawing.Point(379, 7)
        Me.LblTvs2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LblTvs2.Name = "LblTvs2"
        Me.LblTvs2.Size = New System.Drawing.Size(40, 18)
        Me.LblTvs2.TabIndex = 10
        Me.LblTvs2.Text = "No. "
        '
        'LblTvs
        '
        Me.LblTvs.AutoSize = True
        Me.LblTvs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTvs.Location = New System.Drawing.Point(5, 7)
        Me.LblTvs.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LblTvs.Name = "LblTvs"
        Me.LblTvs.Size = New System.Drawing.Size(40, 18)
        Me.LblTvs.TabIndex = 7
        Me.LblTvs.Text = "No. "
        '
        'GridDiferenteTipoServicio
        '
        Me.GridDiferenteTipoServicio.AllowUserToAddRows = False
        Me.GridDiferenteTipoServicio.AllowUserToDeleteRows = False
        Me.GridDiferenteTipoServicio.BackgroundColor = System.Drawing.SystemColors.Control
        Me.GridDiferenteTipoServicio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridDiferenteTipoServicio.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colClv_Servicio3, Me.colDescripcion1, Me.colClv_TipSer, Me.ColMonto2})
        Me.GridDiferenteTipoServicio.Location = New System.Drawing.Point(8, 36)
        Me.GridDiferenteTipoServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GridDiferenteTipoServicio.Name = "GridDiferenteTipoServicio"
        Me.GridDiferenteTipoServicio.ReadOnly = True
        Me.GridDiferenteTipoServicio.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridDiferenteTipoServicio.Size = New System.Drawing.Size(687, 142)
        Me.GridDiferenteTipoServicio.TabIndex = 6
        '
        'colClv_Servicio3
        '
        Me.colClv_Servicio3.DataPropertyName = "Clv_Servicio"
        Me.colClv_Servicio3.HeaderText = "Clave del Servicio"
        Me.colClv_Servicio3.Name = "colClv_Servicio3"
        Me.colClv_Servicio3.ReadOnly = True
        Me.colClv_Servicio3.Visible = False
        '
        'colDescripcion1
        '
        Me.colDescripcion1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colDescripcion1.DataPropertyName = "Descripcion"
        Me.colDescripcion1.HeaderText = "Descripción"
        Me.colDescripcion1.Name = "colDescripcion1"
        Me.colDescripcion1.ReadOnly = True
        '
        'colClv_TipSer
        '
        Me.colClv_TipSer.DataPropertyName = "Clv_TipSer"
        Me.colClv_TipSer.HeaderText = "Clave Tipo de Servicio"
        Me.colClv_TipSer.Name = "colClv_TipSer"
        Me.colClv_TipSer.ReadOnly = True
        Me.colClv_TipSer.Visible = False
        '
        'ColMonto2
        '
        Me.ColMonto2.DataPropertyName = "MONTO"
        Me.ColMonto2.HeaderText = "Monto"
        Me.ColMonto2.Name = "ColMonto2"
        Me.ColMonto2.ReadOnly = True
        '
        'UpDownTVS
        '
        Me.UpDownTVS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UpDownTVS.Location = New System.Drawing.Point(200, 5)
        Me.UpDownTVS.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.UpDownTVS.Name = "UpDownTVS"
        Me.UpDownTVS.Size = New System.Drawing.Size(100, 24)
        Me.UpDownTVS.TabIndex = 8
        Me.UpDownTVS.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(8, 304)
        Me.RadioButton2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(107, 29)
        Me.RadioButton2.TabIndex = 4
        Me.RadioButton2.Text = "Cambio"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblServicio)
        Me.GroupBox3.Controls.Add(Me.GridServiciosDelCliente)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(4, 258)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Size = New System.Drawing.Size(601, 532)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Servicios con los que cuenta el Cliente :"
        '
        'lblServicio
        '
        Me.lblServicio.AutoSize = True
        Me.lblServicio.Location = New System.Drawing.Point(13, 32)
        Me.lblServicio.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblServicio.Name = "lblServicio"
        Me.lblServicio.Size = New System.Drawing.Size(30, 25)
        Me.lblServicio.TabIndex = 2
        Me.lblServicio.Text = "..."
        '
        'btnSalir
        '
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(1135, 820)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(181, 44)
        Me.btnSalir.TabIndex = 39
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'BtnGuardar
        '
        Me.BtnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnGuardar.Location = New System.Drawing.Point(16, 820)
        Me.BtnGuardar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BtnGuardar.Name = "BtnGuardar"
        Me.BtnGuardar.Size = New System.Drawing.Size(181, 44)
        Me.BtnGuardar.TabIndex = 42
        Me.BtnGuardar.Text = "&GUARDAR"
        Me.BtnGuardar.UseVisualStyleBackColor = True
        '
        'FrmCambiosServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1344, 898)
        Me.Controls.Add(Me.BtnGuardar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmCambiosServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de servicios"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.GridServiciosDelCliente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.GridMismoServicio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CajasCombo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgrCombo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlPorTipoServicio.ResumeLayout(False)
        Me.PnlPorTipoServicio.PerformLayout()
        CType(Me.UpDownTVS2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridDiferenteTipoServicio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UpDownTVS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnBuscarCliente As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TxtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents TxtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtContrato As System.Windows.Forms.TextBox
    Friend WithEvents GridServiciosDelCliente As System.Windows.Forms.DataGridView
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GridDiferenteTipoServicio As System.Windows.Forms.DataGridView
    Friend WithEvents GridMismoServicio As System.Windows.Forms.DataGridView
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents UpDownTVS As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblTvs As System.Windows.Forms.Label
    Friend WithEvents PnlPorTipoServicio As System.Windows.Forms.Panel
    Friend WithEvents colContrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colContratoNet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClvTipSer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClvServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClvCableModem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMacCableModem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents BtnGuardar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgrCombo As System.Windows.Forms.DataGridView
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents lblServicio As System.Windows.Forms.Label
    Friend WithEvents lblCamServ As System.Windows.Forms.Label
    Friend WithEvents lblComboCajas As System.Windows.Forms.Label
    Friend WithEvents CajasCombo As System.Windows.Forms.NumericUpDown
    Friend WithEvents ColClv_Descuento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColClv_TipoCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDescripcion2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColClv_TipSer4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClvServicio1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMonto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClv_Servicio3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDescripcion1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClv_TipSer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColMonto2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LblTvs2 As System.Windows.Forms.Label
    Friend WithEvents UpDownTVS2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
End Class
