﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmHub
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label7 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim NOMBRELabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Clv_TxtLabel = New System.Windows.Forms.Label()
        Me.DescripcionLabel = New System.Windows.Forms.Label()
        Me.NombreLabel = New System.Windows.Forms.Label()
        Me.Clv_SectorTextBox = New System.Windows.Forms.TextBox()
        Me.ConSector1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConSectorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clv_TxtTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.ConRelSectorColoniaDataGridView = New System.Windows.Forms.DataGridView()
        Me.ConRelSectorColonia1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConRelSectorColoniaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSectorTableAdapter = New sofTV.DataSetEricTableAdapters.ConSectorTableAdapter()
        Me.ConRelSectorColoniaTableAdapter = New sofTV.DataSetEricTableAdapters.ConRelSectorColoniaTableAdapter()
        Me.BorRelSectorColoniaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorRelSectorColoniaTableAdapter = New sofTV.DataSetEricTableAdapters.BorRelSectorColoniaTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ComboBoxEstado = New System.Windows.Forms.ComboBox()
        Me.ComboBoxCiudad = New System.Windows.Forms.ComboBox()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.NombreComboBox = New System.Windows.Forms.ComboBox()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.MuestraColoniaSec1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button3 = New System.Windows.Forms.Button()
        Me.NueRelSectorColoniaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueRelSectorColoniaTableAdapter = New sofTV.DataSetEricTableAdapters.NueRelSectorColoniaTableAdapter()
        Me.MuestraColoniaSec1TableAdapter = New sofTV.DataSetEricTableAdapters.MuestraColoniaSec1TableAdapter()
        Me.ConSector1TableAdapter = New sofTV.DataSetEricTableAdapters.ConSector1TableAdapter()
        Me.ConRelSectorColonia1TableAdapter = New sofTV.DataSetEricTableAdapters.ConRelSectorColonia1TableAdapter()
        Me.ModSector1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModSector1TableAdapter = New sofTV.DataSetEricTableAdapters.ModSector1TableAdapter()
        Me.DataSetEric1 = New sofTV.DataSetEric()
        Me.NueSector1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueSector1TableAdapter = New sofTV.DataSetEricTableAdapters.NueSector1TableAdapter()
        Me.BorSector1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorSector1TableAdapter = New sofTV.DataSetEricTableAdapters.BorSector1TableAdapter()
        Me.BorRelSectorColonia1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorRelSectorColonia1TableAdapter = New sofTV.DataSetEricTableAdapters.BorRelSectorColonia1TableAdapter()
        Me.DataSetEric2 = New sofTV.DataSetEric()
        Me.NueRelSectorColonia1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueRelSectorColonia1TableAdapter = New sofTV.DataSetEricTableAdapters.NueRelSectorColonia1TableAdapter()
        Me.Clv_RelSecCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ciudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Localidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Colonia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Colonia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Label7 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        NOMBRELabel1 = New System.Windows.Forms.Label()
        CType(Me.ConSector1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSectorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelSectorColoniaDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelSectorColonia1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelSectorColoniaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorRelSectorColoniaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.MuestraColoniaSec1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueRelSectorColoniaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModSector1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueSector1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorSector1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorRelSectorColonia1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueRelSectorColonia1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(27, 26)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(65, 16)
        Label7.TabIndex = 17
        Label7.Text = "Estado :"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(27, 71)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(65, 16)
        Label6.TabIndex = 15
        Label6.Text = "Ciudad :"
        '
        'NOMBRELabel1
        '
        NOMBRELabel1.AutoSize = True
        NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        NOMBRELabel1.ForeColor = System.Drawing.Color.LightSlateGray
        NOMBRELabel1.Location = New System.Drawing.Point(27, 114)
        NOMBRELabel1.Name = "NOMBRELabel1"
        NOMBRELabel1.Size = New System.Drawing.Size(85, 16)
        NOMBRELabel1.TabIndex = 14
        NOMBRELabel1.Text = "Localidad :"
        '
        'Clv_TxtLabel
        '
        Me.Clv_TxtLabel.AutoSize = True
        Me.Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtLabel.Location = New System.Drawing.Point(70, 66)
        Me.Clv_TxtLabel.Name = "Clv_TxtLabel"
        Me.Clv_TxtLabel.Size = New System.Drawing.Size(56, 16)
        Me.Clv_TxtLabel.TabIndex = 2
        Me.Clv_TxtLabel.Text = "Clave :"
        '
        'DescripcionLabel
        '
        Me.DescripcionLabel.AutoSize = True
        Me.DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionLabel.Location = New System.Drawing.Point(27, 91)
        Me.DescripcionLabel.Name = "DescripcionLabel"
        Me.DescripcionLabel.Size = New System.Drawing.Size(99, 16)
        Me.DescripcionLabel.TabIndex = 4
        Me.DescripcionLabel.Text = "Descripción :"
        '
        'NombreLabel
        '
        Me.NombreLabel.AutoSize = True
        Me.NombreLabel.Location = New System.Drawing.Point(25, 160)
        Me.NombreLabel.Name = "NombreLabel"
        Me.NombreLabel.Size = New System.Drawing.Size(65, 16)
        Me.NombreLabel.TabIndex = 11
        Me.NombreLabel.Text = "Colonia:"
        '
        'Clv_SectorTextBox
        '
        Me.Clv_SectorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_SectorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSector1BindingSource, "Clv_Sector", True))
        Me.Clv_SectorTextBox.Location = New System.Drawing.Point(153, 313)
        Me.Clv_SectorTextBox.Name = "Clv_SectorTextBox"
        Me.Clv_SectorTextBox.ReadOnly = True
        Me.Clv_SectorTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_SectorTextBox.TabIndex = 1
        Me.Clv_SectorTextBox.TabStop = False
        '
        'ConSector1BindingSource
        '
        Me.ConSector1BindingSource.DataMember = "ConSector1"
        Me.ConSector1BindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConSectorBindingSource
        '
        Me.ConSectorBindingSource.DataMember = "ConSector"
        Me.ConSectorBindingSource.DataSource = Me.DataSetEric
        '
        'Clv_TxtTextBox
        '
        Me.Clv_TxtTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_TxtTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_TxtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSector1BindingSource, "Clv_Txt", True))
        Me.Clv_TxtTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtTextBox.Location = New System.Drawing.Point(142, 59)
        Me.Clv_TxtTextBox.Name = "Clv_TxtTextBox"
        Me.Clv_TxtTextBox.Size = New System.Drawing.Size(127, 24)
        Me.Clv_TxtTextBox.TabIndex = 0
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSector1BindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(142, 89)
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(287, 24)
        Me.DescripcionTextBox.TabIndex = 1
        '
        'ConRelSectorColoniaDataGridView
        '
        Me.ConRelSectorColoniaDataGridView.AllowUserToAddRows = False
        Me.ConRelSectorColoniaDataGridView.AllowUserToDeleteRows = False
        Me.ConRelSectorColoniaDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConRelSectorColoniaDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConRelSectorColoniaDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_RelSecCol, Me.Ciudad, Me.Localidad, Me.Colonia, Me.Clv_Colonia})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConRelSectorColoniaDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConRelSectorColoniaDataGridView.Location = New System.Drawing.Point(6, 209)
        Me.ConRelSectorColoniaDataGridView.Name = "ConRelSectorColoniaDataGridView"
        Me.ConRelSectorColoniaDataGridView.ReadOnly = True
        Me.ConRelSectorColoniaDataGridView.RowHeadersVisible = False
        Me.ConRelSectorColoniaDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConRelSectorColoniaDataGridView.Size = New System.Drawing.Size(509, 248)
        Me.ConRelSectorColoniaDataGridView.TabIndex = 7
        Me.ConRelSectorColoniaDataGridView.TabStop = False
        '
        'ConRelSectorColonia1BindingSource
        '
        Me.ConRelSectorColonia1BindingSource.DataMember = "ConRelSectorColonia1"
        Me.ConRelSectorColonia1BindingSource.DataSource = Me.DataSetEric
        '
        'ConRelSectorColoniaBindingSource
        '
        Me.ConRelSectorColoniaBindingSource.DataMember = "ConRelSectorColonia"
        Me.ConRelSectorColoniaBindingSource.DataSource = Me.DataSetEric
        '
        'ConSectorTableAdapter
        '
        Me.ConSectorTableAdapter.ClearBeforeFill = True
        '
        'ConRelSectorColoniaTableAdapter
        '
        Me.ConRelSectorColoniaTableAdapter.ClearBeforeFill = True
        '
        'BorRelSectorColoniaBindingSource
        '
        Me.BorRelSectorColoniaBindingSource.DataMember = "BorRelSectorColonia"
        Me.BorRelSectorColoniaBindingSource.DataSource = Me.DataSetEric
        '
        'BorRelSectorColoniaTableAdapter
        '
        Me.BorRelSectorColoniaTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(365, 45)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 33)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Agregar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Clv_TxtTextBox)
        Me.GroupBox1.Controls.Add(Me.BindingNavigator1)
        Me.GroupBox1.Controls.Add(Me.DescripcionTextBox)
        Me.GroupBox1.Controls.Add(Me.DescripcionLabel)
        Me.GroupBox1.Controls.Add(Me.Clv_TxtLabel)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(521, 162)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cluster"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton2, Me.ToolStripButton3})
        Me.BindingNavigator1.Location = New System.Drawing.Point(3, 18)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(515, 25)
        Me.BindingNavigator1.TabIndex = 2
        Me.BindingNavigator1.TabStop = True
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(74, 22)
        Me.ToolStripButton2.Text = "&ELIMINAR"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(75, 22)
        Me.ToolStripButton3.Text = "&GUARDAR"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ComboBoxEstado)
        Me.GroupBox2.Controls.Add(Label7)
        Me.GroupBox2.Controls.Add(Me.ComboBoxCiudad)
        Me.GroupBox2.Controls.Add(Label6)
        Me.GroupBox2.Controls.Add(Me.ComboBox3)
        Me.GroupBox2.Controls.Add(NOMBRELabel1)
        Me.GroupBox2.Controls.Add(Me.NombreLabel)
        Me.GroupBox2.Controls.Add(Me.NombreComboBox)
        Me.GroupBox2.Controls.Add(Me.ConRelSectorColoniaDataGridView)
        Me.GroupBox2.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.GroupBox2.Controls.Add(Me.Clv_SectorTextBox)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 171)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(521, 463)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Relación de Colonias/Ciudad/Cluster"
        '
        'ComboBoxEstado
        '
        Me.ComboBoxEstado.DisplayMember = "Nombre"
        Me.ComboBoxEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxEstado.FormattingEnabled = True
        Me.ComboBoxEstado.Location = New System.Drawing.Point(30, 45)
        Me.ComboBoxEstado.Name = "ComboBoxEstado"
        Me.ComboBoxEstado.Size = New System.Drawing.Size(320, 23)
        Me.ComboBoxEstado.TabIndex = 18
        Me.ComboBoxEstado.ValueMember = "Clv_Estado"
        '
        'ComboBoxCiudad
        '
        Me.ComboBoxCiudad.DisplayMember = "Nombre"
        Me.ComboBoxCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCiudad.FormattingEnabled = True
        Me.ComboBoxCiudad.Location = New System.Drawing.Point(30, 89)
        Me.ComboBoxCiudad.Name = "ComboBoxCiudad"
        Me.ComboBoxCiudad.Size = New System.Drawing.Size(320, 23)
        Me.ComboBoxCiudad.TabIndex = 16
        Me.ComboBoxCiudad.ValueMember = "Clv_Ciudad"
        '
        'ComboBox3
        '
        Me.ComboBox3.DisplayMember = "NOMBRE"
        Me.ComboBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(30, 133)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(320, 23)
        Me.ComboBox3.TabIndex = 13
        Me.ComboBox3.ValueMember = "Clv_Localidad"
        '
        'NombreComboBox
        '
        Me.NombreComboBox.DisplayMember = "Nombre"
        Me.NombreComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.NombreComboBox.FormattingEnabled = True
        Me.NombreComboBox.Location = New System.Drawing.Point(28, 179)
        Me.NombreComboBox.Name = "NombreComboBox"
        Me.NombreComboBox.Size = New System.Drawing.Size(322, 23)
        Me.NombreComboBox.TabIndex = 12
        Me.NombreComboBox.ValueMember = "CLV_COLONIA"
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRelSectorColonia1BindingSource, "Clv_Colonia", True))
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(169, 312)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.ReadOnly = True
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_ColoniaTextBox.TabIndex = 11
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(365, 84)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 33)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Eliminar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'MuestraColoniaSec1BindingSource
        '
        Me.MuestraColoniaSec1BindingSource.DataMember = "MuestraColoniaSec1"
        Me.MuestraColoniaSec1BindingSource.DataSource = Me.DataSetEric
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(397, 640)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'NueRelSectorColoniaBindingSource
        '
        Me.NueRelSectorColoniaBindingSource.DataMember = "NueRelSectorColonia"
        Me.NueRelSectorColoniaBindingSource.DataSource = Me.DataSetEric
        '
        'NueRelSectorColoniaTableAdapter
        '
        Me.NueRelSectorColoniaTableAdapter.ClearBeforeFill = True
        '
        'MuestraColoniaSec1TableAdapter
        '
        Me.MuestraColoniaSec1TableAdapter.ClearBeforeFill = True
        '
        'ConSector1TableAdapter
        '
        Me.ConSector1TableAdapter.ClearBeforeFill = True
        '
        'ConRelSectorColonia1TableAdapter
        '
        Me.ConRelSectorColonia1TableAdapter.ClearBeforeFill = True
        '
        'ModSector1BindingSource
        '
        Me.ModSector1BindingSource.DataMember = "ModSector1"
        Me.ModSector1BindingSource.DataSource = Me.DataSetEric
        '
        'ModSector1TableAdapter
        '
        Me.ModSector1TableAdapter.ClearBeforeFill = True
        '
        'DataSetEric1
        '
        Me.DataSetEric1.DataSetName = "DataSetEric"
        Me.DataSetEric1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NueSector1BindingSource
        '
        Me.NueSector1BindingSource.DataMember = "NueSector1"
        Me.NueSector1BindingSource.DataSource = Me.DataSetEric1
        '
        'NueSector1TableAdapter
        '
        Me.NueSector1TableAdapter.ClearBeforeFill = True
        '
        'BorSector1BindingSource
        '
        Me.BorSector1BindingSource.DataMember = "BorSector1"
        Me.BorSector1BindingSource.DataSource = Me.DataSetEric
        '
        'BorSector1TableAdapter
        '
        Me.BorSector1TableAdapter.ClearBeforeFill = True
        '
        'BorRelSectorColonia1BindingSource
        '
        Me.BorRelSectorColonia1BindingSource.DataMember = "BorRelSectorColonia1"
        Me.BorRelSectorColonia1BindingSource.DataSource = Me.DataSetEric
        '
        'BorRelSectorColonia1TableAdapter
        '
        Me.BorRelSectorColonia1TableAdapter.ClearBeforeFill = True
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NueRelSectorColonia1BindingSource
        '
        Me.NueRelSectorColonia1BindingSource.DataMember = "NueRelSectorColonia1"
        Me.NueRelSectorColonia1BindingSource.DataSource = Me.DataSetEric2
        '
        'NueRelSectorColonia1TableAdapter
        '
        Me.NueRelSectorColonia1TableAdapter.ClearBeforeFill = True
        '
        'Clv_RelSecCol
        '
        Me.Clv_RelSecCol.DataPropertyName = "Clv_RelSecCol"
        Me.Clv_RelSecCol.HeaderText = "Clv_RelSecCol"
        Me.Clv_RelSecCol.Name = "Clv_RelSecCol"
        Me.Clv_RelSecCol.ReadOnly = True
        Me.Clv_RelSecCol.Visible = False
        '
        'Ciudad
        '
        Me.Ciudad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Ciudad.DataPropertyName = "Ciudad"
        Me.Ciudad.HeaderText = "Ciudad"
        Me.Ciudad.Name = "Ciudad"
        Me.Ciudad.ReadOnly = True
        '
        'Localidad
        '
        Me.Localidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Localidad.DataPropertyName = "Localidad"
        Me.Localidad.HeaderText = "Localidad"
        Me.Localidad.Name = "Localidad"
        Me.Localidad.ReadOnly = True
        '
        'Colonia
        '
        Me.Colonia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Colonia.DataPropertyName = "Colonia"
        Me.Colonia.HeaderText = "Colonia"
        Me.Colonia.Name = "Colonia"
        Me.Colonia.ReadOnly = True
        '
        'Clv_Colonia
        '
        Me.Clv_Colonia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Clv_Colonia.DataPropertyName = "Clv_Colonia"
        Me.Clv_Colonia.HeaderText = "Clv_Colonia"
        Me.Clv_Colonia.Name = "Clv_Colonia"
        Me.Clv_Colonia.ReadOnly = True
        Me.Clv_Colonia.Visible = False
        '
        'FrmHub
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(545, 689)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "FrmHub"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cluster"
        CType(Me.ConSector1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSectorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelSectorColoniaDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelSectorColonia1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelSectorColoniaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorRelSectorColoniaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.MuestraColoniaSec1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueRelSectorColoniaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModSector1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueSector1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorSector1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorRelSectorColonia1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueRelSectorColonia1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConSectorBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSectorTableAdapter As sofTV.DataSetEricTableAdapters.ConSectorTableAdapter
    Friend WithEvents Clv_SectorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TxtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConRelSectorColoniaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelSectorColoniaTableAdapter As sofTV.DataSetEricTableAdapters.ConRelSectorColoniaTableAdapter
    Friend WithEvents ConRelSectorColoniaDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents BorRelSectorColoniaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorRelSectorColoniaTableAdapter As sofTV.DataSetEricTableAdapters.BorRelSectorColoniaTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NueRelSectorColoniaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueRelSectorColoniaTableAdapter As sofTV.DataSetEricTableAdapters.NueRelSectorColoniaTableAdapter
    Friend WithEvents NombreComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_TxtLabel As System.Windows.Forms.Label
    Friend WithEvents DescripcionLabel As System.Windows.Forms.Label
    Friend WithEvents NombreLabel As System.Windows.Forms.Label
    Friend WithEvents MuestraColoniaSec1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraColoniaSec1TableAdapter As sofTV.DataSetEricTableAdapters.MuestraColoniaSec1TableAdapter
    Friend WithEvents ConSector1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSector1TableAdapter As Softv.DataSetEricTableAdapters.ConSector1TableAdapter
    Friend WithEvents ConRelSectorColonia1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelSectorColonia1TableAdapter As Softv.DataSetEricTableAdapters.ConRelSectorColonia1TableAdapter
    Friend WithEvents ModSector1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModSector1TableAdapter As sofTV.DataSetEricTableAdapters.ModSector1TableAdapter
    Friend WithEvents DataSetEric1 As sofTV.DataSetEric
    Friend WithEvents NueSector1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueSector1TableAdapter As Softv.DataSetEricTableAdapters.NueSector1TableAdapter
    Friend WithEvents BorSector1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorSector1TableAdapter As sofTV.DataSetEricTableAdapters.BorSector1TableAdapter
    Friend WithEvents BorRelSectorColonia1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorRelSectorColonia1TableAdapter As sofTV.DataSetEricTableAdapters.BorRelSectorColonia1TableAdapter
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric
    Friend WithEvents NueRelSectorColonia1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueRelSectorColonia1TableAdapter As Softv.DataSetEricTableAdapters.NueRelSectorColonia1TableAdapter
    Friend WithEvents ComboBoxEstado As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_RelSecCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ciudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Localidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Colonia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Colonia As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
