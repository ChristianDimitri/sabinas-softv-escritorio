﻿Imports System.ComponentModel
Imports System.Threading
Public Class LoadingForm

    Private _worker As BackgroundWorker

    'Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
    '    MyBase.OnLoad(e)

    '    _worker = New BackgroundWorker()
    '    AddHandler _worker.DoWork, AddressOf WorkerDoWork
    '    AddHandler _worker.RunWorkerCompleted, AddressOf WorkerCompleted

    '    _worker.RunWorkerAsync()
    'End Sub

    ' This is executed on a worker thread and will not make the dialog unresponsive.  If you want
    ' to interact with the dialog (like changing a progress bar or label), you need to use the
    ' worker's ReportProgress() method (see documentation for details)
    Dim contador As Integer = 0
    Private Sub WorkerDoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        While True
            Me.ProgressBar1.Value = contador
            If contador = 100 Then
                contador = 0
            Else
                contador = contador + 1
            End If
            System.Threading.Thread.Sleep(3000)
        End While
    End Sub

    ' This is executed on the UI thread after the work is complete.  It's a good place to either
    ' close the dialog or indicate that the initialization is complete.  It's safe to work with
    ' controls from this event.
    Private Sub WorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub LoadingForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'MyBase.OnLoad(e)

        '_worker = New BackgroundWorker()
        'AddHandler _worker.DoWork, AddressOf WorkerDoWork
        'AddHandler _worker.RunWorkerCompleted, AddressOf WorkerCompleted

        '_worker.RunWorkerAsync()
        'Dim obj_multi As New Thread(AddressOf llenar)
        obj_multi.Start()
    End Sub
    Dim obj_multi As New Thread(AddressOf llenar)
    Delegate Sub AddItemCallBack(ByVal Item As Integer)

    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Dim obj_multi As New Thread(AddressOf llenar)
    '    obj_multi.Start()
    'End Sub
    Private Sub llenar()
        While True
            Me.AddItemToList(contador)
            If contador = 10 Then
                contador = 0
            Else
                contador = contador + 1
            End If
            System.Threading.Thread.Sleep(3000)
        End While
    End Sub

    'Este método demuestra un patrón para realizar llamadas seguras a hilos
    'en un control Windows Forms.
    'Si el hilo que hace la llamada es diferente que el hilo que creo el control
    'ListBox, entonces el método crea un AddItemCallBack y lo manda llamar desde
    'el mismo de manera asíncrona utilizando en método invoke.
    'Si el hilo es el mismo que el que creo el control ListBox, entonces el  mÍtem
    ' es agregado directamente
    Public Sub AddItemToList(ByVal Item As Integer)
        If Me.ProgressBar1.InvokeRequired Then
            Dim d As New AddItemCallBack(AddressOf AddItemToList)
            Me.Invoke(d, New Object() {Item})
        Else
            Me.ProgressBar1.Value = Item
        End If
    End Sub

    Private Sub LoadingForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        'obj_multi.Suspend();
    End Sub
End Class