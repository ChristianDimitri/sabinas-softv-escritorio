﻿Public Class FrmRoboAparatos
    Public clv_ordenRobo As Integer
    Public contratoRobo As Integer
    Private Sub FrmRoboAparatos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        AparatosPorReportarRobo()
    End Sub

    Private Sub AparatosPorReportarRobo()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.Int, clv_ordenRobo)
        DataGridView1.DataSource = BaseII.ConsultaDT("AparatosPorReportarRobo")

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub dataGridView1_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged

        Try
            If DataGridView1.Columns(e.ColumnIndex).Name = "Robado" Then
                Dim checkCell As DataGridViewCheckBoxCell = CType(DataGridView1.Rows(e.RowIndex).Cells("Robado"), DataGridViewCheckBoxCell)

                If Not IsNumeric(checkCell.Value) Then
                    checkCell.Value = 0
                End If
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, clv_ordenRobo)
                BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, DataGridView1.Rows(e.RowIndex).Cells(1).Value)
                BaseII.CreateMyParameter("@Clv_Cablemodem", SqlDbType.BigInt, CInt(DataGridView1.Rows(e.RowIndex).Cells(2).Value))
                BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, CInt(DataGridView1.Rows(e.RowIndex).Cells(4).Value))
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.BigInt, CInt(DataGridView1.Rows(e.RowIndex).Cells(5).Value))
                BaseII.CreateMyParameter("@Robado", SqlDbType.Bit, CType(checkCell.Value, [Boolean]))
                BaseII.CreateMyParameter("@clv_usuario", SqlDbType.BigInt, GloClvUsuario)
                BaseII.Inserta("ReportaAparatosRobados")

                If CType(checkCell.Value, [Boolean]) Then
                    bitsist(GloUsuario, contratoRobo, LocGloSistema, "Aparatos sin Recuperar", "Recuperación del aparto: " + DataGridView1.Rows(e.RowIndex).Cells(2).Value.ToString + ", en la orden " + clv_ordenRobo.ToString, "", "Sin Recuperar", LocClv_Ciudad)
                End If
            End If


        Catch ex As System.Exception
        End Try
    End Sub

    Sub dataGridView1_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DataGridView1.CurrentCellDirtyStateChanged

        If DataGridView1.IsCurrentCellDirty Then
            DataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub
End Class