﻿Imports System.Data.SqlClient

Public Class FrmSelServicioContrato

    Private Sub FrmSelServicioContrato_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter1.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter1.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)

        CON.Close()

        colorea(Me, Me.Name)
        Label1.ForeColor = Color.Black

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        rGloTIpserCont = CboxTipSer.SelectedValue
        Me.Close()

    End Sub

End Class