<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImpresionEdoCuenta_DEV
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ImpresionEdoCuenta_DEV))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPeriodoCobro = New System.Windows.Forms.TextBox()
        Me.CMBLblContratoFin = New System.Windows.Forms.Label()
        Me.CMBLblContratoIni = New System.Windows.Forms.Label()
        Me.TxtContratoIni = New System.Windows.Forms.TextBox()
        Me.TxtContratoFin = New System.Windows.Forms.TextBox()
        Me.CMBBtnBusca2 = New System.Windows.Forms.Button()
        Me.CMBBtnBusca1 = New System.Windows.Forms.Button()
        Me.CMBBtnSalir = New System.Windows.Forms.Button()
        Me.CMBBtnImprimir = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnCargarPeriodo = New System.Windows.Forms.Button()
        Me.PeriodosCobro = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodosCobro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtPeriodoCobro)
        Me.Panel1.Controls.Add(Me.CMBLblContratoFin)
        Me.Panel1.Controls.Add(Me.CMBLblContratoIni)
        Me.Panel1.Controls.Add(Me.TxtContratoIni)
        Me.Panel1.Controls.Add(Me.TxtContratoFin)
        Me.Panel1.Controls.Add(Me.CMBBtnBusca2)
        Me.Panel1.Controls.Add(Me.CMBBtnBusca1)
        Me.Panel1.Location = New System.Drawing.Point(31, 33)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(553, 267)
        Me.Panel1.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(20, 212)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(180, 20)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Periodo de Cobro:"
        '
        'txtPeriodoCobro
        '
        Me.txtPeriodoCobro.Enabled = False
        Me.txtPeriodoCobro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriodoCobro.Location = New System.Drawing.Point(223, 208)
        Me.txtPeriodoCobro.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtPeriodoCobro.MaxLength = 6
        Me.txtPeriodoCobro.Name = "txtPeriodoCobro"
        Me.txtPeriodoCobro.Size = New System.Drawing.Size(255, 24)
        Me.txtPeriodoCobro.TabIndex = 6
        '
        'CMBLblContratoFin
        '
        Me.CMBLblContratoFin.AutoSize = True
        Me.CMBLblContratoFin.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblContratoFin.Location = New System.Drawing.Point(20, 121)
        Me.CMBLblContratoFin.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLblContratoFin.Name = "CMBLblContratoFin"
        Me.CMBLblContratoFin.Size = New System.Drawing.Size(151, 20)
        Me.CMBLblContratoFin.TabIndex = 5
        Me.CMBLblContratoFin.Text = "Contrato Final:"
        '
        'CMBLblContratoIni
        '
        Me.CMBLblContratoIni.AutoSize = True
        Me.CMBLblContratoIni.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblContratoIni.Location = New System.Drawing.Point(20, 21)
        Me.CMBLblContratoIni.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLblContratoIni.Name = "CMBLblContratoIni"
        Me.CMBLblContratoIni.Size = New System.Drawing.Size(165, 20)
        Me.CMBLblContratoIni.TabIndex = 4
        Me.CMBLblContratoIni.Text = "Contrato Inicial:"
        '
        'TxtContratoIni
        '
        Me.TxtContratoIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtContratoIni.Location = New System.Drawing.Point(223, 21)
        Me.TxtContratoIni.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtContratoIni.MaxLength = 6
        Me.TxtContratoIni.Name = "TxtContratoIni"
        Me.TxtContratoIni.Size = New System.Drawing.Size(255, 24)
        Me.TxtContratoIni.TabIndex = 1
        '
        'TxtContratoFin
        '
        Me.TxtContratoFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtContratoFin.Location = New System.Drawing.Point(223, 117)
        Me.TxtContratoFin.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtContratoFin.MaxLength = 6
        Me.TxtContratoFin.Name = "TxtContratoFin"
        Me.TxtContratoFin.Size = New System.Drawing.Size(255, 24)
        Me.TxtContratoFin.TabIndex = 2
        '
        'CMBBtnBusca2
        '
        Me.CMBBtnBusca2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CMBBtnBusca2.Location = New System.Drawing.Point(497, 117)
        Me.CMBBtnBusca2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnBusca2.Name = "CMBBtnBusca2"
        Me.CMBBtnBusca2.Size = New System.Drawing.Size(40, 28)
        Me.CMBBtnBusca2.TabIndex = 1
        Me.CMBBtnBusca2.Text = "..."
        Me.CMBBtnBusca2.UseVisualStyleBackColor = True
        '
        'CMBBtnBusca1
        '
        Me.CMBBtnBusca1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnBusca1.Location = New System.Drawing.Point(493, 21)
        Me.CMBBtnBusca1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnBusca1.Name = "CMBBtnBusca1"
        Me.CMBBtnBusca1.Size = New System.Drawing.Size(40, 25)
        Me.CMBBtnBusca1.TabIndex = 0
        Me.CMBBtnBusca1.Text = "..."
        Me.CMBBtnBusca1.UseVisualStyleBackColor = True
        '
        'CMBBtnSalir
        '
        Me.CMBBtnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CMBBtnSalir.Image = CType(resources.GetObject("CMBBtnSalir.Image"), System.Drawing.Image)
        Me.CMBBtnSalir.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.CMBBtnSalir.Location = New System.Drawing.Point(639, 455)
        Me.CMBBtnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnSalir.Name = "CMBBtnSalir"
        Me.CMBBtnSalir.Size = New System.Drawing.Size(423, 89)
        Me.CMBBtnSalir.TabIndex = 8
        Me.CMBBtnSalir.Text = "&SALIR"
        Me.CMBBtnSalir.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.CMBBtnSalir.UseVisualStyleBackColor = True
        '
        'CMBBtnImprimir
        '
        Me.CMBBtnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CMBBtnImprimir.Image = CType(resources.GetObject("CMBBtnImprimir.Image"), System.Drawing.Image)
        Me.CMBBtnImprimir.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.CMBBtnImprimir.Location = New System.Drawing.Point(201, 455)
        Me.CMBBtnImprimir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnImprimir.Name = "CMBBtnImprimir"
        Me.CMBBtnImprimir.Size = New System.Drawing.Size(423, 89)
        Me.CMBBtnImprimir.TabIndex = 3
        Me.CMBBtnImprimir.Text = "&Comenzar Impresión"
        Me.CMBBtnImprimir.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.CMBBtnImprimir.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.btnCargarPeriodo)
        Me.GroupBox1.Controls.Add(Me.PeriodosCobro)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(671, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(672, 370)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Selecciona el Periodo de Cobro"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(620, 311)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(44, 44)
        Me.PictureBox1.TabIndex = 14
        Me.PictureBox1.TabStop = False
        '
        'btnCargarPeriodo
        '
        Me.btnCargarPeriodo.Image = CType(resources.GetObject("btnCargarPeriodo.Image"), System.Drawing.Image)
        Me.btnCargarPeriodo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCargarPeriodo.Location = New System.Drawing.Point(52, 295)
        Me.btnCargarPeriodo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCargarPeriodo.Name = "btnCargarPeriodo"
        Me.btnCargarPeriodo.Size = New System.Drawing.Size(527, 60)
        Me.btnCargarPeriodo.TabIndex = 13
        Me.btnCargarPeriodo.Text = "Cargar el Periodo Seleccionado"
        Me.btnCargarPeriodo.UseVisualStyleBackColor = True
        '
        'PeriodosCobro
        '
        Me.PeriodosCobro.AllowUserToAddRows = False
        Me.PeriodosCobro.AllowUserToDeleteRows = False
        Me.PeriodosCobro.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.PeriodosCobro.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.PeriodosCobro.BackgroundColor = System.Drawing.SystemColors.ButtonFace
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(2)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PeriodosCobro.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.PeriodosCobro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PeriodosCobro.Location = New System.Drawing.Point(52, 33)
        Me.PeriodosCobro.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PeriodosCobro.Name = "PeriodosCobro"
        Me.PeriodosCobro.ReadOnly = True
        Me.PeriodosCobro.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.PeriodosCobro.Size = New System.Drawing.Size(573, 247)
        Me.PeriodosCobro.TabIndex = 12
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(16, 15)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(623, 356)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Ingresa el rango de contratos"
        '
        'BackgroundWorker1
        '
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(1263, 393)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 33)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ImpresionEdoCuenta_DEV
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1359, 596)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.CMBBtnSalir)
        Me.Controls.Add(Me.CMBBtnImprimir)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "ImpresionEdoCuenta_DEV"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Impresión de Estados de Cuenta"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodosCobro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBLblContratoFin As System.Windows.Forms.Label
    Friend WithEvents CMBLblContratoIni As System.Windows.Forms.Label
    Friend WithEvents TxtContratoIni As System.Windows.Forms.TextBox
    Friend WithEvents TxtContratoFin As System.Windows.Forms.TextBox
    Friend WithEvents CMBBtnBusca2 As System.Windows.Forms.Button
    Friend WithEvents CMBBtnBusca1 As System.Windows.Forms.Button
    Friend WithEvents CMBBtnSalir As System.Windows.Forms.Button
    Friend WithEvents CMBBtnImprimir As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PeriodosCobro As System.Windows.Forms.DataGridView
    Friend WithEvents btnCargarPeriodo As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPeriodoCobro As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class
