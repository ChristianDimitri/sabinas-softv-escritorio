<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwPreguntas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ButtonBusPregunta = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxPregunta = New System.Windows.Forms.TextBox()
        Me.ComboBoxTipoRespuesta = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ButtonBusTipoRespuesta = New System.Windows.Forms.Button()
        Me.ButtonNuevo = New System.Windows.Forms.Button()
        Me.ButtonConsultar = New System.Windows.Forms.Button()
        Me.ButtonModificar = New System.Windows.Forms.Button()
        Me.ButtonSalir = New System.Windows.Forms.Button()
        Me.DataGridViewPregunta = New System.Windows.Forms.DataGridView()
        Me.IDPregunta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pregunta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDTipoRespuesta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Activa = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TipoRespuesta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label3 = New System.Windows.Forms.Label()
        CType(Me.DataGridViewPregunta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButtonBusPregunta
        '
        Me.ButtonBusPregunta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusPregunta.Location = New System.Drawing.Point(16, 143)
        Me.ButtonBusPregunta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBusPregunta.Name = "ButtonBusPregunta"
        Me.ButtonBusPregunta.Size = New System.Drawing.Size(100, 28)
        Me.ButtonBusPregunta.TabIndex = 0
        Me.ButtonBusPregunta.Text = "Buscar"
        Me.ButtonBusPregunta.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 87)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 18)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Pregunta"
        '
        'TextBoxPregunta
        '
        Me.TextBoxPregunta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxPregunta.Location = New System.Drawing.Point(16, 110)
        Me.TextBoxPregunta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxPregunta.Name = "TextBoxPregunta"
        Me.TextBoxPregunta.Size = New System.Drawing.Size(316, 24)
        Me.TextBoxPregunta.TabIndex = 2
        '
        'ComboBoxTipoRespuesta
        '
        Me.ComboBoxTipoRespuesta.DisplayMember = "TipoRespuesta"
        Me.ComboBoxTipoRespuesta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxTipoRespuesta.FormattingEnabled = True
        Me.ComboBoxTipoRespuesta.Location = New System.Drawing.Point(16, 231)
        Me.ComboBoxTipoRespuesta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxTipoRespuesta.Name = "ComboBoxTipoRespuesta"
        Me.ComboBoxTipoRespuesta.Size = New System.Drawing.Size(316, 26)
        Me.ComboBoxTipoRespuesta.TabIndex = 3
        Me.ComboBoxTipoRespuesta.ValueMember = "IDTipoRespuesta"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 209)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(126, 18)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Tipo Respuesta"
        '
        'ButtonBusTipoRespuesta
        '
        Me.ButtonBusTipoRespuesta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusTipoRespuesta.Location = New System.Drawing.Point(16, 267)
        Me.ButtonBusTipoRespuesta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBusTipoRespuesta.Name = "ButtonBusTipoRespuesta"
        Me.ButtonBusTipoRespuesta.Size = New System.Drawing.Size(100, 28)
        Me.ButtonBusTipoRespuesta.TabIndex = 5
        Me.ButtonBusTipoRespuesta.Text = "Buscar"
        Me.ButtonBusTipoRespuesta.UseVisualStyleBackColor = True
        '
        'ButtonNuevo
        '
        Me.ButtonNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonNuevo.Location = New System.Drawing.Point(1157, 15)
        Me.ButtonNuevo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonNuevo.Name = "ButtonNuevo"
        Me.ButtonNuevo.Size = New System.Drawing.Size(181, 44)
        Me.ButtonNuevo.TabIndex = 6
        Me.ButtonNuevo.Text = "&NUEVO"
        Me.ButtonNuevo.UseVisualStyleBackColor = True
        '
        'ButtonConsultar
        '
        Me.ButtonConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonConsultar.Location = New System.Drawing.Point(1157, 69)
        Me.ButtonConsultar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonConsultar.Name = "ButtonConsultar"
        Me.ButtonConsultar.Size = New System.Drawing.Size(181, 44)
        Me.ButtonConsultar.TabIndex = 7
        Me.ButtonConsultar.Text = "&CONSULTAR"
        Me.ButtonConsultar.UseVisualStyleBackColor = True
        '
        'ButtonModificar
        '
        Me.ButtonModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonModificar.Location = New System.Drawing.Point(1157, 121)
        Me.ButtonModificar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonModificar.Name = "ButtonModificar"
        Me.ButtonModificar.Size = New System.Drawing.Size(181, 44)
        Me.ButtonModificar.TabIndex = 8
        Me.ButtonModificar.Text = "&MODIFICAR"
        Me.ButtonModificar.UseVisualStyleBackColor = True
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(1157, 844)
        Me.ButtonSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(181, 44)
        Me.ButtonSalir.TabIndex = 9
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'DataGridViewPregunta
        '
        Me.DataGridViewPregunta.AllowUserToAddRows = False
        Me.DataGridViewPregunta.AllowUserToDeleteRows = False
        Me.DataGridViewPregunta.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewPregunta.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewPregunta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewPregunta.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDPregunta, Me.Pregunta, Me.IDTipoRespuesta, Me.Activa, Me.TipoRespuesta})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewPregunta.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewPregunta.Location = New System.Drawing.Point(377, 15)
        Me.DataGridViewPregunta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridViewPregunta.Name = "DataGridViewPregunta"
        Me.DataGridViewPregunta.ReadOnly = True
        Me.DataGridViewPregunta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewPregunta.Size = New System.Drawing.Size(743, 874)
        Me.DataGridViewPregunta.TabIndex = 10
        '
        'IDPregunta
        '
        Me.IDPregunta.DataPropertyName = "IDPregunta"
        Me.IDPregunta.HeaderText = "IDPregunta"
        Me.IDPregunta.Name = "IDPregunta"
        Me.IDPregunta.ReadOnly = True
        Me.IDPregunta.Visible = False
        '
        'Pregunta
        '
        Me.Pregunta.DataPropertyName = "Pregunta"
        Me.Pregunta.HeaderText = "Pregunta"
        Me.Pregunta.Name = "Pregunta"
        Me.Pregunta.ReadOnly = True
        Me.Pregunta.Width = 250
        '
        'IDTipoRespuesta
        '
        Me.IDTipoRespuesta.DataPropertyName = "IDTipoRespuesta"
        Me.IDTipoRespuesta.HeaderText = "IDTipoRespuesta"
        Me.IDTipoRespuesta.Name = "IDTipoRespuesta"
        Me.IDTipoRespuesta.ReadOnly = True
        Me.IDTipoRespuesta.Visible = False
        '
        'Activa
        '
        Me.Activa.DataPropertyName = "Activa"
        Me.Activa.HeaderText = "Activa"
        Me.Activa.Name = "Activa"
        Me.Activa.ReadOnly = True
        '
        'TipoRespuesta
        '
        Me.TipoRespuesta.DataPropertyName = "TipoRespuesta"
        Me.TipoRespuesta.HeaderText = "TipoRespuesta"
        Me.TipoRespuesta.Name = "TipoRespuesta"
        Me.TipoRespuesta.ReadOnly = True
        Me.TipoRespuesta.Width = 150
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(16, 34)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(216, 25)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Buscar Pregunta por:"
        '
        'BrwPreguntas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1355, 903)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DataGridViewPregunta)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.ButtonModificar)
        Me.Controls.Add(Me.ButtonConsultar)
        Me.Controls.Add(Me.ButtonNuevo)
        Me.Controls.Add(Me.ButtonBusTipoRespuesta)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboBoxTipoRespuesta)
        Me.Controls.Add(Me.TextBoxPregunta)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ButtonBusPregunta)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwPreguntas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Preguntas"
        CType(Me.DataGridViewPregunta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButtonBusPregunta As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBoxPregunta As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxTipoRespuesta As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ButtonBusTipoRespuesta As System.Windows.Forms.Button
    Friend WithEvents ButtonNuevo As System.Windows.Forms.Button
    Friend WithEvents ButtonConsultar As System.Windows.Forms.Button
    Friend WithEvents ButtonModificar As System.Windows.Forms.Button
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents DataGridViewPregunta As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents IDPregunta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pregunta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDTipoRespuesta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Activa As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents TipoRespuesta As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
