﻿Imports System.Data.SqlClient
Public Class FrmMensajeQuejaCall
    Dim contrato As String
    Dim clv_queja As String
    Dim nombre As String
    Dim tipo As Integer
    Private Sub FrmMensajeQuejaCall_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim comando As New SqlCommand()

        If siRecibeMensaje = True Then
            comando.Connection = conexion
            comando.CommandText = "exec DameFaltantesRevisarRecibir " + GloClvUsuario.ToString + ",2"
            Dim reader As SqlDataReader = comando.ExecuteReader()
            reader.Read()
            Label1.Text = "Tiene " + reader(0).ToString + " contratos no revisados y " + reader(1).ToString + " por recibir documentos." + vbCrLf + " Favor de ponerse al corriente respecto al tema de comisiones."
            reader.Close()

        ElseIf ClvProspecto = 0 Then
            comando.Connection = conexion
            comando.CommandText = "exec DameQuejaCallCenter " + GloClvUsuario.ToString + ",2," + ClvQuejaMensaje.ToString
            Dim reader As SqlDataReader = comando.ExecuteReader()
            reader.Read()
            contrato = reader(2).ToString
            clv_queja = reader(0).ToString
            nombre = reader(3).ToString
            tipo = reader(4)
            If tipo = 1 Then
                Label1.Text = "El Reporte #" + reader(0).ToString + " se ha generado desde el Call Center y le pertenece al contrato " + reader(1).ToString + "." + vbNewLine + " Favor de agendar el reporte."
            Else
                Label1.Text = "El Reporte #" + reader(0).ToString + " se ha generado desde otra sucursal y le pertenece al contrato " + reader(1).ToString + "." + vbNewLine + " Favor de agendar el reporte."
            End If
            reader.Close()
            ElseIf ClvQuejaMensaje = 0 Then
                comando.Connection = conexion
                comando.CommandText = "exec DameProspectoCallCenter " + GloClvUsuario.ToString + ",2," + ClvProspecto.ToString
                Dim reader As SqlDataReader = comando.ExecuteReader()
                reader.Read()
                ClvProspecto = reader(0).ToString
                nombre = reader(1).ToString
                Label1.Text = "El Prospecto #" + reader(0).ToString + " se ha generado desde el Call Center y le pertenece a la plaza " + reader(2).ToString + "."
                reader.Close()
            End If
            conexion.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If siRecibeMensaje = True Then
            bitsist(GloUsuario, contrato, LocGloSistema, "Mensaje Call Center", "", "Mensaje de documentos no revisados y por recibir", GloSucursal, LocClv_Ciudad)
            siRecibeMensaje = False
        ElseIf ClvProspecto = 0 Then
            If tipo = 1 Then
                bitsist(GloUsuario, contrato, LocGloSistema, "Mensaje Call Center", "", "Mensaje de Reporte #" + clv_queja.ToString + " leído por el usuario " + nombre, GloSucursal, LocClv_Ciudad)
            Else
                bitsist(GloUsuario, contrato, LocGloSistema, "Mensaje de Sucursal", "", "Mensaje de Reporte #" + clv_queja.ToString + " leído por el usuario " + nombre, GloSucursal, LocClv_Ciudad)
            End If
        ElseIf ClvQuejaMensaje = 0 Then
            bitsist(GloUsuario, 0, LocGloSistema, "Mensaje Call Center", "", "Mensaje de Prospecto #" + ClvProspecto.ToString + " leído por el usuario " + nombre, GloSucursal, LocClv_Ciudad)
        End If
        Me.Close()
    End Sub
End Class