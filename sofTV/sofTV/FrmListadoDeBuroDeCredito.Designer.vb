﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmListadoDeBuroDeCredito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim CMBGrupoLabel As System.Windows.Forms.Label
        Dim CMBLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvListadoBuro = New System.Windows.Forms.DataGridView()
        Me.colIdListadoBuro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colContrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRentas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPagare = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFechaReporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFechaRetiro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColClv_Buro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnGenerarListado = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.txtRutaDestino = New System.Windows.Forms.TextBox()
        Me.btnSeleccionarRuta = New System.Windows.Forms.Button()
        Me.btnExportarListado = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CMBGrupoLabel = New System.Windows.Forms.Label()
        CMBLabel1 = New System.Windows.Forms.Label()
        CType(Me.dgvListadoBuro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBGrupoLabel
        '
        CMBGrupoLabel.AutoSize = True
        CMBGrupoLabel.Dock = System.Windows.Forms.DockStyle.Fill
        CMBGrupoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBGrupoLabel.Location = New System.Drawing.Point(4, 0)
        CMBGrupoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBGrupoLabel.Name = "CMBGrupoLabel"
        CMBGrupoLabel.Size = New System.Drawing.Size(1037, 31)
        CMBGrupoLabel.TabIndex = 4
        CMBGrupoLabel.Text = "Listado Para Buro De Credito:"
        CMBGrupoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CMBLabel1
        '
        CMBLabel1.AutoSize = True
        CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel1.Location = New System.Drawing.Point(4, 12)
        CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBLabel1.Name = "CMBLabel1"
        CMBLabel1.Size = New System.Drawing.Size(172, 18)
        CMBLabel1.TabIndex = 13
        CMBLabel1.Text = "Ruta Destino Archivo:"
        '
        'dgvListadoBuro
        '
        Me.dgvListadoBuro.AllowUserToAddRows = False
        Me.dgvListadoBuro.AllowUserToDeleteRows = False
        Me.dgvListadoBuro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListadoBuro.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIdListadoBuro, Me.colContrato, Me.colNombre, Me.colRentas, Me.colPagare, Me.ColTotal, Me.colFechaReporte, Me.colFechaRetiro, Me.ColClv_Buro})
        Me.dgvListadoBuro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvListadoBuro.Location = New System.Drawing.Point(4, 35)
        Me.dgvListadoBuro.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvListadoBuro.Name = "dgvListadoBuro"
        Me.dgvListadoBuro.ReadOnly = True
        Me.dgvListadoBuro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvListadoBuro.Size = New System.Drawing.Size(1037, 407)
        Me.dgvListadoBuro.TabIndex = 5
        '
        'colIdListadoBuro
        '
        Me.colIdListadoBuro.DataPropertyName = "IdListadoBuro"
        Me.colIdListadoBuro.HeaderText = "IdListadoBuro"
        Me.colIdListadoBuro.Name = "colIdListadoBuro"
        Me.colIdListadoBuro.ReadOnly = True
        Me.colIdListadoBuro.Visible = False
        '
        'colContrato
        '
        Me.colContrato.DataPropertyName = "Contrato"
        Me.colContrato.HeaderText = "Contrato"
        Me.colContrato.Name = "colContrato"
        Me.colContrato.ReadOnly = True
        '
        'colNombre
        '
        Me.colNombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colNombre.DataPropertyName = "Nombre"
        Me.colNombre.HeaderText = "Nombre"
        Me.colNombre.Name = "colNombre"
        Me.colNombre.ReadOnly = True
        '
        'colRentas
        '
        Me.colRentas.DataPropertyName = "Rentas"
        DataGridViewCellStyle1.Format = "C2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colRentas.DefaultCellStyle = DataGridViewCellStyle1
        Me.colRentas.HeaderText = "Rentas"
        Me.colRentas.MinimumWidth = 100
        Me.colRentas.Name = "colRentas"
        Me.colRentas.ReadOnly = True
        '
        'colPagare
        '
        Me.colPagare.DataPropertyName = "Pagare"
        DataGridViewCellStyle2.Format = "C2"
        Me.colPagare.DefaultCellStyle = DataGridViewCellStyle2
        Me.colPagare.HeaderText = "Pagare"
        Me.colPagare.MinimumWidth = 100
        Me.colPagare.Name = "colPagare"
        Me.colPagare.ReadOnly = True
        '
        'ColTotal
        '
        Me.ColTotal.DataPropertyName = "Total"
        DataGridViewCellStyle3.Format = "C2"
        Me.ColTotal.DefaultCellStyle = DataGridViewCellStyle3
        Me.ColTotal.HeaderText = "Total"
        Me.ColTotal.MinimumWidth = 100
        Me.ColTotal.Name = "ColTotal"
        Me.ColTotal.ReadOnly = True
        '
        'colFechaReporte
        '
        Me.colFechaReporte.DataPropertyName = "FechaReporte"
        DataGridViewCellStyle4.Format = "d"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.colFechaReporte.DefaultCellStyle = DataGridViewCellStyle4
        Me.colFechaReporte.HeaderText = "Fecha Reporte"
        Me.colFechaReporte.MinimumWidth = 120
        Me.colFechaReporte.Name = "colFechaReporte"
        Me.colFechaReporte.ReadOnly = True
        Me.colFechaReporte.Width = 120
        '
        'colFechaRetiro
        '
        Me.colFechaRetiro.DataPropertyName = "FechaRetiro"
        DataGridViewCellStyle5.Format = "d"
        Me.colFechaRetiro.DefaultCellStyle = DataGridViewCellStyle5
        Me.colFechaRetiro.HeaderText = "Fecha Retiro"
        Me.colFechaRetiro.MinimumWidth = 100
        Me.colFechaRetiro.Name = "colFechaRetiro"
        Me.colFechaRetiro.ReadOnly = True
        '
        'ColClv_Buro
        '
        Me.ColClv_Buro.DataPropertyName = "Clv_Buro"
        Me.ColClv_Buro.HeaderText = "Clv_Buro"
        Me.ColClv_Buro.Name = "ColClv_Buro"
        Me.ColClv_Buro.ReadOnly = True
        Me.ColClv_Buro.Visible = False
        '
        'btnGenerarListado
        '
        Me.btnGenerarListado.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarListado.Location = New System.Drawing.Point(784, 55)
        Me.btnGenerarListado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnGenerarListado.Name = "btnGenerarListado"
        Me.btnGenerarListado.Size = New System.Drawing.Size(233, 44)
        Me.btnGenerarListado.TabIndex = 12
        Me.btnGenerarListado.Text = "Generar Listado"
        Me.btnGenerarListado.UseVisualStyleBackColor = True
        '
        'txtRutaDestino
        '
        Me.txtRutaDestino.Enabled = False
        Me.txtRutaDestino.Location = New System.Drawing.Point(204, 11)
        Me.txtRutaDestino.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtRutaDestino.Name = "txtRutaDestino"
        Me.txtRutaDestino.Size = New System.Drawing.Size(571, 22)
        Me.txtRutaDestino.TabIndex = 14
        '
        'btnSeleccionarRuta
        '
        Me.btnSeleccionarRuta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSeleccionarRuta.Location = New System.Drawing.Point(784, 2)
        Me.btnSeleccionarRuta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSeleccionarRuta.Name = "btnSeleccionarRuta"
        Me.btnSeleccionarRuta.Size = New System.Drawing.Size(233, 37)
        Me.btnSeleccionarRuta.TabIndex = 15
        Me.btnSeleccionarRuta.Text = "Seleccionar Ruta"
        Me.btnSeleccionarRuta.UseVisualStyleBackColor = True
        '
        'btnExportarListado
        '
        Me.btnExportarListado.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExportarListado.Location = New System.Drawing.Point(543, 55)
        Me.btnExportarListado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnExportarListado.Name = "btnExportarListado"
        Me.btnExportarListado.Size = New System.Drawing.Size(233, 44)
        Me.btnExportarListado.TabIndex = 16
        Me.btnExportarListado.Text = "Exportar Listado"
        Me.btnExportarListado.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(CMBGrupoLabel, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvListadoBuro, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 123.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1045, 569)
        Me.TableLayoutPanel1.TabIndex = 17
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(CMBLabel1)
        Me.Panel1.Controls.Add(Me.btnExportarListado)
        Me.Panel1.Controls.Add(Me.btnGenerarListado)
        Me.Panel1.Controls.Add(Me.btnSeleccionarRuta)
        Me.Panel1.Controls.Add(Me.txtRutaDestino)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(4, 450)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1037, 115)
        Me.Panel1.TabIndex = 6
        '
        'FrmListadoDeBuroDeCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1045, 569)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MinimumSize = New System.Drawing.Size(1061, 605)
        Me.Name = "FrmListadoDeBuroDeCredito"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listado Del Buro De Credito"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvListadoBuro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvListadoBuro As System.Windows.Forms.DataGridView
    Friend WithEvents btnGenerarListado As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents txtRutaDestino As System.Windows.Forms.TextBox
    Friend WithEvents btnSeleccionarRuta As System.Windows.Forms.Button
    Friend WithEvents colIdListadoBuro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colContrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRentas As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPagare As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFechaReporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFechaRetiro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColClv_Buro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnExportarListado As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
