﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDetDecodificadores
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cbDecodificador = New System.Windows.Forms.ComboBox()
        Me.cbTipoCliente = New System.Windows.Forms.ComboBox()
        Me.cbServicio = New System.Windows.Forms.ComboBox()
        Me.dgvDet = New System.Windows.Forms.DataGridView()
        Me.Detalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioPrincipal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioAdicional = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.CMBLabel77 = New System.Windows.Forms.Label()
        Me.txtPrecioPrincipal = New System.Windows.Forms.TextBox()
        Me.CMBLabel76 = New System.Windows.Forms.Label()
        Me.txtPrecioAdicional = New System.Windows.Forms.TextBox()
        Me.txtEliminarDec = New System.Windows.Forms.Button()
        Me.txtAgregarDec = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        CType(Me.dgvDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbDecodificador
        '
        Me.cbDecodificador.DisplayMember = "Descripcion"
        Me.cbDecodificador.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDecodificador.FormattingEnabled = True
        Me.cbDecodificador.Location = New System.Drawing.Point(359, 22)
        Me.cbDecodificador.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbDecodificador.Name = "cbDecodificador"
        Me.cbDecodificador.Size = New System.Drawing.Size(409, 26)
        Me.cbDecodificador.TabIndex = 0
        Me.cbDecodificador.ValueMember = "Id"
        '
        'cbTipoCliente
        '
        Me.cbTipoCliente.DisplayMember = "Descripcion"
        Me.cbTipoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipoCliente.FormattingEnabled = True
        Me.cbTipoCliente.Location = New System.Drawing.Point(359, 58)
        Me.cbTipoCliente.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbTipoCliente.Name = "cbTipoCliente"
        Me.cbTipoCliente.Size = New System.Drawing.Size(409, 26)
        Me.cbTipoCliente.TabIndex = 2
        Me.cbTipoCliente.ValueMember = "Clv_TipoCliente"
        '
        'cbServicio
        '
        Me.cbServicio.DisplayMember = "Descripcion"
        Me.cbServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbServicio.FormattingEnabled = True
        Me.cbServicio.Location = New System.Drawing.Point(359, 113)
        Me.cbServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbServicio.Name = "cbServicio"
        Me.cbServicio.Size = New System.Drawing.Size(409, 26)
        Me.cbServicio.TabIndex = 3
        Me.cbServicio.ValueMember = "Clv_Servicio"
        '
        'dgvDet
        '
        Me.dgvDet.AllowUserToAddRows = False
        Me.dgvDet.AllowUserToDeleteRows = False
        Me.dgvDet.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDet.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Detalle, Me.Servicio, Me.PrecioPrincipal, Me.PrecioAdicional})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDet.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvDet.Location = New System.Drawing.Point(77, 251)
        Me.dgvDet.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvDet.Name = "dgvDet"
        Me.dgvDet.ReadOnly = True
        Me.dgvDet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDet.Size = New System.Drawing.Size(809, 346)
        Me.dgvDet.TabIndex = 4
        '
        'Detalle
        '
        Me.Detalle.DataPropertyName = "Detalle"
        Me.Detalle.HeaderText = "Detalle"
        Me.Detalle.Name = "Detalle"
        Me.Detalle.ReadOnly = True
        Me.Detalle.Visible = False
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.ReadOnly = True
        Me.Servicio.Width = 250
        '
        'PrecioPrincipal
        '
        Me.PrecioPrincipal.DataPropertyName = "PrecioPrincipal"
        Me.PrecioPrincipal.HeaderText = "PrecioPrincipal"
        Me.PrecioPrincipal.Name = "PrecioPrincipal"
        Me.PrecioPrincipal.ReadOnly = True
        Me.PrecioPrincipal.Width = 150
        '
        'PrecioAdicional
        '
        Me.PrecioAdicional.DataPropertyName = "PrecioAdicional"
        Me.PrecioAdicional.HeaderText = "PrecioAdicional"
        Me.PrecioAdicional.Name = "PrecioAdicional"
        Me.PrecioAdicional.ReadOnly = True
        Me.PrecioAdicional.Width = 150
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(217, 32)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(118, 18)
        Me.CMBLabel1.TabIndex = 5
        Me.CMBLabel1.Text = "Decodificador:"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(233, 68)
        Me.CMBLabel3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(103, 18)
        Me.CMBLabel3.TabIndex = 7
        Me.CMBLabel3.Text = "Tipo Cliente:"
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(187, 123)
        Me.CMBLabel4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(144, 18)
        Me.CMBLabel4.TabIndex = 8
        Me.CMBLabel4.Text = "Servicio Principal:"
        '
        'CMBLabel77
        '
        Me.CMBLabel77.AutoSize = True
        Me.CMBLabel77.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel77.Location = New System.Drawing.Point(200, 156)
        Me.CMBLabel77.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel77.Name = "CMBLabel77"
        Me.CMBLabel77.Size = New System.Drawing.Size(132, 18)
        Me.CMBLabel77.TabIndex = 69
        Me.CMBLabel77.Text = "Precio Principal:"
        '
        'txtPrecioPrincipal
        '
        Me.txtPrecioPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrecioPrincipal.Location = New System.Drawing.Point(359, 149)
        Me.txtPrecioPrincipal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtPrecioPrincipal.Name = "txtPrecioPrincipal"
        Me.txtPrecioPrincipal.Size = New System.Drawing.Size(212, 24)
        Me.txtPrecioPrincipal.TabIndex = 68
        '
        'CMBLabel76
        '
        Me.CMBLabel76.AutoSize = True
        Me.CMBLabel76.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel76.Location = New System.Drawing.Point(197, 190)
        Me.CMBLabel76.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel76.Name = "CMBLabel76"
        Me.CMBLabel76.Size = New System.Drawing.Size(135, 18)
        Me.CMBLabel76.TabIndex = 67
        Me.CMBLabel76.Text = "Precio Adicional:"
        '
        'txtPrecioAdicional
        '
        Me.txtPrecioAdicional.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrecioAdicional.Location = New System.Drawing.Point(359, 182)
        Me.txtPrecioAdicional.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtPrecioAdicional.Name = "txtPrecioAdicional"
        Me.txtPrecioAdicional.Size = New System.Drawing.Size(212, 24)
        Me.txtPrecioAdicional.TabIndex = 66
        '
        'txtEliminarDec
        '
        Me.txtEliminarDec.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEliminarDec.Location = New System.Drawing.Point(895, 251)
        Me.txtEliminarDec.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtEliminarDec.Name = "txtEliminarDec"
        Me.txtEliminarDec.Size = New System.Drawing.Size(100, 28)
        Me.txtEliminarDec.TabIndex = 72
        Me.txtEliminarDec.Text = "&Eliminar"
        Me.txtEliminarDec.UseVisualStyleBackColor = True
        '
        'txtAgregarDec
        '
        Me.txtAgregarDec.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgregarDec.Location = New System.Drawing.Point(895, 180)
        Me.txtAgregarDec.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtAgregarDec.Name = "txtAgregarDec"
        Me.txtAgregarDec.Size = New System.Drawing.Size(100, 28)
        Me.txtAgregarDec.TabIndex = 71
        Me.txtAgregarDec.Text = "&Agregar"
        Me.txtAgregarDec.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(813, 629)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(181, 44)
        Me.btnSalir.TabIndex = 70
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'FrmDetDecodificadores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1011, 688)
        Me.Controls.Add(Me.txtEliminarDec)
        Me.Controls.Add(Me.txtAgregarDec)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.CMBLabel77)
        Me.Controls.Add(Me.txtPrecioPrincipal)
        Me.Controls.Add(Me.CMBLabel76)
        Me.Controls.Add(Me.txtPrecioAdicional)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.dgvDet)
        Me.Controls.Add(Me.cbServicio)
        Me.Controls.Add(Me.cbTipoCliente)
        Me.Controls.Add(Me.cbDecodificador)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmDetDecodificadores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Precio Decodificadores"
        CType(Me.dgvDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbDecodificador As System.Windows.Forms.ComboBox
    Friend WithEvents cbTipoCliente As System.Windows.Forms.ComboBox
    Friend WithEvents cbServicio As System.Windows.Forms.ComboBox
    Friend WithEvents dgvDet As System.Windows.Forms.DataGridView
    Friend WithEvents Detalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioPrincipal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioAdicional As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel77 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel76 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioAdicional As System.Windows.Forms.TextBox
    Friend WithEvents txtEliminarDec As System.Windows.Forms.Button
    Friend WithEvents txtAgregarDec As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
End Class
