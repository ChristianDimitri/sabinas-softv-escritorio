<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Bwr_FacturasCancelar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ImporteLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim ClienteLabel As System.Windows.Forms.Label
        Dim FECHALabel As System.Windows.Forms.Label
        Dim FacturaLabel As System.Windows.Forms.Label
        Dim SerieLabel As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ImporteLabel1 = New System.Windows.Forms.Label()
        Me.BUSCAFACTURASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New sofTV.NewsoftvDataSet1()
        Me.ClienteLabel1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.FECHALabel1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CMBNOMBRETextBox1 = New System.Windows.Forms.TextBox()
        Me.CMBPanel2 = New System.Windows.Forms.Panel()
        Me.FECHADateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.CMBPanel4 = New System.Windows.Forms.Panel()
        Me.CMBPanel3 = New System.Windows.Forms.Panel()
        Me.SERIETextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.BUSCANOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.DetalleNOTASDECREDITODataGridView = New System.Windows.Forms.DataGridView()
        Me.Column1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHAdegeneracionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvNotadecreditoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DetalleNOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBTextBox1 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.FOLIOTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.FECHATextBox = New System.Windows.Forms.TextBox()
        Me.BUSCANOTASDECREDITODataGridView = New System.Windows.Forms.DataGridView()
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHAdegeneracionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContratoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuariocapturaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuarioautorizoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechacaducidadDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ObservacionesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MotCanDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MUESTRATIPOFACTURABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.FacturaLabel1 = New System.Windows.Forms.Label()
        Me.SerieLabel1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Clv_FacturaLabel1 = New System.Windows.Forms.Label()
        Me.BUSCAFACTURASTableAdapter = New sofTV.NewsoftvDataSet1TableAdapters.BUSCAFACTURASTableAdapter()
        Me.CANCELACIONFACTURASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CANCELACIONFACTURASTableAdapter = New sofTV.NewsoftvDataSet1TableAdapters.CANCELACIONFACTURASTableAdapter()
        Me.DAMEFECHADELSERVIDORBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMEFECHADELSERVIDORTableAdapter = New sofTV.NewsoftvDataSet1TableAdapters.DAMEFECHADELSERVIDORTableAdapter()
        Me.MUESTRATIPOFACTURATableAdapter = New sofTV.DataSetLidia2TableAdapters.MUESTRATIPOFACTURATableAdapter()
        Me.BUSCANOTASDECREDITOTableAdapter = New sofTV.DataSetLidia2TableAdapters.BUSCANOTASDECREDITOTableAdapter()
        Me.DetalleNOTASDECREDITOTableAdapter = New sofTV.DataSetLidia2TableAdapters.DetalleNOTASDECREDITOTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        ImporteLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        ClienteLabel = New System.Windows.Forms.Label()
        FECHALabel = New System.Windows.Forms.Label()
        FacturaLabel = New System.Windows.Forms.Label()
        SerieLabel = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        CType(Me.BUSCAFACTURASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel2.SuspendLayout()
        Me.CMBPanel4.SuspendLayout()
        Me.CMBPanel3.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.BUSCANOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.BUSCANOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATIPOFACTURABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CANCELACIONFACTURASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImporteLabel
        '
        ImporteLabel.AutoSize = True
        ImporteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImporteLabel.ForeColor = System.Drawing.Color.White
        ImporteLabel.Location = New System.Drawing.Point(17, 219)
        ImporteLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ImporteLabel.Name = "ImporteLabel"
        ImporteLabel.Size = New System.Drawing.Size(75, 18)
        ImporteLabel.TabIndex = 35
        ImporteLabel.Text = "Importe :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.ForeColor = System.Drawing.Color.White
        NOMBRELabel.Location = New System.Drawing.Point(15, 158)
        NOMBRELabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(78, 18)
        NOMBRELabel.TabIndex = 34
        NOMBRELabel.Text = "Nombre :"
        '
        'ClienteLabel
        '
        ClienteLabel.AutoSize = True
        ClienteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClienteLabel.ForeColor = System.Drawing.Color.White
        ClienteLabel.Location = New System.Drawing.Point(11, 129)
        ClienteLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ClienteLabel.Name = "ClienteLabel"
        ClienteLabel.Size = New System.Drawing.Size(84, 18)
        ClienteLabel.TabIndex = 33
        ClienteLabel.Text = "Contrato :"
        '
        'FECHALabel
        '
        FECHALabel.AutoSize = True
        FECHALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FECHALabel.ForeColor = System.Drawing.Color.White
        FECHALabel.Location = New System.Drawing.Point(31, 101)
        FECHALabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        FECHALabel.Name = "FECHALabel"
        FECHALabel.Size = New System.Drawing.Size(64, 18)
        FECHALabel.TabIndex = 32
        FECHALabel.Text = "Fecha :"
        '
        'FacturaLabel
        '
        FacturaLabel.AutoSize = True
        FacturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FacturaLabel.ForeColor = System.Drawing.Color.White
        FacturaLabel.Location = New System.Drawing.Point(31, 73)
        FacturaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        FacturaLabel.Name = "FacturaLabel"
        FacturaLabel.Size = New System.Drawing.Size(56, 18)
        FacturaLabel.TabIndex = 30
        FacturaLabel.Text = "Folio :"
        '
        'SerieLabel
        '
        SerieLabel.AutoSize = True
        SerieLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SerieLabel.ForeColor = System.Drawing.Color.White
        SerieLabel.Location = New System.Drawing.Point(37, 44)
        SerieLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        SerieLabel.Name = "SerieLabel"
        SerieLabel.Size = New System.Drawing.Size(57, 18)
        SerieLabel.TabIndex = 29
        SerieLabel.Text = "Serie :"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.White
        Label5.Location = New System.Drawing.Point(31, 245)
        Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(61, 18)
        Label5.TabIndex = 101
        Label5.Text = "Status:"
        AddHandler Label5.Click, AddressOf Me.Label5_Click
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.White
        Label10.Location = New System.Drawing.Point(89, 151)
        Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(61, 18)
        Label10.TabIndex = 101
        Label10.Text = "Saldo :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.White
        Label12.Location = New System.Drawing.Point(85, 123)
        Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(66, 18)
        Label12.TabIndex = 35
        Label12.Text = "Monto :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.White
        Label14.Location = New System.Drawing.Point(75, 91)
        Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(64, 18)
        Label14.TabIndex = 34
        Label14.Text = "Ticket :"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.White
        Label15.Location = New System.Drawing.Point(67, 59)
        Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(84, 18)
        Label15.TabIndex = 33
        Label15.Text = "Contrato :"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.White
        Label17.Location = New System.Drawing.Point(87, 183)
        Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(64, 18)
        Label17.TabIndex = 32
        Label17.Text = "Fecha :"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.White
        Label19.Location = New System.Drawing.Point(5, 31)
        Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(137, 18)
        Label19.TabIndex = 29
        Label19.Text = "Nota de Crédito :"
        '
        'ImporteLabel1
        '
        Me.ImporteLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "importe", True))
        Me.ImporteLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteLabel1.ForeColor = System.Drawing.Color.White
        Me.ImporteLabel1.Location = New System.Drawing.Point(111, 219)
        Me.ImporteLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ImporteLabel1.Name = "ImporteLabel1"
        Me.ImporteLabel1.Size = New System.Drawing.Size(225, 18)
        Me.ImporteLabel1.TabIndex = 36
        '
        'BUSCAFACTURASBindingSource
        '
        Me.BUSCAFACTURASBindingSource.DataMember = "BUSCAFACTURAS"
        Me.BUSCAFACTURASBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ClienteLabel1
        '
        Me.ClienteLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "cliente", True))
        Me.ClienteLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClienteLabel1.ForeColor = System.Drawing.Color.White
        Me.ClienteLabel1.Location = New System.Drawing.Point(111, 129)
        Me.ClienteLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ClienteLabel1.Name = "ClienteLabel1"
        Me.ClienteLabel1.Size = New System.Drawing.Size(133, 28)
        Me.ClienteLabel1.TabIndex = 34
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(4, 4)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 102)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "&Cancelar Ticket"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'FECHALabel1
        '
        Me.FECHALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "FECHA", True))
        Me.FECHALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHALabel1.ForeColor = System.Drawing.Color.White
        Me.FECHALabel1.Location = New System.Drawing.Point(111, 101)
        Me.FECHALabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.FECHALabel1.Name = "FECHALabel1"
        Me.FECHALabel1.Size = New System.Drawing.Size(133, 28)
        Me.FECHALabel1.TabIndex = 33
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Status})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(740, 864)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.TabStop = False
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(17, 156)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(234, 29)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar Ticket Por :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(19, 202)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 18)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Serie :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(19, 302)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 18)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Fecha :"
        '
        'CMBNOMBRETextBox1
        '
        Me.CMBNOMBRETextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNOMBRETextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNOMBRETextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "NOMBRE", True))
        Me.CMBNOMBRETextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBNOMBRETextBox1.Location = New System.Drawing.Point(111, 161)
        Me.CMBNOMBRETextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBNOMBRETextBox1.Multiline = True
        Me.CMBNOMBRETextBox1.Name = "CMBNOMBRETextBox1"
        Me.CMBNOMBRETextBox1.ReadOnly = True
        Me.CMBNOMBRETextBox1.Size = New System.Drawing.Size(240, 54)
        Me.CMBNOMBRETextBox1.TabIndex = 100
        Me.CMBNOMBRETextBox1.TabStop = False
        '
        'CMBPanel2
        '
        Me.CMBPanel2.Controls.Add(Me.Button2)
        Me.CMBPanel2.Location = New System.Drawing.Point(1143, 36)
        Me.CMBPanel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBPanel2.Name = "CMBPanel2"
        Me.CMBPanel2.Size = New System.Drawing.Size(191, 110)
        Me.CMBPanel2.TabIndex = 31
        '
        'FECHADateTimePicker
        '
        Me.FECHADateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FECHADateTimePicker.Location = New System.Drawing.Point(5, 778)
        Me.FECHADateTimePicker.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FECHADateTimePicker.Name = "FECHADateTimePicker"
        Me.FECHADateTimePicker.Size = New System.Drawing.Size(135, 22)
        Me.FECHADateTimePicker.TabIndex = 34
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Orange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(4, 22)
        Me.Button8.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(181, 102)
        Me.Button8.TabIndex = 0
        Me.Button8.Text = "&Ver  Ticket"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(1152, 839)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 1
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Orange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(13, 4)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 102)
        Me.Button6.TabIndex = 0
        Me.Button6.Text = "&ReImprimir  Ticket"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'CMBPanel4
        '
        Me.CMBPanel4.Controls.Add(Me.Button8)
        Me.CMBPanel4.Location = New System.Drawing.Point(1148, 203)
        Me.CMBPanel4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBPanel4.Name = "CMBPanel4"
        Me.CMBPanel4.Size = New System.Drawing.Size(195, 158)
        Me.CMBPanel4.TabIndex = 33
        '
        'CMBPanel3
        '
        Me.CMBPanel3.Controls.Add(Me.Button6)
        Me.CMBPanel3.Location = New System.Drawing.Point(1135, 153)
        Me.CMBPanel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBPanel3.Name = "CMBPanel3"
        Me.CMBPanel3.Size = New System.Drawing.Size(212, 110)
        Me.CMBPanel3.TabIndex = 32
        '
        'SERIETextBox
        '
        Me.SERIETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SERIETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SERIETextBox.Location = New System.Drawing.Point(92, 199)
        Me.SERIETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SERIETextBox.Name = "SERIETextBox"
        Me.SERIETextBox.Size = New System.Drawing.Size(117, 24)
        Me.SERIETextBox.TabIndex = 3
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(19, 64)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(117, 28)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "&Buscar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(15, 9)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 18)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Contrato :"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(19, 176)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(117, 28)
        Me.Button4.TabIndex = 11
        Me.Button4.Text = "&Buscar"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(11, 36)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label22)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBoxCompanias)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.FOLIOTextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.FECHADateTimePicker)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBox4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.FECHATextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.SERIETextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.BUSCANOTASDECREDITODataGridView)
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(1115, 864)
        Me.SplitContainer1.SplitterDistance = 370
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 30
        Me.SplitContainer1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel2.Controls.Add(Label10)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.DetalleNOTASDECREDITODataGridView)
        Me.Panel2.Controls.Add(Me.CMBTextBox1)
        Me.Panel2.Controls.Add(Label12)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Label14)
        Me.Panel2.Controls.Add(Label15)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Label17)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Label19)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.Label21)
        Me.Panel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(9, 529)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(363, 368)
        Me.Panel2.TabIndex = 34
        Me.Panel2.Visible = False
        '
        'Label11
        '
        Me.Label11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "Saldo", True))
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(183, 151)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(143, 28)
        Me.Label11.TabIndex = 102
        '
        'BUSCANOTASDECREDITOBindingSource
        '
        Me.BUSCANOTASDECREDITOBindingSource.DataMember = "BUSCANOTASDECREDITO"
        Me.BUSCANOTASDECREDITOBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DetalleNOTASDECREDITODataGridView
        '
        Me.DetalleNOTASDECREDITODataGridView.AutoGenerateColumns = False
        Me.DetalleNOTASDECREDITODataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1DataGridViewTextBoxColumn, Me.MontoDataGridViewTextBoxColumn, Me.FECHAdegeneracionDataGridViewTextBoxColumn, Me.ClvNotadecreditoDataGridViewTextBoxColumn})
        Me.DetalleNOTASDECREDITODataGridView.DataSource = Me.DetalleNOTASDECREDITOBindingSource
        Me.DetalleNOTASDECREDITODataGridView.Location = New System.Drawing.Point(0, 212)
        Me.DetalleNOTASDECREDITODataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DetalleNOTASDECREDITODataGridView.Name = "DetalleNOTASDECREDITODataGridView"
        Me.DetalleNOTASDECREDITODataGridView.Size = New System.Drawing.Size(364, 155)
        Me.DetalleNOTASDECREDITODataGridView.TabIndex = 47
        '
        'Column1DataGridViewTextBoxColumn
        '
        Me.Column1DataGridViewTextBoxColumn.DataPropertyName = "Column1"
        Me.Column1DataGridViewTextBoxColumn.HeaderText = "Ticket"
        Me.Column1DataGridViewTextBoxColumn.Name = "Column1DataGridViewTextBoxColumn"
        Me.Column1DataGridViewTextBoxColumn.ReadOnly = True
        '
        'MontoDataGridViewTextBoxColumn
        '
        Me.MontoDataGridViewTextBoxColumn.DataPropertyName = "monto"
        Me.MontoDataGridViewTextBoxColumn.HeaderText = "Monto"
        Me.MontoDataGridViewTextBoxColumn.Name = "MontoDataGridViewTextBoxColumn"
        '
        'FECHAdegeneracionDataGridViewTextBoxColumn
        '
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.DataPropertyName = "FECHA_degeneracion"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.HeaderText = "Fecha"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.Name = "FECHAdegeneracionDataGridViewTextBoxColumn"
        '
        'ClvNotadecreditoDataGridViewTextBoxColumn
        '
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.DataPropertyName = "clv_Notadecredito"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.HeaderText = "Folio Nota de Crédito"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.Name = "ClvNotadecreditoDataGridViewTextBoxColumn"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DetalleNOTASDECREDITOBindingSource
        '
        Me.DetalleNOTASDECREDITOBindingSource.DataMember = "DetalleNOTASDECREDITO"
        Me.DetalleNOTASDECREDITOBindingSource.DataSource = Me.DataSetLidia2
        '
        'CMBTextBox1
        '
        Me.CMBTextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "Column1", True))
        Me.CMBTextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox1.Location = New System.Drawing.Point(187, 91)
        Me.CMBTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTextBox1.Multiline = True
        Me.CMBTextBox1.Name = "CMBTextBox1"
        Me.CMBTextBox1.ReadOnly = True
        Me.CMBTextBox1.Size = New System.Drawing.Size(143, 28)
        Me.CMBTextBox1.TabIndex = 100
        Me.CMBTextBox1.TabStop = False
        '
        'Label13
        '
        Me.Label13.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "monto", True))
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(187, 123)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(143, 28)
        Me.Label13.TabIndex = 36
        '
        'Label16
        '
        Me.Label16.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "contrato", True))
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(187, 59)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(143, 28)
        Me.Label16.TabIndex = 34
        '
        'Label18
        '
        Me.Label18.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "FECHA_degeneracion", True))
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(187, 180)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(169, 28)
        Me.Label18.TabIndex = 33
        '
        'Label20
        '
        Me.Label20.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "clv_Notadecredito", True))
        Me.Label20.ForeColor = System.Drawing.Color.DarkOrange
        Me.Label20.Location = New System.Drawing.Point(183, 28)
        Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(143, 28)
        Me.Label20.TabIndex = 0
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(5, 4)
        Me.Label21.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(266, 24)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Datos de la Nota de Crédito"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label22.Location = New System.Drawing.Point(17, 7)
        Me.Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(163, 29)
        Me.Label22.TabIndex = 94
        Me.Label22.Text = "Distribuidor :"
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(19, 48)
        Me.ComboBoxCompanias.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(317, 28)
        Me.ComboBoxCompanias.TabIndex = 93
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(168, 324)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(169, 21)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Ej. : dd/mm/aa"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Button3)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.Button4)
        Me.Panel5.Controls.Add(Me.CONTRATOTextBox)
        Me.Panel5.Controls.Add(Me.NOMBRETextBox)
        Me.Panel5.Controls.Add(Me.Label7)
        Me.Panel5.Location = New System.Drawing.Point(4, 391)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(363, 188)
        Me.Panel5.TabIndex = 28
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CONTRATOTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CONTRATOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(19, 31)
        Me.CONTRATOTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(117, 24)
        Me.CONTRATOTextBox.TabIndex = 8
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NOMBRETextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(19, 143)
        Me.NOMBRETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(331, 24)
        Me.NOMBRETextBox.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(15, 121)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 18)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Nombre :"
        '
        'FOLIOTextBox
        '
        Me.FOLIOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FOLIOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FOLIOTextBox.Location = New System.Drawing.Point(92, 233)
        Me.FOLIOTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FOLIOTextBox.Name = "FOLIOTextBox"
        Me.FOLIOTextBox.Size = New System.Drawing.Size(117, 24)
        Me.FOLIOTextBox.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(19, 235)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 18)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Folio :"
        '
        'ComboBox4
        '
        Me.ComboBox4.DisplayMember = "CLAVE"
        Me.ComboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(23, 119)
        Me.ComboBox4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(313, 28)
        Me.ComboBox4.TabIndex = 2
        Me.ComboBox4.ValueMember = "CLAVE"
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel5.Location = New System.Drawing.Point(17, 85)
        Me.CMBLabel5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(204, 29)
        Me.CMBLabel5.TabIndex = 15
        Me.CMBLabel5.Text = "Tipo de Ticket  :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(23, 357)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(117, 28)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(23, 266)
        Me.Button7.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(117, 28)
        Me.Button7.TabIndex = 5
        Me.Button7.Text = "&Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'FECHATextBox
        '
        Me.FECHATextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FECHATextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FECHATextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHATextBox.Location = New System.Drawing.Point(23, 324)
        Me.FECHATextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FECHATextBox.Name = "FECHATextBox"
        Me.FECHATextBox.Size = New System.Drawing.Size(117, 24)
        Me.FECHATextBox.TabIndex = 6
        '
        'BUSCANOTASDECREDITODataGridView
        '
        Me.BUSCANOTASDECREDITODataGridView.AllowUserToAddRows = False
        Me.BUSCANOTASDECREDITODataGridView.AllowUserToDeleteRows = False
        Me.BUSCANOTASDECREDITODataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BUSCANOTASDECREDITODataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.BUSCANOTASDECREDITODataGridView.ColumnHeadersHeight = 35
        Me.BUSCANOTASDECREDITODataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvNotadecreditoDataGridViewTextBoxColumn1, Me.StatusDataGridViewTextBoxColumn, Me.Column1DataGridViewTextBoxColumn1, Me.FECHAdegeneracionDataGridViewTextBoxColumn1, Me.ContratoDataGridViewTextBoxColumn, Me.MontoDataGridViewTextBoxColumn1, Me.UsuariocapturaDataGridViewTextBoxColumn, Me.UsuarioautorizoDataGridViewTextBoxColumn, Me.FechacaducidadDataGridViewTextBoxColumn, Me.ObservacionesDataGridViewTextBoxColumn, Me.MotCanDataGridViewTextBoxColumn, Me.ClvFacturaAplicadaDataGridViewTextBoxColumn, Me.SaldoDataGridViewTextBoxColumn})
        Me.BUSCANOTASDECREDITODataGridView.DataSource = Me.BUSCANOTASDECREDITOBindingSource
        Me.BUSCANOTASDECREDITODataGridView.Location = New System.Drawing.Point(0, 0)
        Me.BUSCANOTASDECREDITODataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BUSCANOTASDECREDITODataGridView.Name = "BUSCANOTASDECREDITODataGridView"
        Me.BUSCANOTASDECREDITODataGridView.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BUSCANOTASDECREDITODataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.BUSCANOTASDECREDITODataGridView.RowHeadersVisible = False
        Me.BUSCANOTASDECREDITODataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.BUSCANOTASDECREDITODataGridView.Size = New System.Drawing.Size(739, 864)
        Me.BUSCANOTASDECREDITODataGridView.TabIndex = 30
        Me.BUSCANOTASDECREDITODataGridView.Visible = False
        '
        'ClvNotadecreditoDataGridViewTextBoxColumn1
        '
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1.DataPropertyName = "clv_Notadecredito"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1.HeaderText = "clv_Notadecredito"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1.Name = "ClvNotadecreditoDataGridViewTextBoxColumn1"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1.ReadOnly = True
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1.Visible = False
        '
        'StatusDataGridViewTextBoxColumn
        '
        Me.StatusDataGridViewTextBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewTextBoxColumn.Name = "StatusDataGridViewTextBoxColumn"
        Me.StatusDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Column1DataGridViewTextBoxColumn1
        '
        Me.Column1DataGridViewTextBoxColumn1.DataPropertyName = "Column1"
        Me.Column1DataGridViewTextBoxColumn1.HeaderText = "Nota de Crédito"
        Me.Column1DataGridViewTextBoxColumn1.Name = "Column1DataGridViewTextBoxColumn1"
        Me.Column1DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'FECHAdegeneracionDataGridViewTextBoxColumn1
        '
        Me.FECHAdegeneracionDataGridViewTextBoxColumn1.DataPropertyName = "FECHA_degeneracion"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn1.HeaderText = "Fecha"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn1.Name = "FECHAdegeneracionDataGridViewTextBoxColumn1"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'ContratoDataGridViewTextBoxColumn
        '
        Me.ContratoDataGridViewTextBoxColumn.DataPropertyName = "contrato"
        Me.ContratoDataGridViewTextBoxColumn.HeaderText = "Contrato"
        Me.ContratoDataGridViewTextBoxColumn.Name = "ContratoDataGridViewTextBoxColumn"
        Me.ContratoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MontoDataGridViewTextBoxColumn1
        '
        Me.MontoDataGridViewTextBoxColumn1.DataPropertyName = "monto"
        Me.MontoDataGridViewTextBoxColumn1.HeaderText = "Monto"
        Me.MontoDataGridViewTextBoxColumn1.Name = "MontoDataGridViewTextBoxColumn1"
        Me.MontoDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'UsuariocapturaDataGridViewTextBoxColumn
        '
        Me.UsuariocapturaDataGridViewTextBoxColumn.DataPropertyName = "Usuario_captura"
        Me.UsuariocapturaDataGridViewTextBoxColumn.HeaderText = "Usuario que generó"
        Me.UsuariocapturaDataGridViewTextBoxColumn.Name = "UsuariocapturaDataGridViewTextBoxColumn"
        Me.UsuariocapturaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UsuarioautorizoDataGridViewTextBoxColumn
        '
        Me.UsuarioautorizoDataGridViewTextBoxColumn.DataPropertyName = "Usuario_autorizo"
        Me.UsuarioautorizoDataGridViewTextBoxColumn.HeaderText = "Usuario_autorizo"
        Me.UsuarioautorizoDataGridViewTextBoxColumn.Name = "UsuarioautorizoDataGridViewTextBoxColumn"
        Me.UsuarioautorizoDataGridViewTextBoxColumn.ReadOnly = True
        Me.UsuarioautorizoDataGridViewTextBoxColumn.Visible = False
        '
        'FechacaducidadDataGridViewTextBoxColumn
        '
        Me.FechacaducidadDataGridViewTextBoxColumn.DataPropertyName = "fecha_caducidad"
        Me.FechacaducidadDataGridViewTextBoxColumn.HeaderText = "Fecha de caducidad"
        Me.FechacaducidadDataGridViewTextBoxColumn.Name = "FechacaducidadDataGridViewTextBoxColumn"
        Me.FechacaducidadDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ObservacionesDataGridViewTextBoxColumn
        '
        Me.ObservacionesDataGridViewTextBoxColumn.DataPropertyName = "observaciones"
        Me.ObservacionesDataGridViewTextBoxColumn.HeaderText = "observaciones"
        Me.ObservacionesDataGridViewTextBoxColumn.Name = "ObservacionesDataGridViewTextBoxColumn"
        Me.ObservacionesDataGridViewTextBoxColumn.ReadOnly = True
        Me.ObservacionesDataGridViewTextBoxColumn.Visible = False
        '
        'MotCanDataGridViewTextBoxColumn
        '
        Me.MotCanDataGridViewTextBoxColumn.DataPropertyName = "MotCan"
        Me.MotCanDataGridViewTextBoxColumn.HeaderText = "MotCan"
        Me.MotCanDataGridViewTextBoxColumn.Name = "MotCanDataGridViewTextBoxColumn"
        Me.MotCanDataGridViewTextBoxColumn.ReadOnly = True
        Me.MotCanDataGridViewTextBoxColumn.Visible = False
        '
        'ClvFacturaAplicadaDataGridViewTextBoxColumn
        '
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn.DataPropertyName = "Clv_Factura_Aplicada"
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn.HeaderText = "Clv_Factura_Aplicada"
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn.Name = "ClvFacturaAplicadaDataGridViewTextBoxColumn"
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn.Visible = False
        '
        'SaldoDataGridViewTextBoxColumn
        '
        Me.SaldoDataGridViewTextBoxColumn.DataPropertyName = "Saldo"
        Me.SaldoDataGridViewTextBoxColumn.HeaderText = "Saldo"
        Me.SaldoDataGridViewTextBoxColumn.Name = "SaldoDataGridViewTextBoxColumn"
        Me.SaldoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MUESTRATIPOFACTURABindingSource
        '
        Me.MUESTRATIPOFACTURABindingSource.DataMember = "MUESTRATIPOFACTURA"
        Me.MUESTRATIPOFACTURABindingSource.DataSource = Me.DataSetLidia2
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Label5)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.CMBNOMBRETextBox1)
        Me.Panel1.Controls.Add(ImporteLabel)
        Me.Panel1.Controls.Add(Me.ImporteLabel1)
        Me.Panel1.Controls.Add(NOMBRELabel)
        Me.Panel1.Controls.Add(ClienteLabel)
        Me.Panel1.Controls.Add(Me.ClienteLabel1)
        Me.Panel1.Controls.Add(FECHALabel)
        Me.Panel1.Controls.Add(Me.FECHALabel1)
        Me.Panel1.Controls.Add(FacturaLabel)
        Me.Panel1.Controls.Add(Me.FacturaLabel1)
        Me.Panel1.Controls.Add(SerieLabel)
        Me.Panel1.Controls.Add(Me.SerieLabel1)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(12, 620)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(363, 279)
        Me.Panel1.TabIndex = 27
        '
        'Label9
        '
        Me.Label9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "Status", True))
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(111, 245)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(225, 18)
        Me.Label9.TabIndex = 102
        '
        'FacturaLabel1
        '
        Me.FacturaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "Factura", True))
        Me.FacturaLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FacturaLabel1.ForeColor = System.Drawing.Color.White
        Me.FacturaLabel1.Location = New System.Drawing.Point(111, 73)
        Me.FacturaLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.FacturaLabel1.Name = "FacturaLabel1"
        Me.FacturaLabel1.Size = New System.Drawing.Size(133, 28)
        Me.FacturaLabel1.TabIndex = 32
        '
        'SerieLabel1
        '
        Me.SerieLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "Serie", True))
        Me.SerieLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieLabel1.ForeColor = System.Drawing.Color.White
        Me.SerieLabel1.Location = New System.Drawing.Point(111, 44)
        Me.SerieLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.SerieLabel1.Name = "SerieLabel1"
        Me.SerieLabel1.Size = New System.Drawing.Size(133, 28)
        Me.SerieLabel1.TabIndex = 31
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(5, 5)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(159, 24)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Datos del Ticket"
        '
        'Clv_FacturaLabel1
        '
        Me.Clv_FacturaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "clv_Factura", True))
        Me.Clv_FacturaLabel1.ForeColor = System.Drawing.Color.DarkOrange
        Me.Clv_FacturaLabel1.Location = New System.Drawing.Point(1204, 842)
        Me.Clv_FacturaLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Clv_FacturaLabel1.Name = "Clv_FacturaLabel1"
        Me.Clv_FacturaLabel1.Size = New System.Drawing.Size(77, 28)
        Me.Clv_FacturaLabel1.TabIndex = 28
        '
        'BUSCAFACTURASTableAdapter
        '
        Me.BUSCAFACTURASTableAdapter.ClearBeforeFill = True
        '
        'CANCELACIONFACTURASBindingSource
        '
        Me.CANCELACIONFACTURASBindingSource.DataMember = "CANCELACIONFACTURAS"
        Me.CANCELACIONFACTURASBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'CANCELACIONFACTURASTableAdapter
        '
        Me.CANCELACIONFACTURASTableAdapter.ClearBeforeFill = True
        '
        'DAMEFECHADELSERVIDORBindingSource
        '
        Me.DAMEFECHADELSERVIDORBindingSource.DataMember = "DAMEFECHADELSERVIDOR"
        Me.DAMEFECHADELSERVIDORBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'DAMEFECHADELSERVIDORTableAdapter
        '
        Me.DAMEFECHADELSERVIDORTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOFACTURATableAdapter
        '
        Me.MUESTRATIPOFACTURATableAdapter.ClearBeforeFill = True
        '
        'BUSCANOTASDECREDITOTableAdapter
        '
        Me.BUSCANOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'DetalleNOTASDECREDITOTableAdapter
        '
        Me.DetalleNOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Bwr_FacturasCancelar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 924)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.CMBPanel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CMBPanel4)
        Me.Controls.Add(Me.CMBPanel3)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Clv_FacturaLabel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "Bwr_FacturasCancelar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Historial de Pagos"
        CType(Me.BUSCAFACTURASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel2.ResumeLayout(False)
        Me.CMBPanel4.ResumeLayout(False)
        Me.CMBPanel3.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.BUSCANOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.BUSCANOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATIPOFACTURABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CANCELACIONFACTURASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ImporteLabel1 As System.Windows.Forms.Label
    Friend WithEvents ClienteLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents FECHALabel1 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CMBNOMBRETextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBPanel2 As System.Windows.Forms.Panel
    Friend WithEvents FECHADateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents CMBPanel4 As System.Windows.Forms.Panel
    Friend WithEvents CMBPanel3 As System.Windows.Forms.Panel
    Friend WithEvents SERIETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents FacturaLabel1 As System.Windows.Forms.Label
    Friend WithEvents SerieLabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_FacturaLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents FOLIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents FECHATextBox As System.Windows.Forms.TextBox
    Friend WithEvents NewsoftvDataSet1 As sofTV.NewsoftvDataSet1
    Friend WithEvents BUSCAFACTURASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCAFACTURASTableAdapter As sofTV.NewsoftvDataSet1TableAdapters.BUSCAFACTURASTableAdapter
    Friend WithEvents CANCELACIONFACTURASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CANCELACIONFACTURASTableAdapter As sofTV.NewsoftvDataSet1TableAdapters.CANCELACIONFACTURASTableAdapter
    Friend WithEvents DAMEFECHADELSERVIDORBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEFECHADELSERVIDORTableAdapter As sofTV.NewsoftvDataSet1TableAdapters.DAMEFECHADELSERVIDORTableAdapter
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents MUESTRATIPOFACTURABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATIPOFACTURATableAdapter As sofTV.DataSetLidia2TableAdapters.MUESTRATIPOFACTURATableAdapter
    Friend WithEvents BUSCANOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCANOTASDECREDITOTableAdapter As sofTV.DataSetLidia2TableAdapters.BUSCANOTASDECREDITOTableAdapter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents DetalleNOTASDECREDITODataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents CMBTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents DetalleNOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DetalleNOTASDECREDITOTableAdapter As sofTV.DataSetLidia2TableAdapters.DetalleNOTASDECREDITOTableAdapter
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHAdegeneracionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvNotadecreditoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents BUSCANOTASDECREDITODataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ClvNotadecreditoDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHAdegeneracionDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContratoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuariocapturaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuarioautorizoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechacaducidadDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ObservacionesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MotCanDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvFacturaAplicadaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SaldoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
