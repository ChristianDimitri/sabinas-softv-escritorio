﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Xml
Imports System.Net
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Public Class FrmDocumentosTecnico
    Dim llenaCapacitacion As Boolean = False

    Private Sub GuardaCapacitacion()
        Try

        
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@estado", SqlDbType.Bit, cbCapacitacion.CheckState)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.CreateMyParameter("@clv_tecnico", SqlDbType.Int, GloClvTecnico)
        BaseII.Inserta("ModCapacitacionTecnico")

        Catch ex As Exception

        End Try

        If llenaCapacitacion Then
            llenaCapacitacion = False
            Exit Sub
        End If
        If GloClvTecnico = 0 Then
            'MsgBox("Necesita seleccionar un técnico para poder marcar/desmarcar la casilla.")
            cbCapacitacion.Checked = False
            Exit Sub
        End If
        Try
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Modifico la Opcion de Capacitación", "", "Opcion de Capacitación del Tecnico: " + Me.tbNombre.Text + "Cambio a : " + CStr(cbCapacitacion.CheckState), LocClv_Ciudad)


        Catch ex As Exception

        End Try
    End Sub
    Private Sub FrmDocumentosTecnico_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaCapacitacion = False
        If GloTipoUsuario <> 40 Then
            cbCapacitacion.Enabled = False
        End If
        BaseII.limpiaParametros()
        ComboBoxCompanias.ValueMember = "IdDocumento"
        ComboBoxCompanias.DisplayMember = "Nombre"
        ComboBoxCompanias.DataSource = BaseII.ConsultaDT("DameDocumentosTecnico")
        LlenaGrid()
        Button1.BackColor = Button2.BackColor
        Button1.ForeColor = Button2.ForeColor
        Button5.BackColor = Button2.BackColor
        Button5.ForeColor = Button2.ForeColor
    End Sub

    Private Sub llenaGrid()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_tecnico", SqlDbType.BigInt, GloClvTecnico)
            Grid.DataSource = BaseII.ConsultaDT("DameDocumentosTecnicoGrid")

        Catch ex As Exception

        End Try

        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_tecnico", SqlDbType.Int, GloClvTecnico)
            BaseII.CreateMyParameter("@estado", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("DameCapacitacionTecnico")
            llenaCapacitacion = True
            cbCapacitacion.Checked = BaseII.dicoPar("@estado")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GroupBox1_Enter(sender As System.Object, e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Dim ofd As New OpenFileDialog()
        ofd.Title = "Selecciona un archivo."
        ofd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        ofd.FilterIndex = 1
        ofd.RestoreDirectory = True
        If ofd.ShowDialog() = DialogResult.OK Then
            Dim fi As FileInfo = New FileInfo(ofd.FileName)
            If ofd.FileName.Contains(".pdf") Or ofd.FileName.Contains(".PDF") Then
                If fi.Length > 1500000 Then
                    MsgBox("Archivo muy grande, no soportado por el sistema")
                    Exit Sub
                End If
                pdfViewer.Visible = True
                PictureBox1.Visible = False
                pdfViewer.Navigate(ofd.FileName)
                tbNombre.Text = ofd.FileName
            Else
                MsgBox("Formato no soportado por el sistema.")
            End If
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If tbNombre.Text = "" Then
            MsgBox("Debe seleccionar un archivo.")
            Exit Sub
        End If
        Try
            GuardaCapacitacion()
            If pdfViewer.Visible Then
                Dim fInfo As New FileInfo(tbNombre.Text)
                Dim numBytes As Long = fInfo.Length
                Dim fStream As New FileStream(tbNombre.Text, FileMode.Open, FileAccess.Read)
                Dim br As New BinaryReader(fStream)
                Dim data As Byte() = br.ReadBytes(CInt(numBytes))
                br.Close()
                fStream.Close()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@iddocumento", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                BaseII.CreateMyParameter("@clv_tecnico", SqlDbType.Int, GloClvTecnico)
                BaseII.CreateMyParameter("@archivo", SqlDbType.VarBinary, data)
                BaseII.Inserta("GuardaDocumentoPDFTecnico")
            Else
                Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()
                PictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@iddocumento", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                BaseII.CreateMyParameter("@clv_tecnico", SqlDbType.Int, GloClvTecnico)
                BaseII.CreateMyParameter("@archivo", SqlDbType.Image, ms.GetBuffer())
                BaseII.Inserta("GuardaDocumentoImageTecnico")
            End If
            MsgBox("Documento guardado con éxito.")
            llenaGrid()
            tbNombre.Text = ""
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Grid_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles Grid.SelectionChanged
        Try
            If Grid.SelectedCells(2).Value = 1 And Grid.Rows.Count <> 0 Then

                Dim conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim comando As New SqlCommand()
                comando.Connection = conexion
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@dimeTipoDocumento", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@idDocumento", SqlDbType.BigInt, Grid.SelectedCells(0).Value.ToString)
                BaseII.CreateMyParameter("@clv_tecnico", SqlDbType.BigInt, GloClvTecnico)
                BaseII.ProcedimientoOutPut("DimeTipoDocumentoTecnico")
                If BaseII.dicoPar("@dimeTipoDocumento") = 1 Then 'image
                    comando.CommandText = "select imagen from DocumentosTecnico where clv_tecnico=" + GloClvTecnico.ToString + " and iddocumento=" + Grid.SelectedCells(0).Value.ToString
                    Dim reader As SqlDataReader = comando.ExecuteReader
                    reader.Read()
                    Dim by(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
                    reader.GetBytes(0, 0, by, 0, by.Length)
                    Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(by)
                    PictureBox1.Image = Image.FromStream(ms)
                    PictureBox1.Visible = True
                    pdfViewer.Visible = False
                    reader.Close()
                Else
                    Dim ToSaveFileTo As String = RutaReportes + "\ReportTemp.doc"
                    comando.CommandText = "select archivo from DocumentosPDFTecnico where clv_tecnico=" + GloClvTecnico.ToString + " and iddocumento=" + Grid.SelectedCells(0).Value.ToString
                    Dim reader As SqlDataReader = comando.ExecuteReader(System.Data.CommandBehavior.Default)
                    reader.Read()
                    Dim contents(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
                    'contents = reader.GetBytes(0, 0, contents, 0, contents.Length)
                    contents = reader.GetValue(0)
                    Dim fileName As String = System.IO.Path.GetTempFileName() + ".pdf"
                    File.WriteAllBytes(fileName, contents)
                    reader.Close()
                    PictureBox1.Visible = False
                    pdfViewer.Visible = True
                    pdfViewer.Navigate(fileName)
                    reader.Close()
                End If
                conexion.Close()
            Else
                PictureBox1.Visible = True
                pdfViewer.Visible = False
                PictureBox1.Image = sofTV.My.Resources.Resources.filenotfound4041
            End If
        Catch ex As Exception


        End Try
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub cbCapacitacion_CheckedChanged_1(sender As Object, e As EventArgs) Handles cbCapacitacion.CheckedChanged
        GuardaCapacitacion()
    End Sub
End Class