﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.IO

Public Class FrmTarifasPagare

    Private Sub LlenaServicios(LocClv_TipSer As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_tipser", SqlDbType.Int, LocClv_TipSer)
        CBoxSERVICIO.DataSource = BaseII.ConsultaDT("MuestraServiciosRentas")
        ChecaTelefonosDisponibles()
        CBoxSERVICIO.DisplayMember = "SERVICIO"
        CBoxSERVICIO.ValueMember = "CLV_SERVICIO"
    End Sub

    'Private Sub Llena_companias()
    '    Try
    '        BaseII.limpiaParametros()
    '        'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
    '        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
    '        ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
    '        ComboBoxCompanias.DisplayMember = "razon_social"
    '        ComboBoxCompanias.ValueMember = "id_compania"

    '        If ComboBoxCompanias.Items.Count > 1 Then
    '            ComboBoxCompanias.SelectedValue = 0

    '        End If
    '        GloIdCompania = 0
    '        'ComboBoxCiudades.Text = ""
    '    Catch ex As Exception

    '    End Try
    'ñ
    'End Sub
    Private Sub FrmTarifasPagare_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        MuestraTipoServicioPagare(2)
        MuestraMarcasAparatosPagare()
        MuestraServiciosRentas()
        'Llena_companias()
        If OpPagare = "N" Then
            'Me.ComboBoxCompanias.Enabled = True
            Me.TipServCombo.Enabled = True
            'Me.NormalRadioBut.Enabled = False
            'Me.HDRadioBut.Enabled = False
            Me.AparatoCombo.Enabled = True
            Me.CBoxSERVICIO.Enabled = True
        ElseIf OpPagare = "M" Then
            ConCostosPagare()
            'Me.ComboBoxCompanias.Enabled = False
            Me.TipServCombo.Enabled = False
            'Me.NormalRadioBut.Enabled = False
            'Me.HDRadioBut.Enabled = False
            Me.AparatoCombo.Enabled = False
            Me.CBoxSERVICIO.Enabled = False

        End If
    End Sub

    Private Sub MuestraTipoServicioPagare(ByVal OPCION As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraTipoServicioPagare ")
        StrSQL.Append(CStr(OPCION))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            TipServCombo.DataSource = BS.DataSource
            If TipServCombo.Items.Count > 0 Then
                LlenaServicios(TipServCombo.SelectedValue)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub MuestraMarcasAparatosPagare()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraMarcasAparatosPagare ")
        StrSQL.Append(CStr(TipServCombo.SelectedValue) & ", ")
        If Me.NormalRadioBut.Checked Then
            StrSQL.Append(CStr("1"))
        ElseIf Me.HDRadioBut.Checked Then
            StrSQL.Append(CStr("2"))
        Else
            StrSQL.Append(CStr("0"))
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            AparatoCombo.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub

    Private Sub GuardaCostosPagare()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("GuardaCostosPagare", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ID", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = IdPagare
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@TIPSER", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = Me.TipServCombo.SelectedValue
        CMD.Parameters.Add(PRM2)

        Dim PRM4 As New SqlParameter("@IDARTICULO", SqlDbType.Int)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = Me.AparatoCombo.SelectedValue
        CMD.Parameters.Add(PRM4)

        Dim PRM5 As New SqlParameter("@COSTOARTICULO", SqlDbType.Money)
        PRM5.Direction = ParameterDirection.Input
        PRM5.Value = CostoText.Text
        CMD.Parameters.Add(PRM5)

        Dim PRM8 As New SqlParameter("@RENTAPRINCIPAL", SqlDbType.Money)
        PRM8.Direction = ParameterDirection.Input
        PRM8.Value = Me.RentaPrincipalText.Text
        CMD.Parameters.Add(PRM8)

        If IsNumeric(RentaAdicionalText.Text) = False Then RentaAdicionalText.Text = 0
        Dim PRM9 As New SqlParameter("@RENTAADICIONAL", SqlDbType.Money)
        PRM9.Direction = ParameterDirection.Input
        PRM9.Value = Me.RentaAdicionalText.Text
        CMD.Parameters.Add(PRM9)

        Dim PRM6 As New SqlParameter("@OPCION", SqlDbType.VarChar, 1)
        PRM6.Direction = ParameterDirection.Input
        PRM6.Value = OpPagare
        CMD.Parameters.Add(PRM6)

        Dim PRM7 As New SqlParameter("@EXISTENCIA", SqlDbType.Bit)
        PRM7.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM7)

        Dim PRM10 As New SqlParameter("@idcompania", SqlDbType.Int)
        PRM10.Direction = ParameterDirection.Input
        PRM10.Value = 1 'ComboBoxCompanias.SelectedValue
        CMD.Parameters.Add(PRM10)

        If IsNumeric(RentaAdicionalText2.Text) = False Then RentaAdicionalText2.Text = 0
        Dim PRM92 As New SqlParameter("@RENTAADICIONAL2", SqlDbType.Money)
        PRM92.Direction = ParameterDirection.Input
        PRM92.Value = Me.RentaAdicionalText2.Text
        CMD.Parameters.Add(PRM92)

        If IsNumeric(RentaAdicionalText3.Text) = False Then RentaAdicionalText3.Text = 0
        Dim PRM93 As New SqlParameter("@RENTAADICIONAL3", SqlDbType.Money)
        PRM93.Direction = ParameterDirection.Input
        PRM93.Value = Me.RentaAdicionalText3.Text
        CMD.Parameters.Add(PRM93)

        Dim PRM110 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        PRM110.Direction = ParameterDirection.Input
        PRM110.Value = Me.CBoxSERVICIO.SelectedValue
        CMD.Parameters.Add(PRM110)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            If PRM7.Value = True Then
                MsgBox("El registro ya fue dado de alta anteriormente", MsgBoxStyle.Information)
                Exit Sub
            Else
                MsgBox("Registro almacenado Satisfactoriamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Dispose()
            CON.Close()
        End Try
    End Sub

    Private Sub ConCostosPagare()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ConCostosPagare", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@OP", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = 2
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@TIPSER", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = 0
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@ID", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = IdPagare
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@idcompania", SqlDbType.Int)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = 0
        CMD.Parameters.Add(PRM4)
        Dim SqlReader As SqlDataReader

        'Dim PRM5 As New SqlParameter("@Servicio", SqlDbType.Int)
        'PRM5.Direction = ParameterDirection.Input
        'PRM5.Value = Tipo_Servicio
        'CMD.Parameters.Add(PRM5)

        Try
            CON.Open()
            SqlReader = CMD.ExecuteReader

            While (SqlReader.Read)
                Me.TipServCombo.SelectedValue = SqlReader(0).ToString
                Me.AparatoCombo.SelectedValue = SqlReader(1).ToString
                Me.CostoText.Text = SqlReader(2).ToString
                Me.CostoText.Text = CType(Me.CostoText.Text, Double)
                Me.RentaPrincipalText.Text = SqlReader(3).ToString
                Me.RentaPrincipalText.Text = CType(Me.RentaPrincipalText.Text, Double)
                Me.RentaAdicionalText.Text = SqlReader(4).ToString
                Me.RentaAdicionalText.Text = CType(Me.RentaAdicionalText.Text, Double)

                'Me.ComboBoxCompanias.SelectedValue = SqlReader(6)
                Me.RentaAdicionalText2.Text = CType(SqlReader(7).ToString, Double)
                Me.RentaAdicionalText3.Text = CType(SqlReader(8).ToString, Double)
                Me.CBoxSERVICIO.Text = SqlReader(9).ToString
                If SqlReader(5).ToString = "False" Then
                    NormalRadioBut.Checked = True
                Else
                    HDRadioBut.Checked = True
                End If
                'ComBoxServicio.Text = SqlReader(9).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Dispose()
            CON.Close()
        End Try
    End Sub

    Private Sub TipServCombo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipServCombo.SelectedIndexChanged
        If Me.TipServCombo.SelectedValue = 2 Then
            'Me.NormalRadioBut.Visible = False
            'Me.HDRadioBut.Visible = False
            'Me.CMBLabel3.Visible = False
            CMBLabel7.Visible = False
            RentaAdicionalText.Visible = False
            RentaAdicionalText2.Visible = False
            RentaAdicionalText3.Visible = False
            Label2.Visible = False
            Label3.Visible = False
            CMBLabel4.Text = "Aparato"
        Else
            'Me.NormalRadioBut.Visible = True
            'Me.HDRadioBut.Visible = True
            'Me.CMBLabel3.Visible = True
            CMBLabel7.Visible = True
            RentaAdicionalText.Visible = True
            RentaAdicionalText2.Visible = True
            RentaAdicionalText3.Visible = True
            Label2.Visible = True
            Label3.Visible = True
            CMBLabel4.Text = "Setup Box"
        End If
        LlenaServicios(TipServCombo.SelectedValue)
        MuestraMarcasAparatosPagare()
    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Me.Close()
    End Sub



    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        'If ComboBoxCompanias.SelectedValue = 0 Then
        '    MsgBox("Seleccione una Compañía")
        '    Exit Sub
        'End If
        If Me.CostoText.TextLength = 0 Then
            MsgBox("Capture el costo del Aparato", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If Me.RentaPrincipalText.TextLength = 0 Then
            MsgBox("Capture el costo de la renta mensual del Aparato Principal", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If Me.RentaAdicionalText.TextLength = 0 And TipServCombo.SelectedValue = 3 Then
            MsgBox("Capture el costo de la renta mensual del Aparato Adicional", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If Me.AparatoCombo.SelectedValue > 0 Then
            GuardaCostosPagare()
            BndActualizaPagare = True
            Me.Close()
        Else
            MsgBox("No existen aparatos en la Lista", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
    End Sub

    Private Sub NormalRadioBut_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NormalRadioBut.CheckedChanged
        'If Me.Visible = True Then
        '    Me.AparatoCombo.SelectedValue = 0
        '    MuestraMarcasAparatosPagare()
        'End If
    End Sub

    Private Sub HDRadioBut_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HDRadioBut.CheckedChanged
        'If Me.Visible = True Then
        '    Me.AparatoCombo.SelectedValue = 0
        '    MuestraMarcasAparatosPagare()
        'End If
    End Sub

    Private Sub CMBLabel7_Click(sender As System.Object, e As System.EventArgs) Handles CMBLabel7.Click

    End Sub

    Private Sub MuestraServiciosRentas()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, TipServCombo.SelectedValue)
            CBoxSERVICIO.DataSource = BaseII.ConsultaDT("MuestraServiciosRentas")
            CBoxSERVICIO.DisplayMember = "Servicio"
            CBoxSERVICIO.ValueMember = "CLV_SERVICIO"

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub


    Private Sub ComBoxServicio_SelectedValueChanged(sender As Object, e As EventArgs) Handles CBoxSERVICIO.SelectedValueChanged


    End Sub

    Private Sub ComBoxServicio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBoxSERVICIO.SelectedIndexChanged

    End Sub
End Class