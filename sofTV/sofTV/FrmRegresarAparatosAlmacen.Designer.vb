﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRegresarAparatosAlmacen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bnOrdenBuscar = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.tbOrden = New System.Windows.Forms.TextBox()
        Me.cbTipoAparato = New System.Windows.Forms.ComboBox()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.CMBLabel6 = New System.Windows.Forms.Label()
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.tbMac = New System.Windows.Forms.TextBox()
        Me.tbFolio = New System.Windows.Forms.TextBox()
        Me.tbSerie = New System.Windows.Forms.TextBox()
        Me.bnUsuarioBuscar = New System.Windows.Forms.Button()
        Me.bnMacBuscar = New System.Windows.Forms.Button()
        Me.bnTipoAparatoBuscar = New System.Windows.Forms.Button()
        Me.bnSerieFolioBuscar = New System.Windows.Forms.Button()
        Me.cbUsuario = New System.Windows.Forms.ComboBox()
        Me.dgvD = New System.Windows.Forms.DataGridView()
        Me.Clv_Orden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoAparato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Cablemodem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MacCablemodem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PROVIENEDE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Recibio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EstadoAparato = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Marca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RegresarAlAlmacen = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.MARCAACTUAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bnRegresar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        CType(Me.dgvD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnOrdenBuscar
        '
        Me.bnOrdenBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnOrdenBuscar.Location = New System.Drawing.Point(21, 114)
        Me.bnOrdenBuscar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnOrdenBuscar.Name = "bnOrdenBuscar"
        Me.bnOrdenBuscar.Size = New System.Drawing.Size(100, 28)
        Me.bnOrdenBuscar.TabIndex = 1
        Me.bnOrdenBuscar.Text = "Buscar"
        Me.bnOrdenBuscar.UseVisualStyleBackColor = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(16, 11)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(195, 24)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar Aparato por:"
        '
        'tbOrden
        '
        Me.tbOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbOrden.Location = New System.Drawing.Point(21, 81)
        Me.tbOrden.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbOrden.Name = "tbOrden"
        Me.tbOrden.Size = New System.Drawing.Size(192, 24)
        Me.tbOrden.TabIndex = 0
        '
        'cbTipoAparato
        '
        Me.cbTipoAparato.DisplayMember = "TIPO"
        Me.cbTipoAparato.Enabled = False
        Me.cbTipoAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipoAparato.FormattingEnabled = True
        Me.cbTipoAparato.Location = New System.Drawing.Point(455, 172)
        Me.cbTipoAparato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbTipoAparato.Name = "cbTipoAparato"
        Me.cbTipoAparato.Size = New System.Drawing.Size(188, 26)
        Me.cbTipoAparato.TabIndex = 5
        Me.cbTipoAparato.TabStop = False
        Me.cbTipoAparato.ValueMember = "TipoAparato"
        Me.cbTipoAparato.Visible = False
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(17, 59)
        Me.CMBLabel2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(68, 18)
        Me.CMBLabel2.TabIndex = 4
        Me.CMBLabel2.Text = "# Orden"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Enabled = False
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(451, 250)
        Me.CMBLabel3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(47, 18)
        Me.CMBLabel3.TabIndex = 5
        Me.CMBLabel3.Text = "Serie"
        Me.CMBLabel3.Visible = False
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Enabled = False
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(451, 305)
        Me.CMBLabel4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(46, 18)
        Me.CMBLabel4.TabIndex = 6
        Me.CMBLabel4.Text = "Folio"
        Me.CMBLabel4.Visible = False
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Enabled = False
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(451, 150)
        Me.CMBLabel5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(127, 18)
        Me.CMBLabel5.TabIndex = 7
        Me.CMBLabel5.Text = "Tipo de Aparato"
        Me.CMBLabel5.Visible = False
        '
        'CMBLabel6
        '
        Me.CMBLabel6.AutoSize = True
        Me.CMBLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel6.Location = New System.Drawing.Point(17, 172)
        Me.CMBLabel6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel6.Name = "CMBLabel6"
        Me.CMBLabel6.Size = New System.Drawing.Size(40, 18)
        Me.CMBLabel6.TabIndex = 8
        Me.CMBLabel6.Text = "Mac"
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Enabled = False
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.Location = New System.Drawing.Point(16, 294)
        Me.CMBLabel7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(67, 18)
        Me.CMBLabel7.TabIndex = 9
        Me.CMBLabel7.Text = "Usuario"
        '
        'tbMac
        '
        Me.tbMac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMac.Location = New System.Drawing.Point(21, 194)
        Me.tbMac.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbMac.Name = "tbMac"
        Me.tbMac.Size = New System.Drawing.Size(192, 24)
        Me.tbMac.TabIndex = 7
        '
        'tbFolio
        '
        Me.tbFolio.Enabled = False
        Me.tbFolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFolio.Location = New System.Drawing.Point(455, 327)
        Me.tbFolio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbFolio.Name = "tbFolio"
        Me.tbFolio.Size = New System.Drawing.Size(239, 24)
        Me.tbFolio.TabIndex = 3
        Me.tbFolio.TabStop = False
        Me.tbFolio.Visible = False
        '
        'tbSerie
        '
        Me.tbSerie.Enabled = False
        Me.tbSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSerie.Location = New System.Drawing.Point(455, 272)
        Me.tbSerie.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbSerie.Name = "tbSerie"
        Me.tbSerie.Size = New System.Drawing.Size(239, 24)
        Me.tbSerie.TabIndex = 2
        Me.tbSerie.TabStop = False
        Me.tbSerie.Visible = False
        '
        'bnUsuarioBuscar
        '
        Me.bnUsuarioBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnUsuarioBuscar.Location = New System.Drawing.Point(21, 352)
        Me.bnUsuarioBuscar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnUsuarioBuscar.Name = "bnUsuarioBuscar"
        Me.bnUsuarioBuscar.Size = New System.Drawing.Size(100, 28)
        Me.bnUsuarioBuscar.TabIndex = 10
        Me.bnUsuarioBuscar.Text = "Buscar"
        Me.bnUsuarioBuscar.UseVisualStyleBackColor = True
        '
        'bnMacBuscar
        '
        Me.bnMacBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnMacBuscar.Location = New System.Drawing.Point(21, 228)
        Me.bnMacBuscar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnMacBuscar.Name = "bnMacBuscar"
        Me.bnMacBuscar.Size = New System.Drawing.Size(100, 28)
        Me.bnMacBuscar.TabIndex = 8
        Me.bnMacBuscar.Text = "Buscar"
        Me.bnMacBuscar.UseVisualStyleBackColor = True
        '
        'bnTipoAparatoBuscar
        '
        Me.bnTipoAparatoBuscar.Enabled = False
        Me.bnTipoAparatoBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTipoAparatoBuscar.Location = New System.Drawing.Point(455, 206)
        Me.bnTipoAparatoBuscar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnTipoAparatoBuscar.Name = "bnTipoAparatoBuscar"
        Me.bnTipoAparatoBuscar.Size = New System.Drawing.Size(100, 28)
        Me.bnTipoAparatoBuscar.TabIndex = 6
        Me.bnTipoAparatoBuscar.TabStop = False
        Me.bnTipoAparatoBuscar.Text = "Buscar"
        Me.bnTipoAparatoBuscar.UseVisualStyleBackColor = True
        Me.bnTipoAparatoBuscar.Visible = False
        '
        'bnSerieFolioBuscar
        '
        Me.bnSerieFolioBuscar.Enabled = False
        Me.bnSerieFolioBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSerieFolioBuscar.Location = New System.Drawing.Point(455, 361)
        Me.bnSerieFolioBuscar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnSerieFolioBuscar.Name = "bnSerieFolioBuscar"
        Me.bnSerieFolioBuscar.Size = New System.Drawing.Size(100, 28)
        Me.bnSerieFolioBuscar.TabIndex = 4
        Me.bnSerieFolioBuscar.Text = "Buscar"
        Me.bnSerieFolioBuscar.UseVisualStyleBackColor = True
        Me.bnSerieFolioBuscar.Visible = False
        '
        'cbUsuario
        '
        Me.cbUsuario.DisplayMember = "NOMBRE"
        Me.cbUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbUsuario.FormattingEnabled = True
        Me.cbUsuario.Location = New System.Drawing.Point(21, 316)
        Me.cbUsuario.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbUsuario.Name = "cbUsuario"
        Me.cbUsuario.Size = New System.Drawing.Size(192, 26)
        Me.cbUsuario.TabIndex = 9
        Me.cbUsuario.TabStop = False
        Me.cbUsuario.ValueMember = "CLV_USUARIO"
        '
        'dgvD
        '
        Me.dgvD.AllowUserToAddRows = False
        Me.dgvD.AllowUserToDeleteRows = False
        Me.dgvD.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvD.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvD.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Orden, Me.TipoAparato, Me.Tipo, Me.Clv_Cablemodem, Me.MacCablemodem, Me.PROVIENEDE, Me.Recibio, Me.EstadoAparato, Me.Marca, Me.RegresarAlAlmacen, Me.MARCAACTUAL})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvD.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvD.Location = New System.Drawing.Point(236, 11)
        Me.dgvD.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvD.Name = "dgvD"
        Me.dgvD.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvD.Size = New System.Drawing.Size(943, 869)
        Me.dgvD.TabIndex = 24
        Me.dgvD.TabStop = False
        '
        'Clv_Orden
        '
        Me.Clv_Orden.DataPropertyName = "Clv_Orden"
        Me.Clv_Orden.HeaderText = "#Orden"
        Me.Clv_Orden.Name = "Clv_Orden"
        Me.Clv_Orden.Width = 70
        '
        'TipoAparato
        '
        Me.TipoAparato.DataPropertyName = "TipoAparato"
        Me.TipoAparato.HeaderText = "TipoAparato"
        Me.TipoAparato.Name = "TipoAparato"
        Me.TipoAparato.Visible = False
        '
        'Tipo
        '
        Me.Tipo.DataPropertyName = "Tipo"
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.Visible = False
        '
        'Clv_Cablemodem
        '
        Me.Clv_Cablemodem.DataPropertyName = "Clv_Cablemodem"
        Me.Clv_Cablemodem.HeaderText = "Clv_Cablemodem"
        Me.Clv_Cablemodem.Name = "Clv_Cablemodem"
        Me.Clv_Cablemodem.Visible = False
        '
        'MacCablemodem
        '
        Me.MacCablemodem.DataPropertyName = "MacCablemodem"
        Me.MacCablemodem.HeaderText = "Mac"
        Me.MacCablemodem.Name = "MacCablemodem"
        Me.MacCablemodem.Width = 120
        '
        'PROVIENEDE
        '
        Me.PROVIENEDE.DataPropertyName = "PROVIENEDE"
        Me.PROVIENEDE.HeaderText = "Proviene"
        Me.PROVIENEDE.Name = "PROVIENEDE"
        Me.PROVIENEDE.Width = 80
        '
        'Recibio
        '
        Me.Recibio.DataPropertyName = "Recibio"
        Me.Recibio.HeaderText = "Usuario que Recibió"
        Me.Recibio.Name = "Recibio"
        '
        'EstadoAparato
        '
        Me.EstadoAparato.DataPropertyName = "EstadoAparato"
        Me.EstadoAparato.FalseValue = "false"
        Me.EstadoAparato.HeaderText = "Buen Estado"
        Me.EstadoAparato.Name = "EstadoAparato"
        Me.EstadoAparato.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.EstadoAparato.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.EstadoAparato.TrueValue = "true"
        Me.EstadoAparato.Width = 60
        '
        'Marca
        '
        Me.Marca.DataPropertyName = "MARCA"
        Me.Marca.HeaderText = "Marca"
        Me.Marca.Name = "Marca"
        Me.Marca.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Marca.Width = 150
        '
        'RegresarAlAlmacen
        '
        Me.RegresarAlAlmacen.DataPropertyName = "REGRESARALALMACEN"
        Me.RegresarAlAlmacen.FalseValue = "false"
        Me.RegresarAlAlmacen.HeaderText = "Regresar"
        Me.RegresarAlAlmacen.Name = "RegresarAlAlmacen"
        Me.RegresarAlAlmacen.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RegresarAlAlmacen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.RegresarAlAlmacen.TrueValue = "true"
        Me.RegresarAlAlmacen.Width = 70
        '
        'MARCAACTUAL
        '
        Me.MARCAACTUAL.DataPropertyName = "MARCAACTUAL"
        Me.MARCAACTUAL.HeaderText = "MARCAACTUAL"
        Me.MARCAACTUAL.Name = "MARCAACTUAL"
        Me.MARCAACTUAL.Visible = False
        '
        'bnRegresar
        '
        Me.bnRegresar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnRegresar.Location = New System.Drawing.Point(1187, 11)
        Me.bnRegresar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnRegresar.Name = "bnRegresar"
        Me.bnRegresar.Size = New System.Drawing.Size(141, 102)
        Me.bnRegresar.TabIndex = 11
        Me.bnRegresar.Text = "&REGRESAR AL ALMACÉN"
        Me.bnRegresar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(1187, 836)
        Me.bnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(141, 44)
        Me.bnSalir.TabIndex = 12
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'FrmRegresarAparatosAlmacen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1344, 898)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnRegresar)
        Me.Controls.Add(Me.dgvD)
        Me.Controls.Add(Me.cbUsuario)
        Me.Controls.Add(Me.bnSerieFolioBuscar)
        Me.Controls.Add(Me.bnTipoAparatoBuscar)
        Me.Controls.Add(Me.bnMacBuscar)
        Me.Controls.Add(Me.bnUsuarioBuscar)
        Me.Controls.Add(Me.tbSerie)
        Me.Controls.Add(Me.tbFolio)
        Me.Controls.Add(Me.tbMac)
        Me.Controls.Add(Me.CMBLabel7)
        Me.Controls.Add(Me.CMBLabel6)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.cbTipoAparato)
        Me.Controls.Add(Me.tbOrden)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.bnOrdenBuscar)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmRegresarAparatosAlmacen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Regresar Aparatos al Almacén"
        CType(Me.dgvD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnOrdenBuscar As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents tbOrden As System.Windows.Forms.TextBox
    Friend WithEvents cbTipoAparato As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel6 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents tbMac As System.Windows.Forms.TextBox
    Friend WithEvents tbFolio As System.Windows.Forms.TextBox
    Friend WithEvents tbSerie As System.Windows.Forms.TextBox
    Friend WithEvents bnUsuarioBuscar As System.Windows.Forms.Button
    Friend WithEvents bnMacBuscar As System.Windows.Forms.Button
    Friend WithEvents bnTipoAparatoBuscar As System.Windows.Forms.Button
    Friend WithEvents bnSerieFolioBuscar As System.Windows.Forms.Button
    Friend WithEvents cbUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents dgvD As System.Windows.Forms.DataGridView
    Friend WithEvents bnRegresar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents Clv_Orden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoAparato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Cablemodem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MacCablemodem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PROVIENEDE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Recibio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EstadoAparato As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Marca As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RegresarAlAlmacen As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents MARCAACTUAL As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
