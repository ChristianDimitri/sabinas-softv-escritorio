<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMsjPorCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CIUDADLabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim CONTRATOLabel As System.Windows.Forms.Label
        Dim COLONIALabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tbcontratocompania = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.DameClientesActivosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.CIUDADTextBox = New System.Windows.Forms.TextBox()
        Me.NUMEROTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.COLONIATextBox = New System.Windows.Forms.TextBox()
        Me.CALLETextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.MuestraServCteResetDataGridView = New System.Windows.Forms.DataGridView()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvCableModemDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MacCableModemDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MuestraServCteResetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.MuestraServCteResetTableAdapter = New sofTV.DataSetLidiaTableAdapters.MuestraServCteResetTableAdapter()
        Me.DameClientesActivosTableAdapter = New sofTV.DataSetLidiaTableAdapters.DameClientesActivosTableAdapter()
        Me.Mensaje_Por_ClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Mensaje_Por_ClienteTableAdapter = New sofTV.DataSetLidiaTableAdapters.Mensaje_Por_ClienteTableAdapter()
        CIUDADLabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        CONTRATOLabel = New System.Windows.Forms.Label()
        COLONIALabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        CALLELabel = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.MuestraServCteResetDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServCteResetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.Mensaje_Por_ClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CIUDADLabel
        '
        CIUDADLabel.AutoSize = True
        CIUDADLabel.Location = New System.Drawing.Point(61, 185)
        CIUDADLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CIUDADLabel.Name = "CIUDADLabel"
        CIUDADLabel.Size = New System.Drawing.Size(79, 20)
        CIUDADLabel.TabIndex = 13
        CIUDADLabel.Text = "Ciudad :"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Location = New System.Drawing.Point(655, 154)
        NUMEROLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(31, 20)
        NUMEROLabel.TabIndex = 11
        NUMEROLabel.Text = "# :"
        '
        'CONTRATOLabel
        '
        CONTRATOLabel.AutoSize = True
        CONTRATOLabel.Location = New System.Drawing.Point(61, 54)
        CONTRATOLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CONTRATOLabel.Name = "CONTRATOLabel"
        CONTRATOLabel.Size = New System.Drawing.Size(93, 20)
        CONTRATOLabel.TabIndex = 3
        CONTRATOLabel.Text = "Contrato :"
        '
        'COLONIALabel
        '
        COLONIALabel.AutoSize = True
        COLONIALabel.Location = New System.Drawing.Point(61, 150)
        COLONIALabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        COLONIALabel.Name = "COLONIALabel"
        COLONIALabel.Size = New System.Drawing.Size(84, 20)
        COLONIALabel.TabIndex = 9
        COLONIALabel.Text = "Colonia :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Location = New System.Drawing.Point(61, 86)
        NOMBRELabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(86, 20)
        NOMBRELabel.TabIndex = 5
        NOMBRELabel.Text = "Nombre :"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Location = New System.Drawing.Point(61, 118)
        CALLELabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(64, 20)
        CALLELabel.TabIndex = 7
        CALLELabel.Text = "Calle :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tbcontratocompania)
        Me.GroupBox1.Controls.Add(Me.NOMBRETextBox)
        Me.GroupBox1.Controls.Add(Me.CIUDADTextBox)
        Me.GroupBox1.Controls.Add(CIUDADLabel)
        Me.GroupBox1.Controls.Add(Me.NUMEROTextBox)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(NUMEROLabel)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.COLONIATextBox)
        Me.GroupBox1.Controls.Add(CONTRATOLabel)
        Me.GroupBox1.Controls.Add(COLONIALabel)
        Me.GroupBox1.Controls.Add(NOMBRELabel)
        Me.GroupBox1.Controls.Add(Me.CALLETextBox)
        Me.GroupBox1.Controls.Add(CALLELabel)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(56, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(983, 238)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente para Envio de Mensaje"
        '
        'tbcontratocompania
        '
        Me.tbcontratocompania.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcontratocompania.Location = New System.Drawing.Point(195, 47)
        Me.tbcontratocompania.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbcontratocompania.Name = "tbcontratocompania"
        Me.tbcontratocompania.Size = New System.Drawing.Size(133, 26)
        Me.tbcontratocompania.TabIndex = 15
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(195, 82)
        Me.NOMBRETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.ReadOnly = True
        Me.NOMBRETextBox.Size = New System.Drawing.Size(637, 26)
        Me.NOMBRETextBox.TabIndex = 6
        Me.NOMBRETextBox.TabStop = False
        '
        'DameClientesActivosBindingSource
        '
        Me.DameClientesActivosBindingSource.DataMember = "DameClientesActivos"
        Me.DameClientesActivosBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CIUDADTextBox
        '
        Me.CIUDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CIUDAD", True))
        Me.CIUDADTextBox.Location = New System.Drawing.Point(195, 181)
        Me.CIUDADTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CIUDADTextBox.Name = "CIUDADTextBox"
        Me.CIUDADTextBox.ReadOnly = True
        Me.CIUDADTextBox.Size = New System.Drawing.Size(240, 26)
        Me.CIUDADTextBox.TabIndex = 14
        Me.CIUDADTextBox.TabStop = False
        '
        'NUMEROTextBox
        '
        Me.NUMEROTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NUMERO", True))
        Me.NUMEROTextBox.Location = New System.Drawing.Point(695, 150)
        Me.NUMEROTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NUMEROTextBox.Name = "NUMEROTextBox"
        Me.NUMEROTextBox.ReadOnly = True
        Me.NUMEROTextBox.Size = New System.Drawing.Size(137, 26)
        Me.NUMEROTextBox.TabIndex = 12
        Me.NUMEROTextBox.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(195, 47)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(133, 26)
        Me.TextBox1.TabIndex = 0
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(351, 47)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(59, 28)
        Me.Button3.TabIndex = 1
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'COLONIATextBox
        '
        Me.COLONIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "COLONIA", True))
        Me.COLONIATextBox.Location = New System.Drawing.Point(195, 146)
        Me.COLONIATextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.COLONIATextBox.Name = "COLONIATextBox"
        Me.COLONIATextBox.ReadOnly = True
        Me.COLONIATextBox.Size = New System.Drawing.Size(413, 26)
        Me.COLONIATextBox.TabIndex = 10
        Me.COLONIATextBox.TabStop = False
        '
        'CALLETextBox
        '
        Me.CALLETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CALLE", True))
        Me.CALLETextBox.Location = New System.Drawing.Point(195, 114)
        Me.CALLETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CALLETextBox.Name = "CALLETextBox"
        Me.CALLETextBox.ReadOnly = True
        Me.CALLETextBox.Size = New System.Drawing.Size(413, 26)
        Me.CALLETextBox.TabIndex = 8
        Me.CALLETextBox.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Button1.Location = New System.Drawing.Point(56, 742)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(383, 55)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&ENVIAR MENSAJE"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Button2.Location = New System.Drawing.Point(847, 742)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(192, 55)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.MuestraServCteResetDataGridView)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(56, 274)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Size = New System.Drawing.Size(983, 219)
        Me.GroupBox3.TabIndex = 45
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "STB(s) del Cliente"
        '
        'MuestraServCteResetDataGridView
        '
        Me.MuestraServCteResetDataGridView.AllowUserToAddRows = False
        Me.MuestraServCteResetDataGridView.AllowUserToDeleteRows = False
        Me.MuestraServCteResetDataGridView.AutoGenerateColumns = False
        Me.MuestraServCteResetDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MuestraServCteResetDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MuestraServCteResetDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Contrato, Me.ClvCableModemDataGridViewTextBoxColumn, Me.MacCableModemDataGridViewTextBoxColumn})
        Me.MuestraServCteResetDataGridView.DataSource = Me.MuestraServCteResetBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MuestraServCteResetDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.MuestraServCteResetDataGridView.Location = New System.Drawing.Point(92, 43)
        Me.MuestraServCteResetDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MuestraServCteResetDataGridView.Name = "MuestraServCteResetDataGridView"
        Me.MuestraServCteResetDataGridView.ReadOnly = True
        Me.MuestraServCteResetDataGridView.RowHeadersVisible = False
        Me.MuestraServCteResetDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MuestraServCteResetDataGridView.Size = New System.Drawing.Size(457, 146)
        Me.MuestraServCteResetDataGridView.TabIndex = 2
        Me.MuestraServCteResetDataGridView.TabStop = False
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        Me.Contrato.Visible = False
        '
        'ClvCableModemDataGridViewTextBoxColumn
        '
        Me.ClvCableModemDataGridViewTextBoxColumn.DataPropertyName = "Clv_CableModem"
        Me.ClvCableModemDataGridViewTextBoxColumn.HeaderText = "Clv_CableModem"
        Me.ClvCableModemDataGridViewTextBoxColumn.Name = "ClvCableModemDataGridViewTextBoxColumn"
        Me.ClvCableModemDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvCableModemDataGridViewTextBoxColumn.Visible = False
        '
        'MacCableModemDataGridViewTextBoxColumn
        '
        Me.MacCableModemDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.MacCableModemDataGridViewTextBoxColumn.DataPropertyName = "MacCableModem"
        Me.MacCableModemDataGridViewTextBoxColumn.HeaderText = "STB's"
        Me.MacCableModemDataGridViewTextBoxColumn.Name = "MacCableModemDataGridViewTextBoxColumn"
        Me.MacCableModemDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MuestraServCteResetBindingSource
        '
        Me.MuestraServCteResetBindingSource.DataMember = "MuestraServCteReset"
        Me.MuestraServCteResetBindingSource.DataSource = Me.DataSetLidia
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(65, 26)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox2.MaxLength = 200
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(832, 169)
        Me.TextBox2.TabIndex = 46
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBox2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(56, 517)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(983, 218)
        Me.GroupBox2.TabIndex = 47
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Captura el Mensaje a Enviar"
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(195, 25)
        Me.ComboBoxCompanias.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(317, 33)
        Me.ComboBoxCompanias.TabIndex = 102
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ComboBoxCompanias)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.GroupBox4.Location = New System.Drawing.Point(56, 543)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox4.Size = New System.Drawing.Size(983, 79)
        Me.GroupBox4.TabIndex = 103
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Compañía"
        Me.GroupBox4.Visible = False
        '
        'MuestraServCteResetTableAdapter
        '
        Me.MuestraServCteResetTableAdapter.ClearBeforeFill = True
        '
        'DameClientesActivosTableAdapter
        '
        Me.DameClientesActivosTableAdapter.ClearBeforeFill = True
        '
        'Mensaje_Por_ClienteBindingSource
        '
        Me.Mensaje_Por_ClienteBindingSource.DataMember = "Mensaje_Por_Cliente"
        Me.Mensaje_Por_ClienteBindingSource.DataSource = Me.DataSetLidia
        '
        'Mensaje_Por_ClienteTableAdapter
        '
        Me.Mensaje_Por_ClienteTableAdapter.ClearBeforeFill = True
        '
        'FrmMsjPorCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1107, 825)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmMsjPorCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mensaje a Clientes"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.MuestraServCteResetDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServCteResetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.Mensaje_Por_ClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CIUDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NUMEROTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents COLONIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents MuestraServCteResetDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents MuestraServCteResetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServCteResetTableAdapter As sofTV.DataSetLidiaTableAdapters.MuestraServCteResetTableAdapter
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents DameClientesActivosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClientesActivosTableAdapter As sofTV.DataSetLidiaTableAdapters.DameClientesActivosTableAdapter
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Mensaje_Por_ClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Mensaje_Por_ClienteTableAdapter As sofTV.DataSetLidiaTableAdapters.Mensaje_Por_ClienteTableAdapter
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents tbcontratocompania As System.Windows.Forms.TextBox
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvCableModemDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MacCableModemDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
