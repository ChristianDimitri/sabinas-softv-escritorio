﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Collections.Generic
Imports System.IO
Public Class FrmEvidenciasFoliosCancelados

    Private Sub FrmEvidenciasFoliosCancelados_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@busqueda", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        DataGridView1.DataSource = BaseII.ConsultaDT("MuestraFoliosCancelados")
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            If DataGridView1.SelectedCells(4).Value = 0 Then
                PictureBox1.Image = sofTV.My.Resources.Resources.filenotfound4041
            Else
                Dim conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim comando As New SqlCommand()
                comando.Connection = conexion
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@tipoDocumento", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@serie", SqlDbType.VarChar, DataGridView1.SelectedCells(1).Value.ToString)
                BaseII.CreateMyParameter("@folio", SqlDbType.BigInt, DataGridView1.SelectedCells(0).Value.ToString)
                BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.Int, DataGridView1.SelectedCells(2).Value.ToString)
                BaseII.ProcedimientoOutPut("DameTipoEvidencia")
                If BaseII.dicoPar("@tipoDocumento") = 1 Then 'image
                    comando.CommandText = "select evidencia from EvidenciaFolioCancelado where serie='" + DataGridView1.SelectedCells(1).Value.ToString + "' and folio=" + DataGridView1.SelectedCells(0).Value.ToString + " and clv_vendedor=" + DataGridView1.SelectedCells(2).Value.ToString
                    Dim reader As SqlDataReader = comando.ExecuteReader
                    reader.Read()
                    Dim by(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
                    reader.GetBytes(0, 0, by, 0, by.Length)
                    Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(by)
                    PictureBox1.Image = Image.FromStream(ms)
                    reader.Close()
                    PictureBox1.Visible = True
                    pdfViewer.Visible = False
                Else
                    comando.CommandText = "select evidencia from EvidenciaFolioCancelado where serie='" + DataGridView1.SelectedCells(1).Value.ToString + "' and folio=" + DataGridView1.SelectedCells(0).Value.ToString + " and clv_vendedor=" + DataGridView1.SelectedCells(2).Value.ToString
                    Dim reader As SqlDataReader = comando.ExecuteReader(System.Data.CommandBehavior.Default)
                    reader.Read()
                    Dim contents(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
                    contents = reader.GetValue(0)
                    Dim fileName As String = System.IO.Path.GetTempFileName() + ".pdf"
                    File.WriteAllBytes(fileName, contents)
                    reader.Close()
                    PictureBox1.Visible = False
                    pdfViewer.Visible = True
                    pdfViewer.Navigate(fileName)
                End If
                conexion.Close()
            End If
        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If TextBox1.Text = "" Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@busqueda", SqlDbType.VarChar, TextBox1.Text)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        DataGridView1.DataSource = BaseII.ConsultaDT("MuestraFoliosCancelados")
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class