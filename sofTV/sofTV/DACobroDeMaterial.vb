﻿Imports System.Xml
Imports System.Data.SqlClient

Public Class DACobroDeMaterial

    Public Shared Sesion As Integer = 0
    Public Shared ExistenCargos As Integer = 0
    Public Shared Mostrar As Boolean = False
    Public Shared Vista As Integer = 0

    ' Funciones de Rangos de Montos para le número máximo de Pagos Diferidos
    'Agregar Rangos
    Public Shared Function spAgregaRangosCobroMaterial(ByVal RInicial As Integer, ByVal RFinal As Integer, ByVal NMaxDiferidos As Integer) As Integer

        Dim conn As New SqlConnection(MiConexion)
        Dim IdRetorno As Integer = 0

        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("spAgregaRangosCobroMaterial", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@RInicial", SqlDbType.Decimal).Value = RInicial
            comando.Parameters.Add("@RFinal", SqlDbType.Decimal).Value = RFinal
            comando.Parameters.Add("@NMaxDiferidos", SqlDbType.TinyInt).Value = NMaxDiferidos
            comando.Parameters.Add("@IdRetorno", SqlDbType.BigInt).Value = 0
            comando.Parameters.Add("@idcompania", SqlDbType.Int).Value = FrmGenerales_Sistema.ComboBoxCompanias.SelectedValue
            comando.Parameters("@IdRetorno").Direction = ParameterDirection.Output

            comando.ExecuteNonQuery()

            If Not IsDBNull(comando.Parameters("@IdRetorno").Value) Then
                IdRetorno = comando.Parameters("@IdRetorno").Value
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return IdRetorno
    End Function

    'Eliminar Rangos
    Public Shared Sub spEliminaRangosCobroMaterial(ByVal IDRango As Integer)

        Dim conn As New SqlConnection(MiConexion)
        Try
            conn.Open()

            Dim comando As New SqlClient.SqlCommand("spEliminaRangosCobroMaterial", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@IDRango", SqlDbType.BigInt).Value = IDRango
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    'Muestra los Rangos Actuales
    Public Shared Function spConsultaRangosCobroMaterial() As DataTable

        Dim conn As New SqlConnection(MiConexion)
        Dim dt As New DataTable

        Try
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("spConsultaRangosCobroMaterial", conn)
            comando.Parameters.Add("@idcompania", SqlDbType.Int).Value = FrmGenerales_Sistema.ComboBoxCompanias.SelectedValue
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando
            Adaptador.Fill(dt)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try
        Return dt
    End Function
End Class