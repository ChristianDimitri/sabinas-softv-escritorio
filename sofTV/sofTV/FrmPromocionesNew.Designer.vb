﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPromocionesNew
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPromocionesNew))
        Me.TextBoxClave = New System.Windows.Forms.TextBox()
        Me.BindingNavigatorPromocion = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.TextBoxNombre = New System.Windows.Forms.TextBox()
        Me.TextBoxContratacion = New System.Windows.Forms.TextBox()
        Me.TextBoxMensualidad = New System.Windows.Forms.TextBox()
        Me.TextBoxPlazoForzoso = New System.Windows.Forms.TextBox()
        Me.ComboBoxPlazas = New System.Windows.Forms.ComboBox()
        Me.ListBoxPlazas = New System.Windows.Forms.ListBox()
        Me.ListBoxServicios = New System.Windows.Forms.ListBox()
        Me.ComboBoxServicios = New System.Windows.Forms.ComboBox()
        Me.AgregarDistribuidor = New System.Windows.Forms.Button()
        Me.EliminarDistribuidor = New System.Windows.Forms.Button()
        Me.EliminarServicio = New System.Windows.Forms.Button()
        Me.AgregarServicio = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ComboBoxTipSer = New System.Windows.Forms.ComboBox()
        Me.CheckBoxActiva = New System.Windows.Forms.CheckBox()
        Me.DateTimePickerInicio = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePickerFinal = New System.Windows.Forms.DateTimePicker()
        Me.tbMAdicional1 = New System.Windows.Forms.TextBox()
        Me.tbCAdicional1 = New System.Windows.Forms.TextBox()
        Me.tbMAdicional2 = New System.Windows.Forms.TextBox()
        Me.tbCAdicional2 = New System.Windows.Forms.TextBox()
        Me.UPplazo = New System.Windows.Forms.NumericUpDown()
        Me.PanelMensualidad = New System.Windows.Forms.Panel()
        Me.CheckBoxMensualidad = New System.Windows.Forms.CheckBox()
        Me.CheckBoxContratacion = New System.Windows.Forms.CheckBox()
        Me.PanelContratacion = New System.Windows.Forms.Panel()
        Me.CheckBoxRenta = New System.Windows.Forms.CheckBox()
        Me.PanelRenta = New System.Windows.Forms.Panel()
        Me.btnGuardarModelo = New System.Windows.Forms.Button()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        CType(Me.BindingNavigatorPromocion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigatorPromocion.SuspendLayout()
        CType(Me.UPplazo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelMensualidad.SuspendLayout()
        Me.PanelContratacion.SuspendLayout()
        Me.PanelRenta.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(20, 39)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(171, 18)
        Label1.TabIndex = 17
        Label1.Text = "Clave de Promoción :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(123, 87)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(78, 18)
        Label2.TabIndex = 20
        Label2.Text = "Nombre :"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(75, 310)
        Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(125, 18)
        Label5.TabIndex = 26
        Label5.Text = "Plazo forzoso :"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Label17.Location = New System.Drawing.Point(29, 361)
        Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(105, 18)
        Label17.TabIndex = 51
        Label17.Text = "Distribuidor :"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(776, 153)
        Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(100, 18)
        Label6.TabIndex = 54
        Label6.Text = "Fecha final :"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(613, 361)
        Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(140, 18)
        Label7.TabIndex = 62
        Label7.Text = "Tipo de Servicio :"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(820, 60)
        Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(63, 18)
        Label8.TabIndex = 63
        Label8.Text = "Activa :"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(761, 113)
        Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(112, 18)
        Label9.TabIndex = 66
        Label9.Text = "Fecha inicial :"
        AddHandler Label9.Click, AddressOf Me.Label9_Click
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(616, 418)
        Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(79, 18)
        Label10.TabIndex = 70
        Label10.Text = "Servicio :"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(4, 0)
        Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(78, 18)
        Label11.TabIndex = 75
        Label11.Text = "Principal "
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Label12.Location = New System.Drawing.Point(7, 0)
        Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(78, 18)
        Label12.TabIndex = 76
        Label12.Text = "Principal "
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(173, 0)
        Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(104, 18)
        Label13.TabIndex = 77
        Label13.Text = "Adicional #1 "
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(176, 1)
        Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(104, 18)
        Label14.TabIndex = 78
        Label14.Text = "Adicional #1 "
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(335, 0)
        Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(104, 18)
        Label15.TabIndex = 79
        Label15.Text = "Adicional #2 "
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(337, 0)
        Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(104, 18)
        Label16.TabIndex = 80
        Label16.Text = "Adicional #2 "
        '
        'TextBoxClave
        '
        Me.TextBoxClave.BackColor = System.Drawing.Color.White
        Me.TextBoxClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxClave.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxClave.Enabled = False
        Me.TextBoxClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxClave.Location = New System.Drawing.Point(219, 36)
        Me.TextBoxClave.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxClave.MaxLength = 255
        Me.TextBoxClave.Name = "TextBoxClave"
        Me.TextBoxClave.Size = New System.Drawing.Size(157, 24)
        Me.TextBoxClave.TabIndex = 0
        '
        'BindingNavigatorPromocion
        '
        Me.BindingNavigatorPromocion.AddNewItem = Nothing
        Me.BindingNavigatorPromocion.CountItem = Nothing
        Me.BindingNavigatorPromocion.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.BindingNavigatorPromocion.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.BindingNavigatorPromocion.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.BindingNavigatorPromocion.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigatorPromocion.MoveFirstItem = Nothing
        Me.BindingNavigatorPromocion.MoveLastItem = Nothing
        Me.BindingNavigatorPromocion.MoveNextItem = Nothing
        Me.BindingNavigatorPromocion.MovePreviousItem = Nothing
        Me.BindingNavigatorPromocion.Name = "BindingNavigatorPromocion"
        Me.BindingNavigatorPromocion.PositionItem = Nothing
        Me.BindingNavigatorPromocion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigatorPromocion.Size = New System.Drawing.Size(1199, 28)
        Me.BindingNavigatorPromocion.TabIndex = 18
        Me.BindingNavigatorPromocion.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(122, 25)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'BindingNavigatorSaveItem
        '
        Me.BindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorSaveItem.Image = CType(resources.GetObject("BindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem"
        Me.BindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 25)
        Me.BindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'TextBoxNombre
        '
        Me.TextBoxNombre.BackColor = System.Drawing.Color.White
        Me.TextBoxNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNombre.Location = New System.Drawing.Point(219, 80)
        Me.TextBoxNombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNombre.MaxLength = 255
        Me.TextBoxNombre.Name = "TextBoxNombre"
        Me.TextBoxNombre.Size = New System.Drawing.Size(383, 24)
        Me.TextBoxNombre.TabIndex = 0
        '
        'TextBoxContratacion
        '
        Me.TextBoxContratacion.BackColor = System.Drawing.Color.White
        Me.TextBoxContratacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxContratacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxContratacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxContratacion.Location = New System.Drawing.Point(8, 20)
        Me.TextBoxContratacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxContratacion.MaxLength = 255
        Me.TextBoxContratacion.Name = "TextBoxContratacion"
        Me.TextBoxContratacion.Size = New System.Drawing.Size(157, 24)
        Me.TextBoxContratacion.TabIndex = 1
        '
        'TextBoxMensualidad
        '
        Me.TextBoxMensualidad.BackColor = System.Drawing.Color.White
        Me.TextBoxMensualidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxMensualidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxMensualidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxMensualidad.Location = New System.Drawing.Point(11, 22)
        Me.TextBoxMensualidad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxMensualidad.MaxLength = 255
        Me.TextBoxMensualidad.Name = "TextBoxMensualidad"
        Me.TextBoxMensualidad.Size = New System.Drawing.Size(157, 24)
        Me.TextBoxMensualidad.TabIndex = 2
        '
        'TextBoxPlazoForzoso
        '
        Me.TextBoxPlazoForzoso.BackColor = System.Drawing.Color.White
        Me.TextBoxPlazoForzoso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxPlazoForzoso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxPlazoForzoso.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxPlazoForzoso.Location = New System.Drawing.Point(385, 308)
        Me.TextBoxPlazoForzoso.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxPlazoForzoso.MaxLength = 255
        Me.TextBoxPlazoForzoso.Name = "TextBoxPlazoForzoso"
        Me.TextBoxPlazoForzoso.Size = New System.Drawing.Size(157, 24)
        Me.TextBoxPlazoForzoso.TabIndex = 3
        Me.TextBoxPlazoForzoso.Visible = False
        '
        'ComboBoxPlazas
        '
        Me.ComboBoxPlazas.DisplayMember = "Nombre"
        Me.ComboBoxPlazas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxPlazas.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxPlazas.FormattingEnabled = True
        Me.ComboBoxPlazas.Location = New System.Drawing.Point(39, 383)
        Me.ComboBoxPlazas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxPlazas.Name = "ComboBoxPlazas"
        Me.ComboBoxPlazas.Size = New System.Drawing.Size(436, 30)
        Me.ComboBoxPlazas.TabIndex = 50
        Me.ComboBoxPlazas.ValueMember = "Clv_Plaza"
        '
        'ListBoxPlazas
        '
        Me.ListBoxPlazas.DisplayMember = "Nombre"
        Me.ListBoxPlazas.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ListBoxPlazas.FormattingEnabled = True
        Me.ListBoxPlazas.ItemHeight = 22
        Me.ListBoxPlazas.Location = New System.Drawing.Point(41, 422)
        Me.ListBoxPlazas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ListBoxPlazas.Name = "ListBoxPlazas"
        Me.ListBoxPlazas.Size = New System.Drawing.Size(433, 246)
        Me.ListBoxPlazas.TabIndex = 52
        Me.ListBoxPlazas.ValueMember = "Clv_Plaza"
        '
        'ListBoxServicios
        '
        Me.ListBoxServicios.DisplayMember = "Descripcion"
        Me.ListBoxServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ListBoxServicios.FormattingEnabled = True
        Me.ListBoxServicios.ItemHeight = 22
        Me.ListBoxServicios.Location = New System.Drawing.Point(620, 489)
        Me.ListBoxServicios.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ListBoxServicios.Name = "ListBoxServicios"
        Me.ListBoxServicios.Size = New System.Drawing.Size(433, 180)
        Me.ListBoxServicios.TabIndex = 55
        Me.ListBoxServicios.ValueMember = "Clv_Servicio"
        '
        'ComboBoxServicios
        '
        Me.ComboBoxServicios.DisplayMember = "Descripcion"
        Me.ComboBoxServicios.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxServicios.FormattingEnabled = True
        Me.ComboBoxServicios.Location = New System.Drawing.Point(617, 441)
        Me.ComboBoxServicios.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxServicios.Name = "ComboBoxServicios"
        Me.ComboBoxServicios.Size = New System.Drawing.Size(436, 30)
        Me.ComboBoxServicios.TabIndex = 53
        Me.ComboBoxServicios.ValueMember = "Clv_Servicio"
        '
        'AgregarDistribuidor
        '
        Me.AgregarDistribuidor.BackColor = System.Drawing.Color.DarkOrange
        Me.AgregarDistribuidor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AgregarDistribuidor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AgregarDistribuidor.ForeColor = System.Drawing.Color.Black
        Me.AgregarDistribuidor.Location = New System.Drawing.Point(484, 422)
        Me.AgregarDistribuidor.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.AgregarDistribuidor.Name = "AgregarDistribuidor"
        Me.AgregarDistribuidor.Size = New System.Drawing.Size(111, 34)
        Me.AgregarDistribuidor.TabIndex = 56
        Me.AgregarDistribuidor.Text = "AGREGAR"
        Me.AgregarDistribuidor.UseVisualStyleBackColor = False
        '
        'EliminarDistribuidor
        '
        Me.EliminarDistribuidor.BackColor = System.Drawing.Color.DarkOrange
        Me.EliminarDistribuidor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EliminarDistribuidor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EliminarDistribuidor.ForeColor = System.Drawing.Color.Black
        Me.EliminarDistribuidor.Location = New System.Drawing.Point(484, 464)
        Me.EliminarDistribuidor.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.EliminarDistribuidor.Name = "EliminarDistribuidor"
        Me.EliminarDistribuidor.Size = New System.Drawing.Size(111, 34)
        Me.EliminarDistribuidor.TabIndex = 57
        Me.EliminarDistribuidor.Text = "ELIMINAR"
        Me.EliminarDistribuidor.UseVisualStyleBackColor = False
        '
        'EliminarServicio
        '
        Me.EliminarServicio.BackColor = System.Drawing.Color.DarkOrange
        Me.EliminarServicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EliminarServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EliminarServicio.ForeColor = System.Drawing.Color.Black
        Me.EliminarServicio.Location = New System.Drawing.Point(1063, 530)
        Me.EliminarServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.EliminarServicio.Name = "EliminarServicio"
        Me.EliminarServicio.Size = New System.Drawing.Size(111, 34)
        Me.EliminarServicio.TabIndex = 59
        Me.EliminarServicio.Text = "ELIMINAR"
        Me.EliminarServicio.UseVisualStyleBackColor = False
        '
        'AgregarServicio
        '
        Me.AgregarServicio.BackColor = System.Drawing.Color.DarkOrange
        Me.AgregarServicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AgregarServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AgregarServicio.ForeColor = System.Drawing.Color.Black
        Me.AgregarServicio.Location = New System.Drawing.Point(1063, 489)
        Me.AgregarServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.AgregarServicio.Name = "AgregarServicio"
        Me.AgregarServicio.Size = New System.Drawing.Size(111, 34)
        Me.AgregarServicio.TabIndex = 58
        Me.AgregarServicio.Text = "AGREGAR"
        Me.AgregarServicio.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(1077, 683)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(111, 34)
        Me.Button1.TabIndex = 60
        Me.Button1.Text = "SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ComboBoxTipSer
        '
        Me.ComboBoxTipSer.DisplayMember = "Concepto"
        Me.ComboBoxTipSer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxTipSer.FormattingEnabled = True
        Me.ComboBoxTipSer.Location = New System.Drawing.Point(617, 383)
        Me.ComboBoxTipSer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxTipSer.Name = "ComboBoxTipSer"
        Me.ComboBoxTipSer.Size = New System.Drawing.Size(436, 30)
        Me.ComboBoxTipSer.TabIndex = 61
        Me.ComboBoxTipSer.ValueMember = "Clv_TipSerPrincipal"
        '
        'CheckBoxActiva
        '
        Me.CheckBoxActiva.AutoSize = True
        Me.CheckBoxActiva.Location = New System.Drawing.Point(897, 63)
        Me.CheckBoxActiva.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBoxActiva.Name = "CheckBoxActiva"
        Me.CheckBoxActiva.Size = New System.Drawing.Size(18, 17)
        Me.CheckBoxActiva.TabIndex = 65
        Me.CheckBoxActiva.UseVisualStyleBackColor = True
        '
        'DateTimePickerInicio
        '
        Me.DateTimePickerInicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePickerInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePickerInicio.Location = New System.Drawing.Point(897, 107)
        Me.DateTimePickerInicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePickerInicio.Name = "DateTimePickerInicio"
        Me.DateTimePickerInicio.Size = New System.Drawing.Size(275, 24)
        Me.DateTimePickerInicio.TabIndex = 68
        '
        'DateTimePickerFinal
        '
        Me.DateTimePickerFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePickerFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePickerFinal.Location = New System.Drawing.Point(897, 146)
        Me.DateTimePickerFinal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePickerFinal.Name = "DateTimePickerFinal"
        Me.DateTimePickerFinal.Size = New System.Drawing.Size(275, 24)
        Me.DateTimePickerFinal.TabIndex = 69
        '
        'tbMAdicional1
        '
        Me.tbMAdicional1.BackColor = System.Drawing.Color.White
        Me.tbMAdicional1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbMAdicional1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbMAdicional1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMAdicional1.Location = New System.Drawing.Point(176, 23)
        Me.tbMAdicional1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbMAdicional1.MaxLength = 255
        Me.tbMAdicional1.Name = "tbMAdicional1"
        Me.tbMAdicional1.Size = New System.Drawing.Size(157, 24)
        Me.tbMAdicional1.TabIndex = 72
        '
        'tbCAdicional1
        '
        Me.tbCAdicional1.BackColor = System.Drawing.Color.White
        Me.tbCAdicional1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbCAdicional1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCAdicional1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCAdicional1.Location = New System.Drawing.Point(173, 21)
        Me.tbCAdicional1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCAdicional1.MaxLength = 255
        Me.tbCAdicional1.Name = "tbCAdicional1"
        Me.tbCAdicional1.Size = New System.Drawing.Size(157, 24)
        Me.tbCAdicional1.TabIndex = 71
        '
        'tbMAdicional2
        '
        Me.tbMAdicional2.BackColor = System.Drawing.Color.White
        Me.tbMAdicional2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbMAdicional2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbMAdicional2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMAdicional2.Location = New System.Drawing.Point(341, 23)
        Me.tbMAdicional2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbMAdicional2.MaxLength = 255
        Me.tbMAdicional2.Name = "tbMAdicional2"
        Me.tbMAdicional2.Size = New System.Drawing.Size(157, 24)
        Me.tbMAdicional2.TabIndex = 74
        '
        'tbCAdicional2
        '
        Me.tbCAdicional2.BackColor = System.Drawing.Color.White
        Me.tbCAdicional2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbCAdicional2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCAdicional2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCAdicional2.Location = New System.Drawing.Point(339, 21)
        Me.tbCAdicional2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCAdicional2.MaxLength = 255
        Me.tbCAdicional2.Name = "tbCAdicional2"
        Me.tbCAdicional2.Size = New System.Drawing.Size(157, 24)
        Me.tbCAdicional2.TabIndex = 73
        '
        'UPplazo
        '
        Me.UPplazo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UPplazo.Location = New System.Drawing.Point(217, 309)
        Me.UPplazo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.UPplazo.Maximum = New Decimal(New Integer() {24, 0, 0, 0})
        Me.UPplazo.Name = "UPplazo"
        Me.UPplazo.Size = New System.Drawing.Size(160, 24)
        Me.UPplazo.TabIndex = 81
        '
        'PanelMensualidad
        '
        Me.PanelMensualidad.Controls.Add(Me.tbMAdicional1)
        Me.PanelMensualidad.Controls.Add(Me.TextBoxMensualidad)
        Me.PanelMensualidad.Controls.Add(Label16)
        Me.PanelMensualidad.Controls.Add(Me.tbMAdicional2)
        Me.PanelMensualidad.Controls.Add(Label12)
        Me.PanelMensualidad.Controls.Add(Label14)
        Me.PanelMensualidad.Enabled = False
        Me.PanelMensualidad.Location = New System.Drawing.Point(211, 181)
        Me.PanelMensualidad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PanelMensualidad.Name = "PanelMensualidad"
        Me.PanelMensualidad.Size = New System.Drawing.Size(515, 57)
        Me.PanelMensualidad.TabIndex = 82
        '
        'CheckBoxMensualidad
        '
        Me.CheckBoxMensualidad.AutoSize = True
        Me.CheckBoxMensualidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxMensualidad.Location = New System.Drawing.Point(48, 207)
        Me.CheckBoxMensualidad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBoxMensualidad.Name = "CheckBoxMensualidad"
        Me.CheckBoxMensualidad.Size = New System.Drawing.Size(124, 22)
        Me.CheckBoxMensualidad.TabIndex = 83
        Me.CheckBoxMensualidad.Text = "&Mensualidad"
        Me.CheckBoxMensualidad.UseVisualStyleBackColor = True
        '
        'CheckBoxContratacion
        '
        Me.CheckBoxContratacion.AutoSize = True
        Me.CheckBoxContratacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxContratacion.Location = New System.Drawing.Point(48, 150)
        Me.CheckBoxContratacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBoxContratacion.Name = "CheckBoxContratacion"
        Me.CheckBoxContratacion.Size = New System.Drawing.Size(127, 22)
        Me.CheckBoxContratacion.TabIndex = 84
        Me.CheckBoxContratacion.Text = "&Contratación"
        Me.CheckBoxContratacion.UseVisualStyleBackColor = True
        '
        'PanelContratacion
        '
        Me.PanelContratacion.Controls.Add(Me.tbCAdicional1)
        Me.PanelContratacion.Controls.Add(Me.TextBoxContratacion)
        Me.PanelContratacion.Controls.Add(Me.tbCAdicional2)
        Me.PanelContratacion.Controls.Add(Label11)
        Me.PanelContratacion.Controls.Add(Label13)
        Me.PanelContratacion.Controls.Add(Label15)
        Me.PanelContratacion.Enabled = False
        Me.PanelContratacion.Location = New System.Drawing.Point(209, 128)
        Me.PanelContratacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PanelContratacion.Name = "PanelContratacion"
        Me.PanelContratacion.Size = New System.Drawing.Size(517, 53)
        Me.PanelContratacion.TabIndex = 85
        '
        'CheckBoxRenta
        '
        Me.CheckBoxRenta.AutoSize = True
        Me.CheckBoxRenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxRenta.Location = New System.Drawing.Point(48, 263)
        Me.CheckBoxRenta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBoxRenta.Name = "CheckBoxRenta"
        Me.CheckBoxRenta.Size = New System.Drawing.Size(74, 22)
        Me.CheckBoxRenta.TabIndex = 87
        Me.CheckBoxRenta.Text = "&Renta"
        Me.CheckBoxRenta.UseVisualStyleBackColor = True
        '
        'PanelRenta
        '
        Me.PanelRenta.Controls.Add(Me.btnGuardarModelo)
        Me.PanelRenta.Enabled = False
        Me.PanelRenta.Location = New System.Drawing.Point(211, 238)
        Me.PanelRenta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PanelRenta.Name = "PanelRenta"
        Me.PanelRenta.Size = New System.Drawing.Size(347, 57)
        Me.PanelRenta.TabIndex = 86
        '
        'btnGuardarModelo
        '
        Me.btnGuardarModelo.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardarModelo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardarModelo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardarModelo.ForeColor = System.Drawing.Color.Black
        Me.btnGuardarModelo.Location = New System.Drawing.Point(11, 12)
        Me.btnGuardarModelo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnGuardarModelo.Name = "btnGuardarModelo"
        Me.btnGuardarModelo.Size = New System.Drawing.Size(323, 34)
        Me.btnGuardarModelo.TabIndex = 88
        Me.btnGuardarModelo.Text = "Establecer Renta de Aparatos"
        Me.btnGuardarModelo.UseVisualStyleBackColor = False
        '
        'FrmPromocionesNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1199, 727)
        Me.Controls.Add(Me.CheckBoxRenta)
        Me.Controls.Add(Me.PanelRenta)
        Me.Controls.Add(Me.PanelContratacion)
        Me.Controls.Add(Me.CheckBoxContratacion)
        Me.Controls.Add(Me.CheckBoxMensualidad)
        Me.Controls.Add(Me.PanelMensualidad)
        Me.Controls.Add(Me.UPplazo)
        Me.Controls.Add(Label10)
        Me.Controls.Add(Me.DateTimePickerFinal)
        Me.Controls.Add(Me.DateTimePickerInicio)
        Me.Controls.Add(Label9)
        Me.Controls.Add(Me.CheckBoxActiva)
        Me.Controls.Add(Label8)
        Me.Controls.Add(Me.ComboBoxTipSer)
        Me.Controls.Add(Label7)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.EliminarServicio)
        Me.Controls.Add(Me.AgregarServicio)
        Me.Controls.Add(Me.EliminarDistribuidor)
        Me.Controls.Add(Me.AgregarDistribuidor)
        Me.Controls.Add(Me.ListBoxServicios)
        Me.Controls.Add(Me.ComboBoxServicios)
        Me.Controls.Add(Label6)
        Me.Controls.Add(Me.ListBoxPlazas)
        Me.Controls.Add(Me.ComboBoxPlazas)
        Me.Controls.Add(Label17)
        Me.Controls.Add(Me.TextBoxPlazoForzoso)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Me.TextBoxNombre)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.BindingNavigatorPromocion)
        Me.Controls.Add(Me.TextBoxClave)
        Me.Controls.Add(Label1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmPromocionesNew"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promoción"
        CType(Me.BindingNavigatorPromocion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigatorPromocion.ResumeLayout(False)
        Me.BindingNavigatorPromocion.PerformLayout()
        CType(Me.UPplazo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelMensualidad.ResumeLayout(False)
        Me.PanelMensualidad.PerformLayout()
        Me.PanelContratacion.ResumeLayout(False)
        Me.PanelContratacion.PerformLayout()
        Me.PanelRenta.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBoxClave As System.Windows.Forms.TextBox
    Friend WithEvents BindingNavigatorPromocion As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents TextBoxNombre As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxContratacion As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxMensualidad As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxPlazoForzoso As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxPlazas As System.Windows.Forms.ComboBox
    Friend WithEvents ListBoxPlazas As System.Windows.Forms.ListBox
    Friend WithEvents ListBoxServicios As System.Windows.Forms.ListBox
    Friend WithEvents ComboBoxServicios As System.Windows.Forms.ComboBox
    Friend WithEvents AgregarDistribuidor As System.Windows.Forms.Button
    Friend WithEvents EliminarDistribuidor As System.Windows.Forms.Button
    Friend WithEvents EliminarServicio As System.Windows.Forms.Button
    Friend WithEvents AgregarServicio As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ComboBoxTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBoxActiva As System.Windows.Forms.CheckBox
    Friend WithEvents DateTimePickerInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePickerFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents tbMAdicional1 As System.Windows.Forms.TextBox
    Friend WithEvents tbCAdicional1 As System.Windows.Forms.TextBox
    Friend WithEvents tbMAdicional2 As System.Windows.Forms.TextBox
    Friend WithEvents tbCAdicional2 As System.Windows.Forms.TextBox
    Friend WithEvents UPplazo As System.Windows.Forms.NumericUpDown
    Friend WithEvents PanelMensualidad As System.Windows.Forms.Panel
    Friend WithEvents CheckBoxMensualidad As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxContratacion As System.Windows.Forms.CheckBox
    Friend WithEvents PanelContratacion As System.Windows.Forms.Panel
    Friend WithEvents CheckBoxRenta As System.Windows.Forms.CheckBox
    Friend WithEvents PanelRenta As System.Windows.Forms.Panel
    Friend WithEvents btnGuardarModelo As System.Windows.Forms.Button
End Class
