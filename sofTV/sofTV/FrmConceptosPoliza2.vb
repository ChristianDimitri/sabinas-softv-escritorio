﻿Public Class FrmConceptosPoliza2
    Dim locEs_Principal As Boolean = False
    Private Sub FrmConceptosPoliza2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaDistribuidores()
        llenaSucursales()
        llenaSucursalesOtros()
        llenaGridConceptos()
    End Sub

    Public Sub llenaDistribuidores()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_plaza", SqlDbType.Int, 0)
            ComboBoxPlazas.DataSource = BaseII.ConsultaDT("Muestra_Plazas")
        Catch ex As Exception

        End Try
    End Sub

    Public Sub llenaSucursales()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlazas.SelectedValue)
            ComboBoxSucursales.DataSource = BaseII.ConsultaDT("sp_dameSucursalesConceptos")
            If ComboBoxSucursales.Items.Count = 0 Then
                ComboBoxSucursales.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub llenaGridConceptos()
        Try
            If Me.TabControl1.SelectedTab.Name = "tpDebe" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
                dgvConceptos.DataSource = BaseII.ConsultaDT("sp_dameConceptosCuenta")
            End If
            If Me.TabControl1.SelectedTab.Name = "tpHaber" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
                dgvConceptos.DataSource = BaseII.ConsultaDT("sp_dameConceptosCuenta")
            End If
            If Me.TabControl1.SelectedTab.Name = "tpImpuestos" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
                dgvConceptos.DataSource = BaseII.ConsultaDT("sp_dameConceptosCuenta")
            End If
            If Me.TabControl1.SelectedTab.Name = "tpOtrasSucursales" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
                dgvConceptos.DataSource = BaseII.ConsultaDT("sp_dameConceptosCuenta")
            End If
        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub ComboBoxPlazas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxPlazas.SelectedIndexChanged
        llenaSucursales()
    End Sub

    Private Sub btnAgregarDebe_Click(sender As Object, e As EventArgs) Handles btnAgregarDebe.Click
        If tbCuentaDebe.Text = "" Then
            MsgBox("Debe capturar la cuenta.")
            Exit Sub
        End If
        If ComboBoxSucursales.Items.Count = 0 Then
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlazas.SelectedValue)
            BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, ComboBoxSucursales.SelectedValue)
            BaseII.CreateMyParameter("@cuenta", SqlDbType.VarChar, tbCuentaDebe.Text)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.ProcedimientoOutPut("sp_insertaConceptoDebe")
            If BaseII.dicoPar("@error") = "Todo bien" Then
                llenaGridConceptos()
                MsgBox("Concepto agregado exitosamente.")
            Else
                MsgBox(BaseII.dicoPar("@error"))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Try
            If Me.TabControl1.SelectedTab.Name = "tpDebe" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_concepto", SqlDbType.Int, dgvConceptos.SelectedCells(0).Value)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
                BaseII.Inserta("sp_eliminaConceptoDebe")
                llenaGridConceptos()
            End If
            If Me.TabControl1.SelectedTab.Name = "tpHaber" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, dgvConceptos.SelectedCells(2).Value.ToString)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
                BaseII.Inserta("sp_eliminaConceptoDebe")
                llenaGridConceptos()
            End If
            If Me.TabControl1.SelectedTab.Name = "tpImpuestos" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, dgvConceptos.SelectedCells(2).Value.ToString)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
                BaseII.Inserta("sp_eliminaConceptoDebe")
                llenaGridConceptos()
            End If
            If Me.TabControl1.SelectedTab.Name = "tpOtrasSucursales" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clave", SqlDbType.VarChar, dgvConceptos.SelectedCells(4).Value.ToString)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
                BaseII.Inserta("sp_eliminaConceptoDebe")
                llenaGridConceptos()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TabControl1_TabIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.TabIndexChanged
        If Me.TabControl1.SelectedTab.Name = "tpHaber" Then
            llenaServicios()
            llenaConceptos()
        End If
        llenaGridConceptos()
    End Sub

    Private Sub llenaServicios()
        Try
            BaseII.limpiaParametros()
            ComboBoxServicio.DataSource = BaseII.ConsultaDT("sp_dameServiciosConceptos")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxServicio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxServicio.SelectedIndexChanged
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, ComboBoxServicio.SelectedValue)
        BaseII.CreateMyParameter("@es_principal", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("sp_dimeSiPrincipalConceptos")
        If BaseII.dicoPar("@es_principal") = 1 Then
            locEs_Principal = True
            ComboBoxConcepto.Visible = True
            Label6.Visible = True
        Else
            ComboBoxConcepto.Visible = False
            Label6.Visible = False
        End If
    End Sub

    Private Sub llenaConceptos()
        BaseII.limpiaParametros()
        ComboBoxConcepto.DataSource = BaseII.ConsultaDT("sp_dameConceptosServicios")
    End Sub

    Private Sub btnAgregarHaber_Click(sender As Object, e As EventArgs) Handles btnAgregarHaber.Click
        If tbCuentaHaber.Text = "" Then
            MsgBox("Debe capturar la cuenta.")
            Exit Sub
        End If
        Dim locClave As Integer
        Dim descripcion As String
        If locEs_Principal Then
            locClave = ComboBoxConcepto.SelectedValue
            descripcion = ComboBoxConcepto.Text + " de " + ComboBoxServicio.Text
        Else
            locClave = 0
            descripcion = ComboBoxServicio.Text
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, ComboBoxServicio.Text)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, ComboBoxServicio.SelectedValue)
        BaseII.CreateMyParameter("@clave", SqlDbType.Int, locClave)
        BaseII.CreateMyParameter("@cuenta", SqlDbType.VarChar, tbCuentaHaber.Text)
        BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.VarChar, 500)
        BaseII.ProcedimientoOutPut("sp_insertaConceptoHaber")
        If BaseII.dicoPar("@error") = "Todo bien" Then
            llenaGridConceptos()
            MsgBox("Concepto agregado exitosamente.")
        Else
            MsgBox(BaseII.dicoPar("@error"))
        End If
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
        If Me.TabControl1.SelectedTab.Name = "tpHaber" Then
            llenaServicios()
            llenaConceptos()
        End If
        llenaGridConceptos()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAgregarImpuestos_Click(sender As Object, e As EventArgs) Handles btnAgregarImpuestos.Click
        If tbCuentaImpuestos.Text = "" Then
            MsgBox("Debe capturar la cuenta.")
            Exit Sub
        End If
        If tbDescripcionImpuestos.Text = "" Then
            MsgBox("Debe capturar la descripción.")
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, tbDescripcionImpuestos.Text)
        BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, ComboBoxImpuesto.Text)
        BaseII.CreateMyParameter("@cuenta", SqlDbType.VarChar, tbCuentaImpuestos.Text)
        BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.VarChar, 500)
        BaseII.ProcedimientoOutPut("sp_insertaConceptoImpuestos")
        If BaseII.dicoPar("@error") = "Todo bien" Then
            llenaGridConceptos()
            MsgBox("Concepto agregado exitosamente.")
        Else
            MsgBox(BaseII.dicoPar("@error"))
        End If
    End Sub

    Private Sub llenaSucursalesOtros()
        Try
            BaseII.limpiaParametros()
            ComboBoxSucursalOtros.DataSource = BaseII.ConsultaDT("sp_dameSucursalesOtrosConceptos")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnAgregarSucursalOtros_Click(sender As Object, e As EventArgs) Handles btnAgregarSucursalOtros.Click
        If tbCuentaSucursalOtros.Text = "" Then
            MsgBox("Debe capturar la cuenta.")
            Exit Sub
        End If
        If ComboBoxSucursalOtros.Items.Count = 0 Then
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, ComboBoxSucursalOtros.SelectedValue)
            BaseII.CreateMyParameter("@cuenta", SqlDbType.VarChar, tbCuentaSucursalOtros.Text)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.ProcedimientoOutPut("sp_insertaConceptoSucursalesOtros")
            If BaseII.dicoPar("@error") = "Todo bien" Then
                llenaGridConceptos()
                MsgBox("Concepto agregado exitosamente.")
            Else
                MsgBox(BaseII.dicoPar("@error"))
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class