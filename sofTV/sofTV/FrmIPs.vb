﻿Public Class FrmIPs

    Dim status = ""
    Dim contador As Integer = 0
    Dim fechAsignacion As String = ""
    Dim fechaLiberacion As String = ""


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub


    Private Sub BrwRedes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        Me.Text = "Detalle IP - " + iD_ip.ToString() 'iD_redSelec

        TextBox1.Enabled = False
        TextBox2.Enabled = False
        TextBox3.Enabled = False
        asignacionDate.Enabled = False
        liberacionDate.Enabled = False

        ComboBox1.Enabled = False
        Button2.Enabled = False

        If OpcAccion = "C" Then

        ElseIf OpcAccion = "M" Then
            ComboBox1.Enabled = True
            Button2.Enabled = True
        ElseIf OpcAccion = "N" Then
            Me.Text = "Catálogo de IP"
        End If

        mostrarDetalle()

    End Sub

    Private Sub mostrarDetalle()

        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@IdIP", SqlDbType.BigInt, iD_ip)
        '    BaseII.CreateMyParameter("@IdIP", SqlDbType.BigInt, iD_redSelec)
        Dim DTResults As DataTable = BaseII.ConsultaDT("GetOneIP")

        TextBox1.Text = DTResults.Rows(0)(5).ToString()
        TextBox2.Text = DTResults.Rows(0)(10).ToString()
        TextBox3.Text = DTResults.Rows(0)(9).ToString()
        fechAsignacion = DTResults.Rows(0)(7).ToString()
        fechaLiberacion = DTResults.Rows(0)(8).ToString()
        status = DTResults.Rows(0)(6).ToString()

        asignacionDate.Text = fechAsignacion
        liberacionDate.Text = fechaLiberacion

        BaseII.limpiaParametros()
        ComboBox1.DataSource = BaseII.ConsultaDT("GetStatus_Combo")
        ComboBox1.SelectedValue = status

        contador = 1

    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click


        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdIP", SqlDbType.BigInt, iD_ip)
            '  BaseII.CreateMyParameter("@IdIP", SqlDbType.BigInt, iD_redSelec)
            BaseII.CreateMyParameter("@FechaLiberacion", SqlDbType.Date, liberacionDate.Text)
            BaseII.CreateMyParameter("@FechaASignacion", SqlDbType.Date, asignacionDate.Text)
            BaseII.CreateMyParameter("@Status", SqlDbType.VarChar, ComboBox1.SelectedValue)
            BaseII.ConsultaDT("GetUpdateIP")

        Catch ex As Exception
            MsgBox("Error. " & ex.Message)
        End Try

        If origenForm = "IpRed" Then
            BrwIPs.llenaGrid_idIP()
        Else
            BrwIPs.llenaGrid()
        End If

        ' BrwIPs.llenaGrid(0)
        '   BrwIPs.llenaGrid_idIP()
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged

        Dim nuevoStatus = ComboBox1.SelectedValue

        Dim dt As Date = Date.Today
        Dim fechAhora As String
        fechAhora = Format(dt, "dd/MM/yyyy")

        If status = "D" Then      'Disponible

            If nuevoStatus = "A" Then
                asignacionDate.Enabled = True
                If contador = 1 Then
                    asignacionDate.Text = fechAhora
                End If

            Else
                asignacionDate.Enabled = False

                If contador = 1 Then
                    asignacionDate.Text = fechAsignacion
                End If
            End If

        ElseIf status = "A" Then 'Asignada

            If nuevoStatus = "D" Then
                liberacionDate.Enabled = True

                If contador = 1 Then
                    liberacionDate.Text = fechAhora
                End If
            Else
                liberacionDate.Enabled = False

                If contador = 1 Then
                    liberacionDate.Text = fechaLiberacion
                End If

            End If

        End If



    End Sub


End Class