Imports System.Data.SqlClient
Public Class BrwVisitas

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub Busqueda(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Select Case op
            Case 0
                Me.Busqueda_CatalogoVisitasTableAdapter.Connection = CON
                Me.Busqueda_CatalogoVisitasTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busqueda_CatalogoVisitas, 0, "", op)
            Case 1
                Me.Busqueda_CatalogoVisitasTableAdapter.Connection = CON
                Me.Busqueda_CatalogoVisitasTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busqueda_CatalogoVisitas, CLng(Me.TextBox1.Text), "", op)
            Case 2
                Me.Busqueda_CatalogoVisitasTableAdapter.Connection = CON
                Me.Busqueda_CatalogoVisitasTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busqueda_CatalogoVisitas, 0, Me.TextBox2.Text, op)
        End Select
        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
        If Asc(e.KeyChar) = 13 Then
            Busqueda(1)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox2, Asc(LCase(e.KeyChar)), "S")))
        If Asc(e.KeyChar) = 13 Then
            Busqueda(2)
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busqueda(1)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busqueda(2)
    End Sub

    Private Sub BrwVisitas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If LocBndVisitas = True Then
            LocBndVisitas = False
            Busqueda(0)
        End If
    End Sub

    Private Sub BrwVisitas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busqueda(0)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        LocOpVisita = "N"
        FrmVisitas.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        LocOpVisita = "C"
        Locclv_visita = CLng(Me.Clv_calleLabel2.Text)
        FrmVisitas.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        LocOpVisita = "M"
        Locclv_visita = CLng(Me.Clv_calleLabel2.Text)
        FrmVisitas.Show()
    End Sub
End Class