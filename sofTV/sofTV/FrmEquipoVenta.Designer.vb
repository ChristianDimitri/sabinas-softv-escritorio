<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEquipoVenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_EquipoLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEquipoVenta))
        Me.Clv_EquipoTextBox = New System.Windows.Forms.TextBox()
        Me.Consulta_EquiposVentaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Muestra_Rel_EquipoPlanDataGridView = New System.Windows.Forms.DataGridView()
        Me.DescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Muestra_Rel_EquipoPlanBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Consulta_EquiposVentaTableAdapter = New sofTV.DataSetLidia2TableAdapters.Consulta_EquiposVentaTableAdapter()
        Me.Consulta_EquiposVentaBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_EquiposVentaBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.MuestraServiciosTableAdapter = New sofTV.DataSetLidia2TableAdapters.MuestraServiciosTableAdapter()
        Me.Muestra_Rel_EquipoPlanTableAdapter = New sofTV.DataSetLidia2TableAdapters.Muestra_Rel_EquipoPlanTableAdapter()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextCosto = New System.Windows.Forms.TextBox()
        Clv_EquipoLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        CType(Me.Consulta_EquiposVentaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Rel_EquipoPlanDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Rel_EquipoPlanBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_EquiposVentaBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_EquiposVentaBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_EquipoLabel
        '
        Clv_EquipoLabel.AutoSize = True
        Clv_EquipoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_EquipoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_EquipoLabel.Location = New System.Drawing.Point(87, 60)
        Clv_EquipoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_EquipoLabel.Name = "Clv_EquipoLabel"
        Clv_EquipoLabel.Size = New System.Drawing.Size(140, 18)
        Clv_EquipoLabel.TabIndex = 1
        Clv_EquipoLabel.Text = "Clave de Equipo :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(163, 102)
        NombreLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(78, 18)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Nombre :"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(129, 150)
        DescripcionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(108, 18)
        DescripcionLabel.TabIndex = 5
        DescripcionLabel.Text = "Descripción :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(32, 326)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(452, 25)
        Label1.TabIndex = 13
        Label1.Text = "Costo del Equipo de Acuerdo al Plan Tarifario"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(125, 396)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(109, 18)
        Label2.TabIndex = 15
        Label2.Text = "Plan Tarifario"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(183, 263)
        Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(64, 18)
        Label5.TabIndex = 21
        Label5.Text = "Costo :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(665, 422)
        Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(118, 18)
        Label3.TabIndex = 25
        Label3.Text = "% Descuento :"
        '
        'Clv_EquipoTextBox
        '
        Me.Clv_EquipoTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_EquipoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_EquiposVentaBindingSource, "Clv_Equipo", True))
        Me.Clv_EquipoTextBox.Location = New System.Drawing.Point(275, 59)
        Me.Clv_EquipoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_EquipoTextBox.Name = "Clv_EquipoTextBox"
        Me.Clv_EquipoTextBox.ReadOnly = True
        Me.Clv_EquipoTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clv_EquipoTextBox.TabIndex = 2
        '
        'Consulta_EquiposVentaBindingSource
        '
        Me.Consulta_EquiposVentaBindingSource.DataMember = "Consulta_EquiposVenta"
        Me.Consulta_EquiposVentaBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NombreTextBox
        '
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_EquiposVentaBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(275, 103)
        Me.NombreTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(661, 24)
        Me.NombreTextBox.TabIndex = 4
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_EquiposVentaBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(275, 146)
        Me.DescripcionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(661, 104)
        Me.DescripcionTextBox.TabIndex = 6
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(900, 837)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(196, 47)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Muestra_Rel_EquipoPlanDataGridView
        '
        Me.Muestra_Rel_EquipoPlanDataGridView.AllowUserToAddRows = False
        Me.Muestra_Rel_EquipoPlanDataGridView.AllowUserToDeleteRows = False
        Me.Muestra_Rel_EquipoPlanDataGridView.AllowUserToOrderColumns = True
        Me.Muestra_Rel_EquipoPlanDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Muestra_Rel_EquipoPlanDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Muestra_Rel_EquipoPlanDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DescripcionDataGridViewTextBoxColumn, Me.NombreDataGridViewTextBoxColumn, Me.CostoDataGridViewTextBoxColumn})
        Me.Muestra_Rel_EquipoPlanDataGridView.DataSource = Me.Muestra_Rel_EquipoPlanBindingSource
        Me.Muestra_Rel_EquipoPlanDataGridView.Location = New System.Drawing.Point(37, 543)
        Me.Muestra_Rel_EquipoPlanDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Muestra_Rel_EquipoPlanDataGridView.Name = "Muestra_Rel_EquipoPlanDataGridView"
        Me.Muestra_Rel_EquipoPlanDataGridView.ReadOnly = True
        Me.Muestra_Rel_EquipoPlanDataGridView.Size = New System.Drawing.Size(1059, 271)
        Me.Muestra_Rel_EquipoPlanDataGridView.TabIndex = 9
        '
        'DescripcionDataGridViewTextBoxColumn
        '
        Me.DescripcionDataGridViewTextBoxColumn.DataPropertyName = "descripcion"
        Me.DescripcionDataGridViewTextBoxColumn.HeaderText = "Servicio"
        Me.DescripcionDataGridViewTextBoxColumn.Name = "DescripcionDataGridViewTextBoxColumn"
        Me.DescripcionDataGridViewTextBoxColumn.ReadOnly = True
        Me.DescripcionDataGridViewTextBoxColumn.Width = 350
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "nombre"
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "Equipo"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        Me.NombreDataGridViewTextBoxColumn.ReadOnly = True
        Me.NombreDataGridViewTextBoxColumn.Width = 250
        '
        'CostoDataGridViewTextBoxColumn
        '
        Me.CostoDataGridViewTextBoxColumn.DataPropertyName = "Costo"
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.CostoDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.CostoDataGridViewTextBoxColumn.HeaderText = "% Decuento"
        Me.CostoDataGridViewTextBoxColumn.Name = "CostoDataGridViewTextBoxColumn"
        Me.CostoDataGridViewTextBoxColumn.ReadOnly = True
        Me.CostoDataGridViewTextBoxColumn.Width = 150
        '
        'Muestra_Rel_EquipoPlanBindingSource
        '
        Me.Muestra_Rel_EquipoPlanBindingSource.DataMember = "Muestra_Rel_EquipoPlan"
        Me.Muestra_Rel_EquipoPlanBindingSource.DataSource = Me.DataSetLidia2
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(85, 480)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(160, 30)
        Me.Button2.TabIndex = 10
        Me.Button2.Text = "&AGREGAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(421, 480)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(160, 30)
        Me.Button3.TabIndex = 11
        Me.Button3.Text = "&MODIFICAR"
        Me.Button3.UseVisualStyleBackColor = True
        Me.Button3.Visible = False
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(253, 480)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(160, 30)
        Me.Button4.TabIndex = 12
        Me.Button4.Text = "&BORRAR"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MuestraServiciosBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(129, 418)
        Me.ComboBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(513, 28)
        Me.ComboBox1.TabIndex = 14
        Me.ComboBox1.ValueMember = "Clv_Servicio"
        '
        'MuestraServiciosBindingSource
        '
        Me.MuestraServiciosBindingSource.DataMember = "MuestraServicios"
        Me.MuestraServiciosBindingSource.DataSource = Me.DataSetLidia2
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(805, 418)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox3.MaxLength = 3
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(91, 26)
        Me.TextBox3.TabIndex = 20
        '
        'Consulta_EquiposVentaTableAdapter
        '
        Me.Consulta_EquiposVentaTableAdapter.ClearBeforeFill = True
        '
        'Consulta_EquiposVentaBindingNavigator
        '
        Me.Consulta_EquiposVentaBindingNavigator.AddNewItem = Nothing
        Me.Consulta_EquiposVentaBindingNavigator.BindingSource = Me.Consulta_EquiposVentaBindingSource
        Me.Consulta_EquiposVentaBindingNavigator.CountItem = Nothing
        Me.Consulta_EquiposVentaBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_EquiposVentaBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_EquiposVentaBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.Consulta_EquiposVentaBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_EquiposVentaBindingNavigatorSaveItem})
        Me.Consulta_EquiposVentaBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_EquiposVentaBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_EquiposVentaBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_EquiposVentaBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_EquiposVentaBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_EquiposVentaBindingNavigator.Name = "Consulta_EquiposVentaBindingNavigator"
        Me.Consulta_EquiposVentaBindingNavigator.PositionItem = Nothing
        Me.Consulta_EquiposVentaBindingNavigator.Size = New System.Drawing.Size(1096, 28)
        Me.Consulta_EquiposVentaBindingNavigator.TabIndex = 22
        Me.Consulta_EquiposVentaBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(103, 25)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'Consulta_EquiposVentaBindingNavigatorSaveItem
        '
        Me.Consulta_EquiposVentaBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_EquiposVentaBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_EquiposVentaBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_EquiposVentaBindingNavigatorSaveItem.Name = "Consulta_EquiposVentaBindingNavigatorSaveItem"
        Me.Consulta_EquiposVentaBindingNavigatorSaveItem.Size = New System.Drawing.Size(156, 25)
        Me.Consulta_EquiposVentaBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'MuestraServiciosTableAdapter
        '
        Me.MuestraServiciosTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Rel_EquipoPlanTableAdapter
        '
        Me.Muestra_Rel_EquipoPlanTableAdapter.ClearBeforeFill = True
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Muestra_Rel_EquipoPlanBindingSource, "Clv_Servicio", True))
        Me.TextBox1.Location = New System.Drawing.Point(216, 929)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(231, 22)
        Me.TextBox1.TabIndex = 23
        '
        'TextCosto
        '
        Me.TextCosto.CausesValidation = False
        Me.TextCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextCosto.Location = New System.Drawing.Point(275, 263)
        Me.TextCosto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextCosto.Name = "TextCosto"
        Me.TextCosto.Size = New System.Drawing.Size(151, 26)
        Me.TextCosto.TabIndex = 24
        '
        'FrmEquipoVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1113, 912)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Me.TextCosto)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Consulta_EquiposVentaBindingNavigator)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Muestra_Rel_EquipoPlanDataGridView)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Clv_EquipoLabel)
        Me.Controls.Add(Me.Clv_EquipoTextBox)
        Me.Controls.Add(NombreLabel)
        Me.Controls.Add(Me.NombreTextBox)
        Me.Controls.Add(DescripcionLabel)
        Me.Controls.Add(Me.DescripcionTextBox)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmEquipoVenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Equipos a la Venta"
        CType(Me.Consulta_EquiposVentaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Rel_EquipoPlanDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Rel_EquipoPlanBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_EquiposVentaBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_EquiposVentaBindingNavigator.ResumeLayout(False)
        Me.Consulta_EquiposVentaBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Clv_EquipoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Muestra_Rel_EquipoPlanDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents Consulta_EquiposVentaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_EquiposVentaTableAdapter As sofTV.DataSetLidia2TableAdapters.Consulta_EquiposVentaTableAdapter
    Friend WithEvents Consulta_EquiposVentaBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_EquiposVentaBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents MuestraServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServiciosTableAdapter As sofTV.DataSetLidia2TableAdapters.MuestraServiciosTableAdapter
    Friend WithEvents Muestra_Rel_EquipoPlanBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Rel_EquipoPlanTableAdapter As sofTV.DataSetLidia2TableAdapters.Muestra_Rel_EquipoPlanTableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextCosto As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
