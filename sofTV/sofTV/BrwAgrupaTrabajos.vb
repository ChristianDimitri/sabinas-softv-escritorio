﻿Public Class BrwAgrupaTrabajos

    Private Sub BrwAgrupaTrabajos_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            llenaAgrupatrabajos(0)
            If Me.ResumenClienteDataGridView.RowCount > 0 Then
                GloClvAgruTrabajo = Me.ResumenClienteDataGridView.SelectedCells(0).Value.ToString
                GloDescAgruTrabajo = Me.ResumenClienteDataGridView.SelectedCells(1).Value.ToString
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaAgrupatrabajos(ByVal Op As String)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, TextBox1.Text, 250)

        ResumenClienteDataGridView.DataSource = BaseII.ConsultaDT("Usp_ConsultaAgrupaTrabajos")

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        llenaAgrupatrabajos(1)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If GloClvAgruTrabajo > 0 And GloDescAgruTrabajo.Length > 0 Then
            OpAT = "M"
            Dim frmAgrupaTrabajos As New FrmAgrupaTrabajos
            frmAgrupaTrabajos.ShowDialog()
            llenaAgrupatrabajos(0)
        Else
            MsgBox("Selecciona una Agrupación")
        End If

    End Sub

    Private Sub ResumenClienteDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles ResumenClienteDataGridView.CellClick
        If Me.ResumenClienteDataGridView.RowCount > 0 Then
            GloClvAgruTrabajo = Me.ResumenClienteDataGridView.SelectedCells(0).Value.ToString
            GloDescAgruTrabajo = Me.ResumenClienteDataGridView.SelectedCells(1).Value.ToString
        End If
    End Sub

    Private Sub ResumenClienteDataGridView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ResumenClienteDataGridView.CellContentClick

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If GloClvAgruTrabajo > 0 And GloDescAgruTrabajo.Length > 0 Then
            Dim msg As String
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clave", SqlDbType.Int, GloClvAgruTrabajo)
            BaseII.CreateMyParameter("@msg", ParameterDirection.Output, SqlDbType.VarChar, 100)
            BaseII.ProcedimientoOutPut("Usp_BorraAgrupaTrabajos")
            msg = BaseII.dicoPar("@msg").ToString

            If msg.Length > 0 Then
                MsgBox(msg, MsgBoxStyle.Information)
            Else
                MsgBox("Eliminado con éxito")
            End If
        Else
            MsgBox("Selecciona una Agrupación")
        End If
    End Sub

    Private Sub Nuevo_Click(sender As Object, e As EventArgs) Handles Nuevo.Click
        OpAT = "N"
        Dim frmAgrupaTrabajos As New FrmAgrupaTrabajos
        frmAgrupaTrabajos.ShowDialog()
        llenaAgrupatrabajos(0)
    End Sub
End Class