﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.IO
Public Class FrmImprimirAgenda

    Private Sub FrmImprimirAgenda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ImprimirAgenda()
    End Sub
    Private Sub ImprimirAgenda()
        Try
            Dim fechas As String = Nothing
            Dim customersByCityReport As ReportDocument = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            reportPath = RutaReportes + "\ReporteAgendaTecnico.rpt"
            mySelectFormula = "Agenda de Actividades Del Técnico"

            Dim dSet As New DataSet
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@op", SqlDbType.Int, CInt(LocopAgnd))
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, LocClv_tecnico)
            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1Agnd)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2Agnd)

            Dim listatablas As New List(Of String)
            listatablas.Add("REPORTEAGENDATECNICO")
            dSet = BaseII.ConsultaDS("REPORTEAGENDATECNICO", listatablas)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dSet)

            fechas = "del " + LocFecha1Agnd + " al " + LocFecha2Agnd

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("nomservicio").Text = "'" & fechas & "'"
            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAceptar.Click
        ImprimirAgenda()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim customersByCityReport As ReportDocument = New ReportDocument
            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim connectionInfo As New ConnectionInfo
            Dim mySelectFormula As String = Nothing
            reportPath = RutaReportes + "\ReporteOrdenes.rpt"
            mySelectFormula = "Orden De Servicio: "
            Dim DS As New DataSet
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@op", SqlDbType.Int, CInt(LocopAgnd))
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, LocClv_tecnico)
            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1Agnd)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2Agnd)
            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteOrdSer;1")
            listatablas.Add("Comentarios_DetalleOrden")
            listatablas.Add("DameDatosGenerales_2;1")
            listatablas.Add("DetOrdSer")
            listatablas.Add("Trabajos")
            listatablas.Add("ClientesConElMismoPoste")
            DS = BaseII.ConsultaDS("ReporteOrdSerAgenda", listatablas)
            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim customersByCityReport As ReportDocument = New ReportDocument
        Dim reportPath As String = Nothing
        Dim Subtitulo As String = Nothing
        Dim connectionInfo As New ConnectionInfo
        Dim mySelectFormula As String = Nothing
        reportPath = RutaReportes + "\ReporteQuejas.rpt"
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@op", SqlDbType.Int, CInt(LocopAgnd))
        BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, LocClv_tecnico)
        BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1Agnd)
        BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2Agnd)
        BaseII.CreateMyParameter("@clvusuario", SqlDbType.Int, GloClvUsuario)
        Dim lp As List(Of String) = New List(Of String)
        lp.Add("ClientesConElMismoPoste")
        lp.Add("DameDatosGenerales_2")
        lp.Add("ReporteAreaTecnicaQuejas1")
        Dim ds As DataSet = BaseII.ConsultaDS("ReporteQuejaAgenda", lp)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)
        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing
    End Sub
End Class