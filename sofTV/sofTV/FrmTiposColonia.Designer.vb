﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTiposColonia
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_CiudadLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTiposColonia))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CONTipoColonias1BindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONTipoColonias1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.CONCIUDADESBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CONTipo_Colonias1TableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONTipo_Colonias1TableAdapter()
        Clv_CiudadLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.CONTipoColonias1BindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONTipoColonias1BindingNavigator.SuspendLayout()
        CType(Me.CONTipoColonias1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_CiudadLabel
        '
        Clv_CiudadLabel.AutoSize = True
        Clv_CiudadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CiudadLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_CiudadLabel.Location = New System.Drawing.Point(120, 64)
        Clv_CiudadLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_CiudadLabel.Name = "Clv_CiudadLabel"
        Clv_CiudadLabel.Size = New System.Drawing.Size(65, 18)
        Clv_CiudadLabel.TabIndex = 0
        Clv_CiudadLabel.Text = "Clave : "
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(104, 97)
        NombreLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(78, 18)
        NombreLabel.TabIndex = 2
        NombreLabel.Text = "Nombre :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.CONTipoColonias1BindingNavigator)
        Me.Panel1.Controls.Add(Clv_CiudadLabel)
        Me.Panel1.Controls.Add(Me.Clv_CiudadTextBox)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Location = New System.Drawing.Point(16, 15)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(851, 246)
        Me.Panel1.TabIndex = 20
        '
        'CONTipoColonias1BindingNavigator
        '
        Me.CONTipoColonias1BindingNavigator.AddNewItem = Nothing
        Me.CONTipoColonias1BindingNavigator.BindingSource = Me.CONTipoColonias1BindingSource
        Me.CONTipoColonias1BindingNavigator.CountItem = Nothing
        Me.CONTipoColonias1BindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONTipoColonias1BindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONTipoColonias1BindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorSeparator2, Me.BindingNavigatorDeleteItem, Me.CONCIUDADESBindingNavigatorSaveItem})
        Me.CONTipoColonias1BindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONTipoColonias1BindingNavigator.MoveFirstItem = Nothing
        Me.CONTipoColonias1BindingNavigator.MoveLastItem = Nothing
        Me.CONTipoColonias1BindingNavigator.MoveNextItem = Nothing
        Me.CONTipoColonias1BindingNavigator.MovePreviousItem = Nothing
        Me.CONTipoColonias1BindingNavigator.Name = "CONTipoColonias1BindingNavigator"
        Me.CONTipoColonias1BindingNavigator.PositionItem = Nothing
        Me.CONTipoColonias1BindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONTipoColonias1BindingNavigator.Size = New System.Drawing.Size(851, 28)
        Me.CONTipoColonias1BindingNavigator.TabIndex = 1
        Me.CONTipoColonias1BindingNavigator.TabStop = True
        Me.CONTipoColonias1BindingNavigator.Text = "BindingNavigator1"
        '
        'CONTipoColonias1BindingSource
        '
        Me.CONTipoColonias1BindingSource.DataMember = "CONTipo_Colonias1"
        Me.CONTipoColonias1BindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(122, 25)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(105, 25)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 28)
        '
        'CONCIUDADESBindingNavigatorSaveItem
        '
        Me.CONCIUDADESBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCIUDADESBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCIUDADESBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCIUDADESBindingNavigatorSaveItem.Name = "CONCIUDADESBindingNavigatorSaveItem"
        Me.CONCIUDADESBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 25)
        Me.CONCIUDADESBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_CiudadTextBox
        '
        Me.Clv_CiudadTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_CiudadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTipoColonias1BindingSource, "Clave", True))
        Me.Clv_CiudadTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CiudadTextBox.Location = New System.Drawing.Point(200, 62)
        Me.Clv_CiudadTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_CiudadTextBox.Name = "Clv_CiudadTextBox"
        Me.Clv_CiudadTextBox.ReadOnly = True
        Me.Clv_CiudadTextBox.Size = New System.Drawing.Size(133, 24)
        Me.Clv_CiudadTextBox.TabIndex = 1
        Me.Clv_CiudadTextBox.TabStop = False
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTipoColonias1BindingSource, "Concepto", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(200, 95)
        Me.NombreTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(475, 24)
        Me.NombreTextBox.TabIndex = 0
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(685, 268)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONTipo_Colonias1TableAdapter
        '
        Me.CONTipo_Colonias1TableAdapter.ClearBeforeFill = True
        '
        'FrmTiposColonia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(883, 321)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmTiposColonia"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Tipos Colonias"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONTipoColonias1BindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONTipoColonias1BindingNavigator.ResumeLayout(False)
        Me.CONTipoColonias1BindingNavigator.PerformLayout()
        CType(Me.CONTipoColonias1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CONTipoColonias1BindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CONCIUDADESBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents CONTipoColonias1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONTipo_Colonias1TableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONTipo_Colonias1TableAdapter
End Class
