﻿
Public Class FrmDispositivoMovil
    Public Clv_unicanetDigLoc As Integer


    Private Sub FrmCoordenadas_Load(sender As Object, e As EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        dameDispositivoMovil()
    End Sub

    Private Sub dameDispositivoMovil()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_unicanet", SqlDbType.BigInt, Clv_unicanetDigLoc)
        BaseII.CreateMyParameter("@Usuario", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("dameDispositivoMovil")
        tbUser.Text = BaseII.dicoPar("@Usuario").ToString

    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If tbPassword.Text <> TbPasswordConfirma.Text Then
            MsgBox("Las contraseñas son diferentes", MsgBoxStyle.Critical)
            tbPassword.Text = ""
            TbPasswordConfirma.Text = ""
            Exit Sub
        End If

        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_unicanet", SqlDbType.BigInt, Clv_unicanetDigLoc)
            BaseII.CreateMyParameter("@Usuario", SqlDbType.VarChar, tbUser.Text, 50)
            BaseII.CreateMyParameter("@Password", SqlDbType.VarChar, tbPassword.Text, 50)
            BaseII.CreateMyParameter("@guardo", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.CreateMyParameter("@msn", ParameterDirection.Output, SqlDbType.VarChar, 50)

            BaseII.ProcedimientoOutPut("SP_GuardaDispositivoMovil")
            If CBool(BaseII.dicoPar("@guardo").ToString) Then
                Me.Close()
            Else
                MsgBox(BaseII.dicoPar("@msn").ToString, MsgBoxStyle.Critical)
                tbUser.Text = ""
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


End Class