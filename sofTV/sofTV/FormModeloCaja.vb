﻿Imports System.Data.SqlClient

Public Class FormModeloCaja

    Private Sub LLENA_cajas()

        Dim dTable As New DataTable
        Dim i As Integer = 0
        BaseII.limpiaParametros()
        Me.ComboBox1.DataSource = BaseII.ConsultaDT("SP_MUESTRATipoCaja")
        Me.ComboBox1.ValueMember = "NOARTICULO"
        Me.ComboBox1.DisplayMember = "CAJA"

        'DataGridView1.Columns.Item(7).Visible = False
    End Sub

    Private Sub FormModeloCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LLENA_cajas()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        LocNoArticuloCajas = 0
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If ComboBox1.SelectedIndex >= 1 Then
            LocNoArticuloCajas = ComboBox1.SelectedValue
            Me.Close()
        Else
            LocNoArticuloCajas = 0
            MsgBox("Seleccione un modelo de Set Up Box por favor ", MsgBoxStyle.Information, "Importante")
        End If
    End Sub
End Class