﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwToken
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ContratoLabel = New System.Windows.Forms.Label()
        Me.NombreLabel = New System.Windows.Forms.Label()
        Me.ConceptoLabel = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.LabelIdToken = New System.Windows.Forms.Label()
        Me.ConCambioServClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.LabelFechaActivacion = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Fecha_SolLabel1 = New System.Windows.Forms.Label()
        Me.StatusLabel2 = New System.Windows.Forms.Label()
        Me.LabelFactura = New System.Windows.Forms.Label()
        Me.LabelPrecio = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ConceptoLabel2 = New System.Windows.Forms.Label()
        Me.ContratoLabel2 = New System.Windows.Forms.Label()
        Me.NombreLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ConCambioServClienteDataGridView = New System.Windows.Forms.DataGridView()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ComboBoxCiudad = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.RealizarTextBox = New System.Windows.Forms.TextBox()
        Me.ConCambioServClienteTableAdapter = New sofTV.DataSetEricTableAdapters.ConCambioServClienteTableAdapter()
        Me.BorCambioServClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorCambioServClienteTableAdapter = New sofTV.DataSetEricTableAdapters.BorCambioServClienteTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.IdCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tokenCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CONTRATOSencilloCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContratoCompuestoCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOMBRECol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaActivacionCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDesactivacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_FacturaCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.facturaCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreUsuarioCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.LabelUsuario = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ConCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConCambioServClienteDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContratoLabel
        '
        Me.ContratoLabel.AutoSize = True
        Me.ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoLabel.Location = New System.Drawing.Point(7, 68)
        Me.ContratoLabel.Name = "ContratoLabel"
        Me.ContratoLabel.Size = New System.Drawing.Size(65, 15)
        Me.ContratoLabel.TabIndex = 13
        Me.ContratoLabel.Text = "Contrato:"
        '
        'NombreLabel
        '
        Me.NombreLabel.AutoSize = True
        Me.NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreLabel.Location = New System.Drawing.Point(7, 112)
        Me.NombreLabel.Name = "NombreLabel"
        Me.NombreLabel.Size = New System.Drawing.Size(62, 15)
        Me.NombreLabel.TabIndex = 14
        Me.NombreLabel.Text = "Nombre:"
        '
        'ConceptoLabel
        '
        Me.ConceptoLabel.AutoSize = True
        Me.ConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoLabel.Location = New System.Drawing.Point(7, 165)
        Me.ConceptoLabel.Name = "ConceptoLabel"
        Me.ConceptoLabel.Size = New System.Drawing.Size(50, 15)
        Me.ConceptoLabel.TabIndex = 15
        Me.ConceptoLabel.Text = "Token:"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(-8, -3)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBoxCompanias)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel2.Controls.Add(Me.ConCambioServClienteDataGridView)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ComboBoxCiudad)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button2)
        Me.SplitContainer1.Size = New System.Drawing.Size(872, 712)
        Me.SplitContainer1.SplitterDistance = 273
        Me.SplitContainer1.TabIndex = 0
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(28, 51)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(239, 24)
        Me.ComboBoxCompanias.TabIndex = 99
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(25, 32)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(47, 16)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Plaza"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(25, 163)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 16)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Nombre"
        '
        'Button6
        '
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(28, 210)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(80, 27)
        Me.Button6.TabIndex = 11
        Me.Button6.Text = "Buscar"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(28, 182)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(214, 22)
        Me.TextBox2.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Contrato"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.LabelUsuario)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.LabelIdToken)
        Me.Panel1.Controls.Add(Me.LabelFechaActivacion)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Fecha_SolLabel1)
        Me.Panel1.Controls.Add(Me.StatusLabel2)
        Me.Panel1.Controls.Add(Me.LabelFactura)
        Me.Panel1.Controls.Add(Me.ConceptoLabel)
        Me.Panel1.Controls.Add(Me.NombreLabel)
        Me.Panel1.Controls.Add(Me.LabelPrecio)
        Me.Panel1.Controls.Add(Me.ContratoLabel)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.ConceptoLabel2)
        Me.Panel1.Controls.Add(Me.ContratoLabel2)
        Me.Panel1.Controls.Add(Me.NombreLabel2)
        Me.Panel1.Location = New System.Drawing.Point(17, 253)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(255, 458)
        Me.Panel1.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(9, 28)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(25, 15)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "ID:"
        '
        'LabelIdToken
        '
        Me.LabelIdToken.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Contrato", True))
        Me.LabelIdToken.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelIdToken.ForeColor = System.Drawing.Color.White
        Me.LabelIdToken.Location = New System.Drawing.Point(12, 48)
        Me.LabelIdToken.Name = "LabelIdToken"
        Me.LabelIdToken.Size = New System.Drawing.Size(100, 23)
        Me.LabelIdToken.TabIndex = 23
        '
        'ConCambioServClienteBindingSource
        '
        Me.ConCambioServClienteBindingSource.DataMember = "ConCambioServCliente"
        Me.ConCambioServClienteBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.EnforceConstraints = False
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LabelFechaActivacion
        '
        Me.LabelFechaActivacion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Servicio_Actual", True))
        Me.LabelFechaActivacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFechaActivacion.ForeColor = System.Drawing.Color.White
        Me.LabelFechaActivacion.Location = New System.Drawing.Point(7, 272)
        Me.LabelFechaActivacion.Name = "LabelFechaActivacion"
        Me.LabelFechaActivacion.Size = New System.Drawing.Size(240, 23)
        Me.LabelFechaActivacion.TabIndex = 22
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(10, 257)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 15)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Fecha Activación"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(10, 212)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 15)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Costo :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(10, 306)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 15)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Factura:"
        '
        'Fecha_SolLabel1
        '
        Me.Fecha_SolLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Fecha_Sol", True))
        Me.Fecha_SolLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_SolLabel1.ForeColor = System.Drawing.Color.White
        Me.Fecha_SolLabel1.Location = New System.Drawing.Point(10, 424)
        Me.Fecha_SolLabel1.Name = "Fecha_SolLabel1"
        Me.Fecha_SolLabel1.Size = New System.Drawing.Size(240, 23)
        Me.Fecha_SolLabel1.TabIndex = 13
        '
        'StatusLabel2
        '
        Me.StatusLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Status", True))
        Me.StatusLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusLabel2.ForeColor = System.Drawing.Color.White
        Me.StatusLabel2.Location = New System.Drawing.Point(3, 423)
        Me.StatusLabel2.Name = "StatusLabel2"
        Me.StatusLabel2.Size = New System.Drawing.Size(240, 23)
        Me.StatusLabel2.TabIndex = 12
        '
        'LabelFactura
        '
        Me.LabelFactura.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Servicio_Actual", True))
        Me.LabelFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFactura.ForeColor = System.Drawing.Color.White
        Me.LabelFactura.Location = New System.Drawing.Point(13, 321)
        Me.LabelFactura.Name = "LabelFactura"
        Me.LabelFactura.Size = New System.Drawing.Size(240, 23)
        Me.LabelFactura.TabIndex = 10
        '
        'LabelPrecio
        '
        Me.LabelPrecio.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Servicio_Anterior", True))
        Me.LabelPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelPrecio.ForeColor = System.Drawing.Color.White
        Me.LabelPrecio.Location = New System.Drawing.Point(10, 227)
        Me.LabelPrecio.Name = "LabelPrecio"
        Me.LabelPrecio.Size = New System.Drawing.Size(240, 23)
        Me.LabelPrecio.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(13, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(147, 20)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Datos del Cliente"
        '
        'ConceptoLabel2
        '
        Me.ConceptoLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Concepto", True))
        Me.ConceptoLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoLabel2.ForeColor = System.Drawing.Color.White
        Me.ConceptoLabel2.Location = New System.Drawing.Point(10, 180)
        Me.ConceptoLabel2.Name = "ConceptoLabel2"
        Me.ConceptoLabel2.Size = New System.Drawing.Size(240, 23)
        Me.ConceptoLabel2.TabIndex = 6
        '
        'ContratoLabel2
        '
        Me.ContratoLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Contrato", True))
        Me.ContratoLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoLabel2.ForeColor = System.Drawing.Color.White
        Me.ContratoLabel2.Location = New System.Drawing.Point(10, 88)
        Me.ContratoLabel2.Name = "ContratoLabel2"
        Me.ContratoLabel2.Size = New System.Drawing.Size(100, 23)
        Me.ContratoLabel2.TabIndex = 2
        '
        'NombreLabel2
        '
        Me.NombreLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Nombre", True))
        Me.NombreLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreLabel2.ForeColor = System.Drawing.Color.White
        Me.NombreLabel2.Location = New System.Drawing.Point(10, 127)
        Me.NombreLabel2.Name = "NombreLabel2"
        Me.NombreLabel2.Size = New System.Drawing.Size(240, 38)
        Me.NombreLabel2.TabIndex = 4
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.CMBLabel1.Location = New System.Drawing.Point(37, 6)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(168, 20)
        Me.CMBLabel1.TabIndex = 2
        Me.CMBLabel1.Text = "Buscar Cliente Por :"
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(28, 127)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(80, 27)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "Buscar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(28, 99)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(142, 22)
        Me.TextBox1.TabIndex = 3
        '
        'ConCambioServClienteDataGridView
        '
        Me.ConCambioServClienteDataGridView.AllowUserToAddRows = False
        Me.ConCambioServClienteDataGridView.AllowUserToDeleteRows = False
        Me.ConCambioServClienteDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConCambioServClienteDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.ConCambioServClienteDataGridView.ColumnHeadersHeight = 40
        Me.ConCambioServClienteDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdCol, Me.tokenCol, Me.DescripcionCol, Me.CONTRATOSencilloCol, Me.ContratoCompuestoCol, Me.NOMBRECol, Me.CostoCol, Me.FechaActivacionCol, Me.FechaDesactivacion, Me.Clv_FacturaCol, Me.facturaCol, Me.NombreUsuarioCol})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConCambioServClienteDataGridView.DefaultCellStyle = DataGridViewCellStyle4
        Me.ConCambioServClienteDataGridView.GridColor = System.Drawing.Color.WhiteSmoke
        Me.ConCambioServClienteDataGridView.Location = New System.Drawing.Point(0, 3)
        Me.ConCambioServClienteDataGridView.Name = "ConCambioServClienteDataGridView"
        Me.ConCambioServClienteDataGridView.ReadOnly = True
        Me.ConCambioServClienteDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConCambioServClienteDataGridView.Size = New System.Drawing.Size(591, 709)
        Me.ConCambioServClienteDataGridView.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(356, 163)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(78, 16)
        Me.Label9.TabIndex = 207
        Me.Label9.Text = "Compañía"
        Me.Label9.Visible = False
        '
        'ComboBoxCiudad
        '
        Me.ComboBoxCiudad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCiudad.FormattingEnabled = True
        Me.ComboBoxCiudad.Location = New System.Drawing.Point(235, 199)
        Me.ComboBoxCiudad.Name = "ComboBoxCiudad"
        Me.ComboBoxCiudad.Size = New System.Drawing.Size(236, 23)
        Me.ComboBoxCiudad.TabIndex = 206
        Me.ComboBoxCiudad.Visible = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(423, 299)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&NUEVO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(423, 341)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "&ELIMINAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(869, 673)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(136, 36)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "&SALIR"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(923, 70)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(10, 20)
        Me.ClaveTextBox.TabIndex = 2
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(939, 70)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_TipSerTextBox.TabIndex = 4
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'RealizarTextBox
        '
        Me.RealizarTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Realizar", True))
        Me.RealizarTextBox.Location = New System.Drawing.Point(955, 70)
        Me.RealizarTextBox.Name = "RealizarTextBox"
        Me.RealizarTextBox.ReadOnly = True
        Me.RealizarTextBox.Size = New System.Drawing.Size(10, 20)
        Me.RealizarTextBox.TabIndex = 6
        Me.RealizarTextBox.TabStop = False
        '
        'ConCambioServClienteTableAdapter
        '
        Me.ConCambioServClienteTableAdapter.ClearBeforeFill = True
        '
        'BorCambioServClienteBindingSource
        '
        Me.BorCambioServClienteBindingSource.DataMember = "BorCambioServCliente"
        Me.BorCambioServClienteBindingSource.DataSource = Me.DataSetEric
        '
        'BorCambioServClienteTableAdapter
        '
        Me.BorCambioServClienteTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(869, 11)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "&NUEVO"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(869, 56)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(136, 36)
        Me.Button7.TabIndex = 8
        Me.Button7.Text = "CONSULTAR"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'IdCol
        '
        Me.IdCol.DataPropertyName = "id"
        Me.IdCol.HeaderText = "ID"
        Me.IdCol.Name = "IdCol"
        Me.IdCol.ReadOnly = True
        Me.IdCol.Width = 50
        '
        'tokenCol
        '
        Me.tokenCol.DataPropertyName = "token"
        Me.tokenCol.HeaderText = "Token"
        Me.tokenCol.Name = "tokenCol"
        Me.tokenCol.ReadOnly = True
        Me.tokenCol.Visible = False
        '
        'DescripcionCol
        '
        Me.DescripcionCol.DataPropertyName = "Descripcion"
        Me.DescripcionCol.HeaderText = "Descripción"
        Me.DescripcionCol.Name = "DescripcionCol"
        Me.DescripcionCol.ReadOnly = True
        Me.DescripcionCol.Width = 130
        '
        'CONTRATOSencilloCol
        '
        Me.CONTRATOSencilloCol.DataPropertyName = "CONTRATO"
        Me.CONTRATOSencilloCol.HeaderText = "ContratoSencillo"
        Me.CONTRATOSencilloCol.Name = "CONTRATOSencilloCol"
        Me.CONTRATOSencilloCol.ReadOnly = True
        Me.CONTRATOSencilloCol.Visible = False
        Me.CONTRATOSencilloCol.Width = 80
        '
        'ContratoCompuestoCol
        '
        Me.ContratoCompuestoCol.DataPropertyName = "ContratoCompuesto"
        Me.ContratoCompuestoCol.HeaderText = "Contrato"
        Me.ContratoCompuestoCol.Name = "ContratoCompuestoCol"
        Me.ContratoCompuestoCol.ReadOnly = True
        Me.ContratoCompuestoCol.Width = 80
        '
        'NOMBRECol
        '
        Me.NOMBRECol.DataPropertyName = "NOMBRE"
        Me.NOMBRECol.HeaderText = "NOMBRE"
        Me.NOMBRECol.Name = "NOMBRECol"
        Me.NOMBRECol.ReadOnly = True
        Me.NOMBRECol.Width = 120
        '
        'CostoCol
        '
        Me.CostoCol.DataPropertyName = "Costo"
        Me.CostoCol.HeaderText = "Costo"
        Me.CostoCol.Name = "CostoCol"
        Me.CostoCol.ReadOnly = True
        Me.CostoCol.Width = 70
        '
        'FechaActivacionCol
        '
        Me.FechaActivacionCol.DataPropertyName = "FechaActivacion"
        Me.FechaActivacionCol.HeaderText = "Fecha de Activación"
        Me.FechaActivacionCol.Name = "FechaActivacionCol"
        Me.FechaActivacionCol.ReadOnly = True
        '
        'FechaDesactivacion
        '
        Me.FechaDesactivacion.DataPropertyName = "FechaDesactivacion"
        Me.FechaDesactivacion.HeaderText = "Fecha de desactivación"
        Me.FechaDesactivacion.Name = "FechaDesactivacion"
        Me.FechaDesactivacion.ReadOnly = True
        Me.FechaDesactivacion.Visible = False
        '
        'Clv_FacturaCol
        '
        Me.Clv_FacturaCol.DataPropertyName = "Clv_Factura"
        Me.Clv_FacturaCol.HeaderText = "Clv_Factura"
        Me.Clv_FacturaCol.Name = "Clv_FacturaCol"
        Me.Clv_FacturaCol.ReadOnly = True
        Me.Clv_FacturaCol.Visible = False
        '
        'facturaCol
        '
        Me.facturaCol.DataPropertyName = "factura"
        Me.facturaCol.HeaderText = "Factura"
        Me.facturaCol.Name = "facturaCol"
        Me.facturaCol.ReadOnly = True
        '
        'NombreUsuarioCol
        '
        Me.NombreUsuarioCol.DataPropertyName = "NombreUsuario"
        Me.NombreUsuarioCol.HeaderText = "Usuario"
        Me.NombreUsuarioCol.Name = "NombreUsuarioCol"
        Me.NombreUsuarioCol.ReadOnly = True
        Me.NombreUsuarioCol.Width = 150
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(9, 344)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 15)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Usuario:"
        '
        'LabelUsuario
        '
        Me.LabelUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Servicio_Actual", True))
        Me.LabelUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelUsuario.ForeColor = System.Drawing.Color.White
        Me.LabelUsuario.Location = New System.Drawing.Point(12, 360)
        Me.LabelUsuario.Name = "LabelUsuario"
        Me.LabelUsuario.Size = New System.Drawing.Size(240, 23)
        Me.LabelUsuario.TabIndex = 25
        '
        'BrwToken
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.RealizarTextBox)
        Me.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "BrwToken"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Token"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConCambioServClienteDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConCambioServClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConCambioServClienteTableAdapter As sofTV.DataSetEricTableAdapters.ConCambioServClienteTableAdapter
    Friend WithEvents ConCambioServClienteDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RealizarTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusLabel2 As System.Windows.Forms.Label
    Friend WithEvents LabelFactura As System.Windows.Forms.Label
    Friend WithEvents ConceptoLabel2 As System.Windows.Forms.Label
    Friend WithEvents ContratoLabel2 As System.Windows.Forms.Label
    Friend WithEvents NombreLabel2 As System.Windows.Forms.Label
    Friend WithEvents Fecha_SolLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents BorCambioServClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorCambioServClienteTableAdapter As sofTV.DataSetEricTableAdapters.BorCambioServClienteTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents LabelPrecio As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ContratoLabel As System.Windows.Forms.Label
    Friend WithEvents NombreLabel As System.Windows.Forms.Label
    Friend WithEvents ConceptoLabel As System.Windows.Forms.Label
    Friend WithEvents LabelFechaActivacion As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents LabelIdToken As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents LabelUsuario As System.Windows.Forms.Label
    Friend WithEvents IdCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tokenCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CONTRATOSencilloCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContratoCompuestoCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBRECol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaActivacionCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDesactivacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_FacturaCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents facturaCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreUsuarioCol As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
