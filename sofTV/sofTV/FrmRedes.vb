﻿Public Class FrmRedes

    Dim idRed As Long = 0
    Dim op As Integer = 0

    Private Sub BrwRedes_Nuevo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Text = "Red"
        TabPage1.Text = "Plazas"
        TabPage2.Text = "Medios"


        BuscaRedBtn.Enabled = False
        numA.Enabled = False
        numB.Enabled = False
        numC.Enabled = False
        numD.Enabled = False
        mask.Enabled = False

        agregar.Enabled = True
        agregartodo.Enabled = True
        quitar.Enabled = True
        quitartodo.Enabled = True

        agregarM.Enabled = True
        agregarTodoM.Enabled = True
        quitarM.Enabled = True
        quitarTodoM.Enabled = True

        Me.Text = "Red - " + iD_redSelec.ToString()

        If OpcAccion = "M" Then
            op = 2
            consulta_Red()

        ElseIf OpcAccion = "C" Then
            op = 0
            consulta_Red()

            Button2.Visible = False
            agregar.Enabled = False
            agregartodo.Enabled = False
            quitar.Enabled = False
            quitartodo.Enabled = False

            agregarM.Enabled = False
            agregarTodoM.Enabled = False
            quitarM.Enabled = False
            quitarTodoM.Enabled = False

        ElseIf OpcAccion = "N" Then
            op = 1
            Me.Text = "Nueva Red"
            iD_redSelec = 0
            BuscaRedBtn.Enabled = True
            'a     Button3.Enabled = False
            Button2.Visible = False
            '   Panel1.Visible = False
            TabControl1.TabPages.Remove(TabPage1)
            TabControl1.TabPages.Remove(TabPage2)

            numA.Enabled = True
            numB.Enabled = True
            numD.Enabled = True
            mask.Enabled = True
            numC.Enabled = True
            numA.Value = 0
            numB.Value = 0
            numC.Value = 0
            numD.Value = 0
            mask.Value = 1
        End If

    End Sub

    Public Sub CargarElementosPlaza_RedXML_2()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.CreateMyParameter("@IdRed", SqlDbType.Int, iD_redSelec)
        BaseII.CreateMyParameter("@PlazasXML", ParameterDirection.Output, SqlDbType.Xml, 1)
        BaseII.ProcedimientoOutPut("ProcedureSeleccionPlaza_RedXML")
        Dim resultado = BaseII.dicoPar("@PlazasXML")
        DocPlazas.LoadXml(resultado)

        llenalistboxs_plaza()

    End Sub

    Public Sub CargarElementosMedios_RedXML()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.CreateMyParameter("@IdRed", SqlDbType.Int, iD_redSelec)
        BaseII.CreateMyParameter("@MediosXML", ParameterDirection.Output, SqlDbType.Xml, 1)
        BaseII.ProcedimientoOutPut("ProcedureSeleccionMedio_RedXML")
        Dim resultado = BaseII.dicoPar("@MediosXML")

        DocMedios.LoadXml(resultado)

        llenalistboxs()

    End Sub


    Private Sub consulta_Red()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdRed", SqlDbType.BigInt, iD_redSelec)
        Dim DTResults As DataTable = BaseII.ConsultaDT("ConsultaRed")
        Dim octeto1, octeto2, octeto3, octeto4, mascara As String

        numA.Value = DTResults.Rows(0)(0).ToString() 'octeto 1
        numB.Value = DTResults.Rows(0)(1).ToString()  'octeto 2
        numC.Value = DTResults.Rows(0)(2).ToString()  'octeto 3
        numD.Value = DTResults.Rows(0)(3).ToString()  'octeto 4
        mask.Value = DTResults.Rows(0)(4).ToString()  'mask

        CargarElementosPlaza_RedXML_2()
        CargarElementosMedios_RedXML()

    End Sub


    Private Sub BuscaRedBtn_Click(sender As Object, e As EventArgs) Handles BuscaRedBtn.Click

        If numA.Text = "" Or numB.Text = "" Or numD.Text = "" Or numC.Text = "" Then
            MessageBox.Show("IP inválida")
        ElseIf numA.Text = 0 And numB.Text = 0 And numD.Text = 0 And numC.Text = 0 Then
            MessageBox.Show("IP inválida")
        Else
            numA.Enabled = True
            numB.Enabled = True
            numD.Enabled = True
            mask.Enabled = True
            numC.Enabled = True

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@a", SqlDbType.BigInt, numA.Value)
            BaseII.CreateMyParameter("@b", SqlDbType.BigInt, numB.Value)
            BaseII.CreateMyParameter("@c", SqlDbType.BigInt, numC.Value)
            BaseII.CreateMyParameter("@d", SqlDbType.BigInt, numD.Value)
            BaseII.CreateMyParameter("@mask", SqlDbType.Int, mask.Value)
            BaseII.CreateMyParameter("@status", SqlDbType.VarChar, "D")
            Dim DTResults As DataTable = BaseII.ConsultaDT("Add_CatalogoIps_S")
            Dim yaExistia = DTResults.Rows(0)("yaExistia")
            '   idRed = DTResults.Rows(0)("IdRed")
            iD_redSelec = DTResults.Rows(0)("IdRed")


            If yaExistia = 0 Then
                MessageBox.Show("Se guardó Red")
                '  Panel1.Visible = True
                TabControl1.TabPages.Insert(0, TabPage1)
                TabControl1.TabPages.Insert(1, TabPage2)
                TabControl1.SelectedTab = TabPage1

                Button2.Visible = True
                Button3.Enabled = True
                BuscaRedBtn.Enabled = False
            Else
                MessageBox.Show("Red guardada anteriormente")
            End If

            BrwRedes.llenaGrid()

            'llenaListBox1()
        End If

        CargarElementosPlaza_RedXML_2()
        CargarElementosMedios_RedXML()

    End Sub

 

    Private Sub numA_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles numA.KeyPress
        soloNumeros(e)
    End Sub

    Private Sub numB_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles numB.KeyPress
        soloNumeros(e)
    End Sub
    Private Sub mask_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles numC.KeyPress
        soloNumeros(e)
    End Sub
    Private Sub numC_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles numD.KeyPress
        soloNumeros(e)
    End Sub
    Private Sub numD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mask.KeyPress
        soloNumeros(e)
    End Sub


    Private Sub soloNumeros(e)
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
            MessageBox.Show("Solo números")
            e.Handled = True
        End If
    End Sub


    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehayPlaza.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Plaza") = loquehayPlaza.SelectedValue Then
                elem.SetAttribute("Seleccion", 1)
            End If
        Next
        llenalistboxs_plaza()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccionPlaza.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Plaza") = seleccionPlaza.SelectedValue Then
                elem.SetAttribute("Seleccion", 0)
            End If
        Next
        llenalistboxs_plaza()

    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehayPlaza.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        llenalistboxs_plaza()


    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccionPlaza.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos

            elem.SetAttribute("Seleccion", 0)

        Next
        llenalistboxs_plaza()

    End Sub


    Private Sub llenalistboxs_plaza()
        Dim dt As New DataTable
        dt.Columns.Add("Clv_Plaza", Type.GetType("System.String"))
        dt.Columns.Add("nombre", Type.GetType("System.String"))

        Dim dt2 As New DataTable
        dt2.Columns.Add("Clv_Plaza", Type.GetType("System.String"))
        dt2.Columns.Add("nombre", Type.GetType("System.String"))

        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Seleccion") = 0 Then
                dt.Rows.Add(elem.GetAttribute("Clv_Plaza"), elem.GetAttribute("nombre"))
            End If
            If elem.GetAttribute("Seleccion") = 1 Then
                dt2.Rows.Add(elem.GetAttribute("Clv_Plaza"), elem.GetAttribute("nombre"))
            End If
        Next
        loquehayPlaza.DataSource = New BindingSource(dt, Nothing)
        seleccionPlaza.DataSource = New BindingSource(dt2, Nothing)
    End Sub


    Private Sub llenalistboxs()   'medio
        Dim dt As New DataTable
        dt.Columns.Add("Clv_Medio", Type.GetType("System.String"))
        dt.Columns.Add("Nombre", Type.GetType("System.String"))

        Dim dt2 As New DataTable
        dt2.Columns.Add("Clv_Medio", Type.GetType("System.String"))
        dt2.Columns.Add("Nombre", Type.GetType("System.String"))

        Dim elementos = DocMedios.SelectNodes("//MEDIO")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Seleccion") = 0 Then
                dt.Rows.Add(elem.GetAttribute("Clv_Medio"), elem.GetAttribute("Nombre"))
            End If
            If elem.GetAttribute("Seleccion") = 1 Then
                dt2.Rows.Add(elem.GetAttribute("Clv_Medio"), elem.GetAttribute("Nombre"))
            End If
        Next
        loquehay.DataSource = New BindingSource(dt, Nothing)
        seleccion.DataSource = New BindingSource(dt2, Nothing)
    End Sub


  



    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        origenForm = "IpRed"
        BrwIPs.Show()
    End Sub


    Private Sub agregarTodoM_Click(sender As Object, e As EventArgs) Handles agregarTodoM.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocMedios.SelectNodes("//MEDIO")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        llenalistboxs()
    End Sub


    Private Sub quitarM_Click(sender As Object, e As EventArgs) Handles quitarM.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocMedios.SelectNodes("//MEDIO")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Medio") = seleccion.SelectedValue Then
                elem.SetAttribute("Seleccion", 0)
            End If
        Next
        llenalistboxs()
    End Sub

    Private Sub quitarTodoM_Click(sender As Object, e As EventArgs) Handles quitarTodoM.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocMedios.SelectNodes("//MEDIO")
        For Each elem As Xml.XmlElement In elementos

            elem.SetAttribute("Seleccion", 0)

        Next
        llenalistboxs()
    End Sub


    Private Sub agregarM_Click(sender As Object, e As EventArgs) Handles agregarM.Click

        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocMedios.SelectNodes("//MEDIO")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Medio") = loquehay.SelectedValue Then
                elem.SetAttribute("Seleccion", 1)
            End If
        Next
        llenalistboxs()

    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
     
        If seleccionPlaza.Items.Count = 0 And seleccion.Items.Count = 0 Then
            MsgBox("Seleccione alguna Plaza y Medio")
        ElseIf seleccionPlaza.Items.Count = 0 And seleccion.Items.Count = 1 Then
            MsgBox("Seleccione alguna Plaza")
        ElseIf seleccionPlaza.Items.Count = 1 And seleccion.Items.Count = 0 Then
            MsgBox("Seleccione algún Medio")
        End If

        If seleccionPlaza.Items.Count > 0 And seleccion.Items.Count > 0 Then
            Dim companiaGuardada = Guardar_RedCompania()
            Dim medioGuardado = Guardar_RedMedio()

            If companiaGuardada = 1 And medioGuardado = 1 Then
                MsgBox("Plazas y Medios guardados")
            ElseIf companiaGuardada = 0 And medioGuardado = 0 Then
                MsgBox("No se ha guardado")
            ElseIf companiaGuardada = 0 And medioGuardado = 1 Then
                MsgBox("Plazas guardadas")
            ElseIf companiaGuardada = 1 And medioGuardado = 0 Then
                MsgBox("Medios guardados")
            End If

        End If

    End Sub



    Private Function Guardar_RedCompania()

        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@xmlPlaza", SqlDbType.Xml, DocPlazas.InnerXml) 'Companias
        BaseII.CreateMyParameter("@IdRed", SqlDbType.BigInt, iD_redSelec) 'idRed
        Dim DTResults As DataTable = BaseII.ConsultaDT("Guarda_RedCompania")
        Dim hecho = DTResults.Rows(0)("hecho")

        Return hecho

    End Function


    Private Function Guardar_RedMedio()

        Dim elementos = DocMedios.SelectNodes("//MEDIO")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@xmlMedio", SqlDbType.Xml, DocMedios.InnerXml) 'Companias
        BaseII.CreateMyParameter("@IdRed", SqlDbType.BigInt, iD_redSelec) 'idRed
        Dim DTResults As DataTable = BaseII.ConsultaDT("Guarda_RedMedio")
        Dim hecho = DTResults.Rows(0)("hecho")

        Return hecho

    End Function


End Class