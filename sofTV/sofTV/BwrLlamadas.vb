Imports System.Data.SqlClient
Public Class BwrLlamadas

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If IsNumeric(Me.DataGridView1.SelectedCells(0).Value.ToString) = True Then
                GLOCLV_oRDENbus = Me.DataGridView1.SelectedCells(0).Value.ToString
                Me.Close()
            Else
                MsgBox("Seleccione la Llamada ", MsgBoxStyle.Information)
            End If
        Catch
            MsgBox("No hay llamadas que seleccionar", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim sTATUS As String = "P"
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try

            If IsNumeric(GloClv_TipSer) = True Then
                If op = 0 Then 'clv_llamada
                    If IsNumeric(Me.TextBox3.Text) = True Then
                        'Me.BusquedaLlamadasDynamicTableAdapter.Connection = CON
                        'Me.BusquedaLlamadasDynamicTableAdapter.Fill(Me.DataSetarnoldo.BusquedaLlamadasDynamic, GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, CInt(Me.TextBox3.Text), 0, "", 0)
                        BusquedaLlamadasDynamic(GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, CInt(Me.TextBox3.Text), 0, "", 0, GloIdCompania)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 1 Then
                    If Len(Trim(Me.TextBox1.Text)) > 0 Then
                        'Me.BusquedaLlamadasDynamicTableAdapter.Connection = CON
                        'Me.BusquedaLlamadasDynamicTableAdapter.Fill(Me.DataSetarnoldo.BusquedaLlamadasDynamic, GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, CInt(Me.TextBox1.Text), "", 1)
                        BusquedaLlamadasDynamic(GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, CInt(Me.TextBox1.Text), "", 1, GloIdCompania)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 2 Then 'Nombre
                    If Len(Trim(Me.TextBox2.Text)) > 0 Then
                        'Me.BusquedaLlamadasDynamicTableAdapter.Connection = CON
                        'Me.BusquedaLlamadasDynamicTableAdapter.Fill(Me.DataSetarnoldo.BusquedaLlamadasDynamic, GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox2.Text, 2)
                        BusquedaLlamadasDynamic(GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox2.Text, 2, GloIdCompania)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 4 Then 'Respectivos a lo que corresponde
                    'Me.BusquedaLlamadasDynamicTableAdapter.Connection = CON
                    'Me.BusquedaLlamadasDynamicTableAdapter.Fill(Me.DataSetarnoldo.BusquedaLlamadasDynamic, GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, 0, 0, 4)
                    BusquedaLlamadasDynamic(GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, 0, 0, 4, GloIdCompania)
                End If
                Me.TextBox1.Clear()
                Me.TextBox2.Clear()
                Me.TextBox3.Clear()
            Else
                MsgBox("Seleccione el Tipo de Servicio")
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub BusquedaLlamadasDynamic(ByVal Clv_tipser As Integer, ByVal SinQueja As Integer, ByVal ConQueja As Integer, ByVal Ambas As Integer, ByVal Clv_Llamada As Integer,
                                      ByVal Contrato As Integer, ByVal Nombre As String, ByVal Op As Integer, ByVal Compa�ia As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_tipser", SqlDbType.Int, Clv_tipser)
        BaseII.CreateMyParameter("@SinQueja", SqlDbType.Bit, SinQueja)
        BaseII.CreateMyParameter("@ConQueja", SqlDbType.Bit, ConQueja)
        BaseII.CreateMyParameter("@Ambas", SqlDbType.Bit, Ambas)
        BaseII.CreateMyParameter("@Clv_Llamada", SqlDbType.BigInt, Clv_Llamada)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Nombre, 150)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Compania", SqlDbType.Int, Compa�ia)
        DataGridView1.DataSource = BaseII.ConsultaDT("BusquedaLlamadasDynamic")

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(1)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(2)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub FrmBuscaQuejas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busca(4)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(0)
    End Sub

    Private Sub TextBox1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyUp

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub Clv_calleLabel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value.ToString) = True Then
            GLOCLV_oRDENbus = Me.DataGridView1.SelectedCells(0).Value.ToString
            Me.Close()
        Else
            MsgBox("Seleccione la Llamada", MsgBoxStyle.Information)
        End If

    End Sub

    Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub Button9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Button9.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub Button7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Button7.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub BusquedaLlamadasDynamicDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class