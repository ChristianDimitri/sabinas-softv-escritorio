<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmServiciosPPE
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim HitsLabel As System.Windows.Forms.Label
        Dim ClasificacionLabel As System.Windows.Forms.Label
        Dim PrecioLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblClasificacion = New System.Windows.Forms.Label()
        Me.gvMuestraProgramacion = New System.Windows.Forms.DataGridView()
        Me.lblHits = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.lblPrecio = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblClv = New System.Windows.Forms.Label()
        Me.dtmFecha = New System.Windows.Forms.DateTimePicker()
        Me.btnBusca = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.gbDetalleServicio = New System.Windows.Forms.GroupBox()
        Me.gbBuscaServicio = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.gbBuscaContratos = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblPuntos = New System.Windows.Forms.Label()
        Me.btnBuscaContrato = New System.Windows.Forms.Button()
        Me.LblStatus = New System.Windows.Forms.Label()
        Me.lblPuntosNecesarios = New System.Windows.Forms.Label()
        Me.txtBuscaContrato = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.gvMuestraServicios = New System.Windows.Forms.DataGridView()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.gvPelis = New System.Windows.Forms.DataGridView()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Txt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_PPE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Hits = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clasificacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Progra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Txt1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnQuitar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnLimpiar = New System.Windows.Forms.Button()
        HitsLabel = New System.Windows.Forms.Label()
        ClasificacionLabel = New System.Windows.Forms.Label()
        PrecioLabel = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        Clv_TxtLabel = New System.Windows.Forms.Label()
        CType(Me.gvMuestraProgramacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.gbDetalleServicio.SuspendLayout()
        Me.gbBuscaServicio.SuspendLayout()
        Me.gbBuscaContratos.SuspendLayout()
        CType(Me.gvMuestraServicios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPelis, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'HitsLabel
        '
        HitsLabel.AutoSize = True
        HitsLabel.BackColor = System.Drawing.Color.WhiteSmoke
        HitsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        HitsLabel.ForeColor = System.Drawing.Color.Black
        HitsLabel.Location = New System.Drawing.Point(11, 201)
        HitsLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        HitsLabel.Name = "HitsLabel"
        HitsLabel.Size = New System.Drawing.Size(50, 20)
        HitsLabel.TabIndex = 21
        HitsLabel.Text = "Hits:"
        '
        'ClasificacionLabel
        '
        ClasificacionLabel.AutoSize = True
        ClasificacionLabel.BackColor = System.Drawing.Color.WhiteSmoke
        ClasificacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClasificacionLabel.ForeColor = System.Drawing.Color.Black
        ClasificacionLabel.Location = New System.Drawing.Point(11, 245)
        ClasificacionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ClasificacionLabel.Name = "ClasificacionLabel"
        ClasificacionLabel.Size = New System.Drawing.Size(130, 20)
        ClasificacionLabel.TabIndex = 22
        ClasificacionLabel.Text = "Clasificación :"
        '
        'PrecioLabel
        '
        PrecioLabel.AutoSize = True
        PrecioLabel.BackColor = System.Drawing.Color.WhiteSmoke
        PrecioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PrecioLabel.ForeColor = System.Drawing.Color.Black
        PrecioLabel.Location = New System.Drawing.Point(11, 150)
        PrecioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        PrecioLabel.Name = "PrecioLabel"
        PrecioLabel.Size = New System.Drawing.Size(69, 20)
        PrecioLabel.TabIndex = 20
        PrecioLabel.Text = "Precio:"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.BackColor = System.Drawing.Color.WhiteSmoke
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.Black
        DescripcionLabel.Location = New System.Drawing.Point(11, 81)
        DescripcionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(122, 20)
        DescripcionLabel.TabIndex = 19
        DescripcionLabel.Text = "Descripción :"
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.BackColor = System.Drawing.Color.WhiteSmoke
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.ForeColor = System.Drawing.Color.Black
        Clv_TxtLabel.Location = New System.Drawing.Point(11, 31)
        Clv_TxtLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(68, 20)
        Clv_TxtLabel.TabIndex = 18
        Clv_TxtLabel.Text = "Clave :"
        '
        'lblClasificacion
        '
        Me.lblClasificacion.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblClasificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClasificacion.Location = New System.Drawing.Point(15, 265)
        Me.lblClasificacion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblClasificacion.Name = "lblClasificacion"
        Me.lblClasificacion.Size = New System.Drawing.Size(301, 18)
        Me.lblClasificacion.TabIndex = 23
        '
        'gvMuestraProgramacion
        '
        Me.gvMuestraProgramacion.AllowUserToAddRows = False
        Me.gvMuestraProgramacion.AllowUserToDeleteRows = False
        Me.gvMuestraProgramacion.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvMuestraProgramacion.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gvMuestraProgramacion.ColumnHeadersHeight = 25
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvMuestraProgramacion.DefaultCellStyle = DataGridViewCellStyle2
        Me.gvMuestraProgramacion.Location = New System.Drawing.Point(373, 522)
        Me.gvMuestraProgramacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gvMuestraProgramacion.MultiSelect = False
        Me.gvMuestraProgramacion.Name = "gvMuestraProgramacion"
        Me.gvMuestraProgramacion.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvMuestraProgramacion.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.gvMuestraProgramacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvMuestraProgramacion.Size = New System.Drawing.Size(572, 375)
        Me.gvMuestraProgramacion.TabIndex = 22
        Me.gvMuestraProgramacion.TabStop = False
        '
        'lblHits
        '
        Me.lblHits.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblHits.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHits.Location = New System.Drawing.Point(15, 220)
        Me.lblHits.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblHits.Name = "lblHits"
        Me.lblHits.Size = New System.Drawing.Size(301, 18)
        Me.lblHits.TabIndex = 22
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(1081, 742)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(181, 44)
        Me.btnAceptar.TabIndex = 26
        Me.btnAceptar.Text = "&ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'lblPrecio
        '
        Me.lblPrecio.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecio.Location = New System.Drawing.Point(15, 170)
        Me.lblPrecio.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Size = New System.Drawing.Size(301, 18)
        Me.lblPrecio.TabIndex = 21
        '
        'lblDescripcion
        '
        Me.lblDescripcion.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescripcion.Location = New System.Drawing.Point(15, 101)
        Me.lblDescripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(301, 41)
        Me.lblDescripcion.TabIndex = 20
        '
        'lblClv
        '
        Me.lblClv.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblClv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClv.Location = New System.Drawing.Point(15, 50)
        Me.lblClv.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblClv.Name = "lblClv"
        Me.lblClv.Size = New System.Drawing.Size(301, 18)
        Me.lblClv.TabIndex = 19
        '
        'dtmFecha
        '
        Me.dtmFecha.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtmFecha.CustomFormat = "dd-mm-aaaa"
        Me.dtmFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtmFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtmFecha.Location = New System.Drawing.Point(8, 113)
        Me.dtmFecha.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dtmFecha.Name = "dtmFecha"
        Me.dtmFecha.Size = New System.Drawing.Size(304, 26)
        Me.dtmFecha.TabIndex = 26
        Me.dtmFecha.Value = New Date(2008, 1, 18, 0, 0, 0, 0)
        '
        'btnBusca
        '
        Me.btnBusca.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBusca.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBusca.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBusca.Location = New System.Drawing.Point(87, 164)
        Me.btnBusca.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnBusca.Name = "btnBusca"
        Me.btnBusca.Size = New System.Drawing.Size(117, 28)
        Me.btnBusca.TabIndex = 7
        Me.btnBusca.Text = "Buscar"
        Me.btnBusca.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 89)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 20)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Fecha :"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(1081, 846)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(181, 44)
        Me.btnSalir.TabIndex = 27
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.gbDetalleServicio)
        Me.Panel1.Controls.Add(Me.gbBuscaServicio)
        Me.Panel1.Controls.Add(Me.gbBuscaContratos)
        Me.Panel1.Location = New System.Drawing.Point(4, 2)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(361, 898)
        Me.Panel1.TabIndex = 25
        Me.Panel1.TabStop = True
        '
        'gbDetalleServicio
        '
        Me.gbDetalleServicio.Controls.Add(ClasificacionLabel)
        Me.gbDetalleServicio.Controls.Add(Clv_TxtLabel)
        Me.gbDetalleServicio.Controls.Add(Me.lblClasificacion)
        Me.gbDetalleServicio.Controls.Add(Me.lblClv)
        Me.gbDetalleServicio.Controls.Add(DescripcionLabel)
        Me.gbDetalleServicio.Controls.Add(PrecioLabel)
        Me.gbDetalleServicio.Controls.Add(Me.lblDescripcion)
        Me.gbDetalleServicio.Controls.Add(HitsLabel)
        Me.gbDetalleServicio.Controls.Add(Me.lblHits)
        Me.gbDetalleServicio.Controls.Add(Me.lblPrecio)
        Me.gbDetalleServicio.Location = New System.Drawing.Point(12, 572)
        Me.gbDetalleServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbDetalleServicio.Name = "gbDetalleServicio"
        Me.gbDetalleServicio.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbDetalleServicio.Size = New System.Drawing.Size(325, 322)
        Me.gbDetalleServicio.TabIndex = 29
        Me.gbDetalleServicio.TabStop = False
        Me.gbDetalleServicio.Text = "Detalle del Servicio"
        '
        'gbBuscaServicio
        '
        Me.gbBuscaServicio.Controls.Add(Me.dtmFecha)
        Me.gbBuscaServicio.Controls.Add(Me.Label3)
        Me.gbBuscaServicio.Controls.Add(Me.btnBusca)
        Me.gbBuscaServicio.Controls.Add(Me.txtDescripcion)
        Me.gbBuscaServicio.Controls.Add(Me.Label1)
        Me.gbBuscaServicio.Location = New System.Drawing.Point(12, 357)
        Me.gbBuscaServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbBuscaServicio.Name = "gbBuscaServicio"
        Me.gbBuscaServicio.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbBuscaServicio.Size = New System.Drawing.Size(325, 208)
        Me.gbBuscaServicio.TabIndex = 28
        Me.gbBuscaServicio.TabStop = False
        Me.gbBuscaServicio.Text = "Buscar Servicio PPE:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 20)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 20)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Descripción :"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.Location = New System.Drawing.Point(5, 43)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(303, 26)
        Me.txtDescripcion.TabIndex = 2
        '
        'gbBuscaContratos
        '
        Me.gbBuscaContratos.Controls.Add(Me.Label4)
        Me.gbBuscaContratos.Controls.Add(Me.Label2)
        Me.gbBuscaContratos.Controls.Add(Me.lblPuntos)
        Me.gbBuscaContratos.Controls.Add(Me.btnBuscaContrato)
        Me.gbBuscaContratos.Controls.Add(Me.LblStatus)
        Me.gbBuscaContratos.Controls.Add(Me.lblPuntosNecesarios)
        Me.gbBuscaContratos.Controls.Add(Me.txtBuscaContrato)
        Me.gbBuscaContratos.Controls.Add(Me.Label6)
        Me.gbBuscaContratos.Location = New System.Drawing.Point(12, 14)
        Me.gbBuscaContratos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbBuscaContratos.Name = "gbBuscaContratos"
        Me.gbBuscaContratos.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbBuscaContratos.Size = New System.Drawing.Size(325, 336)
        Me.gbBuscaContratos.TabIndex = 27
        Me.gbBuscaContratos.TabStop = False
        Me.gbBuscaContratos.Text = "Buscar Contratos"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(11, 281)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(169, 20)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Puntos del Cliente:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(11, 159)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(176, 20)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Nombre del Cliente:"
        '
        'lblPuntos
        '
        Me.lblPuntos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPuntos.AutoEllipsis = True
        Me.lblPuntos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntos.Location = New System.Drawing.Point(11, 300)
        Me.lblPuntos.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPuntos.Name = "lblPuntos"
        Me.lblPuntos.Size = New System.Drawing.Size(309, 32)
        Me.lblPuntos.TabIndex = 29
        '
        'btnBuscaContrato
        '
        Me.btnBuscaContrato.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscaContrato.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscaContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscaContrato.Location = New System.Drawing.Point(184, 58)
        Me.btnBuscaContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnBuscaContrato.Name = "btnBuscaContrato"
        Me.btnBuscaContrato.Size = New System.Drawing.Size(41, 28)
        Me.btnBuscaContrato.TabIndex = 28
        Me.btnBuscaContrato.Text = "..."
        Me.btnBuscaContrato.UseVisualStyleBackColor = False
        '
        'LblStatus
        '
        Me.LblStatus.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblStatus.AutoEllipsis = True
        Me.LblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblStatus.Location = New System.Drawing.Point(9, 178)
        Me.LblStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LblStatus.Name = "LblStatus"
        Me.LblStatus.Size = New System.Drawing.Size(307, 91)
        Me.LblStatus.TabIndex = 20
        '
        'lblPuntosNecesarios
        '
        Me.lblPuntosNecesarios.AutoSize = True
        Me.lblPuntosNecesarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntosNecesarios.Location = New System.Drawing.Point(11, 116)
        Me.lblPuntosNecesarios.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPuntosNecesarios.Name = "lblPuntosNecesarios"
        Me.lblPuntosNecesarios.Size = New System.Drawing.Size(180, 20)
        Me.lblPuntosNecesarios.TabIndex = 19
        Me.lblPuntosNecesarios.Text = "Puntos Necesarios: "
        '
        'txtBuscaContrato
        '
        Me.txtBuscaContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscaContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscaContrato.Location = New System.Drawing.Point(8, 59)
        Me.txtBuscaContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtBuscaContrato.Name = "txtBuscaContrato"
        Me.txtBuscaContrato.Size = New System.Drawing.Size(167, 26)
        Me.txtBuscaContrato.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(15, 36)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 20)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "Contrato: "
        '
        'gvMuestraServicios
        '
        Me.gvMuestraServicios.AllowUserToAddRows = False
        Me.gvMuestraServicios.AllowUserToDeleteRows = False
        Me.gvMuestraServicios.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvMuestraServicios.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvMuestraServicios.DefaultCellStyle = DataGridViewCellStyle5
        Me.gvMuestraServicios.Location = New System.Drawing.Point(377, 28)
        Me.gvMuestraServicios.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gvMuestraServicios.MultiSelect = False
        Me.gvMuestraServicios.Name = "gvMuestraServicios"
        Me.gvMuestraServicios.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvMuestraServicios.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.gvMuestraServicios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvMuestraServicios.Size = New System.Drawing.Size(572, 464)
        Me.gvMuestraServicios.TabIndex = 29
        Me.gvMuestraServicios.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(373, 2)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(216, 24)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Servicios Disponibles:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(373, 496)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(210, 24)
        Me.Label7.TabIndex = 31
        Me.Label7.Text = "Horarios Disponibles:"
        '
        'gvPelis
        '
        Me.gvPelis.AllowUserToAddRows = False
        Me.gvPelis.AllowUserToDeleteRows = False
        Me.gvPelis.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.gvPelis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvPelis.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Servicio, Me.Clv_Txt, Me.Descripcion, Me.Precio, Me.Clv_PPE, Me.Hits, Me.Clasificacion, Me.Clv_Progra, Me.Clv_Txt1, Me.Fecha})
        Me.gvPelis.Location = New System.Drawing.Point(5, 26)
        Me.gvPelis.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gvPelis.MultiSelect = False
        Me.gvPelis.Name = "gvPelis"
        Me.gvPelis.ReadOnly = True
        Me.gvPelis.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvPelis.Size = New System.Drawing.Size(380, 384)
        Me.gvPelis.TabIndex = 32
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.ReadOnly = True
        Me.Clv_Servicio.Visible = False
        '
        'Clv_Txt
        '
        Me.Clv_Txt.HeaderText = "Clv_Txt"
        Me.Clv_Txt.Name = "Clv_Txt"
        Me.Clv_Txt.ReadOnly = True
        Me.Clv_Txt.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'Precio
        '
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Name = "Precio"
        Me.Precio.ReadOnly = True
        Me.Precio.Visible = False
        '
        'Clv_PPE
        '
        Me.Clv_PPE.HeaderText = "Clv_PPE"
        Me.Clv_PPE.Name = "Clv_PPE"
        Me.Clv_PPE.ReadOnly = True
        Me.Clv_PPE.Visible = False
        '
        'Hits
        '
        Me.Hits.HeaderText = "Hits"
        Me.Hits.Name = "Hits"
        Me.Hits.ReadOnly = True
        Me.Hits.Visible = False
        '
        'Clasificacion
        '
        Me.Clasificacion.HeaderText = "Clasificacion"
        Me.Clasificacion.Name = "Clasificacion"
        Me.Clasificacion.ReadOnly = True
        Me.Clasificacion.Visible = False
        '
        'Clv_Progra
        '
        Me.Clv_Progra.HeaderText = "Clv_Progra"
        Me.Clv_Progra.Name = "Clv_Progra"
        Me.Clv_Progra.ReadOnly = True
        Me.Clv_Progra.Visible = False
        '
        'Clv_Txt1
        '
        Me.Clv_Txt1.HeaderText = "Clv_Txt1"
        Me.Clv_Txt1.Name = "Clv_Txt1"
        Me.Clv_Txt1.ReadOnly = True
        Me.Clv_Txt1.Visible = False
        '
        'Fecha
        '
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnQuitar)
        Me.GroupBox1.Controls.Add(Me.gvPelis)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(957, 28)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(393, 464)
        Me.GroupBox1.TabIndex = 34
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Peliculas Agregadas"
        '
        'btnQuitar
        '
        Me.btnQuitar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnQuitar.Enabled = False
        Me.btnQuitar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnQuitar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuitar.Location = New System.Drawing.Point(141, 417)
        Me.btnQuitar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(117, 39)
        Me.btnQuitar.TabIndex = 35
        Me.btnQuitar.Text = "Quitar"
        Me.btnQuitar.UseVisualStyleBackColor = False
        Me.btnQuitar.Visible = False
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.Enabled = False
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Location = New System.Drawing.Point(963, 558)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(117, 39)
        Me.btnAgregar.TabIndex = 27
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = False
        Me.btnAgregar.Visible = False
        '
        'btnLimpiar
        '
        Me.btnLimpiar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLimpiar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLimpiar.Location = New System.Drawing.Point(1081, 794)
        Me.btnLimpiar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(181, 44)
        Me.btnLimpiar.TabIndex = 35
        Me.btnLimpiar.Text = "&LIMPIAR"
        Me.btnLimpiar.UseVisualStyleBackColor = False
        Me.btnLimpiar.Visible = False
        '
        'FrmServiciosPPE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1355, 903)
        Me.Controls.Add(Me.btnLimpiar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.gvMuestraServicios)
        Me.Controls.Add(Me.gvMuestraProgramacion)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmServiciosPPE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Servicios Pago Por Evento"
        CType(Me.gvMuestraProgramacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.gbDetalleServicio.ResumeLayout(False)
        Me.gbDetalleServicio.PerformLayout()
        Me.gbBuscaServicio.ResumeLayout(False)
        Me.gbBuscaServicio.PerformLayout()
        Me.gbBuscaContratos.ResumeLayout(False)
        Me.gbBuscaContratos.PerformLayout()
        CType(Me.gvMuestraServicios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPelis, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblClasificacion As System.Windows.Forms.Label
    Friend WithEvents gvMuestraProgramacion As System.Windows.Forms.DataGridView
    Friend WithEvents lblHits As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents lblPrecio As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblClv As System.Windows.Forms.Label
    Friend WithEvents dtmFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnBusca As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents gvMuestraServicios As System.Windows.Forms.DataGridView
    Friend WithEvents gbBuscaContratos As System.Windows.Forms.GroupBox
    Friend WithEvents txtBuscaContrato As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblPuntosNecesarios As System.Windows.Forms.Label
    Friend WithEvents btnBuscaContrato As System.Windows.Forms.Button
    Friend WithEvents LblStatus As System.Windows.Forms.Label
    Friend WithEvents gbBuscaServicio As System.Windows.Forms.GroupBox
    Friend WithEvents gbDetalleServicio As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents gvPelis As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblPuntos As System.Windows.Forms.Label
    Friend WithEvents btnQuitar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Txt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_PPE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Hits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clasificacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Progra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Txt1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnLimpiar As System.Windows.Forms.Button
End Class
