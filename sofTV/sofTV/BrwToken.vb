﻿Imports System.Data.SqlClient
Public Class BrwToken

    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim contrato As Long = Nothing
    Private busca As Integer = 0

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                GloIdCompania = ComboBoxCompanias.SelectedValue
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_Ciudad()
        Try
            BaseII.limpiaParametros()
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("Muestra_ciudad")
            ComboBoxCiudad.DisplayMember = "Nombre"
            ComboBoxCiudad.ValueMember = "clv_ciudad"

            If ComboBoxCiudad.Items.Count > 0 Then
                ComboBoxCiudad.SelectedIndex = 0
                GloClvCiudad = ComboBoxCiudad.SelectedValue
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        eContrato = 0
        FrmCamServCte.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        If Len(Me.TextBox1.Text) > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim array As String()
            Try
                array = TextBox1.Text.Trim.Split("-")
                If (array.Length = 1) Then
                    GloIdCompania = 999
                Else
                    GloIdCompania = array(1)

                End If

            Catch ex As Exception
                MsgBox("Contrato inválido. Inténtelo nuevamente.")
                Exit Sub
            End Try
            llenaTokens(CInt(array(0)), "", 2, GloIdCompania)
            busca = 1
        ElseIf Len(Me.TextBox1.Text) = 0 Then
            busca = 0
        End If
    End Sub



    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress

        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros y guión
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Or val = 45 Then
            e.Handled = False
        Else
            e.Handled = True
        End If

        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TextBox1.Text) > 0 Then
                'CON.Open()
                'Me.ConCambioServClienteTableAdapter.Connection = CON
                'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, Me.TextBox1.Text, "", 0, 2)
                'CON.Close()
                Dim array As String()
                Try
                    array = TextBox1.Text.Trim.Split("-")
                    If (array.Length = 1) Then
                        GloIdCompania = 999
                    Else
                        GloIdCompania = array(1)

                    End If

                Catch ex As Exception
                    MsgBox("Contrato inválido. Inténtelo nuevamente.")
                    Exit Sub
                End Try
                

                llenaTokens(array(0), "", 2, GloIdCompania)
                busca = 1
            ElseIf Len(Me.TextBox1.Text) = 0 Then
                busca = 0
            End If
        End If

    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If TextBox2.Text.Length() = 0 Then
            Exit Sub
        End If
        llenaTokens(0, Me.TextBox2.Text, 3, GloIdCompania)
        If Len(Me.TextBox2.Text) = 0 Then
            busca = 0
        Else
            busca = 1
        End If

    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            'CON.Open()
            'Me.ConCambioServClienteTableAdapter.Connection = CON
            'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, Me.TextBox2.Text, 0, 3)
            'CON.Close()
           

            llenaTokens(0, Me.TextBox2.Text, 3, GloIdCompania)
        End If

    End Sub


    Private Sub BrwCamServCte_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If busca <> 1 Then
            llenaTokens(0, "", 0, GloIdCompania)
        End If
    End Sub

    Private Sub BrwCamServCte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        colorea(Me, Me.Name)


        Llena_companias()
      


        llenaTokens(0, "", 0, GloIdCompania)

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.BorCambioServClienteTableAdapter.Connection = CON
                Me.BorCambioServClienteTableAdapter.Fill(Me.DataSetEric.BorCambioServCliente, contrato, 0, eRes, eMsg)
                CON.Close()
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    MsgBox(mensaje6)
                    Refrescar()
                End If


            Else
                MsgBox("Selecciona un Cambio de Servicio.", , "Atención")
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            If Me.RealizarTextBox.Text = 1 Then
                eContrato = Me.ContratoLabel2.Text
                eTipSer = Me.Clv_TipSerTextBox.Text
                eOpcion = "M"
                FrmCamServCte.Show()
            Else
                MsgBox("Sólo se pueden Modificar Procesos Activos.")
            End If
        Else
            MsgBox("Selecciona un Cliente.", , "Atención")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Refrescar()
        

        llenaTokens(0, "", 0, GloIdCompania)
    End Sub

    Private Sub ConCambioServClienteDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConCambioServClienteDataGridView.CellContentClick
        
    End Sub

    Private Sub Button3_Click_1(sender As System.Object, e As System.EventArgs) Handles Button3.Click

        eOpcion = "N"
        eContrato = 0
        FrmToken.Show()
    End Sub

    Private Sub Button7_Click(sender As System.Object, e As System.EventArgs) Handles Button7.Click
        gloIdToken = CInt(LabelIdToken.Text)
        eOpcion = "C"
        eContrato = 0
        FrmToken.Show()
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue        
            llenaTokens(0, "", 4, GloIdCompania)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConCambioServClienteDataGridView_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConCambioServClienteDataGridView.CellClick
        Try
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                contrato = Me.ConCambioServClienteDataGridView.SelectedCells(3).Value.ToString
                LabelIdToken.Text = Me.ConCambioServClienteDataGridView.SelectedCells(0).Value.ToString
                ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
                NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(5).Value.ToString
                ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
                LabelPrecio.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
                LabelFechaActivacion.Text = Me.ConCambioServClienteDataGridView.SelectedCells(7).Value.ToString
                LabelFactura.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
                LabelUsuario.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
            Else
                limpiacampos()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub limpiacampos()
        contrato = 0
        ContratoLabel2.Text = ""
        NombreLabel2.Text = ""
        ConceptoLabel2.Text = ""
        LabelPrecio.Text = ""
        LabelFechaActivacion.Text = ""
        LabelFactura.Text = ""
        StatusLabel2.Text = ""
        Fecha_SolLabel1.Text = ""
    End Sub

    Private Sub ComboBoxCiudad_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCiudad.SelectedIndexChanged
        Try
            GloClvCiudad = ComboBoxCiudad.SelectedValue
            Llena_companias()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaTokens(ByVal contratoLoc As Integer, ByVal nombreLoc As String, ByVal OpLoc As Integer, ByVal IdcompaniaLoc As Integer)
        Llena_companias()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, contratoLoc)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, nombreLoc)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, OpLoc)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, IdcompaniaLoc)
        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)

        ConCambioServClienteDataGridView.DataSource = BaseII.ConsultaDT("DamenTokens")

        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            contrato = Me.ConCambioServClienteDataGridView.SelectedCells(3).Value.ToString
            LabelIdToken.Text = Me.ConCambioServClienteDataGridView.SelectedCells(0).Value.ToString
            ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
            NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(5).Value.ToString
            ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
            LabelPrecio.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
            LabelFechaActivacion.Text = Me.ConCambioServClienteDataGridView.SelectedCells(7).Value.ToString
            LabelFactura.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
            LabelUsuario.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
        Else
            limpiacampos()
        End If
    End Sub

    Private Sub ConCambioServClienteDataGridView_SelectionChanged(sender As Object, e As EventArgs) Handles ConCambioServClienteDataGridView.SelectionChanged
        Try
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                contrato = Me.ConCambioServClienteDataGridView.SelectedCells(3).Value.ToString
                LabelIdToken.Text = Me.ConCambioServClienteDataGridView.SelectedCells(0).Value.ToString
                ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value.ToString
                NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(5).Value.ToString
                ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value.ToString
                LabelPrecio.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value.ToString
                LabelFechaActivacion.Text = Me.ConCambioServClienteDataGridView.SelectedCells(7).Value.ToString
                LabelFactura.Text = Me.ConCambioServClienteDataGridView.SelectedCells(10).Value.ToString
                LabelUsuario.Text = Me.ConCambioServClienteDataGridView.SelectedCells(11).Value.ToString
            Else
                limpiacampos()
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class