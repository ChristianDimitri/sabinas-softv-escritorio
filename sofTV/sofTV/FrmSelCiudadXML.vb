﻿Imports System.Data.SqlClient
Public Class FrmSelCiudadXML

    Private Sub FrmSelCiudadXML_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CargarElementosCiudadesXML()
        llenalistboxs()
        If loquehay.Items.Count = 1 Or PasarTodoXML Then
            llevamealotro()
            Me.Close()
        End If
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCiudades.SelectNodes("//CIUDAD")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Ciudad") = loquehay.SelectedValue Then
                elem.SetAttribute("Seleccion", 1)
            End If
        Next
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCiudades.SelectNodes("//CIUDAD")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Ciudad") = seleccion.SelectedValue Then
                elem.SetAttribute("Seleccion", 0)
            End If
        Next
        llenalistboxs()

    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCiudades.SelectNodes("//CIUDAD")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        llenalistboxs()

    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCiudades.SelectNodes("//CIUDAD")
        For Each elem As Xml.XmlElement In elementos

            elem.SetAttribute("Seleccion", 0)

        Next
        llenalistboxs()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        EntradasSelCiudad = 0
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        If GloOpFiltrosXML = "VariosDesconectados" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaListado" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaOrdenes" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "VariosAlCorriente" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "VariosContrataciones" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCortesia" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCiudad" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "AtencionTelefonica" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "PendientesDeRealizar" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "ResumenOrdenReporte" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "DetalleOrdenes" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "ListadoQuejas" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "ListaCumpleanos" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "EnviarSMS" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "QuejasConcetoDetallado" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "InterfazDigital" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "InstalacionesEfectuadas" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "sindocumentos" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "MensajesVarios" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "Carteras" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "CarteraDistribuidor" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "PruebasDeInternet" Then
            FrmSelLocalidadXML.Show()
        End If
        Me.Close()
    End Sub
    Private Sub llevamealotro()
        Dim elementos = DocCiudades.SelectNodes("//CIUDAD")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        If GloOpFiltrosXML = "VariosDesconectados" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaListado" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaOrdenes" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "VariosAlCorriente" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "VariosContrataciones" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCortesia" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCiudad" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "AtencionTelefonica" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "PendientesDeRealizar" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "ResumenOrdenReporte" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "DetalleOrdenes" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "ListadoQuejas" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "ListaCumpleanos" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "EnviarSMS" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "QuejasConcetoDetallado" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "InterfazDigital" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "InstalacionesEfectuadas" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "sindocumentos" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "MensajesVarios" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "Carteras" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "CarteraDistribuidor" Then
            FrmSelLocalidadXML.Show()
        End If
        If GloOpFiltrosXML = "PruebasDeInternet" Then
            FrmSelLocalidadXML.Show()
        End If
    End Sub

    Private Sub llenalistboxs()
        Dim dt As New DataTable
        dt.Columns.Add("Clv_Ciudad", Type.GetType("System.String"))
        dt.Columns.Add("Nombre", Type.GetType("System.String"))

        Dim dt2 As New DataTable
        dt2.Columns.Add("Clv_Ciudad", Type.GetType("System.String"))
        dt2.Columns.Add("Nombre", Type.GetType("System.String"))

        Dim elementos = DocCiudades.SelectNodes("//CIUDAD")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Seleccion") = 0 Then
                dt.Rows.Add(elem.GetAttribute("Clv_Ciudad"), elem.GetAttribute("Nombre"))
            End If
            If elem.GetAttribute("Seleccion") = 1 Then
                dt2.Rows.Add(elem.GetAttribute("Clv_Ciudad"), elem.GetAttribute("Nombre"))
            End If
        Next
        loquehay.DataSource = New BindingSource(dt, Nothing)
        seleccion.DataSource = New BindingSource(dt2, Nothing)
    End Sub
End Class