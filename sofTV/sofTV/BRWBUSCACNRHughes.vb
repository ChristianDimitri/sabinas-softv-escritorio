Imports System.Data.SqlClient
Public Class BRWBUSCACNRHughes
    Dim loccon As Integer = 0
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            End If
            GloIdCompania = 0
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BRWBUSCACNR_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        Llena_companias()
        CON.Open()
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        'Me.BUSCACNRTableAdapter.Connection = CON
        'Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BuscaCNRHughes, 0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", -1)
        BuscaCNRHughes(0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", -1, 0)
        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If op = 0 Then
            '--Por Consecutivo
            If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
            'Me.BUSCACNRTableAdapter.Connection = CON
            'Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BuscaCNRHughes, Me.TextBox1.Text, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", op)
            BuscaCNRHughes(Me.TextBox1.Text, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", op, 0)
        ElseIf op = 1 Then
            '--Por numero_de_contrato
            '--If IsNumeric(Me.TextBox2.Text) = False Then Me.TextBox2.Text = 0
            'Me.BUSCACNRTableAdapter.Connection = CON
            'Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BuscaCNRHughes, 0, Me.TextBox2.Text, "", 0, 0, "", "01/01/1900", "01/01/1900", op)
            Try
                If TextBox2.Text.Contains("-") Then
                    Dim array As String()
                    array = TextBox2.Text.Trim.Split("-")
                    loccon = array(0).Trim
                    If array(1).Length = 0 Then
                        Exit Sub
                    End If
                    GloIdCompania = array(1).Trim
                    BuscaCNRHughes(0, loccon, "", 0, 0, "", "01/01/1900", "01/01/1900", op, GloIdCompania)
                Else
                    loccon = TextBox2.Text
                    GloIdCompania = 999
                    BuscaCNRHughes(0, Me.TextBox2.Text, "", 0, 0, "", "01/01/1900", "01/01/1900", op, GloIdCompania)
                End If
            Catch ex As Exception

            End Try
        ElseIf op = 2 Then
            '--Por @mac_addres
            If Len(Trim(Me.TextBox3.Text)) = 0 Then Me.TextBox3.Text = ""
            'Me.BUSCACNRTableAdapter.Connection = CON
            'Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BuscaCNRHughes, 0, 0, Me.TextBox3.Text, 0, 0, "", "01/01/1900", "01/01/1900", op)
            BuscaCNRHughes(0, 0, Me.TextBox3.Text, 0, 0, "", "01/01/1900", "01/01/1900", op, 0)
        ElseIf op = 3 Then
            '--Por @resultado
            Dim Resultado As Integer = 0
            Resultado = CLng(Mid(Trim(Me.ComboBox1.Text), 1, 1))
            'Me.BUSCACNRTableAdapter.Connection = CON
            'Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BuscaCNRHughes, 0, 0, "", Resultado, 0, "", "01/01/1900", "01/01/1900", op)
            BuscaCNRHughes(0, 0, "", Resultado, 0, "", "01/01/1900", "01/01/1900", op, 0)
        ElseIf op = 4 Then
            '--Por @Clv_Orden
            If IsNumeric(Me.TextBox5.Text) = False Then Me.TextBox5.Text = 0
            'Me.BUSCACNRTableAdapter.Connection = CON
            'Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BuscaCNRHughes, 0, 0, "", 0, Me.TextBox5.Text, "", "01/01/1900", "01/01/1900", op)
            BuscaCNRHughes(0, 0, "", 0, Me.TextBox5.Text, "", "01/01/1900", "01/01/1900", op, 0)
        ElseIf op = 5 Then
            '--Por @Status
            Dim Status As String = "P"
            Status = CStr(Mid(Trim(Me.ComboBox2.Text), 1, 1))
            'Me.BUSCACNRTableAdapter.Connection = CON
            'Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BuscaCNRHughes, 0, 0, "", 0, 0, Status, "01/01/1900", "01/01/1900", op)
            BuscaCNRHughes(0, 0, "", 0, 0, Status, "01/01/1900", "01/01/1900", op, 0)
        ElseIf op = 6 Then
            '--Por @Fec_Sol
            If IsDate(Me.TextBox7.Text) = True Then
                'Me.BUSCACNRTableAdapter.Connection = CON
                'Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BuscaCNRHughes, 0, 0, "", 0, 0, "", Me.TextBox7.Text, "01/01/1900", op)
                BuscaCNRHughes(0, 0, "", 0, 0, "", Me.TextBox7.Text, "01/01/1900", op, 0)
            End If
        ElseIf op = 7 Then
            '--Por @Fec_Eje
            If IsDate(Me.TextBox8.Text) = True Then
                'Me.BUSCACNRTableAdapter.Connection = CON
                'Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BuscaCNRHughes, 0, 0, "", 0, 0, "", "01/01/1900", Me.TextBox8.Text, op)
                BuscaCNRHughes(0, 0, "", 0, 0, "", "01/01/1900", Me.TextBox8.Text, op, 0)
            End If
        Else
            'Me.BUSCACNRTableAdapter.Connection = CON
            'Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BuscaCNRHughes, 0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", -1)
            BuscaCNRHughes(0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", -1, 0)
        End If

        Me.TextBox1.Text = ""
        Me.TextBox2.Text = ""
        Me.TextBox3.Text = ""
        Me.TextBox5.Text = ""
        Me.TextBox7.Text = ""
        Me.TextBox8.Text = ""
        Me.ComboBox1.Text = ""
        Me.ComboBox2.Text = ""
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Busca(2)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Busca(3)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Busca(4)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Busca(5)
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(6)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(7)
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Busca(3)
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(4)
        End If
    End Sub

    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Busca(5)
    End Sub

    Private Sub TextBox7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox7.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(6)
        End If
    End Sub

    Private Sub TextBox7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox7.TextChanged

    End Sub

    Private Sub TextBox8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox8.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(7)
        End If
    End Sub

    Private Sub TextBox8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox8.TextChanged

    End Sub
    Private Sub BuscaCNRHughes(ByVal consecutivo As Long, ByVal contrato As Integer, ByVal mac As String, ByVal resultado As Integer, ByVal clvorden As Long, ByVal status As String, ByVal fecsol As String, ByVal feceje As String, ByVal op As Integer, ByVal idcompania As Integer)
        '@consecutivo bigint,@numero_de_contrato varchar(50),@mac_addres varchar(50),@resultado int,@Clv_Orden bigint,@Status varchar(5),@Fec_Sol Datetime,@Fec_Eje Datetime,@Op int,@id_companiaJano int
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@consecutivo", SqlDbType.BigInt, consecutivo)
        BaseII.CreateMyParameter("@numero_de_contrato", SqlDbType.VarChar, contrato)
        BaseII.CreateMyParameter("@mac_addres", SqlDbType.VarChar, mac)
        BaseII.CreateMyParameter("@resultado", SqlDbType.Int, resultado)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, clvorden)
        BaseII.CreateMyParameter("@Status", SqlDbType.VarChar, status)
        BaseII.CreateMyParameter("@Fec_Sol", SqlDbType.DateTime, fecsol)
        BaseII.CreateMyParameter("@Fec_Eje", SqlDbType.DateTime, feceje)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
        BaseII.CreateMyParameter("@id_companiaJano", SqlDbType.Int, idcompania)
        BaseII.CreateMyParameter("@clave", SqlDbType.Int, GloClvUsuario)
        DataGridView1.DataSource = BaseII.ConsultaDT("BuscaCNR_Hughes")
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        Try
            ConsecutivoLabel1.Text = DataGridView1.SelectedCells(0).Value.ToString
            Numero_de_contratoLabel1.Text = DataGridView1.SelectedCells(1).Value.ToString
            Mac_addresLabel1.Text = DataGridView1.SelectedCells(2).Value.ToString
            PaqueteLabel1.Text = DataGridView1.SelectedCells(3).Value.ToString
            ComandoLabel1.Text = DataGridView1.SelectedCells(4).Value.ToString
            ResultadoLabel1.Text = DataGridView1.SelectedCells(5).Value.ToString
            Descripcion_transaccionLabel1.Text = DataGridView1.SelectedCells(6).Value.ToString
            Clv_OrdenLabel1.Text = DataGridView1.SelectedCells(7).Value.ToString
            Fec_SolLabel1.Text = DataGridView1.SelectedCells(8).Value.ToString
            Fec_EjeLabel1.Text = DataGridView1.SelectedCells(9).Value.ToString
            StatusLabel1.Text = DataGridView1.SelectedCells(10).Value.ToString
            LabelBeam.Text = DataGridView1.SelectedCells(11).Value.ToString
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        ' Try
        'GloIdCompania = ComboBoxCompanias.SelectedValue
        If (IsNumeric(ComboBoxCompanias.SelectedValue)) Then
            BuscaCNRHughes(0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", -1, ComboBoxCompanias.SelectedValue)
        End If

        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Information)
        'End Try
    End Sub

    Private Sub Label13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label13.Click

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class