<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelUsuariosVentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ConSeleccionaUsuariosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSeleccionaUsuariosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSeleccionaUsuariosTmpTableAdapter()
        Me.NombreListBox = New System.Windows.Forms.ListBox()
        Me.ConSeleccionaUsuariosProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSeleccionaUsuariosProTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSeleccionaUsuariosProTableAdapter()
        Me.NombreListBox1 = New System.Windows.Forms.ListBox()
        Me.InsertarSeleccionaUsuariosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarSeleccionaUsuariosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarSeleccionaUsuariosTmpTableAdapter()
        Me.BorrarSeleccionaUsuariosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarSeleccionaUsuariosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.BorrarSeleccionaUsuariosTmpTableAdapter()
        Me.Dame_clv_session_ReportesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_session_ReportesTableAdapter = New sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter()
        Me.Clv_GrupoTextBox = New System.Windows.Forms.TextBox()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_GrupoTextBox1 = New System.Windows.Forms.TextBox()
        Me.ClaveTextBox1 = New System.Windows.Forms.TextBox()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSeleccionaUsuariosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSeleccionaUsuariosProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarSeleccionaUsuariosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarSeleccionaUsuariosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(459, 119)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 31)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(459, 158)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(85, 31)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(459, 309)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(85, 31)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(459, 347)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(85, 31)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(659, 489)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&ACEPTAR"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(848, 489)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 44)
        Me.Button6.TabIndex = 5
        Me.Button6.Text = "&SALIR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConSeleccionaUsuariosTmpBindingSource
        '
        Me.ConSeleccionaUsuariosTmpBindingSource.DataMember = "ConSeleccionaUsuariosTmp"
        Me.ConSeleccionaUsuariosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSeleccionaUsuariosTmpTableAdapter
        '
        Me.ConSeleccionaUsuariosTmpTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox
        '
        Me.NombreListBox.DataSource = Me.ConSeleccionaUsuariosTmpBindingSource
        Me.NombreListBox.DisplayMember = "Nombre"
        Me.NombreListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreListBox.FormattingEnabled = True
        Me.NombreListBox.ItemHeight = 18
        Me.NombreListBox.Location = New System.Drawing.Point(595, 52)
        Me.NombreListBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreListBox.Name = "NombreListBox"
        Me.NombreListBox.Size = New System.Drawing.Size(367, 382)
        Me.NombreListBox.TabIndex = 9
        '
        'ConSeleccionaUsuariosProBindingSource
        '
        Me.ConSeleccionaUsuariosProBindingSource.DataMember = "ConSeleccionaUsuariosPro"
        Me.ConSeleccionaUsuariosProBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSeleccionaUsuariosProTableAdapter
        '
        Me.ConSeleccionaUsuariosProTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox1
        '
        Me.NombreListBox1.DataSource = Me.ConSeleccionaUsuariosProBindingSource
        Me.NombreListBox1.DisplayMember = "Nombre"
        Me.NombreListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreListBox1.FormattingEnabled = True
        Me.NombreListBox1.ItemHeight = 18
        Me.NombreListBox1.Location = New System.Drawing.Point(48, 52)
        Me.NombreListBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreListBox1.Name = "NombreListBox1"
        Me.NombreListBox1.Size = New System.Drawing.Size(367, 382)
        Me.NombreListBox1.TabIndex = 13
        '
        'InsertarSeleccionaUsuariosTmpBindingSource
        '
        Me.InsertarSeleccionaUsuariosTmpBindingSource.DataMember = "InsertarSeleccionaUsuariosTmp"
        Me.InsertarSeleccionaUsuariosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarSeleccionaUsuariosTmpTableAdapter
        '
        Me.InsertarSeleccionaUsuariosTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarSeleccionaUsuariosTmpBindingSource
        '
        Me.BorrarSeleccionaUsuariosTmpBindingSource.DataMember = "BorrarSeleccionaUsuariosTmp"
        Me.BorrarSeleccionaUsuariosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'BorrarSeleccionaUsuariosTmpTableAdapter
        '
        Me.BorrarSeleccionaUsuariosTmpTableAdapter.ClearBeforeFill = True
        '
        'Dame_clv_session_ReportesBindingSource
        '
        Me.Dame_clv_session_ReportesBindingSource.DataMember = "Dame_clv_session_Reportes"
        Me.Dame_clv_session_ReportesBindingSource.DataSource = Me.DataSetEric2
        '
        'Dame_clv_session_ReportesTableAdapter
        '
        Me.Dame_clv_session_ReportesTableAdapter.ClearBeforeFill = True
        '
        'Clv_GrupoTextBox
        '
        Me.Clv_GrupoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSeleccionaUsuariosTmpBindingSource, "Clv_Grupo", True))
        Me.Clv_GrupoTextBox.Location = New System.Drawing.Point(752, 164)
        Me.Clv_GrupoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_GrupoTextBox.Name = "Clv_GrupoTextBox"
        Me.Clv_GrupoTextBox.ReadOnly = True
        Me.Clv_GrupoTextBox.Size = New System.Drawing.Size(12, 22)
        Me.Clv_GrupoTextBox.TabIndex = 14
        Me.Clv_GrupoTextBox.TabStop = False
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSeleccionaUsuariosTmpBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(773, 164)
        Me.ClaveTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(12, 22)
        Me.ClaveTextBox.TabIndex = 15
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_GrupoTextBox1
        '
        Me.Clv_GrupoTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSeleccionaUsuariosProBindingSource, "Clv_Grupo", True))
        Me.Clv_GrupoTextBox1.Location = New System.Drawing.Point(232, 123)
        Me.Clv_GrupoTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_GrupoTextBox1.Name = "Clv_GrupoTextBox1"
        Me.Clv_GrupoTextBox1.ReadOnly = True
        Me.Clv_GrupoTextBox1.Size = New System.Drawing.Size(12, 22)
        Me.Clv_GrupoTextBox1.TabIndex = 16
        Me.Clv_GrupoTextBox1.TabStop = False
        '
        'ClaveTextBox1
        '
        Me.ClaveTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSeleccionaUsuariosProBindingSource, "Clave", True))
        Me.ClaveTextBox1.Location = New System.Drawing.Point(253, 123)
        Me.ClaveTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ClaveTextBox1.Name = "ClaveTextBox1"
        Me.ClaveTextBox1.ReadOnly = True
        Me.ClaveTextBox1.Size = New System.Drawing.Size(12, 22)
        Me.ClaveTextBox1.TabIndex = 17
        Me.ClaveTextBox1.TabStop = False
        '
        'FrmSelUsuariosVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1056, 558)
        Me.Controls.Add(Me.NombreListBox)
        Me.Controls.Add(Me.NombreListBox1)
        Me.Controls.Add(Me.ClaveTextBox1)
        Me.Controls.Add(Me.Clv_GrupoTextBox1)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.Clv_GrupoTextBox)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelUsuariosVentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Usuarios"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSeleccionaUsuariosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSeleccionaUsuariosProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarSeleccionaUsuariosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarSeleccionaUsuariosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConSeleccionaUsuariosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSeleccionaUsuariosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConSeleccionaUsuariosTmpTableAdapter
    Friend WithEvents NombreListBox As System.Windows.Forms.ListBox
    Friend WithEvents ConSeleccionaUsuariosProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSeleccionaUsuariosProTableAdapter As sofTV.DataSetEric2TableAdapters.ConSeleccionaUsuariosProTableAdapter
    Friend WithEvents NombreListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents InsertarSeleccionaUsuariosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarSeleccionaUsuariosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarSeleccionaUsuariosTmpTableAdapter
    Friend WithEvents BorrarSeleccionaUsuariosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarSeleccionaUsuariosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.BorrarSeleccionaUsuariosTmpTableAdapter
    Friend WithEvents Dame_clv_session_ReportesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_session_ReportesTableAdapter As sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter
    Friend WithEvents Clv_GrupoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_GrupoTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox1 As System.Windows.Forms.TextBox
End Class
