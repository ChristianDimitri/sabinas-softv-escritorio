Imports System.Data.SqlClient
Public Class BrwCalles
    'Private Sub Llena_companias()
    '    Try
    '        BaseII.limpiaParametros()
    '        'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
    '        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
    '        ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
    '        ComboBoxCompanias.DisplayMember = "razon_social"
    '        ComboBoxCompanias.ValueMember = "id_compania"

    '        If ComboBoxCompanias.Items.Count > 0 Then
    '            ComboBoxCompanias.SelectedIndex = 0
    '            GloIdCompania = ComboBoxCompanias.SelectedValue
    '        End If
    '        'GloIdCompania = 0
    '        'ComboBoxCiudades.Text = ""
    '    Catch ex As Exception

    '    End Try
    'End Sub


    Public Sub CREAARBOL()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            If IsNumeric(Clv_calleLabel2.Text) = True Then
                Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
                Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Clv_calleLabel2.Text, Integer)))
                Dim FilaRow As DataRow
                Me.TreeView1.Nodes.Clear()
                For Each FilaRow In Me.NewSofTvDataSet.DAMECOLONIA_CALLE.Rows
                    'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                    X = 0
                    If IsNumeric(Trim(FilaRow("Clv_colonia").ToString())) = True Then
                        Me.TreeView1.Nodes.Add(Trim(FilaRow("Clv_colonia").ToString()), Trim(FilaRow("Colonia").ToString()))
                        Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("Clv_colonia").ToString())
                        I += 1
                    End If
                Next
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub llenacalles()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BusCalleTableAdapter1.Connection = CON
            Me.BusCalleTableAdapter1.Fill(Me.DataSetEDGAR.BusCalle, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(3, Integer)), GloIdCompania)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub BUSCA_CLAVE()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.TextBox1.Text) = True Then
                Me.BusCalleTableAdapter1.Connection = CON
                Me.BusCalleTableAdapter1.Fill(Me.DataSetEDGAR.BusCalle, New System.Nullable(Of Integer)(CType(Me.TextBox1.Text, Integer)), "", New System.Nullable(Of Integer)(CType(0, Integer)), GloIdCompania)
            Else
                MsgBox("La Clave que busca no es valida")
            End If
            Me.TextBox1.Clear()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BUSCA_NOMBRE()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If Len(Trim(Me.TextBox2.Text)) > 0 Then
                Me.BusCalleTableAdapter1.Connection = CON
                Me.BusCalleTableAdapter1.Fill(Me.DataSetEDGAR.BusCalle, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)), GloIdCompania)
            Else
                MsgBox("El Nombre de la Calle que busca no es valida")
            End If
            Me.TextBox2.Clear()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.BUSCA_CLAVE()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.BUSCA_NOMBRE()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCA_CLAVE()
        End If
    End Sub


    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCA_NOMBRE()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloClv_Calle = 0
        FrmCalles.Show()
    End Sub

    Private Sub consultar()
        opcion = "C"
        GloClv_Calle = Me.Clv_calleLabel2.Text
        FrmCalles.Show()
    End Sub

    Private Sub Modificar()
        opcion = "M"
        GloClv_Calle = Me.Clv_calleLabel2.Text
        FrmCalles.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            Modificar()
        Else
            MsgBox(mensaje1)
        End If
    End Sub


    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        Me.CREAARBOL()
    End Sub

    Private Sub BrwCalles_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            llenacalles()
        End If
    End Sub

    Private Sub BrwCalles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

        
            colorea(Me, Me.Name)
            GloIdCompania = 0
            'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
            If gloescr = 1 Then
                Me.Button2.Enabled = False
                Me.Button4.Enabled = False
            End If
            'Llena_companias()
            llenacalles()
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
            UspDesactivaBotones(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            Modificar()
        End If
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            llenacalles()
        Catch ex As Exception

        End Try
    End Sub
End Class