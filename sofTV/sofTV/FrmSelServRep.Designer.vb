<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelServRep
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.MuestraSeleccionServiciosCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.LLena_Tabla_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.MuestraSeleccionaServiciosTmpCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ElegirServicioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ElegirServicioTableAdapter = New sofTV.DataSetarnoldoTableAdapters.ElegirServicioTableAdapter()
        Me.LLena_Tabla_ServiciosTableAdapter = New sofTV.DataSetarnoldoTableAdapters.LLena_Tabla_ServiciosTableAdapter()
        Me.CMB2Label1 = New System.Windows.Forms.Label()
        Me.CMBLABEL = New System.Windows.Forms.Label()
        Me.MuestraSeleccion_ServiciosCONSULTATableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSeleccion_ServiciosCONSULTATableAdapter()
        Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter()
        Me.MuestraSelecciona_TipoServicioTmpNUEVOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_TipoServicioTmpNUEVOTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_TipoServicioTmpNUEVOTableAdapter()
        Me.Insertauno_Seleccion_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_ServiciosTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_ServiciosTableAdapter()
        Me.Insertauno_Seleccion_ServiciostmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_ServiciostmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_ServiciostmpTableAdapter()
        Me.InsertaTOSelecciona_ServiciosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSelecciona_ServiciosTmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_ServiciosTmpTableAdapter()
        Me.InsertaTOSelecciona_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSelecciona_ServiciosTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_ServiciosTableAdapter()
        Me.Separa_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Separa_ServiciosTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Separa_ServiciosTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.MuestraSeleccionServiciosCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LLena_Tabla_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionaServiciosTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ElegirServicioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSelecciona_TipoServicioTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_ServiciostmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSelecciona_ServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSelecciona_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Separa_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel2.Location = New System.Drawing.Point(369, 14)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(185, 16)
        Me.CMBLabel2.TabIndex = 31
        Me.CMBLabel2.Text = "Servicios Seleccionados:"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(15, 14)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(200, 16)
        Me.CMBLabel1.TabIndex = 30
        Me.CMBLabel1.Text = "Servicios para Seleccionar:"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(445, 272)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(282, 272)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 4
        Me.Button6.Text = "&Aceptar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(265, 159)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(265, 130)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(265, 62)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(265, 33)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.DataSource = Me.MuestraSeleccionServiciosCONSULTABindingSource
        Me.ListBox2.DisplayMember = "Descripcion"
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(372, 33)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(209, 225)
        Me.ListBox2.TabIndex = 22
        Me.ListBox2.TabStop = False
        Me.ListBox2.ValueMember = "clv_txt"
        '
        'MuestraSeleccionServiciosCONSULTABindingSource
        '
        Me.MuestraSeleccionServiciosCONSULTABindingSource.DataMember = "MuestraSeleccion_ServiciosCONSULTA"
        Me.MuestraSeleccionServiciosCONSULTABindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LLena_Tabla_ServiciosBindingSource
        '
        Me.LLena_Tabla_ServiciosBindingSource.DataMember = "LLena_Tabla_Servicios"
        Me.LLena_Tabla_ServiciosBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.MuestraSeleccionaServiciosTmpCONSULTABindingSource
        Me.ListBox1.DisplayMember = "Descripcion"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(18, 33)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(202, 225)
        Me.ListBox1.TabIndex = 21
        Me.ListBox1.TabStop = False
        Me.ListBox1.ValueMember = "clv_txt"
        Me.ListBox1.Visible = False
        '
        'MuestraSeleccionaServiciosTmpCONSULTABindingSource
        '
        Me.MuestraSeleccionaServiciosTmpCONSULTABindingSource.DataMember = "MuestraSelecciona_ServiciosTmpCONSULTA"
        Me.MuestraSeleccionaServiciosTmpCONSULTABindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ElegirServicioBindingSource
        '
        Me.ElegirServicioBindingSource.DataMember = "ElegirServicio"
        Me.ElegirServicioBindingSource.DataSource = Me.DataSetarnoldo
        '
        'ElegirServicioTableAdapter
        '
        Me.ElegirServicioTableAdapter.ClearBeforeFill = True
        '
        'LLena_Tabla_ServiciosTableAdapter
        '
        Me.LLena_Tabla_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'CMB2Label1
        '
        Me.CMB2Label1.AutoSize = True
        Me.CMB2Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMB2Label1.ForeColor = System.Drawing.Color.Brown
        Me.CMB2Label1.Location = New System.Drawing.Point(15, 9)
        Me.CMB2Label1.Name = "CMB2Label1"
        Me.CMB2Label1.Size = New System.Drawing.Size(200, 16)
        Me.CMB2Label1.TabIndex = 30
        Me.CMB2Label1.Text = "Servicios para Seleccionar:"
        '
        'CMBLABEL
        '
        Me.CMBLABEL.AutoSize = True
        Me.CMBLABEL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLABEL.ForeColor = System.Drawing.Color.Brown
        Me.CMBLABEL.Location = New System.Drawing.Point(369, 9)
        Me.CMBLABEL.Name = "CMBLABEL"
        Me.CMBLABEL.Size = New System.Drawing.Size(185, 16)
        Me.CMBLABEL.TabIndex = 31
        Me.CMBLABEL.Text = "Servicios Seleccionados:"
        '
        'MuestraSeleccion_ServiciosCONSULTATableAdapter
        '
        Me.MuestraSeleccion_ServiciosCONSULTATableAdapter.ClearBeforeFill = True
        '
        'MuestraSelecciona_ServiciosTmpCONSULTATableAdapter
        '
        Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.ClearBeforeFill = True
        '
        'MuestraSelecciona_TipoServicioTmpNUEVOBindingSource
        '
        Me.MuestraSelecciona_TipoServicioTmpNUEVOBindingSource.DataMember = "MuestraSelecciona_TipoServicioTmpNUEVO"
        Me.MuestraSelecciona_TipoServicioTmpNUEVOBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraSelecciona_TipoServicioTmpNUEVOTableAdapter
        '
        Me.MuestraSelecciona_TipoServicioTmpNUEVOTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_ServiciosBindingSource
        '
        Me.Insertauno_Seleccion_ServiciosBindingSource.DataMember = "Insertauno_Seleccion_Servicios"
        Me.Insertauno_Seleccion_ServiciosBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_ServiciosTableAdapter
        '
        Me.Insertauno_Seleccion_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_ServiciostmpBindingSource
        '
        Me.Insertauno_Seleccion_ServiciostmpBindingSource.DataMember = "Insertauno_Seleccion_Serviciostmp"
        Me.Insertauno_Seleccion_ServiciostmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_ServiciostmpTableAdapter
        '
        Me.Insertauno_Seleccion_ServiciostmpTableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSelecciona_ServiciosTmpBindingSource
        '
        Me.InsertaTOSelecciona_ServiciosTmpBindingSource.DataMember = "InsertaTOSelecciona_ServiciosTmp"
        Me.InsertaTOSelecciona_ServiciosTmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSelecciona_ServiciosTmpTableAdapter
        '
        Me.InsertaTOSelecciona_ServiciosTmpTableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSelecciona_ServiciosBindingSource
        '
        Me.InsertaTOSelecciona_ServiciosBindingSource.DataMember = "InsertaTOSelecciona_Servicios"
        Me.InsertaTOSelecciona_ServiciosBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSelecciona_ServiciosTableAdapter
        '
        Me.InsertaTOSelecciona_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'Separa_ServiciosBindingSource
        '
        Me.Separa_ServiciosBindingSource.DataMember = "Separa_Servicios"
        Me.Separa_ServiciosBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Separa_ServiciosTableAdapter
        '
        Me.Separa_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmSelServRep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(624, 321)
        Me.Controls.Add(Me.CMBLABEL)
        Me.Controls.Add(Me.CMB2Label1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "FrmSelServRep"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección de Servicios"
        Me.TopMost = True
        CType(Me.MuestraSeleccionServiciosCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LLena_Tabla_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionaServiciosTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ElegirServicioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSelecciona_TipoServicioTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_ServiciostmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSelecciona_ServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSelecciona_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Separa_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ElegirServicioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents ElegirServicioTableAdapter As sofTV.DataSetarnoldoTableAdapters.ElegirServicioTableAdapter
    Friend WithEvents LLena_Tabla_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LLena_Tabla_ServiciosTableAdapter As sofTV.DataSetarnoldoTableAdapters.LLena_Tabla_ServiciosTableAdapter
    Friend WithEvents CMB2Label1 As System.Windows.Forms.Label
    Friend WithEvents CMBLABEL As System.Windows.Forms.Label
    Friend WithEvents MuestraSeleccionServiciosCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents MuestraSeleccionaServiciosTmpCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSeleccion_ServiciosCONSULTATableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSeleccion_ServiciosCONSULTATableAdapter
    Friend WithEvents MuestraSelecciona_ServiciosTmpCONSULTATableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter
    Friend WithEvents MuestraSelecciona_TipoServicioTmpNUEVOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_TipoServicioTmpNUEVOTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_TipoServicioTmpNUEVOTableAdapter
    Friend WithEvents Insertauno_Seleccion_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_ServiciosTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_ServiciosTableAdapter
    Friend WithEvents Insertauno_Seleccion_ServiciostmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_ServiciostmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_ServiciostmpTableAdapter
    Friend WithEvents InsertaTOSelecciona_ServiciosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSelecciona_ServiciosTmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_ServiciosTmpTableAdapter
    Friend WithEvents InsertaTOSelecciona_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSelecciona_ServiciosTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_ServiciosTableAdapter
    Friend WithEvents Separa_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Separa_ServiciosTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Separa_ServiciosTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
