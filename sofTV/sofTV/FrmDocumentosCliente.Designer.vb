﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDocumentosCliente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label50 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Grid = New System.Windows.Forms.DataGridView()
        Me.iddocumento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.activo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.cbRevisado = New System.Windows.Forms.CheckBox()
        Me.cbRecibido = New System.Windows.Forms.CheckBox()
        Me.pdfViewer = New System.Windows.Forms.WebBrowser()
        Label50 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label50
        '
        Label50.AutoSize = True
        Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label50.ForeColor = System.Drawing.Color.LightSlateGray
        Label50.Location = New System.Drawing.Point(34, 83)
        Label50.Name = "Label50"
        Label50.Size = New System.Drawing.Size(66, 15)
        Label50.TabIndex = 2
        Label50.Text = "Nombre: "
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(16, 38)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(84, 15)
        Label1.TabIndex = 33
        Label1.Text = "Documento:"
        '
        'tbNombre
        '
        Me.tbNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombre.Location = New System.Drawing.Point(107, 79)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(295, 21)
        Me.tbNombre.TabIndex = 1
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(353, 113)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(49, 30)
        Me.Button5.TabIndex = 31
        Me.Button5.Text = "..."
        Me.Button5.UseVisualStyleBackColor = False
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Items.AddRange(New Object() {"IFE", "Comprobante Domicilio", "Contrato Firmado"})
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(107, 32)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(295, 26)
        Me.ComboBoxCompanias.TabIndex = 32
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(107, 149)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(137, 40)
        Me.Button1.TabIndex = 34
        Me.Button1.Text = "Guardar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(1053, 707)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(137, 40)
        Me.Button2.TabIndex = 35
        Me.Button2.Text = "Salir"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tbNombre)
        Me.GroupBox1.Controls.Add(Label50)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.ComboBoxCompanias)
        Me.GroupBox1.Controls.Add(Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(408, 208)
        Me.GroupBox1.TabIndex = 37
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Subir archivos"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Grid)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 226)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(408, 239)
        Me.GroupBox2.TabIndex = 38
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Archivos existentes"
        '
        'Grid
        '
        Me.Grid.AllowUserToAddRows = False
        Me.Grid.AllowUserToDeleteRows = False
        Me.Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.iddocumento, Me.nombre, Me.activo, Me.Tipo})
        Me.Grid.Location = New System.Drawing.Point(21, 23)
        Me.Grid.Name = "Grid"
        Me.Grid.ReadOnly = True
        Me.Grid.RowHeadersVisible = False
        Me.Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Grid.Size = New System.Drawing.Size(372, 199)
        Me.Grid.TabIndex = 0
        '
        'iddocumento
        '
        Me.iddocumento.DataPropertyName = "IdDocumento"
        Me.iddocumento.HeaderText = "iddocumento"
        Me.iddocumento.Name = "iddocumento"
        Me.iddocumento.ReadOnly = True
        Me.iddocumento.Visible = False
        '
        'nombre
        '
        Me.nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nombre.DataPropertyName = "Nombre"
        Me.nombre.HeaderText = "Nombre"
        Me.nombre.Name = "nombre"
        Me.nombre.ReadOnly = True
        '
        'activo
        '
        Me.activo.DataPropertyName = "existe"
        Me.activo.FalseValue = "0"
        Me.activo.HeaderText = "Existe"
        Me.activo.Name = "activo"
        Me.activo.ReadOnly = True
        Me.activo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.activo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.activo.TrueValue = "1"
        Me.activo.Width = 70
        '
        'Tipo
        '
        Me.Tipo.DataPropertyName = "Tipo"
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.ReadOnly = True
        Me.Tipo.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.sofTV.My.Resources.Resources.filenotfound4041
        Me.PictureBox1.Location = New System.Drawing.Point(426, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(764, 689)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 36
        Me.PictureBox1.TabStop = False
        '
        'cbRevisado
        '
        Me.cbRevisado.AutoSize = True
        Me.cbRevisado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbRevisado.Location = New System.Drawing.Point(89, 489)
        Me.cbRevisado.Name = "cbRevisado"
        Me.cbRevisado.Size = New System.Drawing.Size(258, 20)
        Me.cbRevisado.TabIndex = 39
        Me.cbRevisado.Text = "Expediente Electrónico Revisado"
        Me.cbRevisado.UseVisualStyleBackColor = True
        Me.cbRevisado.Visible = False
        '
        'cbRecibido
        '
        Me.cbRecibido.AutoSize = True
        Me.cbRecibido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbRecibido.Location = New System.Drawing.Point(89, 525)
        Me.cbRecibido.Name = "cbRecibido"
        Me.cbRecibido.Size = New System.Drawing.Size(256, 20)
        Me.cbRecibido.TabIndex = 40
        Me.cbRecibido.Text = "Orden y Contrato Físico Recibido"
        Me.cbRecibido.UseVisualStyleBackColor = True
        Me.cbRecibido.Visible = False
        '
        'pdfViewer
        '
        Me.pdfViewer.Location = New System.Drawing.Point(426, 12)
        Me.pdfViewer.MinimumSize = New System.Drawing.Size(20, 20)
        Me.pdfViewer.Name = "pdfViewer"
        Me.pdfViewer.Size = New System.Drawing.Size(764, 689)
        Me.pdfViewer.TabIndex = 41
        Me.pdfViewer.Visible = False
        '
        'FrmDocumentosCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1194, 750)
        Me.Controls.Add(Me.pdfViewer)
        Me.Controls.Add(Me.cbRecibido)
        Me.Controls.Add(Me.cbRevisado)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button2)
        Me.Name = "FrmDocumentosCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Documentos del Cliente"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.Grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Grid As System.Windows.Forms.DataGridView
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cbRevisado As System.Windows.Forms.CheckBox
    Friend WithEvents cbRecibido As System.Windows.Forms.CheckBox
    Friend WithEvents pdfViewer As System.Windows.Forms.WebBrowser
    Friend WithEvents iddocumento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents activo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
