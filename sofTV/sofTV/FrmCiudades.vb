Imports System.Data.SqlClient
Public Class FrmCiudades
    Private nomciudad As String = Nothing
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                nomciudad = Me.NombreTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Llena_estados()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, Clv_CiudadTextBox.Text)
            ComboBoxEstado.DataSource = BaseII.ConsultaDT("DameEstadosAll")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Select Case op
            Case 0
                If opcion = "N" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se guardo un nueva ciudad", "", "Se guardo un nueva ciudad: " + Me.NombreTextBox.Text, LocClv_Ciudad)
                ElseIf opcion = "M" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NombreTextBox.Name, nomciudad, Me.NombreTextBox.Text, LocClv_Ciudad)
                End If
            Case 1
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Una Ciudad", "", "Se Elimino Una Ciudad: " + Me.NombreTextBox.Text, LocClv_Ciudad)
        End Select
    End Sub

    Private Sub FrmCiudades_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        If opcion = "N" Then
            Me.CONCIUDADESBindingSource.AddNew()
            Panel1.Enabled = True
            GroupBox1.Enabled = False
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
            Llena_estados()
            llena_dgvcompanias()
            GroupBox1.Enabled = False
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
            Llena_estados()
            llena_dgvcompanias()
        End If
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub DameEstadoCiudad()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, gloClave)
            BaseII.CreateMyParameter("@clv_estado", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("DameEstadoCiudad")
            ComboBoxEstado.SelectedValue = BaseII.dicoPar("@clv_estado")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCIUDADESTableAdapter.Connection = CON
            Me.CONCIUDADESTableAdapter.Fill(Me.NewSofTvDataSet.CONCIUDADES, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            damedatosbitacora()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub CONCIUDADESBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCIUDADESBindingNavigatorSaveItem.Click
        'If ComboBoxEstados.Text = "" Then
        '    MsgBox("Selecciona el estado al cual pertenece la ciudad.")
        '    Exit Sub
        'End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, NombreTextBox.Text)
        BaseII.CreateMyParameter("@mismoNombre", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.VarChar, Clv_CiudadTextBox.Text)
        BaseII.ProcedimientoOutPut("ValidaNombreCiudad")
        Dim mismoNombre = BaseII.dicoPar("@mismoNombre")
        If mismoNombre = 1 Then
            MsgBox("Ya existe una ciudad con el mismo nombre.")
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONCIUDADESBindingSource.EndEdit()
        Me.CONCIUDADESTableAdapter.Connection = CON
        Me.CONCIUDADESTableAdapter.Update(Me.NewSofTvDataSet.CONCIUDADES)
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, gloClave)
        'BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        'BaseII.Inserta("ModEstadoCiudad")
        guardabitacora(0)
        MsgBox("Se ha Guardado con Ex�to")
        GloBnd = True
        CON.Close()
        GroupBox1.Enabled = True
        Llena_estados()
        If opcion = "N" Then
            opcion = "M"
        End If
    End Sub


    Private Sub Clv_CiudadTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadTextBox.TextChanged
        If Len(eMsj) > 0 Then
            Me.Clv_CiudadTextBox.Text = gloClave
        End If
        gloClave = Me.Clv_CiudadTextBox.Text
    End Sub

    Private Sub NombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreTextBox.TextChanged
        If Len(nomciudad) > 0 Then
            NombreTextBox.Text = nomciudad
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'If dgvcompania.Rows.Count = 0 Then
        '    MsgBox("Tiene que establecer la relaci�n con alg�n estado.")
        '    Exit Sub
        'End If
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONCIUDADESBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If dgvcompania.Rows.Count > 0 Then
            MsgBox("No se puede eliminar la ciudad porque est� relacionada con su respectivo estado.")
            Exit Sub
        End If
        'ValidaEliminarRelCompaniaCiudad(2)
        'If Len(eMsj) > 0 Then
        '    MsgBox(eMsj, MsgBoxStyle.Information)
        '    nomciudad = NombreTextBox.Text
        '    Exit Sub
        'End If

        Me.CONCIUDADESTableAdapter.Connection = CON
        Me.CONCIUDADESTableAdapter.Delete(gloClave)
        guardabitacora(1)
        GloBnd = True
        Me.Close()
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ComboBoxEstado.SelectedValue = 0 Then
            Exit Sub
        End If
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand()
        conexion.Open()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "AgregaEliminaRelEstadoCiudad"
        Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
        p4.Direction = ParameterDirection.Input
        p4.Value = 1
        comando.Parameters.Add(p4)
        Dim p1 As New SqlParameter("@clv_ciudad", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = Clv_CiudadTextBox.Text
        comando.Parameters.Add(p1)
        Dim p2 As New SqlParameter("@clv_estado", SqlDbType.Int)
        p2.Direction = ParameterDirection.Input
        p2.Value = ComboBoxEstado.SelectedValue
        comando.Parameters.Add(p2)
        Dim resultado As Integer = comando.ExecuteScalar()
        If resultado = 1 Then
            MsgBox("El estado ya pertenece a la relaci�n.")
        Else
            llena_dgvcompanias()
            Llena_estados()
        End If
        If ComboBoxEstado.Items.Count = 0 Then
            ComboBoxEstado.Text = ""
        End If
    End Sub

    Private Sub llena_dgvcompanias()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, Clv_CiudadTextBox.Text)
            BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
            dgvcompania.DataSource = BaseII.ConsultaDT("AgregaEliminaRelEstadoCiudad")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If dgvcompania.Rows.Count = 0 Then
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, Clv_CiudadTextBox.Text)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, dgvcompania.SelectedCells(0).Value.ToString)
        BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.VarChar, 500)
        BaseII.ProcedimientoOutPut("ValidaEliminaRelCiudadEstado")
        eMsj = BaseII.dicoPar("@error")
        If Len(eMsj) > 0 Then
            MsgBox(eMsj, MsgBoxStyle.Information)
            Exit Sub
        End If

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand()
        conexion.Open()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "AgregaEliminaRelEstadoCiudad"
        Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
        p4.Direction = ParameterDirection.Input
        p4.Value = 2
        comando.Parameters.Add(p4)
        Dim p1 As New SqlParameter("@clv_ciudad", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = Clv_CiudadTextBox.Text
        comando.Parameters.Add(p1)
        Dim p2 As New SqlParameter("@clv_estado", SqlDbType.Int)
        p2.Direction = ParameterDirection.Input
        p2.Value = dgvcompania.SelectedCells(0).Value.ToString
        comando.Parameters.Add(p2)
        comando.ExecuteNonQuery()
        llena_dgvcompanias()
        Llena_estados()
    End Sub

    Private Sub ValidaEliminarRelCompaniaCiudad(ByVal Op As Integer)
        Dim IdCompa�ia As Integer = 0

        If dgvcompania.Rows.Count > 0 Then
            IdCompa�ia = dgvcompania.SelectedCells(0).Value.ToString
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, Clv_CiudadTextBox.Text)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, IdCompa�ia)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("ValidaEliminarRelCompaniaCiudad")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString
    End Sub

    Private Sub CONCIUDADESBindingNavigator_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCIUDADESBindingNavigator.RefreshItems

    End Sub

    Private Sub FrmCiudades_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If opcion = "N" Then
            Exit Sub
        End If
        'If dgvcompania.Rows.Count = 0 Then
        '    e.Cancel = True
        '    MsgBox("Tiene que establecer la relaci�n con alg�n estado.")
        'End If
    End Sub
End Class