Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Collections.Generic
Public Class BrwCatalogoSeries
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                GloIdCompania = ComboBoxCompanias.SelectedValue
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaDistribuidores()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@clvusuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxDistribuidor.DataSource = BaseII.ConsultaDT("Muestra_PlazasPorUsuario")
            ComboBoxDistribuidor.DisplayMember = "Nombre"
            ComboBoxDistribuidor.ValueMember = "Clv_Plaza"

            If ComboBoxDistribuidor.Items.Count > 0 Then
                ComboBoxDistribuidor.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmCatalogoSeries.Show()
    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            gloClave = Me.Clv_calleLabel2.Text
            FrmCatalogoSeries.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            gloClave = Me.Clv_calleLabel2.Text
            FrmCatalogoSeries.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            modificar()
        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If op = 0 Then
                Me.BUSCACatalogoSeriesTableAdapter.Connection = CON
                Me.BUSCACatalogoSeriesTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCACatalogoSeries, "", 0, "", 0, GloClvUsuario, 0, ComboBoxTipo.SelectedValue)
            ElseIf op = 1 Then
                If Len(Trim(Me.TextBox3.Text)) > 0 Then
                    Me.BUSCACatalogoSeriesTableAdapter.Connection = CON
                    Me.BUSCACatalogoSeriesTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCACatalogoSeries, Me.TextBox3.Text, 0, "", 1, GloClvUsuario, 0, ComboBoxTipo.SelectedValue)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 2 Then
                If Len(Trim(Me.TextBox1.Text)) > 0 Then
                    Me.BUSCACatalogoSeriesTableAdapter.Connection = CON
                    Me.BUSCACatalogoSeriesTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCACatalogoSeries, "", Me.TextBox1.Text, "", 2, GloClvUsuario, 0, ComboBoxTipo.SelectedValue)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 3 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSCACatalogoSeriesTableAdapter.Connection = CON
                    Me.BUSCACatalogoSeriesTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCACatalogoSeries, "", 0, Me.TextBox2.Text, 3, GloClvUsuario, 0, ComboBoxTipo.SelectedValue)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 4 Then
                Me.BUSCACatalogoSeriesTableAdapter.Connection = CON
                Me.BUSCACatalogoSeriesTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCACatalogoSeries, "", 0, "", 4, GloClvUsuario, ComboBoxDistribuidor.SelectedValue, ComboBoxTipo.SelectedValue)
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Me.TextBox3.Clear()
            CON.Close()
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(2)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(3)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub

    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            gloClave = Me.Clv_calleLabel2.Text
        End If
    End Sub



    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub


    Private Sub BrwCatalogoSeries_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Busca(0)
        End If
    End Sub

    Private Sub BrwCatalogoSeries_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        'Llena_companias()
        llenaDistribuidores()
        Llena_tipo()
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Busca(0)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub


    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(1)
    End Sub


    Private Sub Btn_Imprime_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Imprime.Click
        If Me.DataGridView1.RowCount > 0 Then
            VendedorSeries = DataGridView1.SelectedCells(4).Value.ToString()
        End If
        FrmSeriesFolio.Show()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        FrmReimpresionSeries.Show()
    End Sub

    Private Sub Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancelar.Click
        FrmFoliosCancelados.Show()
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        GloReportFolios = 5
        FormReporteFolios.Show()
    End Sub

    Private Sub bnFoliosCancelados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnFoliosCancelados.Click
        'Try
        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
        '    BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt,eClv_Session)

        '    Dim listNombreTablas As New List(Of String)
        '    listNombreTablas.Add("USPReporteFoliosVentas")

        '    Dim DS As DataSet = BaseII.ConsultaDS("USPReporteFoliosVentas", listNombreTablas)

        '    Dim diccioFormulasReporte As New Dictionary(Of String, String)
        '    diccioFormulasReporte.Add("Empresa", LocGloNomEmpresa)
        '    diccioFormulasReporte.Add("Titulo", "Reporte de Folios Faltantes y Cancelados")
        '    diccioFormulasReporte.Add("Sucursal", GloCiudadEmpresa)

        '    BaseII.llamarReporteCentralizado(RutaReportes + "\rptFoliosVentas", DS, diccioFormulasReporte)
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try

        eOpVentas = 2310


        'JUAN PABLO REPORTES
        'Dim selDiaMetas As New FrmSelDiaMetas()
        'FrmSelVendedor.vieneDeVendedores = True
        Dim selVendedor As New FrmSelVendedor()
        selVendedor.vieneDeVendedores = True
        Dim imprimirCentralizada As New FrmImprimirCentralizada()
        Dim reporte As New ReportDocument()


        If (selVendedor.ShowDialog() = DialogResult.OK) Then
            Try
                'ruta = RutaReportes & "\ReporteServicioNivel2.rpt"
                ''ruta = "D:\ReporteServicioNivel2.rpt"

                'reporte.Load(ruta)
                'reporte.SetDataSource(_DataSet)

                'imprimirCentralizada.rd = reporte
                'imprimirCentralizada.ShowDialog()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, eClv_Session)

                Dim listNombreTablas As New List(Of String)
                listNombreTablas.Add("USPReporteFoliosVentas")

                Dim DS As DataSet = BaseII.ConsultaDS("USPReporteFoliosVentas", listNombreTablas)

                Dim diccioFormulasReporte As New Dictionary(Of String, String)
                diccioFormulasReporte.Add("Empresa", GloEmpresaPRincipal)
                diccioFormulasReporte.Add("Titulo", "Reporte de Folios Faltantes y Cancelados")
                diccioFormulasReporte.Add("Sucursal", "")

                BaseII.llamarReporteCentralizado(RutaReportes + "\rptFoliosVentas", DS, diccioFormulasReporte)
                'Else
                'MsgBox("Seleccione el Vendedor(es)", MsgBoxStyle.Information)
                'Exit Sub
                'End If

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information)
            End Try

        End If
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Busca(4)
    End Sub

    Private Sub Llena_tipo()
        Try
            BaseII.limpiaParametros()
            ComboBoxTipo.DataSource = BaseII.ConsultaDT("DameTipoComboSerie")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxTipo.SelectedIndexChanged
        Busca(4)
    End Sub

    Private Sub Button10_Click(sender As System.Object, e As System.EventArgs) Handles Button10.Click
        FrmEvidenciasFoliosCancelados.ShowDialog()
    End Sub

    Private Sub ComboBoxDistribuidor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxDistribuidor.SelectedIndexChanged
        Busca(4)
    End Sub
End Class