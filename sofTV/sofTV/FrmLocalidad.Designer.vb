﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLocalidad
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label1 As System.Windows.Forms.Label
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLocalidad))
        Me.tbnombre = New System.Windows.Forms.TextBox()
        Me.tbclvlocalidad = New System.Windows.Forms.TextBox()
        Me.LocalidadBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.dgvciudad = New System.Windows.Forms.DataGridView()
        Me.IdCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Razon_Social = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComboBoxCiudad = New System.Windows.Forms.ComboBox()
        Label1 = New System.Windows.Forms.Label()
        ClaveLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        CType(Me.LocalidadBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LocalidadBindingNavigator.SuspendLayout()
        CType(Me.dgvciudad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(135, 117)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(78, 18)
        Label1.TabIndex = 39
        Label1.Text = "Nombre :"
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ClaveLabel.Location = New System.Drawing.Point(67, 70)
        ClaveLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(137, 18)
        ClaveLabel.TabIndex = 37
        ClaveLabel.Text = "Clave Localidad :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(47, 217)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(70, 18)
        Label2.TabIndex = 107
        Label2.Text = "Ciudad :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(193, 180)
        Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(283, 25)
        Label3.TabIndex = 110
        Label3.Text = "Relación Localidad - Ciudad"
        '
        'tbnombre
        '
        Me.tbnombre.BackColor = System.Drawing.Color.White
        Me.tbnombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbnombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbnombre.Location = New System.Drawing.Point(243, 114)
        Me.tbnombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbnombre.MaxLength = 255
        Me.tbnombre.Name = "tbnombre"
        Me.tbnombre.Size = New System.Drawing.Size(429, 24)
        Me.tbnombre.TabIndex = 40
        '
        'tbclvlocalidad
        '
        Me.tbclvlocalidad.BackColor = System.Drawing.Color.White
        Me.tbclvlocalidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbclvlocalidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbclvlocalidad.Enabled = False
        Me.tbclvlocalidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbclvlocalidad.Location = New System.Drawing.Point(243, 68)
        Me.tbclvlocalidad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbclvlocalidad.MaxLength = 5
        Me.tbclvlocalidad.Name = "tbclvlocalidad"
        Me.tbclvlocalidad.Size = New System.Drawing.Size(133, 24)
        Me.tbclvlocalidad.TabIndex = 38
        Me.tbclvlocalidad.Text = "0"
        '
        'LocalidadBindingNavigator
        '
        Me.LocalidadBindingNavigator.AddNewItem = Nothing
        Me.LocalidadBindingNavigator.CountItem = Nothing
        Me.LocalidadBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.LocalidadBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.LocalidadBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONSERVICIOSBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.LocalidadBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.LocalidadBindingNavigator.MoveFirstItem = Nothing
        Me.LocalidadBindingNavigator.MoveLastItem = Nothing
        Me.LocalidadBindingNavigator.MoveNextItem = Nothing
        Me.LocalidadBindingNavigator.MovePreviousItem = Nothing
        Me.LocalidadBindingNavigator.Name = "LocalidadBindingNavigator"
        Me.LocalidadBindingNavigator.PositionItem = Nothing
        Me.LocalidadBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LocalidadBindingNavigator.Size = New System.Drawing.Size(700, 28)
        Me.LocalidadBindingNavigator.TabIndex = 41
        Me.LocalidadBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(122, 25)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(105, 25)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONSERVICIOSBindingNavigatorSaveItem
        '
        Me.CONSERVICIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSERVICIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSERVICIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Name = "CONSERVICIOSBindingNavigatorSaveItem"
        Me.CONSERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 25)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(471, 303)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 41)
        Me.Button2.TabIndex = 109
        Me.Button2.Text = "ELIMINAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(471, 235)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 41)
        Me.Button1.TabIndex = 105
        Me.Button1.Text = "AGREGAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(491, 593)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 104
        Me.Button5.Text = "SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'dgvciudad
        '
        Me.dgvciudad.AllowUserToAddRows = False
        Me.dgvciudad.AllowUserToDeleteRows = False
        Me.dgvciudad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvciudad.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdCompania, Me.Razon_Social})
        Me.dgvciudad.Location = New System.Drawing.Point(51, 305)
        Me.dgvciudad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvciudad.Name = "dgvciudad"
        Me.dgvciudad.ReadOnly = True
        Me.dgvciudad.RowHeadersVisible = False
        Me.dgvciudad.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvciudad.Size = New System.Drawing.Size(403, 274)
        Me.dgvciudad.TabIndex = 108
        '
        'IdCompania
        '
        Me.IdCompania.DataPropertyName = "Clv_Ciudad"
        Me.IdCompania.HeaderText = "idcompania"
        Me.IdCompania.Name = "IdCompania"
        Me.IdCompania.ReadOnly = True
        Me.IdCompania.Visible = False
        '
        'Razon_Social
        '
        Me.Razon_Social.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Razon_Social.DataPropertyName = "Nombre"
        Me.Razon_Social.HeaderText = "Nombre"
        Me.Razon_Social.Name = "Razon_Social"
        Me.Razon_Social.ReadOnly = True
        '
        'ComboBoxCiudad
        '
        Me.ComboBoxCiudad.DisplayMember = "Nombre"
        Me.ComboBoxCiudad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCiudad.FormattingEnabled = True
        Me.ComboBoxCiudad.Location = New System.Drawing.Point(51, 244)
        Me.ComboBoxCiudad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxCiudad.Name = "ComboBoxCiudad"
        Me.ComboBoxCiudad.Size = New System.Drawing.Size(349, 30)
        Me.ComboBoxCiudad.TabIndex = 106
        Me.ComboBoxCiudad.ValueMember = "Clv_Ciudad"
        '
        'FrmLocalidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(700, 645)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.dgvciudad)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.ComboBoxCiudad)
        Me.Controls.Add(Me.LocalidadBindingNavigator)
        Me.Controls.Add(Me.tbnombre)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.tbclvlocalidad)
        Me.Controls.Add(ClaveLabel)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximumSize = New System.Drawing.Size(718, 690)
        Me.MinimumSize = New System.Drawing.Size(718, 690)
        Me.Name = "FrmLocalidad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Localidad"
        CType(Me.LocalidadBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LocalidadBindingNavigator.ResumeLayout(False)
        Me.LocalidadBindingNavigator.PerformLayout()
        CType(Me.dgvciudad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbnombre As System.Windows.Forms.TextBox
    Friend WithEvents tbclvlocalidad As System.Windows.Forms.TextBox
    Friend WithEvents LocalidadBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents dgvciudad As System.Windows.Forms.DataGridView
    Friend WithEvents IdCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Razon_Social As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComboBoxCiudad As System.Windows.Forms.ComboBox
End Class
