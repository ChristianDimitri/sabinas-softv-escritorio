<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelCd_CarteraPorPlaza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.DameClv_Session_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_Session_ServiciosTableAdapter = New sofTV.DataSetarnoldoTableAdapters.DameClv_Session_ServiciosTableAdapter()
        Me.Clv_Unicanet1 = New System.Windows.Forms.TextBox()
        Me.Contratonet = New System.Windows.Forms.TextBox()
        Me.Clv_Unicanet = New System.Windows.Forms.TextBox()
        Me.Contratonet1 = New System.Windows.Forms.TextBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter = New sofTV.DataSetLidiaTableAdapters.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter()
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter = New sofTV.DataSetLidiaTableAdapters.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter()
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter = New sofTV.DataSetLidiaTableAdapters.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter()
        Me.MuestraSelecciona_CiudadTmpNUEVOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter = New sofTV.DataSetLidiaTableAdapters.MuestraSelecciona_CiudadTmpNUEVOTableAdapter()
        Me.MuestraSelecciona_CiudadTmpCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter = New sofTV.DataSetLidiaTableAdapters.MuestraSelecciona_CiudadTmpCONSULTATableAdapter()
        Me.MuestraSeleccion_CiudadCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSeleccion_CiudadCONSULTATableAdapter = New sofTV.DataSetLidiaTableAdapters.MuestraSeleccion_CiudadCONSULTATableAdapter()
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter = New sofTV.DataSetLidiaTableAdapters.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter()
        Me.Borra_Seleccion_CiudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Seleccion_CiudadTableAdapter = New sofTV.DataSetLidiaTableAdapters.Borra_Seleccion_CiudadTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSelecciona_CiudadTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSelecciona_CiudadTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccion_CiudadCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkRed
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(365, 226)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(191, 37)
        Me.Button4.TabIndex = 118
        Me.Button4.Text = "<< Quitar To&do "
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(365, 135)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(191, 37)
        Me.Button3.TabIndex = 117
        Me.Button3.Text = "Agregar &Todo >>"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(365, 181)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(191, 37)
        Me.Button2.TabIndex = 116
        Me.Button2.Text = "< &Quitar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(365, 91)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(191, 37)
        Me.Button1.TabIndex = 115
        Me.Button1.Text = "&Agregar >"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(16, 37)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(321, 352)
        Me.TreeView1.TabIndex = 119
        '
        'TreeView2
        '
        Me.TreeView2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView2.Location = New System.Drawing.Point(580, 37)
        Me.TreeView2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(321, 352)
        Me.TreeView2.TabIndex = 120
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 11)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 24)
        Me.Label1.TabIndex = 121
        Me.Label1.Text = "Cuidades"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(721, 414)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 122
        Me.Button5.Text = "SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameClv_Session_ServiciosBindingSource
        '
        Me.DameClv_Session_ServiciosBindingSource.DataMember = "DameClv_Session_Servicios"
        Me.DameClv_Session_ServiciosBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DameClv_Session_ServiciosTableAdapter
        '
        Me.DameClv_Session_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'Clv_Unicanet1
        '
        Me.Clv_Unicanet1.Location = New System.Drawing.Point(419, 576)
        Me.Clv_Unicanet1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_Unicanet1.Name = "Clv_Unicanet1"
        Me.Clv_Unicanet1.Size = New System.Drawing.Size(160, 22)
        Me.Clv_Unicanet1.TabIndex = 123
        '
        'Contratonet
        '
        Me.Contratonet.Location = New System.Drawing.Point(296, 414)
        Me.Contratonet.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Contratonet.Name = "Contratonet"
        Me.Contratonet.Size = New System.Drawing.Size(175, 22)
        Me.Contratonet.TabIndex = 124
        Me.Contratonet.Visible = False
        '
        'Clv_Unicanet
        '
        Me.Clv_Unicanet.Location = New System.Drawing.Point(615, 574)
        Me.Clv_Unicanet.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_Unicanet.Name = "Clv_Unicanet"
        Me.Clv_Unicanet.Size = New System.Drawing.Size(141, 22)
        Me.Clv_Unicanet.TabIndex = 125
        '
        'Contratonet1
        '
        Me.Contratonet1.Location = New System.Drawing.Point(639, 617)
        Me.Contratonet1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Contratonet1.Name = "Contratonet1"
        Me.Contratonet1.Size = New System.Drawing.Size(143, 22)
        Me.Contratonet1.TabIndex = 126
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(520, 414)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 41)
        Me.Button6.TabIndex = 127
        Me.Button6.Text = "ACEPTAR"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource
        '
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataMember = "PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmp"
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataSource = Me.DataSetLidia
        '
        'PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
        '
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.ClearBeforeFill = True
        '
        'PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource
        '
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataMember = "PONTODOSSelecciona_CiudadTmp_Seleccion_Ciudad"
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataSource = Me.DataSetLidia
        '
        'PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
        '
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.ClearBeforeFill = True
        '
        'PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource
        '
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataMember = "PONUNOSeleccion_Ciudad_Selecciona_CiudadTmp"
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataSource = Me.DataSetLidia
        '
        'PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
        '
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.ClearBeforeFill = True
        '
        'MuestraSelecciona_CiudadTmpNUEVOBindingSource
        '
        Me.MuestraSelecciona_CiudadTmpNUEVOBindingSource.DataMember = "MuestraSelecciona_CiudadTmpNUEVO"
        Me.MuestraSelecciona_CiudadTmpNUEVOBindingSource.DataSource = Me.DataSetLidia
        '
        'MuestraSelecciona_CiudadTmpNUEVOTableAdapter
        '
        Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.ClearBeforeFill = True
        '
        'MuestraSelecciona_CiudadTmpCONSULTABindingSource
        '
        Me.MuestraSelecciona_CiudadTmpCONSULTABindingSource.DataMember = "MuestraSelecciona_CiudadTmpCONSULTA"
        Me.MuestraSelecciona_CiudadTmpCONSULTABindingSource.DataSource = Me.DataSetLidia
        '
        'MuestraSelecciona_CiudadTmpCONSULTATableAdapter
        '
        Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.ClearBeforeFill = True
        '
        'MuestraSeleccion_CiudadCONSULTABindingSource
        '
        Me.MuestraSeleccion_CiudadCONSULTABindingSource.DataMember = "MuestraSeleccion_CiudadCONSULTA"
        Me.MuestraSeleccion_CiudadCONSULTABindingSource.DataSource = Me.DataSetLidia
        '
        'MuestraSeleccion_CiudadCONSULTATableAdapter
        '
        Me.MuestraSeleccion_CiudadCONSULTATableAdapter.ClearBeforeFill = True
        '
        'PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource
        '
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataSource = Me.DataSetLidia
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.Position = 0
        '
        'PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
        '
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.ClearBeforeFill = True
        '
        'Borra_Seleccion_CiudadBindingSource
        '
        Me.Borra_Seleccion_CiudadBindingSource.DataMember = "Borra_Seleccion_Ciudad"
        Me.Borra_Seleccion_CiudadBindingSource.DataSource = Me.DataSetLidia
        '
        'Borra_Seleccion_CiudadTableAdapter
        '
        Me.Borra_Seleccion_CiudadTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmSelCd_CarteraPorPlaza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(920, 470)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Contratonet1)
        Me.Controls.Add(Me.Clv_Unicanet)
        Me.Controls.Add(Me.Contratonet)
        Me.Controls.Add(Me.Clv_Unicanet1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TreeView2)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelCd_CarteraPorPlaza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona la Ciudad (es)"
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSelecciona_CiudadTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSelecciona_CiudadTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccion_CiudadCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents DameClv_Session_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_Session_ServiciosTableAdapter As sofTV.DataSetarnoldoTableAdapters.DameClv_Session_ServiciosTableAdapter
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter As sofTV.DataSetLidiaTableAdapters.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
    Friend WithEvents PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter As sofTV.DataSetLidiaTableAdapters.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
    Friend WithEvents PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter As sofTV.DataSetLidiaTableAdapters.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
    Friend WithEvents MuestraSelecciona_CiudadTmpNUEVOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_CiudadTmpNUEVOTableAdapter As sofTV.DataSetLidiaTableAdapters.MuestraSelecciona_CiudadTmpNUEVOTableAdapter
    Friend WithEvents MuestraSelecciona_CiudadTmpCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_CiudadTmpCONSULTATableAdapter As sofTV.DataSetLidiaTableAdapters.MuestraSelecciona_CiudadTmpCONSULTATableAdapter
    Friend WithEvents MuestraSeleccion_CiudadCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSeleccion_CiudadCONSULTATableAdapter As sofTV.DataSetLidiaTableAdapters.MuestraSeleccion_CiudadCONSULTATableAdapter
    Friend WithEvents PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter As sofTV.DataSetLidiaTableAdapters.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
    Friend WithEvents Clv_Unicanet1 As System.Windows.Forms.TextBox
    Friend WithEvents Contratonet As System.Windows.Forms.TextBox
    Friend WithEvents Clv_Unicanet As System.Windows.Forms.TextBox
    Friend WithEvents Contratonet1 As System.Windows.Forms.TextBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Borra_Seleccion_CiudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Seleccion_CiudadTableAdapter As sofTV.DataSetLidiaTableAdapters.Borra_Seleccion_CiudadTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
