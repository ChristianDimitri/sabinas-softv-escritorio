﻿Public Class BrwLocalidad

    Private Sub BrwLocalidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        NombreTextBox.BackColor = Button4.BackColor
        NombreTextBox.ForeColor = Button4.ForeColor
        busca()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub busca()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, TextBox2.Text)
        DataGridView1.DataSource = BaseII.ConsultaDT("BuscaLocalidad")
        TextBox2.Text = ""
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            If DataGridView1.Rows.Count = 0 Then
                Exit Sub
            End If
            Clv_LocalidadLabel.Text = DataGridView1.SelectedCells(0).Value.ToString
            NombreTextBox.Text = DataGridView1.SelectedCells(1).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If TextBox2.Text = "" Then
            Exit Sub
        End If
        busca()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TextBox2.Text = "" Then
                Exit Sub
            End If
            busca()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FrmLocalidad.opcionL = "N"
        FrmLocalidad.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        FrmLocalidad.opcionL = "C"
        FrmLocalidad.clv_localidad = Clv_LocalidadLabel.Text
        FrmLocalidad.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        FrmLocalidad.opcionL = "M"
        FrmLocalidad.clv_localidad = Clv_LocalidadLabel.Text
        FrmLocalidad.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Public entraActivate As Integer = 0
    Private Sub BrwLocalidad_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If entraActivate = 1 Then
            busca()
            entraActivate = 0
        End If
    End Sub
End Class