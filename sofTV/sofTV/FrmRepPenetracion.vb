Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class FrmRepPenetracion
    Private customersByCityReport As ReportDocument
    Dim princ As String = Nothing
    Dim oprep As String = Nothing
    Dim Titulo As String = Nothing
    Private Sub ConfigureCrystalReportsNew6()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing


            If eTipoPen = 1 Then
                mySelectFormula = "Penetraci�n por Calle y Colonia Del "
                LocDesPC = "Colonia: " & LocDesPC
                LocDesPCa = "Calle: " & LocDesPCa
            ElseIf eTipoPen = 2 Then
                mySelectFormula = "Penetraci�n por Sector "
                LocDesPC = "Sector: " & LocDesPC
            End If





            reportPath = RutaReportes + "\ReportePenetracion_CalleyColonia.rpt"

            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)

            ''@op
            'customersByCityReport.SetParameterValue(0, GloClv_tipser2)
            ''@clv_colonia
            'customersByCityReport.SetParameterValue(1, Locclv_colonia)
            ''@clv_calle
            'customersByCityReport.SetParameterValue(2, Locclv_calle)
            ''@clv_txt
            'customersByCityReport.SetParameterValue(3, Locclv_txt)
            ''@Clv_Sector
            'customersByCityReport.SetParameterValue(4, eClv_Sector)
            ''Tipo
            'customersByCityReport.SetParameterValue(5, eTipoPen)
            ''Session
            'customersByCityReport.SetParameterValue(6, eClv_Session)

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@op", SqlDbType.Int, GloClv_tipser2)
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, Locclv_colonia)
            BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, Locclv_calle)
            If (Locclv_txt = Nothing) Then
                Locclv_txt = ""
            End If
            BaseII.CreateMyParameter("@CLV_TXT", SqlDbType.VarChar, CStr(Locclv_txt))
            BaseII.CreateMyParameter("@CLV_SECTOR", SqlDbType.Int, CInt(eClv_Sector))
            BaseII.CreateMyParameter("@TIPO", SqlDbType.Int, eTipoPen)
            BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.BigInt, CInt(eClv_Session))
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.BigInt, GloClvUsuario)
            Dim listatablas As New List(Of String)

            listatablas.Add("Dame_Datos_Rep_P")

            DS = BaseII.ConsultaDS("Dame_Datos_Rep_P", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)


            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & LocDesP & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia").Text = "'" & LocDesPC & "'"
            customersByCityReport.DataDefinition.FormulaFields("Calle").Text = "'" & LocDesPCa & "'"






            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eBndPenetracion = False
        Locbndpen1 = False
        Me.Close()
    End Sub

    Private Sub FrmRepPenetracion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Locbndpen1 = True And eBndPenetracion = False Then
            Locbndpen1 = False
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.Dame_Datos_Rep_PTableAdapter.Connection = CON
            'Me.Dame_Datos_Rep_PTableAdapter.Fill(Me.DataSetarnoldo.Dame_Datos_Rep_P, GloClv_tipser2, Locclv_colonia, Locclv_calle, Locclv_txt)
            'CON.Close()

            'If CInt(Me.SalidasTapsTextBox.Text) > 0 Then
            '    Me.TextBox2.Text = Math.Round(((CDbl(Me.ITextBox.Text) + CDbl(Me.DTextBox.Text) + CDbl(Me.STextBox.Text)) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
            '    Me.TextBox3.Text = Math.Round((CDbl(Me.ITextBox.Text) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
            DameDatosPenetracion(GloClv_tipser2, Locclv_colonia, Locclv_calle, eClv_Session, eClv_Sector, eTipoPen)
            ConfigureCrystalReportsNew6()
            'Else
            '    MsgBox("En esa Colonia(s) y/o Calle(s) no existen salidas en los Taps", MsgBoxStyle.Information)
            'End If
        End If

        If eBndPenetracion = True Then
            eBndPenetracion = False
            Locbndpen1 = False
            ReportePenetracion(GloClv_tipser2, Locclv_colonia, Locclv_calle, eClv_Session, eClv_Sector, eTipoPen)
            ReportePermanenciaCrystal()

        End If
    End Sub

    Private Sub FrmRepPenetracion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)

        princ = Me.ComboBox4.SelectedValue.ToString
        GloClv_tipser2 = Me.ComboBox4.SelectedValue
        Select Case princ
            Case "1"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            Case "2"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            Case "3"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
        End Select
        CON.Close()
        colorea(Me, Me.Name)
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
            GloClv_tipser2 = ComboBox4.SelectedValue
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
            Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            CON.Close()
        End If
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
            eTipoPen = Me.DataGridView1.SelectedCells(0).Value.ToString
            Titulo = Me.DataGridView1.SelectedCells(1).Value.ToString
            'If oprep = "1" Then
            My.Forms.FrmSelDatosP.Show()
            'End If
            'If oprep = "2" Then
            '    'Ottra cosa
            'End If
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DameDatosPenetracion(ByVal Op As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Calle As Integer, ByVal Clv_Session As Long, ByVal Clv_Sector As Integer, ByVal Tipo As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameDatosPenetracion", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Op", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Op
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Colonia
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Calle", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Calle
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_Session
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clv_Sector", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Clv_Sector
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Tipo", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Tipo
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Contratados", SqlDbType.BigInt)
        parametro7.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@Instalados", SqlDbType.BigInt)
        parametro8.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Desconectados", SqlDbType.BigInt)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Suspendidos", SqlDbType.BigInt)
        parametro10.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro10)

        Dim parametro11 As New SqlParameter("@Baja", SqlDbType.BigInt)
        parametro11.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro11)

        Dim parametro12 As New SqlParameter("@FueraA", SqlDbType.BigInt)
        parametro12.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro12)

        Dim parametro13 As New SqlParameter("@SalidaTap", SqlDbType.BigInt)
        parametro13.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro13)

        Dim parametro14 As New SqlParameter("@TotClientes", SqlDbType.BigInt)
        parametro14.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro14)

        Dim parametro15 As New SqlParameter("@TotTotal", SqlDbType.BigInt)
        parametro15.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro15)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Me.CTextBox.Text = parametro7.Value.ToString
            Me.ITextBox.Text = parametro8.Value.ToString
            Me.DTextBox.Text = parametro9.Value.ToString
            Me.STextBox.Text = parametro10.Value.ToString
            Me.BTextBox.Text = parametro11.Value.ToString
            Me.FTextBox.Text = parametro12.Value.ToString
            Me.SalidasTapsTextBox.Text = parametro13.Value.ToString
            Me.TotalTextBox.Text = parametro15.Value.ToString

            If IsNumeric(Me.SalidasTapsTextBox.Text) = True Then
                If CLng(Me.SalidasTapsTextBox.Text) > 0 Then
                    Me.TextBox2.Text = Math.Round(((CDbl(Me.ITextBox.Text) + CDbl(Me.DTextBox.Text) + CDbl(Me.STextBox.Text)) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
                    Me.TextBox3.Text = Math.Round((CDbl(Me.ITextBox.Text) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ReportePenetracion(ByVal Op As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Calle As Integer, ByVal Clv_Session As Long, ByVal Clv_Sector As Integer, ByVal Tipo As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ReportePenetracion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Op", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Op
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Colonia
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Calle", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Calle
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_Session
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clv_Sector", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Clv_Sector
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Tipo", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Tipo
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@ClvUsuario", SqlDbType.Int)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = GloClvUsuario
        comando.Parameters.Add(parametro7)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                Me.SalidasTapsTextBox.Text = reader(3).ToString()
                Me.TextBox3.Text = reader(4).ToString()
                Me.TextBox2.Text = reader(5).ToString()
                Me.CTextBox.Text = reader(6).ToString()
                Me.ITextBox.Text = reader(7).ToString()
                Me.DTextBox.Text = reader(8).ToString()
                Me.STextBox.Text = reader(9).ToString()
                Me.FTextBox.Text = reader(10).ToString()
                Me.BTextBox.Text = reader(11).ToString()
                'reader(12).ToString()
                Me.TotalTextBox.Text = reader(13).ToString()
                Exit Sub
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ReportePermanenciaCrystal()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing


            'If eTipoPen = 1 Then
            '    mySelectFormula = "Penetraci�n por Calle y Colonia Del "
            '    LocDesPC = "Colonia: " & LocDesPC
            '    LocDesPCa = "Calle: " & LocDesPCa
            'ElseIf eTipoPen = 2 Then
            '    mySelectFormula = "Penetraci�n por Sector "
            '    LocDesPC = "Sector: " & LocDesPC
            'End If

            reportPath = RutaReportes + "\ReportePermanencia.rpt"

            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)

            ''@op
            'customersByCityReport.SetParameterValue(0, GloClv_tipser2)
            ''@clv_colonia
            'customersByCityReport.SetParameterValue(1, Locclv_colonia)
            ''@clv_calle
            'customersByCityReport.SetParameterValue(2, Locclv_calle)
            ''Session
            'customersByCityReport.SetParameterValue(3, eClv_Session)
            ''@Clv_Sector
            'customersByCityReport.SetParameterValue(4, eClv_Sector)
            ''Tipo
            'customersByCityReport.SetParameterValue(5, eTipoPen)

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@op", SqlDbType.Int, GloClv_tipser2)
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, Locclv_colonia)
            BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, Locclv_calle)
            BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.BigInt, CInt(eClv_Session))
            BaseII.CreateMyParameter("@CLV_SECTOR", SqlDbType.Int, CInt(eClv_Sector))
            BaseII.CreateMyParameter("@TIPO", SqlDbType.Int, eTipoPen)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            Dim listatablas As New List(Of String)

            listatablas.Add("ReportePenetracion")

            DS = BaseII.ConsultaDS("ReportePenetracion", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)




            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & LocDesP & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Colonia").Text = "'" & LocDesPC & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Calle").Text = "'" & LocDesPCa & "'"


            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



End Class