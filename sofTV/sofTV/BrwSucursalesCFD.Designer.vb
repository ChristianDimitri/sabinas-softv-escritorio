﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwSucursalesCFD
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bnSerieBus = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.tbSerieBus = New System.Windows.Forms.TextBox()
        Me.dgwSerie = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Serie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Calle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Colonia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.tbCalleBus = New System.Windows.Forms.TextBox()
        Me.tbColoniaBus = New System.Windows.Forms.TextBox()
        Me.CMBLabel12 = New System.Windows.Forms.Label()
        Me.CMBLabel23 = New System.Windows.Forms.Label()
        Me.bnColoniaBus = New System.Windows.Forms.Button()
        Me.bnCalleBus = New System.Windows.Forms.Button()
        Me.bnNuevo = New System.Windows.Forms.Button()
        Me.bnConsultar = New System.Windows.Forms.Button()
        Me.bnModificar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        CType(Me.dgwSerie, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnSerieBus
        '
        Me.bnSerieBus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSerieBus.Location = New System.Drawing.Point(21, 133)
        Me.bnSerieBus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnSerieBus.Name = "bnSerieBus"
        Me.bnSerieBus.Size = New System.Drawing.Size(100, 28)
        Me.bnSerieBus.TabIndex = 1
        Me.bnSerieBus.Text = "&Buscar"
        Me.bnSerieBus.UseVisualStyleBackColor = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(16, 11)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(146, 29)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar por:"
        '
        'tbSerieBus
        '
        Me.tbSerieBus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSerieBus.Location = New System.Drawing.Point(21, 100)
        Me.tbSerieBus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbSerieBus.Name = "tbSerieBus"
        Me.tbSerieBus.Size = New System.Drawing.Size(257, 24)
        Me.tbSerieBus.TabIndex = 0
        '
        'dgwSerie
        '
        Me.dgwSerie.AllowUserToAddRows = False
        Me.dgwSerie.AllowUserToDeleteRows = False
        Me.dgwSerie.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgwSerie.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgwSerie.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwSerie.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Serie, Me.Calle, Me.Colonia})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgwSerie.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgwSerie.Location = New System.Drawing.Point(308, 11)
        Me.dgwSerie.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgwSerie.Name = "dgwSerie"
        Me.dgwSerie.ReadOnly = True
        Me.dgwSerie.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgwSerie.Size = New System.Drawing.Size(797, 820)
        Me.dgwSerie.TabIndex = 3
        Me.dgwSerie.TabStop = False
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        '
        'Serie
        '
        Me.Serie.DataPropertyName = "Serie"
        Me.Serie.HeaderText = "Serie"
        Me.Serie.Name = "Serie"
        Me.Serie.ReadOnly = True
        Me.Serie.Width = 80
        '
        'Calle
        '
        Me.Calle.DataPropertyName = "Calle"
        Me.Calle.HeaderText = "Calle"
        Me.Calle.Name = "Calle"
        Me.Calle.ReadOnly = True
        Me.Calle.Width = 230
        '
        'Colonia
        '
        Me.Colonia.DataPropertyName = "Colonia"
        Me.Colonia.HeaderText = "Colonia"
        Me.Colonia.Name = "Colonia"
        Me.Colonia.ReadOnly = True
        Me.Colonia.Width = 230
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(17, 78)
        Me.CMBLabel2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(52, 18)
        Me.CMBLabel2.TabIndex = 4
        Me.CMBLabel2.Text = "Serie:"
        '
        'tbCalleBus
        '
        Me.tbCalleBus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCalleBus.Location = New System.Drawing.Point(21, 235)
        Me.tbCalleBus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCalleBus.Name = "tbCalleBus"
        Me.tbCalleBus.Size = New System.Drawing.Size(257, 24)
        Me.tbCalleBus.TabIndex = 2
        '
        'tbColoniaBus
        '
        Me.tbColoniaBus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbColoniaBus.Location = New System.Drawing.Point(21, 369)
        Me.tbColoniaBus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbColoniaBus.Name = "tbColoniaBus"
        Me.tbColoniaBus.Size = New System.Drawing.Size(257, 24)
        Me.tbColoniaBus.TabIndex = 4
        '
        'CMBLabel12
        '
        Me.CMBLabel12.AutoSize = True
        Me.CMBLabel12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel12.Location = New System.Drawing.Point(17, 213)
        Me.CMBLabel12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel12.Name = "CMBLabel12"
        Me.CMBLabel12.Size = New System.Drawing.Size(51, 18)
        Me.CMBLabel12.TabIndex = 7
        Me.CMBLabel12.Text = "Calle:"
        '
        'CMBLabel23
        '
        Me.CMBLabel23.AutoSize = True
        Me.CMBLabel23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel23.Location = New System.Drawing.Point(17, 347)
        Me.CMBLabel23.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel23.Name = "CMBLabel23"
        Me.CMBLabel23.Size = New System.Drawing.Size(71, 18)
        Me.CMBLabel23.TabIndex = 8
        Me.CMBLabel23.Text = "Colonia:"
        '
        'bnColoniaBus
        '
        Me.bnColoniaBus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnColoniaBus.Location = New System.Drawing.Point(21, 402)
        Me.bnColoniaBus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnColoniaBus.Name = "bnColoniaBus"
        Me.bnColoniaBus.Size = New System.Drawing.Size(100, 28)
        Me.bnColoniaBus.TabIndex = 5
        Me.bnColoniaBus.Text = "&Buscar"
        Me.bnColoniaBus.UseVisualStyleBackColor = True
        '
        'bnCalleBus
        '
        Me.bnCalleBus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnCalleBus.Location = New System.Drawing.Point(21, 268)
        Me.bnCalleBus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnCalleBus.Name = "bnCalleBus"
        Me.bnCalleBus.Size = New System.Drawing.Size(100, 28)
        Me.bnCalleBus.TabIndex = 3
        Me.bnCalleBus.Text = "&Buscar"
        Me.bnCalleBus.UseVisualStyleBackColor = True
        '
        'bnNuevo
        '
        Me.bnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNuevo.Location = New System.Drawing.Point(1147, 11)
        Me.bnNuevo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnNuevo.Name = "bnNuevo"
        Me.bnNuevo.Size = New System.Drawing.Size(181, 44)
        Me.bnNuevo.TabIndex = 6
        Me.bnNuevo.Text = "&NUEVO"
        Me.bnNuevo.UseVisualStyleBackColor = True
        '
        'bnConsultar
        '
        Me.bnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnConsultar.Location = New System.Drawing.Point(1147, 63)
        Me.bnConsultar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnConsultar.Name = "bnConsultar"
        Me.bnConsultar.Size = New System.Drawing.Size(181, 44)
        Me.bnConsultar.TabIndex = 7
        Me.bnConsultar.Text = "&CONSULTAR"
        Me.bnConsultar.UseVisualStyleBackColor = True
        '
        'bnModificar
        '
        Me.bnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnModificar.Location = New System.Drawing.Point(1147, 117)
        Me.bnModificar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnModificar.Name = "bnModificar"
        Me.bnModificar.Size = New System.Drawing.Size(181, 44)
        Me.bnModificar.TabIndex = 8
        Me.bnModificar.Text = "&MODIFICAR"
        Me.bnModificar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(1147, 839)
        Me.bnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(181, 44)
        Me.bnSalir.TabIndex = 9
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'BrwSucursalesCFD
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1344, 898)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnModificar)
        Me.Controls.Add(Me.bnConsultar)
        Me.Controls.Add(Me.bnNuevo)
        Me.Controls.Add(Me.bnCalleBus)
        Me.Controls.Add(Me.bnColoniaBus)
        Me.Controls.Add(Me.CMBLabel23)
        Me.Controls.Add(Me.CMBLabel12)
        Me.Controls.Add(Me.tbColoniaBus)
        Me.Controls.Add(Me.tbCalleBus)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.dgwSerie)
        Me.Controls.Add(Me.tbSerieBus)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.bnSerieBus)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwSucursalesCFD"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Series SAT"
        CType(Me.dgwSerie, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnSerieBus As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents tbSerieBus As System.Windows.Forms.TextBox
    Friend WithEvents dgwSerie As System.Windows.Forms.DataGridView
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents tbCalleBus As System.Windows.Forms.TextBox
    Friend WithEvents tbColoniaBus As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel12 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel23 As System.Windows.Forms.Label
    Friend WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Serie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Calle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Colonia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bnColoniaBus As System.Windows.Forms.Button
    Friend WithEvents bnCalleBus As System.Windows.Forms.Button
    Friend WithEvents bnNuevo As System.Windows.Forms.Button
    Friend WithEvents bnConsultar As System.Windows.Forms.Button
    Friend WithEvents bnModificar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
End Class
