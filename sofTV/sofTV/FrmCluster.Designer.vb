﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCluster
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCluster))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Clv_TxtTextBox = New System.Windows.Forms.TextBox()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.eliminar = New System.Windows.Forms.ToolStripButton()
        Me.guardar = New System.Windows.Forms.ToolStripButton()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.tbclv_cluster = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.SectorComboBox = New System.Windows.Forms.ComboBox()
        Me.dgvrelsector = New System.Windows.Forms.DataGridView()
        Me.Clv_Sector = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_SectorTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        DescripcionLabel = New System.Windows.Forms.Label()
        Clv_TxtLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvrelsector, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.Location = New System.Drawing.Point(175, 133)
        DescripcionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(122, 20)
        DescripcionLabel.TabIndex = 4
        DescripcionLabel.Text = "Descripción :"
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.Location = New System.Drawing.Point(232, 102)
        Clv_TxtLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(68, 20)
        Clv_TxtLabel.TabIndex = 2
        Clv_TxtLabel.Text = "Clave :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Location = New System.Drawing.Point(119, 47)
        NombreLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(80, 20)
        NombreLabel.TabIndex = 11
        NombreLabel.Text = "Nombre:"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Clv_TxtTextBox)
        Me.GroupBox1.Controls.Add(Me.BindingNavigator1)
        Me.GroupBox1.Controls.Add(Me.DescripcionTextBox)
        Me.GroupBox1.Controls.Add(DescripcionLabel)
        Me.GroupBox1.Controls.Add(Clv_TxtLabel)
        Me.GroupBox1.Controls.Add(Me.tbclv_cluster)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(16, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(808, 199)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cluster"
        '
        'Clv_TxtTextBox
        '
        Me.Clv_TxtTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_TxtTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_TxtTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtTextBox.Location = New System.Drawing.Point(328, 94)
        Me.Clv_TxtTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TxtTextBox.Name = "Clv_TxtTextBox"
        Me.Clv_TxtTextBox.Size = New System.Drawing.Size(169, 29)
        Me.Clv_TxtTextBox.TabIndex = 0
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.eliminar, Me.guardar})
        Me.BindingNavigator1.Location = New System.Drawing.Point(4, 23)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(800, 28)
        Me.BindingNavigator1.TabIndex = 2
        Me.BindingNavigator1.TabStop = True
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'eliminar
        '
        Me.eliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.eliminar.Image = CType(resources.GetObject("eliminar.Image"), System.Drawing.Image)
        Me.eliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.eliminar.Name = "eliminar"
        Me.eliminar.Size = New System.Drawing.Size(102, 25)
        Me.eliminar.Text = "&ELIMINAR"
        '
        'guardar
        '
        Me.guardar.Image = CType(resources.GetObject("guardar.Image"), System.Drawing.Image)
        Me.guardar.Name = "guardar"
        Me.guardar.Size = New System.Drawing.Size(121, 25)
        Me.guardar.Text = "&GUARDAR"
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(328, 130)
        Me.DescripcionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(382, 29)
        Me.DescripcionTextBox.TabIndex = 1
        '
        'tbclv_cluster
        '
        Me.tbclv_cluster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbclv_cluster.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbclv_cluster.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbclv_cluster.Location = New System.Drawing.Point(328, 130)
        Me.tbclv_cluster.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbclv_cluster.Name = "tbclv_cluster"
        Me.tbclv_cluster.Size = New System.Drawing.Size(169, 29)
        Me.tbclv_cluster.TabIndex = 5
        Me.tbclv_cluster.Text = "0"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(NombreLabel)
        Me.GroupBox2.Controls.Add(Me.SectorComboBox)
        Me.GroupBox2.Controls.Add(Me.dgvrelsector)
        Me.GroupBox2.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.GroupBox2.Controls.Add(Me.Clv_SectorTextBox)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 222)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(808, 570)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Relación de Sectores con el Cluster"
        '
        'SectorComboBox
        '
        Me.SectorComboBox.DisplayMember = "Nombre"
        Me.SectorComboBox.FormattingEnabled = True
        Me.SectorComboBox.Location = New System.Drawing.Point(123, 70)
        Me.SectorComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SectorComboBox.Name = "SectorComboBox"
        Me.SectorComboBox.Size = New System.Drawing.Size(533, 28)
        Me.SectorComboBox.TabIndex = 12
        Me.SectorComboBox.ValueMember = "Clv_Colonia"
        '
        'dgvrelsector
        '
        Me.dgvrelsector.AllowUserToAddRows = False
        Me.dgvrelsector.AllowUserToDeleteRows = False
        Me.dgvrelsector.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvrelsector.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvrelsector.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Sector, Me.descripcion})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvrelsector.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvrelsector.Location = New System.Drawing.Point(123, 130)
        Me.dgvrelsector.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvrelsector.Name = "dgvrelsector"
        Me.dgvrelsector.ReadOnly = True
        Me.dgvrelsector.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvrelsector.Size = New System.Drawing.Size(535, 415)
        Me.dgvrelsector.TabIndex = 7
        Me.dgvrelsector.TabStop = False
        '
        'Clv_Sector
        '
        Me.Clv_Sector.DataPropertyName = "Clv_Sector"
        Me.Clv_Sector.HeaderText = "Clv_Sector"
        Me.Clv_Sector.Name = "Clv_Sector"
        Me.Clv_Sector.ReadOnly = True
        Me.Clv_Sector.Visible = False
        '
        'descripcion
        '
        Me.descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.descripcion.DataPropertyName = "Descripcion"
        Me.descripcion.HeaderText = "Sector"
        Me.descripcion.Name = "descripcion"
        Me.descripcion.ReadOnly = True
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(315, 219)
        Me.Clv_ColoniaTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.ReadOnly = True
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(12, 26)
        Me.Clv_ColoniaTextBox.TabIndex = 11
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'Clv_SectorTextBox
        '
        Me.Clv_SectorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_SectorTextBox.Location = New System.Drawing.Point(293, 220)
        Me.Clv_SectorTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_SectorTextBox.Name = "Clv_SectorTextBox"
        Me.Clv_SectorTextBox.ReadOnly = True
        Me.Clv_SectorTextBox.Size = New System.Drawing.Size(13, 26)
        Me.Clv_SectorTextBox.TabIndex = 1
        Me.Clv_SectorTextBox.TabStop = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(679, 59)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 41)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Agregar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(679, 107)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(100, 41)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Eliminar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmCluster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(847, 802)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmCluster"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cluster"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvrelsector, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Clv_TxtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents eliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents guardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents tbclv_cluster As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents SectorComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents dgvrelsector As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SectorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Clv_Sector As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
