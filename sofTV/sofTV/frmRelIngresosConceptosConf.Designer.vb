﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRelIngresosConceptosConf
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label20 As System.Windows.Forms.Label
        Dim MOTCANLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Me.ComboBoxDis = New System.Windows.Forms.ComboBox()
        Me.txtCuenta = New System.Windows.Forms.TextBox()
        Me.ComboBoxPlaza = New System.Windows.Forms.ComboBox()
        Me.ComboBoxServicio = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Clv_Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Plaza = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cuenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Posicion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.txtPosicion = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.RadioServicio = New System.Windows.Forms.RadioButton()
        Me.RadioOtro = New System.Windows.Forms.RadioButton()
        Me.txtOtro = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbOtros = New System.Windows.Forms.RadioButton()
        Me.rbEfectivo = New System.Windows.Forms.RadioButton()
        Label20 = New System.Windows.Forms.Label()
        MOTCANLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Label20.Location = New System.Drawing.Point(27, 132)
        Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(100, 18)
        Label20.TabIndex = 202
        Label20.Text = "Distribuidor:"
        '
        'MOTCANLabel
        '
        MOTCANLabel.AutoSize = True
        MOTCANLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MOTCANLabel.ForeColor = System.Drawing.Color.LightSlateGray
        MOTCANLabel.Location = New System.Drawing.Point(27, 439)
        MOTCANLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        MOTCANLabel.Name = "MOTCANLabel"
        MOTCANLabel.Size = New System.Drawing.Size(71, 18)
        MOTCANLabel.TabIndex = 201
        MOTCANLabel.Text = "Cuenta :"
        MOTCANLabel.UseWaitCursor = True
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(27, 204)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(55, 18)
        Label1.TabIndex = 204
        Label1.Text = "Plaza:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(27, 513)
        Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(84, 18)
        Label4.TabIndex = 215
        Label4.Text = "Posición :"
        Label4.UseWaitCursor = True
        '
        'ComboBoxDis
        '
        Me.ComboBoxDis.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxDis.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxDis.FormattingEnabled = True
        Me.ComboBoxDis.Location = New System.Drawing.Point(27, 154)
        Me.ComboBoxDis.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxDis.Name = "ComboBoxDis"
        Me.ComboBoxDis.Size = New System.Drawing.Size(315, 26)
        Me.ComboBoxDis.TabIndex = 0
        '
        'txtCuenta
        '
        Me.txtCuenta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuenta.Location = New System.Drawing.Point(31, 462)
        Me.txtCuenta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCuenta.Name = "txtCuenta"
        Me.txtCuenta.Size = New System.Drawing.Size(311, 24)
        Me.txtCuenta.TabIndex = 3
        Me.txtCuenta.TabStop = False
        '
        'ComboBoxPlaza
        '
        Me.ComboBoxPlaza.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxPlaza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxPlaza.FormattingEnabled = True
        Me.ComboBoxPlaza.Location = New System.Drawing.Point(27, 226)
        Me.ComboBoxPlaza.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxPlaza.Name = "ComboBoxPlaza"
        Me.ComboBoxPlaza.Size = New System.Drawing.Size(315, 26)
        Me.ComboBoxPlaza.TabIndex = 1
        '
        'ComboBoxServicio
        '
        Me.ComboBoxServicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxServicio.FormattingEnabled = True
        Me.ComboBoxServicio.Location = New System.Drawing.Point(31, 305)
        Me.ComboBoxServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxServicio.Name = "ComboBoxServicio"
        Me.ComboBoxServicio.Size = New System.Drawing.Size(311, 26)
        Me.ComboBoxServicio.TabIndex = 2
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(215, 590)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(128, 37)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "&Quitar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(28, 590)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(128, 37)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "&Agregar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Concepto, Me.Clv_Plaza, Me.IdCompania, Me.Descripcion, Me.Clv_Servicio, Me.Clave, Me.Cuenta, Me.Posicion})
        Me.DataGridView1.Location = New System.Drawing.Point(369, 15)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(875, 651)
        Me.DataGridView1.TabIndex = 210
        '
        'Clv_Concepto
        '
        Me.Clv_Concepto.DataPropertyName = "Clv_Concepto"
        Me.Clv_Concepto.HeaderText = "Clv_Concepto"
        Me.Clv_Concepto.Name = "Clv_Concepto"
        Me.Clv_Concepto.ReadOnly = True
        Me.Clv_Concepto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Clv_Concepto.Visible = False
        '
        'Clv_Plaza
        '
        Me.Clv_Plaza.DataPropertyName = "Nombre"
        Me.Clv_Plaza.HeaderText = "Distribuidor"
        Me.Clv_Plaza.Name = "Clv_Plaza"
        Me.Clv_Plaza.ReadOnly = True
        Me.Clv_Plaza.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Clv_Plaza.Width = 120
        '
        'IdCompania
        '
        Me.IdCompania.DataPropertyName = "razon_social"
        Me.IdCompania.HeaderText = "Plaza"
        Me.IdCompania.Name = "IdCompania"
        Me.IdCompania.ReadOnly = True
        Me.IdCompania.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Concepto"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Descripcion.Width = 180
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.ReadOnly = True
        Me.Clv_Servicio.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Clv_Servicio.Visible = False
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "Clave"
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.ReadOnly = True
        Me.Clave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Clave.Visible = False
        '
        'Cuenta
        '
        Me.Cuenta.DataPropertyName = "Cuenta"
        Me.Cuenta.HeaderText = "Cuenta"
        Me.Cuenta.Name = "Cuenta"
        Me.Cuenta.ReadOnly = True
        Me.Cuenta.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Cuenta.Width = 180
        '
        'Posicion
        '
        Me.Posicion.DataPropertyName = "Posicion"
        Me.Posicion.HeaderText = "Posición"
        Me.Posicion.Name = "Posicion"
        Me.Posicion.ReadOnly = True
        Me.Posicion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Posicion.Width = 70
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(405, 434)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(43, 37)
        Me.Button3.TabIndex = 211
        Me.Button3.Text = "ᴧ"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkRed
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(407, 490)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(41, 37)
        Me.Button4.TabIndex = 212
        Me.Button4.Text = "v"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'txtPosicion
        '
        Me.txtPosicion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPosicion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPosicion.Location = New System.Drawing.Point(31, 535)
        Me.txtPosicion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtPosicion.Name = "txtPosicion"
        Me.txtPosicion.Size = New System.Drawing.Size(107, 24)
        Me.txtPosicion.TabIndex = 4
        Me.txtPosicion.TabStop = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(1069, 673)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(175, 44)
        Me.Button5.TabIndex = 216
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'RadioServicio
        '
        Me.RadioServicio.AutoSize = True
        Me.RadioServicio.Checked = True
        Me.RadioServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.RadioServicio.ForeColor = System.Drawing.Color.LightSlateGray
        Me.RadioServicio.Location = New System.Drawing.Point(31, 274)
        Me.RadioServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioServicio.Name = "RadioServicio"
        Me.RadioServicio.Size = New System.Drawing.Size(90, 22)
        Me.RadioServicio.TabIndex = 217
        Me.RadioServicio.TabStop = True
        Me.RadioServicio.Text = "Servicio"
        Me.RadioServicio.UseVisualStyleBackColor = True
        '
        'RadioOtro
        '
        Me.RadioOtro.AutoSize = True
        Me.RadioOtro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.RadioOtro.ForeColor = System.Drawing.Color.LightSlateGray
        Me.RadioOtro.Location = New System.Drawing.Point(31, 352)
        Me.RadioOtro.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioOtro.Name = "RadioOtro"
        Me.RadioOtro.Size = New System.Drawing.Size(63, 22)
        Me.RadioOtro.TabIndex = 218
        Me.RadioOtro.Text = "Otro"
        Me.RadioOtro.UseVisualStyleBackColor = True
        '
        'txtOtro
        '
        Me.txtOtro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtOtro.Enabled = False
        Me.txtOtro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtro.Location = New System.Drawing.Point(27, 396)
        Me.txtOtro.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtOtro.Name = "txtOtro"
        Me.txtOtro.Size = New System.Drawing.Size(311, 24)
        Me.txtOtro.TabIndex = 219
        Me.txtOtro.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbOtros)
        Me.GroupBox1.Controls.Add(Me.rbEfectivo)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(16, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(345, 97)
        Me.GroupBox1.TabIndex = 220
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cuentas para Medio de Pago"
        '
        'rbOtros
        '
        Me.rbOtros.AutoSize = True
        Me.rbOtros.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbOtros.Location = New System.Drawing.Point(191, 47)
        Me.rbOtros.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.rbOtros.Name = "rbOtros"
        Me.rbOtros.Size = New System.Drawing.Size(120, 22)
        Me.rbOtros.TabIndex = 222
        Me.rbOtros.Text = "Otros medios"
        Me.rbOtros.UseVisualStyleBackColor = True
        '
        'rbEfectivo
        '
        Me.rbEfectivo.AutoSize = True
        Me.rbEfectivo.Checked = True
        Me.rbEfectivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbEfectivo.Location = New System.Drawing.Point(16, 47)
        Me.rbEfectivo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.rbEfectivo.Name = "rbEfectivo"
        Me.rbEfectivo.Size = New System.Drawing.Size(142, 22)
        Me.rbEfectivo.TabIndex = 221
        Me.rbEfectivo.TabStop = True
        Me.rbEfectivo.Text = "Efectivo y Tarjeta"
        Me.rbEfectivo.UseVisualStyleBackColor = True
        '
        'frmRelIngresosConceptosConf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1265, 731)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtOtro)
        Me.Controls.Add(Me.RadioOtro)
        Me.Controls.Add(Me.RadioServicio)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.txtPosicion)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ComboBoxServicio)
        Me.Controls.Add(Me.ComboBoxPlaza)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.ComboBoxDis)
        Me.Controls.Add(Label20)
        Me.Controls.Add(Me.txtCuenta)
        Me.Controls.Add(MOTCANLabel)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button4)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmRelIngresosConceptosConf"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cuentas contables y conceptos"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ComboBoxDis As System.Windows.Forms.ComboBox
    Friend WithEvents txtCuenta As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxPlaza As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxServicio As System.Windows.Forms.ComboBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents txtPosicion As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Clv_Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Plaza As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cuenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Posicion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RadioServicio As System.Windows.Forms.RadioButton
    Friend WithEvents RadioOtro As System.Windows.Forms.RadioButton
    Friend WithEvents txtOtro As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbOtros As System.Windows.Forms.RadioButton
    Friend WithEvents rbEfectivo As System.Windows.Forms.RadioButton
End Class
