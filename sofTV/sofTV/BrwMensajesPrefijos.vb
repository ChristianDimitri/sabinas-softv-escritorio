Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class BrwMensajesPrefijos

    Private Sub BrwMensajesPrefijos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.AbcGrid(1, 0, "", 0)
        Me.MuestraTipoMensajes()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub btnBuscaNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscaNombre.Click
        Me.AbcGrid(3, 0, Me.TextBox1.Text.ToString, 0)
    End Sub

    Private Sub btnFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFecha.Click
        Me.AbcGrid(2, Me.ComboBox1.SelectedValue, "", 0)
    End Sub

    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        FrmPrefijoMsj.actualiza = False
        FrmPrefijoMsj.ShowDialog()
        Me.AbcGrid(1, 0, "", 0)
        rSession = 0
        rMensajes = False
    End Sub

    Private Sub btnModifica_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModifica.Click
        If Me.gvMensajes.Rows.Count > 0 Then
            Dim clv As Integer = Integer.Parse(Me.gvMensajes.Rows(Integer.Parse(Me.gvMensajes.CurrentRow.Index.ToString)).Cells("ClvMensaje").Value.ToString)
            FrmPrefijoMsj.actualizar(clv)
            FrmPrefijoMsj.actualiza = True
            FrmPrefijoMsj.ShowDialog()
            Me.AbcGrid(1, 0, "", 0)
        Else
            MsgBox("No existen registros", MsgBoxStyle.Information)
        End If

    End Sub

    Private Sub btnElimina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnElimina.Click
        If Me.gvMensajes.Rows.Count > 0 Then
            Dim clv As Integer = Integer.Parse(Me.gvMensajes.Rows(Integer.Parse(Me.gvMensajes.CurrentRow.Index.ToString)).Cells("ClvMensaje").Value.ToString)
            Me.AbcGrid(4, 0, "", clv)
            Me.AbcGrid(1, 0, "", 0)
        Else
            MsgBox("No hay mas filas para eliminar", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub AbcGrid(ByVal op As Integer, ByVal tipo As Integer, ByVal descripcion As String, ByVal clvMensaje As Integer)
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("MuestraCatalogoMensajesPer", con)
        com.CommandTimeout = 0
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Op", op))
        com.Parameters.Add(New SqlParameter("@TipoMensaje", tipo))
        com.Parameters.Add(New SqlParameter("@Descripcion", descripcion))
        com.Parameters.Add(New SqlParameter("@ClvMensaje", clvMensaje))
        Dim tabla As DataTable = New DataTable
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Dim bs As BindingSource = New BindingSource()
        Try
            con.Open()
            da.Fill(tabla)
            'bs.DataSource = tabla
            Me.gvMensajes.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        'If tabla.Rows.Count > 0 Then
        '    Me.gvMensajes.DataSource = bs
        'End If

    End Sub

    Private Sub MuestraTipoMensajes()
        Dim con As New SqlConnection(MiConexion)
        Dim query As String = "Exec MuestraTipoMensajes "
        Dim dataadapter As New SqlDataAdapter(query, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource
        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.ComboBox1.DataSource = binding
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub gvMensajes_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gvMensajes.SelectionChanged
        Try
            Dim row As Integer = Integer.Parse(Me.gvMensajes.CurrentRow.Index.ToString)
            Me.lblDescripcion.Text = Me.gvMensajes.Rows(row).Cells("Descripcion").Value.ToString
            Me.lblTipoMensaje.Text = Me.gvMensajes.Rows(row).Cells("Tipo_Mensaje").Value.ToString
            Me.TextBox3.Text = Me.gvMensajes.Rows(row).Cells("Mensaje").Value.ToString
        Catch ex As Exception

        End Try
    End Sub

End Class