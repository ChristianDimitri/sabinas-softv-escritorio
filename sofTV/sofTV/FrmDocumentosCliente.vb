﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Xml
Imports System.Net
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Public Class FrmDocumentosCliente
    Public tipopersona As Integer = 0
    Public clv_vendedor As Integer = 0
    Private Sub FrmDocumentosCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        ComboBoxCompanias.ValueMember = "IdDocumento"
        ComboBoxCompanias.DisplayMember = "Nombre"
        If tipopersona = 2 Then
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("DameDocumentosVendedor")
            cbRecibido.Visible = False
            cbRevisado.Visible = False
        Else
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, Contrato)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("DameDocumentos")
            noentra = True
            llenaOpciones()
            noentra = False
        End If
        LlenaGrid()
        Button1.BackColor = Button2.BackColor
        Button1.ForeColor = Button2.ForeColor
        Button5.BackColor = Button2.BackColor
        Button5.ForeColor = Button2.ForeColor
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_tipousuario", SqlDbType.Int, GloTipoUsuario)
        BaseII.CreateMyParameter("@desactivar", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ValidaPerfilActivarChecksDocumentos")
        If BaseII.dicoPar("@desactivar") = 1 Then
            cbRecibido.Enabled = False
            cbRevisado.Enabled = False
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
    Dim noentra As Boolean
    Private Sub LlenaGrid()
        Try
            If tipopersona = 2 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.BigInt, clv_vendedor)
                Grid.DataSource = BaseII.ConsultaDT("DameDocumentosVendedorGrid")
            Else
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoDocumentos)
                Grid.DataSource = BaseII.ConsultaDT("DameDocumentosContrato")
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim ofd As New OpenFileDialog()
        ofd.Title = "Selecciona un archivo."
        ofd.Filter = "Images (*.jpg,*.png)|*.jpg;*.jpeg;*.png;*.JPG;*.JPEG;*.PNG|Documents (*.pdf)|*.PDF;*.pdf"
        ofd.FilterIndex = 1
        ofd.RestoreDirectory = True
        If ofd.ShowDialog() = DialogResult.OK Then
            Dim fi As FileInfo = New FileInfo(ofd.FileName)
            If ofd.FileName.Contains(".pdf") Or ofd.FileName.Contains(".PDF") Then
                If fi.Length > 1500000 Then
                    MsgBox("Archivo muy grande, no soportado por el sistema")
                    Exit Sub
                End If
                pdfViewer.Visible = True
                PictureBox1.Visible = False
                pdfViewer.Navigate(ofd.FileName)
                tbNombre.Text = ofd.FileName
            ElseIf ofd.FileName.Contains(".JPG") Or ofd.FileName.Contains(".jpg") Then
                If fi.Length > 1500000 Then
                    MsgBox("Archivo muy grande, no soportado por el sistema")
                    Exit Sub
                End If
                pdfViewer.Visible = False
                PictureBox1.Visible = True
                PictureBox1.Image = System.Drawing.Bitmap.FromFile(ofd.FileName)
                tbNombre.Text = ofd.FileName
            Else
                MsgBox("Formato no soportado por el sistema.")
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If tbNombre.Text = "" Then
            MsgBox("Debe seleccionar un archivo.")
            Exit Sub
        End If
        Try
            'Dim argbit As Byte() = File.ReadAllBytes((tbNombre.Text))
            'Dim arch As Archivo = New Archivo()
            'arch.imagen = argbit
            'Dim xsSubmit As System.Xml.Serialization.XmlSerializer = New System.Xml.Serialization.XmlSerializer(arch.GetType())
            'Dim sww As StringWriter = New StringWriter()
            'Dim writer As XmlWriter = XmlWriter.Create(sww)
            'xsSubmit.Serialize(writer, arch)
            'Dim xml As String = sww.ToString()
            'Fin Prueba 2
            'MsgBox(xmlstring)
            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()
            BaseII.limpiaParametros()
            If tipopersona = 2 Then
                If pdfViewer.Visible Then
                    Dim fInfo As New FileInfo(tbNombre.Text)
                    Dim numBytes As Long = fInfo.Length
                    Dim fStream As New FileStream(tbNombre.Text, FileMode.Open, FileAccess.Read)
                    Dim br As New BinaryReader(fStream)
                    Dim data As Byte() = br.ReadBytes(CInt(numBytes))
                    br.Close()
                    fStream.Close()
                    BaseII.CreateMyParameter("@iddocumento", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.Int, clv_vendedor)
                    BaseII.CreateMyParameter("@archivo", SqlDbType.VarBinary, data)
                    BaseII.Inserta("GuardaDocumentoPDFVendedor")
                Else
                    PictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
                    BaseII.CreateMyParameter("@iddocumento", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.Int, clv_vendedor)
                    BaseII.CreateMyParameter("@archivo", SqlDbType.Image, ms.GetBuffer())
                    BaseII.Inserta("GuardaDocumentoImageVendedor")
                End If
            Else
                If pdfViewer.Visible Then
                    'Dim fStream As FileStream = File.OpenRead(tbNombre.Text)
                    'Dim contents(fStream.Length) As Byte
                    'fStream.Read(contents, 0, fStream.Length)
                    'fStream.Close()
                    'opcion 2
                    Dim fInfo As New FileInfo(tbNombre.Text)
                    Dim numBytes As Long = fInfo.Length
                    Dim fStream As New FileStream(tbNombre.Text, FileMode.Open, FileAccess.Read)
                    Dim br As New BinaryReader(fStream)
                    Dim data As Byte() = br.ReadBytes(CInt(numBytes))
                    br.Close()
                    fStream.Close()
                    BaseII.CreateMyParameter("@iddocumento", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@contrato", SqlDbType.Int, ContratoDocumentos)
                    BaseII.CreateMyParameter("@archivo", SqlDbType.VarBinary, data)
                    BaseII.Inserta("GuardaDocumentoPDF")
                Else
                    PictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
                    BaseII.CreateMyParameter("@iddocumento", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@contrato", SqlDbType.Int, ContratoDocumentos)
                    BaseII.CreateMyParameter("@archivo", SqlDbType.Image, ms.GetBuffer())
                    BaseII.Inserta("GuardaDocumentoImage")
                End If
            End If
            MsgBox("Documento guardado con éxito.")
            InsertaNuevo()
            LlenaGrid()
            tbNombre.Text = ""
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub InsertaNuevo()

    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
    Public Class Archivo

        Public imagen As Byte()
    End Class
    Function Base64ToImage(ByVal base64String As String)
        MsgBox(base64String)
        Dim imageBytes As Byte() = Convert.FromBase64String(base64String)
        Dim ms As MemoryStream = New MemoryStream(imageBytes, 0, imageBytes.Length)
        ms.Write(imageBytes, 0, imageBytes.Length)
        Dim image As Image = image.FromStream(ms, True)
        Return image
    End Function

    Private Sub Grid_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid.SelectionChanged
        Try
            If Grid.SelectedCells(2).Value = 1 And Grid.Rows.Count <> 0 Then
                Dim conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim comando As New SqlCommand()
                comando.Connection = conexion
                If tipopersona = 2 Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@dimeTipoDocumento", ParameterDirection.Output, SqlDbType.Int)
                    BaseII.CreateMyParameter("@idDocumento", SqlDbType.BigInt, Grid.SelectedCells(0).Value.ToString)
                    BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.BigInt, clv_vendedor)
                    BaseII.ProcedimientoOutPut("DimeTipoDocumentoVendedor")
                    If BaseII.dicoPar("@dimeTipoDocumento") = 1 Then 'image
                        comando.CommandText = "select imagen from DocumentosClientes.dbo.DocumentosVendedores where clv_vendedor=" + clv_vendedor.ToString + " and iddocumento=" + Grid.SelectedCells(0).Value.ToString
                        Dim reader As SqlDataReader = comando.ExecuteReader
                        reader.Read()
                        Dim by(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
                        reader.GetBytes(0, 0, by, 0, by.Length)
                        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(by)
                        PictureBox1.Image = Image.FromStream(ms)
                        reader.Close()
                        PictureBox1.Visible = True
                        pdfViewer.Visible = False
                    Else
                        Dim ToSaveFileTo As String = RutaReportes + "\ReportTemp.doc"
                        comando.CommandText = "select archivo from DocumentosClientes.dbo.DocumentosPDFVendedor where clv_vendedor=" + clv_vendedor.ToString + " and iddocumento=" + Grid.SelectedCells(0).Value.ToString
                        Dim reader As SqlDataReader = comando.ExecuteReader(System.Data.CommandBehavior.Default)
                        reader.Read()
                        Dim contents(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
                        'contents = reader.GetBytes(0, 0, contents, 0, contents.Length)
                        contents = reader.GetValue(0)
                        Dim fileName As String = System.IO.Path.GetTempFileName() + ".pdf"
                        File.WriteAllBytes(fileName, contents)
                        reader.Close()
                        PictureBox1.Visible = False
                        pdfViewer.Visible = True
                        pdfViewer.Navigate(fileName)
                    End If

                Else
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@dimeTipoDocumento", ParameterDirection.Output, SqlDbType.Int)
                    BaseII.CreateMyParameter("@idDocumento", SqlDbType.BigInt, Grid.SelectedCells(0).Value.ToString)
                    BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoDocumentos)
                    BaseII.ProcedimientoOutPut("DimeTipoDocumento")
                    If BaseII.dicoPar("@dimeTipoDocumento") = 1 Then 'image
                        comando.CommandText = "select imagen from DocumentosClientes.dbo.DocumentosNew where contrato=" + ContratoDocumentos.ToString + " and iddocumento=" + Grid.SelectedCells(0).Value.ToString
                        Dim reader As SqlDataReader = comando.ExecuteReader
                        reader.Read()
                        Dim by(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
                        reader.GetBytes(0, 0, by, 0, by.Length)
                        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(by)
                        PictureBox1.Image = Image.FromStream(ms)
                        reader.Close()
                        PictureBox1.Visible = True
                        pdfViewer.Visible = False
                    Else
                        Dim ToSaveFileTo As String = RutaReportes + "\ReportTemp.doc"
                        comando.CommandText = "select archivo from DocumentosClientes.dbo.DocumentosPDF where contrato=" + ContratoDocumentos.ToString + " and iddocumento=" + Grid.SelectedCells(0).Value.ToString
                        Dim reader As SqlDataReader = comando.ExecuteReader(System.Data.CommandBehavior.Default)
                        reader.Read()
                        Dim contents(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
                        'contents = reader.GetBytes(0, 0, contents, 0, contents.Length)
                        contents = reader.GetValue(0)
                        Dim fileName As String = System.IO.Path.GetTempFileName() + ".pdf"
                        File.WriteAllBytes(fileName, contents)
                        reader.Close()
                        PictureBox1.Visible = False
                        pdfViewer.Visible = True
                        pdfViewer.Navigate(fileName)
                        'System.IO.File.Delete(fileName)
                    End If
                    'comando.CommandText = "select imagen from DocumentosClientes.dbo.DocumentosNew where contrato=" + ContratoDocumentos.ToString + " and iddocumento=" + Grid.SelectedCells(0).Value.ToString
                End If
                conexion.Close()
            Else
                PictureBox1.Visible = True
                pdfViewer.Visible = False
                PictureBox1.Image = sofTV.My.Resources.Resources.filenotfound4041
            End If
        Catch ex As Exception
            'MsgBox(ex.ToString)

        End Try

    End Sub
    Private Sub dameimagen()
        Try
            Dim path As String = RutaReportes + "\DocumentoTemporalNuevo.xml"

            ' Create or overwrite the file. 
            Dim fs2 As FileStream = File.Create(path)
            Dim i As Image
            ' Add text to the file. 
            Dim info As Byte() = New UTF8Encoding(True).GetBytes(Grid.SelectedCells(3).Value.ToString)
            fs2.Write(info, 0, info.Length)



            Dim immm As imagen = New imagen
            ' Create an instance of the XmlSerializer specifying type and namespace. 
            Dim serializer As New System.Xml.Serialization.XmlSerializer(immm.GetType)

            ' A FileStream is needed to read the XML document. 
            'Dim fs As New FileStream(RutaReportes + "\DocumentoTemporalNuevo.xml", FileMode.Open)
            Dim reader As XmlReader = XmlReader.Create(fs2)
            fs2.Seek(0, SeekOrigin.Begin)
            ' Use the Deserialize method to restore the object's state.
            PictureBox1.Image = CType(serializer.Deserialize(reader), Image)
            'fs.Close()
            'fs.Flush()
            fs2.Close()
            fs2.Flush()
            reader.Close()
            PictureBox1.Image = i
        Catch ex As Exception
            MsgBox(ex.ToString)
        Finally

        End Try
    End Sub
    Private Sub dameimagen2()
        Dim path As String = RutaReportes + "\DocumentoTemporalNuevo.xml"

        ' Create or overwrite the file. 
        Dim fs2 As FileStream = File.Create(path)
        Dim i As Image
        ' Add text to the file. 
        Dim info As Byte() = New UTF8Encoding(True).GetBytes(Grid.SelectedCells(3).Value.ToString)
        fs2.Write(info, 0, info.Length)
        fs2.Close()
        Dim objImage As MemoryStream
        Dim objwebClient As WebClient
        Dim sURL As String = Trim("http://" + RutaReportes + "\DocumentoTemporalNuevo.xml")
        Dim bAns As Boolean

        Try
            If Not sURL.ToLower().StartsWith("http://") _
                 Then sURL = "http://" & sURL
            objwebClient = New WebClient()


            objImage = New  _
               MemoryStream(objwebClient.DownloadData(sURL))
            PictureBox1.Image = Image.FromStream(objImage)
            bAns = True
        Catch ex As Exception
            MsgBox(ex.ToString)
            fs2.Flush()
            bAns = False
        End Try
    End Sub
    Private Sub dameimagen3()
        Dim bmpName As String
        Dim stream As System.IO.Stream = Me.GetType().Assembly.GetManifestResourceStream(bmpName)
        ' if the stream is found...
        If Not stream Is Nothing Then
            Dim bmp As New Bitmap(stream)
            ' and the bitmpat is loaded...
            If Not bmp Is Nothing Then
                ' load it in the PictureBox
                PictureBox1.Image = bmp
            End If
        End If
    End Sub
    Public Class imagen
        Public imagen As Image
    End Class

    Private Sub cbRevisado_CheckedChanged(sender As Object, e As EventArgs) Handles cbRevisado.CheckedChanged
        If noentra = True Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@cbRevisado", SqlDbType.Bit, cbRevisado.Checked)
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoDocumentos)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ModificaRevisado")
    End Sub

    Private Sub cbRecibido_CheckedChanged(sender As Object, e As EventArgs) Handles cbRecibido.CheckedChanged
        If noentra = True Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@cbRecibido", SqlDbType.Bit, cbRecibido.Checked)
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoDocumentos)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ModificaRecibido")
    End Sub
    Private Sub llenaOpciones()
        
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@cbRecibido", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@cbRevisado", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoDocumentos)
        BaseII.ProcedimientoOutPut("DameOpcionesDocumentos")
        cbRecibido.Checked = BaseII.dicoPar("@cbRecibido")
        cbRevisado.Checked = BaseII.dicoPar("@cbRevisado")

    End Sub
End Class
