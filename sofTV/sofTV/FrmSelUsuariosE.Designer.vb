<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelUsuariosE
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ConSelUsuariosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSelUsuariosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSelUsuariosTmpTableAdapter()
        Me.NombreListBox = New System.Windows.Forms.ListBox()
        Me.ConSelUsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSelUsuariosTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSelUsuariosTableAdapter()
        Me.NombreListBox1 = New System.Windows.Forms.ListBox()
        Me.InsertarSelUsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarSelUsuariosTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarSelUsuariosTableAdapter()
        Me.EliminarSelUsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EliminarSelUsuariosTableAdapter = New sofTV.DataSetEric2TableAdapters.EliminarSelUsuariosTableAdapter()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSelUsuariosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSelUsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarSelUsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EliminarSelUsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConSelUsuariosTmpBindingSource
        '
        Me.ConSelUsuariosTmpBindingSource.DataMember = "ConSelUsuariosTmp"
        Me.ConSelUsuariosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSelUsuariosTmpTableAdapter
        '
        Me.ConSelUsuariosTmpTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox
        '
        Me.NombreListBox.DataSource = Me.ConSelUsuariosTmpBindingSource
        Me.NombreListBox.DisplayMember = "Nombre"
        Me.NombreListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreListBox.FormattingEnabled = True
        Me.NombreListBox.ItemHeight = 18
        Me.NombreListBox.Location = New System.Drawing.Point(48, 52)
        Me.NombreListBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreListBox.Name = "NombreListBox"
        Me.NombreListBox.Size = New System.Drawing.Size(367, 364)
        Me.NombreListBox.TabIndex = 3
        Me.NombreListBox.ValueMember = "Clave"
        '
        'ConSelUsuariosBindingSource
        '
        Me.ConSelUsuariosBindingSource.DataMember = "ConSelUsuarios"
        Me.ConSelUsuariosBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSelUsuariosTableAdapter
        '
        Me.ConSelUsuariosTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox1
        '
        Me.NombreListBox1.DataSource = Me.ConSelUsuariosBindingSource
        Me.NombreListBox1.DisplayMember = "Nombre"
        Me.NombreListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreListBox1.FormattingEnabled = True
        Me.NombreListBox1.ItemHeight = 18
        Me.NombreListBox1.Location = New System.Drawing.Point(595, 52)
        Me.NombreListBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreListBox1.Name = "NombreListBox1"
        Me.NombreListBox1.Size = New System.Drawing.Size(367, 364)
        Me.NombreListBox1.TabIndex = 6
        Me.NombreListBox1.ValueMember = "Clave"
        '
        'InsertarSelUsuariosBindingSource
        '
        Me.InsertarSelUsuariosBindingSource.DataMember = "InsertarSelUsuarios"
        Me.InsertarSelUsuariosBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarSelUsuariosTableAdapter
        '
        Me.InsertarSelUsuariosTableAdapter.ClearBeforeFill = True
        '
        'EliminarSelUsuariosBindingSource
        '
        Me.EliminarSelUsuariosBindingSource.DataMember = "EliminarSelUsuarios"
        Me.EliminarSelUsuariosBindingSource.DataSource = Me.DataSetEric2
        '
        'EliminarSelUsuariosTableAdapter
        '
        Me.EliminarSelUsuariosTableAdapter.ClearBeforeFill = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(844, 489)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(185, 44)
        Me.Button6.TabIndex = 17
        Me.Button6.Text = "&SALIR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(651, 489)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(185, 44)
        Me.Button5.TabIndex = 16
        Me.Button5.Text = "&CONTINUAR"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(459, 342)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(85, 31)
        Me.Button4.TabIndex = 15
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(459, 304)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(85, 31)
        Me.Button3.TabIndex = 14
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(459, 158)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(85, 31)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(459, 119)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 31)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmSelUsuariosE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1056, 558)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.NombreListBox1)
        Me.Controls.Add(Me.NombreListBox)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelUsuariosE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Usuarios"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSelUsuariosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSelUsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarSelUsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EliminarSelUsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConSelUsuariosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSelUsuariosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConSelUsuariosTmpTableAdapter
    Friend WithEvents NombreListBox As System.Windows.Forms.ListBox
    Friend WithEvents ConSelUsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSelUsuariosTableAdapter As sofTV.DataSetEric2TableAdapters.ConSelUsuariosTableAdapter
    Friend WithEvents NombreListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents InsertarSelUsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarSelUsuariosTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarSelUsuariosTableAdapter
    Friend WithEvents EliminarSelUsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EliminarSelUsuariosTableAdapter As sofTV.DataSetEric2TableAdapters.EliminarSelUsuariosTableAdapter
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
