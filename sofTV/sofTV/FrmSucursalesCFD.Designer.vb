﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSucursalesCFD
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSucursalesCFD))
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.tbFolio = New System.Windows.Forms.TextBox()
        Me.tbSerie = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.tbCp = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tbLocalidad = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbMunicipio = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbColonia = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tbEstado = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbPais = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tbNumeroInterior = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbNumeroExt = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbCalle = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbReferencia = New System.Windows.Forms.TextBox()
        Me.bnSucursalesSAT = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminar = New System.Windows.Forms.ToolStripButton()
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.lbIzq = New System.Windows.Forms.ListBox()
        Me.bnAgregar = New System.Windows.Forms.Button()
        Me.lbDer = New System.Windows.Forms.ListBox()
        Me.bnAgrearTodos = New System.Windows.Forms.Button()
        Me.bnEliminarTodos = New System.Windows.Forms.Button()
        Me.bnEliminar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.gbSerie = New System.Windows.Forms.GroupBox()
        Me.tbSerieNotas = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.tbFolioNotas = New System.Windows.Forms.TextBox()
        Me.gbSucursal = New System.Windows.Forms.GroupBox()
        CType(Me.bnSucursalesSAT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnSucursalesSAT.SuspendLayout()
        Me.gbSerie.SuspendLayout()
        Me.gbSucursal.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(85, 50)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(123, 18)
        Me.Label14.TabIndex = 79
        Me.Label14.Text = "Serie Facturas:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(88, 81)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(122, 18)
        Me.Label15.TabIndex = 78
        Me.Label15.Text = "Folio Facturas:"
        '
        'tbFolio
        '
        Me.tbFolio.Enabled = False
        Me.tbFolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFolio.Location = New System.Drawing.Point(232, 74)
        Me.tbFolio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbFolio.MaxLength = 50
        Me.tbFolio.Name = "tbFolio"
        Me.tbFolio.ReadOnly = True
        Me.tbFolio.Size = New System.Drawing.Size(148, 24)
        Me.tbFolio.TabIndex = 1
        '
        'tbSerie
        '
        Me.tbSerie.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSerie.Location = New System.Drawing.Point(232, 43)
        Me.tbSerie.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbSerie.MaxLength = 50
        Me.tbSerie.Name = "tbSerie"
        Me.tbSerie.Size = New System.Drawing.Size(148, 24)
        Me.tbSerie.TabIndex = 0
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(677, 224)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(120, 18)
        Me.Label13.TabIndex = 75
        Me.Label13.Text = "Código Postal:"
        '
        'tbCp
        '
        Me.tbCp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCp.Location = New System.Drawing.Point(819, 217)
        Me.tbCp.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCp.MaxLength = 50
        Me.tbCp.Name = "tbCp"
        Me.tbCp.Size = New System.Drawing.Size(148, 24)
        Me.tbCp.TabIndex = 13
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(69, 206)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(137, 18)
        Me.Label12.TabIndex = 74
        Me.Label12.Text = "Número Exterior:"
        '
        'tbLocalidad
        '
        Me.tbLocalidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLocalidad.Location = New System.Drawing.Point(819, 55)
        Me.tbLocalidad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbLocalidad.MaxLength = 150
        Me.tbLocalidad.Name = "tbLocalidad"
        Me.tbLocalidad.Size = New System.Drawing.Size(416, 24)
        Me.tbLocalidad.TabIndex = 8
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(712, 63)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(85, 18)
        Me.Label11.TabIndex = 73
        Me.Label11.Text = "Localidad:"
        '
        'tbMunicipio
        '
        Me.tbMunicipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMunicipio.Location = New System.Drawing.Point(819, 119)
        Me.tbMunicipio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbMunicipio.MaxLength = 150
        Me.tbMunicipio.Name = "tbMunicipio"
        Me.tbMunicipio.Size = New System.Drawing.Size(416, 24)
        Me.tbMunicipio.TabIndex = 10
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(712, 127)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(85, 18)
        Me.Label10.TabIndex = 72
        Me.Label10.Text = "Municipio:"
        '
        'tbColonia
        '
        Me.tbColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbColonia.Location = New System.Drawing.Point(819, 25)
        Me.tbColonia.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbColonia.MaxLength = 150
        Me.tbColonia.Name = "tbColonia"
        Me.tbColonia.Size = New System.Drawing.Size(416, 24)
        Me.tbColonia.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(731, 32)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(71, 18)
        Me.Label9.TabIndex = 71
        Me.Label9.Text = "Colonia:"
        '
        'tbEstado
        '
        Me.tbEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEstado.Location = New System.Drawing.Point(819, 153)
        Me.tbEstado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbEstado.MaxLength = 150
        Me.tbEstado.Name = "tbEstado"
        Me.tbEstado.Size = New System.Drawing.Size(416, 24)
        Me.tbEstado.TabIndex = 11
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(737, 160)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 18)
        Me.Label8.TabIndex = 70
        Me.Label8.Text = "Estado:"
        '
        'tbPais
        '
        Me.tbPais.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPais.Location = New System.Drawing.Point(819, 186)
        Me.tbPais.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbPais.MaxLength = 150
        Me.tbPais.Name = "tbPais"
        Me.tbPais.Size = New System.Drawing.Size(416, 24)
        Me.tbPais.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(75, 239)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(131, 18)
        Me.Label7.TabIndex = 69
        Me.Label7.Text = "Número Interior:"
        '
        'tbNumeroInterior
        '
        Me.tbNumeroInterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNumeroInterior.Location = New System.Drawing.Point(232, 231)
        Me.tbNumeroInterior.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbNumeroInterior.MaxLength = 50
        Me.tbNumeroInterior.Name = "tbNumeroInterior"
        Me.tbNumeroInterior.Size = New System.Drawing.Size(148, 24)
        Me.tbNumeroInterior.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(703, 94)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 18)
        Me.Label6.TabIndex = 68
        Me.Label6.Text = "Referencia:"
        '
        'tbNumeroExt
        '
        Me.tbNumeroExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNumeroExt.Location = New System.Drawing.Point(232, 201)
        Me.tbNumeroExt.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbNumeroExt.MaxLength = 50
        Me.tbNumeroExt.Name = "tbNumeroExt"
        Me.tbNumeroExt.Size = New System.Drawing.Size(148, 24)
        Me.tbNumeroExt.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(759, 192)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 18)
        Me.Label5.TabIndex = 67
        Me.Label5.Text = "País:"
        '
        'tbCalle
        '
        Me.tbCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCalle.Location = New System.Drawing.Point(232, 167)
        Me.tbCalle.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCalle.MaxLength = 250
        Me.tbCalle.Name = "tbCalle"
        Me.tbCalle.Size = New System.Drawing.Size(416, 24)
        Me.tbCalle.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(165, 175)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 18)
        Me.Label4.TabIndex = 66
        Me.Label4.Text = "Calle:"
        '
        'tbReferencia
        '
        Me.tbReferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbReferencia.Location = New System.Drawing.Point(819, 90)
        Me.tbReferencia.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbReferencia.MaxLength = 150
        Me.tbReferencia.Name = "tbReferencia"
        Me.tbReferencia.Size = New System.Drawing.Size(416, 24)
        Me.tbReferencia.TabIndex = 9
        '
        'bnSucursalesSAT
        '
        Me.bnSucursalesSAT.AddNewItem = Nothing
        Me.bnSucursalesSAT.CountItem = Nothing
        Me.bnSucursalesSAT.DeleteItem = Me.tsbEliminar
        Me.bnSucursalesSAT.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.bnSucursalesSAT.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminar, Me.tsbGuardar})
        Me.bnSucursalesSAT.Location = New System.Drawing.Point(0, 0)
        Me.bnSucursalesSAT.MoveFirstItem = Nothing
        Me.bnSucursalesSAT.MoveLastItem = Nothing
        Me.bnSucursalesSAT.MoveNextItem = Nothing
        Me.bnSucursalesSAT.MovePreviousItem = Nothing
        Me.bnSucursalesSAT.Name = "bnSucursalesSAT"
        Me.bnSucursalesSAT.PositionItem = Nothing
        Me.bnSucursalesSAT.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnSucursalesSAT.Size = New System.Drawing.Size(1344, 28)
        Me.bnSucursalesSAT.TabIndex = 0
        Me.bnSucursalesSAT.TabStop = True
        Me.bnSucursalesSAT.Text = "BindingNavigator1"
        '
        'tsbEliminar
        '
        Me.tsbEliminar.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsbEliminar.Image = CType(resources.GetObject("tsbEliminar.Image"), System.Drawing.Image)
        Me.tsbEliminar.Name = "tsbEliminar"
        Me.tsbEliminar.RightToLeftAutoMirrorImage = True
        Me.tsbEliminar.Size = New System.Drawing.Size(103, 25)
        Me.tsbEliminar.Text = "&Eliminar"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(103, 25)
        Me.tsbGuardar.Text = "&Guardar"
        '
        'lbIzq
        '
        Me.lbIzq.DisplayMember = "Nombre"
        Me.lbIzq.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbIzq.FormattingEnabled = True
        Me.lbIzq.ItemHeight = 18
        Me.lbIzq.Location = New System.Drawing.Point(20, 62)
        Me.lbIzq.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.lbIzq.Name = "lbIzq"
        Me.lbIzq.Size = New System.Drawing.Size(391, 364)
        Me.lbIzq.TabIndex = 0
        Me.lbIzq.ValueMember = "Clv_Sucursal"
        '
        'bnAgregar
        '
        Me.bnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAgregar.Location = New System.Drawing.Point(445, 143)
        Me.bnAgregar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnAgregar.Name = "bnAgregar"
        Me.bnAgregar.Size = New System.Drawing.Size(80, 28)
        Me.bnAgregar.TabIndex = 2
        Me.bnAgregar.Text = ">"
        Me.bnAgregar.UseVisualStyleBackColor = True
        '
        'lbDer
        '
        Me.lbDer.DisplayMember = "Nombre"
        Me.lbDer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbDer.FormattingEnabled = True
        Me.lbDer.ItemHeight = 18
        Me.lbDer.Location = New System.Drawing.Point(557, 62)
        Me.lbDer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.lbDer.Name = "lbDer"
        Me.lbDer.Size = New System.Drawing.Size(391, 364)
        Me.lbDer.TabIndex = 1
        Me.lbDer.ValueMember = "Clv_Sucursal"
        '
        'bnAgrearTodos
        '
        Me.bnAgrearTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAgrearTodos.Location = New System.Drawing.Point(445, 178)
        Me.bnAgrearTodos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnAgrearTodos.Name = "bnAgrearTodos"
        Me.bnAgrearTodos.Size = New System.Drawing.Size(80, 28)
        Me.bnAgrearTodos.TabIndex = 3
        Me.bnAgrearTodos.Text = ">>"
        Me.bnAgrearTodos.UseVisualStyleBackColor = True
        '
        'bnEliminarTodos
        '
        Me.bnEliminarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEliminarTodos.Location = New System.Drawing.Point(445, 329)
        Me.bnEliminarTodos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnEliminarTodos.Name = "bnEliminarTodos"
        Me.bnEliminarTodos.Size = New System.Drawing.Size(80, 28)
        Me.bnEliminarTodos.TabIndex = 5
        Me.bnEliminarTodos.Text = "<<"
        Me.bnEliminarTodos.UseVisualStyleBackColor = True
        '
        'bnEliminar
        '
        Me.bnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEliminar.Location = New System.Drawing.Point(445, 293)
        Me.bnEliminar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnEliminar.Name = "bnEliminar"
        Me.bnEliminar.Size = New System.Drawing.Size(80, 28)
        Me.bnEliminar.TabIndex = 4
        Me.bnEliminar.Text = "<"
        Me.bnEliminar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(16, 39)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(233, 18)
        Me.Label1.TabIndex = 87
        Me.Label1.Text = "Sucursales sin Relacionar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(553, 39)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(199, 18)
        Me.Label2.TabIndex = 88
        Me.Label2.Text = "Sucursales Relacionadas"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(1147, 839)
        Me.bnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(181, 44)
        Me.bnSalir.TabIndex = 1
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'gbSerie
        '
        Me.gbSerie.Controls.Add(Me.tbSerieNotas)
        Me.gbSerie.Controls.Add(Me.Label3)
        Me.gbSerie.Controls.Add(Me.Label16)
        Me.gbSerie.Controls.Add(Me.tbFolioNotas)
        Me.gbSerie.Controls.Add(Me.tbSerie)
        Me.gbSerie.Controls.Add(Me.tbReferencia)
        Me.gbSerie.Controls.Add(Me.Label4)
        Me.gbSerie.Controls.Add(Me.tbCalle)
        Me.gbSerie.Controls.Add(Me.Label5)
        Me.gbSerie.Controls.Add(Me.tbNumeroExt)
        Me.gbSerie.Controls.Add(Me.Label6)
        Me.gbSerie.Controls.Add(Me.tbNumeroInterior)
        Me.gbSerie.Controls.Add(Me.Label7)
        Me.gbSerie.Controls.Add(Me.tbPais)
        Me.gbSerie.Controls.Add(Me.Label8)
        Me.gbSerie.Controls.Add(Me.tbEstado)
        Me.gbSerie.Controls.Add(Me.Label14)
        Me.gbSerie.Controls.Add(Me.Label9)
        Me.gbSerie.Controls.Add(Me.Label15)
        Me.gbSerie.Controls.Add(Me.tbColonia)
        Me.gbSerie.Controls.Add(Me.tbFolio)
        Me.gbSerie.Controls.Add(Me.Label10)
        Me.gbSerie.Controls.Add(Me.tbMunicipio)
        Me.gbSerie.Controls.Add(Me.Label13)
        Me.gbSerie.Controls.Add(Me.Label11)
        Me.gbSerie.Controls.Add(Me.tbCp)
        Me.gbSerie.Controls.Add(Me.tbLocalidad)
        Me.gbSerie.Controls.Add(Me.Label12)
        Me.gbSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSerie.Location = New System.Drawing.Point(36, 50)
        Me.gbSerie.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbSerie.Name = "gbSerie"
        Me.gbSerie.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbSerie.Size = New System.Drawing.Size(1277, 277)
        Me.gbSerie.TabIndex = 90
        Me.gbSerie.TabStop = False
        Me.gbSerie.Text = "Información de la Serie"
        '
        'tbSerieNotas
        '
        Me.tbSerieNotas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbSerieNotas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSerieNotas.Location = New System.Drawing.Point(232, 103)
        Me.tbSerieNotas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbSerieNotas.MaxLength = 50
        Me.tbSerieNotas.Name = "tbSerieNotas"
        Me.tbSerieNotas.Size = New System.Drawing.Size(148, 24)
        Me.tbSerieNotas.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(16, 111)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(185, 18)
        Me.Label3.TabIndex = 83
        Me.Label3.Text = "Serie Notas de Crédito:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(19, 142)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(184, 18)
        Me.Label16.TabIndex = 82
        Me.Label16.Text = "Folio Notas de Crédito:"
        '
        'tbFolioNotas
        '
        Me.tbFolioNotas.Enabled = False
        Me.tbFolioNotas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFolioNotas.Location = New System.Drawing.Point(232, 134)
        Me.tbFolioNotas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbFolioNotas.MaxLength = 50
        Me.tbFolioNotas.Name = "tbFolioNotas"
        Me.tbFolioNotas.ReadOnly = True
        Me.tbFolioNotas.Size = New System.Drawing.Size(148, 24)
        Me.tbFolioNotas.TabIndex = 3
        '
        'gbSucursal
        '
        Me.gbSucursal.Controls.Add(Me.lbIzq)
        Me.gbSucursal.Controls.Add(Me.bnAgregar)
        Me.gbSucursal.Controls.Add(Me.lbDer)
        Me.gbSucursal.Controls.Add(Me.Label2)
        Me.gbSucursal.Controls.Add(Me.bnAgrearTodos)
        Me.gbSucursal.Controls.Add(Me.Label1)
        Me.gbSucursal.Controls.Add(Me.bnEliminar)
        Me.gbSucursal.Controls.Add(Me.bnEliminarTodos)
        Me.gbSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSucursal.Location = New System.Drawing.Point(179, 347)
        Me.gbSucursal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbSucursal.Name = "gbSucursal"
        Me.gbSucursal.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbSucursal.Size = New System.Drawing.Size(972, 458)
        Me.gbSucursal.TabIndex = 91
        Me.gbSucursal.TabStop = False
        Me.gbSucursal.Text = "Sucursales relacionadas con la Serie"
        '
        'FrmSucursalesCFD
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1344, 898)
        Me.Controls.Add(Me.gbSucursal)
        Me.Controls.Add(Me.gbSerie)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnSucursalesSAT)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSucursalesCFD"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Series SAT"
        CType(Me.bnSucursalesSAT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnSucursalesSAT.ResumeLayout(False)
        Me.bnSucursalesSAT.PerformLayout()
        Me.gbSerie.ResumeLayout(False)
        Me.gbSerie.PerformLayout()
        Me.gbSucursal.ResumeLayout(False)
        Me.gbSucursal.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents tbFolio As System.Windows.Forms.TextBox
    Friend WithEvents tbSerie As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents tbCp As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents tbLocalidad As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbMunicipio As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tbColonia As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tbEstado As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbPais As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbNumeroInterior As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbNumeroExt As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbCalle As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbReferencia As System.Windows.Forms.TextBox
    Friend WithEvents bnSucursalesSAT As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents lbIzq As System.Windows.Forms.ListBox
    Friend WithEvents bnAgregar As System.Windows.Forms.Button
    Friend WithEvents lbDer As System.Windows.Forms.ListBox
    Friend WithEvents bnAgrearTodos As System.Windows.Forms.Button
    Friend WithEvents bnEliminarTodos As System.Windows.Forms.Button
    Friend WithEvents bnEliminar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents gbSerie As System.Windows.Forms.GroupBox
    Friend WithEvents gbSucursal As System.Windows.Forms.GroupBox
    Friend WithEvents tbSerieNotas As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents tbFolioNotas As System.Windows.Forms.TextBox
End Class
