Imports System.Data.SqlClient
Imports System.Text

Public Class FrmTap

    Dim IdTap, Clv_Sector, Clv_Cluster, Clv_Colonia, Clv_Calle, IdPoste As Integer
    Dim entra = False

    Private Sub CONSULTATap(ByVal Op As Integer, ByVal IdTap As Integer, ByVal Clave As String, ByVal Sector As String, ByVal Poste As String, ByVal Colonia As String, ByVal Calle As String, ByVal Cluster As String)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@IdTap", SqlDbType.Int, IdTap)
        BaseII.CreateMyParameter("@Clave", SqlDbType.VarChar, Clave, 150)
        BaseII.CreateMyParameter("@Sector", SqlDbType.VarChar, Sector, 150)
        BaseII.CreateMyParameter("@Poste", SqlDbType.VarChar, Poste, 150)
        BaseII.CreateMyParameter("@Colonia", SqlDbType.VarChar, Colonia, 150)
        BaseII.CreateMyParameter("@Calle", SqlDbType.VarChar, Calle, 150)
        BaseII.CreateMyParameter("@Cluster", SqlDbType.VarChar, Cluster, 150)
        dTable = BaseII.ConsultaDT("CONSULTATap")

        'IdTap = dTable.Rows(0)("IdTap").ToString
        Clv_Sector = dTable.Rows(0)("Clv_Sector").ToString
        Clv_Colonia = dTable.Rows(0)("Clv_Colonia").ToString
        Clv_Calle = dTable.Rows(0)("Clv_Calle").ToString
        IdPoste = dTable.Rows(0)("IdPoste").ToString
        tbIngenieria.Text = dTable.Rows(0)("Ingenieria").ToString
        tbSalidas.Text = dTable.Rows(0)("Salidas").ToString
        tbClave.Text = dTable.Rows(0)("Clave").ToString
        tbNoCasas.Text = dTable.Rows(0)("NoCasas").ToString
        tbNoNegocios.Text = dTable.Rows(0)("NoNegocios").ToString
        tbNoLotes.Text = dTable.Rows(0)("NoLotes").ToString
        tbNoServicios.Text = dTable.Rows(0)("NoServicios").ToString
        tbFrenteANumero.Text = dTable.Rows(0)("FrenteANumero").ToString
        Clv_Cluster = dTable.Rows(0)("Clv_Cluster").ToString


        llenacbcluster(6, 0)
        ConSector(0, "", "", 4)
        MUESTRAPostes(0)
        MuestraColoniaSec(0, Clv_Sector, 1)
        If Clv_Cluster > 0 Then
            cbcluster.SelectedValue = Clv_Cluster
        End If
        If Clv_Sector > 0 Then
            'ConSector(Clv_Sector, "", "", 3)
            cbSector.SelectedValue = Clv_Sector
            'Else
            '    ConSector(0, "", "", 4)
        End If

        'MUESTRAPostes(IdPoste)
        cbPoste.SelectedValue = IdPoste
        'If Clv_Colonia > 0 Then
        '    MuestraColoniaSec(Clv_Colonia, Clv_Sector, 2)
        'Else
        '    MuestraColoniaSec(0, Clv_Sector, 1)
        'End If
        If Clv_Colonia > 0 Then
            cbColonia.SelectedValue = Clv_Colonia
        End If
        MuestraCalleSec(Clv_Sector, cbColonia.SelectedValue, 0, 0)
        'If Clv_Calle = 0 Then
        If Clv_Calle > 0 Then
            'MuestraCalleSec(Clv_Sector, cbColonia.SelectedValue, 0, 0)
            cbCalle.SelectedValue = Clv_Calle
            'Else
            '    MuestraCalleSec(Clv_Sector, cbColonia.SelectedValue, Clv_Calle, 1)
        End If

    End Sub

    Private Sub ConSector(ByVal Clv_Sector As Integer, ByVal Clv_Txt As String, ByVal Descripcion As String, ByVal Op As Integer)
        Dim cluster As Integer
        If cbcluster.Items.Count > 0 Then
            cluster = cbcluster.SelectedValue
        Else
            cluster = 1
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Sector", SqlDbType.Int, Clv_Sector)
        BaseII.CreateMyParameter("@Clv_Txt", SqlDbType.Int, Clv_Sector)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.Int, Clv_Sector)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@clv_cluster", SqlDbType.Int, CInt(cluster))
        cbSector.DataSource = BaseII.ConsultaDT("ConSector2")
    End Sub

    Private Sub MUESTRAPostes(ByVal Id As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Id", SqlDbType.Int, Id)
        cbPoste.DataSource = BaseII.ConsultaDT("MUESTRAPostes")
    End Sub

    Private Sub MuestraColoniaSec(ByVal Clv_Colonia As Integer, ByVal Clv_Sector As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.CreateMyParameter("@Clv_Sector", SqlDbType.Int, Clv_Sector)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        cbColonia.DataSource = BaseII.ConsultaDT("MuestraColoniaSec")
    End Sub

    Private Sub MuestraCalleSec(ByVal Clv_Sector As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Calle As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Sector", SqlDbType.Int, Clv_Sector)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.CreateMyParameter("@Clv_Calle", SqlDbType.Int, Clv_Calle)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        cbCalle.DataSource = BaseII.ConsultaDT("MuestraCalleSec")
    End Sub

    Private Sub INSERTATap(ByVal Clv_Sector As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Calle As Integer, ByVal IdPoste As Integer, ByVal Ingenieria As Integer, ByVal Salidas As Integer,
                           ByVal Clave As String, ByVal NoCasas As Integer, ByVal NoNegocios As Integer, ByVal NoLotes As Integer, ByVal NoServicios As Integer, ByVal FrenteANumero As String)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdTap", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Clv_Sector", SqlDbType.Int, Clv_Sector)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.CreateMyParameter("@Clv_Calle", SqlDbType.Int, Clv_Calle)
        BaseII.CreateMyParameter("@IdPoste", SqlDbType.Int, IdPoste)
        BaseII.CreateMyParameter("@Ingenieria", SqlDbType.Int, Ingenieria)
        BaseII.CreateMyParameter("@Salidas", SqlDbType.Int, Salidas)
        BaseII.CreateMyParameter("@Clave", SqlDbType.VarChar, Clave, 150)
        BaseII.CreateMyParameter("@NoCasas", SqlDbType.Int, NoCasas)
        BaseII.CreateMyParameter("@NoNegocios", SqlDbType.Int, NoNegocios)
        BaseII.CreateMyParameter("@NoLotes", SqlDbType.Int, NoLotes)
        BaseII.CreateMyParameter("@NoServicios", SqlDbType.Int, NoServicios)
        BaseII.CreateMyParameter("@FrenteANumero", SqlDbType.VarChar, FrenteANumero, 150)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.CreateMyParameter("@Clv_Cluster", SqlDbType.Int, cbcluster.SelectedValue)
        BaseII.ProcedimientoOutPut("INSERTATap")
        eIdTap = CInt(BaseII.dicoPar("@IdTap").ToString())
        eMsj = BaseII.dicoPar("@Msj").ToString()

        If eMsj.Length > 0 Then
            MessageBox.Show(eMsj)
        Else
            MessageBox.Show("Se guard� con �xito.")
            CONSULTATap(6, eIdTap, "", "", "", "", "", "")
            gbDetalleClave.Enabled = True
            eOpcion = "M"
            GloBnd = True
        End If
    End Sub

    Private Sub MODIFICATap(ByVal IdTap As Integer, ByVal Clv_Sector As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Calle As Integer, ByVal IdPoste As Integer, ByVal Ingenieria As Integer, ByVal Salidas As Integer,
                            ByVal Clave As String, ByVal NoCasas As Integer, ByVal NoNegocios As Integer, ByVal NoLotes As Integer, ByVal NoServicios As Integer, ByVal FrenteANumero As String)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdTap", SqlDbType.Int, IdTap)
        BaseII.CreateMyParameter("@Clv_Sector", SqlDbType.Int, Clv_Sector)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.CreateMyParameter("@Clv_Calle", SqlDbType.Int, Clv_Calle)
        BaseII.CreateMyParameter("@IdPoste", SqlDbType.Int, IdPoste)
        BaseII.CreateMyParameter("@Ingenieria", SqlDbType.Int, Ingenieria)
        BaseII.CreateMyParameter("@Salidas", SqlDbType.Int, Salidas)
        BaseII.CreateMyParameter("@Clave", SqlDbType.VarChar, Clave, 150)
        BaseII.CreateMyParameter("@NoCasas", SqlDbType.Int, NoCasas)
        BaseII.CreateMyParameter("@NoNegocios", SqlDbType.Int, NoNegocios)
        BaseII.CreateMyParameter("@NoLotes", SqlDbType.Int, NoLotes)
        BaseII.CreateMyParameter("@NoServicios", SqlDbType.Int, NoServicios)
        BaseII.CreateMyParameter("@FrenteANumero", SqlDbType.VarChar, FrenteANumero, 150)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.CreateMyParameter("@Clv_Cluster", SqlDbType.Int, cbcluster.SelectedValue)
        BaseII.ProcedimientoOutPut("MODIFICATap")

        eMsj = BaseII.dicoPar("@Msj").ToString()

        If eMsj.Length > 0 Then
            MessageBox.Show(eMsj)
        Else
            MessageBox.Show("Se modific� con �xito.")
            CONSULTATap(6, eIdTap, "", "", "", "", "", "")
            GloBnd = True
        End If

    End Sub

    Private Sub FrmTap_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If eOpcion = "N" Then
            llenacbcluster(6, 0)
            ConSector(0, "", "", 4)
            MUESTRAPostes(0)
            gbDetalleClave.Enabled = False
        ElseIf eOpcion = "C" Then
            CONSULTATap(6, eIdTap, "", "", "", "", "", "")
            bnTap.Enabled = False
            gbClave.Enabled = False
            gbDetalleClave.Enabled = False
        ElseIf eOpcion = "M" Then
            CONSULTATap(6, eIdTap, "", "", "", "", "", "")
        End If
        entra = True
        If eOpcion = "N" Or eOpcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub tbIngenieria_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles tbIngenieria.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.tbIngenieria, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub tbSalidas_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles tbSalidas.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.tbSalidas, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub tbNoCasas_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles tbNoCasas.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.tbNoCasas, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub tbNoNegocios_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles tbNoNegocios.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.tbNoNegocios, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub tbNoLotes_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles tbNoLotes.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.tbNoLotes, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub tbNoServicios_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles tbNoServicios.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.tbNoServicios, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub tbFrenteANumero_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles tbFrenteANumero.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.tbFrenteANumero, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub tsbGuardar_Click(sender As System.Object, e As System.EventArgs) Handles tsbGuardar.Click
        If eOpcion = "N" Then
            If cbcluster.Text.Length = 0 Then
                MsgBox("Selecciona un Cluster.")
                Exit Sub
            End If
            If cbSector.Text.Length = 0 Then
                MessageBox.Show("Selecciona un Sector.")
                Exit Sub
            End If

            If cbSector.SelectedValue = 0 Then
                MessageBox.Show("Selecciona un Sector.")
                Exit Sub
            End If

            If cbPoste.Text.Length = 0 Then
                MessageBox.Show("Selecciona Poste.")
                Exit Sub
            End If

            If cbPoste.SelectedValue = 0 Then
                MessageBox.Show("Selecciona Poste.")
                Exit Sub
            End If

            If tbIngenieria.Text.Length = 0 Then
                MessageBox.Show("Captura n�mero de Tap.")
                Exit Sub
            End If

            If tbSalidas.Text.Length = 0 Then
                MessageBox.Show("Captura n�mero de Salidas")
                Exit Sub
            End If

            INSERTATap(cbSector.SelectedValue, 0, 0, cbPoste.SelectedValue, tbIngenieria.Text, tbSalidas.Text, "", 0, 0, 0, 0, 0)

        ElseIf eOpcion = "M" Then
            If cbcluster.Text.Length = 0 Then
                MsgBox("Selecciona un Cluster.")
                Exit Sub
            End If
            If cbSector.Text.Length = 0 Then
                MessageBox.Show("Selecciona un Sector.")
                Exit Sub
            End If

            If cbSector.SelectedValue = 0 Then
                MessageBox.Show("Selecciona un Sector.")
                Exit Sub
            End If

            If cbPoste.Text.Length = 0 Then
                MessageBox.Show("Selecciona Poste.")
                Exit Sub
            End If

            If cbPoste.SelectedValue = 0 Then
                MessageBox.Show("Selecciona Poste.")
                Exit Sub
            End If

            If tbIngenieria.Text.Length = 0 Then
                MessageBox.Show("Captura n�mero de Tap.")
                Exit Sub
            End If

            If tbSalidas.Text.Length = 0 Then
                MessageBox.Show("Captura n�mero de Salidas.")
                Exit Sub
            End If

            If cbColonia.Text.Length = 0 Then
                MessageBox.Show("Seleccina una Colonia.")
                Exit Sub
            End If

            If cbColonia.SelectedValue = 0 Then
                MessageBox.Show("Selecciona una Colonia.")
                Exit Sub
            End If

            If cbCalle.Text.Length = 0 Then
                MessageBox.Show("Selecciona una Calle.")
                Exit Sub
            End If

            If cbCalle.SelectedValue = 0 Then
                MessageBox.Show("Selecciona una Calle.")
                Exit Sub
            End If

            MODIFICATap(eIdTap, cbSector.SelectedValue, cbColonia.SelectedValue, cbCalle.SelectedValue, cbPoste.SelectedValue, tbIngenieria.Text, tbSalidas.Text, "", tbNoCasas.Text, tbNoNegocios.Text, tbNoLotes.Text, tbNoServicios.Text, tbFrenteANumero.Text)

        End If
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub


    Private Sub cbColonia_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbColonia.SelectedIndexChanged
        If entra = True Then
            If cbColonia.Text.Length = 0 Then Exit Sub
            If cbColonia.SelectedValue = 0 Then Exit Sub
            If Clv_Calle = 0 Then
                MuestraCalleSec(cbSector.SelectedValue, cbColonia.SelectedValue, 0, 0)
            Else
                MuestraCalleSec(cbSector.SelectedValue, cbColonia.SelectedValue, 9999, 1)
            End If
        End If
    End Sub
    Private Sub llenacbcluster(ByVal opcion As Integer, ByVal clv_clust As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, opcion)
        BaseII.CreateMyParameter("@clave", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@clv_cluster", SqlDbType.Int, clv_clust)
        cbcluster.DisplayMember = "Descripcion"
        cbcluster.ValueMember = "Clv_Cluster"
        cbcluster.DataSource = BaseII.ConsultaDT("MuestraCluster")
        

    End Sub

    Private Sub cbSector_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSector.SelectedIndexChanged
        MuestraColoniaSec(0, cbSector.SelectedValue, 1)
    End Sub

    Private Sub cbcluster_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbcluster.SelectedIndexChanged
        ConSector(0, "", "", 4)
    End Sub
End Class