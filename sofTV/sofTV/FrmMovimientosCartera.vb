﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class FrmMovimientosCartera



    Private Sub GENERAMovimientosCartera(ByVal CLV_TIPSER As Integer, ByVal CLV_USUARIO As String, ByVal TIPO As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
            BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, CLV_USUARIO, 10)
            BaseII.CreateMyParameter("@TIPO", SqlDbType.Int, TIPO)
            BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, LocClv_session)
            BaseII.CreateMyParameter("@CLV_CARTERA", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.ProcedimientoOutPut("GENERAMovimientosCartera")
            GloMovimientoCartera = CLng(BaseII.dicoPar("@CLV_CARTERA").ToString())
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub REPORTEMovimientosCartera(ByVal Clv_Cartera As Integer)
        Dim rDocument As New ReportDocument
        Dim dSet As New DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("REPORTEMovimientosCartera")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, Clv_Cartera)
        dSet = BaseII.ConsultaDS("REPORTEMovimientosCartera", tableNameList)

        rDocument.Load(RutaReportes + "\REPORTEMovimientosCartera.rpt")
        rDocument.SetDataSource(dSet)
        rDocument.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
        CrystalReportViewer1.ReportSource = rDocument
        rDocument.Dispose()
    End Sub

    Private Sub ELIMINAMovimientosCartera(ByVal Clv_Cartera As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Cartera", SqlDbType.Int, Clv_Cartera)
        BaseII.Inserta("ELIMINAMovimientosCartera")
    End Sub

    Private Sub MuestraTipSerPrincipal()
        BaseII.limpiaParametros()
        cbTipSer.DataSource = BaseII.ConsultaDT("MuestraTipSerPrincipal")
    End Sub

    Private Sub CATALOGOMovimientosCartera()
        BaseII.limpiaParametros()
        dgvCatalogo.DataSource = BaseII.ConsultaDT("CATALOGOMovimientosCartera")
    End Sub

    Private Sub FrmMovimientosCartera_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'If truncarReporte = False Then
        If BndMovimientoCartera = True Then
            BndMovimientoCartera = False
            REPORTEMovimientosCartera(GloMovimientoCartera)
        End If

        If bgMovCartera = True Then
            bgMovCartera = False

            GENERAMovimientosCartera(GloClv_TipSer, GloUsuario, 0)
            REPORTEMovimientosCartera(GloMovimientoCartera)
            If MessageBox.Show("¿Deseas guardar el movimiento de cartera?", "¡Atención!", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.No Then
                ELIMINAMovimientosCartera(GloMovimientoCartera)
            End If

        End If
        'Else
        '    truncarReporte = False
        'End If
    End Sub

    Private Sub FrmMovimientosCartera_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        MuestraTipSerPrincipal()
        CATALOGOMovimientosCartera()


    End Sub

    Private Sub dgvCatalogo_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCatalogo.CellClick
        GloClv_TipSer = cbTipSer.SelectedValue
        If dgvCatalogo.SelectedCells(0).Value = 0 Then
            'GENERAMovimientosCartera(GloClv_TipSer, GloUsuario, 0)
            'REPORTEMovimientosCartera(GloMovimientoCartera)
            'If MessageBox.Show("¿Deseas guardar el movimiento de cartera?", "¡Atención!", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.No Then
            '    ELIMINAMovimientosCartera(GloMovimientoCartera)
            'End If
            DameClaveSession()
            varfrmselcompania = "movimientoscartera"
            FrmSelCompaniaCartera.Show()
            'FrmSelCompania.Show()
        ElseIf dgvCatalogo.SelectedCells(0).Value = 1 Then
            BrwMovimientosCartera.Show()

        End If
    End Sub


    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub DameClaveSession()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameClv_Session", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            LocClv_session = parametro.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub cbTipSer_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbTipSer.SelectedIndexChanged

    End Sub
End Class