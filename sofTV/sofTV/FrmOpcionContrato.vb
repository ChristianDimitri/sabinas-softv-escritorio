﻿Public Class FrmOpcionContrato
    Public opcionLoc As Integer
    Public opcionAcc As Boolean
    Private Sub FrmOpcionContrato_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@nuevo", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ValidaReportePorFecha")
        If BaseII.dicoPar("@nuevo") = 1 Then
            RadioButton1.Text = "Solo hoja 1 y 5"
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        opcionAcc = False
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        If rbTodo.Checked Then
            opcionLoc = 1
        Else
            opcionLoc = 2
        End If
        opcionAcc = True
        Me.Close()
    End Sub
End Class