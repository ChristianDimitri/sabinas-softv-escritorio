﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambioTipoServicio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnEliminarCombinacion = New System.Windows.Forms.Button()
        Me.btnAgregaCombinacion = New System.Windows.Forms.Button()
        Me.dgvPosiblesCambios = New System.Windows.Forms.DataGridView()
        Me.ColClv_TipSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColClv_TipSerPosible = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColServicioP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmbPosibleCambio = New System.Windows.Forms.ComboBox()
        Me.cmbTipoServicio = New System.Windows.Forms.ComboBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnEliminaTrabajoAct = New System.Windows.Forms.Button()
        Me.cmbTrabajosActivacion = New System.Windows.Forms.ComboBox()
        Me.btnAgregaTrabajoAct = New System.Windows.Forms.Button()
        Me.dgvOrdenerActivacion = New System.Windows.Forms.DataGridView()
        Me.ColCombinación = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCLVTRABAJOACT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCLVTRABAJODES = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColClv_Trabajo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTRABAJO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDESCRIPCION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnEliminarTrabajoDes = New System.Windows.Forms.Button()
        Me.cmbTrabajosDesactivacion = New System.Windows.Forms.ComboBox()
        Me.btnAgregaTrabajoDes = New System.Windows.Forms.Button()
        Me.dgvOrdenesDesacivacion = New System.Windows.Forms.DataGridView()
        Me.ColIDCOMBINACION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVTRABAJOACT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVTRABAJODES = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Trabajo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRABAJO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIPCION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvPosiblesCambios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvOrdenerActivacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvOrdenesDesacivacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(212, 30)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tipo de Servicio:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(115, 71)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(221, 18)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Posible Cambio de Servicio:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.btnEliminarCombinacion)
        Me.Panel1.Controls.Add(Me.btnAgregaCombinacion)
        Me.Panel1.Controls.Add(Me.dgvPosiblesCambios)
        Me.Panel1.Controls.Add(Me.cmbPosibleCambio)
        Me.Panel1.Controls.Add(Me.cmbTipoServicio)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(16, 44)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(857, 351)
        Me.Panel1.TabIndex = 2
        '
        'btnEliminarCombinacion
        '
        Me.btnEliminarCombinacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminarCombinacion.Location = New System.Drawing.Point(604, 97)
        Me.btnEliminarCombinacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnEliminarCombinacion.Name = "btnEliminarCombinacion"
        Me.btnEliminarCombinacion.Size = New System.Drawing.Size(100, 28)
        Me.btnEliminarCombinacion.TabIndex = 8
        Me.btnEliminarCombinacion.Text = "Eliminar"
        Me.btnEliminarCombinacion.UseVisualStyleBackColor = True
        '
        'btnAgregaCombinacion
        '
        Me.btnAgregaCombinacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregaCombinacion.Location = New System.Drawing.Point(457, 97)
        Me.btnAgregaCombinacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregaCombinacion.Name = "btnAgregaCombinacion"
        Me.btnAgregaCombinacion.Size = New System.Drawing.Size(100, 28)
        Me.btnAgregaCombinacion.TabIndex = 7
        Me.btnAgregaCombinacion.Text = "Agregar"
        Me.btnAgregaCombinacion.UseVisualStyleBackColor = True
        '
        'dgvPosiblesCambios
        '
        Me.dgvPosiblesCambios.AllowUserToAddRows = False
        Me.dgvPosiblesCambios.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPosiblesCambios.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPosiblesCambios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPosiblesCambios.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColClv_TipSer, Me.ColClv_TipSerPosible, Me.ColID, Me.ColServicio, Me.ColServicioP})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPosiblesCambios.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvPosiblesCambios.Location = New System.Drawing.Point(97, 133)
        Me.dgvPosiblesCambios.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvPosiblesCambios.Name = "dgvPosiblesCambios"
        Me.dgvPosiblesCambios.ReadOnly = True
        Me.dgvPosiblesCambios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPosiblesCambios.Size = New System.Drawing.Size(641, 198)
        Me.dgvPosiblesCambios.TabIndex = 4
        '
        'ColClv_TipSer
        '
        Me.ColClv_TipSer.DataPropertyName = "Clv_TipSer"
        Me.ColClv_TipSer.HeaderText = "Clv_TipSer"
        Me.ColClv_TipSer.Name = "ColClv_TipSer"
        Me.ColClv_TipSer.ReadOnly = True
        Me.ColClv_TipSer.Visible = False
        '
        'ColClv_TipSerPosible
        '
        Me.ColClv_TipSerPosible.DataPropertyName = "Clv_TipSerPosible"
        Me.ColClv_TipSerPosible.HeaderText = "Clv_TipSerPosible"
        Me.ColClv_TipSerPosible.Name = "ColClv_TipSerPosible"
        Me.ColClv_TipSerPosible.ReadOnly = True
        Me.ColClv_TipSerPosible.Visible = False
        '
        'ColID
        '
        Me.ColID.DataPropertyName = "ID"
        Me.ColID.HeaderText = "ID"
        Me.ColID.Name = "ColID"
        Me.ColID.ReadOnly = True
        '
        'ColServicio
        '
        Me.ColServicio.DataPropertyName = "Servicio"
        Me.ColServicio.HeaderText = "Servicio"
        Me.ColServicio.Name = "ColServicio"
        Me.ColServicio.ReadOnly = True
        Me.ColServicio.Width = 165
        '
        'ColServicioP
        '
        Me.ColServicioP.DataPropertyName = "ServicioP"
        Me.ColServicioP.HeaderText = "Servicio Posible"
        Me.ColServicioP.Name = "ColServicioP"
        Me.ColServicioP.ReadOnly = True
        Me.ColServicioP.Width = 165
        '
        'cmbPosibleCambio
        '
        Me.cmbPosibleCambio.DisplayMember = "Concepto"
        Me.cmbPosibleCambio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPosibleCambio.FormattingEnabled = True
        Me.cmbPosibleCambio.Location = New System.Drawing.Point(399, 62)
        Me.cmbPosibleCambio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbPosibleCambio.Name = "cmbPosibleCambio"
        Me.cmbPosibleCambio.Size = New System.Drawing.Size(325, 26)
        Me.cmbPosibleCambio.TabIndex = 3
        Me.cmbPosibleCambio.ValueMember = "Clv_TipSer"
        '
        'cmbTipoServicio
        '
        Me.cmbTipoServicio.DisplayMember = "Concepto"
        Me.cmbTipoServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTipoServicio.FormattingEnabled = True
        Me.cmbTipoServicio.Location = New System.Drawing.Point(399, 20)
        Me.cmbTipoServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbTipoServicio.Name = "cmbTipoServicio"
        Me.cmbTipoServicio.Size = New System.Drawing.Size(325, 26)
        Me.cmbTipoServicio.TabIndex = 2
        Me.cmbTipoServicio.ValueMember = "Clv_TipSer"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.btnEliminaTrabajoAct)
        Me.Panel2.Controls.Add(Me.cmbTrabajosActivacion)
        Me.Panel2.Controls.Add(Me.btnAgregaTrabajoAct)
        Me.Panel2.Controls.Add(Me.dgvOrdenerActivacion)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(16, 402)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(857, 267)
        Me.Panel2.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(529, 98)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(153, 18)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Selecciona Trabajo"
        '
        'btnEliminaTrabajoAct
        '
        Me.btnEliminaTrabajoAct.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminaTrabajoAct.Location = New System.Drawing.Point(733, 156)
        Me.btnEliminaTrabajoAct.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnEliminaTrabajoAct.Name = "btnEliminaTrabajoAct"
        Me.btnEliminaTrabajoAct.Size = New System.Drawing.Size(100, 28)
        Me.btnEliminaTrabajoAct.TabIndex = 6
        Me.btnEliminaTrabajoAct.Text = "Eliminar"
        Me.btnEliminaTrabajoAct.UseVisualStyleBackColor = True
        '
        'cmbTrabajosActivacion
        '
        Me.cmbTrabajosActivacion.DisplayMember = "DESCRIPCION"
        Me.cmbTrabajosActivacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTrabajosActivacion.FormattingEnabled = True
        Me.cmbTrabajosActivacion.Location = New System.Drawing.Point(527, 121)
        Me.cmbTrabajosActivacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbTrabajosActivacion.Name = "cmbTrabajosActivacion"
        Me.cmbTrabajosActivacion.Size = New System.Drawing.Size(325, 26)
        Me.cmbTrabajosActivacion.TabIndex = 7
        Me.cmbTrabajosActivacion.ValueMember = "Clv_Trabajo"
        '
        'btnAgregaTrabajoAct
        '
        Me.btnAgregaTrabajoAct.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregaTrabajoAct.Location = New System.Drawing.Point(587, 156)
        Me.btnAgregaTrabajoAct.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregaTrabajoAct.Name = "btnAgregaTrabajoAct"
        Me.btnAgregaTrabajoAct.Size = New System.Drawing.Size(100, 28)
        Me.btnAgregaTrabajoAct.TabIndex = 5
        Me.btnAgregaTrabajoAct.Text = "Agregar"
        Me.btnAgregaTrabajoAct.UseVisualStyleBackColor = True
        '
        'dgvOrdenerActivacion
        '
        Me.dgvOrdenerActivacion.AllowUserToAddRows = False
        Me.dgvOrdenerActivacion.AllowUserToDeleteRows = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOrdenerActivacion.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvOrdenerActivacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOrdenerActivacion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColCombinación, Me.ColCLVTRABAJOACT, Me.ColCLVTRABAJODES, Me.ColClv_Trabajo, Me.ColTRABAJO, Me.ColDESCRIPCION})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvOrdenerActivacion.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvOrdenerActivacion.Location = New System.Drawing.Point(4, 34)
        Me.dgvOrdenerActivacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvOrdenerActivacion.Name = "dgvOrdenerActivacion"
        Me.dgvOrdenerActivacion.ReadOnly = True
        Me.dgvOrdenerActivacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOrdenerActivacion.Size = New System.Drawing.Size(515, 208)
        Me.dgvOrdenerActivacion.TabIndex = 5
        '
        'ColCombinación
        '
        Me.ColCombinación.DataPropertyName = "IDCOMBINACION"
        Me.ColCombinación.HeaderText = "IDCOMBINACION"
        Me.ColCombinación.Name = "ColCombinación"
        Me.ColCombinación.ReadOnly = True
        Me.ColCombinación.Visible = False
        '
        'ColCLVTRABAJOACT
        '
        Me.ColCLVTRABAJOACT.DataPropertyName = "CLVTRABAJOACT"
        Me.ColCLVTRABAJOACT.HeaderText = "CLVTRABAJOACT"
        Me.ColCLVTRABAJOACT.Name = "ColCLVTRABAJOACT"
        Me.ColCLVTRABAJOACT.ReadOnly = True
        Me.ColCLVTRABAJOACT.Visible = False
        '
        'ColCLVTRABAJODES
        '
        Me.ColCLVTRABAJODES.DataPropertyName = "CLVTRABAJODES"
        Me.ColCLVTRABAJODES.HeaderText = "CLVTRABAJODES"
        Me.ColCLVTRABAJODES.Name = "ColCLVTRABAJODES"
        Me.ColCLVTRABAJODES.ReadOnly = True
        Me.ColCLVTRABAJODES.Visible = False
        '
        'ColClv_Trabajo
        '
        Me.ColClv_Trabajo.DataPropertyName = "Clv_Trabajo"
        Me.ColClv_Trabajo.HeaderText = "Clave Trabajo"
        Me.ColClv_Trabajo.Name = "ColClv_Trabajo"
        Me.ColClv_Trabajo.ReadOnly = True
        Me.ColClv_Trabajo.Width = 70
        '
        'ColTRABAJO
        '
        Me.ColTRABAJO.DataPropertyName = "TRABAJO"
        Me.ColTRABAJO.HeaderText = "Trabajo"
        Me.ColTRABAJO.Name = "ColTRABAJO"
        Me.ColTRABAJO.ReadOnly = True
        Me.ColTRABAJO.Width = 70
        '
        'ColDESCRIPCION
        '
        Me.ColDESCRIPCION.DataPropertyName = "DESCRIPCION"
        Me.ColDESCRIPCION.HeaderText = "Descripción"
        Me.ColDESCRIPCION.Name = "ColDESCRIPCION"
        Me.ColDESCRIPCION.ReadOnly = True
        Me.ColDESCRIPCION.Width = 200
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(4, 12)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(390, 18)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Ordenes Generadas para la Activación del Servicio"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.btnEliminarTrabajoDes)
        Me.Panel3.Controls.Add(Me.cmbTrabajosDesactivacion)
        Me.Panel3.Controls.Add(Me.btnAgregaTrabajoDes)
        Me.Panel3.Controls.Add(Me.dgvOrdenesDesacivacion)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Location = New System.Drawing.Point(16, 677)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(857, 255)
        Me.Panel3.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(529, 90)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(153, 18)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Selecciona Trabajo"
        '
        'btnEliminarTrabajoDes
        '
        Me.btnEliminarTrabajoDes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminarTrabajoDes.Location = New System.Drawing.Point(733, 148)
        Me.btnEliminarTrabajoDes.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnEliminarTrabajoDes.Name = "btnEliminarTrabajoDes"
        Me.btnEliminarTrabajoDes.Size = New System.Drawing.Size(100, 28)
        Me.btnEliminarTrabajoDes.TabIndex = 10
        Me.btnEliminarTrabajoDes.Text = "Eliminar"
        Me.btnEliminarTrabajoDes.UseVisualStyleBackColor = True
        '
        'cmbTrabajosDesactivacion
        '
        Me.cmbTrabajosDesactivacion.DisplayMember = "DESCRIPCION"
        Me.cmbTrabajosDesactivacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTrabajosDesactivacion.FormattingEnabled = True
        Me.cmbTrabajosDesactivacion.Location = New System.Drawing.Point(527, 112)
        Me.cmbTrabajosDesactivacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbTrabajosDesactivacion.Name = "cmbTrabajosDesactivacion"
        Me.cmbTrabajosDesactivacion.Size = New System.Drawing.Size(325, 26)
        Me.cmbTrabajosDesactivacion.TabIndex = 11
        Me.cmbTrabajosDesactivacion.ValueMember = "Clv_Trabajo"
        '
        'btnAgregaTrabajoDes
        '
        Me.btnAgregaTrabajoDes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregaTrabajoDes.Location = New System.Drawing.Point(587, 148)
        Me.btnAgregaTrabajoDes.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregaTrabajoDes.Name = "btnAgregaTrabajoDes"
        Me.btnAgregaTrabajoDes.Size = New System.Drawing.Size(100, 28)
        Me.btnAgregaTrabajoDes.TabIndex = 9
        Me.btnAgregaTrabajoDes.Text = "Agregar"
        Me.btnAgregaTrabajoDes.UseVisualStyleBackColor = True
        '
        'dgvOrdenesDesacivacion
        '
        Me.dgvOrdenesDesacivacion.AllowUserToAddRows = False
        Me.dgvOrdenesDesacivacion.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOrdenesDesacivacion.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvOrdenesDesacivacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOrdenesDesacivacion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColIDCOMBINACION, Me.CLVTRABAJOACT, Me.CLVTRABAJODES, Me.Clv_Trabajo, Me.TRABAJO, Me.DESCRIPCION})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvOrdenesDesacivacion.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvOrdenesDesacivacion.Location = New System.Drawing.Point(4, 28)
        Me.dgvOrdenesDesacivacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvOrdenesDesacivacion.Name = "dgvOrdenesDesacivacion"
        Me.dgvOrdenesDesacivacion.ReadOnly = True
        Me.dgvOrdenesDesacivacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOrdenesDesacivacion.Size = New System.Drawing.Size(515, 208)
        Me.dgvOrdenesDesacivacion.TabIndex = 5
        '
        'ColIDCOMBINACION
        '
        Me.ColIDCOMBINACION.DataPropertyName = "IDCOMBINACION"
        Me.ColIDCOMBINACION.HeaderText = "IDCOMBINACION"
        Me.ColIDCOMBINACION.Name = "ColIDCOMBINACION"
        Me.ColIDCOMBINACION.ReadOnly = True
        Me.ColIDCOMBINACION.Visible = False
        '
        'CLVTRABAJOACT
        '
        Me.CLVTRABAJOACT.DataPropertyName = "CLVTRABAJOACT"
        Me.CLVTRABAJOACT.HeaderText = "CLVTRABAJOACT"
        Me.CLVTRABAJOACT.Name = "CLVTRABAJOACT"
        Me.CLVTRABAJOACT.ReadOnly = True
        Me.CLVTRABAJOACT.Visible = False
        '
        'CLVTRABAJODES
        '
        Me.CLVTRABAJODES.DataPropertyName = "CLVTRABAJODES"
        Me.CLVTRABAJODES.HeaderText = "CLVTRABAJODES"
        Me.CLVTRABAJODES.Name = "CLVTRABAJODES"
        Me.CLVTRABAJODES.ReadOnly = True
        Me.CLVTRABAJODES.Visible = False
        '
        'Clv_Trabajo
        '
        Me.Clv_Trabajo.DataPropertyName = "Clv_Trabajo"
        Me.Clv_Trabajo.HeaderText = "Clave Trabajo"
        Me.Clv_Trabajo.Name = "Clv_Trabajo"
        Me.Clv_Trabajo.ReadOnly = True
        Me.Clv_Trabajo.Width = 70
        '
        'TRABAJO
        '
        Me.TRABAJO.DataPropertyName = "TRABAJO"
        Me.TRABAJO.HeaderText = "Trabajo"
        Me.TRABAJO.Name = "TRABAJO"
        Me.TRABAJO.ReadOnly = True
        Me.TRABAJO.Width = 70
        '
        'DESCRIPCION
        '
        Me.DESCRIPCION.DataPropertyName = "DESCRIPCION"
        Me.DESCRIPCION.HeaderText = "Descripcón"
        Me.DESCRIPCION.Name = "DESCRIPCION"
        Me.DESCRIPCION.ReadOnly = True
        Me.DESCRIPCION.Width = 200
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(4, 6)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(419, 18)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Ordenes Generadas para la Desactivación del Servicio"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(688, 939)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(181, 44)
        Me.btnSalir.TabIndex = 37
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'FrmCambioTipoServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(889, 994)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmCambioTipoServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configuración de Cambio de Tipos de Servicios"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvPosiblesCambios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvOrdenerActivacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgvOrdenesDesacivacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvPosiblesCambios As System.Windows.Forms.DataGridView
    Friend WithEvents cmbPosibleCambio As System.Windows.Forms.ComboBox
    Friend WithEvents cmbTipoServicio As System.Windows.Forms.ComboBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvOrdenerActivacion As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents dgvOrdenesDesacivacion As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnEliminarCombinacion As System.Windows.Forms.Button
    Friend WithEvents btnAgregaCombinacion As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnEliminaTrabajoAct As System.Windows.Forms.Button
    Friend WithEvents cmbTrabajosActivacion As System.Windows.Forms.ComboBox
    Friend WithEvents btnAgregaTrabajoAct As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnEliminarTrabajoDes As System.Windows.Forms.Button
    Friend WithEvents cmbTrabajosDesactivacion As System.Windows.Forms.ComboBox
    Friend WithEvents btnAgregaTrabajoDes As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ColClv_TipSer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColClv_TipSerPosible As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColServicioP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ColCombinación As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColCLVTRABAJOACT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColCLVTRABAJODES As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColClv_Trabajo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTRABAJO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDESCRIPCION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColIDCOMBINACION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVTRABAJOACT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVTRABAJODES As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Trabajo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRABAJO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCRIPCION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
