﻿Imports System.Data.SqlClient
Imports sofTV.BAL
Imports System.Collections.Generic

Public Class FrmSucursales
    Public RFC As String = Nothing
    Public LocIdCompania As Integer = 0
    Public eFila As Integer = 0
    Private Descripcion As String = Nothing
    Private Ip As String = Nothing
    Private Impresora As String = Nothing

    Private Sub Llena_Estructura_Grid()

        DataGridView1.Columns.Add("Clvid", "Clvid")
        DataGridView1.Columns.Item("Clvid").Width = 5
        DataGridView1.Columns.Item("Clvid").ReadOnly = True

        DataGridView1.Columns.Add("id_compania", "id_compania")
        DataGridView1.Columns.Item("id_compania").Width = 5
        DataGridView1.Columns.Item("id_compania").ReadOnly = True

        DataGridView1.Columns.Add("razon_social", "razon_social")
        DataGridView1.Columns.Item("razon_social").Width = 300
        DataGridView1.Columns.Item("razon_social").ReadOnly = True

        DataGridView1.Columns.Add("ClvEq", "ClvEq")
        DataGridView1.Columns.Item("ClvEq").Width = 80
        DataGridView1.Columns.Add("Serie", "Serie")
        DataGridView1.Columns.Item("Serie").Width = 80
        DataGridView1.Columns.Add("Folio", "Folio")
        DataGridView1.Columns.Item("Folio").Width = 80
        DataGridView1.Columns.Add("SerieG", "SerieG")
        DataGridView1.Columns.Item("SerieG").Width = 80
        DataGridView1.Columns.Add("FolioG", "FolioG")
        DataGridView1.Columns.Item("FolioG").Width = 80
        DataGridView1.Columns.Add("SerieD", "SerieD")
        DataGridView1.Columns.Item("SerieD").Width = 80
        DataGridView1.Columns.Add("FolioD", "FolioD")
        DataGridView1.Columns.Item("FolioD").Width = 80
        DataGridView1.Columns.Add("Rfc", "Rfc")
        DataGridView1.Columns.Item("Rfc").Width = 5

    End Sub

    Private Sub Llena_companias()
        Try


            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                Dim i As Integer = 0
                For i = 0 To ComboBoxCompanias.Items.Count - 2
                    DataGridView1.Rows.Add()
                Next
            End If
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub SP_Mizar_MuestraRFC()
        Dim conn As New SqlConnection(MiConexion)
        Try
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("SP_Mizar_MuestraRFC", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando
            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "SP_Mizar_MuestraRFC")
            Bs.DataSource = Dataset.Tables("SP_Mizar_MuestraRFC")
            'dgvResultadosClasificacion.DataSource = Bs
            ComboBoxRFC.DataSource = Bs
            ComboBoxRFC.DisplayMember = "RFC"
            ComboBoxRFC.ValueMember = "id_compania"

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conn.Close()
        End Try
    End Sub


    Private Sub RFC_CFD(ByVal eRFC As String)
        Dim conn As New SqlConnection(MiConexion)
        Try

            conn.Open()
            Dim comando As New SqlClient.SqlCommand("Dame_Serire_Folio_RFC", conn)
            comando.CommandType = CommandType.StoredProcedure
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            'Agrego el parámetro
            Adaptador.SelectCommand.Parameters.Add("@RFC", SqlDbType.VarChar, 50).Value = eRFC

            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "Dame_Serire_Folio_RFC")
            Bs.DataSource = Dataset.Tables("Dame_Serire_Folio_RFC")

            'dgvResultadosClasificacion.DataSource = Bs
            ComboBox1.DataSource = Bs
            ComboBox1.DisplayMember = "Serie_Folio"
            ComboBox1.ValueMember = "Clave"
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conn.Close()
        End Try
    End Sub

    Public Sub Guarda_Rel_Ciudad_Ciudad(ByVal eClv_Sucursal As Integer, ByVal eClv_Ciudad As Integer, ByVal eClave_SerieFolio As String, ByVal eMatriz As Boolean, ByVal oId_Compania As Long)

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Insetra_Rel_Sucursal_Ciudad"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm1 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                prm1.Value = eClv_Sucursal
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Clv_Ciudad", SqlDbType.Int)
                prm2.Value = eClv_Ciudad
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@Clave_SerieFolio", SqlDbType.VarChar, 50)
                prm3.Value = eClave_SerieFolio
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Matriz", SqlDbType.Bit)
                prm4.Value = eMatriz
                .Parameters.Add(prm4)

                Dim prm5 As New SqlParameter("@Id_compania", SqlDbType.Int)
                prm5.Value = oId_Compania
                .Parameters.Add(prm5)


                Dim i As Integer = .ExecuteNonQuery()
                'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con éxito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With
            CON80.Close()
        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Sub

    Private Sub Muestra_Rel_Ciudad_Ciudad(ByVal eClv_Sucursal As Integer)

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Muestra_Rel_Sucursal_Ciudad"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm2 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                prm2.Value = eClv_Sucursal
                .Parameters.Add(prm2)

                Dim prm1 As New SqlParameter("@Clv_Ciudad", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@Clave_SerieFolio", SqlDbType.VarChar, 50)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Matriz", SqlDbType.Bit)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)

                Dim i As Integer = .ExecuteNonQuery()
                Ciudadcmb.SelectedValue = prm1.Value
                'Dame_RFC_Por_Ciudad(prm1.Value)
                'RFC_CFD(RFC)
                ComboBox1.SelectedValue = prm3.Value
                MatrizChck.CheckState = CheckState.Unchecked
                If IsNumeric(prm4.Value) = True Then
                    If prm4.Value = True Then MatrizChck.CheckState = CheckState.Checked
                End If
                'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con éxito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With

        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Sub


    Public Function Valida_Sucursal_Matriz(ByVal eClv_Sucursal As Integer, ByVal eClv_Ciudad As Integer) As Integer

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Valida_Sucursal_Matriz = 0
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Valida_Sucursal_Matriz"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm2 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                prm2.Value = eClv_Sucursal
                .Parameters.Add(prm2)

                Dim prm1 As New SqlParameter("@Clv_Ciudad", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = eClv_Ciudad
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@Bnd", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim i As Integer = .ExecuteNonQuery()
                Valida_Sucursal_Matriz = prm3.Value
            End With

        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Function

    Private Sub Dame_RFC_Por_Ciudad(ByVal eClv_Ciudad As Integer)

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        RFC = ""
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Dame_RFC_Por_Ciudad"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm2 As New SqlParameter("@Clv_Ciudad", SqlDbType.Int)
                prm2.Value = eClv_Ciudad
                .Parameters.Add(prm2)

                Dim prm1 As New SqlParameter("@RFC", SqlDbType.VarChar, 50)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)



                Dim i As Integer = .ExecuteNonQuery()
                RFC = prm1.Value

                'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con éxito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With

        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Sub

    'Private Sub Llena_Combo()
    '    'Me.CiudadCmb.DataSource = Nothing        
    '    Dim lciudad As List(Of Ciudad) = Ciudad.GetAll()
    '    lciudad.Insert(0, New Ciudad(-1, "< Seleccione la Ciudad >", 0))
    '    Me.Ciudadcmb.DataSource = lciudad
    '    Me.Ciudadcmb.ValueMember = "Clv_Ciudad"
    '    Me.Ciudadcmb.DisplayMember = "Nombre"
    'End Sub



    Private Sub CONSUCURSALESBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSUCURSALESBindingNavigatorSaveItem.Click

        If IsNumeric(Ciudadcmb.SelectedValue) = False And Ciudadcmb.Items.Count > 0 Then
            MsgBox("Seleccione una Ciudad de la Lista ", vbInformation)
            Exit Sub
        End If
        If Ciudadcmb.SelectedValue <= 0 And Ciudadcmb.Items.Count > 0 Then
            MsgBox("Seleccione una Ciudad de la Lista ", vbInformation)
            Exit Sub
        End If
        If ComboBox1.SelectedIndex <= 0 And ComboBox1.Items.Count > 0 Then
            MsgBox("Seleccione la Series y Folio de la Lista ", vbInformation)
            Exit Sub
        End If
        If MatrizChck.CheckState = CheckState.Checked Then
            If Valida_Sucursal_Matriz(Clv_SucursalTextBox.Text, Ciudadcmb.SelectedValue) = 0 Then
                'MatrizChck.CheckState = CheckState.Unchecked
                MsgBox("Ya Existe una Sucursal que la Matriz ", vbInformation)
                Exit Sub
            End If
        End If

        If Me.txtCalle.Text.Length = 0 Then
            MsgBox("¡Capture la Calle de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtNumero.Text.Length = 0 Then
            MsgBox("¡Capture el Número de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtColonia.Text.Length = 0 Then
            MsgBox("¡Capture la Colonia de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtCP.Text.Length = 0 Then
            MsgBox("¡Capture el CP de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtMunicipio.Text.Length = 0 Then
            MsgBox("¡Capture el Municipio de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtCiudad.Text.Length = 0 Then
            MsgBox("¡Capture la Ciudad de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        Else
            If opcion <> "N" Then
                insertaDatosGeneralesSucursal(gloClave, Me.txtCalle.Text, Me.txtNumero.Text, Me.txtColonia.Text, Me.txtCP.Text, Me.txtMunicipio.Text, _
                                         Me.txtCiudad.Text, Me.txtTelefono.Text)
            End If
           
        End If

        Dim locerror As Integer = 0
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.No_folioTextBox.Text) = False Then Me.No_folioTextBox.Text = 0
            'Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
            'Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)
            If locerror = 0 Then
                If IsNumeric(Me.UltimoFolioUsadoTextBox.Text) = False Then Me.UltimoFolioUsadoTextBox.Text = 0
                Me.Validate()
                Me.CONSUCURSALESBindingSource.EndEdit()
                Me.CONSUCURSALESTableAdapter.Connection = CON
                Me.CONSUCURSALESTableAdapter.Update(Me.NewSofTvDataSet.CONSUCURSALES)
                Me.Inserta_impresora_sucursalTableAdapter1.Connection = CON
                Me.Inserta_impresora_sucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.inserta_impresora_sucursal, CInt(Me.Clv_SucursalTextBox.Text), Me.Impresora_ContratosTextBox.Text, Me.Impresora_TarjetasTextBox.Text, Me.TextBox1.Text)
                Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
                Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)
                If opcion = "N" Then
                    insertaDatosGeneralesSucursal(Me.Clv_SucursalTextBox.Text, Me.txtCalle.Text, Me.txtNumero.Text, Me.txtColonia.Text, Me.txtCP.Text, Me.txtMunicipio.Text, _
                                         Me.txtCiudad.Text, Me.txtTelefono.Text)
                End If
                If IsNumeric(Ciudadcmb.SelectedValue) = True Then
                    If Ciudadcmb.SelectedValue > 0 Then
                        If MatrizChck.CheckState = CheckState.Checked Then
                            LocIdCompania = 0
                            If ComboBoxRFC.SelectedIndex > 0 Then
                                If IsNumeric(ComboBoxRFC.SelectedValue) = True Then
                                    LocIdCompania = ComboBoxRFC.SelectedValue
                                End If
                            End If
                            Guarda_Rel_Ciudad_Ciudad(Clv_SucursalTextBox.Text, Ciudadcmb.SelectedValue, ComboBox1.SelectedValue, True, LocIdCompania)
                        Else
                            LocIdCompania = 0
                            If ComboBoxRFC.SelectedIndex > 0 Then
                                If IsNumeric(ComboBoxRFC.SelectedValue) = True Then
                                    LocIdCompania = ComboBoxRFC.SelectedValue
                                End If
                            End If
                            Guarda_Rel_Ciudad_Ciudad(Clv_SucursalTextBox.Text, Ciudadcmb.SelectedValue, ComboBox1.SelectedValue, False, LocIdCompania)
                        End If

                    End If
                End If
                Guarda_Detalle()
                bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Creación Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
                MsgBox(mensaje5)
                GloBnd = True
                CON.Close()
                Me.Close()
            Else
                MsgBox("La Serie de Factura Global ya Existe no se puede Guardar", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            'MsgBox("La Serie ya Existe no se Puede Guardar")
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONSUCURSALESTableAdapter.Connection = CON
            Me.CONSUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.CONSUCURSALES, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            Me.Consulta_Impresora_SucursalTableAdapter1.Connection = CON
            Me.Consulta_Impresora_SucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.Consulta_Impresora_Sucursal, CLAVE)
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Connection = CON
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Consulta_Generales_FacturasGlobales, CLAVE)
            CON.Close()

            Muestra_Rel_Ciudad_Ciudad(CLAVE)
            'If (pos) > 0 Then
            '    Ciudadcmb.SelectedValue = pos
            'End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmSucursales_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmSucursales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Llena_Estructura_Grid()
        Llena_companias()

        'Llena_Combo()
        'SP_Mizar_MuestraRFC()
        If opcion = "N" Then
            Me.CONSUCURSALESBindingSource.AddNew()
            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
            consultaDatosGeneralesSucursal(gloClave, 0)
            LLena_DetalleSuc(gloClave)
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
            consultaDatosGeneralesSucursal(gloClave, 0)
            LLena_DetalleSuc(gloClave)
            If NombreTextBox.Text = "Puntos Web" Then
                SerieTextBox1.ReadOnly = True
                No_folioTextBox.ReadOnly = True
            End If
        End If
    End Sub

    Private Sub Clv_SucursalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SucursalTextBox.TextChanged
        If Len(eMsj) > 0 Then
            Me.Clv_SucursalTextBox.Text = gloClave
        End If
        gloClave = Me.Clv_SucursalTextBox.Text
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click

        ValidaEliminarRelCompaniasucursal(2)
        If Len(eMsj) > 0 Then
            MsgBox(eMsj, MsgBoxStyle.Information)
            Descripcion = NombreTextBox.Text
            Ip = IPTextBox.Text
            Impresora = ImpresoraTextBox.Text
            Exit Sub
        End If

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONSUCURSALESTableAdapter.Connection = CON
        Me.CONSUCURSALESTableAdapter.Delete(gloClave)
        Me.Borra_Impresora_SucursalesTableAdapter.Connection = CON
        Me.Borra_Impresora_SucursalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Impresora_Sucursales, gloClave)
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Connection = CON
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Generales_FacturasGlobales, gloClave)
        bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Eliminó Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
        CON.Close()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub




    Private Sub Ciudadcmb_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ciudadcmb.SelectedValueChanged
        'If IsNumeric(Ciudadcmb.SelectedValue) = True Then
        '    If Ciudadcmb.SelectedValue > 0 Then
        '        Dame_RFC_Por_Ciudad(Ciudadcmb.SelectedValue)
        '        RFC_CFD(RFC)
        '    End If
        'End If
    End Sub

    Private Sub Ciudadcmb_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ciudadcmb.SelectedIndexChanged

    End Sub

    Private Sub ComboBoxRFC_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxRFC.SelectedIndexChanged

    End Sub

    Private Sub ComboBoxRFC_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBoxRFC.SelectedValueChanged
        'If IsNumeric(ComboBoxRFC.SelectedValue) = True Then
        '    If ComboBoxRFC.SelectedValue > 0 Then
        '        'Dame_RFC_Por_Ciudad(ComboBoxRFC.SelectedValue)
        '        RFC = ComboBoxRFC.Text
        '        RFC_CFD(RFC)
        '    End If
        'End If
    End Sub

    Private Sub Label13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub insertaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmCalle As String, ByVal prmNumero As String, ByVal prmColonia As String, ByVal prmCp As String, _
                                              ByVal prmMunicipio As String, ByVal prmCiudad As String, ByVal prmTelefono As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, prmCalle, 250)
            BaseII.CreateMyParameter("@numero", SqlDbType.VarChar, prmNumero, 100)
            BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, prmColonia, 250)
            BaseII.CreateMyParameter("@cp", SqlDbType.VarChar, prmCp, 50)
            BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, prmMunicipio, 250)
            BaseII.CreateMyParameter("@ciudad", SqlDbType.VarChar, prmCiudad, 250)
            BaseII.CreateMyParameter("@telefono", SqlDbType.VarChar, prmTelefono, 250)

            BaseII.Inserta("uspInsertaTblRelSucursalDatosGenerales")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub consultaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmClvFactura As Integer)

        Dim dtDatosGenerales As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
        BaseII.CreateMyParameter("@clvFactura", SqlDbType.Int, prmClvFactura)
        dtDatosGenerales = BaseII.ConsultaDT("uspConsultaTblRelSucursalDatosGenerales")


        If dtDatosGenerales.Rows.Count > 0 Then
            Me.txtCalle.Text = dtDatosGenerales.Rows(0)("calle").ToString
            Me.txtNumero.Text = dtDatosGenerales.Rows(0)("numero").ToString
            Me.txtColonia.Text = dtDatosGenerales.Rows(0)("colonia").ToString
            Me.txtCP.Text = dtDatosGenerales.Rows(0)("cp").ToString
            Me.txtMunicipio.Text = dtDatosGenerales.Rows(0)("municipio").ToString
            Me.txtCiudad.Text = dtDatosGenerales.Rows(0)("ciudad").ToString
            Me.txtTelefono.Text = dtDatosGenerales.Rows(0)("telefono").ToString
        End If
    End Sub

    Public Sub SP_GUARDARRel_SucursalCompania(ByVal oClv_Sucursa As Integer, oid_compania As Integer, oClvEq As String, oSerie As String, oFolio As Integer, oSerieG As String, FolioG As Integer, oSerieD As String, FolioD As Integer, oRfc As String)
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            '@Clv_Sucursal int,
            '@id_compania Int,	
            '@ClvEq Varchar(50),
            '@Serie Varchar(50),
            '@Folio int,
            '@SerieG Varchar(50),
            '@FolioG int,
            '@SerieD Varchar(50),
            '@FolioD int,
            '@Rfc Varchar(50)
            BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, oClv_Sucursa)
            BaseII.CreateMyParameter("@id_compania", SqlDbType.Int, oid_compania)
            BaseII.CreateMyParameter("@ClvEq", SqlDbType.VarChar, oClvEq, 50)
            BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, oSerie, 50)
            BaseII.CreateMyParameter("@Folio", SqlDbType.VarChar, oFolio)
            BaseII.CreateMyParameter("@SerieG", SqlDbType.VarChar, oSerieG, 50)
            BaseII.CreateMyParameter("@FolioG", SqlDbType.VarChar, FolioG)
            BaseII.CreateMyParameter("@SerieD", SqlDbType.VarChar, oSerieD, 50)
            BaseII.CreateMyParameter("@FolioD", SqlDbType.VarChar, FolioD)
            BaseII.CreateMyParameter("@Rfc", SqlDbType.VarChar, oRfc)
            BaseII.Inserta("SP_GUARDARRel_SucursalCompania")
            'MsgBox("Se guardo con éxito", MsgBoxStyle.Information, )
            'ComboBoxCartera.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ButtonAgregar_Click(sender As System.Object, e As System.EventArgs) Handles ButtonAgregar.Click
        If ComboBoxCompanias.SelectedIndex <= 0 Then
            MsgBox("Seleccione la Compañía", MsgBoxStyle.Information, "Importante")
            Exit Sub
        End If
        If Len(Clv_EquivalenteTextBox.Text) = 0 Or (Len(SerieTextBox.Text) = 0 Or IsNumeric(SerieTextBox.Text) = True) Or (Len(SerieTextBox1.Text) = 0 Or IsNumeric(SerieTextBox.Text) = True) Or IsNumeric(UltimoFolioUsadoTextBox.Text) = False Or IsNumeric(No_folioTextBox.Text) = False Then
            MsgBox("Ingrese todos los campos antes de agregar", MsgBoxStyle.Information, "Importante")
            Exit Sub
        End If
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim Bnd As Boolean = False
        For i = 0 To DataGridView1.RowCount - 1
            If CStr(DataGridView1.Item(1, i).Value) = ComboBoxCompanias.SelectedValue.ToString Then
                MsgBox("Ya existe la compañía en la lista")
                Clv_EquivalenteTextBox.Text = ""
                SerieTextBox.Text = ""
                UltimoFolioUsadoTextBox.Text = ""
                SerieTextBox1.Text = ""
                No_folioTextBox.Text = ""
                SerieFDTextBox.Text = ""
                FolioFDTextbox.Text = ""
                Exit Sub
            End If
        Next
        For i = 0 To DataGridView1.RowCount - 1
            If CStr(DataGridView1.Item(1, i).Value) = ComboBoxCompanias.SelectedValue.ToString Then
                MsgBox("Ya existe la compañía en la lista")
                Exit For
            End If
            If CStr(DataGridView1.Item(0, i).Value) = "" Then
                j = i
                Bnd = True
                Exit For
            End If
        Next
        If Bnd = True Then
            DataGridView1.Item(0, j).Value = 0
            DataGridView1.Item(1, j).Value = ComboBoxCompanias.SelectedValue
            DataGridView1.Item(2, j).Value = CStr(ComboBoxCompanias.Text)
            DataGridView1.Item(3, j).Value = CStr(Clv_EquivalenteTextBox.Text)
            DataGridView1.Item(4, j).Value = CStr(SerieTextBox.Text)
            DataGridView1.Item(5, j).Value = CStr(UltimoFolioUsadoTextBox.Text)
            DataGridView1.Item(6, j).Value = CStr(SerieTextBox1.Text)
            DataGridView1.Item(7, j).Value = CStr(No_folioTextBox.Text)
            DataGridView1.Item(8, j).Value = CStr(SerieFDTextBox.Text)
            DataGridView1.Item(9, j).Value = CStr(FolioFDTextbox.Text)
        End If
        Clv_EquivalenteTextBox.Text = ""
        SerieTextBox.Text = ""
        UltimoFolioUsadoTextBox.Text = ""
        SerieTextBox1.Text = ""
        No_folioTextBox.Text = ""
        SerieFDTextBox.Text = ""
        FolioFDTextbox.Text = ""
    End Sub

    Private Sub ButtonEliminar_Click(sender As System.Object, e As System.EventArgs) Handles ButtonEliminar.Click

        ValidaEliminarRelCompaniasucursal(1)
        If Len(eMsj) > 0 Then
            MsgBox(eMsj, MsgBoxStyle.Information)
            Exit Sub
        End If

        DataGridView1.Item(0, eFila).Value = ""
        DataGridView1.Item(1, eFila).Value = ""
        DataGridView1.Item(2, eFila).Value = ""
        DataGridView1.Item(3, eFila).Value = ""
        DataGridView1.Item(4, eFila).Value = ""
        DataGridView1.Item(5, eFila).Value = ""
        DataGridView1.Item(6, eFila).Value = ""
        DataGridView1.Item(7, eFila).Value = ""
        DataGridView1.Item(8, eFila).Value = ""
        DataGridView1.Item(9, eFila).Value = ""
        
    End Sub

    Private Sub ValidaEliminarRelCompaniasucursal(ByVal Op As Integer)
        Dim IdCompañia As Integer = 0

        If DataGridView1.Rows.Count > 0 Then
            IdCompañia = DataGridView1.SelectedCells(1).Value.ToString
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvsucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, IdCompañia)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("ValidaEliminarRelCompaniasucursal")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        eFila = e.RowIndex
    End Sub

    Private Sub ButtonGuardar_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub DataGridView1_RowValidated(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowValidated
        eFila = e.RowIndex
    End Sub

    Private Sub DataGridView1_RowValidating(sender As Object, e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles DataGridView1.RowValidating
        eFila = e.RowIndex
    End Sub

    Private Sub Guarda_Detalle()
        Dim oClv_Sucursa As Integer, oid_compania As Integer, oClvEq As String, oSerie As String, oFolio As Integer, oSerieG As String, FolioG As Integer, oSerieD As String, FolioD As Integer, oRfc As String

        oClv_Sucursa = Clv_SucursalTextBox.Text
        

        Dim i As Integer = 0
        For i = 0 To DataGridView1.RowCount - 1
            If CStr(DataGridView1.Item(0, i).Value) <> "" Then
                If IsNumeric(DataGridView1.Item(1, i).Value) = True Then oid_compania = DataGridView1.Item(1, i).Value Else oid_compania = 0
                oClvEq = DataGridView1.Item(3, i).Value
                oSerie = DataGridView1.Item(4, i).Value
                If IsNumeric(DataGridView1.Item(5, i).Value) = True Then oFolio = DataGridView1.Item(5, i).Value Else oFolio = 0
                oSerieG = DataGridView1.Item(6, i).Value
                If IsNumeric(DataGridView1.Item(7, i).Value) = True Then FolioG = DataGridView1.Item(7, i).Value Else FolioG = 0
                oSerieD = DataGridView1.Item(8, i).Value
                If IsNumeric(DataGridView1.Item(9, i).Value) = True Then FolioD = DataGridView1.Item(9, i).Value Else FolioD = 0
                SP_GUARDARRel_SucursalCompania(Clv_SucursalTextBox.Text, oid_compania, oClvEq, oSerie, oFolio, oSerieG, FolioG, oSerieD, FolioD, "")
            End If
        Next
    End Sub




    Private Sub LLena_DetalleSuc(oClv_Sucursal As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("SP_CONSULTARel_SucursalCompania", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Clv_Sucursal", oClv_Sucursal)


        Dim READER As SqlDataReader

        Try
            CON.Open()
            Dim J As Integer = 0
            READER = CMD.ExecuteReader()
            While (READER.Read)
                DataGridView1.Item(0, j).Value = READER(0).ToString()
                DataGridView1.Item(1, J).Value = READER(1).ToString()
                DataGridView1.Item(2, J).Value = READER(2).ToString()
                DataGridView1.Item(3, J).Value = READER(3).ToString()
                DataGridView1.Item(4, J).Value = READER(4).ToString()
                DataGridView1.Item(5, J).Value = READER(5).ToString()
                DataGridView1.Item(6, J).Value = READER(6).ToString()
                DataGridView1.Item(7, J).Value = READER(7).ToString()
                DataGridView1.Item(8, J).Value = READER(8).ToString()
                DataGridView1.Item(9, J).Value = READER(9).ToString()
                J += 1
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub NombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreTextBox.TextChanged
        If Len(Descripcion) > 0 Then
            NombreTextBox.Text = Descripcion
        End If
    End Sub

    Private Sub IPTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IPTextBox.TextChanged
        If Len(Ip) > 0 Then
            IPTextBox.Text = Ip
        End If
    End Sub

    Private Sub ImpresoraTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImpresoraTextBox.TextChanged
        If Len(Impresora) > 0 Then
            ImpresoraTextBox.Text = Impresora
        End If
    End Sub
End Class