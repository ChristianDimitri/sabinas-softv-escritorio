<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAtenTel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ObservacionLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim NombreLabel1 As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim NombreLabel2 As System.Windows.Forms.Label
        Dim FechaLabel1 As System.Windows.Forms.Label
        Dim HoraLabel1 As System.Windows.Forms.Label
        Dim NombreLabel3 As System.Windows.Forms.Label
        Dim TelefonoLabel1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAtenTel))
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConAtenTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConAtenTelTableAdapter = New sofTV.DataSetEricTableAdapters.ConAtenTelTableAdapter()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.TelefonoTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_MotivoTextBox = New System.Windows.Forms.TextBox()
        Me.HoraDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.FechaDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ObservacionTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraColoniaSecBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraColoniaSecTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraColoniaSecTableAdapter()
        Me.NombreComboBox = New System.Windows.Forms.ComboBox()
        Me.ConMotAtenTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConMotAtenTelTableAdapter = New sofTV.DataSetEricTableAdapters.ConMotAtenTelTableAdapter()
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.NombreTextBox1 = New System.Windows.Forms.TextBox()
        Me.CONUSUARIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameFechaHoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameFechaHoraTableAdapter = New sofTV.DataSetEricTableAdapters.DameFechaHoraTableAdapter()
        Me.CONUSUARIOSTableAdapter = New sofTV.DataSetEricTableAdapters.CONUSUARIOSTableAdapter()
        Me.FechaDateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.HoraDateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.ConAtenTelCteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConAtenTelCteTableAdapter = New sofTV.DataSetEricTableAdapters.ConAtenTelCteTableAdapter()
        Me.ContratoTextBox1 = New System.Windows.Forms.TextBox()
        Me.NombreTextBox2 = New System.Windows.Forms.TextBox()
        Me.TelefonoTextBox1 = New System.Windows.Forms.TextBox()
        Me.Clv_ColoniaTextBox1 = New System.Windows.Forms.TextBox()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ConAtenTelBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.ConAtenTelBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.BuscaSiTieneQuejaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuscaSiTieneQuejaTableAdapter = New sofTV.DataSetEric2TableAdapters.BuscaSiTieneQuejaTableAdapter()
        ObservacionLabel = New System.Windows.Forms.Label()
        ConceptoLabel = New System.Windows.Forms.Label()
        NombreLabel1 = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        NombreLabel2 = New System.Windows.Forms.Label()
        FechaLabel1 = New System.Windows.Forms.Label()
        HoraLabel1 = New System.Windows.Forms.Label()
        NombreLabel3 = New System.Windows.Forms.Label()
        TelefonoLabel1 = New System.Windows.Forms.Label()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConAtenTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraColoniaSecBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMotAtenTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.CONUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameFechaHoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConAtenTelCteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConAtenTelBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConAtenTelBindingNavigator.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BuscaSiTieneQuejaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ObservacionLabel
        '
        ObservacionLabel.AutoSize = True
        ObservacionLabel.Location = New System.Drawing.Point(11, 218)
        ObservacionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ObservacionLabel.Name = "ObservacionLabel"
        ObservacionLabel.Size = New System.Drawing.Size(113, 18)
        ObservacionLabel.TabIndex = 24
        ObservacionLabel.Text = "Observación :"
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConceptoLabel.Location = New System.Drawing.Point(13, 16)
        ConceptoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(140, 18)
        ConceptoLabel.TabIndex = 30
        ConceptoLabel.Text = "Tipo de Servicio :"
        '
        'NombreLabel1
        '
        NombreLabel1.AutoSize = True
        NombreLabel1.Location = New System.Drawing.Point(11, 94)
        NombreLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel1.Name = "NombreLabel1"
        NombreLabel1.Size = New System.Drawing.Size(76, 18)
        NombreLabel1.TabIndex = 32
        NombreLabel1.Text = "Colonia :"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.Location = New System.Drawing.Point(13, 73)
        DescripcionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(178, 18)
        DescripcionLabel.TabIndex = 34
        DescripcionLabel.Text = "Motivo de la Llamada :"
        '
        'NombreLabel2
        '
        NombreLabel2.AutoSize = True
        NombreLabel2.Location = New System.Drawing.Point(80, 21)
        NombreLabel2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel2.Name = "NombreLabel2"
        NombreLabel2.Size = New System.Drawing.Size(77, 18)
        NombreLabel2.TabIndex = 39
        NombreLabel2.Text = "Usuario :"
        '
        'FechaLabel1
        '
        FechaLabel1.AutoSize = True
        FechaLabel1.Location = New System.Drawing.Point(824, 21)
        FechaLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        FechaLabel1.Name = "FechaLabel1"
        FechaLabel1.Size = New System.Drawing.Size(64, 18)
        FechaLabel1.TabIndex = 40
        FechaLabel1.Text = "Fecha :"
        '
        'HoraLabel1
        '
        HoraLabel1.AutoSize = True
        HoraLabel1.Location = New System.Drawing.Point(1023, 21)
        HoraLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        HoraLabel1.Name = "HoraLabel1"
        HoraLabel1.Size = New System.Drawing.Size(55, 18)
        HoraLabel1.TabIndex = 41
        HoraLabel1.Text = "Hora :"
        '
        'NombreLabel3
        '
        NombreLabel3.AutoSize = True
        NombreLabel3.Location = New System.Drawing.Point(11, 34)
        NombreLabel3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel3.Name = "NombreLabel3"
        NombreLabel3.Size = New System.Drawing.Size(78, 18)
        NombreLabel3.TabIndex = 50
        NombreLabel3.Text = "Nombre :"
        '
        'TelefonoLabel1
        '
        TelefonoLabel1.AutoSize = True
        TelefonoLabel1.Location = New System.Drawing.Point(11, 156)
        TelefonoLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        TelefonoLabel1.Name = "TelefonoLabel1"
        TelefonoLabel1.Size = New System.Drawing.Size(84, 18)
        TelefonoLabel1.TabIndex = 51
        TelefonoLabel1.Text = "Teléfono :"
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.EnforceConstraints = False
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConAtenTelBindingSource
        '
        Me.ConAtenTelBindingSource.DataMember = "ConAtenTel"
        Me.ConAtenTelBindingSource.DataSource = Me.DataSetEric
        '
        'ConAtenTelTableAdapter
        '
        Me.ConAtenTelTableAdapter.ClearBeforeFill = True
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelBindingSource, "Contrato", True))
        Me.ContratoTextBox.Location = New System.Drawing.Point(201, 293)
        Me.ContratoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.ReadOnly = True
        Me.ContratoTextBox.Size = New System.Drawing.Size(12, 24)
        Me.ContratoTextBox.TabIndex = 5
        Me.ContratoTextBox.TabStop = False
        '
        'NombreTextBox
        '
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelBindingSource, "Nombre", True))
        Me.NombreTextBox.Location = New System.Drawing.Point(201, 325)
        Me.NombreTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.ReadOnly = True
        Me.NombreTextBox.Size = New System.Drawing.Size(12, 24)
        Me.NombreTextBox.TabIndex = 7
        Me.NombreTextBox.TabStop = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(201, 357)
        Me.Clv_TipSerTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(12, 24)
        Me.Clv_TipSerTextBox.TabIndex = 9
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelBindingSource, "Clv_Colonia", True))
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(223, 326)
        Me.Clv_ColoniaTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.ReadOnly = True
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(12, 24)
        Me.Clv_ColoniaTextBox.TabIndex = 13
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'TelefonoTextBox
        '
        Me.TelefonoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelBindingSource, "Telefono", True))
        Me.TelefonoTextBox.Location = New System.Drawing.Point(223, 293)
        Me.TelefonoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TelefonoTextBox.Name = "TelefonoTextBox"
        Me.TelefonoTextBox.ReadOnly = True
        Me.TelefonoTextBox.Size = New System.Drawing.Size(12, 24)
        Me.TelefonoTextBox.TabIndex = 17
        Me.TelefonoTextBox.TabStop = False
        '
        'Clv_MotivoTextBox
        '
        Me.Clv_MotivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelBindingSource, "Clv_Motivo", True))
        Me.Clv_MotivoTextBox.Location = New System.Drawing.Point(223, 357)
        Me.Clv_MotivoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_MotivoTextBox.Name = "Clv_MotivoTextBox"
        Me.Clv_MotivoTextBox.ReadOnly = True
        Me.Clv_MotivoTextBox.Size = New System.Drawing.Size(12, 24)
        Me.Clv_MotivoTextBox.TabIndex = 19
        Me.Clv_MotivoTextBox.TabStop = False
        '
        'HoraDateTimePicker
        '
        Me.HoraDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConAtenTelBindingSource, "Hora", True))
        Me.HoraDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.HoraDateTimePicker.Location = New System.Drawing.Point(367, 337)
        Me.HoraDateTimePicker.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.HoraDateTimePicker.Name = "HoraDateTimePicker"
        Me.HoraDateTimePicker.Size = New System.Drawing.Size(12, 24)
        Me.HoraDateTimePicker.TabIndex = 21
        Me.HoraDateTimePicker.TabStop = False
        '
        'FechaDateTimePicker
        '
        Me.FechaDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConAtenTelBindingSource, "Fecha", True))
        Me.FechaDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaDateTimePicker.Location = New System.Drawing.Point(367, 303)
        Me.FechaDateTimePicker.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FechaDateTimePicker.Name = "FechaDateTimePicker"
        Me.FechaDateTimePicker.Size = New System.Drawing.Size(12, 24)
        Me.FechaDateTimePicker.TabIndex = 23
        Me.FechaDateTimePicker.TabStop = False
        '
        'ObservacionTextBox
        '
        Me.ObservacionTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ObservacionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ObservacionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelBindingSource, "Observacion", True))
        Me.ObservacionTextBox.Location = New System.Drawing.Point(15, 240)
        Me.ObservacionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ObservacionTextBox.Multiline = True
        Me.ObservacionTextBox.Name = "ObservacionTextBox"
        Me.ObservacionTextBox.Size = New System.Drawing.Size(429, 161)
        Me.ObservacionTextBox.TabIndex = 8
        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelBindingSource, "Clv_Usuario", True))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(244, 357)
        Me.Clv_UsuarioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.ReadOnly = True
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(12, 24)
        Me.Clv_UsuarioTextBox.TabIndex = 27
        Me.Clv_UsuarioTextBox.TabStop = False
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(17, 36)
        Me.ConceptoComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(425, 26)
        Me.ConceptoComboBox.TabIndex = 10
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'MuestraColoniaSecBindingSource
        '
        Me.MuestraColoniaSecBindingSource.DataMember = "MuestraColoniaSec"
        Me.MuestraColoniaSecBindingSource.DataSource = Me.DataSetEric
        '
        'MuestraColoniaSecTableAdapter
        '
        Me.MuestraColoniaSecTableAdapter.ClearBeforeFill = True
        '
        'NombreComboBox
        '
        Me.NombreComboBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NombreComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraColoniaSecBindingSource, "Nombre", True))
        Me.NombreComboBox.DataSource = Me.MuestraColoniaSecBindingSource
        Me.NombreComboBox.DisplayMember = "Nombre"
        Me.NombreComboBox.FormattingEnabled = True
        Me.NombreComboBox.Location = New System.Drawing.Point(15, 116)
        Me.NombreComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreComboBox.Name = "NombreComboBox"
        Me.NombreComboBox.Size = New System.Drawing.Size(428, 26)
        Me.NombreComboBox.TabIndex = 6
        Me.NombreComboBox.ValueMember = "Clv_Colonia"
        '
        'ConMotAtenTelBindingSource
        '
        Me.ConMotAtenTelBindingSource.DataMember = "ConMotAtenTel"
        Me.ConMotAtenTelBindingSource.DataSource = Me.DataSetEric
        '
        'ConMotAtenTelTableAdapter
        '
        Me.ConMotAtenTelTableAdapter.ClearBeforeFill = True
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DescripcionComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMotAtenTelBindingSource, "Descripcion", True))
        Me.DescripcionComboBox.DataSource = Me.ConMotAtenTelBindingSource
        Me.DescripcionComboBox.DisplayMember = "Descripcion"
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(17, 92)
        Me.DescripcionComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(425, 26)
        Me.DescripcionComboBox.TabIndex = 11
        Me.DescripcionComboBox.ValueMember = "Clv_Motivo"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(15, 123)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(455, 111)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "¿Es un Cliente?"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(195, 71)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 28)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Buscar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(29, 39)
        Me.RadioButton2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton2.TabIndex = 0
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "No"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(120, 39)
        Me.RadioButton1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(44, 22)
        Me.RadioButton1.TabIndex = 1
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Sí"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.Location = New System.Drawing.Point(195, 38)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(227, 24)
        Me.TextBox3.TabIndex = 2
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Location = New System.Drawing.Point(1027, 43)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(170, 24)
        Me.TextBox2.TabIndex = 43
        Me.TextBox2.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(828, 43)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(170, 24)
        Me.TextBox1.TabIndex = 42
        Me.TextBox1.TabStop = False
        '
        'NombreTextBox1
        '
        Me.NombreTextBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NombreTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "Nombre", True))
        Me.NombreTextBox1.Location = New System.Drawing.Point(77, 43)
        Me.NombreTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreTextBox1.Name = "NombreTextBox1"
        Me.NombreTextBox1.ReadOnly = True
        Me.NombreTextBox1.Size = New System.Drawing.Size(473, 24)
        Me.NombreTextBox1.TabIndex = 40
        Me.NombreTextBox1.TabStop = False
        '
        'CONUSUARIOSBindingSource
        '
        Me.CONUSUARIOSBindingSource.DataMember = "CONUSUARIOS"
        Me.CONUSUARIOSBindingSource.DataSource = Me.DataSetEric
        '
        'DameFechaHoraBindingSource
        '
        Me.DameFechaHoraBindingSource.DataMember = "DameFechaHora"
        Me.DameFechaHoraBindingSource.DataSource = Me.DataSetEric
        '
        'DameFechaHoraTableAdapter
        '
        Me.DameFechaHoraTableAdapter.ClearBeforeFill = True
        '
        'CONUSUARIOSTableAdapter
        '
        Me.CONUSUARIOSTableAdapter.ClearBeforeFill = True
        '
        'FechaDateTimePicker1
        '
        Me.FechaDateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DameFechaHoraBindingSource, "Fecha", True))
        Me.FechaDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaDateTimePicker1.Location = New System.Drawing.Point(340, 290)
        Me.FechaDateTimePicker1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FechaDateTimePicker1.Name = "FechaDateTimePicker1"
        Me.FechaDateTimePicker1.Size = New System.Drawing.Size(12, 24)
        Me.FechaDateTimePicker1.TabIndex = 41
        Me.FechaDateTimePicker1.TabStop = False
        '
        'HoraDateTimePicker1
        '
        Me.HoraDateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DameFechaHoraBindingSource, "Hora", True))
        Me.HoraDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.HoraDateTimePicker1.Location = New System.Drawing.Point(340, 324)
        Me.HoraDateTimePicker1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.HoraDateTimePicker1.Name = "HoraDateTimePicker1"
        Me.HoraDateTimePicker1.Size = New System.Drawing.Size(12, 24)
        Me.HoraDateTimePicker1.TabIndex = 42
        Me.HoraDateTimePicker1.TabStop = False
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.Location = New System.Drawing.Point(15, 59)
        Me.TextBox4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(429, 24)
        Me.TextBox4.TabIndex = 5
        '
        'TextBox5
        '
        Me.TextBox5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox5.Location = New System.Drawing.Point(15, 178)
        Me.TextBox5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(429, 24)
        Me.TextBox5.TabIndex = 7
        '
        'ConAtenTelCteBindingSource
        '
        Me.ConAtenTelCteBindingSource.DataMember = "ConAtenTelCte"
        Me.ConAtenTelCteBindingSource.DataSource = Me.DataSetEric
        '
        'ConAtenTelCteTableAdapter
        '
        Me.ConAtenTelCteTableAdapter.ClearBeforeFill = True
        '
        'ContratoTextBox1
        '
        Me.ContratoTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelCteBindingSource, "Contrato", True))
        Me.ContratoTextBox1.Location = New System.Drawing.Point(244, 293)
        Me.ContratoTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ContratoTextBox1.Name = "ContratoTextBox1"
        Me.ContratoTextBox1.ReadOnly = True
        Me.ContratoTextBox1.Size = New System.Drawing.Size(12, 24)
        Me.ContratoTextBox1.TabIndex = 50
        Me.ContratoTextBox1.TabStop = False
        '
        'NombreTextBox2
        '
        Me.NombreTextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelCteBindingSource, "Nombre", True))
        Me.NombreTextBox2.Location = New System.Drawing.Point(265, 293)
        Me.NombreTextBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreTextBox2.Name = "NombreTextBox2"
        Me.NombreTextBox2.ReadOnly = True
        Me.NombreTextBox2.Size = New System.Drawing.Size(12, 24)
        Me.NombreTextBox2.TabIndex = 51
        Me.NombreTextBox2.TabStop = False
        '
        'TelefonoTextBox1
        '
        Me.TelefonoTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelCteBindingSource, "Telefono", True))
        Me.TelefonoTextBox1.Location = New System.Drawing.Point(244, 322)
        Me.TelefonoTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TelefonoTextBox1.Name = "TelefonoTextBox1"
        Me.TelefonoTextBox1.ReadOnly = True
        Me.TelefonoTextBox1.Size = New System.Drawing.Size(12, 24)
        Me.TelefonoTextBox1.TabIndex = 52
        Me.TelefonoTextBox1.TabStop = False
        '
        'Clv_ColoniaTextBox1
        '
        Me.Clv_ColoniaTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelCteBindingSource, "Clv_Colonia", True))
        Me.Clv_ColoniaTextBox1.Location = New System.Drawing.Point(265, 322)
        Me.Clv_ColoniaTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ColoniaTextBox1.Name = "Clv_ColoniaTextBox1"
        Me.Clv_ColoniaTextBox1.ReadOnly = True
        Me.Clv_ColoniaTextBox1.Size = New System.Drawing.Size(12, 24)
        Me.Clv_ColoniaTextBox1.TabIndex = 53
        Me.Clv_ColoniaTextBox1.TabStop = False
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 28)
        '
        'ConAtenTelBindingNavigatorSaveItem
        '
        Me.ConAtenTelBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConAtenTelBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConAtenTelBindingNavigatorSaveItem.Name = "ConAtenTelBindingNavigatorSaveItem"
        Me.ConAtenTelBindingNavigatorSaveItem.Size = New System.Drawing.Size(210, 25)
        Me.ConAtenTelBindingNavigatorSaveItem.Text = "&GUARDAR LLAMADA"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 28)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 28)
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 28)
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 28)
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(182, 25)
        Me.BindingNavigatorAddNewItem.Text = "&NUEVA LLAMADA"
        '
        'ConAtenTelBindingNavigator
        '
        Me.ConAtenTelBindingNavigator.AddNewItem = Nothing
        Me.ConAtenTelBindingNavigator.BindingSource = Me.ConAtenTelBindingSource
        Me.ConAtenTelBindingNavigator.CountItem = Nothing
        Me.ConAtenTelBindingNavigator.DeleteItem = Nothing
        Me.ConAtenTelBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConAtenTelBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ConAtenTelBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorSeparator2, Me.ConAtenTelBindingNavigatorSaveItem, Me.ToolStripSeparator1, Me.ToolStripSeparator2, Me.ToolStripSeparator3, Me.ToolStripSeparator4, Me.BindingNavigatorAddNewItem})
        Me.ConAtenTelBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConAtenTelBindingNavigator.MoveFirstItem = Nothing
        Me.ConAtenTelBindingNavigator.MoveLastItem = Nothing
        Me.ConAtenTelBindingNavigator.MoveNextItem = Nothing
        Me.ConAtenTelBindingNavigator.MovePreviousItem = Nothing
        Me.ConAtenTelBindingNavigator.Name = "ConAtenTelBindingNavigator"
        Me.ConAtenTelBindingNavigator.PositionItem = Nothing
        Me.ConAtenTelBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConAtenTelBindingNavigator.Size = New System.Drawing.Size(1355, 28)
        Me.ConAtenTelBindingNavigator.TabIndex = 12
        Me.ConAtenTelBindingNavigator.TabStop = True
        Me.ConAtenTelBindingNavigator.Text = "BindingNavigator1"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ObservacionTextBox)
        Me.GroupBox2.Controls.Add(Me.NombreComboBox)
        Me.GroupBox2.Controls.Add(Me.Clv_ColoniaTextBox1)
        Me.GroupBox2.Controls.Add(ObservacionLabel)
        Me.GroupBox2.Controls.Add(Me.TelefonoTextBox1)
        Me.GroupBox2.Controls.Add(Me.FechaDateTimePicker)
        Me.GroupBox2.Controls.Add(NombreLabel1)
        Me.GroupBox2.Controls.Add(Me.HoraDateTimePicker)
        Me.GroupBox2.Controls.Add(Me.HoraDateTimePicker1)
        Me.GroupBox2.Controls.Add(Me.NombreTextBox2)
        Me.GroupBox2.Controls.Add(Me.FechaDateTimePicker1)
        Me.GroupBox2.Controls.Add(Me.ContratoTextBox1)
        Me.GroupBox2.Controls.Add(Me.TextBox4)
        Me.GroupBox2.Controls.Add(Me.TextBox5)
        Me.GroupBox2.Controls.Add(NombreLabel3)
        Me.GroupBox2.Controls.Add(TelefonoLabel1)
        Me.GroupBox2.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.GroupBox2.Controls.Add(Me.Clv_MotivoTextBox)
        Me.GroupBox2.Controls.Add(Me.ContratoTextBox)
        Me.GroupBox2.Controls.Add(Me.TelefonoTextBox)
        Me.GroupBox2.Controls.Add(Me.NombreTextBox)
        Me.GroupBox2.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.GroupBox2.Controls.Add(Me.Clv_TipSerTextBox)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(15, 241)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(455, 422)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TextBox2)
        Me.GroupBox3.Controls.Add(FechaLabel1)
        Me.GroupBox3.Controls.Add(NombreLabel2)
        Me.GroupBox3.Controls.Add(HoraLabel1)
        Me.GroupBox3.Controls.Add(Me.NombreTextBox1)
        Me.GroupBox3.Controls.Add(Me.TextBox1)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(15, 36)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Size = New System.Drawing.Size(1324, 79)
        Me.GroupBox3.TabIndex = 55
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Llamada Atendida por"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.DescripcionComboBox)
        Me.GroupBox4.Controls.Add(Me.ConceptoComboBox)
        Me.GroupBox4.Controls.Add(ConceptoLabel)
        Me.GroupBox4.Controls.Add(DescripcionLabel)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(15, 671)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox4.Size = New System.Drawing.Size(455, 142)
        Me.GroupBox4.TabIndex = 9
        Me.GroupBox4.TabStop = False
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(152, 826)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 44)
        Me.Button2.TabIndex = 48
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(477, 123)
        Me.WebBrowser1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(27, 25)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(861, 689)
        Me.WebBrowser1.TabIndex = 56
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BuscaSiTieneQuejaBindingSource
        '
        Me.BuscaSiTieneQuejaBindingSource.DataMember = "BuscaSiTieneQueja"
        Me.BuscaSiTieneQuejaBindingSource.DataSource = Me.DataSetEric2
        '
        'BuscaSiTieneQuejaTableAdapter
        '
        Me.BuscaSiTieneQuejaTableAdapter.ClearBeforeFill = True
        '
        'FrmAtenTel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1355, 912)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ConAtenTelBindingNavigator)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmAtenTel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Atención Telefónica"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConAtenTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraColoniaSecBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMotAtenTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.CONUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameFechaHoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConAtenTelCteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConAtenTelBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConAtenTelBindingNavigator.ResumeLayout(False)
        Me.ConAtenTelBindingNavigator.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BuscaSiTieneQuejaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConAtenTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConAtenTelTableAdapter As sofTV.DataSetEricTableAdapters.ConAtenTelTableAdapter
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefonoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_MotivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HoraDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents ObservacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraColoniaSecBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraColoniaSecTableAdapter As sofTV.DataSetEricTableAdapters.MuestraColoniaSecTableAdapter
    Friend WithEvents NombreComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ConMotAtenTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConMotAtenTelTableAdapter As sofTV.DataSetEricTableAdapters.ConMotAtenTelTableAdapter
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DameFechaHoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameFechaHoraTableAdapter As sofTV.DataSetEricTableAdapters.DameFechaHoraTableAdapter
    Friend WithEvents CONUSUARIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONUSUARIOSTableAdapter As sofTV.DataSetEricTableAdapters.CONUSUARIOSTableAdapter
    Friend WithEvents NombreTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents FechaDateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents HoraDateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents ConAtenTelCteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConAtenTelCteTableAdapter As sofTV.DataSetEricTableAdapters.ConAtenTelCteTableAdapter
    Friend WithEvents ContratoTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TelefonoTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ConAtenTelBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConAtenTelBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Private WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents BuscaSiTieneQuejaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BuscaSiTieneQuejaTableAdapter As sofTV.DataSetEric2TableAdapters.BuscaSiTieneQuejaTableAdapter
End Class
