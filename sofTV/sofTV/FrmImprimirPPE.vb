Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports NPOI.HSSF.UserModel
Imports NPOI.SS.UserModel
Imports NPOI.SS.Util
Imports NPOI.HSSF.Util
Imports NPOI.POIFS.FileSystem
Imports NPOI.HPSF
Imports NPOI.XSSF.UserModel
Imports NPOI.XSSF.Util
Imports System.Text
Imports System.IO
Imports System.Web.Security


Public Class FrmImprimirPPE
    Private customersByCityReport As ReportDocument

    Private Sub ConfigureCrystalReports()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim eFecha As String = Nothing
        eFecha = "Del " & eFechaIniPPE & " al " & eFechaFinPPE
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim companias As String = ""
        Dim ciudades As String = ""
        Dim contador As Integer = 0
        Dim reportPath As String = Nothing


        If eOpPPE = 1 Then

            Me.Text = "Reporte de Venta de Pel�culas PPE"


            reportPath = RutaReportes + "\ReportServiciosPPE.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(1, eFechaFinPPE)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"


        ElseIf eOpPPE = 2 Then
            Me.Text = "Bit�cora de Activaci�n de Paquetes"


            reportPath = RutaReportes + "\ReportBitActPaq.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(1, eFechaFinPPE)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@FechaInicial", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@FechaFinal", SqlDbType.DateTime, CObj(eFechaFinPPE))
            'BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteBitActPaq")
            DS = BaseII.ConsultaDS("ReporteBitActPaq", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            ciudades = "Plazas: "
            Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            For Each elem As Xml.XmlElement In elementos
                If elem.GetAttribute("Seleccion") = "1" Then
                    If ciudades = "Plazas: " Then
                        ciudades = ciudades + elem.GetAttribute("Nombre")
                    Else
                        ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport.DataDefinition.FormulaFields("Compania").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & ciudades & "'"



        ElseIf eOpPPE = 3 Then

            Me.Text = "Reporte Resumen de Ventas Sucursal"

            Dim conexion As New SqlConnection(MiConexion)

            Dim sBuilder As New StringBuilder("EXEC ReporteResumenVentas '" + eFechaIniPPE + "', '" + eFechaFinPPE + "', " + identificador.ToString)
            'Dim sBuilder As New StringBuilder("EXEC REPORTETEST")
            Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
            Dim dTable As New DataTable

            dAdapter.Fill(dTable)

            reportPath = RutaReportes + "\ReportResumenVentas.rpt"
            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dTable)
            Dim con As New SqlConnection(MiConexion)
            con.Open()
            Dim comando As New SqlCommand()
            comando.Connection = con
            comando.CommandText = "exec DamePlazasTituloReporte " + identificador.ToString
            companias = "Distribuidor: "
            Dim reader As SqlDataReader = comando.ExecuteReader()
            contador = 0
            While reader.Read()
                If contador = 0 Then
                    companias = companias + reader(0).ToString
                    contador = contador + 1
                Else
                    companias = companias + ", " + reader(0).ToString
                End If
            End While
            reader.Close()
            comando.Dispose()
            comando.Connection = con
            comando.CommandText = "exec DameCompaniasTituloReporte " + identificador.ToString
            reader = comando.ExecuteReader()
            contador = 0
            ciudades = "Plazas: "
            While reader.Read()
                If contador = 0 Then
                    ciudades = ciudades + reader(0).ToString
                    contador = contador + 1
                Else
                    ciudades = ciudades + ", " + reader(0).ToString
                End If
            End While
            con.Close()
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Compania").Text = "'" & companias & "'"




        ElseIf eOpPPE = 4 Then
            Me.Text = "Reporte Resumen de Ventas Vendedores"


            reportPath = RutaReportes + "\ReportResumenVentasVendedores.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(1, eFechaFinPPE)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFinPPE))
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)

            Dim listatablas As New List(Of String)
            listatablas.Add("VentasVendedor")
            DS = BaseII.ConsultaDS("VentasVendedor", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            Dim con As New SqlConnection(MiConexion)
            con.Open()
            Dim comando As New SqlCommand()
            comando.Connection = con
            comando.CommandText = "exec DamePlazasTituloReporte " + identificador.ToString
            companias = "Distribuidor: "
            Dim reader As SqlDataReader = comando.ExecuteReader()
            contador = 0
            While reader.Read()
                If contador = 0 Then
                    companias = companias + reader(0).ToString
                    contador = contador + 1
                Else
                    companias = companias + ", " + reader(0).ToString
                End If
            End While
            reader.Close()
            comando.Dispose()
            comando.Connection = con
            comando.CommandText = "exec DameCompaniasTituloReporte " + identificador.ToString
            reader = comando.ExecuteReader()
            contador = 0
            ciudades = "Plazas: "
            While reader.Read()
                If contador = 0 Then
                    ciudades = ciudades + reader(0).ToString
                    contador = contador + 1
                Else
                    ciudades = ciudades + ", " + reader(0).ToString
                End If
            End While
            con.Close()
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Compania").Text = "'" & companias & "'"
        ElseIf eOpPPE = 5 Then
            eOpPPE = 0
            Me.Text = "Listado de Adelantados"
            Dim dSet As New DataSet
            dSet = LISTADOAdelantados()
            customersByCityReport.Load(RutaReportes + "\LISTADOAdelantados.rpt")
            customersByCityReport.SetDataSource(dSet)
        ElseIf eOpPPE = 6 Then
            Me.Text = "Resumen Ordenes y Quejas Ejecutadas"
            reportPath = RutaReportes + "\ResumenOrdenQueja.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, CObj(eFechaFinPPE))
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            BaseII.CreateMyParameter("@CiudadesXml", SqlDbType.Xml, DocCiudades.InnerXml)
            BaseII.CreateMyParameter("@LocalidadesXml", SqlDbType.Xml, DocLocalidades.InnerXml)
            Dim listatablas As New List(Of String)
            listatablas.Add("UspReporteResumenOrdenQueja")
            DS = BaseII.ConsultaDS("ReporteResumenOrdenQueja", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            Dim fechas As String = Nothing
            ciudades = "Plazas: "
            Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            For Each elem As Xml.XmlElement In elementos
                If elem.GetAttribute("Seleccion") = "1" Then
                    If ciudades = "Plazas: " Then
                        ciudades = ciudades + elem.GetAttribute("Nombre")
                    Else
                        ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & ciudades & "'" 'plazas
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
        ElseIf eOpPPE = 7 Then
            Me.Text = "Resumen Ordenes y Quejas Ejecutadas"
            reportPath = RutaReportes + "\ResumenOrdenQueja.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, CObj(eFechaFinPPE))
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            BaseII.CreateMyParameter("@CiudadesXml", SqlDbType.Xml, DocCiudades.InnerXml)
            BaseII.CreateMyParameter("@LocalidadesXml", SqlDbType.Xml, DocLocalidades.InnerXml)
            Dim listatablas As New List(Of String)
            listatablas.Add("UspReporteResumenOrdenQueja")
            DS = BaseII.ConsultaDS("ReporteResumenOrdenQueja", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            Dim fechas As String = Nothing
            ciudades = "Plazas: "
            Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            For Each elem As Xml.XmlElement In elementos
                If elem.GetAttribute("Seleccion") = "1" Then
                    If ciudades = "Plazas: " Then
                        ciudades = ciudades + elem.GetAttribute("Nombre")
                    Else
                        ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & ciudades & "'" 'plazas
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
        ElseIf eOpPPE = 8 Then
            If ExportarExcel Then
                Dim wb As New XSSFWorkbook()
                Dim fileOut
                Try
                    Dim DS As New DataSet
                    Dim rutaFile As String = ""
                    DS.Clear()
                    BaseII.limpiaParametros()
                    'BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
                    BaseII.CreateMyParameter("@TipoReporte", SqlDbType.SmallInt, GloTipoReporteSinDocumnetos)
                    BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
                    BaseII.CreateMyParameter("@CiudadesXml", SqlDbType.Xml, DocCiudades.InnerXml)
                    BaseII.CreateMyParameter("@LocalidadesXml", SqlDbType.Xml, DocLocalidades.InnerXml)

                    Dim listatablas As New List(Of String)
                    listatablas.Add("ClientesSinDocumento")
                    listatablas.Add("DetalleDocumentos")
                    listatablas.Add("Encabezados")
                    DS = BaseII.ConsultaDS("ReporteClientesSinDocumentoExcel", listatablas)

                    Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()
                    If (result = DialogResult.OK) Then
                        rutaFile = Me.FolderBrowserDialog1.SelectedPath.ToString
                        If File.Exists(rutaFile + "\ClientesSinDocumentos.xlsx") Then
                            File.Delete(rutaFile + "\ClientesSinDocumentos.xlsx")
                        End If
                        Dim sheet1 As ISheet = wb.CreateSheet("Hoja1")
                        fileOut = New FileStream(rutaFile + "\ClientesSinDocumentos.xlsx", FileMode.Create, FileAccess.ReadWrite)

                        'Sacamos la cantidad de documentos por tipo
                        BaseII.limpiaParametros()
                        BaseII.CreateMyParameter("@cantidad1", ParameterDirection.Output, SqlDbType.Int)
                        BaseII.CreateMyParameter("@cantidad2", ParameterDirection.Output, SqlDbType.Int)
                        BaseII.ProcedimientoOutPut("DameCantidadDocumentosPorTipo")
                        Dim cantidad1 As Integer = BaseII.dicoPar("@cantidad1")
                        Dim cantidad2 As Integer = BaseII.dicoPar("@cantidad2")
                        'Apuntadores
                        Dim tipo1 As Integer = 0
                        Dim tipo2 As Integer = 0

                        Dim rowNew As IRow = sheet1.CreateRow(0)
                        Dim c As Integer = 0
                        'rowNew.CreateCell(0).SetCellValue("")

                        For Each row As DataRow In DS.Tables(2).Rows
                            If row(2).ToString = "0" And tipo1 = 0 Then
                                tipo1 = c
                            End If
                            If row(2).ToString = "1" And tipo2 = 0 Then
                                tipo2 = c
                            End If
                            If row(1).GetType().ToString = "System.Int32" Then
                                rowNew.CreateCell(c).SetCellValue(CInt(row(1).ToString))
                            ElseIf row(1).GetType().ToString = "System.Int16" Then
                                rowNew.CreateCell(c).SetCellValue(CInt(row(1).ToString))
                            ElseIf row(1).GetType().ToString = "System.Decimal" Then
                                rowNew.CreateCell(c).SetCellValue(CDbl(row(1).ToString))
                            Else
                                rowNew.CreateCell(c).SetCellValue(row(1).ToString)
                            End If
                            c = c + 1
                        Next
                        'MsgBox(tipo1.ToString)
                        'MsgBox(tipo2.ToString)

                        Dim f As Integer = 1
                        c = 0
                        Dim aux As Integer = 0
                        Dim rowNew2 As IRow = sheet1.CreateRow(f)

                        For Each row As DataRow In DS.Tables(0).Rows
                            rowNew2.CreateCell(0).SetCellValue(row(6).ToString)
                            rowNew2.CreateCell(1).SetCellValue(row(1).ToString)
                            rowNew2.CreateCell(2).SetCellValue(row(2).ToString)
                            If row(3).ToString = "True" Then
                                rowNew2.CreateCell(3).SetCellValue(row(8).ToString.Substring(0, 10))
                            Else
                                rowNew2.CreateCell(3).SetCellValue("NA")
                            End If
                            If row(4).ToString = "True" Then
                                rowNew2.CreateCell(4).SetCellValue(row(7).ToString.Substring(0, 10))
                            Else
                                rowNew2.CreateCell(4).SetCellValue("NA")
                            End If
                            'Vamos a recorrer los detalles nom�s de ese compa
                            If aux < DS.Tables(1).Rows.Count Then
                                Dim rowAux As DataRow = DS.Tables(1).Rows(aux)
                                If rowAux(0).ToString = row(0) Then
                                    If rowAux(4).ToString = "0" Then 'si es tipo 1
                                        Dim i As Integer = 0
                                        Dim posicion1 As Integer = tipo1
                                        While i < cantidad1
                                            If aux < DS.Tables(1).Rows.Count Then
                                                rowAux = DS.Tables(1).Rows(aux)
                                                If rowAux(3).ToString.Substring(0, 10) = "01/01/1900" Then
                                                    rowNew2.CreateCell(posicion1).SetCellValue("NA")
                                                Else
                                                    rowNew2.CreateCell(posicion1).SetCellValue(rowAux(3).ToString.Substring(0, 10))
                                                End If
                                                posicion1 = posicion1 + 1
                                                i = i + 1
                                                aux = aux + 1
                                            End If
                                        End While
                                    End If
                                    If rowAux(4).ToString = "1" Then 'si es tipo 2
                                        Dim i As Integer = 0
                                        Dim posicion2 As Integer = tipo2
                                        While i < cantidad2
                                            If aux < DS.Tables(1).Rows.Count Then
                                                rowAux = DS.Tables(1).Rows(aux)
                                                If rowAux(3).ToString.Substring(0, 10) = "01/01/1900" Then
                                                    rowNew2.CreateCell(posicion2).SetCellValue("NA")
                                                Else
                                                    rowNew2.CreateCell(posicion2).SetCellValue(rowAux(3).ToString.Substring(0, 10))
                                                End If
                                                posicion2 = posicion2 + 1
                                                i = i + 1
                                                aux = aux + 1
                                            End If
                                        End While
                                    End If
                                    If rowAux(4).ToString = "99" Then 'si es de los dos
                                        Dim i As Integer = 0
                                        Dim posicion2 As Integer = tipo1
                                        'Creamos el estilo de una celda
                                        Dim cellStyle As ICellStyle = wb.CreateCellStyle()
                                        cellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightGreen.Index
                                        cellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.LightGreen.Index
                                        'Fin estilo de celda
                                        While i < cantidad2 + cantidad1
                                            If aux < DS.Tables(1).Rows.Count Then
                                                rowAux = DS.Tables(1).Rows(aux)
                                                Dim cell As ICell = rowNew2.CreateCell(posicion2)
                                                If rowAux(3).ToString.Substring(0, 10) = "01/01/1900" Then
                                                    'rowNew2.CreateCell(posicion2).SetCellValue("NA")
                                                    cell.SetCellValue("NA")
                                                Else
                                                    'rowNew2.CreateCell(posicion2).SetCellValue(rowAux(3).ToString.Substring(0, 10))
                                                    cell.SetCellValue(rowAux(3).ToString.Substring(0, 10))
                                                End If
                                                If rowAux(5).ToString = "False" And i >= 0 And i < cantidad1 Then
                                                    cell.CellStyle = cellStyle
                                                    cell.CellStyle.FillPattern = FillPattern.SolidForeground
                                                    cell.CellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightGreen.Index
                                                ElseIf rowAux(5).ToString = "True" And i >= cantidad1 And i <= cantidad2 + (cantidad1 - 1) Then
                                                    cell.CellStyle = cellStyle
                                                    cell.CellStyle.FillPattern = FillPattern.SolidForeground
                                                    cell.CellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightGreen.Index
                                                End If
                                                posicion2 = posicion2 + 1
                                                i = i + 1
                                                aux = aux + 1
                                            End If
                                        End While
                                    End If
                                End If
                            End If
                            f = f + 1
                            rowNew2 = sheet1.CreateRow(f)
                        Next

                        sheet1.AutoSizeColumn(0)
                        sheet1.AutoSizeColumn(1)
                        sheet1.AutoSizeColumn(2)
                        sheet1.AutoSizeColumn(3)
                        sheet1.AutoSizeColumn(4)
                        sheet1.AutoSizeColumn(5)
                        sheet1.AutoSizeColumn(6)
                        sheet1.AutoSizeColumn(7)
                        sheet1.AutoSizeColumn(8)
                        sheet1.AutoSizeColumn(9)
                        sheet1.AutoSizeColumn(10)
                        sheet1.AutoSizeColumn(11)
                        sheet1.AutoSizeColumn(12)
                        sheet1.AutoSizeColumn(13)
                        sheet1.AutoSizeColumn(14)
                        sheet1.AutoSizeColumn(15)
                        sheet1.AutoSizeColumn(16)
                        sheet1.AutoSizeColumn(17)



                        wb.Write(fileOut)
                        fileOut.Close()
                        MsgBox("Archivo generado exitosamente.")
                        Me.Close()
                        Exit Sub
                    End If
                Catch ex As Exception
                    wb.Write(fileOut)
                    fileOut.Close()
                    MsgBox(ex.ToString)
                End Try

            Else
                Me.Text = "Resumen de Clientes sin Documentos"
                reportPath = RutaReportes + "\ReporteClientesSinDocumento.rpt"
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                'BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
                BaseII.CreateMyParameter("@TipoReporte", SqlDbType.SmallInt, GloTipoReporteSinDocumnetos)
                BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
                BaseII.CreateMyParameter("@CiudadesXml", SqlDbType.Xml, DocCiudades.InnerXml)
                BaseII.CreateMyParameter("@LocalidadesXml", SqlDbType.Xml, DocLocalidades.InnerXml)

                Dim listatablas As New List(Of String)
                listatablas.Add("ClientesSinDocumento")
                listatablas.Add("DetalleDocumentos")
                DS = BaseII.ConsultaDS("ReporteClientesSinDocumento", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)
                Dim fechas As String = Nothing

                ciudades = "Plazas: "
                Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
                For Each elem As Xml.XmlElement In elementos
                    If elem.GetAttribute("Seleccion") = "1" Then
                        If ciudades = "Plazas: " Then
                            ciudades = ciudades + elem.GetAttribute("Nombre")
                        Else
                            ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
                        End If
                    End If
                Next
                companias = "Distribuidores: "
                Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
                For Each elem As Xml.XmlElement In elementos2
                    If elem.GetAttribute("Seleccion") = "1" Then
                        If companias = "Distribuidores: " Then
                            companias = companias + elem.GetAttribute("nombre")
                        Else
                            companias = companias + ", " + elem.GetAttribute("nombre")
                        End If
                    End If
                Next

                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'Resumen de Clientes sin Documentos'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & companias & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo2").Text = "'" & ciudades & "'"
            End If
        ElseIf eOpPPE = 9 Then
            Me.Text = "Resumen de Ordenes Pendientes por Concepto"
            reportPath = RutaReportes + "\ReporteListadoOrdenesConcepto.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)

            Dim listatablas As New List(Of String)
            'listatablas.Add("ListadoOrdenes")
            'DS = BaseII.ConsultaDS("sp_ListadoOrdenesPorConcepto", listatablas)
            listatablas.Add("ReporteAreaTecnicaOrdSer_Prueba")
            listatablas.Add("ListadoOrdenes")
            DS = BaseII.ConsultaDS("ReporteListadoOrdenesConceptoDetallado", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            Dim fechas As String = Nothing

            Dim con As New SqlConnection(MiConexion)
            con.Open()
            Dim comando As New SqlCommand()
            comando.Connection = con
            comando.CommandText = "exec DamePlazasTituloReporte " + identificador.ToString
            companias = "Distribuidor: "
            Dim reader As SqlDataReader = comando.ExecuteReader()
            contador = 0
            While reader.Read()
                If contador = 0 Then
                    companias = companias + reader(0).ToString
                    contador = contador + 1
                Else
                    companias = companias + ", " + reader(0).ToString
                End If
            End While
            reader.Close()
            comando.Dispose()
            comando.Connection = con
            comando.CommandText = "exec DameCompaniasTituloReporte " + identificador.ToString
            reader = comando.ExecuteReader()
            contador = 0
            ciudades = "Plazas: "
            While reader.Read()
                If contador = 0 Then
                    ciudades = ciudades + reader(0).ToString
                    contador = contador + 1
                Else
                    ciudades = ciudades + ", " + reader(0).ToString
                End If
            End While
            con.Close()
            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
            'BaseII.ConsultaDT("DameCiudadesTituloReporte")



            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'Resumen de Ordenes Pendientes por Concepto'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo2").Text = "'" & ciudades & "'"

        ElseIf eOpPPE = 10 Then
            Me.Text = "Detalle de Ordenes Pendientes por Concepto"
            reportPath = RutaReportes + "\ReporteOrdenesListadoMayores.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            BaseII.CreateMyParameter("@CiudadesXml", SqlDbType.Xml, DocCiudades.InnerXml)
            BaseII.CreateMyParameter("@LocalidadesXml", SqlDbType.Xml, DocLocalidades.InnerXml)
            BaseII.CreateMyParameter("@ConceptosXml", SqlDbType.Xml, DocConceptos.InnerXml)
            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteAreaTecnicaOrdSer_Prueba")
            listatablas.Add("ListadoOrdenes")
            DS = BaseII.ConsultaDS("ReporteListadoOrdenesConceptoDetallado", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            ciudades = "Plazas: "
            Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            For Each elem As Xml.XmlElement In elementos
                If elem.GetAttribute("Seleccion") = "1" Then
                    If ciudades = "Plazas: " Then
                        ciudades = ciudades + elem.GetAttribute("Nombre")
                    Else
                        ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & "Detalle de Ordenes Pendientes por Concepto" & "'"
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & ciudades & "'"

        ElseIf eOpPPE = 11 Then
            Me.Text = "Resumen de Reportes Pendientes por Concepto"
            reportPath = RutaReportes + "\ReporteListadoQuejasConcepto.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)

            Dim listatablas As New List(Of String)
            listatablas.Add("ListadoQuejas")
            DS = BaseII.ConsultaDS("sp_ListadoQuejasPorConcepto", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            Dim fechas As String = Nothing

            Dim con As New SqlConnection(MiConexion)
            con.Open()
            Dim comando As New SqlCommand()
            comando.Connection = con
            comando.CommandText = "exec DamePlazasTituloReporte " + identificador.ToString
            companias = "Distribuidor: "
            Dim reader As SqlDataReader = comando.ExecuteReader()
            contador = 0
            While reader.Read()
                If contador = 0 Then
                    companias = companias + reader(0).ToString
                    contador = contador + 1
                Else
                    companias = companias + ", " + reader(0).ToString
                End If
            End While
            reader.Close()
            comando.Dispose()
            comando.Connection = con
            comando.CommandText = "exec DameCompaniasTituloReporte " + identificador.ToString
            reader = comando.ExecuteReader()
            contador = 0
            ciudades = "Plazas: "
            While reader.Read()
                If contador = 0 Then
                    ciudades = ciudades + reader(0).ToString
                    contador = contador + 1
                Else
                    ciudades = ciudades + ", " + reader(0).ToString
                End If
            End While
            con.Close()
            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
            'BaseII.ConsultaDT("DameCiudadesTituloReporte")



            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'Resumen de Reportes Pendientes por Concepto'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo2").Text = "'" & ciudades & "'"

        ElseIf eOpPPE = 12 Then
            Me.Text = "Detalle de Quejas Pendientes por Concepto"
            reportPath = RutaReportes + "\ReporteQuejasConceptoListadoDetallado.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            BaseII.CreateMyParameter("@CiudadesXml", SqlDbType.Xml, DocCiudades.InnerXml)
            BaseII.CreateMyParameter("@LocalidadesXml", SqlDbType.Xml, DocLocalidades.InnerXml)
            BaseII.CreateMyParameter("@ConceptosXml", SqlDbType.Xml, DocConceptos.InnerXml)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteQuejasListadoDetallado")
            listatablas.Add("ListadoQuejas")
            DS = BaseII.ConsultaDS("ReporteQuejasConceptoListadoDetallado", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            ciudades = "Plazas: "
            Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            For Each elem As Xml.XmlElement In elementos
                If elem.GetAttribute("Seleccion") = "1" Then
                    If ciudades = "Plazas: " Then
                        ciudades = ciudades + elem.GetAttribute("Nombre")
                    Else
                        ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & "Detalle de Quejas Pendientes por Concepto" & "'"
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & ciudades & "'"
        ElseIf eOpPPE = 13 Then
            Me.Text = "Env�os Fallidos de Estados de Cuenta"
            reportPath = RutaReportes + "\ReporteBitacoraEdoCuenta.rpt"
            Dim DS As New DataSet
            DS.Clear()
            Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
                BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, eFechaIniPPE)
                BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, eFechaFinPPE)
                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteBitacoraEdoCuenta")
                DS = BaseII.ConsultaDS("sp_reporteBitacoraEnvioEdoCuenta", listatablas)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try


            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            ciudades = "Plazas: "
            Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            For Each elem As Xml.XmlElement In elementos
                If elem.GetAttribute("Seleccion") = "1" Then
                    If ciudades = "Plazas: " Then
                        ciudades = ciudades + elem.GetAttribute("Nombre")
                    Else
                        ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.DataDefinition.FormulaFields("Plazas").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
        ElseIf eOpPPE = 14 Then
            Me.Text = "Impresi�n de Contrato"
            Me.Text = "Impresi�n de Contrato"
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
            BaseII.CreateMyParameter("@nuevo", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("ValidaReportePorFecha")
            If BaseII.dicoPar("@nuevo") = 1 Then
                reportPath = RutaReportes + "\ReporteContratoCompletoVersion2.rpt"
            Else
                reportPath = RutaReportes + "\ReporteContratoCompleto.rpt"
            End If
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("General")
            tableNameList.Add("Clientes")
            tableNameList.Add("DatosFiscales")
            tableNameList.Add("DatosBancarios")
            tableNameList.Add("Periodos")
            tableNameList.Add("CostoSuscripcion")
            tableNameList.Add("CostoMensualidad")
            tableNameList.Add("CostoCambioDomicilio")
            tableNameList.Add("CostoReubicacion")
            tableNameList.Add("Calle")
            tableNameList.Add("Colonia")
            tableNameList.Add("Localidad")
            tableNameList.Add("Ciudad")
            tableNameList.Add("Otros")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, Contrato)
            dSet = BaseII.ConsultaDS("ReporteContratoTVZac", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)
        ElseIf eOpPPE = 15 Then
            Me.Text = "Impresi�n de Contrato"
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
            BaseII.CreateMyParameter("@nuevo", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("ValidaReportePorFecha")
            If BaseII.dicoPar("@nuevo") = 1 Then
                reportPath = RutaReportes + "\ReporteContratoCompleto2Version2.rpt"
            Else
                reportPath = RutaReportes + "\ReporteContratoCompleto2.rpt"
            End If

            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("General")
            tableNameList.Add("Clientes")
            tableNameList.Add("DatosFiscales")
            tableNameList.Add("DatosBancarios")
            tableNameList.Add("Periodos")
            tableNameList.Add("CostoSuscripcion")
            tableNameList.Add("CostoMensualidad")
            tableNameList.Add("CostoCambioDomicilio")
            tableNameList.Add("CostoReubicacion")
            tableNameList.Add("Calle")
            tableNameList.Add("Colonia")
            tableNameList.Add("Localidad")
            tableNameList.Add("Ciudad")
            tableNameList.Add("Otros")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, Contrato)
            dSet = BaseII.ConsultaDS("ReporteContratoTVZac", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)
        ElseIf eOpPPE = 16 Then
            Me.Text = "Impresi�n de Contrato"
            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
            'BaseII.CreateMyParameter("@nuevo", ParameterDirection.Output, SqlDbType.Int)
            'BaseII.ProcedimientoOutPut("ValidaReportePorFecha")
            'If BaseII.dicoPar("@nuevo") = 1 Then
            '    reportPath = RutaReportes + "\ReporteContratoCompletoInternetVersion2.rpt"
            'Else
            '    reportPath = RutaReportes + "\ReporteContratoCompletoInternet.rpt"
            'End If

            reportPath = RutaReportes + "\ReporteContratoVission.rpt"

            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            'tableNameList.Add("General")
            'tableNameList.Add("Clientes")
            'tableNameList.Add("CostoMensualidad")
            'tableNameList.Add("Calle")
            'tableNameList.Add("Colonia")
            'tableNameList.Add("Localidad")
            'tableNameList.Add("Ciudad")
            'tableNameList.Add("Otros")
            tableNameList.Add("Sucursales")
            tableNameList.Add("Cliente")
            tableNameList.Add("DatosFiscales")
            tableNameList.Add("Servicios")
            tableNameList.Add("Aparatos")
            tableNameList.Add("Plaza")
            tableNameList.Add("Compania")
      
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, Contrato)
            dSet = BaseII.ConsultaDS("ReporteContratoVission", tableNameList)
            Try
                customersByCityReport.Load(reportPath)
                SetDBReport(dSet, customersByCityReport)
            Catch ex As Exception

            End Try
            
        ElseIf eOpPPE = 17 Then

            Me.Text = "Resumen de Reportes por Identificador de Usuario"
            reportPath = RutaReportes + "\ReportesIdentificadorUsuario.rpt"
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("ReportesIdentificadorUsuario")
            tableNameList.Add("Detalles")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, eFechaIniPPE)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, eFechaFinPPE)
            BaseII.CreateMyParameter("@ConceptosXml", SqlDbType.Xml, DocConceptos.InnerXml)
            dSet = BaseII.ConsultaDS("ReportesIdentificadorUsuario", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

            ciudades = "Plazas: "
            Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            For Each elem As Xml.XmlElement In elementos
                If elem.GetAttribute("Seleccion") = "1" Then
                    If ciudades = "Plazas: " Then
                        ciudades = ciudades + elem.GetAttribute("Nombre")
                    Else
                        ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.DataDefinition.FormulaFields("Plazas").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
        ElseIf eOpPPE = 18 Then

            Me.Text = "Coincidencias en contrataciones"
            reportPath = RutaReportes + "\CoincidenciasContrataciones.rpt"
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("CoincidenciasContrataciones")
            tableNameList.Add("DetCoincidenciasContrataciones")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, eFechaIniPPE)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, eFechaFinPPE)
            dSet = BaseII.ConsultaDS("CoincidenciasContrataciones", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
        ElseIf eOpPPE = 19 Then

            Me.Text = "Ordenes de Clientes Zona Norte"
            reportPath = RutaReportes + "\OrdenesZonaNorte.rpt"
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("OrdenesZonaNorte")
            'tableNameList.Add("DetCoincidenciasContrataciones")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, eFechaIniPPE)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, eFechaFinPPE)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            dSet = BaseII.ConsultaDS("OrdenesZonaNorte", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

            ciudades = "Plazas: "
            Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            For Each elem As Xml.XmlElement In elementos
                If elem.GetAttribute("Seleccion") = "1" Then
                    If ciudades = "Plazas: " Then
                        ciudades = ciudades + elem.GetAttribute("Nombre")
                    Else
                        ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.DataDefinition.FormulaFields("Plazas").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
        ElseIf eOpPPE = 20 Then

            Me.Text = "Resumen de Clientes por Cobertura"
            reportPath = RutaReportes + "\ResumenCoberturas.rpt"
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("ResumenCoberturas")
            tableNameList.Add("Coberturas")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, eFechaIniPPE)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, eFechaFinPPE)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            dSet = BaseII.ConsultaDS("ResumenCoberturas", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

            ciudades = "Plazas: "
            Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            For Each elem As Xml.XmlElement In elementos
                If elem.GetAttribute("Seleccion") = "1" Then
                    If ciudades = "Plazas: " Then
                        ciudades = ciudades + elem.GetAttribute("Nombre")
                    Else
                        ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.DataDefinition.FormulaFields("Plazas").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
        ElseIf eOpPPE = 21 Then

            Me.Text = "Cuentas Clabe"
            reportPath = RutaReportes + "\CuentasClabe.rpt"
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("CuentasClabeAsignadas")
            tableNameList.Add("CuentasClabeDisponibles")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            dSet = BaseII.ConsultaDS("ReporteCuentasClabe", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)


            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
        ElseIf eOpPPE = 22 Then

            Me.Text = "Bit�cora de Movimientos Datalogic"
            reportPath = RutaReportes + "\BitacoraDatalogic.rpt"
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("BitacoraDatalogic")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, eFechaIniPPE)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, eFechaFinPPE)
            If GloOp = 2 Then
                BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            Else
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
            End If
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, GloOp)
            dSet = BaseII.ConsultaDS("BitacoraDatalogic", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

            If GloOp = 1 Then
                companias = "Contrato: " + op
                customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
            Else
                companias = "Distribuidores: "
                Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
                For Each elem As Xml.XmlElement In elementos2
                    If elem.GetAttribute("Seleccion") = "1" Then
                        If companias = "Distribuidores: " Then
                            companias = companias + elem.GetAttribute("nombre")
                        Else
                            companias = companias + ", " + elem.GetAttribute("nombre")
                        End If
                    End If
                Next
                customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
            End If

            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
        ElseIf eOpPPE = 23 Then

            Me.Text = "Coincidencias de IFE"
            reportPath = RutaReportes + "\CoincidenciasIFE.rpt"
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("CoincidenciasIFE")
            tableNameList.Add("Detalles")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, eFechaIniPPE)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, eFechaFinPPE)
            dSet = BaseII.ConsultaDS("CoincidenciasIFE", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
        ElseIf eOpPPE = 24 Then

            Me.Text = "Resumen de Cajas y Detalle de Cajas por Distribuidor"
            reportPath = RutaReportes + "\NumeroCajasPorPlaza.rpt"
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("DetalleCajas")
            tableNameList.Add("Resumen")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, eFechaIniPPE)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, eFechaFinPPE)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            dSet = BaseII.ConsultaDS("NumeroCajasPorPlaza", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
        ElseIf eOpPPE = 25 Then

            Me.Text = "Reporte de Tokens"
            reportPath = RutaReportes + "\ReporteToken.rpt"
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("ReporteToken")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, eFechaIniPPE)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, eFechaFinPPE)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            dSet = BaseII.ConsultaDS("ReporteToken", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            companias = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"

        ElseIf eOpPPE = 26 Then
            Me.Text = "Resumen Resumen Clientes"
            reportPath = RutaReportes + "\ResumenClientes.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.Int, GloIdResumenCliente)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteResumenClientes")
            DS = BaseII.ConsultaDS("Usp_ReporteResumenClientes", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        ElseIf eOpPPE = 27 Then

            Me.Text = "Resumen Resumen Clientes"
            reportPath = RutaReportes + "\ResumenClientes.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteResumenClientes")
            DS = BaseII.ConsultaDS("usp_Resumenclientes", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        ElseIf eOpPPE = 29 Then

            Me.Text = "Reporte Detalle de Ventas"
            reportPath = RutaReportes + "\ReporteDetalleVentas.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.Date, eFechaIni)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.Date, eFechaFin)


            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteDetalleventas")
            DS = BaseII.ConsultaDS("Usp_ReporteDetalleventas", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        ElseIf eOpPPE = 30 Then

            Me.Text = "Reporte Recuperaci�n"
            reportPath = RutaReportes + "\ReporteRecuperacion.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.Date, eFechaIni)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.Date, eFechaFin)


            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteRecuperacion")
            DS = BaseII.ConsultaDS("Usp_ReporteRecuperacion", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        ElseIf eOpPPE = 31 Then

            Me.Text = "Contrato TV"
            reportPath = RutaReportes + "\ReporteContratoTv.rpt"

            Dim dSet As New DataSet
            dSet.Clear()

            Dim tableNameList As New List(Of String)
            tableNameList.Add("Datos")
            tableNameList.Add("DatosFiscales")
            tableNameList.Add("Factura")
            tableNameList.Add("Tarifas")
            tableNameList.Add("Tvs")
            tableNameList.Add("XAgenda")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, eContrato)
            dSet = BaseII.ConsultaDS("REPORTEContratoTv", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

        ElseIf eOpPPE = 32 Then

            Me.Text = "Contrato Internet"
            reportPath = RutaReportes + "\REPORTEContratoInt.rpt"

            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("Datos")
            tableNameList.Add("DatosFiscales")
            tableNameList.Add("Tvs")
            tableNameList.Add("Tarifas")
            tableNameList.Add("Factura")
            tableNameList.Add("XAgenda")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, Contrato)
            dSet = BaseII.ConsultaDS("REPORTEContratoInt", tableNameList)

            customersByCityReport.Load(reportPath)
            SetDBReport(dSet, customersByCityReport)

        ElseIf eOpPPE = 33 Then
            Me.Text = "Resumen Nuevo"
            reportPath = RutaReportes + "\ResumenNuevo.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.Int, GloIdReporteResumen)

            Dim listatablas As New List(Of String)
            listatablas.Add("Tbl_ResumenNuevo")
            DS = BaseII.ConsultaDS("Usp_ReporteResumenNuevo", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        ElseIf eOpPPE = 34 Then

            Me.Text = "Resumen Nuevo"
            reportPath = RutaReportes + "\ResumenNuevo.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            Dim listatablas As New List(Of String)
            listatablas.Add("Tbl_ResumenNuevo")
            DS = BaseII.ConsultaDS("usp_ResumenNuevo", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
        ElseIf eOpPPE = 35 Then

            Me.Text = "Reporte Piratas"
            reportPath = RutaReportes + "\ReportePiratas.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fechaini", SqlDbType.Date, eFechaIni)
            BaseII.CreateMyParameter("@fechafin", SqlDbType.Date, eFechaFin)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReportePiratas")
            DS = BaseII.ConsultaDS("ReportePiratas", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIni + " A la Fecha: " + eFechaFin
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
        End If

        If eOpPPE <> 999 Then
            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            Me.CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
        End If
        varfrmselcompania = ""
        eOpPPE = 654
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub FrmImprimirPPE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ConfigureCrystalReports()
    End Sub

    Private Function LISTADOAdelantados() As DataSet
        BaseII.limpiaParametros()
        Dim tableNameList As New List(Of String)
        tableNameList.Add("LISTADOAdelantados")
        tableNameList.Add("GENERAL")
        Return BaseII.ConsultaDS("LISTADOAdelantados", tableNameList)
    End Function

End Class
