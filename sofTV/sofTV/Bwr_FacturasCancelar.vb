Imports System.Data.SqlClient
Public Class Bwr_FacturasCancelar
    Private Sub Llena_companias()
        Try


            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"
            If GloOpFacturas <> 3 Then
                If ComboBoxCompanias.Items.Count > 0 Then
                    ComboBoxCompanias.SelectedIndex = 0
                End If

                GloIdCompania = 0
            End If
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BrwFacturas_Cancelar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        Dim op As Integer
        colorea(Me, Me.Name)
        CON.Open()
        'Llena_companias()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet1.MUESTRATIPOFACTURA' Puede moverla o quitarla seg�n sea necesario.
        Me.DAMEFECHADELSERVIDORTableAdapter.Connection = CON
        Me.DAMEFECHADELSERVIDORTableAdapter.Fill(Me.NewsoftvDataSet1.DAMEFECHADELSERVIDOR, Me.FECHADateTimePicker.Text)
        CON.Close()
        'Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
        'Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRATIPOFACTURA, 1)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opc", SqlDbType.Int, 2)
        ComboBox4.DataSource = BaseII.ConsultaDT("MUESTRATIPOFACTURA")
        ComboBox4.DisplayMember = "CONCEPTO"
        ComboBox4.ValueMember = "CLAVE"

        If IdSistema = "LO" Or IdSistema = "YU" Then
            CMBLabel5.Text = "Tipo de Pago"
            CMBLabel1.Text = "Buscar Pago por :"
            Button2.Text = "&Cancelar Pago"
            Button6.Text = "&Reimprimir Pago"
            Button8.Text = "&Ver Pago"
            Label8.Text = "Datos del Pago"
        ElseIf IdSistema = "AG" Or IdSistema = "VA" Then
            op = 2
        Else
            op = 1
        End If
        GloTipo = Me.ComboBox4.SelectedValue
        If GloOpFacturas = 0 Then
            If IdSistema = "LO" Or IdSistema = "YU" Then
                Me.Text = "Cancelaci�n de Pagos"
            Else
                Me.Text = "Cancelaci�n de Tickets"
            End If
            Me.CMBPanel2.Visible = True
            Me.CMBPanel3.Visible = False
            Me.CMBPanel4.Visible = False
            Me.Panel5.Visible = True
            Me.Button8.TabStop = False
            Me.Button6.TabStop = False
            CON.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLidia2.MUESTRATIPOFACTURA, 1)
            'Me.BUSCAFACTURASTableAdapter.Connection = CON
            'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 0, "", 0, "01/01/1900", 0, "", GloTipo)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, GloTipo)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            DataGridView1.DataSource = BaseII.ConsultaDT("BUSCAFACTURAS")
            If DataGridView1.Rows.Count > 0 Then
                Label9.Text = DataGridView1.SelectedCells(8).Value
                Clv_FacturaLabel1.Text = DataGridView1.SelectedCells(0).Value
                SerieLabel1.Text = DataGridView1.SelectedCells(1).Value
                FacturaLabel1.Text = DataGridView1.SelectedCells(2).Value
                FECHALabel1.Text = DataGridView1.SelectedCells(3).Value
                ClienteLabel1.Text = DataGridView1.SelectedCells(4).Value
                CMBNOMBRETextBox1.Text = DataGridView1.SelectedCells(5).Value
                ImporteLabel1.Text = DataGridView1.SelectedCells(6).Value
            End If
            CON.Close()
        ElseIf GloOpFacturas = 1 Then
            If IdSistema = "LO" Or IdSistema = "YU" Then
                Me.Text = "Reimpresi�n de Pagos"
            Else
                Me.Text = "Reimpresi�n de Tickets"
            End If
            Me.CMBPanel2.Visible = False
            Me.CMBPanel3.Visible = True
            Me.CMBPanel4.Visible = False
            Me.Panel5.Visible = True
            CON.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLidia2.MUESTRATIPOFACTURA, 1)
            Me.BUSCAFACTURASTableAdapter.Connection = CON
            Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 0, "", 0, "01/01/1900", 0, "", GloTipo)
            CON.Close()
            Me.Button8.TabStop = False
            Me.Button2.TabStop = False
        ElseIf GloOpFacturas = 3 Then
            Me.Text = "Ver Historial de Pagos"
            ComboBoxCompanias.Visible = False
            Label22.Visible = False
            Me.CMBPanel2.Visible = False
            Me.CMBPanel3.Visible = False
            Me.CMBPanel4.Visible = True
            Me.Panel5.Visible = False
            'CON.Open()
            'Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            'Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLidia2.MUESTRATIPOFACTURA, op)
            'Me.BUSCAFACTURASTableAdapter.Connection = CON
            'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 13, "", 0, "01/01/1900", Contrato, "", GloTipo)
            'CON.Close()
            Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 13)
                BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, "01/01/1900")
                BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, GloTipo)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                DataGridView1.DataSource = BaseII.ConsultaDT("BUSCAFACTURAS")
                DataGridView1.Columns("clv_factura").Visible = False
                If DataGridView1.Rows.Count > 0 Then
                    Label9.Text = DataGridView1.SelectedCells(8).Value
                    Clv_FacturaLabel1.Text = DataGridView1.SelectedCells(0).Value
                    SerieLabel1.Text = DataGridView1.SelectedCells(1).Value
                    FacturaLabel1.Text = DataGridView1.SelectedCells(2).Value
                    FECHALabel1.Text = DataGridView1.SelectedCells(3).Value
                    ClienteLabel1.Text = DataGridView1.SelectedCells(4).Value
                    CMBNOMBRETextBox1.Text = DataGridView1.SelectedCells(5).Value
                    ImporteLabel1.Text = DataGridView1.SelectedCells(6).Value
                End If
                Me.ComboBox4.Text = ""
                Me.Button2.TabStop = False
                Me.Button6.TabStop = False
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub buscaNotas(ByVal opcion As Integer)
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Try
            conlidia.Open()
            Me.BUSCANOTASDECREDITOTableAdapter.Connection = conlidia
            If opcion = 5 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.BUSCANOTASDECREDITO, opcion, 0, Me.FECHATextBox.Text, Contrato, 0, "")
            ElseIf opcion = 6 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.BUSCANOTASDECREDITO, opcion, Me.FOLIOTextBox.Text, "01/01/1900", Contrato, 0, "")
            ElseIf opcion = 2 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.BUSCANOTASDECREDITO, opcion, 0, Me.FECHATextBox.Text, 0, 0, "")
            ElseIf opcion = 3 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.BUSCANOTASDECREDITO, opcion, 0, "01/01/1900", Me.CONTRATOTextBox.Text, 0, "")
            ElseIf opcion = 4 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.BUSCANOTASDECREDITO, 3, 0, "01/01/1900", Contrato, 0, "")
            End If

            conlidia.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Busca(ByVal OP As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            BaseII.limpiaParametros()
            If GloOpFacturas = 3 Then
                If OP = 0 Then
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 10, "", 0, "01/01/1900", GloContrato, "", Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 10)
                    BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, GloContrato)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                ElseIf OP = 1 Then
                    If IsNumeric(Me.FOLIOTextBox.Text) = False Then Me.FOLIOTextBox.Text = 0
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 11, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "01/01/1900", GloContrato, "", Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 11)
                    BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, Me.SERIETextBox.Text)
                    BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, Me.FOLIOTextBox.Text)
                    BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, GloContrato)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                ElseIf OP = 2 Then
                    If IsDate(Me.FECHATextBox.Text) = False Then Me.FECHATextBox.Text = "01/01/1900"
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 12, "", 0, Me.FECHATextBox.Text, GloContrato, "", Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 12)
                    BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, Me.FECHATextBox.Text)
                    BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, GloContrato)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                End If
            Else
                If OP = 0 Then
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 0, "", 0, "01/01/1900", 0, "", Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                ElseIf OP = 1 Then
                    If IsNumeric(Me.FOLIOTextBox.Text) = False Then Me.FOLIOTextBox.Text = 0
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 1, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "01/01/1900", 0, "", Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 1)
                    BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, Me.SERIETextBox.Text)
                    BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, Me.FOLIOTextBox.Text)
                    BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                ElseIf OP = 2 Then
                    If IsDate(Me.FECHATextBox.Text) = False Then Me.FECHATextBox.Text = "01/01/1900"
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 2, "", 0, Me.FECHATextBox.Text, 0, "", Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 2)
                    BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, Me.FECHATextBox.Text)
                    BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                ElseIf OP = 3 Then
                    If IsNumeric(Me.CONTRATOTextBox.Text) = False Then Me.CONTRATOTextBox.Text = 0
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 3, "", 0, "01/01/1900", Me.CONTRATOTextBox.Text, "", Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 3)
                    BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Me.CONTRATOTextBox.Text)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.ComboBox4.SelectedValue)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                ElseIf OP = 4 Then
                    If Len(Trim(Me.NOMBRETextBox.Text)) > 0 Then
                        'Me.BUSCAFACTURASTableAdapter.Connection = CON
                        'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 4, "", 0, "01/01/1900", 0, Me.NOMBRETextBox.Text, Me.ComboBox4.SelectedValue)
                        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 4)
                        BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, "")
                        BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, 0)
                        BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, "01/01/1900")
                        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, 0)
                        BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, Me.NOMBRETextBox.Text)
                        BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.ComboBox4.SelectedValue)
                        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                    Else
                        MsgBox("La B�squeda no se puede realizar sin Datos", MsgBoxStyle.Information)
                    End If
                End If
            End If
            CON.Close()
            DataGridView1.DataSource = BaseII.ConsultaDT("BUSCAFACTURAS")
            If DataGridView1.Rows.Count > 0 Then
                Label9.Text = DataGridView1.SelectedCells(8).Value
                Clv_FacturaLabel1.Text = DataGridView1.SelectedCells(0).Value
                SerieLabel1.Text = DataGridView1.SelectedCells(1).Value
                FacturaLabel1.Text = DataGridView1.SelectedCells(2).Value
                FECHALabel1.Text = DataGridView1.SelectedCells(3).Value
                ClienteLabel1.Text = DataGridView1.SelectedCells(4).Value
                CMBNOMBRETextBox1.Text = DataGridView1.SelectedCells(5).Value
                ImporteLabel1.Text = DataGridView1.SelectedCells(6).Value
            End If
            Me.SERIETextBox.Clear()
            Me.FOLIOTextBox.Clear()
            Me.FECHATextBox.Clear()
            Me.CONTRATOTextBox.Clear()
            Me.NOMBRETextBox.Clear()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Try

        
        If Me.ComboBox4.SelectedValue = "N" Then
            Me.BUSCANOTASDECREDITODataGridView.Visible = True
            Me.CMBLabel1.Text = "Buscar Nota por:"
            Me.Label3.Visible = False
            Me.Panel2.Visible = True
            Me.SERIETextBox.Visible = False
            Me.Label4.Text = "Nota :"
            Me.Button8.Text = "&Ver Nota"
            buscaNotas(4)
        Else
            If IdSistema = "LO" Or IdSistema = "YU" Then
                Me.Button8.Text = "&Ver Pago"
                Me.CMBLabel1.Text = "Buscar Pago por:"
            Else
                Me.Button8.Text = "&Ver Ticket"
                Me.CMBLabel1.Text = "Buscar Ticket por:"
            End If

            Me.Label3.Visible = True
            Me.SERIETextBox.Visible = True
            Me.Panel2.Visible = False
                'Me.Label4.Text = "Serie :"
            GloTipo = ComboBox4.SelectedValue
            Me.BUSCANOTASDECREDITODataGridView.Visible = False
            If GloOpFacturas <> 3 Then
                Busca(0)
            Else
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 14)
                BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@FACTURA", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, "01/01/1900")
                BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, GloTipo)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                DataGridView1.DataSource = BaseII.ConsultaDT("BUSCAFACTURAS")
                If DataGridView1.Rows.Count > 0 Then
                    Label9.Text = DataGridView1.SelectedCells(8).Value
                    Clv_FacturaLabel1.Text = DataGridView1.SelectedCells(0).Value
                    SerieLabel1.Text = DataGridView1.SelectedCells(1).Value
                    FacturaLabel1.Text = DataGridView1.SelectedCells(2).Value
                    FECHALabel1.Text = DataGridView1.SelectedCells(3).Value
                    ClienteLabel1.Text = DataGridView1.SelectedCells(4).Value
                    CMBNOMBRETextBox1.Text = DataGridView1.SelectedCells(5).Value
                    ImporteLabel1.Text = DataGridView1.SelectedCells(6).Value
                End If
            End If

        End If
            GloTipo = Me.ComboBox4.SelectedValue
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If Me.ComboBox4.SelectedValue = "N" Then
            buscaNotas(6)
        Else
            Busca(1)
        End If
    End Sub

    Private Sub FECHATextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FECHATextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.ComboBox4.SelectedValue = "N" Then
                buscaNotas(5)
            Else
                Busca(2)
            End If

        End If
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ComboBox4.SelectedValue = "N" Then
            buscaNotas(5)
        Else
            Busca(2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Busca(3)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Busca(4)
    End Sub

    Private Sub SERIETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SERIETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub


    Private Sub FOLIOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FOLIOTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.ComboBox4.SelectedValue = "N" Then
                buscaNotas(6)
            Else
                Busca(1)
            End If
        End If
    End Sub


    Private Sub CONTRATOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CONTRATOTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub


    Private Sub NOMBRETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NOMBRETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(4)
        End If
    End Sub

    'Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    'End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim resp As MsgBoxResult

        If IsNumeric(Me.Clv_FacturaLabel1.Text) = True Then
            If DateValue(Me.FECHADateTimePicker.Text) = DateValue(Me.FECHALabel1.Text) Then
                resp = MsgBox("Deseas Cancelar la " & Me.ComboBox4.Text & " : " & Me.SerieLabel1.Text & "-" & Me.FacturaLabel1.Text, MsgBoxStyle.OkCancel, "Cancelaci�n de Tickets")
                If resp = MsgBoxResult.Ok Then
                    CANCELAFACTURA()
                End If
            Else

                MsgBox("S�lo se puede cancelar las Tickets del d�a hoy", MsgBoxStyle.Information)
            End If
        Else
            If IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("Seleccione la Pago ", MsgBoxStyle.Information)
            Else
                MsgBox("Seleccione la Ticket ", MsgBoxStyle.Information)
            End If

        End If
    End Sub

    Private Sub FacturaLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FacturaLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub CANCELAFACTURA()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim MSG As String = Nothing
            Dim BNDERROR As Integer = 0
            Me.CANCELACIONFACTURASTableAdapter.Connection = CON
            Me.CANCELACIONFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.CANCELACIONFACTURAS, Me.Clv_FacturaLabel1.Text, 0, MSG, BNDERROR)
            MsgBox(MSG)
            If BNDERROR = 0 Then
                Busca(0)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If IsNumeric(Me.Clv_FacturaLabel1.Text) = True Then
            GloClv_Factura = Me.Clv_FacturaLabel1.Text
            LocGloOpRep = 0
            FrmImprimirFac.Show()
        Else
            If IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("Seleccione la Pago ", MsgBoxStyle.Information)
            Else
                MsgBox("Seleccione la Ticket ", MsgBoxStyle.Information)
            End If

        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        GloTipo = ComboBox4.SelectedValue
        Clv_FacturaLabel1.Text = DataGridView1.SelectedCells(0).Value
        If Me.ComboBox4.SelectedValue = "N" Then
            If IsNumeric(Me.Label20.Text) = True Then
                gloClvNota = Me.Label20.Text
                LocGloOpRep = 3
                FrmImprimirFac.Show()
            Else
                MsgBox("Seleccione la Nota ", MsgBoxStyle.Information)

            End If
        Else
            If IdSistema = "LO" Or IdSistema = "YU" Then LocGloOpRep = 6
            LocGloOpRep = 0
            If IsNumeric(Me.Clv_FacturaLabel1.Text) = True Then
                GloClv_Factura = Me.Clv_FacturaLabel1.Text
                FrmImprimirFac.Show()
            Else
                If IdSistema = "LO" Or IdSistema = "YU" Then
                    MsgBox("Seleccione la Pago ", MsgBoxStyle.Information)
                Else
                    MsgBox("Seleccione el Ticket ", MsgBoxStyle.Information)
                End If
            End If
        End If
    End Sub

    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub BUSCANOTASDECREDITODataGridView_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim cone As New SqlConnection(MiConexion)
        If Me.Label20.Text <> "" Then
            cone.Open()
            Me.DetalleNOTASDECREDITOTableAdapter.Connection = cone
            Me.DetalleNOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.DetalleNOTASDECREDITO, Me.Label20.Text)
            cone.Close()
        End If
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged

    End Sub
End Class