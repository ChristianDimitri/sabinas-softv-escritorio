<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTipoPaqAdicionalesTel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBClv_Tipo_Paquete_AdiocionalLabel As System.Windows.Forms.Label
        Dim CMBDescripcionLabel As System.Windows.Forms.Label
        Dim CMBTipo_CobroLabel As System.Windows.Forms.Label
        Dim DescripcionLabel1 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim CMBLabel3 As System.Windows.Forms.Label
        Dim CMBLabel4 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Clv_ServicioLabel As System.Windows.Forms.Label
        Dim CostoLabel As System.Windows.Forms.Label
        Dim DESCRIPCIONLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTipoPaqAdicionalesTel))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.Consulta_tipo_paquete_AdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_tipo_paquete_AdicionalTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_tipo_paquete_AdicionalTableAdapter()
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_Tipo_Paquete_AdiocionalTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.Tipo_CobroTextBox = New System.Windows.Forms.TextBox()
        Me.Muestra_CatalogoTipoCobrosTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_CatalogoTipoCobrosTelTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_CatalogoTipoCobrosTelTableAdapter()
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.DescripcionComboBox1 = New System.Windows.Forms.ComboBox()
        Me.DameClasificacionLlamadasBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clasificacion_LDTextBox = New System.Windows.Forms.TextBox()
        Me.TextBoxClv_Session = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CatalogoClasificacionLlamnadasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetTelefonia = New sofTV.DataSetTelefonia()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.Muestra_PaisesRelTipoPaqBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_PaisesRelTipoPaqTableAdapter = New sofTV.DataSetLidia2TableAdapters.Muestra_PaisesRelTipoPaqTableAdapter()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Catalogo_Clasificacion_LlamnadasTableAdapter = New sofTV.DataSetTelefoniaTableAdapters.Catalogo_Clasificacion_LlamnadasTableAdapter()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Consultar_Rel_Servicios_PaqAdic_TMPBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DESCRIPCIONTextBox1 = New System.Windows.Forms.TextBox()
        Me.CostoTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.TextBoxCosto_Det = New System.Windows.Forms.TextBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ServiciosTableAdapter = New sofTV.DataSetTelefoniaTableAdapters.ServiciosTableAdapter()
        Me.Consultar_Rel_Servicios_PaqAdic_TMPTableAdapter = New sofTV.DataSetTelefoniaTableAdapters.consultar_Rel_Servicios_PaqAdic_TMPTableAdapter()
        Me.Nuevo_Rel_Servicios_PaqAdic_TMPBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Nuevo_Rel_Servicios_PaqAdic_TMPTableAdapter = New sofTV.DataSetTelefoniaTableAdapters.Nuevo_Rel_Servicios_PaqAdic_TMPTableAdapter()
        Me.Borrar_Rel_Servicios_PaqAdic_TMPBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borrar_Rel_Servicios_PaqAdic_TMPTableAdapter = New sofTV.DataSetTelefoniaTableAdapters.Borrar_Rel_Servicios_PaqAdic_TMPTableAdapter()
        Me.DataTable1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataTable1TableAdapter = New sofTV.DataSetTelefoniaTableAdapters.DataTable1TableAdapter()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Nuevo_Tipo_Paquete_AdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Nuevo_Tipo_Paquete_AdicionalTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Nuevo_Tipo_Paquete_AdicionalTableAdapter()
        Me.DameClasificacionLlamadasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClasificacionLlamadasTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.DameClasificacionLlamadasTableAdapter()
        CMBClv_Tipo_Paquete_AdiocionalLabel = New System.Windows.Forms.Label()
        CMBDescripcionLabel = New System.Windows.Forms.Label()
        CMBTipo_CobroLabel = New System.Windows.Forms.Label()
        DescripcionLabel1 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        CMBLabel3 = New System.Windows.Forms.Label()
        CMBLabel4 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Clv_ServicioLabel = New System.Windows.Forms.Label()
        CostoLabel = New System.Windows.Forms.Label()
        DESCRIPCIONLabel = New System.Windows.Forms.Label()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_tipo_paquete_AdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_tipo_paquete_AdicionalBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.SuspendLayout()
        CType(Me.Muestra_CatalogoTipoCobrosTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel1.SuspendLayout()
        CType(Me.DameClasificacionLlamadasBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CatalogoClasificacionLlamnadasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetTelefonia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_PaisesRelTipoPaqBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consultar_Rel_Servicios_PaqAdic_TMPBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Nuevo_Rel_Servicios_PaqAdic_TMPBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borrar_Rel_Servicios_PaqAdic_TMPBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataTable1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.Nuevo_Tipo_Paquete_AdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClasificacionLlamadasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBClv_Tipo_Paquete_AdiocionalLabel
        '
        CMBClv_Tipo_Paquete_AdiocionalLabel.AutoSize = True
        CMBClv_Tipo_Paquete_AdiocionalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBClv_Tipo_Paquete_AdiocionalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CMBClv_Tipo_Paquete_AdiocionalLabel.Location = New System.Drawing.Point(157, 21)
        CMBClv_Tipo_Paquete_AdiocionalLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBClv_Tipo_Paquete_AdiocionalLabel.Name = "CMBClv_Tipo_Paquete_AdiocionalLabel"
        CMBClv_Tipo_Paquete_AdiocionalLabel.Size = New System.Drawing.Size(60, 18)
        CMBClv_Tipo_Paquete_AdiocionalLabel.TabIndex = 2
        CMBClv_Tipo_Paquete_AdiocionalLabel.Text = "Clave :"
        '
        'CMBDescripcionLabel
        '
        CMBDescripcionLabel.AutoSize = True
        CMBDescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBDescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CMBDescripcionLabel.Location = New System.Drawing.Point(103, 46)
        CMBDescripcionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBDescripcionLabel.Name = "CMBDescripcionLabel"
        CMBDescripcionLabel.Size = New System.Drawing.Size(108, 18)
        CMBDescripcionLabel.TabIndex = 4
        CMBDescripcionLabel.Text = "Descripción :"
        '
        'CMBTipo_CobroLabel
        '
        CMBTipo_CobroLabel.AutoSize = True
        CMBTipo_CobroLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBTipo_CobroLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CMBTipo_CobroLabel.Location = New System.Drawing.Point(111, 135)
        CMBTipo_CobroLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBTipo_CobroLabel.Name = "CMBTipo_CobroLabel"
        CMBTipo_CobroLabel.Size = New System.Drawing.Size(103, 18)
        CMBTipo_CobroLabel.TabIndex = 6
        CMBTipo_CobroLabel.Text = "Tipo Cobro :"
        '
        'DescripcionLabel1
        '
        DescripcionLabel1.AutoSize = True
        DescripcionLabel1.Location = New System.Drawing.Point(291, 98)
        DescripcionLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel1.Name = "DescripcionLabel1"
        DescripcionLabel1.Size = New System.Drawing.Size(86, 17)
        DescripcionLabel1.TabIndex = 8
        DescripcionLabel1.Text = "Descripcion:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.Location = New System.Drawing.Point(9, 20)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(59, 18)
        Label1.TabIndex = 10
        Label1.Text = "Paises"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.Location = New System.Drawing.Point(565, 20)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(263, 18)
        Label2.TabIndex = 11
        Label2.Text = "Paises que pertenecen al Paquete"
        '
        'CMBLabel3
        '
        CMBLabel3.AutoSize = True
        CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel3.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel3.Location = New System.Drawing.Point(456, 9)
        CMBLabel3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBLabel3.Name = "CMBLabel3"
        CMBLabel3.Size = New System.Drawing.Size(64, 18)
        CMBLabel3.TabIndex = 10
        CMBLabel3.Text = "Costo :"
        CMBLabel3.Visible = False
        AddHandler CMBLabel3.Click, AddressOf Me.Label3_Click
        '
        'CMBLabel4
        '
        CMBLabel4.AutoSize = True
        CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel4.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel4.Location = New System.Drawing.Point(95, 102)
        CMBLabel4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBLabel4.Name = "CMBLabel4"
        CMBLabel4.Size = New System.Drawing.Size(115, 18)
        CMBLabel4.TabIndex = 13
        CMBLabel4.Text = "Clasificación :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(48, 52)
        Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(119, 18)
        Label3.TabIndex = 15
        Label3.Text = "Plan Tarifario  "
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(503, 52)
        Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(64, 18)
        Label4.TabIndex = 16
        Label4.Text = "Costo  "
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(8, 12)
        Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(141, 25)
        Label5.TabIndex = 17
        Label5.Text = "Plan Tarifario"
        '
        'Clv_ServicioLabel
        '
        Clv_ServicioLabel.AutoSize = True
        Clv_ServicioLabel.Location = New System.Drawing.Point(743, 231)
        Clv_ServicioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_ServicioLabel.Name = "Clv_ServicioLabel"
        Clv_ServicioLabel.Size = New System.Drawing.Size(85, 17)
        Clv_ServicioLabel.TabIndex = 20
        Clv_ServicioLabel.Text = "Clv Servicio:"
        '
        'CostoLabel
        '
        CostoLabel.AutoSize = True
        CostoLabel.Location = New System.Drawing.Point(756, 148)
        CostoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CostoLabel.Name = "CostoLabel"
        CostoLabel.Size = New System.Drawing.Size(48, 17)
        CostoLabel.TabIndex = 20
        CostoLabel.Text = "Costo:"
        '
        'DESCRIPCIONLabel
        '
        DESCRIPCIONLabel.AutoSize = True
        DESCRIPCIONLabel.Location = New System.Drawing.Point(737, 262)
        DESCRIPCIONLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DESCRIPCIONLabel.Name = "DESCRIPCIONLabel"
        DESCRIPCIONLabel.Size = New System.Drawing.Size(104, 17)
        DESCRIPCIONLabel.TabIndex = 21
        DESCRIPCIONLabel.Text = "DESCRIPCION:"
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_tipo_paquete_AdicionalBindingSource
        '
        Me.Consulta_tipo_paquete_AdicionalBindingSource.DataMember = "Consulta_tipo_paquete_Adicional"
        Me.Consulta_tipo_paquete_AdicionalBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Consulta_tipo_paquete_AdicionalTableAdapter
        '
        Me.Consulta_tipo_paquete_AdicionalTableAdapter.ClearBeforeFill = True
        '
        'Consulta_tipo_paquete_AdicionalBindingNavigator
        '
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.AddNewItem = Nothing
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.BindingSource = Me.Consulta_tipo_paquete_AdicionalBindingSource
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.CountItem = Nothing
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem})
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.Name = "Consulta_tipo_paquete_AdicionalBindingNavigator"
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.PositionItem = Nothing
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.Size = New System.Drawing.Size(1009, 28)
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.TabIndex = 5
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.TabStop = True
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(103, 25)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem
        '
        Me.Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem.Name = "Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem"
        Me.Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem.Size = New System.Drawing.Size(156, 25)
        Me.Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'Clv_Tipo_Paquete_AdiocionalTextBox
        '
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_tipo_paquete_AdicionalBindingSource, "Clv_Tipo_Paquete_Adiocional", True))
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Enabled = False
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Location = New System.Drawing.Point(232, 14)
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Name = "Clv_Tipo_Paquete_AdiocionalTextBox"
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Size = New System.Drawing.Size(231, 24)
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.TabIndex = 1
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_tipo_paquete_AdicionalBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(232, 46)
        Me.DescripcionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(716, 38)
        Me.DescripcionTextBox.TabIndex = 2
        '
        'Tipo_CobroTextBox
        '
        Me.Tipo_CobroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_tipo_paquete_AdicionalBindingSource, "Tipo_Cobro", True))
        Me.Tipo_CobroTextBox.Location = New System.Drawing.Point(653, 98)
        Me.Tipo_CobroTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Tipo_CobroTextBox.Name = "Tipo_CobroTextBox"
        Me.Tipo_CobroTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Tipo_CobroTextBox.TabIndex = 7
        '
        'Muestra_CatalogoTipoCobrosTelBindingSource
        '
        Me.Muestra_CatalogoTipoCobrosTelBindingSource.DataMember = "Muestra_CatalogoTipoCobrosTel"
        Me.Muestra_CatalogoTipoCobrosTelBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Muestra_CatalogoTipoCobrosTelTableAdapter
        '
        Me.Muestra_CatalogoTipoCobrosTelTableAdapter.ClearBeforeFill = True
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.DataSource = Me.Muestra_CatalogoTipoCobrosTelBindingSource
        Me.DescripcionComboBox.DisplayMember = "Descripcion"
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(232, 126)
        Me.DescripcionComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(320, 26)
        Me.DescripcionComboBox.TabIndex = 4
        Me.DescripcionComboBox.ValueMember = "clv_tipo_cobro"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(812, 780)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CMBPanel1
        '
        Me.CMBPanel1.Controls.Add(Me.DescripcionComboBox1)
        Me.CMBPanel1.Controls.Add(Me.DescripcionTextBox)
        Me.CMBPanel1.Controls.Add(Me.DescripcionComboBox)
        Me.CMBPanel1.Controls.Add(CMBLabel4)
        Me.CMBPanel1.Controls.Add(CMBClv_Tipo_Paquete_AdiocionalLabel)
        Me.CMBPanel1.Controls.Add(CMBTipo_CobroLabel)
        Me.CMBPanel1.Controls.Add(CMBDescripcionLabel)
        Me.CMBPanel1.Controls.Add(Me.Clv_Tipo_Paquete_AdiocionalTextBox)
        Me.CMBPanel1.Location = New System.Drawing.Point(15, 34)
        Me.CMBPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(972, 172)
        Me.CMBPanel1.TabIndex = 0
        '
        'DescripcionComboBox1
        '
        Me.DescripcionComboBox1.DataSource = Me.DameClasificacionLlamadasBindingSource1
        Me.DescripcionComboBox1.DisplayMember = "Descripcion"
        Me.DescripcionComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox1.FormattingEnabled = True
        Me.DescripcionComboBox1.Location = New System.Drawing.Point(232, 92)
        Me.DescripcionComboBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionComboBox1.Name = "DescripcionComboBox1"
        Me.DescripcionComboBox1.Size = New System.Drawing.Size(320, 26)
        Me.DescripcionComboBox1.TabIndex = 3
        Me.DescripcionComboBox1.ValueMember = "Clave"
        '
        'DameClasificacionLlamadasBindingSource1
        '
        Me.DameClasificacionLlamadasBindingSource1.DataMember = "DameClasificacionLlamadas"
        Me.DameClasificacionLlamadasBindingSource1.DataSource = Me.Procedimientosarnoldo4
        '
        'Clasificacion_LDTextBox
        '
        Me.Clasificacion_LDTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_tipo_paquete_AdicionalBindingSource, "Clasificacion_LD", True))
        Me.Clasificacion_LDTextBox.Location = New System.Drawing.Point(731, 6)
        Me.Clasificacion_LDTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clasificacion_LDTextBox.Name = "Clasificacion_LDTextBox"
        Me.Clasificacion_LDTextBox.ReadOnly = True
        Me.Clasificacion_LDTextBox.Size = New System.Drawing.Size(12, 22)
        Me.Clasificacion_LDTextBox.TabIndex = 20
        Me.Clasificacion_LDTextBox.TabStop = False
        '
        'TextBoxClv_Session
        '
        Me.TextBoxClv_Session.Location = New System.Drawing.Point(643, 2)
        Me.TextBoxClv_Session.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxClv_Session.Name = "TextBoxClv_Session"
        Me.TextBoxClv_Session.ReadOnly = True
        Me.TextBoxClv_Session.Size = New System.Drawing.Size(12, 22)
        Me.TextBoxClv_Session.TabIndex = 19
        Me.TextBoxClv_Session.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_tipo_paquete_AdicionalBindingSource, "Costo", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(532, 5)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(12, 26)
        Me.TextBox1.TabIndex = 11
        Me.TextBox1.TabStop = False
        Me.TextBox1.Visible = False
        '
        'CatalogoClasificacionLlamnadasBindingSource
        '
        Me.CatalogoClasificacionLlamnadasBindingSource.DataMember = "Catalogo_Clasificacion_Llamnadas"
        Me.CatalogoClasificacionLlamnadasBindingSource.DataSource = Me.DataSetTelefonia
        '
        'DataSetTelefonia
        '
        Me.DataSetTelefonia.DataSetName = "DataSetTelefonia"
        Me.DataSetTelefonia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(569, 42)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(383, 174)
        Me.TreeView1.TabIndex = 4
        '
        'TreeView2
        '
        Me.TreeView2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView2.Location = New System.Drawing.Point(13, 42)
        Me.TreeView2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(383, 174)
        Me.TreeView2.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(405, 182)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(148, 34)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "Agregar Todos >>"
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(405, 98)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(148, 34)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Quitar <<"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(405, 140)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(148, 34)
        Me.Button3.TabIndex = 16
        Me.Button3.Text = "Quitar Todos <<"
        Me.Button3.UseVisualStyleBackColor = False
        Me.Button3.Visible = False
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Muestra_PaisesRelTipoPaqBindingSource
        '
        Me.Muestra_PaisesRelTipoPaqBindingSource.DataMember = "Muestra_PaisesRelTipoPaq"
        Me.Muestra_PaisesRelTipoPaqBindingSource.DataSource = Me.DataSetLidia2
        '
        'Muestra_PaisesRelTipoPaqTableAdapter
        '
        Me.Muestra_PaisesRelTipoPaqTableAdapter.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(405, 57)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(148, 34)
        Me.Button4.TabIndex = 2
        Me.Button4.Text = "Agregar >>"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Catalogo_Clasificacion_LlamnadasTableAdapter
        '
        Me.Catalogo_Clasificacion_LlamnadasTableAdapter.ClearBeforeFill = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView)
        Me.Panel1.Controls.Add(DESCRIPCIONLabel)
        Me.Panel1.Controls.Add(Me.DESCRIPCIONTextBox1)
        Me.Panel1.Controls.Add(CostoLabel)
        Me.Panel1.Controls.Add(Me.CostoTextBox)
        Me.Panel1.Controls.Add(Clv_ServicioLabel)
        Me.Panel1.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Panel1.Controls.Add(Me.Button7)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Label5)
        Me.Panel1.Controls.Add(Label4)
        Me.Panel1.Controls.Add(Label3)
        Me.Panel1.Controls.Add(Me.TextBoxCosto_Det)
        Me.Panel1.Controls.Add(Me.ComboBox2)
        Me.Panel1.Location = New System.Drawing.Point(15, 214)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(972, 311)
        Me.Panel1.TabIndex = 1
        '
        'Consultar_Rel_Servicios_PaqAdic_TMPDataGridView
        '
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.AllowUserToAddRows = False
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.AllowUserToDeleteRows = False
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.AllowUserToOrderColumns = True
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.AutoGenerateColumns = False
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6})
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.DataSource = Me.Consultar_Rel_Servicios_PaqAdic_TMPBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.DefaultCellStyle = DataGridViewCellStyle3
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.Location = New System.Drawing.Point(36, 122)
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.MultiSelect = False
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.Name = "Consultar_Rel_Servicios_PaqAdic_TMPDataGridView"
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.ReadOnly = True
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.Size = New System.Drawing.Size(901, 186)
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.TabIndex = 19
        Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clave_Id"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clave_Id"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 5
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_Session"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_Session"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 5
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Clv_Tipo_Paquete_Adic"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Clv_Tipo_Paquete_Adic"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 5
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 5
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "DESCRIPCION"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Plan Tarifario"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 350
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Costo"
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn6.HeaderText = "Costo"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 150
        '
        'Consultar_Rel_Servicios_PaqAdic_TMPBindingSource
        '
        Me.Consultar_Rel_Servicios_PaqAdic_TMPBindingSource.DataMember = "consultar_Rel_Servicios_PaqAdic_TMP"
        Me.Consultar_Rel_Servicios_PaqAdic_TMPBindingSource.DataSource = Me.DataSetTelefonia
        '
        'DESCRIPCIONTextBox1
        '
        Me.DESCRIPCIONTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Rel_Servicios_PaqAdic_TMPBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONTextBox1.Location = New System.Drawing.Point(629, 231)
        Me.DESCRIPCIONTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DESCRIPCIONTextBox1.Name = "DESCRIPCIONTextBox1"
        Me.DESCRIPCIONTextBox1.Size = New System.Drawing.Size(132, 22)
        Me.DESCRIPCIONTextBox1.TabIndex = 22
        '
        'CostoTextBox
        '
        Me.CostoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Rel_Servicios_PaqAdic_TMPBindingSource, "Costo", True))
        Me.CostoTextBox.Location = New System.Drawing.Point(609, 186)
        Me.CostoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CostoTextBox.Name = "CostoTextBox"
        Me.CostoTextBox.Size = New System.Drawing.Size(132, 22)
        Me.CostoTextBox.TabIndex = 21
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Rel_Servicios_PaqAdic_TMPBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(731, 186)
        Me.Clv_ServicioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clv_ServicioTextBox.TabIndex = 20
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(827, 73)
        Me.Button7.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(127, 28)
        Me.Button7.TabIndex = 4
        Me.Button7.Text = "ELIMINAR"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(692, 73)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(127, 28)
        Me.Button6.TabIndex = 3
        Me.Button6.Text = "AGREGAR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'TextBoxCosto_Det
        '
        Me.TextBoxCosto_Det.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxCosto_Det.Location = New System.Drawing.Point(507, 74)
        Me.TextBoxCosto_Det.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxCosto_Det.Name = "TextBoxCosto_Det"
        Me.TextBoxCosto_Det.Size = New System.Drawing.Size(152, 24)
        Me.TextBoxCosto_Det.TabIndex = 2
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.ServiciosBindingSource
        Me.ComboBox2.DisplayMember = "Descripcion"
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(52, 74)
        Me.ComboBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(445, 26)
        Me.ComboBox2.TabIndex = 1
        Me.ComboBox2.ValueMember = "Clv_Servicio"
        '
        'ServiciosBindingSource
        '
        Me.ServiciosBindingSource.DataMember = "Servicios"
        Me.ServiciosBindingSource.DataSource = Me.DataSetTelefonia
        '
        'ServiciosTableAdapter
        '
        Me.ServiciosTableAdapter.ClearBeforeFill = True
        '
        'Consultar_Rel_Servicios_PaqAdic_TMPTableAdapter
        '
        Me.Consultar_Rel_Servicios_PaqAdic_TMPTableAdapter.ClearBeforeFill = True
        '
        'Nuevo_Rel_Servicios_PaqAdic_TMPBindingSource
        '
        Me.Nuevo_Rel_Servicios_PaqAdic_TMPBindingSource.DataMember = "Nuevo_Rel_Servicios_PaqAdic_TMP"
        Me.Nuevo_Rel_Servicios_PaqAdic_TMPBindingSource.DataSource = Me.DataSetTelefonia
        '
        'Nuevo_Rel_Servicios_PaqAdic_TMPTableAdapter
        '
        Me.Nuevo_Rel_Servicios_PaqAdic_TMPTableAdapter.ClearBeforeFill = True
        '
        'Borrar_Rel_Servicios_PaqAdic_TMPBindingSource
        '
        Me.Borrar_Rel_Servicios_PaqAdic_TMPBindingSource.DataMember = "Borrar_Rel_Servicios_PaqAdic_TMP"
        Me.Borrar_Rel_Servicios_PaqAdic_TMPBindingSource.DataSource = Me.DataSetTelefonia
        '
        'Borrar_Rel_Servicios_PaqAdic_TMPTableAdapter
        '
        Me.Borrar_Rel_Servicios_PaqAdic_TMPTableAdapter.ClearBeforeFill = True
        '
        'DataTable1BindingSource
        '
        Me.DataTable1BindingSource.DataMember = "DataTable1"
        Me.DataTable1BindingSource.DataSource = Me.DataSetTelefonia
        '
        'DataTable1TableAdapter
        '
        Me.DataTable1TableAdapter.ClearBeforeFill = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.TreeView1)
        Me.Panel2.Controls.Add(Me.TreeView2)
        Me.Panel2.Controls.Add(Label1)
        Me.Panel2.Controls.Add(Me.Tipo_CobroTextBox)
        Me.Panel2.Controls.Add(Me.Button4)
        Me.Panel2.Controls.Add(DescripcionLabel1)
        Me.Panel2.Controls.Add(Me.Button3)
        Me.Panel2.Controls.Add(Label2)
        Me.Panel2.Controls.Add(Me.Button2)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Location = New System.Drawing.Point(15, 533)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(972, 241)
        Me.Panel2.TabIndex = 2
        '
        'Nuevo_Tipo_Paquete_AdicionalBindingSource
        '
        Me.Nuevo_Tipo_Paquete_AdicionalBindingSource.DataMember = "Nuevo_Tipo_Paquete_Adicional"
        Me.Nuevo_Tipo_Paquete_AdicionalBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Nuevo_Tipo_Paquete_AdicionalTableAdapter
        '
        Me.Nuevo_Tipo_Paquete_AdicionalTableAdapter.ClearBeforeFill = True
        '
        'DameClasificacionLlamadasBindingSource
        '
        Me.DameClasificacionLlamadasBindingSource.DataMember = "DameClasificacionLlamadas"
        Me.DameClasificacionLlamadasBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'DameClasificacionLlamadasTableAdapter
        '
        Me.DameClasificacionLlamadasTableAdapter.ClearBeforeFill = True
        '
        'FrmTipoPaqAdicionalesTel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1009, 836)
        Me.Controls.Add(Me.Consulta_tipo_paquete_AdicionalBindingNavigator)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.TextBoxClv_Session)
        Me.Controls.Add(Me.Clasificacion_LDTextBox)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(CMBLabel3)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Controls.Add(Me.Button5)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmTipoPaqAdicionalesTel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo Tarifas de Larga Distancia"
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_tipo_paquete_AdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_tipo_paquete_AdicionalBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.ResumeLayout(False)
        Me.Consulta_tipo_paquete_AdicionalBindingNavigator.PerformLayout()
        CType(Me.Muestra_CatalogoTipoCobrosTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        CType(Me.DameClasificacionLlamadasBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CatalogoClasificacionLlamnadasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetTelefonia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_PaisesRelTipoPaqBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Consultar_Rel_Servicios_PaqAdic_TMPDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consultar_Rel_Servicios_PaqAdic_TMPBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Nuevo_Rel_Servicios_PaqAdic_TMPBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borrar_Rel_Servicios_PaqAdic_TMPBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataTable1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.Nuevo_Tipo_Paquete_AdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClasificacionLlamadasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Consulta_tipo_paquete_AdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_tipo_paquete_AdicionalTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_tipo_paquete_AdicionalTableAdapter
    Friend WithEvents Consulta_tipo_paquete_AdicionalBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_Tipo_Paquete_AdiocionalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Tipo_CobroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_CatalogoTipoCobrosTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_CatalogoTipoCobrosTelTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_CatalogoTipoCobrosTelTableAdapter
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents Muestra_PaisesRelTipoPaqBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_PaisesRelTipoPaqTableAdapter As sofTV.DataSetLidia2TableAdapters.Muestra_PaisesRelTipoPaqTableAdapter
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DataSetTelefonia As sofTV.DataSetTelefonia
    Friend WithEvents CatalogoClasificacionLlamnadasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Catalogo_Clasificacion_LlamnadasTableAdapter As sofTV.DataSetTelefoniaTableAdapters.Catalogo_Clasificacion_LlamnadasTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TextBoxCosto_Det As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ServiciosTableAdapter As sofTV.DataSetTelefoniaTableAdapters.ServiciosTableAdapter
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Consultar_Rel_Servicios_PaqAdic_TMPBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consultar_Rel_Servicios_PaqAdic_TMPTableAdapter As sofTV.DataSetTelefoniaTableAdapters.consultar_Rel_Servicios_PaqAdic_TMPTableAdapter
    Friend WithEvents Consultar_Rel_Servicios_PaqAdic_TMPDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents TextBoxClv_Session As System.Windows.Forms.TextBox
    Friend WithEvents Nuevo_Rel_Servicios_PaqAdic_TMPBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Nuevo_Rel_Servicios_PaqAdic_TMPTableAdapter As sofTV.DataSetTelefoniaTableAdapters.Nuevo_Rel_Servicios_PaqAdic_TMPTableAdapter
    Friend WithEvents Borrar_Rel_Servicios_PaqAdic_TMPBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borrar_Rel_Servicios_PaqAdic_TMPTableAdapter As sofTV.DataSetTelefoniaTableAdapters.Borrar_Rel_Servicios_PaqAdic_TMPTableAdapter
    Friend WithEvents DataTable1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataTable1TableAdapter As sofTV.DataSetTelefoniaTableAdapters.DataTable1TableAdapter
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CostoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Nuevo_Tipo_Paquete_AdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Nuevo_Tipo_Paquete_AdicionalTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Nuevo_Tipo_Paquete_AdicionalTableAdapter
    Friend WithEvents Clasificacion_LDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DameClasificacionLlamadasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClasificacionLlamadasTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.DameClasificacionLlamadasTableAdapter
    Friend WithEvents DescripcionComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DameClasificacionLlamadasBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
