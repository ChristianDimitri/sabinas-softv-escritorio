﻿Imports System.Data.SqlClient
Public Class FrmToken

 


    Private Sub MuestraBeam(ByVal contrato As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, contrato)
        BaseII.CreateMyParameter("@beamPAquete", ParameterDirection.Output, SqlDbType.VarChar, 100)

        BaseII.ProcedimientoOutPut("MuestraBeamPorContrato")

        LabelBeam.Text = BaseII.dicoPar("@beamPAquete").ToString()

    End Sub

    Private Sub MuestraServiciosToken(ByVal contrato As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, contrato)
        cbServicio.DataSource = BaseII.ConsultaDT("MuestraServicioToken")
    End Sub

    Private Sub NuevoToken(ByVal Contrato As Integer, ByVal Clv_Servicio As Integer)
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
            BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
            BaseII.CreateMyParameter("@precio", SqlDbType.Money, TextBoxCosto.Text)
            BaseII.CreateMyParameter("@claveUsuario", SqlDbType.Int, GloClvUsuario)
            BaseII.Inserta("nuevoToken")

            MessageBox.Show("Se realizó con éxito.")
            bitsist(GloUsuario, Contrato, LocGloSistema, Me.Text, "", "", "Generó un el token: " + cbServicio.Text, LocClv_Ciudad)
            tbContrato.Text = ""
            tbContratoCompania.Text = ""            
            'dgvCablemodems.DataSource = Nothing
            MuestraServiciosToken(0)
            MuestraBeam(0)
            'cbServicio.DataSource = Nothing


        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub



    Private Sub FrmPruebaInternet_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        LabelBeam.Text = ""
        If eOpcion = "C" Then
            tbContratoCompania.Enabled = False
            cbServicio.Enabled = False
            bnAceptar.Enabled = False

            dameTokenPorId()
        Else
            TextBoxUsuario.Text = FrmMiMenu.LabelNombreUsuario.Text
        End If

    End Sub
    Dim loccon As Integer = 0
    Private Sub tbContrato_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbContrato.KeyDown, tbContratoCompania.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbContratoCompania.Text.Length = 0 Then Exit Sub
        'If IsNumeric(tbContratoCompania.Text) = False Then Exit Sub
        Try
            Dim array As String() = tbContratoCompania.Text.Trim.Split("-")
            loccon = array(0).Trim
            GloIdCompania = array(1).Trim
            Dim comando As New SqlCommand()
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            comando.Connection = conexion
            comando.CommandText = "select count(idcompania) from Rel_Usuario_Compania where clave=" + GloClvUsuario.ToString + " and idcompania=" + GloIdCompania.ToString
            Dim res As Integer = comando.ExecuteScalar()
            If res = 0 Then
                MsgBox("Contrato inválido. Inténtalo nuevamente")
                tbContrato.Text = ""
                tbContratoCompania.Text = ""

                'MuestraServiciosEric(2, 0, 7)
                Exit Sub
            End If
            conexion.Close()

        Catch ex As Exception
            MsgBox("Contrato inválido. Inténtalo nuevamente")
            Exit Sub
        End Try

        MuestraServiciosToken(tbContrato.Text)
        MuestraBeam(tbContrato.Text)
        If cbServicio.Items.Count = 0 Then
            MsgBox("No tiene Cobertura paquete asignado")
        End If
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click

        If cbServicio.Text.Length = 0 Then
            MessageBox.Show("Selecciona el token.")
            Exit Sub
        End If
        If cbServicio.SelectedValue = 0 Then
            MessageBox.Show("Selecciona el token.")
            Exit Sub
        End If

        If MsgBox("Desea continuar, ya que este proceso es irreversible y conlleva algún costo", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            NuevoToken(tbContrato.Text, cbServicio.SelectedValue)
        End If

    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub



    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        GloClv_TipSer = 9885 'cambiar
        FrmSelCliente.ShowDialog()
        If GLOCONTRATOSEL > 0 Then
            tbContratoCompania.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
            MuestraServiciosToken(tbContrato.Text)
            MuestraBeam(tbContrato.Text)
        End If
    End Sub

    Private Sub tbContratoCompania_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbContratoCompania.KeyPress

    End Sub

    Private Sub tbContratoCompania_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbContratoCompania.TextChanged
        Try
            Dim array As String() = tbContratoCompania.Text.Trim.Split("-")
            loccon = array(0).Trim
            GloIdCompania = array(1).Trim
            Dim comando2 As New SqlCommand()
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            comando2.Connection = conexion
            comando2.CommandText = "select contrato from rel_contratos_companias where contratocompania=" + loccon.ToString + " and idcompania=" + GloIdCompania.ToString
            tbContrato.Text = comando2.ExecuteScalar().ToString
            conexion.Close()
        Catch ex As Exception

        End Try
    End Sub



    Private Sub cbServicio_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbServicio.SelectedValueChanged
        If IsNumeric(cbServicio.SelectedValue) And eOpcion = "N" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, cbServicio.SelectedValue)
            BaseII.CreateMyParameter("@precio", ParameterDirection.Output, SqlDbType.VarChar, 30)

            BaseII.ProcedimientoOutPut("MuestraPrecioToken")

            TextBoxCosto.Text = BaseII.dicoPar("@precio").ToString()
        End If
    End Sub

    Private Sub dameTokenPorId()
        Dim dt As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idToken", SqlDbType.BigInt, gloIdToken)
        BaseII.CreateMyParameter("@contratoCompuesto", ParameterDirection.Output, SqlDbType.VarChar, 30)
        BaseII.CreateMyParameter("@DescripcionToken", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@precio", ParameterDirection.Output, SqlDbType.VarChar, 30)
        BaseII.CreateMyParameter("@factura", ParameterDirection.Output, SqlDbType.VarChar, 30)
        BaseII.CreateMyParameter("@fechaActivacion", ParameterDirection.Output, SqlDbType.VarChar, 10)
        BaseII.CreateMyParameter("@fechaDesactivación", ParameterDirection.Output, SqlDbType.VarChar, 10)
        BaseII.CreateMyParameter("@Usuario", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("DamenTokenPorId")

        TextBoxId.Text = gloIdToken
        tbContratoCompania.Text = BaseII.dicoPar("@contratoCompuesto").ToString()
        cbServicio.Text = BaseII.dicoPar("@DescripcionToken").ToString()
        TextBoxCosto.Text = BaseII.dicoPar("@precio").ToString()
        TextBox2.Text = BaseII.dicoPar("@factura").ToString()
        TextBoxFechaActivacion.Text = BaseII.dicoPar("@fechaActivacion").ToString()
        TextBoxFechaDesactivacion.Text = BaseII.dicoPar("@fechaDesactivación").ToString()
        TextBoxUsuario.Text = BaseII.dicoPar("@Usuario").ToString()


    End Sub

End Class