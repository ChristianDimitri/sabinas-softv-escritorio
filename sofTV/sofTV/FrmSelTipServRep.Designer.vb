<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelTipServRep
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.CMBLABEL = New System.Windows.Forms.Label
        Me.CMB2Label1 = New System.Windows.Forms.Label
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.MuestraSelecciontipservppalConsultaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.MuestraSelecciontipservppalTmpConsultaBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciontipservppalTmpConsultaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_Seleccion_tipservppalTmpConsultaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_tipservppalTmpConsultaTableAdapter
        Me.Muestra_Seleccion_tipservppalConsultaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_tipservppalConsultaTableAdapter
        Me.InsertaTodos_Seleccion_clientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTodos_Seleccion_clientesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTodos_Seleccion_clientesTableAdapter
        Me.InsertaTodos_Seleccion_clientestmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTodos_Seleccion_clientestmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTodos_Seleccion_clientestmpTableAdapter
        Me.Insertauno_Seleccion_clientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_clientesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_clientesTableAdapter
        Me.Insertauno_Seleccion_clientestmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_clientestmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_clientestmpTableAdapter
        Me.Muestra_Seleccion_tipservppalTmpNuevoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_Seleccion_tipservppalTmpNuevoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_tipservppalTmpNuevoTableAdapter
        CType(Me.MuestraSelecciontipservppalConsultaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSelecciontipservppalTmpConsultaBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSelecciontipservppalTmpConsultaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTodos_Seleccion_clientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTodos_Seleccion_clientestmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_clientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_clientestmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Seleccion_tipservppalTmpNuevoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLABEL
        '
        Me.CMBLABEL.AutoSize = True
        Me.CMBLABEL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLABEL.ForeColor = System.Drawing.Color.Brown
        Me.CMBLABEL.Location = New System.Drawing.Point(363, 9)
        Me.CMBLABEL.Name = "CMBLABEL"
        Me.CMBLABEL.Size = New System.Drawing.Size(185, 16)
        Me.CMBLABEL.TabIndex = 41
        Me.CMBLABEL.Text = "Servicios Seleccionados:"
        '
        'CMB2Label1
        '
        Me.CMB2Label1.AutoSize = True
        Me.CMB2Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMB2Label1.ForeColor = System.Drawing.Color.Brown
        Me.CMB2Label1.Location = New System.Drawing.Point(9, 9)
        Me.CMB2Label1.Name = "CMB2Label1"
        Me.CMB2Label1.Size = New System.Drawing.Size(200, 16)
        Me.CMB2Label1.TabIndex = 40
        Me.CMB2Label1.Text = "Servicios para Seleccionar:"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(439, 272)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 37
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(276, 272)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 36
        Me.Button6.Text = "&Aceptar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(259, 159)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 35
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(259, 130)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 34
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(259, 62)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 33
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(259, 33)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 32
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.DataSource = Me.MuestraSelecciontipservppalConsultaBindingSource
        Me.ListBox2.DisplayMember = "Concepto"
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(366, 33)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(209, 225)
        Me.ListBox2.TabIndex = 39
        Me.ListBox2.TabStop = False
        Me.ListBox2.ValueMember = "clv_tipser"
        '
        'MuestraSelecciontipservppalConsultaBindingSource
        '
        Me.MuestraSelecciontipservppalConsultaBindingSource.DataMember = "Muestra_Seleccion_tipservppalConsulta"
        Me.MuestraSelecciontipservppalConsultaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListBox1
        '
        Me.ListBox1.Cursor = System.Windows.Forms.Cursors.Default
        Me.ListBox1.DataSource = Me.MuestraSelecciontipservppalTmpConsultaBindingSource1
        Me.ListBox1.DisplayMember = "Concepto"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(12, 33)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(202, 225)
        Me.ListBox1.TabIndex = 38
        Me.ListBox1.TabStop = False
        Me.ListBox1.ValueMember = "clv_tipser"
        '
        'MuestraSelecciontipservppalTmpConsultaBindingSource1
        '
        Me.MuestraSelecciontipservppalTmpConsultaBindingSource1.DataMember = "Muestra_Seleccion_tipservppalTmpConsulta"
        Me.MuestraSelecciontipservppalTmpConsultaBindingSource1.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraSelecciontipservppalTmpConsultaBindingSource
        '
        Me.MuestraSelecciontipservppalTmpConsultaBindingSource.DataMember = "Muestra_Seleccion_tipservppalTmpConsulta"
        Me.MuestraSelecciontipservppalTmpConsultaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Muestra_Seleccion_tipservppalTmpConsultaTableAdapter
        '
        Me.Muestra_Seleccion_tipservppalTmpConsultaTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Seleccion_tipservppalConsultaTableAdapter
        '
        Me.Muestra_Seleccion_tipservppalConsultaTableAdapter.ClearBeforeFill = True
        '
        'InsertaTodos_Seleccion_clientesBindingSource
        '
        Me.InsertaTodos_Seleccion_clientesBindingSource.DataMember = "InsertaTodos_Seleccion_clientes"
        Me.InsertaTodos_Seleccion_clientesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTodos_Seleccion_clientesTableAdapter
        '
        Me.InsertaTodos_Seleccion_clientesTableAdapter.ClearBeforeFill = True
        '
        'InsertaTodos_Seleccion_clientestmpBindingSource
        '
        Me.InsertaTodos_Seleccion_clientestmpBindingSource.DataMember = "InsertaTodos_Seleccion_clientestmp"
        Me.InsertaTodos_Seleccion_clientestmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTodos_Seleccion_clientestmpTableAdapter
        '
        Me.InsertaTodos_Seleccion_clientestmpTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_clientesBindingSource
        '
        Me.Insertauno_Seleccion_clientesBindingSource.DataMember = "Insertauno_Seleccion_clientes"
        Me.Insertauno_Seleccion_clientesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_clientesTableAdapter
        '
        Me.Insertauno_Seleccion_clientesTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_clientestmpBindingSource
        '
        Me.Insertauno_Seleccion_clientestmpBindingSource.DataMember = "Insertauno_Seleccion_clientestmp"
        Me.Insertauno_Seleccion_clientestmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_clientestmpTableAdapter
        '
        Me.Insertauno_Seleccion_clientestmpTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Seleccion_tipservppalTmpNuevoBindingSource
        '
        Me.Muestra_Seleccion_tipservppalTmpNuevoBindingSource.DataMember = "Muestra_Seleccion_tipservppalTmpNuevo"
        Me.Muestra_Seleccion_tipservppalTmpNuevoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Muestra_Seleccion_tipservppalTmpNuevoTableAdapter
        '
        Me.Muestra_Seleccion_tipservppalTmpNuevoTableAdapter.ClearBeforeFill = True
        '
        'FrmSelTipServRep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(598, 326)
        Me.Controls.Add(Me.CMBLABEL)
        Me.Controls.Add(Me.CMB2Label1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "FrmSelTipServRep"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccion de Tipos de Servicios"
        CType(Me.MuestraSelecciontipservppalConsultaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSelecciontipservppalTmpConsultaBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSelecciontipservppalTmpConsultaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTodos_Seleccion_clientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTodos_Seleccion_clientestmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_clientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_clientestmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Seleccion_tipservppalTmpNuevoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLABEL As System.Windows.Forms.Label
    Friend WithEvents CMB2Label1 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents MuestraSelecciontipservppalConsultaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents MuestraSelecciontipservppalTmpConsultaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Seleccion_tipservppalTmpConsultaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_tipservppalTmpConsultaTableAdapter
    Friend WithEvents Muestra_Seleccion_tipservppalConsultaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_tipservppalConsultaTableAdapter
    Friend WithEvents InsertaTodos_Seleccion_clientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTodos_Seleccion_clientesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTodos_Seleccion_clientesTableAdapter
    Friend WithEvents InsertaTodos_Seleccion_clientestmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTodos_Seleccion_clientestmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTodos_Seleccion_clientestmpTableAdapter
    Friend WithEvents Insertauno_Seleccion_clientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_clientesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_clientesTableAdapter
    Friend WithEvents Insertauno_Seleccion_clientestmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_clientestmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_clientestmpTableAdapter
    Friend WithEvents MuestraSelecciontipservppalTmpConsultaBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Seleccion_tipservppalTmpNuevoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Seleccion_tipservppalTmpNuevoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_tipservppalTmpNuevoTableAdapter
End Class
