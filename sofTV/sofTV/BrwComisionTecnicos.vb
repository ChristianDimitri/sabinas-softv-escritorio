﻿Public Class BrwComisionTecnicos

    Private Sub BrwComisionTecnicos_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        MuestraRangosComisionTecnicos()
    End Sub

    Private Sub BrwComisionTecnicos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        Usp_MuestraOrdenQueja()
        MuestraRangosComisionTecnicos()

        If Me.DataGridView1.RowCount > 0 Then
            idRanTec = Me.DataGridView1.SelectedCells(0).Value.ToString
        Else
            idRanTec = 0
        End If
    End Sub

    Private Sub MuestraRangosComisionTecnicos()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@TipoOQ", SqlDbType.VarChar, ComboBox2.SelectedValue, 50)
        DataGridView1.DataSource = BaseII.ConsultaDT("Usp_MuestraRangosComisionTecnicos")

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        opRanTec = "N"
        FrmComisionTecnicos.Show()
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If idRanTec > 0 Then
            opRanTec = "C"
            FrmComisionTecnicos.Show()
        Else
            MsgBox("Seleccione alguna comisión", MsgBoxStyle.Information)
        End If
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If idRanTec > 0 Then
            opRanTec = "M"
            FrmComisionTecnicos.Show()
        Else
            MsgBox("Seleccione alguna comisión", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If Me.DataGridView1.RowCount > 0 Then
            idRanTec = Me.DataGridView1.SelectedCells(0).Value.ToString
        Else
            idRanTec = 0
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, idRanTec)
        BaseII.Inserta("Usp_BorRangosComisionTecnicos")

        MsgBox("Borrado con exito")

        MuestraRangosComisionTecnicos()

    End Sub

    Private Sub Usp_MuestraOrdenQueja()
        Try
            BaseII.limpiaParametros()
            ComboBox2.DataSource = BaseII.ConsultaDT("Usp_MuestraOrdenQueja")
            ComboBox2.DisplayMember = "Tipo"
            ComboBox2.ValueMember = "Idtipo"

            If ComboBox2.Items.Count > 0 Then
                ComboBox2.SelectedIndex = 0
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged
        Try
            MuestraRangosComisionTecnicos()

            If Me.DataGridView1.RowCount > 0 Then
                idRanTec = Me.DataGridView1.SelectedCells(0).Value.ToString
            Else
                idRanTec = 0
            End If

        Catch ex As Exception

        End Try
    End Sub

End Class