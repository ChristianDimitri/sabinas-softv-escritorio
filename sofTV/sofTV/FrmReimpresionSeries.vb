﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports sofTV.Base

Public Class FrmReimpresionSeries

    Dim serie As String
    Dim inicio As Integer
    Dim fin As Integer
    Dim existentes As Integer
    Dim minimo As Integer

    Dim consulta As New CBase
    Dim TipoSerie As Integer

    Dim FoliosTv As Integer = 0
    Dim FoliosInt As Integer = 0
    Dim FoliosRecu As Integer = 0

    Private Sub LlenaVendedor()

        BaseII.limpiaParametros()
        Dim dt As DataTable = BaseII.ConsultaDT("SP_Vendedores")

        If dt.Rows.Count > 0 Then
            ComboBox1.DataSource = dt
            ComboBox1.DisplayMember = "Nombre"
            ComboBox1.ValueMember = "Clv_Vendedor"
        End If

    End Sub

    Private Sub LlenaCombo()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, ComboBox1.SelectedValue)
        Dim dt As DataTable = BaseII.ConsultaDT("SP_SerieFolio")


        If dt.Rows.Count > 0 Then
            Cmb_serie.DataSource = dt
            Cmb_serie.DisplayMember = "Serie"
            Cmb_serie.ValueMember = "Clave"
        End If

    End Sub

    Private Sub ReimpresionFolios(ByVal serie As String, ByVal folio_inicio As Integer, ByVal folio_fin As Integer)
        Try
            'Using conexion As New SqlConnection(MiConexion)
            '    conexion.Open()
            '    Dim cmm As New SqlCommand()
            '    cmm.Connection = conexion
            '    cmm.CommandText = "SP_ReimpresionFolios"
            '    cmm.CommandType = CommandType.StoredProcedure

            '    cmm.Parameters.Add("@serie", SqlDbType.NVarChar, 150)
            '    cmm.Parameters.Add("@folio_inicio", SqlDbType.Int)
            '    cmm.Parameters.Add("@folio_fin", SqlDbType.Int)

            '    cmm.Parameters("@serie").Value = serie
            '    cmm.Parameters("@folio_inicio").Value = folio_inicio
            '    cmm.Parameters("@folio_fin").Value = folio_fin

            '    cmm.ExecuteNonQuery()

            'End Using

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@serie", SqlDbType.NVarChar, serie, 150)
            BaseII.CreateMyParameter("@folio_inicio", SqlDbType.Int, folio_inicio)
            BaseII.CreateMyParameter("@folio_fin", SqlDbType.Int, folio_fin)
            BaseII.CreateMyParameter("@foliostv", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@foliosint", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("SP_ReimpresionFolios")
            FoliosTv = BaseII.dicoPar("@foliostv")
            FoliosInt = BaseII.dicoPar("@foliosint")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub ReimpresionFoliosRecu(ByVal serie As String, ByVal folio_inicio As Integer, ByVal folio_fin As Integer)
        Try
            'Using conexion As New SqlConnection(MiConexion)
            '    conexion.Open()
            '    Dim cmm As New SqlCommand()
            '    cmm.Connection = conexion
            '    cmm.CommandText = "SP_ReimpresionFolios_Recupera"
            '    cmm.CommandType = CommandType.StoredProcedure

            '    cmm.Parameters.Add("@serie", SqlDbType.NVarChar, 150)
            '    cmm.Parameters.Add("@folio_inicio", SqlDbType.Int)
            '    cmm.Parameters.Add("@folio_fin", SqlDbType.Int)

            '    cmm.Parameters("@serie").Value = serie
            '    cmm.Parameters("@folio_inicio").Value = folio_inicio
            '    cmm.Parameters("@folio_fin").Value = folio_fin

            '    cmm.ExecuteNonQuery()
            'End Using

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@serie", SqlDbType.NVarChar, serie, 150)
            BaseII.CreateMyParameter("@folio_inicio", SqlDbType.Int, folio_inicio)
            BaseII.CreateMyParameter("@folio_fin", SqlDbType.Int, folio_fin)
            BaseII.CreateMyParameter("@foliosRecu", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("SP_ReimpresionFolios_Recupera")
            FoliosRecu = BaseII.dicoPar("@foliosRecu")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Function FoliosExistentes(ByVal vserie As String) As Integer

        Dim parametro(1) As SqlParameter

        parametro(0) = New SqlParameter("@serie", SqlDbType.NVarChar, 150)
        parametro(0).Direction = ParameterDirection.Input
        parametro(0).Value = vserie

        Dim registro As DataTable = consulta.consultarDT("SP_ReimpresionFoliosExistentes", parametro)

        If registro.Rows.Count > 0 Then
            Return Convert.ToInt32(registro.Rows(0)("Existentes").ToString())
        Else
            MsgBox("No hay folios existentes", MsgBoxStyle.Information)
            Return 0
        End If

    End Function

    Private Sub FrmReimpresionSeries_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LlenaVendedor()
        'LlenaCombo()
    End Sub

    Private Sub Btn_ReImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_ReImprimir.Click

        serie = Cmb_serie.GetItemText(Cmb_serie.SelectedItem).ToString()

        If (Cmb_serie.SelectedValue = 0 Or Not IsNumeric(Txt_inicio.Text.ToString()) Or Not IsNumeric(Txt_fin.Text.ToString())) Then
            MsgBox("Inserta datos enteros y selecciona una serie", MsgBoxStyle.Information)
        Else

            inicio = Convert.ToInt32(Txt_inicio.Text.ToString())
            fin = Convert.ToInt32(Txt_fin.Text.ToString())
            existentes = FoliosExistentes(serie)
            minimo = FoliosMinimo(serie)
            If inicio < minimo Or inicio > fin Then
                MsgBox("El folio de inicio debe ser mayor a " & minimo.ToString() & " y menor o igual al folio final", MsgBoxStyle.Information)
            Else
                If fin < inicio Or fin > existentes Then
                    MsgBox("El folio final debe ser mayor o igual al folio de inicio y No mayor a " & existentes.ToString(), MsgBoxStyle.Information)
                Else

                    ReimpresionFolios(serie, inicio, fin)
                    ReimpresionFoliosRecu(serie, inicio, fin)

                    'If TipoSerie = 1 Then
                    '    LocGloOpRep = 21
                    'Else
                    GloSeries = serie
                    If FoliosTv > 0 Then
                        LocGloOpRep = 23
                        FrmImprimirFac.ShowDialog()
                    End If
                    If FoliosInt > 0 Then
                        LocGloOpRep = 29
                        FrmImprimirFac.ShowDialog()
                    End If
                    If FoliosRecu > 0 Then
                        LocGloOpRep = 26
                        FrmImprimirFac.ShowDialog()
                    End If
                    'End If

                    'GloSeries = serie

                    'FrmImprimirFac.Show()

                    'ReimpresionFolios(serie, inicio, fin)
                    'REPORTEPreContrato(1, serie)
                    ''LocGloOpRep = 21
                    ''GloSeries = serie

                    ''FrmImprimirFac.Show()
                End If
            End If

        End If

        LocGloOpRep = 0
        Me.Close()
    End Sub

    Private Sub Btn_Salir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
        Me.Close()
    End Sub

    Private Function FoliosMinimo(ByVal vserie As String) As Integer

        Dim parametro(1) As SqlParameter

        parametro(0) = New SqlParameter("@serie", SqlDbType.NVarChar, 150)
        parametro(0).Direction = ParameterDirection.Input
        parametro(0).Value = vserie

        Dim registro As DataTable = consulta.consultarDT("SP_ReimpresionFoliosExistentesMin", parametro)

        If registro.Rows.Count > 0 Then
            Return Convert.ToInt32(registro.Rows(0)("Minimo").ToString())
        Else
            MsgBox("No hay folios existentes", MsgBoxStyle.Information)
            Return 0
        End If

    End Function

    Private Sub REPORTEPreContrato(ByVal OP As Integer, ByVal SERIE As String)
        Dim dTable As New DataTable
        Dim rDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, SERIE, 50)
        dTable = BaseII.ConsultaDT("REPORTEPreContrato")

        rDocument = New CrystalDecisions.CrystalReports.Engine.ReportDocument
        rDocument.Load(RutaReportes + "\REPORTEPreContrato.rpt")
        rDocument.SetDataSource(dTable)

        If dTable.Rows.Count = 0 Then Exit Sub

        MessageBox.Show("Se reimprimirá la parte frontal de " + dTable.Rows.Count.ToString + " precontratos.", "Atención", MessageBoxButtons.OK)

        rDocument.PrintOptions.PrinterName = IMPRESORA_CONTRATOS
        rDocument.PrintToPrinter(1, False, 0, 0)

        MessageBox.Show("Se reimprimirá la parte posterior de " + dTable.Rows.Count.ToString + " precontratos.", "Atención", MessageBoxButtons.OK)

        rDocument = New CrystalDecisions.CrystalReports.Engine.ReportDocument
        rDocument.Load(RutaReportes + "\REPORTEPreContratoPosterior.rpt")
        rDocument.PrintOptions.PrinterName = IMPRESORA_CONTRATOS

        For Each e As DataRow In dTable.Rows
            rDocument.PrintToPrinter(1, False, 0, 0)
        Next


    End Sub

    Private Sub Cmb_serie_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Cmb_serie.SelectedIndexChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clave", SqlDbType.Int, Cmb_serie.SelectedValue)
            BaseII.CreateMyParameter("@tipo", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("DameTipoSerie")
            TipoSerie = BaseII.dicoPar("@tipo")

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        LlenaCombo()
    End Sub
End Class