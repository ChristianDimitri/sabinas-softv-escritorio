﻿Imports System.Data.SqlClient
Public Class BrwProspectos

    Private Sub BrwProspectos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaCompanias()
        llenaColonias()
        busca()
    End Sub

    Private Sub llenaCompanias()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompania.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompania.DisplayMember = "razon_social"
            ComboBoxCompania.ValueMember = "id_compania"
            If ComboBoxCompania.Items.Count > 0 Then
                ComboBoxCompania.SelectedIndex = 0
            End If
            GloIdCompania = ComboBoxCompania.SelectedValue
        Catch ex As Exception

        End Try
    End Sub
    Private Sub llenaColonias()
        Dim colonias As New BaseIII
        Try
            colonias.limpiaParametros()
            colonias.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompania.SelectedValue)
            comboColonias.DataSource = colonias.ConsultaDT("uspConsultaColonias")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub busca()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@apellido1", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@apellido2", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompania.SelectedValue)
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@numero", SqlDbType.Int, 0)
            DataGridView1.DataSource = BaseII.ConsultaDT("BuscaProspectos")
        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        If Not IsNumeric(tbNumero.Text) And Len(tbNumero.Text) > 0 Then
            MsgBox("El número debe ser un valor numérico.")
            Exit Sub
        End If
        If Not IsNumeric(tbProspecto.Text) And Len(tbProspecto.Text) > 0 Then
            MsgBox("El Número de Prospecto debe ser un valor numérico.")
            Exit Sub
        End If
        Dim nombre, apellido1, apellido2, calle, clv_colonia, numero As String
        If tbNombre.Text = "" Then
            nombre = ""
        Else
            nombre = tbNombre.Text
        End If
        If tbApellidoM.Text = "" Then
            apellido2 = ""
        Else
            apellido2 = tbApellidoM.Text
        End If
        If tbApellidoP.Text = "" Then
            apellido1 = ""
        Else
            apellido1 = tbApellidoP.Text
        End If
        If tbCalle.Text = "" Then
            calle = ""
        Else
            calle = tbCalle.Text
        End If
        If tbNumero.Text = "" Then
            numero = 0
        Else
            numero = tbNumero.Text
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, nombre)
            BaseII.CreateMyParameter("@apellido1", SqlDbType.VarChar, apellido1)
            BaseII.CreateMyParameter("@apellido2", SqlDbType.VarChar, apellido2)
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, calle)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompania.SelectedValue)
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, comboColonias.SelectedValue)
            BaseII.CreateMyParameter("@numero", SqlDbType.Int, numero)
            DataGridView1.DataSource = BaseII.ConsultaDT("BuscaProspectos")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmProspectos.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        opcion = "C"
        ClvProspecto = DataGridView1.SelectedCells(0).Value
        FrmProspectos.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        opcion = "M"
        ClvProspecto = DataGridView1.SelectedCells(0).Value
        FrmProspectos.Show()
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            tbnombreM.Text = DataGridView1.SelectedCells(1).Value.ToString + " " + DataGridView1.SelectedCells(2).Value.ToString + " " + DataGridView1.SelectedCells(3).Value.ToString
            tbDireccion.Text = DataGridView1.SelectedCells(8).Value.ToString + " #" + DataGridView1.SelectedCells(9).Value.ToString + ", " + DataGridView1.SelectedCells(7).Value.ToString + " Loc. " + DataGridView1.SelectedCells(6).Value.ToString + ". " + DataGridView1.SelectedCells(5).Value.ToString + ", " + DataGridView1.SelectedCells(4).Value.ToString
            tbTelefono.Text = DataGridView1.SelectedCells(10).Value.ToString
            tbMail.Text = DataGridView1.SelectedCells(12).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Public bndActivate As Integer = 0
    Private Sub BrwProspectos_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If bndActivate = 1 Then
            busca()
            bndActivate = 0
        End If
    End Sub

    Private Sub ComboBoxCompania_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompania.SelectedIndexChanged
        Try
            busca()
        Catch ex As Exception

        End Try
    End Sub
End Class