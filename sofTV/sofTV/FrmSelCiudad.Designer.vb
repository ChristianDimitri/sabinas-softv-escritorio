<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelCiudad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.MuestraSeleccionCiudadCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidiaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraciudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Muestra_ciudadTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_ciudadTableAdapter()
        Me.Inserta_Sel_ciudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Sel_ciudadTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.inserta_Sel_ciudadTableAdapter()
        Me.MuestraSeleccionaCiudadTmpNUEVOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter = New sofTV.DataSetLidiaTableAdapters.MuestraSelecciona_CiudadTmpNUEVOTableAdapter()
        Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter = New sofTV.DataSetLidiaTableAdapters.MuestraSelecciona_CiudadTmpCONSULTATableAdapter()
        Me.MuestraSeleccion_CiudadCONSULTATableAdapter = New sofTV.DataSetLidiaTableAdapters.MuestraSeleccion_CiudadCONSULTATableAdapter()
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter = New sofTV.DataSetLidiaTableAdapters.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter()
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter = New sofTV.DataSetLidiaTableAdapters.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter()
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter = New sofTV.DataSetLidiaTableAdapters.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter()
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter = New sofTV.DataSetLidiaTableAdapters.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter()
        Me.Borra_Seleccion_CiudadTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Seleccion_CiudadTmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Borra_Seleccion_CiudadTmpTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.MuestraSeleccionCiudadCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidiaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraciudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Sel_ciudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionaCiudadTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Seleccion_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel2.Location = New System.Drawing.Point(509, 11)
        Me.CMBLabel2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(222, 18)
        Me.CMBLabel2.TabIndex = 64
        Me.CMBLabel2.Text = "Ciudad(es) Seleccionada(s):"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(16, 11)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(219, 18)
        Me.CMBLabel1.TabIndex = 63
        Me.CMBLabel1.Text = "Seleccione la(s) ciudad(es):"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(628, 329)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 59
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(389, 329)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 44)
        Me.Button6.TabIndex = 58
        Me.Button6.Text = "&Aceptar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(368, 190)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(100, 28)
        Me.Button4.TabIndex = 57
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(368, 154)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(100, 28)
        Me.Button3.TabIndex = 56
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(368, 70)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(100, 28)
        Me.Button2.TabIndex = 55
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(368, 34)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 28)
        Me.Button1.TabIndex = 54
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.DataSource = Me.MuestraSeleccionCiudadCONSULTABindingSource
        Me.ListBox2.DisplayMember = "NOMBRE"
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 16
        Me.ListBox2.Location = New System.Drawing.Point(476, 33)
        Me.ListBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(340, 276)
        Me.ListBox2.TabIndex = 61
        Me.ListBox2.TabStop = False
        Me.ListBox2.ValueMember = "clv_ciudad"
        '
        'MuestraSeleccionCiudadCONSULTABindingSource
        '
        Me.MuestraSeleccionCiudadCONSULTABindingSource.DataMember = "MuestraSeleccion_CiudadCONSULTA"
        Me.MuestraSeleccionCiudadCONSULTABindingSource.DataSource = Me.DataSetLidiaBindingSource
        '
        'DataSetLidiaBindingSource
        '
        Me.DataSetLidiaBindingSource.DataSource = Me.DataSetLidia
        Me.DataSetLidiaBindingSource.Position = 0
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource
        Me.ListBox1.DisplayMember = "nombre"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 16
        Me.ListBox1.Location = New System.Drawing.Point(15, 34)
        Me.ListBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(344, 276)
        Me.ListBox1.TabIndex = 60
        Me.ListBox1.TabStop = False
        Me.ListBox1.ValueMember = "clv_ciudad"
        '
        'MuestraSeleccionaCiudadTmpCONSULTABindingSource
        '
        Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource.DataMember = "MuestraSelecciona_CiudadTmpCONSULTA"
        Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource.DataSource = Me.DataSetLidia
        '
        'MuestraciudadBindingSource
        '
        Me.MuestraciudadBindingSource.DataMember = "Muestra_ciudad"
        Me.MuestraciudadBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Muestra_ciudadTableAdapter
        '
        Me.Muestra_ciudadTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Sel_ciudadBindingSource
        '
        Me.Inserta_Sel_ciudadBindingSource.DataMember = "inserta_Sel_ciudad"
        Me.Inserta_Sel_ciudadBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Sel_ciudadTableAdapter
        '
        Me.Inserta_Sel_ciudadTableAdapter.ClearBeforeFill = True
        '
        'MuestraSeleccionaCiudadTmpNUEVOBindingSource
        '
        Me.MuestraSeleccionaCiudadTmpNUEVOBindingSource.DataMember = "MuestraSelecciona_CiudadTmpNUEVO"
        Me.MuestraSeleccionaCiudadTmpNUEVOBindingSource.DataSource = Me.DataSetLidiaBindingSource
        '
        'MuestraSelecciona_CiudadTmpNUEVOTableAdapter
        '
        Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.ClearBeforeFill = True
        '
        'MuestraSelecciona_CiudadTmpCONSULTATableAdapter
        '
        Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.ClearBeforeFill = True
        '
        'MuestraSeleccion_CiudadCONSULTATableAdapter
        '
        Me.MuestraSeleccion_CiudadCONSULTATableAdapter.ClearBeforeFill = True
        '
        'PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource
        '
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataMember = "PONUNOSeleccion_Ciudad_Selecciona_CiudadTmp"
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataSource = Me.DataSetLidia
        '
        'PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
        '
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.ClearBeforeFill = True
        '
        'PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource
        '
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataMember = "PONUNOSelecciona_CiudadTmp_Seleccion_Ciudad"
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataSource = Me.DataSetLidia
        '
        'PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
        '
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.ClearBeforeFill = True
        '
        'PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource
        '
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataMember = "PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmp"
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataSource = Me.DataSetLidia
        '
        'PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
        '
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.ClearBeforeFill = True
        '
        'PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource
        '
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataMember = "PONTODOSSelecciona_CiudadTmp_Seleccion_Ciudad"
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataSource = Me.DataSetLidia
        '
        'PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
        '
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.ClearBeforeFill = True
        '
        'Borra_Seleccion_CiudadTmpBindingSource
        '
        Me.Borra_Seleccion_CiudadTmpBindingSource.DataMember = "Borra_Seleccion_CiudadTmp"
        Me.Borra_Seleccion_CiudadTmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Borra_Seleccion_CiudadTmpTableAdapter
        '
        Me.Borra_Seleccion_CiudadTmpTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmSelCiudad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(836, 396)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelCiudad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección de Ciudad."
        CType(Me.MuestraSeleccionCiudadCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidiaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraciudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Sel_ciudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionaCiudadTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Seleccion_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents MuestraciudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Muestra_ciudadTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_ciudadTableAdapter
    Friend WithEvents Inserta_Sel_ciudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Sel_ciudadTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.inserta_Sel_ciudadTableAdapter
    Friend WithEvents MuestraSeleccionaCiudadTmpNUEVOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetLidiaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents MuestraSelecciona_CiudadTmpNUEVOTableAdapter As sofTV.DataSetLidiaTableAdapters.MuestraSelecciona_CiudadTmpNUEVOTableAdapter
    Friend WithEvents MuestraSeleccionaCiudadTmpCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_CiudadTmpCONSULTATableAdapter As sofTV.DataSetLidiaTableAdapters.MuestraSelecciona_CiudadTmpCONSULTATableAdapter
    Friend WithEvents MuestraSeleccionCiudadCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSeleccion_CiudadCONSULTATableAdapter As sofTV.DataSetLidiaTableAdapters.MuestraSeleccion_CiudadCONSULTATableAdapter
    Friend WithEvents PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter As sofTV.DataSetLidiaTableAdapters.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
    Friend WithEvents PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter As sofTV.DataSetLidiaTableAdapters.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
    Friend WithEvents PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter As sofTV.DataSetLidiaTableAdapters.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
    Friend WithEvents PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter As sofTV.DataSetLidiaTableAdapters.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
    Friend WithEvents Borra_Seleccion_CiudadTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Seleccion_CiudadTmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Borra_Seleccion_CiudadTmpTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
