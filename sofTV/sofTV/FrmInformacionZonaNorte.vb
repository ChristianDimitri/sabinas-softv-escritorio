﻿Public Class FrmInformacionZonaNorte
    Public opcion As String = ""
    Public contrato As Integer = 0
    Private Sub FrmInformacionZonaNorte_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "Consulta" Then
            btnAceptar.Enabled = False
            btnCancelar.Enabled = True
            tbPassword.Enabled = False
            tbSiteid.Enabled = False
            'ElseIf opcion = "Modificar" Then

        End If
        dameDatos()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If tbSiteid.Text.Length > 0 And tbPassword.Text.Length > 0 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, contrato)
            BaseII.CreateMyParameter("@siteid", SqlDbType.VarChar, tbSiteid.Text)
            BaseII.CreateMyParameter("@password", SqlDbType.VarChar, tbPassword.Text)
            BaseII.Inserta("GuardaInformacionZonaNorte")
            bitsist(GloUsuario, contrato, LocGloSistema, "Información Zona Norte (Internet)", "", "Guardo la información de Internet para Zona Norte", GloSucursal, LocClv_Ciudad)
            Me.Close()
        Else
            MessageBox.Show("Debe capturar todos los datos.")
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub dameDatos()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, contrato)
            BaseII.CreateMyParameter("@siteid", ParameterDirection.Output, SqlDbType.VarChar, 30)
            BaseII.CreateMyParameter("@password", ParameterDirection.Output, SqlDbType.VarChar, 30)
            BaseII.CreateMyParameter("@existe", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("ObtieneInformacionZonaNorta")
            If BaseII.dicoPar("@existe") = 1 Then
                tbSiteid.Text = BaseII.dicoPar("@siteid")
                tbPassword.Text = BaseII.dicoPar("@password")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub FrmInformacionZonaNorte_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        tbPassword.Text = ""
        tbSiteid.Text = ""
    End Sub
End Class