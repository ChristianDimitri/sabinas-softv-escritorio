Imports System.Data.SqlClient
Public Class FrmCambioClienteSoloInternet
    Dim dt As DataTable
    Dim listacompanias As ArrayList
    Private Sub Llena_companias()
        Try
            Dim connection As New SqlConnection(MiConexion)
            connection.Open()
            Dim comando As New SqlCommand()
            comando.Connection = connection
            comando.CommandText = "exec Muestra_Compania_RelUsuario " + GloClvUsuario.ToString
            Dim reader As SqlDataReader = comando.ExecuteReader()
            While reader.Read()
                listacompanias.Add(reader(0).ToString)
            End While
        Catch ex As Exception

        End Try
    End Sub
    Private Sub FrmCambioClienteSoloInternet_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If LocbndProceso = True Then
            LocbndProceso = False
            Me.TextBox1.Text = GLOCONTRATOSEL
            Me.TextBox2.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
        End If
        If LocbndProceso1 = True Then
            LocbndProceso1 = False
            Me.TextBox1.Text = 0
        End If
    End Sub

    Private Sub FrmCambioClienteSoloInternet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Llena_companias()
        Me.TextBox1.Clear()
        'If LocProceso = 1 Then
        '    GloClv_TipSer = 1000
        '    Me.Text = "Proceso De Cambio de Cliente Normal A Internet"
        '    Me.CMBLabel1.Text = "Proceso De Cambio de Cliente Normal A Internet"
        'ElseIf LocProceso = 0 Then
        '    GloClv_TipSer = 1001
        '    Me.Text = "Proceso De Cambio de Solo Internet A Cliente Normal"
        '    Me.CMBLabel1.Text = "Proceso De Cambio de Solo Internet A Cliente Normal"
        'End If
        rbSoloaNormal.Checked = True
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If ComboBoxCompanias.SelectedValue = 0 Then
        '    MsgBox("Selecciona una Compa��a")
        '    Exit Sub
        'End If
        TextBox2.Text = GloIdCompania.ToString
        FrmSelCliente.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If ComboBoxCompanias.SelectedValue = 0 Then
        '    MsgBox("Selecciona una Compa��a")
        '    Exit Sub
        'End If
        If TextBox1.Text = "" Or TextBox1.Text = "0" Then
            MsgBox("Ingresa un contrato")
            Exit Sub
        End If
        If rbSoloaNormal.Checked Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, TextBox1.Text)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.ProcedimientoOutPut("sp_cambioSoloInternetaNormal")
            If BaseII.dicoPar("@error") = "Todo bien" Then
                MsgBox("Proceso realizado con �xito.")
            Else
                MsgBox(BaseII.dicoPar("@error"))
            End If
        Else
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, TextBox1.Text)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.ProcedimientoOutPut("sp_cambioNormalaSoloInternet")
            If BaseII.dicoPar("@error") = "Todo bien" Then
                MsgBox("Proceso realizado con �xito.")
            Else
                MsgBox(BaseII.dicoPar("@error"))
            End If
        End If
        generaOrden()
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
        If Asc(e.KeyChar) = 13 Then

        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
    Private Sub generaOrden()
        Dim op As Integer = 0
        Dim error1 As Integer = 0
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If LocProceso = 1 Then
            op = 2
        ElseIf LocProceso = 0 Then
            op = 1
        End If
        Me.Crea_Orden_Proceso_Cambio_ClienteTableAdapter.Connection = CON
        Me.Crea_Orden_Proceso_Cambio_ClienteTableAdapter.Fill(Me.ProcedimientosArnoldo2.Crea_Orden_Proceso_Cambio_Cliente, Me.TextBox1.Text, op, error1)
        CON.Close()
        '(@contrato bigint,@op int,@error int output,@idcompania int)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CObj(Me.TextBox1.Text))
        BaseII.CreateMyParameter("@op", SqlDbType.Int, op)
        BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
        Select Case error1
            Case 0
                bitsist(GloUsuario, Me.TextBox1.Text, LocGloSistema, Me.Text, "", Me.CMBLabel1.Text, GloSucursal, LocClv_Ciudad)
                MsgBox("Cliente Procesado Con Exito", MsgBoxStyle.Information)
            Case 1
                MsgBox("El Cliente No Tiene Todos Los Servicio Dados De Baja", MsgBoxStyle.Information)
            Case 2
                MsgBox("No se ha Generado La Orden de Servicio", MsgBoxStyle.Information)
        End Select

    End Sub

   

    'Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
    '    Try
    '        GloIdCompania = ComboBoxCompanias.SelectedValue
    '        TextBox1.Text = ""
    '        TextBox2.Text = ""
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub TextBox2_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox2.TextChanged
        If TextBox2.Text = "" Then
            TextBox1.Text = ""
            Exit Sub
        End If
        Try
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            Dim comando As New SqlCommand()
            comando.Connection = conexion
            Dim array As String() = TextBox2.Text.Trim.Split("-")
            GloIdCompania = array(1).Trim
            comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + array(0) + " and idcompania=" + GloIdCompania.ToString + " and idcompania in (select idcompania from Rel_Usuario_Compania where Clave=" + GloClvUsuario.ToString() + ")"
            TextBox1.Text = comando.ExecuteScalar().ToString()
            conexion.Close()
            Button1.Enabled = True
        Catch ex As Exception
            Button1.Enabled = False
            'MsgBox("Contrato no v�lido")
            'TextBox2.Text = ""
        End Try
    End Sub

    Private Sub rbSoloaNormal_CheckedChanged(sender As Object, e As EventArgs) Handles rbSoloaNormal.CheckedChanged
        
        If rbSoloaNormal.Checked Then
            GloClv_TipSer = 1001
            LocProceso = 0
        Else
            LocProceso = 1
            GloClv_TipSer = 1000
        End If
    End Sub
End Class