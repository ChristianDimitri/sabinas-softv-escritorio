Imports System.Data.SqlClient
Public Class FrmTipoClientes

    Private Sub FrmTipoClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ListBox1.Visible = True
        Me.MuestraSelecciona_TipoClienteTmpNUEVOTableAdapter.Connection = CON
        Me.MuestraSelecciona_TipoClienteTmpNUEVOTableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TipoClienteTmpNUEVO, LocClv_session)
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TipoClienteTmpCONSULTA, LocClv_session)
        CON.Close()
        'Me.MUESTRA_TIPOCLIENTESTableAdapter.Fill(Me.DataSetarnoldo.MUESTRA_TIPOCLIENTES, 0)
        'Me.ListBox2.Text = ""
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Dim x As Integer = 0
        'Dim y As Integer
        'x = Me.ListBox2.Items.Count()<zx,
        'If x > 0 Then
        '    For y = 0 To (x - 1)
        '        Me.ListBox2.SelectedIndex = y
        '        If (Me.ListBox1.Text = Me.ListBox2.Text) Then
        '            MsgBox("El Tipo de Cliente ya esta en la lista", MsgBoxStyle.Information)
        '            Exit Sub
        '            'Else
        '            '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '            '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        '        End If
        '    Next
        '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        'Else
        '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        'End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Insertauno_Seleccion_TipoClientestmpTableAdapter.Connection = CON
        Me.Insertauno_Seleccion_TipoClientestmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_TipoClientestmp, LocClv_session, CInt(Me.ListBox1.SelectedValue))
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TipoClienteTmpCONSULTA, LocClv_session)
        Me.MuestraSeleccion_TipoClienteCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_TipoClienteCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_TipoClienteCONSULTA, LocClv_session)
        CON.Close()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        LEdo_Cuenta = False
        LEdo_Cuenta2 = False
        Me.Close()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Dim x As Integer
        'Dim y As Integer
        'x = Me.ListBox1.Items.Count()
        'If Me.ListBox2.Items.Count() > 0 Then
        '    MsgBox("Primero Borre los Tipos de Cliente Seleccionados", MsgBoxStyle.Information)
        'Else
        '    Me.ListBox1.SelectedIndex = 0
        '    For y = 1 To x
        '        Me.ListBox1.SelectedIndex = (y - 1)
        '        Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '        Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        '    Next
        'End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTOSelecciona_TipoClientesTableAdapter.Connection = CON
        Me.InsertaTOSelecciona_TipoClientesTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTOSelecciona_TipoClientes, LocClv_session)
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TipoClienteTmpCONSULTA, LocClv_session)
        Me.MuestraSeleccion_TipoClienteCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_TipoClienteCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_TipoClienteCONSULTA, LocClv_session)
        CON.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Dim x As Integer
        'Dim y As Integer
        'x = Me.ListBox2.Items.Count()
        'For y = 0 To (x - 1)
        '    Me.ListBox2.Items.RemoveAt(0)
        '    Me.ListBox3.Items.RemoveAt(0)
        'Next
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTOSelecciona_TipoClientesTmpTableAdapter.Connection = CON
        Me.InsertaTOSelecciona_TipoClientesTmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTOSelecciona_TipoClientesTmp, LocClv_session)
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TipoClienteTmpCONSULTA, LocClv_session)
        Me.MuestraSeleccion_TipoClienteCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_TipoClienteCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_TipoClienteCONSULTA, LocClv_session)
        CON.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If (Me.ListBox2.SelectedIndex <> -1) Then
        '    Me.ListBox3.SelectedIndex = Me.ListBox2.SelectedIndex
        '    Me.ListBox2.Items.RemoveAt(Me.ListBox2.SelectedIndex)
        '    Me.ListBox3.Items.RemoveAt(Me.ListBox3.SelectedIndex)
        'Else
        '    MsgBox("Selecciona primero un valor a borrar")
        'End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Insertauno_Seleccion_TipoClientesTableAdapter.Connection = CON
        Me.Insertauno_Seleccion_TipoClientesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_TipoClientes, LocClv_session, CInt(Me.ListBox2.SelectedValue))
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TipoClienteTmpCONSULTA, LocClv_session)
        Me.MuestraSeleccion_TipoClienteCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_TipoClienteCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_TipoClienteCONSULTA, LocClv_session)
        CON.Close()

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim x As Integer = 0
        Dim y As Integer
        If varfrmselcompania = "normalrecordatorios" Then
            FrmSelServRep.Show()
            Me.Close()
            Exit Sub
        End If
        x = Me.ListBox2.Items.Count()
        'For y = 0 To (x - 1)
        '    Me.ListBox3.SelectedIndex = y
        '    Me.LLena_Tabla_TipoClientesTableAdapter.Fill(Me.DataSetarnoldo.LLena_Tabla_TipoClientes, LocClv_session, Me.ListBox3.Text)
        'Next
        If x = 0 Then
            MsgBox("Seleccione al menos un tipo de Cliente", MsgBoxStyle.Information)
        ElseIf x > 0 Then
            If LocOp = 2 Then
                If (Me.ListBox2.Items.Count()) > 1 Then 
                    LocDescr = "Todos los Tipos "
                    FrmSelServRep.Show()
                Else
                    LocDescr = Me.ListBox2.Items(0).ToString
                    FrmSelServRep.Show()
                End If
            End If
            If LocOp = 3 Or LocOp = 4 Or LocOp = 5 Or LocOp = 1 Or LocOp = 6 Or LocOp = 7 Or LocOp = 9 Or LocOp = 10 Or LocOp = 20 Or LocOp = 22 Or LocOp = 23 Or LocOp = 25 Or LocOp = 35 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr = "Varios Tipos de Clientes "
                    'FrmSelServRep.Show()
                    FrmSelCiudad.Show()
                Else
                    LocDescr = Me.ListBox2.Items(0).ToString
                    'FrmSelServRep.Show()
                    FrmSelCiudad.Show()
                End If
            ElseIf LocOp = 30 Then
                FrmSelTipServRep.Show()
            ElseIf LEdo_Cuenta = True Or LEdo_Cuenta2 = True Then
                FrmSelCiudad.Show()
            ElseIf LocOp = 90 Then
                FrmSeleccion_TipoCombo.Show()
            ElseIf LocOp = 45 Then 'Nuevo reporte: Contrataciones adicionales
                FrmSelCiudad.Show()
            End If
        End If
        Me.Close()
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged

    End Sub


End Class