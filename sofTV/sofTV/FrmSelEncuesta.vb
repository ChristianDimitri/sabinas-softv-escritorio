Imports System.Data.SqlClient
Public Class FrmSelEncuesta

    Dim Res As Integer = 0
    Dim Msj As String = String.Empty

    Private Sub MuestraEncuestas(ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim dataAdapter As New SqlDataAdapter("EXEC MuestraEncuestas " & CStr(Op) & ",'',''", conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ComboBoxEncuesta.DataSource = bindingSource
        Catch ex As Exception

        End Try
    End Sub

    Private Sub FrmSelEncuesta_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        eBndRepEncuesta = False
    End Sub

    Private Sub FrmSelEncuesta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        If eBndRepEncuesta = True Then
            MuestraEncuestas(0)
        Else
            MuestraEncuestas(4)
        End If

    End Sub

    Private Sub ButtonSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalir.Click

        If IsNumeric(ComboBoxEncuesta.SelectedValue) = False Then
            MsgBox("Selecciona una Encuesta.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If eBndRepEncuesta = True Then
            eBndRepEncuesta = False
            eIDEncuesta = ComboBoxEncuesta.SelectedValue 'Guarda el IDEncuesta
            FrmImprimirComision.Show()

        Else
            ValidaEncuesta(eContrato, ComboBoxEncuesta.SelectedValue)

            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            eBndEncuesta = True 'Dice si se ha seleccionado una Encuesta
            eIDEncuesta = ComboBoxEncuesta.SelectedValue 'Guarda el IDEncuesta 
            eEncuestaNombre = ComboBoxEncuesta.Text 'Guarda el Nombre de la Encuesta
        End If

        
        Me.Close()
    End Sub

    Private Sub ValidaEncuesta(ByVal Contrato As Long, ByVal IDEncuesta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaEncuesta", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@IDEncuesta", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = IDEncuesta
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            Res = 0
            Msj = String.Empty
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = parametro3.Value
            Msj = parametro4.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

End Class