Imports System.Data.SqlClient
Public Class FrmMetasCarteras
    Private Bnd As Boolean = False

    Private Sub FrmMetasCarteras_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraTipServEricTableAdapter.Connection = CON
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)
            Me.MuestraMesesTableAdapter.Connection = CON
            Me.MuestraMesesTableAdapter.Fill(Me.DataSetEric2.MuestraMeses)
            Me.MuestraAniosTableAdapter.Connection = CON
            Me.MuestraAniosTableAdapter.Fill(Me.DataSetEric2.MuestraAnios)
            CON.Close()
            Consultar()
            Bnd = True

            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
            UspDesactivaBotones(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub AnioComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AnioComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub



    Private Sub MesComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MesComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub

    Private Sub ConMetasCarterasBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConMetasCarterasBindingNavigatorSaveItem.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConMetasCarterasTableAdapter.Connection = CON
            Me.ConMetasCarterasDataGridView.EndEdit()
            Me.ConMetasCarterasTableAdapter.Update(Me.DataSetEric2.ConMetasCarteras)
            CON.Close()
            MsgBox("Se Guard� con �xito.")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub


    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim R As Integer = 0
        Try
            R = MsgBox("Se Eliminar�. �Deseas Continuar?", MsgBoxStyle.YesNo, "Atenci�n")
            If R = 6 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.ConMetasCarterasTableAdapter.Connection = CON
                Me.ConMetasCarterasTableAdapter.Delete(0, CInt(Me.ConceptoComboBox.SelectedValue), CInt(Me.MesComboBox.SelectedValue), CInt(Me.AnioComboBox.SelectedValue))
                CON.Close()
                MsgBox("Se Elimin� con �xito.")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Consultar()
        Try
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.ConMetasCarterasTableAdapter.Connection = CON2
            Me.ConMetasCarterasTableAdapter.Fill(Me.DataSetEric2.ConMetasCarteras, CInt(Me.ConceptoComboBox.SelectedValue), CInt(Me.MesComboBox.SelectedValue), CInt(Me.AnioComboBox.SelectedValue))
            CON2.Close()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ConMetasCarterasDataGridView_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConMetasCarterasDataGridView.CellEndEdit
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConMetasCarterasTableAdapter.Connection = CON
            Me.ConMetasCarterasDataGridView.EndEdit()
            Me.ConMetasCarterasTableAdapter.Update(Me.DataSetEric2.ConMetasCarteras)
            CON.Close()
            'MsgBox("Se Guard� con �xito.")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    

    Private Sub ConceptoComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub

    Private Sub ConMetasCarterasDataGridView_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConMetasCarterasDataGridView.CellLeave
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConMetasCarterasTableAdapter.Connection = CON
            Me.ConMetasCarterasDataGridView.EndEdit()
            Me.ConMetasCarterasTableAdapter.Update(Me.DataSetEric2.ConMetasCarteras)
            CON.Close()
            'MsgBox("Se Guard� con �xito.")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class