﻿Imports System.Data.SqlClient

Public Class FrmPlaza

    Private Sub FrmPlaza_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Llena_TipoDistribuidor()
        If opcion = "C" Or opcion = "M" Then
            Clv_TextBox.Text = GloClvPlazaAux
            LlenaCampos()
            If opcion = "C" Then
                TextBox1.Enabled = False
                BindingNavigatorDeleteItem.Enabled = False
                CONSERVICIOSBindingNavigatorSaveItem.Enabled = False
            End If
            If opcion = "M" Then
                TextBox1.Enabled = True
                BindingNavigatorDeleteItem.Enabled = True
                CONSERVICIOSBindingNavigatorSaveItem.Enabled = True
            End If
        End If
        If opcion = "N" Then
            BindingNavigatorDeleteItem.Enabled = False
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
    Private Sub LlenaCampos()
        Try
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            Dim comando As New SqlCommand()
            comando.Connection = conexion
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "Muestra_Plazas"
            Dim p1 As New SqlParameter("@Clv_plaza", SqlDbType.Int)
            p1.Direction = ParameterDirection.Input
            p1.Value = Clv_TextBox.Text
            comando.Parameters.Add(p1)
            Dim reader As SqlDataReader = comando.ExecuteReader()
            reader.Read()
            Clv_TextBox.Text = reader(0).ToString
            TextBox1.Text = reader(1).ToString
            tbrfc.Text = reader(2).ToString
            tbcalle.Text = reader(3).ToString
            tbexterior.Text = reader(4).ToString
            tbinterior.Text = reader(5).ToString
            tbcolonia.Text = reader(6).ToString
            tbcodigop.Text = reader(7).ToString
            tblocalidad.Text = reader(8).ToString
            tbestado.Text = reader(9).ToString
            tbentrecalles.Text = reader(10).ToString
            tbtelefono.Text = reader(11).ToString
            tbfax.Text = reader(12).ToString
            tbemail.Text = reader(13).ToString
            tbmunicipio.Text = reader(14).ToString
            tbpais.Text = reader(15).ToString
            tblada.Text = reader(16).ToString
            tblada2.Text = reader(17).ToString
            tbtelefono2.Text = reader(18).ToString
            tbContactoNombre.Text = reader(19).ToString
            TextImportePagare.Text = reader(20).ToString
            ComboBoxTipoDis.SelectedValue = reader(21).ToString
            'Datos nuevos
            comando.Dispose()
            reader.Close()
            comando.Connection = conexion
            comando.CommandText = "Muestra_PlazasInfoComercial"
            reader = comando.ExecuteReader
            If reader.Read() Then
                tbNombreComercial.Text = reader(0).ToString
                tbCalleComercial.Text = reader(1).ToString
                tbExteriorComercial.Text = reader(2).ToString
                tbInteriorComercial.Text = reader(3).ToString
                tbCPComercial.Text = reader(4).ToString
                tbColoniaComercial.Text = reader(5).ToString
                tbEntrecallesComercial.Text = reader(6).ToString
                tbLocalidadComercial.Text = reader(7).ToString
                tbMunicipioComercial.Text = reader(8).ToString
                tbEstadoComercial.Text = reader(9).ToString
                tbNombreRG.Text = reader(10).ToString
                tbTelefonoRG.Text = reader(11).ToString
                tbCelularRG.Text = reader(12).ToString
                tbEmailRG.Text = reader(13).ToString
                tbResponsableComercial.Text = reader(14).ToString
                tbtbResponsableOperaciones.Text = reader(15).ToString
                tbResponsableAtencion.Text = reader(16).ToString
            End If
            reader.Close()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub Clv_TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TextBox.TextChanged
        If Clv_TextBox.Text = 0 Then
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_PlazasTableAdapter.Connection = CON
        Me.Muestra_PlazasTableAdapter.Fill(Me.DataSetEDGAR.Muestra_Plazas, Clv_TextBox.Text, "")
        CON.Close()
    End Sub

    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        If IsNumeric(TextImportePagare.Text) = False Then TextImportePagare.Text = 0
        If TextImportePagare.Text = 0 Then
            MsgBox("Capture el Importe del Pagare", vbInformation)
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, TextBox1.Text)
        BaseII.CreateMyParameter("@mismoNombre", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.VarChar, Clv_TextBox.Text)
        BaseII.ProcedimientoOutPut("ValidaNombrePlaza")
        Dim mismoNombre = BaseII.dicoPar("@mismoNombre")
        If mismoNombre = 1 Then
            MsgBox("Ya existe un distribuidor con el mismo nombre.")
            Exit Sub
        End If
        If opcion = "M" Then
            If IsNumeric(TextImportePagare.Text) = False Then TextImportePagare.Text = 0
            If Len(TextBox1.Text) > 0 And Len(tbrfc.Text) > 0 And Len(tbcalle.Text) > 0 And Len(tbexterior.Text) > 0 And Len(tbcolonia.Text) > 0 And Len(tbcodigop.Text) > 0 And Len(tbentrecalles.Text) > 0 And Len(tblocalidad.Text) > 0 And Len(tbmunicipio.Text) > 0 And Len(tbestado.Text) > 0 And Len(tbpais.Text) > 0 And Len(tblada.Text) > 0 And Len(tbtelefono.Text) > 0 And Len(tbemail.Text) > 0 And Len(tbContactoNombre.Text) > 0 And Len(tblada2.Text) > 0 And Len(tbtelefono2.Text) > 0 And ComboBoxTipoDis.SelectedValue > 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_plaza", SqlDbType.Int, CInt(Clv_TextBox.Text))
                BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, tbNombreComercial.Text)
                BaseII.CreateMyParameter("@rfc", SqlDbType.VarChar, tbrfc.Text)
                BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, tbcalle.Text)
                BaseII.CreateMyParameter("@entrecalles", SqlDbType.VarChar, tbentrecalles.Text)
                BaseII.CreateMyParameter("@exterior", SqlDbType.VarChar, tbexterior.Text)
                BaseII.CreateMyParameter("@interior", SqlDbType.VarChar, tbinterior.Text)
                BaseII.CreateMyParameter("@telefono", SqlDbType.VarChar, tbtelefono.Text)
                BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, tbcolonia.Text)
                BaseII.CreateMyParameter("@fax", SqlDbType.VarChar, tbfax.Text)
                BaseII.CreateMyParameter("@codigopostal", SqlDbType.VarChar, tbcodigop.Text)
                BaseII.CreateMyParameter("@email", SqlDbType.VarChar, tbemail.Text)
                BaseII.CreateMyParameter("@localidad", SqlDbType.VarChar, tblocalidad.Text)
                BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, tbmunicipio.Text)
                BaseII.CreateMyParameter("@estado", SqlDbType.VarChar, tbestado.Text)
                BaseII.CreateMyParameter("@pais", SqlDbType.VarChar, tbpais.Text)
                BaseII.CreateMyParameter("@lada1", SqlDbType.VarChar, tblada.Text)
                BaseII.CreateMyParameter("@lada2", SqlDbType.VarChar, tblada2.Text)
                BaseII.CreateMyParameter("@telefono2", SqlDbType.VarChar, tbtelefono2.Text)
                BaseII.CreateMyParameter("@ContactoNombre", SqlDbType.VarChar, tbContactoNombre.Text)
                BaseII.CreateMyParameter("@ImportePagare", SqlDbType.Money, TextImportePagare.Text)
                BaseII.CreateMyParameter("@TipoDistribuidor", SqlDbType.Int, ComboBoxTipoDis.SelectedValue)
                'Datos nuevos
                BaseII.CreateMyParameter("@nombreComercial", SqlDbType.VarChar, TextBox1.Text)
                BaseII.CreateMyParameter("@calleComercial", SqlDbType.VarChar, tbCalleComercial.Text)
                BaseII.CreateMyParameter("@exteriorComercial", SqlDbType.VarChar, tbExteriorComercial.Text)
                BaseII.CreateMyParameter("@interiorComercial", SqlDbType.VarChar, tbInteriorComercial.Text)
                BaseII.CreateMyParameter("@cpComercial", SqlDbType.VarChar, tbCPComercial.Text)
                BaseII.CreateMyParameter("@coloniaComercial", SqlDbType.VarChar, tbColoniaComercial.Text)
                BaseII.CreateMyParameter("@entrecallesComercial", SqlDbType.VarChar, tbEntrecallesComercial.Text)
                BaseII.CreateMyParameter("@localidadComercial", SqlDbType.VarChar, tbLocalidadComercial.Text)
                BaseII.CreateMyParameter("@municipioComercial", SqlDbType.VarChar, tbMunicipioComercial.Text)
                BaseII.CreateMyParameter("@estadoComercial", SqlDbType.VarChar, tbEstadoComercial.Text)
                'Contacto
                BaseII.CreateMyParameter("@nombreRG", SqlDbType.VarChar, tbNombreRG.Text)
                BaseII.CreateMyParameter("@telefonoRG", SqlDbType.VarChar, tbTelefonoRG.Text)
                BaseII.CreateMyParameter("@celularRG", SqlDbType.VarChar, tbCelularRG.Text)
                BaseII.CreateMyParameter("@emailRG", SqlDbType.VarChar, tbEmailRG.Text)
                BaseII.CreateMyParameter("@responsableComercial", SqlDbType.VarChar, tbResponsableComercial.Text)
                BaseII.CreateMyParameter("@responsableOperaciones", SqlDbType.VarChar, tbtbResponsableOperaciones.Text)
                BaseII.CreateMyParameter("@responsableAtencion", SqlDbType.VarChar, tbResponsableAtencion.Text)
                BaseII.ConsultaDT("[Update_Plazas]")
                If CLng(Clv_TextBox.Text) >= 1 Then
                    MZ_SPASOCIOADOSMIZAR_DISTRIBUIDORES(tbNombreComercial.Text, tbrfc.Text, tbcalle.Text, tbcodigop.Text, tbcolonia.Text, tbemail.Text, tbentrecalles.Text, tbestado.Text, tblocalidad.Text, tbmunicipio.Text, tbexterior.Text, tbinterior.Text, tbpais.Text, tbtelefono.Text, tbfax.Text, "", Clv_TextBox.Text, "No Especificado")
                End If
                MsgBox("Distribuidor modificado correctamente")
                Me.Close()
            Else
                MsgBox("Faltan datos por completar.")
            End If

        End If
        If opcion = "N" Then
            If Len(TextBox1.Text) > 0 And Len(tbrfc.Text) > 0 And Len(tbcalle.Text) > 0 And Len(tbexterior.Text) > 0 And Len(tbcolonia.Text) > 0 And Len(tbcodigop.Text) > 0 And Len(tbentrecalles.Text) > 0 And Len(tblocalidad.Text) > 0 And Len(tbmunicipio.Text) > 0 And Len(tbestado.Text) > 0 And Len(tbpais.Text) > 0 And Len(tblada.Text) > 0 And Len(tbtelefono.Text) > 0 And Len(tbemail.Text) > 0 And Len(tbContactoNombre.Text) > 0 And Len(tblada2.Text) > 0 And Len(tbtelefono2.Text) And ComboBoxTipoDis.SelectedValue > 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, tbNombreComercial.Text)
                BaseII.CreateMyParameter("@Clv_plaza", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@rfc", SqlDbType.VarChar, tbrfc.Text)
                BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, tbcalle.Text)
                BaseII.CreateMyParameter("@entrecalles", SqlDbType.VarChar, tbentrecalles.Text)
                BaseII.CreateMyParameter("@exterior", SqlDbType.VarChar, tbexterior.Text)
                BaseII.CreateMyParameter("@interior", SqlDbType.VarChar, tbinterior.Text)
                BaseII.CreateMyParameter("@telefono", SqlDbType.VarChar, tbtelefono.Text)
                BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, tbcolonia.Text)
                BaseII.CreateMyParameter("@fax", SqlDbType.VarChar, tbfax.Text)
                BaseII.CreateMyParameter("@codigopostal", SqlDbType.VarChar, tbcodigop.Text)
                BaseII.CreateMyParameter("@email", SqlDbType.VarChar, tbemail.Text)
                BaseII.CreateMyParameter("@localidad", SqlDbType.VarChar, tblocalidad.Text)
                BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, tbmunicipio.Text)
                BaseII.CreateMyParameter("@estado", SqlDbType.VarChar, tbestado.Text)
                BaseII.CreateMyParameter("@pais", SqlDbType.VarChar, tbpais.Text)
                BaseII.CreateMyParameter("@lada1", SqlDbType.VarChar, tblada.Text)
                BaseII.CreateMyParameter("@lada2", SqlDbType.VarChar, tblada2.Text)
                BaseII.CreateMyParameter("@telefono2", SqlDbType.VarChar, tbtelefono2.Text)
                BaseII.CreateMyParameter("@ContactoNombre", SqlDbType.VarChar, tbContactoNombre.Text)
                BaseII.CreateMyParameter("@ImportePagare", SqlDbType.Money, TextImportePagare.Text)
                BaseII.CreateMyParameter("@TipoDistribuidor", SqlDbType.Int, ComboBoxTipoDis.SelectedValue)
                'Datos nuevos
                BaseII.CreateMyParameter("@nombreComercial", SqlDbType.VarChar, TextBox1.Text)
                BaseII.CreateMyParameter("@calleComercial", SqlDbType.VarChar, tbCalleComercial.Text)
                BaseII.CreateMyParameter("@exteriorComercial", SqlDbType.VarChar, tbExteriorComercial.Text)
                BaseII.CreateMyParameter("@interiorComercial", SqlDbType.VarChar, tbInteriorComercial.Text)
                BaseII.CreateMyParameter("@cpComercial", SqlDbType.VarChar, tbCPComercial.Text)
                BaseII.CreateMyParameter("@coloniaComercial", SqlDbType.VarChar, tbColoniaComercial.Text)
                BaseII.CreateMyParameter("@entrecallesComercial", SqlDbType.VarChar, tbEntrecallesComercial.Text)
                BaseII.CreateMyParameter("@localidadComercial", SqlDbType.VarChar, tbLocalidadComercial.Text)
                BaseII.CreateMyParameter("@municipioComercial", SqlDbType.VarChar, tbMunicipioComercial.Text)
                BaseII.CreateMyParameter("@estadoComercial", SqlDbType.VarChar, tbEstadoComercial.Text)
                'Contacto
                BaseII.CreateMyParameter("@nombreRG", SqlDbType.VarChar, tbNombreRG.Text)
                BaseII.CreateMyParameter("@telefonoRG", SqlDbType.VarChar, tbTelefonoRG.Text)
                BaseII.CreateMyParameter("@celularRG", SqlDbType.VarChar, tbCelularRG.Text)
                BaseII.CreateMyParameter("@emailRG", SqlDbType.VarChar, tbEmailRG.Text)
                BaseII.CreateMyParameter("@responsableComercial", SqlDbType.VarChar, tbResponsableComercial.Text)
                BaseII.CreateMyParameter("@responsableOperaciones", SqlDbType.VarChar, tbtbResponsableOperaciones.Text)
                BaseII.CreateMyParameter("@responsableAtencion", SqlDbType.VarChar, tbResponsableAtencion.Text)
                BaseII.ProcedimientoOutPut("[insert_Plazas]")

                Clv_TextBox.Text = BaseII.dicoPar("@Clv_plaza").ToString
                If Clv_TextBox.Text >= 1 Then
                    MZ_SPASOCIOADOSMIZAR_DISTRIBUIDORES(tbNombreComercial.Text, tbrfc.Text, tbcalle.Text, tbcodigop.Text, tbcolonia.Text, tbemail.Text, tbentrecalles.Text, tbestado.Text, tblocalidad.Text, tbmunicipio.Text, tbexterior.Text, tbinterior.Text, tbpais.Text, tbtelefono.Text, tbfax.Text, "", Clv_TextBox.Text, "No Especificado")
                End If

                MsgBox("Distribuidor añadido correctamente")
                Me.Close()
            Else
                MsgBox("Faltan datos por completar.")
            End If

        End If
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@Clv_plaza", SqlDbType.Int, CInt(Clv_TextBox.Text))
        BaseII.CreateMyParameter("@msj", ParameterDirection.Output, SqlDbType.VarChar, 100)
        BaseII.ProcedimientoOutPut("Delete_Plazas")
        MsgBox(BaseII.dicoPar("@msj").ToString)
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub tbmunicipio_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbmunicipio.TextChanged

    End Sub

    Private Sub Label10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Llena_TipoDistribuidor()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxTipoDis.DataSource = BaseII.ConsultaDT("Muestra_TipoDistribuidor")
            ComboBoxTipoDis.DisplayMember = "Descripcion"
            ComboBoxTipoDis.ValueMember = "IdTipoDis"


        Catch ex As Exception

        End Try
    End Sub

    Private Sub Label45_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        FrmPagareDistribuidor.showdialog()
    End Sub

    Private Sub ButtonConfigurarServicios_Click(sender As Object, e As EventArgs) Handles ButtonConfigurarServicios.Click
        FrmConfiguracionServiciosPlaza.ShowDialog()
    End Sub
End Class