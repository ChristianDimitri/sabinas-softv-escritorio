﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCabModPropioProspectos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.redLabel1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRACABLEDELCLIbajaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MUESTRACABLEMODEMSDELCLIBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.MUESTRACABLEMODEMSDELCLITableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLITableAdapter()
        Me.MUESTRACABLEDELCLIbajaTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEDELCLIbajaTableAdapter()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.mac = New System.Windows.Forms.MaskedTextBox()
        Me.ValidaCablemodemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaCablemodemTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.ValidaCablemodemTableAdapter()
        Me.AGREGAMACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AGREGAMACTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.AGREGAMACTableAdapter()
        Me.Panel1.SuspendLayout()
        CType(Me.MUESTRACABLEDELCLIbajaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.ValidaCablemodemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AGREGAMACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'redLabel1
        '
        Me.redLabel1.AutoSize = True
        Me.redLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.redLabel1.Location = New System.Drawing.Point(12, 11)
        Me.redLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.redLabel1.Name = "redLabel1"
        Me.redLabel1.Size = New System.Drawing.Size(272, 20)
        Me.redLabel1.TabIndex = 1
        Me.redLabel1.Text = "Mostrar Lista de Cablemodems"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(16, 321)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(561, 132)
        Me.Panel1.TabIndex = 2
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MUESTRACABLEDELCLIbajaBindingSource
        Me.ComboBox1.DisplayMember = "MACCABLEMODEM"
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(8, 41)
        Me.ComboBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(529, 249)
        Me.ComboBox1.TabIndex = 1
        Me.ComboBox1.TabStop = False
        Me.ComboBox1.ValueMember = "CONTRATONET"
        '
        'MUESTRACABLEDELCLIbajaBindingSource
        '
        Me.MUESTRACABLEDELCLIbajaBindingSource.DataMember = "MUESTRACABLEDELCLIbaja"
        Me.MUESTRACABLEDELCLIbajaBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(4, 12)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(296, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Cablemodem Asignados al Cliente"
        '
        'MUESTRACABLEMODEMSDELCLIBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLIBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI"
        Me.MUESTRACABLEMODEMSDELCLIBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.DarkBlue
        Me.RadioButton1.Location = New System.Drawing.Point(28, 4)
        Me.RadioButton1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(220, 24)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Ya Asignado al Cliente"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.RadioButton3)
        Me.Panel2.Controls.Add(Me.RadioButton2)
        Me.Panel2.Controls.Add(Me.RadioButton1)
        Me.Panel2.Location = New System.Drawing.Point(16, 34)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(561, 112)
        Me.Panel2.TabIndex = 0
        Me.Panel2.TabStop = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.ForeColor = System.Drawing.Color.DarkBlue
        Me.RadioButton3.Location = New System.Drawing.Point(28, 68)
        Me.RadioButton3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(202, 24)
        Me.RadioButton3.TabIndex = 2
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Nuevo Cablemodem "
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.Color.DarkBlue
        Me.RadioButton2.Location = New System.Drawing.Point(28, 36)
        Me.RadioButton2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(231, 24)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Asignado a Otro Cliente"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(396, 469)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(207, 469)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 41)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'MUESTRACABLEMODEMSDELCLITableAdapter
        '
        Me.MUESTRACABLEMODEMSDELCLITableAdapter.ClearBeforeFill = True
        '
        'MUESTRACABLEDELCLIbajaTableAdapter
        '
        Me.MUESTRACABLEDELCLIbajaTableAdapter.ClearBeforeFill = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Button2)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.mac)
        Me.Panel3.Location = New System.Drawing.Point(16, 154)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(561, 308)
        Me.Panel3.TabIndex = 3
        Me.Panel3.TabStop = True
        Me.Panel3.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(257, 65)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(283, 31)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Validar Mac del Cablemodem"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(24, 17)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(284, 20)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Tecleé la Mac del Cablemodem :"
        '
        'mac
        '
        Me.mac.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mac.Location = New System.Drawing.Point(348, 14)
        Me.mac.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.mac.Mask = "aaaa.aaaa.aaaa"
        Me.mac.Name = "mac"
        Me.mac.Size = New System.Drawing.Size(191, 30)
        Me.mac.TabIndex = 3
        Me.mac.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals
        '
        'ValidaCablemodemBindingSource
        '
        Me.ValidaCablemodemBindingSource.DataMember = "ValidaCablemodem"
        Me.ValidaCablemodemBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ValidaCablemodemTableAdapter
        '
        Me.ValidaCablemodemTableAdapter.ClearBeforeFill = True
        '
        'AGREGAMACBindingSource
        '
        Me.AGREGAMACBindingSource.DataSource = Me.NewSofTvDataSet
        Me.AGREGAMACBindingSource.Position = 0
        '
        'AGREGAMACTableAdapter
        '
        Me.AGREGAMACTableAdapter.ClearBeforeFill = True
        '
        'FrmCabModPropioProspectos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(604, 542)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.redLabel1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel3)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmCabModPropioProspectos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asignación de Cablemodem"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.MUESTRACABLEDELCLIbajaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.ValidaCablemodemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AGREGAMACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents redLabel1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents MUESTRACABLEMODEMSDELCLIBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRACABLEMODEMSDELCLITableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLITableAdapter
    Friend WithEvents MUESTRACABLEDELCLIbajaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEDELCLIbajaTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEDELCLIbajaTableAdapter
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents mac As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ValidaCablemodemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaCablemodemTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ValidaCablemodemTableAdapter
    Friend WithEvents AGREGAMACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AGREGAMACTableAdapter As sofTV.NewSofTvDataSetTableAdapters.AGREGAMACTableAdapter
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
End Class
