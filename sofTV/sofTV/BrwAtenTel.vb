Imports System.Data.SqlClient
Public Class BrwAtenTel

   

    Private Sub BrwAtenTel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConAtenTelTableAdapter.Connection = CON
        Me.ConAtenTelTableAdapter.Fill(Me.DataSetEric.ConAtenTel, 0, "", "", 0, 0, Today, 0)
        CON.Close()
    End Sub

    Private Sub BrwAtenTel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConAtenTelTableAdapter.Connection = CON
        Me.ConAtenTelTableAdapter.Fill(Me.DataSetEric.ConAtenTel, 0, "", "", 0, 0, Today, 0)
        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.TextBox1.Text.Length > 0 Then
            CON.Open()
            Me.ConAtenTelTableAdapter.Connection = CON
            Me.ConAtenTelTableAdapter.Fill(Me.DataSetEric.ConAtenTel, 0, Me.TextBox1.Text, "", 0, 0, Today, 1)
            CON.Close()
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 And Me.TextBox1.Text.Length > 0 Then
            CON.Open()
            Me.ConAtenTelTableAdapter.Connection = CON
            Me.ConAtenTelTableAdapter.Fill(Me.DataSetEric.ConAtenTel, 0, Me.TextBox1.Text, "", 0, 0, Today, 1)
            CON.Close()
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
        End If
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.TextBox2.Text.Length > 0 Then
            CON.Open()
            Me.ConAtenTelTableAdapter.Connection = CON
            Me.ConAtenTelTableAdapter.Fill(Me.DataSetEric.ConAtenTel, 0, "", Me.TextBox2.Text, 0, 0, Today, 2)
            CON.Close()
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 And Me.TextBox2.Text.Length > 0 Then
            CON.Open()
            Me.ConAtenTelTableAdapter.Connection = CON
            Me.ConAtenTelTableAdapter.Fill(Me.DataSetEric.ConAtenTel, 0, "", Me.TextBox2.Text, 0, 0, Today, 2)
            CON.Close()
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConAtenTelTableAdapter.Connection = CON
        Me.ConAtenTelTableAdapter.Fill(Me.DataSetEric.ConAtenTel, 0, "", "", 0, 0, Me.DateTimePicker1.Value, 5)
        CON.Close()
        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmAtenTel.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConAtenTelDataGridView.RowCount > 0 Then
            eOpcion = "C"
            eClave = Me.ConAtenTelDataGridView.SelectedCells.Item(0).Value
            FrmAtenTel.Show()
        Else
            MsgBox("Selecciona una Llamada a Consultar.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ConAtenTelDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eClave = Me.ConAtenTelDataGridView.SelectedCells.Item(0).Value
            FrmAtenTel.Show()
        Else
            MsgBox("Selecciona una Llamda a Modificar.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    
    
    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged

    End Sub
End Class