Imports System.Data.SqlClient
Public Class FrmCapArtiAcom

    'Private Sub Llena_companias()
    '    Try
    '        BaseII.limpiaParametros()
    '        'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
    '        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
    '        ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
    '        ComboBoxCompanias.DisplayMember = "razon_social"
    '        ComboBoxCompanias.ValueMember = "id_compania"

    '        GloIdCompania = ComboBoxCompanias.SelectedValue
    '        'GloIdCompania = 0
    '        'ComboBoxCiudades.Text = ""
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub GuardaDatosBitacora(ByVal Op As Integer)
        Try
            If Op = 0 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Nuevo Art�culo", "", "Clave: " + Me.ComboBox3.Text + " / Precio: $" + Me.TextBox1.Text + " /  No. Por Default: " + Me.TextBox2.Text, LocClv_Ciudad)
            ElseIf Op = 1 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Se Elimin� Art�culo", "Clave: " + Me.DataGridView1.SelectedCells.Item(1).Value.ToString + " / Precio: $" + Me.DataGridView1.SelectedCells.Item(3).Value.ToString, "", LocClv_Ciudad)

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = Chr(ValidaKey(Me.TextBox2, Asc(LCase(e.KeyChar)), "N"))
        If e.KeyChar = Chr(13) Then
            Agregar()
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Muestra()
        End If

    End Sub

    Private Sub LLena_Tipo()

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next
            Me.Muestra_Articulos_AcometidaTableAdapter.Connection = CON
            Me.Muestra_Articulos_AcometidaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Articulos_Acometida)
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.ComboBox4.Items.Clear()
            Me.ComboBox6.Items.Clear()
            For Each FilaRow In Me.ProcedimientosArnoldo2.Muestra_Articulos_Acometida.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                Me.ComboBox4.Items.Add(FilaRow("clv_Tipo").ToString())
                Me.ComboBox6.Items.Add(FilaRow("Concepto").ToString())
                I += 1
            Next

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

        CON.Close()

    End Sub
    Private Sub LLena_Categoria()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next
            If IsNumeric(Me.ComboBox4.Text) = True Then
                Me.Muestra_Articulos_ClasificacionTableAdapter.Connection = CON
                Me.Muestra_Articulos_ClasificacionTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Articulos_Clasificacion, CInt(Me.ComboBox4.Text))
                Dim FilaRow As DataRow
                'Me.TextBox1.Text = ""
                Me.ComboBox1.Items.Clear()
                Me.ComboBox3.Items.Clear()
                Me.ComboBox2.Items.Clear()
                For Each FilaRow In Me.ProcedimientosArnoldo2.Muestra_Articulos_Clasificacion.Rows

                    'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                    X = 0
                    'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                    'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                    'Else
                    'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                    'End If
                    Me.ComboBox2.Items.Add(FilaRow("NoArticulo").ToString())
                    Me.ComboBox3.Items.Add(FilaRow("Clave").ToString())
                    Me.ComboBox1.Items.Add(FilaRow("Concepto").ToString())
                    I += 1
                Next
            Else
                MsgBox("Seleccione la Clasificacion de Material", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FrmCapArtiAcom_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Llena_companias()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'ProcedimientosArnoldo2.Valida_Art_Acom' Puede moverla o quitarla seg�n sea necesario.
        'Me.Valida_Art_AcomTableAdapter.Connection = CON
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        Dim DT As DataTable = BaseII.ConsultaDT("Valida_Art_Acom")
        TextBox3.Text = DT.Rows(0)("Cantidad").ToString()
        'Me.Valida_Art_AcomTableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_Art_Acom)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'ProcedimientosArnoldo2.Muestra_Articulos_Acometida' Puede moverla o quitarla seg�n sea necesario.
        colorea(Me, Me.Name)
        'Me.Muestra_Articulos_AcometidaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Articulos_Acometida)
        LLena_Tipo()
        Me.ComboBox6.Text = ""
        Muestra()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub



    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr(ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "L"))
        If e.KeyChar = Chr(13) Then
            Agregar()
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Muestra()
        End If
    End Sub

    Private Sub Agregar()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim error_1 As Integer
        If IsNumeric(Me.TextBox1.Text) = True Then
            'If IsNumeric(Me.TextBox2.Text) = True Then
            If IsNumeric(Me.ComboBox2.Text) = True Then
                'Me.Inserta_Precio_Art_AcomTableAdapter.Connection = CON
                'Me.Inserta_Precio_Art_AcomTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_Precio_Art_Acom, CLng(Me.ComboBox2.Text), CDbl(Me.TextBox1.Text), CLng(Me.TextBox2.Text), error_1)
                '@clv_articulo bigint,@precio money,@cant bigint,@error int output, @idcompania
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_articulo", SqlDbType.BigInt, Me.ComboBox2.Text)
                BaseII.CreateMyParameter("@precio", SqlDbType.Money, Me.TextBox1.Text)
                BaseII.CreateMyParameter("@cant", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
                BaseII.ProcedimientoOutPut("Inserta_Precio_Art_Acom")
                error_1 = CInt(BaseII.dicoPar("@error").ToString)
                Me.ComboBox6.Focus()
                If error_1 = 1 Then

                    MsgBox("El Articulo ya exite en la lista", MsgBoxStyle.Information)
                Else
                    GuardaDatosBitacora(0)
                End If
            Else
                MsgBox("Se Requiere que se escoja el Articulo", MsgBoxStyle.Information)
            End If

            'Else
            '    MsgBox("Se requiere la Cantidad Default del Articulo", MsgBoxStyle.Information)
            'End If
        Else
        MsgBox("Se requiere el Precio del Articulo", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub
    Private Sub Muestra()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 1)
        'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
        DataGridView1.DataSource = BaseII.ConsultaDT("Muestra_Art_Acom")

        'Me.Muestra_Art_AcomTableAdapter.Connection = CON
        'Me.Muestra_Art_AcomTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Art_Acom)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 1)
        Dim DT As DataTable = BaseII.ConsultaDT("Valida_Art_Acom")
        TextBox3.Text = DT.Rows(0)("Cantidad").ToString()
        'Me.Valida_Art_AcomTableAdapter.Connection = CON
        'Me.Valida_Art_AcomTableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_Art_Acom)
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Agregar()
        Muestra()
    End Sub

    Private Sub ComboBox3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox3.LostFocus
        If Me.ComboBox3.SelectedIndex = -1 Then
            Me.ComboBox1.SelectedIndex = -1
            Me.ComboBox2.SelectedIndex = -1
            Me.ComboBox1.Text = ""
            Me.ComboBox2.Text = ""
            Me.ComboBox3.Text = ""
        End If
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
        Me.ComboBox1.SelectedIndex = Me.ComboBox3.SelectedIndex
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try

        
        
            If CInt(Me.TextBox3.Text) > 0 Then

                GuardaDatosBitacora(1)
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.Eliminar_Art_AcomTableAdapter.Connection = CON
                Me.Eliminar_Art_AcomTableAdapter.Fill(Me.ProcedimientosArnoldo2.Eliminar_Art_Acom, CLng(Me.DataGridView1.SelectedCells(0).Value))
                CON.Close()
                Muestra()
            ElseIf CInt(Me.TextBox3.Text) = 0 Then
                MsgBox("No hay Articulos que Borrar", MsgBoxStyle.Information)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBox4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox4.LostFocus
        If Me.ComboBox4.SelectedIndex = -1 Then
            Me.ComboBox6.Text = ""
            Me.ComboBox4.Text = ""
        End If
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Me.ComboBox6.SelectedIndex = Me.ComboBox4.SelectedIndex
        LLena_Categoria()
    End Sub

    Private Sub ComboBox6_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox6.LostFocus
        If Me.ComboBox6.SelectedIndex = -1 Then
            Me.ComboBox4.Text = ""
            Me.ComboBox4.SelectedIndex = -1
            Me.ComboBox2.Items.Clear()
            Me.ComboBox3.Items.Clear()
            Me.ComboBox1.Items.Clear()
            Me.ComboBox2.Text = ""
            Me.ComboBox3.Text = ""
            Me.ComboBox1.Text = ""
        End If
    End Sub

    Private Sub ComboBox6_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox6.SelectedIndexChanged
        Me.ComboBox4.SelectedIndex = Me.ComboBox6.SelectedIndex
        Me.ComboBox1.Text = ""
        Me.ComboBox2.Text = ""
        Me.ComboBox3.Text = ""


    End Sub

    Private Sub ComboBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.LostFocus
        If Me.ComboBox1.SelectedIndex = -1 Then
            Me.ComboBox2.SelectedIndex = -1
            Me.ComboBox3.SelectedIndex = -1
            Me.ComboBox3.Text = ""
            Me.ComboBox2.Text = ""
            Me.ComboBox1.Text = ""
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.ComboBox3.SelectedIndex = Me.ComboBox1.SelectedIndex
        Me.ComboBox2.SelectedIndex = Me.ComboBox1.SelectedIndex
    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub CMBLblTitulo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLblTitulo.Click

    End Sub
    Dim a As Integer = 0
    
    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            If ComboBoxCompanias.SelectedValue = 0 Then
                ComboBoxCompanias.SelectedValue = a
                Exit Sub
            ElseIf ComboBoxCompanias.SelectedValue = a Then
                Exit Sub
            End If
            GloIdCompania = ComboBoxCompanias.SelectedValue
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'TODO: esta l�nea de c�digo carga datos en la tabla 'ProcedimientosArnoldo2.Valida_Art_Acom' Puede moverla o quitarla seg�n sea necesario.
            'Me.Valida_Art_AcomTableAdapter.Connection = CON
            'Me.Valida_Art_AcomTableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_Art_Acom)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            Dim DT As DataTable = BaseII.ConsultaDT("Valida_Art_Acom")
            TextBox3.Text = DT.Rows(0)("Cantidad").ToString()
            'TODO: esta l�nea de c�digo carga datos en la tabla 'ProcedimientosArnoldo2.Muestra_Articulos_Acometida' Puede moverla o quitarla seg�n sea necesario.
            colorea(Me, Me.Name)
            'Me.Muestra_Articulos_AcometidaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Articulos_Acometida)
            LLena_Tipo()
            Me.ComboBox6.Text = ""
            Muestra()
            CON.Close()
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
            UspDesactivaBotones(Me, Me.Name)
            a = ComboBoxCompanias.SelectedValue
        Catch ex As Exception

        End Try
    End Sub
End Class