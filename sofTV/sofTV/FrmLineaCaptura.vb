﻿Imports System.Data.SqlClient
Public Class FrmLineaCaptura

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub FrmLineaCaptura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        rbCobra.Checked = True
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If tbContrato.Text = "" Or Not IsNumeric(tbContrato.Text) Then
            Exit Sub
        End If
        If rbImporte.Checked And Not IsNumeric(tbMonto.Text) Then
            MsgBox("Ingrese el importe por el que se generará la referencia.")
            Exit Sub
        End If
        Dim importe As Double
        If rbImporte.Checked Then
            importe = tbMonto.Text
        Else
            importe = 0
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, tbContrato.Text)
        BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@msg", ParameterDirection.Output, SqlDbType.VarChar, 500)
        BaseII.CreateMyParameter("@referencia", ParameterDirection.Output, SqlDbType.VarChar, 500)
        BaseII.CreateMyParameter("@importe", SqlDbType.Money, importe)
        BaseII.ProcedimientoOutPut("GeneraReferenciaPago")
        If BaseII.dicoPar("@error") = 0 Then
            tbReferencia.Text = BaseII.dicoPar("@referencia").ToString
        Else
            MsgBox("No se pudo generar. Error: " + BaseII.dicoPar("@msg").ToString)
            tbReferencia.Text = ""
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        GloClv_TipSer = 1000
        FrmSelCliente.ShowDialog()
        If GLOCONTRATOSEL > 0 Then
            tbContratoCompania.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
            tbContrato.Text = GLOCONTRATOSEL.ToString
        End If
    End Sub

    Private Sub tbContratoCompania_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbContratoCompania.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If tbContratoCompania.Text = "" Then
                Exit Sub
            End If
            Try
                Dim arr() = tbContratoCompania.Text.Split("-")
                Dim comando As New SqlCommand()
                Dim conexion As New SqlConnection(MiConexion)
                conexion.Open()
                comando.Connection = conexion
                comando.CommandText = "select contrato from Rel_Contratos_Companias where ContratoCompania=" + arr(0).ToString.Trim + " and IdCompania=" + arr(1).ToString.Trim
                tbContrato.Text = comando.ExecuteScalar().ToString
                conexion.Close()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub tbContratoCompania_TextChanged(sender As Object, e As EventArgs) Handles tbContratoCompania.TextChanged
        If tbContratoCompania.Text = "" Then
            Exit Sub
        End If
        Try
            Dim arr() = tbContratoCompania.Text.Split("-")
            Dim comando As New SqlCommand()
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            comando.Connection = conexion
            comando.CommandText = "select contrato from Rel_Contratos_Companias where ContratoCompania=" + arr(0).ToString.Trim + " and IdCompania=" + arr(1).ToString.Trim
            tbContrato.Text = comando.ExecuteScalar().ToString
            conexion.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub rbImporte_CheckedChanged(sender As Object, e As EventArgs) Handles rbImporte.CheckedChanged
        If rbImporte.Checked Then
            lbMonto.Visible = True
            tbMonto.Visible = True
        Else
            lbMonto.Visible = False
            tbMonto.Visible = False
        End If
    End Sub
End Class