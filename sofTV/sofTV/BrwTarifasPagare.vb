﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualBasic

Public Class BrwTarifasPagare
    'Private Sub Llena_companias()
    '    Try
    '        BaseII.limpiaParametros()
    '        'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
    '        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
    '        ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
    '        ComboBoxCompanias.DisplayMember = "razon_social"
    '        ComboBoxCompanias.ValueMember = "id_compania"

    '        If ComboBoxCompanias.Items.Count > 0 Then
    '            ComboBoxCompanias.SelectedIndex = 0

    '        End If
    '        GloIdCompania = 0
    '        'ComboBoxCiudades.Text = ""
    '    Catch ex As Exception

    '    End Try
    'End Sub
    Private Sub BrwTarifasPagare_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If BndActualizaPagare = True Then
            BndActualizaPagare = False
            MuestraTipoServicioPagare(1)
            ConCostosPagare(1, Me.TipServCombo.SelectedValue, 0)
        End If
    End Sub

    Private Sub BrwTarifasPagare_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'Llena_companias()
        GloIdCompania = 1 'ComboBoxCompanias.SelectedValue
        MuestraTipoServicioPagare(1)
        ConCostosPagare(1, Me.TipServCombo.SelectedValue, 0)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Public Sub MuestraTipoServicioPagare(ByVal OPCION As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraTipoServicioPagare ")
        StrSQL.Append(CStr(OPCION))
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            TipServCombo.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ConCostosPagare(ByVal OPCION As Integer, ByVal TIPOSERVICIO As Integer, ByVal ID As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC ConCostosPagare ")
        StrSql.Append(CStr(OPCION) & ", ")
        StrSql.Append(CStr(TIPOSERVICIO) & ", ")
        StrSql.Append(CStr(ID) & ", ")
        StrSql.Append(CStr(GloIdCompania))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSql.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.CostosDataGrid.DataSource = BS.DataSource
            Me.CostosDataGrid.Columns(3).DefaultCellStyle.Format = "c"
            Me.CostosDataGrid.Columns(4).DefaultCellStyle.Format = "c"
            Me.CostosDataGrid.Columns(5).DefaultCellStyle.Format = "c"
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Dispose()
            CON.Close()
        End Try
    End Sub

    Private Sub EliCostosPagare(ByVal IdCosto As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("EliCostosPagare", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@IdCosto", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = IdCosto
        CMD.Parameters.Add(PRM1)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            MsgBox("Registro Eliminado Satisfactoriamente", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Dispose()
            CON.Close()
        End Try
    End Sub

    Private Sub SalirButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirButton.Click
        Me.Close()
    End Sub

    Private Sub NuevoButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoButton.Click
        OpPagare = "N"
        IdPagare = 0
        FrmTarifasPagare.Show()
    End Sub

    Private Sub ModificarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarButton.Click
        If CostosDataGrid.RowCount = 0 Then
            MsgBox("Debe seleccionar al menos un registro para modificar", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        OpPagare = "M"
        IdPagare = Me.CostosDataGrid.SelectedCells.Item(0).Value
        FrmTarifasPagare.Show()
    End Sub

    Private Sub EliminarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarButton.Click
        If CostosDataGrid.RowCount = 0 Then
            MsgBox("Debe seleccionar al menos un registro a eliminar", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        Dim resp = MsgBox("¿Estás seguro de eliminar el registro seleccionado?", MsgBoxStyle.OkCancel)
        
        If resp = MsgBoxResult.Ok Then
            EliCostosPagare(Me.CostosDataGrid.SelectedCells.Item(0).Value())
            ConCostosPagare(1, Me.TipServCombo.SelectedValue, 0)
        Else
            MsgBox("No se ha eliminado ningún registro", MsgBoxStyle.Information)
            BndActualizaPagare = True
        End If
    End Sub

    Private Sub TipServCombo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipServCombo.SelectedIndexChanged
        ConCostosPagare(1, Me.TipServCombo.SelectedValue, 0)
    End Sub

    'Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
    '    Try
    '        GloIdCompania = ComboBoxCompanias.SelectedValue
    '        ConCostosPagare(1, Me.TipServCombo.SelectedValue, 0)
    '    Catch ex As Exception

    '    End Try
    'End Sub
End Class
