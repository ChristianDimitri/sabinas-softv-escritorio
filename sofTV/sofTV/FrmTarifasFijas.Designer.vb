﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTarifasFijas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTarifasFijas))
        Me.dgvTarifasFijas = New System.Windows.Forms.DataGridView()
        Me.IdTarifa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bnTarifas = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.bnSalir = New System.Windows.Forms.Button()
        CType(Me.dgvTarifasFijas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnTarifas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTarifas.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvTarifasFijas
        '
        Me.dgvTarifasFijas.AllowUserToAddRows = False
        Me.dgvTarifasFijas.AllowUserToDeleteRows = False
        Me.dgvTarifasFijas.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTarifasFijas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTarifasFijas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTarifasFijas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdTarifa, Me.Descripcion, Me.Precio})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTarifasFijas.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvTarifasFijas.Location = New System.Drawing.Point(40, 49)
        Me.dgvTarifasFijas.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvTarifasFijas.Name = "dgvTarifasFijas"
        Me.dgvTarifasFijas.Size = New System.Drawing.Size(624, 491)
        Me.dgvTarifasFijas.TabIndex = 0
        '
        'IdTarifa
        '
        Me.IdTarifa.DataPropertyName = "IdTarifa"
        Me.IdTarifa.HeaderText = "IdTarifa"
        Me.IdTarifa.Name = "IdTarifa"
        Me.IdTarifa.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Servicio"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Width = 250
        '
        'Precio
        '
        Me.Precio.DataPropertyName = "Precio"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Precio.DefaultCellStyle = DataGridViewCellStyle2
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Name = "Precio"
        Me.Precio.Width = 150
        '
        'bnTarifas
        '
        Me.bnTarifas.AddNewItem = Nothing
        Me.bnTarifas.CountItem = Nothing
        Me.bnTarifas.DeleteItem = Nothing
        Me.bnTarifas.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTarifas.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.bnTarifas.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbGuardar})
        Me.bnTarifas.Location = New System.Drawing.Point(0, 0)
        Me.bnTarifas.MoveFirstItem = Nothing
        Me.bnTarifas.MoveLastItem = Nothing
        Me.bnTarifas.MoveNextItem = Nothing
        Me.bnTarifas.MovePreviousItem = Nothing
        Me.bnTarifas.Name = "bnTarifas"
        Me.bnTarifas.PositionItem = Nothing
        Me.bnTarifas.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnTarifas.Size = New System.Drawing.Size(709, 27)
        Me.bnTarifas.TabIndex = 4
        Me.bnTarifas.Text = "BindingNavigator1"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(107, 24)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(483, 564)
        Me.bnSalir.Margin = New System.Windows.Forms.Padding(4)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(181, 44)
        Me.bnSalir.TabIndex = 5
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'FrmTarifasFijas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(709, 636)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnTarifas)
        Me.Controls.Add(Me.dgvTarifasFijas)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FrmTarifasFijas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tarifas Fijas de Contratación"
        CType(Me.dgvTarifasFijas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnTarifas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTarifas.ResumeLayout(False)
        Me.bnTarifas.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvTarifasFijas As System.Windows.Forms.DataGridView
    Friend WithEvents IdTarifa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bnTarifas As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnSalir As System.Windows.Forms.Button
End Class
