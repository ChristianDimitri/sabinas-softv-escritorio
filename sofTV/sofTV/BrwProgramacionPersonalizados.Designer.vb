<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwProgramacionPersonalizados
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Clv_calleLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnElimina = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.btnFecha = New System.Windows.Forms.Button()
        Me.btnBuscaNombre = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.NameJob = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Enable = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Date_Created = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Date_Modified = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Enable1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Freq_Type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Active_Start_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Active_End_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Active_Start_Time = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Active_End_Time = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModifica = New System.Windows.Forms.Button()
        NombreLabel = New System.Windows.Forms.Label()
        Clv_calleLabel1 = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.White
        NombreLabel.Location = New System.Drawing.Point(19, 140)
        NombreLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(169, 18)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Fecha de Activación :"
        '
        'Clv_calleLabel1
        '
        Clv_calleLabel1.AutoSize = True
        Clv_calleLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_calleLabel1.ForeColor = System.Drawing.Color.White
        Clv_calleLabel1.Location = New System.Drawing.Point(23, 39)
        Clv_calleLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_calleLabel1.Name = "Clv_calleLabel1"
        Clv_calleLabel1.Size = New System.Drawing.Size(78, 18)
        Clv_calleLabel1.TabIndex = 1
        Clv_calleLabel1.Text = "Nombre :"
        '
        'btnElimina
        '
        Me.btnElimina.BackColor = System.Drawing.Color.Orange
        Me.btnElimina.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnElimina.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnElimina.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.btnElimina.Location = New System.Drawing.Point(1157, 121)
        Me.btnElimina.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnElimina.Name = "btnElimina"
        Me.btnElimina.Size = New System.Drawing.Size(181, 44)
        Me.btnElimina.TabIndex = 33
        Me.btnElimina.Text = "&ELIMINAR"
        Me.btnElimina.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Maroon
        Me.Label2.Location = New System.Drawing.Point(8, 222)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(169, 18)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Fecha de Activación :"
        '
        'btnGenerar
        '
        Me.btnGenerar.BackColor = System.Drawing.Color.Orange
        Me.btnGenerar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerar.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.btnGenerar.Location = New System.Drawing.Point(1157, 17)
        Me.btnGenerar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(181, 44)
        Me.btnGenerar.TabIndex = 32
        Me.btnGenerar.Text = "&NUEVO"
        Me.btnGenerar.UseVisualStyleBackColor = False
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(16, 930)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(433, 22)
        Me.TextBox2.TabIndex = 31
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(12, 255)
        Me.DateTimePicker1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(184, 22)
        Me.DateTimePicker1.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Maroon
        Me.Label3.Location = New System.Drawing.Point(8, 69)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 18)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Nombre :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(16, 15)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.DateTimePicker1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnFecha)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnBuscaNombre)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(1115, 854)
        Me.SplitContainer1.SplitterDistance = 370
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 30
        Me.SplitContainer1.TabStop = False
        '
        'btnFecha
        '
        Me.btnFecha.BackColor = System.Drawing.Color.DarkOrange
        Me.btnFecha.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFecha.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.btnFecha.Location = New System.Drawing.Point(151, 300)
        Me.btnFecha.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnFecha.Name = "btnFecha"
        Me.btnFecha.Size = New System.Drawing.Size(117, 28)
        Me.btnFecha.TabIndex = 8
        Me.btnFecha.Text = "&Buscar"
        Me.btnFecha.UseVisualStyleBackColor = False
        '
        'btnBuscaNombre
        '
        Me.btnBuscaNombre.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscaNombre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscaNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscaNombre.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.btnBuscaNombre.Location = New System.Drawing.Point(151, 139)
        Me.btnBuscaNombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnBuscaNombre.Name = "btnBuscaNombre"
        Me.btnBuscaNombre.Size = New System.Drawing.Size(117, 28)
        Me.btnBuscaNombre.TabIndex = 6
        Me.btnBuscaNombre.Text = "&Buscar"
        Me.btnBuscaNombre.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.lblFecha)
        Me.Panel1.Controls.Add(Me.lblNombre)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Clv_calleLabel1)
        Me.Panel1.Location = New System.Drawing.Point(9, 407)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(324, 422)
        Me.Panel1.TabIndex = 8
        '
        'lblFecha
        '
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(23, 172)
        Me.lblFecha.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(272, 55)
        Me.lblFecha.TabIndex = 10
        '
        'lblNombre
        '
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.Location = New System.Drawing.Point(23, 63)
        Me.lblNombre.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(272, 55)
        Me.lblNombre.TabIndex = 9
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Enabled = False
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(23, 276)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(105, 22)
        Me.CheckBox1.TabIndex = 8
        Me.CheckBox1.Text = "Habilitado"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(4, 2)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(266, 25)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos de la Programación "
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(12, 106)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(255, 24)
        Me.TextBox1.TabIndex = 5
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(7, 17)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(325, 29)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar Programación Por :"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NameJob, Me.Enable, Me.Date_Created, Me.Date_Modified, Me.Enable1, Me.Freq_Type, Me.Active_Start_Date, Me.Active_End_Date, Me.Active_Start_Time, Me.Active_End_Time})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridView1.Location = New System.Drawing.Point(5, 5)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(729, 846)
        Me.DataGridView1.TabIndex = 0
        '
        'NameJob
        '
        Me.NameJob.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.NameJob.DataPropertyName = "NameJob"
        Me.NameJob.HeaderText = "Nombre"
        Me.NameJob.Name = "NameJob"
        Me.NameJob.ReadOnly = True
        Me.NameJob.Width = 103
        '
        'Enable
        '
        Me.Enable.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.Enable.DataPropertyName = "Enabled1"
        Me.Enable.HeaderText = "Trabajo Activado"
        Me.Enable.Name = "Enable"
        Me.Enable.ReadOnly = True
        Me.Enable.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Enable.Width = 163
        '
        'Date_Created
        '
        Me.Date_Created.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Date_Created.DataPropertyName = "Date_Created"
        Me.Date_Created.HeaderText = "Fecha Creacion"
        Me.Date_Created.Name = "Date_Created"
        Me.Date_Created.ReadOnly = True
        Me.Date_Created.Width = 155
        '
        'Date_Modified
        '
        Me.Date_Modified.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Date_Modified.DataPropertyName = "Date_Modified"
        Me.Date_Modified.HeaderText = "Fecha Modificacion"
        Me.Date_Modified.Name = "Date_Modified"
        Me.Date_Modified.ReadOnly = True
        Me.Date_Modified.Width = 183
        '
        'Enable1
        '
        Me.Enable1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.Enable1.DataPropertyName = "Enabled2"
        Me.Enable1.HeaderText = "Programacion Activa"
        Me.Enable1.Name = "Enable1"
        Me.Enable1.ReadOnly = True
        Me.Enable1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Enable1.Width = 193
        '
        'Freq_Type
        '
        Me.Freq_Type.DataPropertyName = "Freq_Type"
        Me.Freq_Type.HeaderText = "Frecuencia"
        Me.Freq_Type.Name = "Freq_Type"
        Me.Freq_Type.ReadOnly = True
        '
        'Active_Start_Date
        '
        Me.Active_Start_Date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Active_Start_Date.DataPropertyName = "Active_Start_Date"
        Me.Active_Start_Date.HeaderText = "Fecha de Activacion"
        Me.Active_Start_Date.Name = "Active_Start_Date"
        Me.Active_Start_Date.ReadOnly = True
        Me.Active_Start_Date.Width = 190
        '
        'Active_End_Date
        '
        Me.Active_End_Date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Active_End_Date.DataPropertyName = "Active_End_Date"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Active_End_Date.DefaultCellStyle = DataGridViewCellStyle2
        Me.Active_End_Date.HeaderText = "Fecha de Finalizacion"
        Me.Active_End_Date.Name = "Active_End_Date"
        Me.Active_End_Date.ReadOnly = True
        Me.Active_End_Date.Width = 202
        '
        'Active_Start_Time
        '
        Me.Active_Start_Time.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Active_Start_Time.DataPropertyName = "Active_Start_Time"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Active_Start_Time.DefaultCellStyle = DataGridViewCellStyle3
        Me.Active_Start_Time.HeaderText = "Hora de Activacion"
        Me.Active_Start_Time.Name = "Active_Start_Time"
        Me.Active_Start_Time.ReadOnly = True
        Me.Active_Start_Time.Width = 181
        '
        'Active_End_Time
        '
        Me.Active_End_Time.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Active_End_Time.DataPropertyName = "Active_End_Time"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.Active_End_Time.DefaultCellStyle = DataGridViewCellStyle4
        Me.Active_End_Time.HeaderText = "Hora de Finalizacion"
        Me.Active_End_Time.Name = "Active_End_Time"
        Me.Active_End_Time.ReadOnly = True
        Me.Active_End_Time.Width = 193
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.btnSalir.Location = New System.Drawing.Point(1157, 821)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(181, 44)
        Me.btnSalir.TabIndex = 29
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnModifica
        '
        Me.btnModifica.BackColor = System.Drawing.Color.Orange
        Me.btnModifica.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModifica.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModifica.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.btnModifica.Location = New System.Drawing.Point(1157, 69)
        Me.btnModifica.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnModifica.Name = "btnModifica"
        Me.btnModifica.Size = New System.Drawing.Size(181, 44)
        Me.btnModifica.TabIndex = 28
        Me.btnModifica.Text = "&MODIFICAR"
        Me.btnModifica.UseVisualStyleBackColor = False
        '
        'BrwProgramacionPersonalizados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1355, 912)
        Me.Controls.Add(Me.btnElimina)
        Me.Controls.Add(Me.btnGenerar)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModifica)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "BrwProgramacionPersonalizados"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catalogo Programaciones"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnElimina As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnFecha As System.Windows.Forms.Button
    Friend WithEvents btnBuscaNombre As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModifica As System.Windows.Forms.Button
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents NameJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Enable As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Date_Created As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Date_Modified As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Enable1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Freq_Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Active_Start_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Active_End_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Active_Start_Time As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Active_End_Time As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
