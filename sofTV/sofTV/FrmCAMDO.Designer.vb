<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCAMDO
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CLAVELabel As System.Windows.Forms.Label
        Dim Clv_OrdenLabel As System.Windows.Forms.Label
        Dim CONTRATOLabel As System.Windows.Forms.Label
        Dim Clv_CalleLabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim ENTRECALLESLabel As System.Windows.Forms.Label
        Dim Clv_ColoniaLabel As System.Windows.Forms.Label
        Dim TELEFONOLabel As System.Windows.Forms.Label
        Dim Clv_CiudadLabel As System.Windows.Forms.Label
        Dim Clv_CalleLabel1 As System.Windows.Forms.Label
        Dim Clv_ColoniaLabel1 As System.Windows.Forms.Label
        Dim Clv_CiudadLabel1 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCAMDO))
        Me.CONCAMDOBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.CONCAMDOBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CLAVETextBox = New System.Windows.Forms.TextBox()
        Me.Clv_OrdenTextBox = New System.Windows.Forms.TextBox()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_CalleComboBox = New System.Windows.Forms.ComboBox()
        Me.NUMEROTextBox = New System.Windows.Forms.TextBox()
        Me.ENTRECALLESTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ColoniaComboBox = New System.Windows.Forms.ComboBox()
        Me.TELEFONOTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Clv_CalleTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbLocalidad = New System.Windows.Forms.ComboBox()
        Me.NumIntTbox = New System.Windows.Forms.TextBox()
        Me.Clv_CiudadComboBox = New System.Windows.Forms.ComboBox()
        Me.ComboBoxSector = New System.Windows.Forms.ComboBox()
        Me.LabelEquipo815 = New System.Windows.Forms.Label()
        Me.LabelNodo = New System.Windows.Forms.Label()
        Me.cbEquipo815 = New System.Windows.Forms.ComboBox()
        Me.cbNodo = New System.Windows.Forms.ComboBox()
        Me.CONCAMDOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.CONCAMDOTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCAMDOTableAdapter()
        Me.MUESTRACALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMECOLONIACALLEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraCVECOLCIUBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACALLESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACALLESTableAdapter()
        Me.DAMECOLONIA_CALLETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DAMECOLONIA_CALLETableAdapter()
        Me.MuestraCVECOLCIUTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraCVECOLCIUTableAdapter()
        Me.BORDetOrdSer_INTELIGENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSer_INTELIGENTETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter()
        CLAVELabel = New System.Windows.Forms.Label()
        Clv_OrdenLabel = New System.Windows.Forms.Label()
        CONTRATOLabel = New System.Windows.Forms.Label()
        Clv_CalleLabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        ENTRECALLESLabel = New System.Windows.Forms.Label()
        Clv_ColoniaLabel = New System.Windows.Forms.Label()
        TELEFONOLabel = New System.Windows.Forms.Label()
        Clv_CiudadLabel = New System.Windows.Forms.Label()
        Clv_CalleLabel1 = New System.Windows.Forms.Label()
        Clv_ColoniaLabel1 = New System.Windows.Forms.Label()
        Clv_CiudadLabel1 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        CType(Me.CONCAMDOBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCAMDOBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.CONCAMDOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECOLONIACALLEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraCVECOLCIUBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CLAVELabel
        '
        CLAVELabel.AutoSize = True
        CLAVELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CLAVELabel.ForeColor = System.Drawing.Color.LightSlateGray
        CLAVELabel.Location = New System.Drawing.Point(-3, 5)
        CLAVELabel.Name = "CLAVELabel"
        CLAVELabel.Size = New System.Drawing.Size(53, 15)
        CLAVELabel.TabIndex = 2
        CLAVELabel.Text = "CLAVE:"
        '
        'Clv_OrdenLabel
        '
        Clv_OrdenLabel.AutoSize = True
        Clv_OrdenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_OrdenLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_OrdenLabel.Location = New System.Drawing.Point(215, 3)
        Clv_OrdenLabel.Name = "Clv_OrdenLabel"
        Clv_OrdenLabel.Size = New System.Drawing.Size(73, 15)
        Clv_OrdenLabel.TabIndex = 4
        Clv_OrdenLabel.Text = "Clv Orden:"
        '
        'CONTRATOLabel
        '
        CONTRATOLabel.AutoSize = True
        CONTRATOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CONTRATOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CONTRATOLabel.Location = New System.Drawing.Point(433, 1)
        CONTRATOLabel.Name = "CONTRATOLabel"
        CONTRATOLabel.Size = New System.Drawing.Size(84, 15)
        CONTRATOLabel.TabIndex = 6
        CONTRATOLabel.Text = "CONTRATO:"
        '
        'Clv_CalleLabel
        '
        Clv_CalleLabel.AutoSize = True
        Clv_CalleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CalleLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_CalleLabel.Location = New System.Drawing.Point(85, 23)
        Clv_CalleLabel.Name = "Clv_CalleLabel"
        Clv_CalleLabel.Size = New System.Drawing.Size(48, 15)
        Clv_CalleLabel.TabIndex = 8
        Clv_CalleLabel.Text = "Calle :"
        Clv_CalleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMEROLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NUMEROLabel.Location = New System.Drawing.Point(67, 50)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(66, 15)
        NUMEROLabel.TabIndex = 10
        NUMEROLabel.Text = "Numero :"
        NUMEROLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ENTRECALLESLabel
        '
        ENTRECALLESLabel.AutoSize = True
        ENTRECALLESLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ENTRECALLESLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ENTRECALLESLabel.Location = New System.Drawing.Point(41, 76)
        ENTRECALLESLabel.Name = "ENTRECALLESLabel"
        ENTRECALLESLabel.Size = New System.Drawing.Size(93, 15)
        ENTRECALLESLabel.TabIndex = 12
        ENTRECALLESLabel.Text = "Entre Calles :"
        ENTRECALLESLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Clv_ColoniaLabel
        '
        Clv_ColoniaLabel.AutoSize = True
        Clv_ColoniaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_ColoniaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_ColoniaLabel.Location = New System.Drawing.Point(71, 102)
        Clv_ColoniaLabel.Name = "Clv_ColoniaLabel"
        Clv_ColoniaLabel.Size = New System.Drawing.Size(64, 15)
        Clv_ColoniaLabel.TabIndex = 14
        Clv_ColoniaLabel.Text = "Colonia :"
        Clv_ColoniaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TELEFONOLabel
        '
        TELEFONOLabel.AutoSize = True
        TELEFONOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TELEFONOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TELEFONOLabel.Location = New System.Drawing.Point(62, 189)
        TELEFONOLabel.Name = "TELEFONOLabel"
        TELEFONOLabel.Size = New System.Drawing.Size(71, 15)
        TELEFONOLabel.TabIndex = 16
        TELEFONOLabel.Text = "Teléfono :"
        TELEFONOLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Clv_CiudadLabel
        '
        Clv_CiudadLabel.AutoSize = True
        Clv_CiudadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CiudadLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_CiudadLabel.Location = New System.Drawing.Point(73, 160)
        Clv_CiudadLabel.Name = "Clv_CiudadLabel"
        Clv_CiudadLabel.Size = New System.Drawing.Size(60, 15)
        Clv_CiudadLabel.TabIndex = 20
        Clv_CiudadLabel.Text = "Ciudad :"
        Clv_CiudadLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Clv_CalleLabel1
        '
        Clv_CalleLabel1.AutoSize = True
        Clv_CalleLabel1.Location = New System.Drawing.Point(308, 26)
        Clv_CalleLabel1.Name = "Clv_CalleLabel1"
        Clv_CalleLabel1.Size = New System.Drawing.Size(51, 13)
        Clv_CalleLabel1.TabIndex = 22
        Clv_CalleLabel1.Text = "Clv Calle:"
        '
        'Clv_ColoniaLabel1
        '
        Clv_ColoniaLabel1.AutoSize = True
        Clv_ColoniaLabel1.Location = New System.Drawing.Point(281, 105)
        Clv_ColoniaLabel1.Name = "Clv_ColoniaLabel1"
        Clv_ColoniaLabel1.Size = New System.Drawing.Size(63, 13)
        Clv_ColoniaLabel1.TabIndex = 23
        Clv_ColoniaLabel1.Text = "Clv Colonia:"
        '
        'Clv_CiudadLabel1
        '
        Clv_CiudadLabel1.AutoSize = True
        Clv_CiudadLabel1.Location = New System.Drawing.Point(177, 162)
        Clv_CiudadLabel1.Name = "Clv_CiudadLabel1"
        Clv_CiudadLabel1.Size = New System.Drawing.Size(61, 13)
        Clv_CiudadLabel1.TabIndex = 24
        Clv_CiudadLabel1.Text = "Clv Ciudad:"
        AddHandler Clv_CiudadLabel1.Click, AddressOf Me.Clv_CiudadLabel1_Click
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(347, 53)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(116, 15)
        Label1.TabIndex = 27
        Label1.Text = "Numero Interior :"
        Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(97, 351)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(56, 15)
        Label2.TabIndex = 445
        Label2.Text = "Sector :"
        Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Label2.Visible = False
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(56, 135)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(78, 15)
        Label3.TabIndex = 451
        Label3.Text = "Localidad :"
        Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CONCAMDOBindingNavigator
        '
        Me.CONCAMDOBindingNavigator.AddNewItem = Nothing
        Me.CONCAMDOBindingNavigator.BindingSource = Me.CONCAMDOBindingSource
        Me.CONCAMDOBindingNavigator.CountItem = Nothing
        Me.CONCAMDOBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONCAMDOBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorSeparator2, Me.BindingNavigatorDeleteItem, Me.CONCAMDOBindingNavigatorSaveItem})
        Me.CONCAMDOBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCAMDOBindingNavigator.MoveFirstItem = Nothing
        Me.CONCAMDOBindingNavigator.MoveLastItem = Nothing
        Me.CONCAMDOBindingNavigator.MoveNextItem = Nothing
        Me.CONCAMDOBindingNavigator.MovePreviousItem = Nothing
        Me.CONCAMDOBindingNavigator.Name = "CONCAMDOBindingNavigator"
        Me.CONCAMDOBindingNavigator.PositionItem = Nothing
        Me.CONCAMDOBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONCAMDOBindingNavigator.Size = New System.Drawing.Size(661, 25)
        Me.CONCAMDOBindingNavigator.TabIndex = 6
        Me.CONCAMDOBindingNavigator.TabStop = True
        Me.CONCAMDOBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(82, 22)
        Me.BindingNavigatorDeleteItem.Text = "&BORRAR"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'CONCAMDOBindingNavigatorSaveItem
        '
        Me.CONCAMDOBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCAMDOBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCAMDOBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCAMDOBindingNavigatorSaveItem.Name = "CONCAMDOBindingNavigatorSaveItem"
        Me.CONCAMDOBindingNavigatorSaveItem.Size = New System.Drawing.Size(87, 22)
        Me.CONCAMDOBindingNavigatorSaveItem.Text = "&ACEPTAR"
        '
        'CLAVETextBox
        '
        Me.CLAVETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOBindingSource, "CLAVE", True))
        Me.CLAVETextBox.Location = New System.Drawing.Point(90, 2)
        Me.CLAVETextBox.Name = "CLAVETextBox"
        Me.CLAVETextBox.Size = New System.Drawing.Size(121, 20)
        Me.CLAVETextBox.TabIndex = 3
        Me.CLAVETextBox.TabStop = False
        '
        'Clv_OrdenTextBox
        '
        Me.Clv_OrdenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOBindingSource, "Clv_Orden", True))
        Me.Clv_OrdenTextBox.Location = New System.Drawing.Point(308, 0)
        Me.Clv_OrdenTextBox.Name = "Clv_OrdenTextBox"
        Me.Clv_OrdenTextBox.Size = New System.Drawing.Size(121, 20)
        Me.Clv_OrdenTextBox.TabIndex = 5
        Me.Clv_OrdenTextBox.TabStop = False
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOBindingSource, "CONTRATO", True))
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(526, -2)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(121, 20)
        Me.CONTRATOTextBox.TabIndex = 7
        Me.CONTRATOTextBox.TabStop = False
        '
        'Clv_CalleComboBox
        '
        Me.Clv_CalleComboBox.DisplayMember = "NOMBRE"
        Me.Clv_CalleComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Clv_CalleComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CalleComboBox.ForeColor = System.Drawing.Color.Black
        Me.Clv_CalleComboBox.FormattingEnabled = True
        Me.Clv_CalleComboBox.Location = New System.Drawing.Point(140, 22)
        Me.Clv_CalleComboBox.Name = "Clv_CalleComboBox"
        Me.Clv_CalleComboBox.Size = New System.Drawing.Size(444, 23)
        Me.Clv_CalleComboBox.TabIndex = 0
        Me.Clv_CalleComboBox.ValueMember = "CLV_CALLE"
        '
        'NUMEROTextBox
        '
        Me.NUMEROTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NUMEROTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOBindingSource, "NUMERO", True))
        Me.NUMEROTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROTextBox.ForeColor = System.Drawing.Color.Black
        Me.NUMEROTextBox.Location = New System.Drawing.Point(140, 49)
        Me.NUMEROTextBox.Name = "NUMEROTextBox"
        Me.NUMEROTextBox.Size = New System.Drawing.Size(203, 21)
        Me.NUMEROTextBox.TabIndex = 1
        '
        'ENTRECALLESTextBox
        '
        Me.ENTRECALLESTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ENTRECALLESTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOBindingSource, "ENTRECALLES", True))
        Me.ENTRECALLESTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ENTRECALLESTextBox.ForeColor = System.Drawing.Color.Black
        Me.ENTRECALLESTextBox.Location = New System.Drawing.Point(140, 75)
        Me.ENTRECALLESTextBox.Name = "ENTRECALLESTextBox"
        Me.ENTRECALLESTextBox.Size = New System.Drawing.Size(338, 21)
        Me.ENTRECALLESTextBox.TabIndex = 2
        '
        'Clv_ColoniaComboBox
        '
        Me.Clv_ColoniaComboBox.DisplayMember = "COLONIA"
        Me.Clv_ColoniaComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Clv_ColoniaComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ColoniaComboBox.ForeColor = System.Drawing.Color.Black
        Me.Clv_ColoniaComboBox.FormattingEnabled = True
        Me.Clv_ColoniaComboBox.Location = New System.Drawing.Point(140, 101)
        Me.Clv_ColoniaComboBox.Name = "Clv_ColoniaComboBox"
        Me.Clv_ColoniaComboBox.Size = New System.Drawing.Size(338, 23)
        Me.Clv_ColoniaComboBox.TabIndex = 3
        Me.Clv_ColoniaComboBox.ValueMember = "CLV_COLONIA"
        '
        'TELEFONOTextBox
        '
        Me.TELEFONOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TELEFONOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOBindingSource, "TELEFONO", True))
        Me.TELEFONOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TELEFONOTextBox.ForeColor = System.Drawing.Color.Black
        Me.TELEFONOTextBox.Location = New System.Drawing.Point(140, 188)
        Me.TELEFONOTextBox.Name = "TELEFONOTextBox"
        Me.TELEFONOTextBox.Size = New System.Drawing.Size(203, 21)
        Me.TELEFONOTextBox.TabIndex = 4
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(511, 334)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Clv_CalleTextBox
        '
        Me.Clv_CalleTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOBindingSource, "Clv_Calle", True))
        Me.Clv_CalleTextBox.Location = New System.Drawing.Point(365, 23)
        Me.Clv_CalleTextBox.Name = "Clv_CalleTextBox"
        Me.Clv_CalleTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_CalleTextBox.TabIndex = 23
        Me.Clv_CalleTextBox.TabStop = False
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOBindingSource, "Clv_Colonia", True))
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(350, 102)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_ColoniaTextBox.TabIndex = 24
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'Clv_CiudadTextBox
        '
        Me.Clv_CiudadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOBindingSource, "Clv_Ciudad", True))
        Me.Clv_CiudadTextBox.Location = New System.Drawing.Point(244, 159)
        Me.Clv_CiudadTextBox.Name = "Clv_CiudadTextBox"
        Me.Clv_CiudadTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_CiudadTextBox.TabIndex = 25
        Me.Clv_CiudadTextBox.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cbLocalidad)
        Me.Panel1.Controls.Add(Label3)
        Me.Panel1.Controls.Add(Me.NumIntTbox)
        Me.Panel1.Controls.Add(Label1)
        Me.Panel1.Controls.Add(Me.Clv_CiudadComboBox)
        Me.Panel1.Controls.Add(Clv_CiudadLabel)
        Me.Panel1.Controls.Add(Me.TELEFONOTextBox)
        Me.Panel1.Controls.Add(TELEFONOLabel)
        Me.Panel1.Controls.Add(Me.Clv_ColoniaComboBox)
        Me.Panel1.Controls.Add(Clv_ColoniaLabel)
        Me.Panel1.Controls.Add(Me.ENTRECALLESTextBox)
        Me.Panel1.Controls.Add(ENTRECALLESLabel)
        Me.Panel1.Controls.Add(Me.NUMEROTextBox)
        Me.Panel1.Controls.Add(Clv_CiudadLabel1)
        Me.Panel1.Controls.Add(Clv_CalleLabel)
        Me.Panel1.Controls.Add(Me.Clv_CiudadTextBox)
        Me.Panel1.Controls.Add(NUMEROLabel)
        Me.Panel1.Controls.Add(Clv_ColoniaLabel1)
        Me.Panel1.Controls.Add(Me.Clv_CalleComboBox)
        Me.Panel1.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.Panel1.Controls.Add(Me.Clv_CalleTextBox)
        Me.Panel1.Controls.Add(Clv_CalleLabel1)
        Me.Panel1.Location = New System.Drawing.Point(30, 40)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(617, 221)
        Me.Panel1.TabIndex = 26
        '
        'cbLocalidad
        '
        Me.cbLocalidad.DisplayMember = "NOMBRE"
        Me.cbLocalidad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbLocalidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbLocalidad.ForeColor = System.Drawing.Color.Black
        Me.cbLocalidad.FormattingEnabled = True
        Me.cbLocalidad.Location = New System.Drawing.Point(140, 130)
        Me.cbLocalidad.Name = "cbLocalidad"
        Me.cbLocalidad.Size = New System.Drawing.Size(338, 23)
        Me.cbLocalidad.TabIndex = 450
        Me.cbLocalidad.ValueMember = "Clv_Localidad"
        '
        'NumIntTbox
        '
        Me.NumIntTbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumIntTbox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOBindingSource, "NUMERO", True))
        Me.NumIntTbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumIntTbox.ForeColor = System.Drawing.Color.Black
        Me.NumIntTbox.Location = New System.Drawing.Point(469, 51)
        Me.NumIntTbox.Name = "NumIntTbox"
        Me.NumIntTbox.Size = New System.Drawing.Size(115, 21)
        Me.NumIntTbox.TabIndex = 26
        '
        'Clv_CiudadComboBox
        '
        Me.Clv_CiudadComboBox.DisplayMember = "Nombre"
        Me.Clv_CiudadComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Clv_CiudadComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CiudadComboBox.ForeColor = System.Drawing.Color.Black
        Me.Clv_CiudadComboBox.FormattingEnabled = True
        Me.Clv_CiudadComboBox.Location = New System.Drawing.Point(140, 159)
        Me.Clv_CiudadComboBox.Name = "Clv_CiudadComboBox"
        Me.Clv_CiudadComboBox.Size = New System.Drawing.Size(338, 23)
        Me.Clv_CiudadComboBox.TabIndex = 5
        Me.Clv_CiudadComboBox.ValueMember = "Clv_Ciudad"
        '
        'ComboBoxSector
        '
        Me.ComboBoxSector.DisplayMember = "Descripcion"
        Me.ComboBoxSector.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxSector.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxSector.FormattingEnabled = True
        Me.ComboBoxSector.Location = New System.Drawing.Point(159, 344)
        Me.ComboBoxSector.Name = "ComboBoxSector"
        Me.ComboBoxSector.Size = New System.Drawing.Size(237, 24)
        Me.ComboBoxSector.TabIndex = 444
        Me.ComboBoxSector.ValueMember = "Clv_Sector"
        Me.ComboBoxSector.Visible = False
        '
        'LabelEquipo815
        '
        Me.LabelEquipo815.AutoSize = True
        Me.LabelEquipo815.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.LabelEquipo815.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelEquipo815.Location = New System.Drawing.Point(36, 296)
        Me.LabelEquipo815.Name = "LabelEquipo815"
        Me.LabelEquipo815.Size = New System.Drawing.Size(127, 15)
        Me.LabelEquipo815.TabIndex = 455
        Me.LabelEquipo815.Text = "Equipo del cliente:"
        Me.LabelEquipo815.Visible = False
        '
        'LabelNodo
        '
        Me.LabelNodo.AutoSize = True
        Me.LabelNodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.LabelNodo.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelNodo.Location = New System.Drawing.Point(73, 269)
        Me.LabelNodo.Name = "LabelNodo"
        Me.LabelNodo.Size = New System.Drawing.Size(90, 15)
        Me.LabelNodo.TabIndex = 454
        Me.LabelNodo.Text = "Nodo de red:"
        Me.LabelNodo.Visible = False
        '
        'cbEquipo815
        '
        Me.cbEquipo815.DisplayMember = "Nombre"
        Me.cbEquipo815.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbEquipo815.FormattingEnabled = True
        Me.cbEquipo815.Location = New System.Drawing.Point(170, 293)
        Me.cbEquipo815.Name = "cbEquipo815"
        Me.cbEquipo815.Size = New System.Drawing.Size(278, 23)
        Me.cbEquipo815.TabIndex = 453
        Me.cbEquipo815.ValueMember = "idEquipo"
        Me.cbEquipo815.Visible = False
        '
        'cbNodo
        '
        Me.cbNodo.DisplayMember = "Nombre"
        Me.cbNodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbNodo.FormattingEnabled = True
        Me.cbNodo.Location = New System.Drawing.Point(170, 267)
        Me.cbNodo.Name = "cbNodo"
        Me.cbNodo.Size = New System.Drawing.Size(278, 23)
        Me.cbNodo.TabIndex = 452
        Me.cbNodo.ValueMember = "idNodo"
        Me.cbNodo.Visible = False
        '
        'CONCAMDOBindingSource
        '
        Me.CONCAMDOBindingSource.DataMember = "CONCAMDO"
        Me.CONCAMDOBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.EnforceConstraints = False
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONCAMDOTableAdapter
        '
        Me.CONCAMDOTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACALLESBindingSource
        '
        Me.MUESTRACALLESBindingSource.DataMember = "MUESTRACALLES"
        Me.MUESTRACALLESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DAMECOLONIACALLEBindingSource
        '
        Me.DAMECOLONIACALLEBindingSource.DataMember = "DAMECOLONIA_CALLE"
        Me.DAMECOLONIACALLEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MuestraCVECOLCIUBindingSource
        '
        Me.MuestraCVECOLCIUBindingSource.DataMember = "MuestraCVECOLCIU"
        Me.MuestraCVECOLCIUBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACALLESTableAdapter
        '
        Me.MUESTRACALLESTableAdapter.ClearBeforeFill = True
        '
        'DAMECOLONIA_CALLETableAdapter
        '
        Me.DAMECOLONIA_CALLETableAdapter.ClearBeforeFill = True
        '
        'MuestraCVECOLCIUTableAdapter
        '
        Me.MuestraCVECOLCIUTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSer_INTELIGENTEBindingSource
        '
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataMember = "BORDetOrdSer_INTELIGENTE"
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSer_INTELIGENTETableAdapter
        '
        Me.BORDetOrdSer_INTELIGENTETableAdapter.ClearBeforeFill = True
        '
        'FrmCAMDO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(661, 388)
        Me.Controls.Add(Me.LabelEquipo815)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.LabelNodo)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cbEquipo815)
        Me.Controls.Add(Me.ComboBoxSector)
        Me.Controls.Add(Me.cbNodo)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.CONCAMDOBindingNavigator)
        Me.Controls.Add(CLAVELabel)
        Me.Controls.Add(Me.CLAVETextBox)
        Me.Controls.Add(Clv_OrdenLabel)
        Me.Controls.Add(Me.Clv_OrdenTextBox)
        Me.Controls.Add(CONTRATOLabel)
        Me.Controls.Add(Me.CONTRATOTextBox)
        Me.Name = "FrmCAMDO"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Capture el Nuevo Domicilio"
        Me.TopMost = True
        CType(Me.CONCAMDOBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCAMDOBindingNavigator.ResumeLayout(False)
        Me.CONCAMDOBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONCAMDOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECOLONIACALLEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraCVECOLCIUBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONCAMDOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCAMDOTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCAMDOTableAdapter
    Friend WithEvents CONCAMDOBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CONCAMDOBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CLAVETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_OrdenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CalleComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents NUMEROTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ENTRECALLESTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TELEFONOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MUESTRACALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACALLESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACALLESTableAdapter
    Friend WithEvents DAMECOLONIACALLEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECOLONIA_CALLETableAdapter As sofTV.NewSofTvDataSetTableAdapters.DAMECOLONIA_CALLETableAdapter
    Friend WithEvents MuestraCVECOLCIUBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraCVECOLCIUTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraCVECOLCIUTableAdapter
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Clv_CalleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BORDetOrdSer_INTELIGENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSer_INTELIGENTETableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents NumIntTbox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CiudadComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxSector As System.Windows.Forms.ComboBox
    Friend WithEvents cbLocalidad As System.Windows.Forms.ComboBox
    Friend WithEvents LabelEquipo815 As System.Windows.Forms.Label
    Friend WithEvents LabelNodo As System.Windows.Forms.Label
    Friend WithEvents cbEquipo815 As System.Windows.Forms.ComboBox
    Friend WithEvents cbNodo As System.Windows.Forms.ComboBox
End Class
