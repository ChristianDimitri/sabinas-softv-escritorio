﻿
Public Class BrwNap

    Private Sub CONSULTATap(ByVal Op As Integer, ByVal IdTap As Integer, ByVal Clave As String, ByVal Sector As String, ByVal Poste As String, ByVal Colonia As String, ByVal Calle As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@IdTap", SqlDbType.Int, IdTap)
        BaseII.CreateMyParameter("@Clave", SqlDbType.VarChar, Clave, 150)
        BaseII.CreateMyParameter("@Sector", SqlDbType.VarChar, Sector, 150)
        BaseII.CreateMyParameter("@Poste", SqlDbType.VarChar, Poste, 150)
        BaseII.CreateMyParameter("@Colonia", SqlDbType.VarChar, Colonia, 150)
        BaseII.CreateMyParameter("@Calle", SqlDbType.VarChar, Calle, 150)
        dgvTaps.DataSource = BaseII.ConsultaDT("CONSULTAnap")
    End Sub

    Private Sub tbClave_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbClave.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbClave.Text.Length = 0 Then Exit Sub
        CONSULTATap(1, 0, tbClave.Text, "", "", "", "")
    End Sub

    Private Sub bnClave_Click(sender As System.Object, e As System.EventArgs) Handles bnClave.Click
        If tbClave.Text.Length = 0 Then Exit Sub
        CONSULTATap(1, 0, tbClave.Text, "", "", "", "")
    End Sub

    Private Sub tbSector_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbSector.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbSector.Text.Length = 0 Then Exit Sub
        CONSULTATap(2, 0, "", tbSector.Text, "", "", "")
    End Sub

    Private Sub bnSector_Click(sender As System.Object, e As System.EventArgs) Handles bnSector.Click
        If tbSector.Text.Length = 0 Then Exit Sub
        CONSULTATap(2, 0, "", tbSector.Text, "", "", "")
    End Sub

    Private Sub tbPoste_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbPoste.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbPoste.Text.Length = 0 Then Exit Sub
        CONSULTATap(3, 0, "", "", tbPoste.Text, "", "")
    End Sub

    Private Sub bnPoste_Click(sender As System.Object, e As System.EventArgs) Handles bnPoste.Click
        If tbPoste.Text.Length = 0 Then Exit Sub
        CONSULTATap(3, 0, "", "", tbPoste.Text, "", "")
    End Sub

    Private Sub tbColonia_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbColonia.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbColonia.Text.Length = 0 Then Exit Sub
        CONSULTATap(4, 0, "", "", "", tbColonia.Text, "")
    End Sub

    Private Sub bnColonia_Click(sender As System.Object, e As System.EventArgs) Handles bnColonia.Click
        If tbColonia.Text.Length = 0 Then Exit Sub
        CONSULTATap(4, 0, "", "", "", tbColonia.Text, "")
    End Sub

    Private Sub tbCalle_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbCalle.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbCalle.Text.Length = 0 Then Exit Sub
        CONSULTATap(5, 0, "", "", "", "", tbCalle.Text)
    End Sub

    Private Sub bnCalle_Click(sender As System.Object, e As System.EventArgs) Handles bnCalle.Click
        If tbCalle.Text.Length = 0 Then Exit Sub
        CONSULTATap(5, 0, "", "", "", "", tbCalle.Text)
    End Sub

    Private Sub bnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles bnNuevo.Click
        eOpcion = "N"
        FrmNap.Show()
    End Sub

    Private Sub bnConsultar_Click(sender As System.Object, e As System.EventArgs) Handles bnConsultar.Click
        If dgvTaps.RowCount = 0 Then
            MessageBox.Show("Selecciona un Clave Técnica.")
            Exit Sub
        End If
        eIdTap = dgvTaps.SelectedCells(0).Value
        eOpcion = "C"
        FrmNap.Show()
    End Sub

    Private Sub bnModifica_Click(sender As System.Object, e As System.EventArgs) Handles bnModifica.Click
        If dgvTaps.RowCount = 0 Then
            MessageBox.Show("Selecciona un Clave Técnica.")
            Exit Sub
        End If
        eIdTap = dgvTaps.SelectedCells(0).Value
        eOpcion = "M"
        FrmNap.Show()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub BrwTap_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            CONSULTATap(0, 0, "", "", "", "", "")
        End If
    End Sub

    Private Sub BrwTap_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        GloBnd = False
        CONSULTATap(0, 0, "", "", "", "", "")
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
End Class