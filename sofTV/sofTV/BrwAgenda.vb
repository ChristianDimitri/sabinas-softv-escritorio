
Imports System.Data.SqlClient
Imports System.Text
Imports sofTV.Base

Enum OpcionSetupBoxTarjeta
    SinFiltro = 1
    PorSetupBox = 2
    PorTarjeta = 3
    PorAmbos = 4
End Enum


Public Class BrwAgenda

    Public consulta As New CBase
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                GloIdCompania = ComboBoxCompanias.SelectedValue
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_Ciudad()
        Try
            BaseII.limpiaParametros()
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("Muestra_ciudad")
            ComboBoxCiudad.DisplayMember = "Nombre"
            ComboBoxCiudad.ValueMember = "clv_ciudad"

            If ComboBoxCiudad.Items.Count > 0 Then
                ComboBoxCiudad.SelectedIndex = 0
                GloClvCiudad = ComboBoxCiudad.SelectedValue
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub CargaSectoresComboBox()
        Dim dt As DataTable = consulta.consultarDT("Softv_MuestraSectores", Nothing)

        If dt.Rows.Count > 0 Then
            SectorComboBox.DisplayMember = "sector"
            SectorComboBox.ValueMember = "Clv_Sector"
            SectorComboBox.DataSource = dt
        End If
    End Sub

    Private Sub BUSCA(ByVal prmIncluirFecha As Boolean)
        'Private Sub BUSCA(ByVal OP As Integer)
        Try
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            ''If IsDate(Me.TextBox3.Text) = True Then
            ''FECHA_TXT
            ''End If
            'GloClv_tecnico = Me.ComboBox4.SelectedValue
            'If OP = 0 Then
            '    If IsNumeric(Me.TextBox1.Text) = True Then
            '        'Me.BUSCAAGENDATableAdapter.Connection = CON
            '        'Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), 0, Me.TextBox1.Text, "", "01/01/2001", "01/01/2001", 0)
            '        BUSCAAGENDAPORSEPARADO(GloClv_tecnico, "", Me.TextBox1.Text, "", "", "", "01/01/2001", "01/01/2001", "", "", 0)
            '    Else
            '        MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
            '    End If
            'ElseIf OP = 1 Then
            '    If Me.TextBox2.Text.Length > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then
            '        'Me.BUSCAAGENDATableAdapter.Connection = CON
            '        'Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), 0, 0, Me.TextBox2.Text, "01/01/2001", "01/01/2001", 1)
            '        BUSCAAGENDAPORSEPARADO(GloClv_tecnico, "", 0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "01/01/2001", "01/01/2001", "", "", 1)
            '    Else
            '        MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
            '    End If
            'ElseIf OP = 2 Then
            '    If IsDate(Me.TextBox3.Text) = True Then
            '        'Me.BUSCAAGENDATableAdapter.Connection = CON
            '        'Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), 0, 0, "", Me.TextBox3.Text, "01/01/2001", 2)
            '        BUSCAAGENDAPORSEPARADO(GloClv_tecnico, "", 0, "", "", "", Me.TextBox3.Text, "01/01/2001", "", "", 2)
            '    Else
            '        MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
            '    End If
            'ElseIf OP = 3 Then
            '    If IsDate(Me.TextBox4.Text) = True Then
            '        'Me.BUSCAAGENDATableAdapter.Connection = CON
            '        'Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), 0, 0, "", "01/01/2001", "01/01/2001" & " " & Me.TextBox4.Text, 3)
            '        Dim h As String = ""
            '        h = FormatDateTime(Me.TextBox4.ValidateText, DateFormat.ShortTime)
            '        'MessageBox(Me.TextBox4.ValidateText)
            '        BUSCAAGENDAPORSEPARADO(GloClv_tecnico, "", 0, "", "", "", "01/01/2001", h, "", "", 3)
            '    Else
            '        MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
            '    End If
            'ElseIf OP = 5 Then
            '    'If IsNumeric(Me.TextBox5.Text) = True Then

            '    'Me.BUSCAAGENDATableAdapter.Connection = CON
            '    'Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), Me.TextBox5.Text, 0, "", "01/01/2001", "01/01/2001", 5)
            '    If Me.SectorComboBox.Items.Count <> 0 Then
            '        BUSCAAGENDAPORSEPARADO(GloClv_tecnico, Me.SectorComboBox.SelectedValue, 0, "", "", "", "01/01/2001", "01/01/2001", "", "", 5)
            '    End If

            '    'Else
            '    '  MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
            '    'End If
            'ElseIf OP = 6 Then

            '    If Len(Trim(Me.TxtSetUpBox.Text)) > 0 Or Len(Trim(Me.TxtTarjeta.Text)) > 0 Then

            '        BUSCAAGENDAPORSEPARADO(GloClv_tecnico, "", 0, "", "", "", "01/01/2001", "01/01/2001", Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 6)

            '    Else
            '        MsgBox("La Busqueda no se puede realizar con datos Invalidos", MsgBoxStyle.Information)
            '    End If
            'Else
            '    BUSCAAGENDAPORSEPARADO(GloClv_tecnico, "", 0, "", "", "", "01/01/2001", "01/01/2001", "", "", 4)
            'End If

            'Me.TextBox1.Clear()
            'Me.TextBox2.Clear()
            'Me.TextBox3.Value = Now
            'Me.TextBox4.Clear()
            ''Me.TextBox5.Clear()
            'CON.Close()

            DataGridView1.DataSource = DesplegarAgenda(prmIncluirFecha)
            DataGridView1.Columns(10).Visible = False
            DataGridView1.Columns(7).Visible = False
            DataGridView1.Columns(8).Visible = False
            DataGridView1.Columns(9).Visible = False
            DataGridView1.Columns(12).Visible = False


            '28/06/2011
            DataGridView1.Columns(4).HeaderText = "Fecha De Programaci�n"

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BrwAgenda_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            'BUSCA(4)
            BUSCA(False)
        End If
    End Sub

    Private Sub BrwAgenda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Llena_Ciudad()
        Llena_companias()
        CargaSectoresComboBox()

        Me.cmbTurno.DataSource = spConsultaTurnos(True)
        Me.cmbTurno.ValueMember = "ID"
        Me.cmbTurno.DisplayMember = "TURNO"


        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRATECNICOS' Puede moverla o quitarla seg�n sea necesario.
        ' Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            If GloTipoUsuario = 3 Then
                Me.Button2.Enabled = False
                Me.Button4.Enabled = True
            Else
                Me.Button2.Enabled = False
                Me.Button4.Enabled = False
            End If
        End If
        'Me.MUESTRATECNICOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATECNICOS)
        'Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
        'Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Tecnicos_Almacen, 0)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        ComboBox4.DataSource = BaseII.ConsultaDT("Muestra_Tecnicos_Agenda")
        'BUSCA(4)
        GloIdCompania = 0
        GloClvCiudad = 0
        BUSCA(False)
        Me.Button6.Visible = False
        CON.Close()

        'Carga sectores
        Me.TextBox4.Visible = False
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            'BUSCA(3)
            BUSCA(False)
        End If
    End Sub


    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        'BUSCA(4)
        BUSCA(False)
    End Sub

    'Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    '    If Asc(e.KeyChar) = 13 Then
    '        BUSCA(5)
    '    End If
    'End Sub

    'Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            'BUSCA(0)
            GloClvCiudad = 0
            Try
                If TextBox1.Text.Contains("-") Then
                    Dim array As String()
                    array = TextBox1.Text.Trim.Split("-")
                    loccon = array(0).Trim
                    If array(1).Length = 0 Then
                        Exit Sub
                    End If
                    GloIdCompania = array(1).Trim
                    BUSCA(False)
                Else
                    loccon = TextBox1.Text
                    GloIdCompania = 999
                    BUSCA(False)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            'BUSCA(1)
            BUSCA(False)
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            'BUSCA(2)
            BUSCA(False)
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'BUSCA(5)
        BUSCA(False)
    End Sub
    Dim loccon As Integer = 0
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        'BUSCA(0)
        GloClvCiudad = 0
        Try
            If TextBox1.Text.Contains("-") Then
                Dim array As String()
                array = TextBox1.Text.Trim.Split("-")
                loccon = array(0).Trim
                If array(1).Length = 0 Then
                    Exit Sub
                End If
                GloIdCompania = array(1).Trim
                BUSCA(False)
            Else
                loccon = TextBox1.Text
                GloIdCompania = 999
                BUSCA(False)
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'BUSCA(1)
        BUSCA(False)
        TextBox2.Text = ""
        APaternoTextBox.Text = ""
        AMaternoTextBox.Text = ""
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'BUSCA(2)
        BUSCA(True)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'BUSCA(3)
        BUSCA(False)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub TextBox4_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            'BUSCA(3)
            BUSCA(False)
        End If
    End Sub

    Private Sub MODIFICAR()
        If Me.DataGridView1.RowCount = 0 Then
            MsgBox("No hay elementos en la lista", MsgBoxStyle.Information)
            Exit Sub
        End If
        gloClave = (Me.DataGridView1.SelectedCells(7).Value)
        If IsNumeric(gloClave) = True And IsNumeric(lblContrato.Text) = True Then
            GloClv_tecnico = (Me.DataGridView1.SelectedCells(8).Value)
            GLONOM_TECNICO = Me.ComboBox4.Text
            opcion = "M"
            Contrato = lblContrato.Text
            OpcionQuejaOrden = (Me.DataGridView1.SelectedCells(0).Value)
            opcionprimera = 1
            FrmAgenda.Show()
        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub CONSULTAR()
        If Me.DataGridView1.RowCount = 0 Then
            MsgBox("No hay elementos en la lista", MsgBoxStyle.Information)
            Exit Sub
        End If
        'Dim i As Integer
        'For i = 0 To 8
        '    MsgBox("i " + Me.DataGridView1.SelectedCells(i).Value.ToString)
        'Next

        gloClave = (Me.DataGridView1.SelectedCells(7).Value)
        If IsNumeric(gloClave) = True Then
            GloClv_tecnico = (Me.DataGridView1.SelectedCells(8).Value)
            GLONOM_TECNICO = Me.ComboBox4.Text
            Contrato = lblContrato.Text
            opcion = "C"
            FrmAgenda.Show()
        Else
            MsgBox(mensaje2)
        End If

    End Sub

    Private Sub NUEVO()
        GloClv_tecnico = Me.ComboBox4.SelectedValue
        GLONOM_TECNICO = Me.ComboBox4.Text
        opcion = "N"
        FrmAgenda.Show()

    End Sub
    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsNumeric(Me.lblClave.Text) Then
            gloClave = Me.lblClave.Text
        End If
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        'Me.Clv_calleLabel2.Text = Me.DataGridView1.SelectedCells.Item(6).Value()
        'Me.Label5.Text = Me.DataGridView1.SelectedCells.Item(1).Value()
        'Me.CONTRATOLabel1.Text = Me.DataGridView1.SelectedCells.Item(2).Value()
        'Me.NOMBRELabel1.Text = Me.DataGridView1.SelectedCells.Item(3).Value()
        'Me.FECHALabel1.Text = Me.DataGridView1.SelectedCells.Item(4).Value()
        'Me.HORALabel1.Text = Me.DataGridView1.SelectedCells.Item(5).Value()

        

    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            CONSULTAR()
        ElseIf Button4.Enabled = True Then
            MODIFICAR()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        NUEVO()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        CONSULTAR()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        MODIFICAR()
    End Sub

    Private Sub TextBox3_KeyPress2(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            'BUSCA(2)
            BUSCA(False)
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        FrmAgendaTecnico.Show()
    End Sub
    Private Sub BUSCAAGENDAPORSEPARADO(ByVal Clv_tecnicoCli As Integer, ByVal Clv_CitaCli As String, ByVal ContratoCli As Long, ByVal NombreCli As String, ByVal APaternoCli As String, ByVal AMaternoCli As String, _
                                         ByVal Fecha As DateTime, ByVal hora As DateTime, ByVal SetUpBox As String, ByVal Tarjeta As String, ByVal OpCli As Integer)


        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC BUSCAAGENDAPORSEPARADO ")
        StrSQL.Append(CStr(Clv_tecnicoCli) & ",")
        StrSQL.Append("'" & Clv_CitaCli & "',")
        StrSQL.Append(CStr(ContratoCli) & ",")
        StrSQL.Append("'" & NombreCli & "',")
        StrSQL.Append("'" & APaternoCli & "',")
        StrSQL.Append("'" & AMaternoCli & "',")
        StrSQL.Append("'" & Fecha & "',")
        StrSQL.Append("'" & hora & "',")
        StrSQL.Append("'" & SetUpBox & "',")
        StrSQL.Append("'" & Tarjeta & "',")
        StrSQL.Append(CStr(OpCli))


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        'BUSCA(6)
        BUSCA(False)
        TxtSetUpBox.Text = ""
        TxtTarjeta.Text = ""
    End Sub

    Private Sub SectorComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SectorComboBox.SelectedIndexChanged
        If SectorComboBox.SelectedValue = 0 Then
            'BUSCA(4)
            BUSCA(False)
        Else
            'BUSCA(5)
            BUSCA(False)
        End If
    End Sub


    Public Function DesplegarAgenda(ByVal prmIncluirFecha As Boolean) As DataTable

        BaseII.limpiaParametros()

        If Me.ComboBox4.Items.Count > 0 Then
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, Me.ComboBox4.SelectedValue)
        End If

        If Me.SectorComboBox.SelectedValue > 0 Then
            BaseII.CreateMyParameter("@Sector", SqlDbType.Int, Me.SectorComboBox.SelectedValue)
        End If

        If loccon > 0 Then
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, CType(loccon, Integer))
        End If

        If Me.TextBox2.Text <> "" Then
            BaseII.CreateMyParameter("@NOMBRE", SqlDbType.NVarChar, Me.TextBox2.Text, 150)
        End If

        If Me.APaternoTextBox.Text <> "" Then
            BaseII.CreateMyParameter("@ApellidoPaterno", SqlDbType.NVarChar, Me.APaternoTextBox.Text, 250)
        End If

        If Me.AMaternoTextBox.Text <> "" Then
            BaseII.CreateMyParameter("@ApellidoMaterno", SqlDbType.NVarChar, Me.AMaternoTextBox.Text, 250)
        End If

        If prmIncluirFecha Then
            If IsDate(Me.TextBox3.Text) Then
                BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, Me.TextBox3.Text)
            End If

        End If

        'If IsDate(Me.TextBox4.Text) Then
        '    BaseII.CreateMyParameter("@HORA", SqlDbType.NVarChar, Me.TextBox4.Text, 5)
        'End If

        If Me.cmbTurno.SelectedValue > 0 Then
            BaseII.CreateMyParameter("@Turno", SqlDbType.NVarChar, Me.cmbTurno.Text)
        End If

        Dim opSetupBoxTarjeta As Integer = OpcionSetupBoxTarjeta.SinFiltro

        If Len(Me.TxtSetUpBox.Text) > 0 And Len(Me.TxtTarjeta.Text) = 0 Then
            opSetupBoxTarjeta = OpcionSetupBoxTarjeta.PorSetupBox
            BaseII.CreateMyParameter("@SetUpBox", SqlDbType.NVarChar, Me.TxtSetUpBox.Text, 50)
        ElseIf Len(Me.TxtSetUpBox.Text) > 0 And Len(Me.TxtTarjeta.Text) > 0 Then
            opSetupBoxTarjeta = OpcionSetupBoxTarjeta.PorAmbos
            BaseII.CreateMyParameter("@SetUpBox", SqlDbType.NVarChar, Me.TxtSetUpBox.Text, 50)
            BaseII.CreateMyParameter("@Tarjeta", SqlDbType.NVarChar, Me.TxtTarjeta.Text, 50)
        ElseIf Len(Me.TxtSetUpBox.Text) = 0 And Len(Me.TxtTarjeta.Text) > 0 Then
            opSetupBoxTarjeta = OpcionSetupBoxTarjeta.PorTarjeta
            BaseII.CreateMyParameter("@Tarjeta", SqlDbType.NVarChar, Me.TxtTarjeta.Text, 50)
        End If
        BaseII.CreateMyParameter("@opSetupBoxTarjeta", SqlDbType.Int, opSetupBoxTarjeta)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
        Dim tblAgenda As DataTable = BaseII.ConsultaDT("DesplegarAgenda") 'Ejecuto Stored Procedure que regresa el listado con la Agenda

        'Regresamos tabla 
        Return tblAgenda
    End Function

    Private Sub Button9_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        BUSCA(False)
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            BUSCA(False)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub lblcontratocompania_TextChanged(sender As System.Object, e As System.EventArgs) Handles lblcontratocompania.TextChanged
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim comando As New SqlCommand()
            comando.Connection = CON
            Dim array() = lblcontratocompania.Text.Split("-")
            comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + array(0).ToString + " and idcompania=" + array(1).ToString
            lblContrato.Text = comando.ExecuteScalar().ToString()
            CON.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxCiudad_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCiudad.SelectedIndexChanged
        Try
            GloClvCiudad = ComboBoxCiudad.SelectedValue
            Llena_companias()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Me.lblClave.Text = Me.DataGridView1.SelectedCells.Item(7).Value()
            Me.lblTipoCita.Text = Me.DataGridView1.SelectedCells.Item(0).Value()
            Me.lblcontratocompania.Text = Me.DataGridView1.SelectedCells.Item(3).Value()
            Me.lblNombre.Text = Me.DataGridView1.SelectedCells.Item(9).Value()
            Me.lblFecha.Text = Me.DataGridView1.SelectedCells.Item(5).Value()
            Me.lblHora.Text = Me.DataGridView1.SelectedCells.Item(6).Value()
        Catch ex As Exception

        End Try
    End Sub
End Class