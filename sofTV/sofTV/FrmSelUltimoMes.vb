﻿Imports System.Data.SqlClient


Public Class FrmSelUltimoMes

    Private Sub FrmSelUltimoMes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        cbUltimoMes.SelectedIndex = 0

        If op = 6 And LocOp = 4 Then

            Me.Text = "Mes y año que adeuda"
            Label1.Text = "Mes que adeuda"
            Label2.Text = "Año que adeuda"

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@year", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("DameYear")
            tbUltimoAno.Text = BaseII.dicoPar("@year").ToString()

        End If

    End Sub



    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If tbUltimoAno.Text = "" Or Not IsNumeric(tbUltimoAno.Text) Then
            Exit Sub
        End If
        Locultimo_anio = tbUltimoAno.Text
        Locultimo_mes = cbUltimoMes.SelectedIndex + 1
        bndReport = True
        bnd1 = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class