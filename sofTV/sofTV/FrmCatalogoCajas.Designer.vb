﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCatalogoCajas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim Clv_sucursalLabel As System.Windows.Forms.Label
        Dim IpMaquinaLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim ImprimeTicketsLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCatalogoCajas))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.ChBoxTermi = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.CONCatalogoCajasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRASUCURSALES2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New sofTV.NewsoftvDataSet1()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CONCatalogoCajasBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONCatalogoCajasBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_sucursalTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.ImprimeTicketsCheckBox = New System.Windows.Forms.CheckBox()
        Me.MUESTRASUCURSALESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CONCatalogoCajasTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCatalogoCajasTableAdapter()
        Me.MUESTRASUCURSALESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRASUCURSALESTableAdapter()
        Me.MUESTRASUCURSALES2TableAdapter = New sofTV.NewsoftvDataSet1TableAdapters.MUESTRASUCURSALES2TableAdapter()
        ClaveLabel = New System.Windows.Forms.Label()
        Clv_sucursalLabel = New System.Windows.Forms.Label()
        IpMaquinaLabel = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        ImprimeTicketsLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.CONCatalogoCajasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRASUCURSALES2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONCatalogoCajasBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCatalogoCajasBindingNavigator.SuspendLayout()
        CType(Me.MUESTRASUCURSALESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ClaveLabel.Location = New System.Drawing.Point(141, 65)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(46, 15)
        ClaveLabel.TabIndex = 0
        ClaveLabel.Text = "Clave:"
        '
        'Clv_sucursalLabel
        '
        Clv_sucursalLabel.AutoSize = True
        Clv_sucursalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_sucursalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_sucursalLabel.Location = New System.Drawing.Point(116, 146)
        Clv_sucursalLabel.Name = "Clv_sucursalLabel"
        Clv_sucursalLabel.Size = New System.Drawing.Size(71, 15)
        Clv_sucursalLabel.TabIndex = 2
        Clv_sucursalLabel.Text = "Sucursal :"
        '
        'IpMaquinaLabel
        '
        IpMaquinaLabel.AutoSize = True
        IpMaquinaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        IpMaquinaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        IpMaquinaLabel.Location = New System.Drawing.Point(68, 118)
        IpMaquinaLabel.Name = "IpMaquinaLabel"
        IpMaquinaLabel.Size = New System.Drawing.Size(119, 15)
        IpMaquinaLabel.TabIndex = 4
        IpMaquinaLabel.Text = "Ip de la Maquina:"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(100, 92)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(87, 15)
        DescripcionLabel.TabIndex = 6
        DescripcionLabel.Text = "Descripción:"
        '
        'ImprimeTicketsLabel
        '
        ImprimeTicketsLabel.AutoSize = True
        ImprimeTicketsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImprimeTicketsLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ImprimeTicketsLabel.Location = New System.Drawing.Point(75, 175)
        ImprimeTicketsLabel.Name = "ImprimeTicketsLabel"
        ImprimeTicketsLabel.Size = New System.Drawing.Size(113, 15)
        ImprimeTicketsLabel.TabIndex = 8
        ImprimeTicketsLabel.Text = "Imprime Tickets:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(11, 32)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(114, 15)
        Label1.TabIndex = 32
        Label1.Text = "Factura Normal :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(174, 32)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(53, 15)
        Label2.TabIndex = 34
        Label2.Text = "Ticket :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(226, 175)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(132, 15)
        Label3.TabIndex = 34
        Label3.Text = "Impresora Termica:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(139, 36)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(47, 15)
        Label4.TabIndex = 207
        Label4.Text = "Plaza:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Label4)
        Me.Panel1.Controls.Add(Me.ComboBoxCompanias)
        Me.Panel1.Controls.Add(Label3)
        Me.Panel1.Controls.Add(Me.ChBoxTermi)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.CONCatalogoCajasBindingNavigator)
        Me.Panel1.Controls.Add(ClaveLabel)
        Me.Panel1.Controls.Add(Me.ClaveTextBox)
        Me.Panel1.Controls.Add(Clv_sucursalLabel)
        Me.Panel1.Controls.Add(Me.Clv_sucursalTextBox)
        Me.Panel1.Controls.Add(IpMaquinaLabel)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Controls.Add(ImprimeTicketsLabel)
        Me.Panel1.Controls.Add(Me.ImprimeTicketsCheckBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(642, 320)
        Me.Panel1.TabIndex = 0
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(192, 33)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(252, 23)
        Me.ComboBoxCompanias.TabIndex = 206
        '
        'ChBoxTermi
        '
        Me.ChBoxTermi.AutoSize = True
        Me.ChBoxTermi.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChBoxTermi.Location = New System.Drawing.Point(366, 178)
        Me.ChBoxTermi.Name = "ChBoxTermi"
        Me.ChBoxTermi.Size = New System.Drawing.Size(15, 14)
        Me.ChBoxTermi.TabIndex = 6
        Me.ChBoxTermi.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Controls.Add(Label2)
        Me.GroupBox1.Controls.Add(Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(144, 212)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(300, 84)
        Me.GroupBox1.TabIndex = 33
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Factura Fiscal"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.CONCatalogoCajasBindingSource, "facticket", True))
        Me.RadioButton2.Location = New System.Drawing.Point(234, 36)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(14, 13)
        Me.RadioButton2.TabIndex = 36
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'CONCatalogoCajasBindingSource
        '
        Me.CONCatalogoCajasBindingSource.DataMember = "CONCatalogoCajas"
        Me.CONCatalogoCajasBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.CONCatalogoCajasBindingSource, "facnormal", True))
        Me.RadioButton1.Location = New System.Drawing.Point(131, 36)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(14, 13)
        Me.RadioButton1.TabIndex = 35
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONCatalogoCajasBindingSource, "Clv_sucursal", True))
        Me.ComboBox1.DataSource = Me.MUESTRASUCURSALES2BindingSource
        Me.ComboBox1.DisplayMember = "NOMBRE"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(193, 144)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(236, 23)
        Me.ComboBox1.TabIndex = 2
        Me.ComboBox1.ValueMember = "CLV_SUCURSAL"
        '
        'MUESTRASUCURSALES2BindingSource
        '
        Me.MUESTRASUCURSALES2BindingSource.DataMember = "MUESTRASUCURSALES2"
        Me.MUESTRASUCURSALES2BindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoCajasBindingSource, "IpMaquina", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(193, 116)
        Me.TextBox1.MaxLength = 15
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(170, 21)
        Me.TextBox1.TabIndex = 1
        '
        'CONCatalogoCajasBindingNavigator
        '
        Me.CONCatalogoCajasBindingNavigator.AddNewItem = Nothing
        Me.CONCatalogoCajasBindingNavigator.BindingSource = Me.CONCatalogoCajasBindingSource
        Me.CONCatalogoCajasBindingNavigator.CountItem = Nothing
        Me.CONCatalogoCajasBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONCatalogoCajasBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONCatalogoCajasBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONCatalogoCajasBindingNavigatorSaveItem})
        Me.CONCatalogoCajasBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCatalogoCajasBindingNavigator.MoveFirstItem = Nothing
        Me.CONCatalogoCajasBindingNavigator.MoveLastItem = Nothing
        Me.CONCatalogoCajasBindingNavigator.MoveNextItem = Nothing
        Me.CONCatalogoCajasBindingNavigator.MovePreviousItem = Nothing
        Me.CONCatalogoCajasBindingNavigator.Name = "CONCatalogoCajasBindingNavigator"
        Me.CONCatalogoCajasBindingNavigator.PositionItem = Nothing
        Me.CONCatalogoCajasBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONCatalogoCajasBindingNavigator.Size = New System.Drawing.Size(642, 27)
        Me.CONCatalogoCajasBindingNavigator.TabIndex = 4
        Me.CONCatalogoCajasBindingNavigator.TabStop = True
        Me.CONCatalogoCajasBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(94, 24)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 24)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONCatalogoCajasBindingNavigatorSaveItem
        '
        Me.CONCatalogoCajasBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCatalogoCajasBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCatalogoCajasBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCatalogoCajasBindingNavigatorSaveItem.Name = "CONCatalogoCajasBindingNavigatorSaveItem"
        Me.CONCatalogoCajasBindingNavigatorSaveItem.Size = New System.Drawing.Size(95, 24)
        Me.CONCatalogoCajasBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoCajasBindingSource, "Clave", True))
        Me.ClaveTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveTextBox.Location = New System.Drawing.Point(193, 62)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(104, 21)
        Me.ClaveTextBox.TabIndex = 10
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_sucursalTextBox
        '
        Me.Clv_sucursalTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_sucursalTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_sucursalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoCajasBindingSource, "Clv_sucursal", True))
        Me.Clv_sucursalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_sucursalTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_sucursalTextBox.Location = New System.Drawing.Point(557, 146)
        Me.Clv_sucursalTextBox.Name = "Clv_sucursalTextBox"
        Me.Clv_sucursalTextBox.Size = New System.Drawing.Size(30, 14)
        Me.Clv_sucursalTextBox.TabIndex = 30
        Me.Clv_sucursalTextBox.TabStop = False
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoCajasBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(193, 89)
        Me.DescripcionTextBox.MaxLength = 150
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(381, 21)
        Me.DescripcionTextBox.TabIndex = 0
        '
        'ImprimeTicketsCheckBox
        '
        Me.ImprimeTicketsCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONCatalogoCajasBindingSource, "ImprimeTickets", True))
        Me.ImprimeTicketsCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ImprimeTicketsCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImprimeTicketsCheckBox.Location = New System.Drawing.Point(194, 171)
        Me.ImprimeTicketsCheckBox.Name = "ImprimeTicketsCheckBox"
        Me.ImprimeTicketsCheckBox.Size = New System.Drawing.Size(26, 24)
        Me.ImprimeTicketsCheckBox.TabIndex = 3
        '
        'MUESTRASUCURSALESBindingSource
        '
        Me.MUESTRASUCURSALESBindingSource.DataMember = "MUESTRASUCURSALES"
        Me.MUESTRASUCURSALESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(518, 347)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONCatalogoCajasTableAdapter
        '
        Me.CONCatalogoCajasTableAdapter.ClearBeforeFill = True
        '
        'MUESTRASUCURSALESTableAdapter
        '
        Me.MUESTRASUCURSALESTableAdapter.ClearBeforeFill = True
        '
        'MUESTRASUCURSALES2TableAdapter
        '
        Me.MUESTRASUCURSALES2TableAdapter.ClearBeforeFill = True
        '
        'FrmCatalogoCajas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(675, 397)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmCatalogoCajas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Cajas"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.CONCatalogoCajasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRASUCURSALES2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONCatalogoCajasBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCatalogoCajasBindingNavigator.ResumeLayout(False)
        Me.CONCatalogoCajasBindingNavigator.PerformLayout()
        CType(Me.MUESTRASUCURSALESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONCatalogoCajasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCatalogoCajasTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCatalogoCajasTableAdapter
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_sucursalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImprimeTicketsCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CONCatalogoCajasBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONCatalogoCajasBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRASUCURSALESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRASUCURSALESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRASUCURSALESTableAdapter
    Friend WithEvents MUESTRASUCURSALES2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewsoftvDataSet1 As sofTV.NewsoftvDataSet1
    Friend WithEvents MUESTRASUCURSALES2TableAdapter As sofTV.NewsoftvDataSet1TableAdapters.MUESTRASUCURSALES2TableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents ChBoxTermi As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
End Class
