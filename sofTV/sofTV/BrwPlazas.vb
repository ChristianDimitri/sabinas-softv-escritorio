﻿Imports System.Data.SqlClient

Public Class BrwPlazas

    Private Sub BrwPlazas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_PlazasTableAdapter.Connection = CON
        Me.Muestra_PlazasTableAdapter.Fill(Me.DataSetEDGAR.Muestra_Plazas, 0, "")
        CON.Close()
    End Sub

    Private Sub BrwPlazas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_PlazasTableAdapter.Connection = CON
        Me.Muestra_PlazasTableAdapter.Fill(Me.DataSetEDGAR.Muestra_Plazas, 0, "")
        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If tbidplaza.Text.Length = 0 Then
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_PlazasTableAdapter.Connection = CON
        Me.Muestra_PlazasTableAdapter.Fill(Me.DataSetEDGAR.Muestra_Plazas, tbidplaza.Text, "")
        CON.Close()
    End Sub

    Private Sub btnrazonsocial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnrazonsocial.Click
        If tbNombre.Text.Length = 0 Then
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_PlazasTableAdapter.Connection = CON
        Me.Muestra_PlazasTableAdapter.Fill(Me.DataSetEDGAR.Muestra_Plazas, 0, tbNombre.Text)
        CON.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If lbidcompania.Text.Length = 0 Then
            MsgBox("Selecciona una plaza")
            Exit Sub
        End If
        opcion = "C"
        GloClvPlazaAux = lbidcompania.Text
        FrmPlaza.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If lbidcompania.Text.Length = 0 Then
            MsgBox("Selecciona una plaza")
            Exit Sub
        End If
        opcion = "M"
        GloClvPlazaAux = lbidcompania.Text
        FrmPlaza.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloClvPlazaAux = 0
        FrmPlaza.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class