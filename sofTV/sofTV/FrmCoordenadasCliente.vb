﻿Public Class FrmCoordenadasCliente

    Private Sub FrmCoordenadasCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Button5.ForeColor = Button5.ForeColor
        Button5.BackColor = Button5.BackColor
        Button2.ForeColor = Button5.ForeColor
        Button2.BackColor = Button5.BackColor

        If opcion = "C" Then

            GroupBoxCoordenadas.Enabled = False
        End If

        llenaCoordenadasCliente()
    End Sub

    Private Sub llenaCoordenadasCliente()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, GloContrato)
        BaseII.CreateMyParameter("@latitud", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@longitud", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("SP_DameCoordenadas")

        tbLatDecimal.Text = BaseII.dicoPar("@latitud").ToString()
        tbLongDecimal.Text = BaseII.dicoPar("@longitud").ToString()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Not (IsNumeric(tbLatDecimal.Text) And IsNumeric(tbLongDecimal.Text)) Then
            MsgBox("Captura las coordenadas", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContrato)
            BaseII.CreateMyParameter("@latitud", SqlDbType.VarChar, tbLatDecimal.Text, 50)
            BaseII.CreateMyParameter("@longitud", SqlDbType.VarChar, tbLongDecimal.Text, 50)
            BaseII.Inserta("SP_GuardaCoordenadas")
            MsgBox("Guardado correctamente")
            bitsist(GloUsuario, GloContrato, "Softv", Me.Name, "Cambio de coordenadas", "", tbLatDecimal.Text + "," + tbLongDecimal.Text, "AG")

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class