﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class FrmImprimirCentralizada
    Public rd As ReportDocument
    Private Sub FrmImprimirCentralizada_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = rd
    End Sub
End Class