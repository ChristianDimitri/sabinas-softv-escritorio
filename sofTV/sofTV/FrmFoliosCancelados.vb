﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Xml
Imports System.Net
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms

Public Class FrmFoliosCancelados

    Private Sub FrmFoliosCancelados2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LenaVendedor()
    End Sub

    Private Sub LenaVendedor()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MUESTRAVENDEDORES_2 " & GloClvUsuario)
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            Me.cmbVendedor.DataSource = dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        'Me.cmbVendedor.Text = ""
    End Sub

    Private Sub LlenaSerie()
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        Dim strSQL As StringBuilder
        Dim conexion As New SqlConnection(MiConexion)
        If IsNumeric(Me.cmbVendedor.SelectedValue) = True Then
            'And Len(Trim(Me.Vendedor.SelectedText)) > 0 
            strSQL = New StringBuilder("EXEC Ultimo_SERIEYFOLIO " & CStr(Me.cmbVendedor.SelectedValue))
        Else
            strSQL = New StringBuilder("EXEC Ultimo_SERIEYFOLIO " & CStr(0))
        End If
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)

        Try
            dataAdapter.Fill(dataTable)
            Me.cmbSerie.DataSource = dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        'Me.cmbSerie.Text = ""
    End Sub

    Private Sub LlenaFolio()
        Dim dataTable As New DataTable
        Dim bindingsource As New BindingSource
        Dim conexion As New SqlConnection(MiConexion)
        If IsNumeric(Me.cmbVendedor.SelectedValue) = True And Len(Me.cmbSerie.SelectedValue) > 0 Then
            Dim strSQL As New StringBuilder("EXEC Folio_Disponible " & CStr(Me.cmbVendedor.SelectedValue) & ", '" & CStr(Me.cmbSerie.SelectedValue) & "'")
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)

            Try
                dataAdapter.Fill(dataTable)
                Me.cmbFolio.DataSource = dataTable
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If
        'Me.cmbFolio.Text = ""
    End Sub

    Private Sub cancelaFolios()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Cancela_Folios", conexion)
        comando.CommandType = CommandType.StoredProcedure
        If IsNumeric(Me.cmbVendedor.SelectedValue) = True Then
            If Len(Me.cmbSerie.SelectedValue) > 0 Then
                If IsNumeric(Me.cmbFolio.SelectedValue) = True Then
                    If tbMotCan.Text = "" Then
                        MsgBox("Introduzca el motivo de cancelación")
                        Exit Sub
                    End If
                    Dim parametro1 As New SqlParameter("@Vendedor", SqlDbType.Int)
                    parametro1.Direction = ParameterDirection.Input
                    parametro1.Value = CInt(Me.cmbVendedor.SelectedValue)
                    comando.Parameters.Add(parametro1)

                    Dim parametro2 As New SqlParameter("@Serie", SqlDbType.VarChar)
                    parametro2.Direction = ParameterDirection.Input
                    parametro2.Value = CStr(Me.cmbSerie.SelectedValue)
                    comando.Parameters.Add(parametro2)

                    Dim parametro3 As New SqlParameter("@Folio", SqlDbType.VarChar)
                    parametro3.Direction = ParameterDirection.Input
                    parametro3.Value = CInt(Me.cmbFolio.SelectedValue)
                    comando.Parameters.Add(parametro3)

                    Dim parametro4 As New SqlParameter("@comentario", SqlDbType.VarChar)
                    parametro4.Direction = ParameterDirection.Input
                    parametro4.Value = tbMotCan.Text
                    comando.Parameters.Add(parametro4)
                    Try
                        conexion.Open()
                        comando.ExecuteNonQuery()
                        If tbRuta.Text.Trim.Length > 0 Then
                            If tbRuta.Text.Contains(".pdf") Or tbRuta.Text.Contains(".PDF") Then
                                Dim fInfo As New FileInfo(tbRuta.Text)
                                Dim numBytes As Long = fInfo.Length
                                Dim fStream As New FileStream(tbRuta.Text, FileMode.Open, FileAccess.Read)
                                Dim br As New BinaryReader(fStream)
                                Dim data As Byte() = br.ReadBytes(CInt(numBytes))
                                br.Close()
                                fStream.Close()
                                BaseII.limpiaParametros()
                                BaseII.CreateMyParameter("@folio", SqlDbType.Int, cmbFolio.SelectedValue)
                                BaseII.CreateMyParameter("@serie", SqlDbType.VarChar, cmbSerie.SelectedValue)
                                BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.BigInt, cmbVendedor.SelectedValue)
                                BaseII.CreateMyParameter("@archivo", SqlDbType.Image, data)
                                BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "pdf")
                                BaseII.Inserta("GuardaEvidenciaCancelacionFolio")
                            Else
                                Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()
                                PictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
                                BaseII.limpiaParametros()
                                BaseII.CreateMyParameter("@folio", SqlDbType.Int, cmbFolio.SelectedValue)
                                BaseII.CreateMyParameter("@serie", SqlDbType.VarChar, cmbSerie.SelectedValue)
                                BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.BigInt, cmbVendedor.SelectedValue)
                                BaseII.CreateMyParameter("@archivo", SqlDbType.Image, ms.GetBuffer())
                                BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "image")
                                BaseII.Inserta("GuardaEvidenciaCancelacionFolio")
                            End If
                        End If
                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                    Finally
                        conexion.Close()
                        conexion.Dispose()
                        MsgBox("Folio Cancelado Correctamente!", MsgBoxStyle.Information)
                    End Try
                Else
                    MsgBox("Seleccione al menos un Folio", MsgBoxStyle.Information)
                End If
            Else
                MsgBox("Seleccione al menos una Serie", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Seleccione al menos un Vendedor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub cmbVendedor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbVendedor.TextChanged
        LlenaSerie()
    End Sub

    Private Sub cmbSerie_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSerie.TextChanged
        LlenaFolio()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.CLOSE()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        cancelaFolios()
        LlenaFolio()
    End Sub

  
    Private Sub btnRuta_Click(sender As Object, e As EventArgs) Handles btnRuta.Click
        Dim ofd As New OpenFileDialog()
        ofd.Title = "Selecciona un archivo."
        ofd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        ofd.FilterIndex = 1
        ofd.RestoreDirectory = True
        If ofd.ShowDialog() = DialogResult.OK Then
            Dim fi As FileInfo = New FileInfo(ofd.FileName)
            If ofd.FileName.Contains(".pdf") Or ofd.FileName.Contains(".PDF") Then
                If fi.Length > 1000000 Then
                    MsgBox("Archivo muy grande, no soportado por el sistema.")
                    Exit Sub
                End If
                tbRuta.Text = ofd.FileName
            Else
                If fi.Length > 1000000 Then
                    MsgBox("Imagen muy grande, no soportada por el sistema.")
                    Exit Sub
                End If
                tbRuta.Text = ofd.FileName
                PictureBox1.Image = Image.FromFile(ofd.FileName)
            End If
            
        End If
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        tbRuta.Text = ""
    End Sub

    Private Sub cmbSerie_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSerie.SelectedIndexChanged
        LlenaFolio()
    End Sub
End Class