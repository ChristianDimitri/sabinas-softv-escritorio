﻿Public Class FrmBonificacion
    Private bndcontrola As Boolean = True
    Private Sub FrmBonificacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try

            'TextBoxImporteFac.Text = 0
            bndcontrola = False
            TextBoxPorImporte.Text = 0
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_queja", SqlDbType.BigInt, gloClave)
            BaseII.CreateMyParameter("@dias", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@comentario", ParameterDirection.Output, SqlDbType.VarChar, 1000)
            BaseII.CreateMyParameter("@clv_factura", ParameterDirection.Output, SqlDbType.VarChar, 15)
            BaseII.CreateMyParameter("@tieneBonificacion", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.CreateMyParameter("@aplicada", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.CreateMyParameter("@fecha", ParameterDirection.Output, SqlDbType.DateTime)
            BaseII.CreateMyParameter("@BndPorMonto", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.CreateMyParameter("@Monto", ParameterDirection.Output, SqlDbType.Float)
            BaseII.CreateMyParameter("@Montofac", ParameterDirection.Output, SqlDbType.Float)
            BaseII.ProcedimientoOutPut("DameBonificacion")
            If BaseII.dicoPar("@tieneBonificacion") = True Then
                tbComentario.Text = BaseII.dicoPar("@comentario").ToString
                If Not BaseII.dicoPar("@clv_factura").Equals("0") Then
                    tbFactura.Text = BaseII.dicoPar("@clv_factura").ToString
                Else
                    tbFactura.Text = ""
                End If
                TextBoxImporteFac.Text = BaseII.dicoPar("@Montofac").ToString
                cbAplicada.Checked = BaseII.dicoPar("@aplicada")
                tbFecha.Text = BaseII.dicoPar("@fecha")
                btnEliminar.Enabled = Not BaseII.dicoPar("@aplicada")
                If BaseII.dicoPar("@BndPorMonto") = False Then
                    RadioButton1.Checked = True
                    RadioButton2.Checked = False
                    TextBoxPorImporte.Text = 0
                Else
                    RadioButton2.Checked = True
                    RadioButton1.Checked = False
                    TextBoxPorImporte.Text = BaseII.dicoPar("@Monto").ToString

                End If
                bndcontrola = True
                UDDias.Value = BaseII.dicoPar("@dias")

            Else
                tbComentario.Text = ""
                UDDias.Value = 1
                cbAplicada.Checked = False
                tbFactura.Text = ""
                tbFecha.Text = ""
                'TextBoxImporteFac.Text = 0
                TextBoxPorImporte.Text = 0
            End If
            bndcontrola = True
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        If OrigenBonificación = 2 Then
            tbComentario.Enabled = False
            UDDias.Enabled = False
            btnGuardar.Enabled = False
            btnEliminar.Enabled = False
            TextBoxPorImporte.Enabled = False
        Else
            calcularCantidad()
            tbComentario.Enabled = True
            UDDias.Enabled = True
            btnGuardar.Enabled = True
            TextBoxPorImporte.Enabled = True
        End If
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try

        
        If RadioButton2.Checked = True Then
            If IsNumeric(TextBoxPorImporte.Text) = False Then TextBoxPorImporte.Text = 0
                If CDec(TextBoxPorImporte.Text) > CDec(TextBoxImporteFac.Text) Then
                    MsgBox("La cantidad es mayor al monto que se le esta cobrando ", vbInformation)
                    Exit Sub
                End If
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_queja", SqlDbType.BigInt, gloClave)
        BaseII.CreateMyParameter("@dias", SqlDbType.Int, UDDias.Value)
        BaseII.CreateMyParameter("@comentario", SqlDbType.VarChar, tbComentario.Text)
        BaseII.CreateMyParameter("@clave", SqlDbType.Int, GloClvUsuario)
        If RadioButton2.Checked = True Then
            BaseII.CreateMyParameter("@BndPorMonto", SqlDbType.Bit, True)
        Else
            BaseII.CreateMyParameter("@BndPorMonto", SqlDbType.Bit, False)
        End If
        If IsNumeric(TextBoxPorImporte.Text) = False Then TextBoxPorImporte.Text = 0
        If IsNumeric(TextBoxImporteFac.Text) = False Then TextBoxImporteFac.Text = 0
        BaseII.CreateMyParameter("@Monto", SqlDbType.Decimal, TextBoxPorImporte.Text)
        BaseII.CreateMyParameter("@Montofac", SqlDbType.Decimal, TextBoxImporteFac.Text)
        BaseII.Inserta("InsertaBonificacion")
        MsgBox("Bonificación guardada con éxito.")
            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_queja", SqlDbType.BigInt, gloClave)
        BaseII.CreateMyParameter("@clave", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("EliminaBonificacion")
        MsgBox("Bonificación eliminada con éxito.")
        Me.Close()
    End Sub

    Private Sub calcularCantidad()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvqueja", SqlDbType.BigInt, gloClave)
            BaseII.CreateMyParameter("@cantidad", ParameterDirection.Output, SqlDbType.Decimal)
            If RadioButton1.Checked = True Then
                BaseII.CreateMyParameter("@BndPorMonto", SqlDbType.Bit, 0)
            Else
                BaseII.CreateMyParameter("@BndPorMonto", SqlDbType.Bit, 1)
            End If
            BaseII.ProcedimientoOutPut("sp_PrecioDiasBonificacion")


            TextBoxImporteFac.Text = Format(BaseII.dicoPar("@cantidad"), "##,##0.00")
            tbCantidad.Text = Format((((BaseII.dicoPar("@cantidad") / 30) * UDDias.Value)), "##,##0.00")
            'Dim s As String = (((BaseII.dicoPar("@cantidad") / 30) * UDDias.Value)).ToString
            'tbCantidad.Text = s.Substring(0, s.IndexOf(".")) + s.Substring(s.IndexOf("."), s.IndexOf(".") + 2)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub UDDias_ValueChanged(sender As Object, e As EventArgs) Handles UDDias.ValueChanged
        If IsNumeric(UDDias.Value) Then
            calcularCantidad()
        End If

    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged

        TextBoxPorImporte.Text = 0
        Panel2.Visible = False
        Panel1.Visible = True
        If bndcontrola = True And RadioButton1.Checked = True Then calcularCantidad()
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        'UDDias.Value = 0
        ' If TextBoxImporteFac.Text = 0 Then calcularCantidad()
        Panel1.Visible = False
        Panel2.Visible = True
        If bndcontrola = True And RadioButton2.Checked = True Then calcularCantidad()
    End Sub
End Class