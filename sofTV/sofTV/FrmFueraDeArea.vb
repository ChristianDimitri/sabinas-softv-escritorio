﻿Imports System.Data.SqlClient
Public Class FrmFueraDeArea

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        GloClv_TipSer = 10101
        FrmSelCliente.ShowDialog()
        If GLOCONTRATOSEL > 0 Then
            ContratoCompaniaTextBox.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
        End If
    End Sub

    Private Sub ContratoCompaniaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoCompaniaTextBox.TextChanged
        Try
            Dim array() = ContratoCompaniaTextBox.Text.Split("-")
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            Dim comando As New SqlCommand()
            comando.Connection = conexion
            comando.CommandText = "select contrato from Rel_Contratos_Companias where ContratoCompania=" + array(0).ToString + " and IdCompania=" + array(1).ToString
            ContratoTextBox.Text = comando.ExecuteScalar().ToString()
            conexion.Close()
        Catch ex As Exception
            ContratoTextBox.Text = ""
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ContratoTextBox.Text = "" Then
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, ContratoTextBox.Text)
            BaseII.CreateMyParameter("@clvusuario", SqlDbType.Int, GloClvUsuario)
            BaseII.CreateMyParameter("@clv_tipser", SqlDbType.Int, ComboBoxTipSer.SelectedValue)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.ProcedimientoOutPut("SPFueraDeArea")
            If BaseII.dicoPar("@error") = "Todo bien" Then
                MsgBox("Contrato " + ContratoCompaniaTextBox.Text + " procesado por Fuera de Área correctamente.")
                ContratoTextBox.Text = ""
                ContratoCompaniaTextBox.Text = ""
            Else
                MsgBox(BaseII.dicoPar("@error"))
            End If
            
        Catch ex As Exception

        End Try
    End Sub

    Private Sub FrmFueraDeArea_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        BaseII.limpiaParametros()
        ComboBoxTipSer.DataSource = BaseII.ConsultaDT("MuestraTipSerPrincipal_SER")
    End Sub
End Class