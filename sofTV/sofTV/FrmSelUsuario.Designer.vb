<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ConUsuariosProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConUsuariosProTableAdapter = New sofTV.DataSetEric2TableAdapters.ConUsuariosProTableAdapter()
        Me.NombreListBox = New System.Windows.Forms.ListBox()
        Me.ConUsuariosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConUsuariosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConUsuariosTmpTableAdapter()
        Me.NombreListBox1 = New System.Windows.Forms.ListBox()
        Me.InsertarUsuarioTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarUsuarioTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarUsuarioTmpTableAdapter()
        Me.BorrarUsuarioTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarUsuarioTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.BorrarUsuarioTmpTableAdapter()
        Me.Dame_clv_session_ReportesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_session_ReportesTableAdapter = New sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConUsuariosProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConUsuariosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarUsuarioTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarUsuarioTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConUsuariosProBindingSource
        '
        Me.ConUsuariosProBindingSource.DataMember = "ConUsuariosPro"
        Me.ConUsuariosProBindingSource.DataSource = Me.DataSetEric2
        '
        'ConUsuariosProTableAdapter
        '
        Me.ConUsuariosProTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox
        '
        Me.NombreListBox.DataSource = Me.ConUsuariosProBindingSource
        Me.NombreListBox.DisplayMember = "Nombre"
        Me.NombreListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreListBox.FormattingEnabled = True
        Me.NombreListBox.ItemHeight = 18
        Me.NombreListBox.Location = New System.Drawing.Point(48, 52)
        Me.NombreListBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreListBox.Name = "NombreListBox"
        Me.NombreListBox.Size = New System.Drawing.Size(367, 382)
        Me.NombreListBox.TabIndex = 3
        Me.NombreListBox.ValueMember = "Clave"
        '
        'ConUsuariosTmpBindingSource
        '
        Me.ConUsuariosTmpBindingSource.DataMember = "ConUsuariosTmp"
        Me.ConUsuariosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConUsuariosTmpTableAdapter
        '
        Me.ConUsuariosTmpTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox1
        '
        Me.NombreListBox1.DataSource = Me.ConUsuariosTmpBindingSource
        Me.NombreListBox1.DisplayMember = "Nombre"
        Me.NombreListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreListBox1.FormattingEnabled = True
        Me.NombreListBox1.ItemHeight = 18
        Me.NombreListBox1.Location = New System.Drawing.Point(595, 52)
        Me.NombreListBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreListBox1.Name = "NombreListBox1"
        Me.NombreListBox1.Size = New System.Drawing.Size(367, 382)
        Me.NombreListBox1.TabIndex = 6
        Me.NombreListBox1.ValueMember = "Clave"
        '
        'InsertarUsuarioTmpBindingSource
        '
        Me.InsertarUsuarioTmpBindingSource.DataMember = "InsertarUsuarioTmp"
        Me.InsertarUsuarioTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarUsuarioTmpTableAdapter
        '
        Me.InsertarUsuarioTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarUsuarioTmpBindingSource
        '
        Me.BorrarUsuarioTmpBindingSource.DataMember = "BorrarUsuarioTmp"
        Me.BorrarUsuarioTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'BorrarUsuarioTmpTableAdapter
        '
        Me.BorrarUsuarioTmpTableAdapter.ClearBeforeFill = True
        '
        'Dame_clv_session_ReportesBindingSource
        '
        Me.Dame_clv_session_ReportesBindingSource.DataMember = "Dame_clv_session_Reportes"
        Me.Dame_clv_session_ReportesBindingSource.DataSource = Me.DataSetEric2
        '
        'Dame_clv_session_ReportesTableAdapter
        '
        Me.Dame_clv_session_ReportesTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(459, 119)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 31)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(459, 158)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(85, 31)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(459, 309)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(85, 31)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(459, 347)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(85, 31)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(659, 489)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 11
        Me.Button5.Text = "&ACEPTAR"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(848, 489)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 44)
        Me.Button6.TabIndex = 12
        Me.Button6.Text = "&SALIR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'FrmSelUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1045, 548)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.NombreListBox1)
        Me.Controls.Add(Me.NombreListBox)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelUsuario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Usuario"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConUsuariosProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConUsuariosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarUsuarioTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarUsuarioTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConUsuariosProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConUsuariosProTableAdapter As sofTV.DataSetEric2TableAdapters.ConUsuariosProTableAdapter
    Friend WithEvents NombreListBox As System.Windows.Forms.ListBox
    Friend WithEvents ConUsuariosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConUsuariosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConUsuariosTmpTableAdapter
    Friend WithEvents NombreListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents InsertarUsuarioTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarUsuarioTmpTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarUsuarioTmpTableAdapter
    Friend WithEvents BorrarUsuarioTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarUsuarioTmpTableAdapter As sofTV.DataSetEric2TableAdapters.BorrarUsuarioTmpTableAdapter
    Friend WithEvents Dame_clv_session_ReportesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_session_ReportesTableAdapter As sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
End Class
