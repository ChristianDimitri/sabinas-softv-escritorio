<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmServicioPPE2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim AplicanComLabel As System.Windows.Forms.Label
        Dim Genera_OrdenLabel As System.Windows.Forms.Label
        Dim ClasificacionLabel As System.Windows.Forms.Label
        Dim HitsLabel As System.Windows.Forms.Label
        Dim ImagenLabel As System.Windows.Forms.Label
        Dim Clv_TxtCNRLabel As System.Windows.Forms.Label
        Dim PrecioLabel As System.Windows.Forms.Label
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim DescripcionLabel1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmServicioPPE2))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Clv_PrograLabel1 = New System.Windows.Forms.Label()
        Me.MuestraProgramacionPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEricPPE = New sofTV.DataSetEricPPE()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.MuestraProgramacionPPEDataGridView = New System.Windows.Forms.DataGridView()
        Me.ClvPrograDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvTxtDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.AplicanComCheckBox = New System.Windows.Forms.CheckBox()
        Me.Genera_OrdenCheckBox = New System.Windows.Forms.CheckBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Clv_TxtCNRTextBox = New System.Windows.Forms.TextBox()
        Me.ConRElPPECNRBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ClasificacionTextBox = New System.Windows.Forms.TextBox()
        Me.ConServiciosPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.HitsTextBox = New System.Windows.Forms.TextBox()
        Me.PrecioTextBox = New System.Windows.Forms.TextBox()
        Me.CONSERVICIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ImagenPictureBox = New System.Windows.Forms.PictureBox()
        Me.Clv_TxtTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_PPETextBox = New System.Windows.Forms.TextBox()
        Me.DameClv_PPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox()
        Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONRel_Trabajos_NoCobroMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sale_en_CarteraCheckBox = New System.Windows.Forms.CheckBox()
        Me.Es_PrincipalCheckBox = New System.Windows.Forms.CheckBox()
        Me.CONSERVICIOSTableAdapter = New sofTV.DataSetEricPPETableAdapters.CONSERVICIOSTableAdapter()
        Me.BORRel_Trabajos_NoCobroMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORRel_Trabajos_NoCobroMensualTableAdapter = New sofTV.DataSetEricPPETableAdapters.BORRel_Trabajos_NoCobroMensualTableAdapter()
        Me.GUARDARel_Trabajos_NoCobroMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDARel_Trabajos_NoCobroMensualTableAdapter = New sofTV.DataSetEricPPETableAdapters.GUARDARel_Trabajos_NoCobroMensualTableAdapter()
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter = New sofTV.DataSetEricPPETableAdapters.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter()
        Me.CONRel_Trabajos_NoCobroMensualTableAdapter = New sofTV.DataSetEricPPETableAdapters.CONRel_Trabajos_NoCobroMensualTableAdapter()
        Me.ConServiciosPPETableAdapter = New sofTV.DataSetEricPPETableAdapters.ConServiciosPPETableAdapter()
        Me.ConProgramacionPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConProgramacionPPETableAdapter = New sofTV.DataSetEricPPETableAdapters.ConProgramacionPPETableAdapter()
        Me.MuestraProgramacionPPETableAdapter = New sofTV.DataSetEricPPETableAdapters.MuestraProgramacionPPETableAdapter()
        Me.BorraFechasProgPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraFechasProgPPETableAdapter = New sofTV.DataSetEricPPETableAdapters.BorraFechasProgPPETableAdapter()
        Me.ValidaBorProgPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaBorProgPPETableAdapter = New sofTV.DataSetEricPPETableAdapters.ValidaBorProgPPETableAdapter()
        Me.InsertaFechasProgPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaFechasProgPPETableAdapter = New sofTV.DataSetEricPPETableAdapters.InsertaFechasProgPPETableAdapter()
        Me.ValidaNueProgPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaNueProgPPETableAdapter = New sofTV.DataSetEricPPETableAdapters.ValidaNueProgPPETableAdapter()
        Me.DameClv_PPETableAdapter = New sofTV.DataSetEricPPETableAdapters.DameClv_PPETableAdapter()
        Me.ConRElPPECNRTableAdapter = New sofTV.DataSetEricPPETableAdapters.ConRElPPECNRTableAdapter()
        Me.Valida_borra_servicioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_borra_servicioTableAdapter = New sofTV.DataSetEricPPETableAdapters.Valida_borra_servicioTableAdapter()
        Me.CONSERVICIOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONSERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        DescripcionLabel = New System.Windows.Forms.Label()
        AplicanComLabel = New System.Windows.Forms.Label()
        Genera_OrdenLabel = New System.Windows.Forms.Label()
        ClasificacionLabel = New System.Windows.Forms.Label()
        HitsLabel = New System.Windows.Forms.Label()
        ImagenLabel = New System.Windows.Forms.Label()
        Clv_TxtCNRLabel = New System.Windows.Forms.Label()
        PrecioLabel = New System.Windows.Forms.Label()
        Clv_TxtLabel = New System.Windows.Forms.Label()
        DescripcionLabel1 = New System.Windows.Forms.Label()
        CType(Me.MuestraProgramacionPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEricPPE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.MuestraProgramacionPPEDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ConRElPPECNRBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConServiciosPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImagenPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_PPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORRel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDARel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConProgramacionPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorraFechasProgPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaBorProgPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaFechasProgPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaNueProgPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_borra_servicioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONSERVICIOSBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(29, 63)
        DescripcionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(108, 18)
        DescripcionLabel.TabIndex = 6
        DescripcionLabel.Text = "Descripción :"
        '
        'AplicanComLabel
        '
        AplicanComLabel.AutoSize = True
        AplicanComLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        AplicanComLabel.ForeColor = System.Drawing.Color.Black
        AplicanComLabel.Location = New System.Drawing.Point(20, 15)
        AplicanComLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        AplicanComLabel.Name = "AplicanComLabel"
        AplicanComLabel.Size = New System.Drawing.Size(193, 25)
        AplicanComLabel.TabIndex = 10
        AplicanComLabel.Text = "Aplican Comisión :"
        '
        'Genera_OrdenLabel
        '
        Genera_OrdenLabel.AutoSize = True
        Genera_OrdenLabel.Enabled = False
        Genera_OrdenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Genera_OrdenLabel.ForeColor = System.Drawing.Color.Black
        Genera_OrdenLabel.Location = New System.Drawing.Point(51, 44)
        Genera_OrdenLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Genera_OrdenLabel.Name = "Genera_OrdenLabel"
        Genera_OrdenLabel.Size = New System.Drawing.Size(162, 25)
        Genera_OrdenLabel.TabIndex = 16
        Genera_OrdenLabel.Text = "Genera Orden :"
        Genera_OrdenLabel.Visible = False
        '
        'ClasificacionLabel
        '
        ClasificacionLabel.AutoSize = True
        ClasificacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClasificacionLabel.Location = New System.Drawing.Point(21, 129)
        ClasificacionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ClasificacionLabel.Name = "ClasificacionLabel"
        ClasificacionLabel.Size = New System.Drawing.Size(115, 18)
        ClasificacionLabel.TabIndex = 39
        ClasificacionLabel.Text = "Clasificación :"
        '
        'HitsLabel
        '
        HitsLabel.AutoSize = True
        HitsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        HitsLabel.Location = New System.Drawing.Point(103, 162)
        HitsLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        HitsLabel.Name = "HitsLabel"
        HitsLabel.Size = New System.Drawing.Size(43, 18)
        HitsLabel.TabIndex = 38
        HitsLabel.Text = "Hits:"
        '
        'ImagenLabel
        '
        ImagenLabel.AutoSize = True
        ImagenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImagenLabel.Location = New System.Drawing.Point(540, 95)
        ImagenLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ImagenLabel.Name = "ImagenLabel"
        ImagenLabel.Size = New System.Drawing.Size(67, 18)
        ImagenLabel.TabIndex = 37
        ImagenLabel.Text = "Imagen:"
        '
        'Clv_TxtCNRLabel
        '
        Clv_TxtCNRLabel.AutoSize = True
        Clv_TxtCNRLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtCNRLabel.Location = New System.Drawing.Point(81, 197)
        Clv_TxtCNRLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_TxtCNRLabel.Name = "Clv_TxtCNRLabel"
        Clv_TxtCNRLabel.Size = New System.Drawing.Size(61, 18)
        Clv_TxtCNRLabel.TabIndex = 41
        Clv_TxtCNRLabel.Text = "Canal :"
        '
        'PrecioLabel
        '
        PrecioLabel.AutoSize = True
        PrecioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PrecioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PrecioLabel.Location = New System.Drawing.Point(76, 96)
        PrecioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        PrecioLabel.Name = "PrecioLabel"
        PrecioLabel.Size = New System.Drawing.Size(67, 18)
        PrecioLabel.TabIndex = 14
        PrecioLabel.Text = "Precio :"
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TxtLabel.Location = New System.Drawing.Point(84, 31)
        Clv_TxtLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(60, 18)
        Clv_TxtLabel.TabIndex = 8
        Clv_TxtLabel.Text = "Clave :"
        '
        'DescripcionLabel1
        '
        DescripcionLabel1.AutoSize = True
        DescripcionLabel1.Enabled = False
        DescripcionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel1.Location = New System.Drawing.Point(24, 15)
        DescripcionLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel1.Name = "DescripcionLabel1"
        DescripcionLabel1.Size = New System.Drawing.Size(75, 18)
        DescripcionLabel1.TabIndex = 0
        DescripcionLabel1.Text = "Trabajo :"
        DescripcionLabel1.Visible = False
        '
        'Clv_PrograLabel1
        '
        Me.Clv_PrograLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraProgramacionPPEBindingSource, "Clv_Progra", True))
        Me.Clv_PrograLabel1.Location = New System.Drawing.Point(176, 117)
        Me.Clv_PrograLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Clv_PrograLabel1.Name = "Clv_PrograLabel1"
        Me.Clv_PrograLabel1.Size = New System.Drawing.Size(133, 28)
        Me.Clv_PrograLabel1.TabIndex = 37
        '
        'MuestraProgramacionPPEBindingSource
        '
        Me.MuestraProgramacionPPEBindingSource.DataMember = "MuestraProgramacionPPE"
        Me.MuestraProgramacionPPEBindingSource.DataSource = Me.DataSetEricPPE
        '
        'DataSetEricPPE
        '
        Me.DataSetEricPPE.DataSetName = "DataSetEricPPE"
        Me.DataSetEricPPE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(73, 112)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 20)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "del Día"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(289, 112)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 20)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "al Día"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(77, 135)
        Me.DateTimePicker1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(133, 26)
        Me.DateTimePicker1.TabIndex = 10
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.Color.Black
        Me.RadioButton2.Location = New System.Drawing.Point(293, 41)
        Me.RadioButton2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(211, 24)
        Me.RadioButton2.TabIndex = 9
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Por Rango de Fechas"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.BindingNavigator1)
        Me.Panel4.Controls.Add(Me.MuestraProgramacionPPEDataGridView)
        Me.Panel4.Controls.Add(Me.Clv_PrograLabel1)
        Me.Panel4.Enabled = False
        Me.Panel4.ForeColor = System.Drawing.Color.Black
        Me.Panel4.Location = New System.Drawing.Point(24, 425)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(631, 439)
        Me.Panel4.TabIndex = 42
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.BindingSource = Me.MuestraProgramacionPPEBindingSource
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Me.ToolStripButton1
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(631, 28)
        Me.BindingNavigator1.TabIndex = 38
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton1.Size = New System.Drawing.Size(122, 25)
        Me.ToolStripButton1.Text = "E&LIMINAR"
        '
        'MuestraProgramacionPPEDataGridView
        '
        Me.MuestraProgramacionPPEDataGridView.AllowUserToAddRows = False
        Me.MuestraProgramacionPPEDataGridView.AllowUserToDeleteRows = False
        Me.MuestraProgramacionPPEDataGridView.AutoGenerateColumns = False
        Me.MuestraProgramacionPPEDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MuestraProgramacionPPEDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MuestraProgramacionPPEDataGridView.ColumnHeadersHeight = 25
        Me.MuestraProgramacionPPEDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvPrograDataGridViewTextBoxColumn, Me.ClvTxtDataGridViewTextBoxColumn, Me.FechaDataGridViewTextBoxColumn})
        Me.MuestraProgramacionPPEDataGridView.DataSource = Me.MuestraProgramacionPPEBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MuestraProgramacionPPEDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.MuestraProgramacionPPEDataGridView.Location = New System.Drawing.Point(4, 34)
        Me.MuestraProgramacionPPEDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MuestraProgramacionPPEDataGridView.Name = "MuestraProgramacionPPEDataGridView"
        Me.MuestraProgramacionPPEDataGridView.ReadOnly = True
        Me.MuestraProgramacionPPEDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MuestraProgramacionPPEDataGridView.Size = New System.Drawing.Size(623, 401)
        Me.MuestraProgramacionPPEDataGridView.TabIndex = 29
        Me.MuestraProgramacionPPEDataGridView.TabStop = False
        '
        'ClvPrograDataGridViewTextBoxColumn
        '
        Me.ClvPrograDataGridViewTextBoxColumn.DataPropertyName = "Clv_Progra"
        Me.ClvPrograDataGridViewTextBoxColumn.HeaderText = "Clave de Programación"
        Me.ClvPrograDataGridViewTextBoxColumn.Name = "ClvPrograDataGridViewTextBoxColumn"
        Me.ClvPrograDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvPrograDataGridViewTextBoxColumn.Width = 180
        '
        'ClvTxtDataGridViewTextBoxColumn
        '
        Me.ClvTxtDataGridViewTextBoxColumn.DataPropertyName = "Clv_Txt"
        Me.ClvTxtDataGridViewTextBoxColumn.HeaderText = "Clv_Txt"
        Me.ClvTxtDataGridViewTextBoxColumn.Name = "ClvTxtDataGridViewTextBoxColumn"
        Me.ClvTxtDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvTxtDataGridViewTextBoxColumn.Visible = False
        '
        'FechaDataGridViewTextBoxColumn
        '
        Me.FechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha"
        Me.FechaDataGridViewTextBoxColumn.HeaderText = "Fecha"
        Me.FechaDataGridViewTextBoxColumn.Name = "FechaDataGridViewTextBoxColumn"
        Me.FechaDataGridViewTextBoxColumn.ReadOnly = True
        Me.FechaDataGridViewTextBoxColumn.Width = 200
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(AplicanComLabel)
        Me.Panel3.Controls.Add(Genera_OrdenLabel)
        Me.Panel3.Controls.Add(Me.AplicanComCheckBox)
        Me.Panel3.Controls.Add(Me.Genera_OrdenCheckBox)
        Me.Panel3.Location = New System.Drawing.Point(1012, 75)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(267, 70)
        Me.Panel3.TabIndex = 34
        Me.Panel3.TabStop = True
        '
        'AplicanComCheckBox
        '
        Me.AplicanComCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AplicanComCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AplicanComCheckBox.ForeColor = System.Drawing.Color.Black
        Me.AplicanComCheckBox.Location = New System.Drawing.Point(236, 10)
        Me.AplicanComCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.AplicanComCheckBox.Name = "AplicanComCheckBox"
        Me.AplicanComCheckBox.Size = New System.Drawing.Size(20, 30)
        Me.AplicanComCheckBox.TabIndex = 5
        '
        'Genera_OrdenCheckBox
        '
        Me.Genera_OrdenCheckBox.Enabled = False
        Me.Genera_OrdenCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Genera_OrdenCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Genera_OrdenCheckBox.ForeColor = System.Drawing.Color.Black
        Me.Genera_OrdenCheckBox.Location = New System.Drawing.Point(236, 39)
        Me.Genera_OrdenCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Genera_OrdenCheckBox.Name = "Genera_OrdenCheckBox"
        Me.Genera_OrdenCheckBox.Size = New System.Drawing.Size(20, 30)
        Me.Genera_OrdenCheckBox.TabIndex = 6
        Me.Genera_OrdenCheckBox.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.Black
        Me.TextBox1.Location = New System.Drawing.Point(948, 44)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(331, 23)
        Me.TextBox1.TabIndex = 41
        Me.TextBox1.TabStop = False
        Me.TextBox1.Text = "Es Servicio :"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Enabled = False
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(293, 135)
        Me.DateTimePicker2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(133, 26)
        Me.DateTimePicker2.TabIndex = 11
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.SystemColors.Control
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(1135, 837)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(181, 44)
        Me.Button4.TabIndex = 38
        Me.Button4.Text = "&SALIR"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(340, 208)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 44)
        Me.Button2.TabIndex = 12
        Me.Button2.Text = "&AGREGAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(720, 492)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(537, 278)
        Me.GroupBox1.TabIndex = 36
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Días de la Programación"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.Black
        Me.RadioButton1.Location = New System.Drawing.Point(77, 41)
        Me.RadioButton1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(116, 24)
        Me.RadioButton1.TabIndex = 8
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Por Fecha"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Clv_TxtCNRLabel)
        Me.Panel1.Controls.Add(Me.Clv_TxtCNRTextBox)
        Me.Panel1.Controls.Add(ClasificacionLabel)
        Me.Panel1.Controls.Add(Me.ClasificacionTextBox)
        Me.Panel1.Controls.Add(HitsLabel)
        Me.Panel1.Controls.Add(Me.HitsTextBox)
        Me.Panel1.Controls.Add(ImagenLabel)
        Me.Panel1.Controls.Add(Me.PrecioTextBox)
        Me.Panel1.Controls.Add(Me.ImagenPictureBox)
        Me.Panel1.Controls.Add(PrecioLabel)
        Me.Panel1.Controls.Add(Me.Clv_TxtTextBox)
        Me.Panel1.Controls.Add(Clv_TxtLabel)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Controls.Add(Me.Clv_PPETextBox)
        Me.Panel1.Location = New System.Drawing.Point(24, 65)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(791, 359)
        Me.Panel1.TabIndex = 31
        Me.Panel1.TabStop = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(408, 132)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(99, 33)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "&Imagen"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Clv_TxtCNRTextBox
        '
        Me.Clv_TxtCNRTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_TxtCNRTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_TxtCNRTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRElPPECNRBindingSource, "Clv_TxtCNR", True))
        Me.Clv_TxtCNRTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtCNRTextBox.Location = New System.Drawing.Point(159, 188)
        Me.Clv_TxtCNRTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TxtCNRTextBox.Name = "Clv_TxtCNRTextBox"
        Me.Clv_TxtCNRTextBox.Size = New System.Drawing.Size(133, 26)
        Me.Clv_TxtCNRTextBox.TabIndex = 4
        '
        'ConRElPPECNRBindingSource
        '
        Me.ConRElPPECNRBindingSource.DataMember = "ConRElPPECNR"
        Me.ConRElPPECNRBindingSource.DataSource = Me.DataSetEricPPE
        '
        'ClasificacionTextBox
        '
        Me.ClasificacionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ClasificacionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ClasificacionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosPPEBindingSource, "Clasificacion", True))
        Me.ClasificacionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClasificacionTextBox.Location = New System.Drawing.Point(159, 121)
        Me.ClasificacionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ClasificacionTextBox.Name = "ClasificacionTextBox"
        Me.ClasificacionTextBox.Size = New System.Drawing.Size(133, 26)
        Me.ClasificacionTextBox.TabIndex = 3
        '
        'ConServiciosPPEBindingSource
        '
        Me.ConServiciosPPEBindingSource.DataMember = "ConServiciosPPE"
        Me.ConServiciosPPEBindingSource.DataSource = Me.DataSetEricPPE
        '
        'HitsTextBox
        '
        Me.HitsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.HitsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosPPEBindingSource, "Hits", True))
        Me.HitsTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HitsTextBox.Location = New System.Drawing.Point(159, 154)
        Me.HitsTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.HitsTextBox.Name = "HitsTextBox"
        Me.HitsTextBox.ReadOnly = True
        Me.HitsTextBox.Size = New System.Drawing.Size(133, 26)
        Me.HitsTextBox.TabIndex = 39
        Me.HitsTextBox.TabStop = False
        '
        'PrecioTextBox
        '
        Me.PrecioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PrecioTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.PrecioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Precio", True))
        Me.PrecioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrecioTextBox.Location = New System.Drawing.Point(159, 87)
        Me.PrecioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PrecioTextBox.Name = "PrecioTextBox"
        Me.PrecioTextBox.Size = New System.Drawing.Size(198, 26)
        Me.PrecioTextBox.TabIndex = 2
        '
        'CONSERVICIOSBindingSource
        '
        Me.CONSERVICIOSBindingSource.DataMember = "CONSERVICIOS"
        Me.CONSERVICIOSBindingSource.DataSource = Me.DataSetEricPPE
        '
        'ImagenPictureBox
        '
        Me.ImagenPictureBox.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.ConServiciosPPEBindingSource, "Imagen", True))
        Me.ImagenPictureBox.Location = New System.Drawing.Point(544, 123)
        Me.ImagenPictureBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ImagenPictureBox.Name = "ImagenPictureBox"
        Me.ImagenPictureBox.Size = New System.Drawing.Size(171, 212)
        Me.ImagenPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ImagenPictureBox.TabIndex = 38
        Me.ImagenPictureBox.TabStop = False
        '
        'Clv_TxtTextBox
        '
        Me.Clv_TxtTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_TxtTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_TxtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Clv_Txt", True))
        Me.Clv_TxtTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtTextBox.Location = New System.Drawing.Point(159, 22)
        Me.Clv_TxtTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TxtTextBox.MaxLength = 6
        Me.Clv_TxtTextBox.Name = "Clv_TxtTextBox"
        Me.Clv_TxtTextBox.Size = New System.Drawing.Size(138, 26)
        Me.Clv_TxtTextBox.TabIndex = 0
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(159, 54)
        Me.DescripcionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionTextBox.MaxLength = 150
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(595, 26)
        Me.DescripcionTextBox.TabIndex = 1
        '
        'Clv_PPETextBox
        '
        Me.Clv_PPETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClv_PPEBindingSource, "Clv_PPE", True))
        Me.Clv_PPETextBox.Location = New System.Drawing.Point(447, 137)
        Me.Clv_PPETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_PPETextBox.Name = "Clv_PPETextBox"
        Me.Clv_PPETextBox.ReadOnly = True
        Me.Clv_PPETextBox.Size = New System.Drawing.Size(12, 22)
        Me.Clv_PPETextBox.TabIndex = 42
        Me.Clv_PPETextBox.TabStop = False
        '
        'DameClv_PPEBindingSource
        '
        Me.DameClv_PPEBindingSource.DataMember = "DameClv_PPE"
        Me.DameClv_PPEBindingSource.DataSource = Me.DataSetEricPPE
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(1244, 852)
        Me.Clv_TipSerTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(12, 26)
        Me.Clv_TipSerTextBox.TabIndex = 33
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(1219, 852)
        Me.Clv_ServicioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.ReadOnly = True
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(12, 26)
        Me.Clv_ServicioTextBox.TabIndex = 32
        Me.Clv_ServicioTextBox.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(DescripcionLabel1)
        Me.Panel2.Controls.Add(Me.DescripcionComboBox)
        Me.Panel2.Enabled = False
        Me.Panel2.Location = New System.Drawing.Point(823, 160)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(516, 91)
        Me.Panel2.TabIndex = 35
        Me.Panel2.TabStop = True
        Me.Panel2.Visible = False
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource, "Descripcion", True))
        Me.DescripcionComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONRel_Trabajos_NoCobroMensualBindingSource, "Clv_Trabajo", True))
        Me.DescripcionComboBox.DisplayMember = "Clv_Trabajo"
        Me.DescripcionComboBox.Enabled = False
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(27, 37)
        Me.DescripcionComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(465, 28)
        Me.DescripcionComboBox.TabIndex = 7
        Me.DescripcionComboBox.ValueMember = "Clv_Trabajo"
        Me.DescripcionComboBox.Visible = False
        '
        'MUESTRATRABAJOS_NOCOBROMENSUALBindingSource
        '
        Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource.DataMember = "MUESTRATRABAJOS_NOCOBROMENSUAL"
        Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource.DataSource = Me.DataSetEricPPE
        '
        'CONRel_Trabajos_NoCobroMensualBindingSource
        '
        Me.CONRel_Trabajos_NoCobroMensualBindingSource.DataMember = "CONRel_Trabajos_NoCobroMensual"
        Me.CONRel_Trabajos_NoCobroMensualBindingSource.DataSource = Me.DataSetEricPPE
        '
        'Sale_en_CarteraCheckBox
        '
        Me.Sale_en_CarteraCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSERVICIOSBindingSource, "Sale_en_Cartera", True))
        Me.Sale_en_CarteraCheckBox.Enabled = False
        Me.Sale_en_CarteraCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Sale_en_CarteraCheckBox.Location = New System.Drawing.Point(1265, 852)
        Me.Sale_en_CarteraCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Sale_en_CarteraCheckBox.Name = "Sale_en_CarteraCheckBox"
        Me.Sale_en_CarteraCheckBox.Size = New System.Drawing.Size(16, 30)
        Me.Sale_en_CarteraCheckBox.TabIndex = 39
        Me.Sale_en_CarteraCheckBox.TabStop = False
        '
        'Es_PrincipalCheckBox
        '
        Me.Es_PrincipalCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSERVICIOSBindingSource, "Es_Principal", True))
        Me.Es_PrincipalCheckBox.Enabled = False
        Me.Es_PrincipalCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Es_PrincipalCheckBox.Location = New System.Drawing.Point(1197, 852)
        Me.Es_PrincipalCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Es_PrincipalCheckBox.Name = "Es_PrincipalCheckBox"
        Me.Es_PrincipalCheckBox.Size = New System.Drawing.Size(13, 30)
        Me.Es_PrincipalCheckBox.TabIndex = 40
        Me.Es_PrincipalCheckBox.TabStop = False
        '
        'CONSERVICIOSTableAdapter
        '
        Me.CONSERVICIOSTableAdapter.ClearBeforeFill = True
        '
        'BORRel_Trabajos_NoCobroMensualBindingSource
        '
        Me.BORRel_Trabajos_NoCobroMensualBindingSource.DataMember = "BORRel_Trabajos_NoCobroMensual"
        Me.BORRel_Trabajos_NoCobroMensualBindingSource.DataSource = Me.DataSetEricPPE
        '
        'BORRel_Trabajos_NoCobroMensualTableAdapter
        '
        Me.BORRel_Trabajos_NoCobroMensualTableAdapter.ClearBeforeFill = True
        '
        'GUARDARel_Trabajos_NoCobroMensualBindingSource
        '
        Me.GUARDARel_Trabajos_NoCobroMensualBindingSource.DataMember = "GUARDARel_Trabajos_NoCobroMensual"
        Me.GUARDARel_Trabajos_NoCobroMensualBindingSource.DataSource = Me.DataSetEricPPE
        '
        'GUARDARel_Trabajos_NoCobroMensualTableAdapter
        '
        Me.GUARDARel_Trabajos_NoCobroMensualTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter
        '
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.ClearBeforeFill = True
        '
        'CONRel_Trabajos_NoCobroMensualTableAdapter
        '
        Me.CONRel_Trabajos_NoCobroMensualTableAdapter.ClearBeforeFill = True
        '
        'ConServiciosPPETableAdapter
        '
        Me.ConServiciosPPETableAdapter.ClearBeforeFill = True
        '
        'ConProgramacionPPEBindingSource
        '
        Me.ConProgramacionPPEBindingSource.DataMember = "ConProgramacionPPE"
        Me.ConProgramacionPPEBindingSource.DataSource = Me.DataSetEricPPE
        '
        'ConProgramacionPPETableAdapter
        '
        Me.ConProgramacionPPETableAdapter.ClearBeforeFill = True
        '
        'MuestraProgramacionPPETableAdapter
        '
        Me.MuestraProgramacionPPETableAdapter.ClearBeforeFill = True
        '
        'BorraFechasProgPPEBindingSource
        '
        Me.BorraFechasProgPPEBindingSource.DataMember = "BorraFechasProgPPE"
        Me.BorraFechasProgPPEBindingSource.DataSource = Me.DataSetEricPPE
        '
        'BorraFechasProgPPETableAdapter
        '
        Me.BorraFechasProgPPETableAdapter.ClearBeforeFill = True
        '
        'ValidaBorProgPPEBindingSource
        '
        Me.ValidaBorProgPPEBindingSource.DataMember = "ValidaBorProgPPE"
        Me.ValidaBorProgPPEBindingSource.DataSource = Me.DataSetEricPPE
        '
        'ValidaBorProgPPETableAdapter
        '
        Me.ValidaBorProgPPETableAdapter.ClearBeforeFill = True
        '
        'InsertaFechasProgPPEBindingSource
        '
        Me.InsertaFechasProgPPEBindingSource.DataMember = "InsertaFechasProgPPE"
        Me.InsertaFechasProgPPEBindingSource.DataSource = Me.DataSetEricPPE
        '
        'InsertaFechasProgPPETableAdapter
        '
        Me.InsertaFechasProgPPETableAdapter.ClearBeforeFill = True
        '
        'ValidaNueProgPPEBindingSource
        '
        Me.ValidaNueProgPPEBindingSource.DataMember = "ValidaNueProgPPE"
        Me.ValidaNueProgPPEBindingSource.DataSource = Me.DataSetEricPPE
        '
        'ValidaNueProgPPETableAdapter
        '
        Me.ValidaNueProgPPETableAdapter.ClearBeforeFill = True
        '
        'DameClv_PPETableAdapter
        '
        Me.DameClv_PPETableAdapter.ClearBeforeFill = True
        '
        'ConRElPPECNRTableAdapter
        '
        Me.ConRElPPECNRTableAdapter.ClearBeforeFill = True
        '
        'Valida_borra_servicioBindingSource
        '
        Me.Valida_borra_servicioBindingSource.DataMember = "Valida_borra_servicio"
        Me.Valida_borra_servicioBindingSource.DataSource = Me.DataSetEricPPE
        '
        'Valida_borra_servicioTableAdapter
        '
        Me.Valida_borra_servicioTableAdapter.ClearBeforeFill = True
        '
        'CONSERVICIOSBindingNavigator
        '
        Me.CONSERVICIOSBindingNavigator.AddNewItem = Nothing
        Me.CONSERVICIOSBindingNavigator.BindingSource = Me.CONSERVICIOSBindingSource
        Me.CONSERVICIOSBindingNavigator.CountItem = Nothing
        Me.CONSERVICIOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONSERVICIOSBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSERVICIOSBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONSERVICIOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CONSERVICIOSBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.CONSERVICIOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONSERVICIOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MoveLastItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MoveNextItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONSERVICIOSBindingNavigator.Name = "CONSERVICIOSBindingNavigator"
        Me.CONSERVICIOSBindingNavigator.PositionItem = Nothing
        Me.CONSERVICIOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONSERVICIOSBindingNavigator.Size = New System.Drawing.Size(1355, 28)
        Me.CONSERVICIOSBindingNavigator.TabIndex = 43
        Me.CONSERVICIOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(122, 25)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'CONSERVICIOSBindingNavigatorSaveItem
        '
        Me.CONSERVICIOSBindingNavigatorSaveItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CONSERVICIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSERVICIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Name = "CONSERVICIOSBindingNavigatorSaveItem"
        Me.CONSERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 25)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'FrmServicioPPE2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1355, 903)
        Me.Controls.Add(Me.CONSERVICIOSBindingNavigator)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Sale_en_CarteraCheckBox)
        Me.Controls.Add(Me.Es_PrincipalCheckBox)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmServicioPPE2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Servicio Pago Por Evento"
        CType(Me.MuestraProgramacionPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEricPPE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.MuestraProgramacionPPEDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConRElPPECNRBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConServiciosPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImagenPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_PPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORRel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDARel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConProgramacionPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorraFechasProgPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaBorProgPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaFechasProgPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaNueProgPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_borra_servicioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONSERVICIOSBindingNavigator.ResumeLayout(False)
        Me.CONSERVICIOSBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Clv_PrograLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents MuestraProgramacionPPEDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents AplicanComCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Genera_OrdenCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Clv_TxtCNRTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClasificacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HitsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PrecioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImagenPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents Clv_TxtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_PPETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Sale_en_CarteraCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Es_PrincipalCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents DataSetEricPPE As sofTV.DataSetEricPPE
    Friend WithEvents CONSERVICIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSERVICIOSTableAdapter As sofTV.DataSetEricPPETableAdapters.CONSERVICIOSTableAdapter
    Friend WithEvents BORRel_Trabajos_NoCobroMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORRel_Trabajos_NoCobroMensualTableAdapter As sofTV.DataSetEricPPETableAdapters.BORRel_Trabajos_NoCobroMensualTableAdapter
    Friend WithEvents GUARDARel_Trabajos_NoCobroMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDARel_Trabajos_NoCobroMensualTableAdapter As sofTV.DataSetEricPPETableAdapters.GUARDARel_Trabajos_NoCobroMensualTableAdapter
    Friend WithEvents MUESTRATRABAJOS_NOCOBROMENSUALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter As sofTV.DataSetEricPPETableAdapters.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter
    Friend WithEvents CONRel_Trabajos_NoCobroMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRel_Trabajos_NoCobroMensualTableAdapter As sofTV.DataSetEricPPETableAdapters.CONRel_Trabajos_NoCobroMensualTableAdapter
    Friend WithEvents ConServiciosPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConServiciosPPETableAdapter As sofTV.DataSetEricPPETableAdapters.ConServiciosPPETableAdapter
    Friend WithEvents ConProgramacionPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConProgramacionPPETableAdapter As sofTV.DataSetEricPPETableAdapters.ConProgramacionPPETableAdapter
    Friend WithEvents MuestraProgramacionPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraProgramacionPPETableAdapter As sofTV.DataSetEricPPETableAdapters.MuestraProgramacionPPETableAdapter
    Friend WithEvents BorraFechasProgPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraFechasProgPPETableAdapter As sofTV.DataSetEricPPETableAdapters.BorraFechasProgPPETableAdapter
    Friend WithEvents ValidaBorProgPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaBorProgPPETableAdapter As sofTV.DataSetEricPPETableAdapters.ValidaBorProgPPETableAdapter
    Friend WithEvents InsertaFechasProgPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaFechasProgPPETableAdapter As sofTV.DataSetEricPPETableAdapters.InsertaFechasProgPPETableAdapter
    Friend WithEvents ValidaNueProgPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaNueProgPPETableAdapter As sofTV.DataSetEricPPETableAdapters.ValidaNueProgPPETableAdapter
    Friend WithEvents DameClv_PPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_PPETableAdapter As sofTV.DataSetEricPPETableAdapters.DameClv_PPETableAdapter
    Friend WithEvents ConRElPPECNRBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRElPPECNRTableAdapter As sofTV.DataSetEricPPETableAdapters.ConRElPPECNRTableAdapter
    Friend WithEvents Valida_borra_servicioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_borra_servicioTableAdapter As sofTV.DataSetEricPPETableAdapters.Valida_borra_servicioTableAdapter
    Friend WithEvents CONSERVICIOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ClvPrograDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvTxtDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
