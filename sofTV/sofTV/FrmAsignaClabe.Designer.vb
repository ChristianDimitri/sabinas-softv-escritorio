﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAsignaClabe
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label1 As System.Windows.Forms.Label
        Me.cbClabe = New System.Windows.Forms.ComboBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.tbClabe = New System.Windows.Forms.TextBox()
        Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(28, 23)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(97, 15)
        Label1.TabIndex = 46
        Label1.Text = "Cuenta Clabe:"
        '
        'cbClabe
        '
        Me.cbClabe.DisplayMember = "Clabe"
        Me.cbClabe.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbClabe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbClabe.FormattingEnabled = True
        Me.cbClabe.Location = New System.Drawing.Point(31, 50)
        Me.cbClabe.Name = "cbClabe"
        Me.cbClabe.Size = New System.Drawing.Size(264, 23)
        Me.cbClabe.TabIndex = 207
        Me.cbClabe.ValueMember = "Id"
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.Orange
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.btnCancelar.Location = New System.Drawing.Point(182, 96)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(81, 28)
        Me.btnCancelar.TabIndex = 209
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.Orange
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.btnAceptar.Location = New System.Drawing.Point(69, 96)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(81, 28)
        Me.btnAceptar.TabIndex = 208
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'tbClabe
        '
        Me.tbClabe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbClabe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbClabe.Location = New System.Drawing.Point(31, 52)
        Me.tbClabe.MaxLength = 50
        Me.tbClabe.Name = "tbClabe"
        Me.tbClabe.ReadOnly = True
        Me.tbClabe.Size = New System.Drawing.Size(264, 21)
        Me.tbClabe.TabIndex = 210
        '
        'FrmAsignaClabe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(321, 146)
        Me.Controls.Add(Me.tbClabe)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.cbClabe)
        Me.Controls.Add(Label1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(337, 184)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(337, 184)
        Me.Name = "FrmAsignaClabe"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asignación Cuenta Clabe"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbClabe As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents tbClabe As System.Windows.Forms.TextBox
End Class
