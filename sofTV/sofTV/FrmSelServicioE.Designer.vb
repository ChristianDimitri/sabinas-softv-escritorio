<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelServicioE
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Aceptar = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ConServiciosProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConServiciosProTableAdapter = New sofTV.DataSetEric2TableAdapters.ConServiciosProTableAdapter()
        Me.DescripcionListBox = New System.Windows.Forms.ListBox()
        Me.ConServiciosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConServiciosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConServiciosTmpTableAdapter()
        Me.DescripcionListBox1 = New System.Windows.Forms.ListBox()
        Me.InsertarServiciosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarServiciosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarServiciosTmpTableAdapter()
        Me.BorrarServiciosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarServiciosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.BorrarServiciosTmpTableAdapter()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConServiciosProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Aceptar
        '
        Me.Aceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Aceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Aceptar.Location = New System.Drawing.Point(659, 489)
        Me.Aceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Aceptar.Name = "Aceptar"
        Me.Aceptar.Size = New System.Drawing.Size(181, 44)
        Me.Aceptar.TabIndex = 9
        Me.Aceptar.Text = "&ACEPTAR"
        Me.Aceptar.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(459, 347)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(85, 31)
        Me.Button4.TabIndex = 8
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(459, 119)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 31)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(459, 158)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(85, 31)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(459, 309)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(85, 31)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConServiciosProBindingSource
        '
        Me.ConServiciosProBindingSource.DataMember = "ConServiciosPro"
        Me.ConServiciosProBindingSource.DataSource = Me.DataSetEric2
        '
        'ConServiciosProTableAdapter
        '
        Me.ConServiciosProTableAdapter.ClearBeforeFill = True
        '
        'DescripcionListBox
        '
        Me.DescripcionListBox.DataSource = Me.ConServiciosProBindingSource
        Me.DescripcionListBox.DisplayMember = "Descripcion"
        Me.DescripcionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionListBox.FormattingEnabled = True
        Me.DescripcionListBox.ItemHeight = 18
        Me.DescripcionListBox.Location = New System.Drawing.Point(57, 34)
        Me.DescripcionListBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionListBox.Name = "DescripcionListBox"
        Me.DescripcionListBox.Size = New System.Drawing.Size(367, 382)
        Me.DescripcionListBox.TabIndex = 12
        Me.DescripcionListBox.ValueMember = "Clv_Servicio"
        '
        'ConServiciosTmpBindingSource
        '
        Me.ConServiciosTmpBindingSource.DataMember = "ConServiciosTmp"
        Me.ConServiciosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConServiciosTmpTableAdapter
        '
        Me.ConServiciosTmpTableAdapter.ClearBeforeFill = True
        '
        'DescripcionListBox1
        '
        Me.DescripcionListBox1.DataSource = Me.ConServiciosTmpBindingSource
        Me.DescripcionListBox1.DisplayMember = "Descripcion"
        Me.DescripcionListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionListBox1.FormattingEnabled = True
        Me.DescripcionListBox1.ItemHeight = 18
        Me.DescripcionListBox1.Location = New System.Drawing.Point(569, 34)
        Me.DescripcionListBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionListBox1.Name = "DescripcionListBox1"
        Me.DescripcionListBox1.Size = New System.Drawing.Size(367, 382)
        Me.DescripcionListBox1.TabIndex = 14
        Me.DescripcionListBox1.ValueMember = "Clv_Servicio"
        '
        'InsertarServiciosTmpBindingSource
        '
        Me.InsertarServiciosTmpBindingSource.DataMember = "InsertarServiciosTmp"
        Me.InsertarServiciosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarServiciosTmpTableAdapter
        '
        Me.InsertarServiciosTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarServiciosTmpBindingSource
        '
        Me.BorrarServiciosTmpBindingSource.DataMember = "BorrarServiciosTmp"
        Me.BorrarServiciosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'BorrarServiciosTmpTableAdapter
        '
        Me.BorrarServiciosTmpTableAdapter.ClearBeforeFill = True
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(848, 489)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 15
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmSelServicioE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1045, 548)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.DescripcionListBox1)
        Me.Controls.Add(Me.DescripcionListBox)
        Me.Controls.Add(Me.Aceptar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button3)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelServicioE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona los Paquetes"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConServiciosProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Aceptar As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConServiciosProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConServiciosProTableAdapter As sofTV.DataSetEric2TableAdapters.ConServiciosProTableAdapter
    Friend WithEvents DescripcionListBox As System.Windows.Forms.ListBox
    Friend WithEvents ConServiciosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConServiciosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConServiciosTmpTableAdapter
    Friend WithEvents DescripcionListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents InsertarServiciosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarServiciosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarServiciosTmpTableAdapter
    Friend WithEvents BorrarServiciosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarServiciosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.BorrarServiciosTmpTableAdapter
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
