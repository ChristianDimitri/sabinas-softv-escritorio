Imports System.Data.Sql
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections.Generic

Public Class ImprimirHistorialDesconexiones
    'Private tipo As String = 0
    'Private contrato As Int64
    'Private tipoSer As Integer = 0
    'Private inicial As DateTime = "01-01-1900"
    'Private final As DateTime = "12-12-9999"
    Private Con As New SqlConnection(MiConexion)
    Private customersByCityReport As ReportDocument

    Private Sub ImprimirHistorialDesconexiones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.contrato = Convert.ToInt64(FrmClientes.CONTRATOTextBox.Text.ToString)
    End Sub

    Private Sub CrystalReportViewer1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CrystalReportViewer1.Load

    End Sub


    Public Sub Reportes(ByVal tipo As Integer, ByVal contrato As Int64, ByVal tiposer As Integer, ByVal inicial As DateTime, ByVal final As DateTime)
        'Me.tipo = tipo
        'Me.contrato = contrato
        'Me.tipoSer = tiposer
        'Me.inicial = inicial
        'Me.final = final
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim reportPath As String

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            reportPath = RutaReportes + "\ReporteHistorialDesconexiones.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            customersByCityReport.SetParameterValue(0, contrato)
            customersByCityReport.SetParameterValue(1, tiposer)
            customersByCityReport.SetParameterValue(2, inicial)
            customersByCityReport.SetParameterValue(3, final)
            customersByCityReport.SetParameterValue(4, tipo)
            CrystalReportViewer1.ReportSource = customersByCityReport
        Catch ex As CrystalDecisions.CrystalReports.Engine.ParameterFieldCurrentValueException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    Public Sub ReporteMaterialInstalaciones(ByVal CLV_SESSION As Int64, ByVal CLV_TIPSER As Integer, ByVal FECHAI As DateTime, ByVal FECHAF As DateTime)
        Me.Text = "Reporte Material Usado en Contrataciones de " & eServicio
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim reportPath As String
            Dim fechas As String = "Del " + FECHAI.ToShortDateString + " al " + FECHAF.ToShortDateString

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            reportPath = RutaReportes + "\ReportMaterialInstalaciones.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            customersByCityReport.SetParameterValue(0, CLV_SESSION)
            customersByCityReport.SetParameterValue(1, CLV_TIPSER)
            customersByCityReport.SetParameterValue(2, FECHAI)
            customersByCityReport.SetParameterValue(3, FECHAF)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
        Catch ex As CrystalDecisions.CrystalReports.Engine.ParameterFieldCurrentValueException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    Public Sub ReporteDesconexiones(ByVal FECHAI As DateTime, ByVal FECHAF As DateTime, ByVal tipoSer As Integer)
        Me.Text = "Historial Desconexiones"
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim reportPath As String
            Dim fechas As String = "Del " + FECHAI.ToShortDateString + " al " + FECHAF.ToShortDateString

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            reportPath = RutaReportes + "\TotalDesconexiones.rpt"

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, FECHAI)
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, FECHAF)
            BaseII.CreateMyParameter("@TIPOSER", SqlDbType.Int, tipoSer)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteTotalDesconexiones")

            Dim DS As New DataSet
            DS.Clear()
            DS = BaseII.ConsultaDS("ReporteTotalDesconexiones", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'customersByCityReport.SetParameterValue(0, FECHAI)
            'customersByCityReport.SetParameterValue(1, FECHAF)
            'customersByCityReport.SetParameterValue(2, tipoSer)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
        Catch ex As CrystalDecisions.CrystalReports.Engine.ParameterFieldCurrentValueException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    Public Sub ReporteProspectos(ByVal fechaI As DateTime, ByVal fechaF As DateTime, ByVal servicio As Integer, ByVal estado As Integer)
        Me.Text = "Reporte Prospectos"
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim reportPath As String
            Dim fechas As String = "Del " + fechaI.ToShortDateString + " al " + fechaF.ToShortDateString

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            reportPath = RutaReportes + "\ReporteProspectos.rpt"
            'reportPath = "C:\Documents and Settings\Rodrigo\Escritorio\ReporteProspectos.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            customersByCityReport.SetParameterValue(0, fechaI)
            customersByCityReport.SetParameterValue(1, fechaF)
            customersByCityReport.SetParameterValue(2, servicio)
            customersByCityReport.SetParameterValue(3, estado)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
        Catch ex As CrystalDecisions.CrystalReports.Engine.ParameterFieldCurrentValueException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class