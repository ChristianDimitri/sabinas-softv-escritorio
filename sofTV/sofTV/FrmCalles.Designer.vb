<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCalles
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_CalleLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim NOMBRELabel1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCalles))
        Me.Clv_CalleTextBox = New System.Windows.Forms.TextBox()
        Me.CONCALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CONCALLESBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.CONCALLESBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cbEstado = New System.Windows.Forms.ComboBox()
        Me.cbCiudad = New System.Windows.Forms.ComboBox()
        Me.cbLocalidad = New System.Windows.Forms.ComboBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.clv_colonia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombrecolonia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clv_localidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombrelocalidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clv_ciudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombreciudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clv_estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MUECOLONIASCompaniaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.MUECOLONIASCompaniaTableAdapter = New sofTV.DataSetEDGARTableAdapters.MUECOLONIASCompaniaTableAdapter()
        Me.MUECOLONIASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONCVECAROLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONCALLESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCALLESTableAdapter()
        Me.MUESTRACALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACALLESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACALLESTableAdapter()
        Me.CONCVECAROLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCVECAROLTableAdapter()
        Me.MUECOLONIASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUECOLONIASTableAdapter()
        Me.DAMECOLONIA_CALLEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMECOLONIA_CALLETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DAMECOLONIA_CALLETableAdapter()
        Clv_CalleLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        NOMBRELabel1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        CType(Me.CONCALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CONCALLESBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCALLESBindingNavigator.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUECOLONIASCompaniaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUECOLONIASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONCVECAROLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECOLONIA_CALLEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_CalleLabel
        '
        Clv_CalleLabel.AutoSize = True
        Clv_CalleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CalleLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_CalleLabel.Location = New System.Drawing.Point(12, 55)
        Clv_CalleLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_CalleLabel.Name = "Clv_CalleLabel"
        Clv_CalleLabel.Size = New System.Drawing.Size(60, 18)
        Clv_CalleLabel.TabIndex = 2
        Clv_CalleLabel.Text = "Clave :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.ForeColor = System.Drawing.Color.LightSlateGray
        NOMBRELabel.Location = New System.Drawing.Point(12, 100)
        NOMBRELabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(78, 18)
        NOMBRELabel.TabIndex = 4
        NOMBRELabel.Text = "Nombre :"
        '
        'NOMBRELabel1
        '
        NOMBRELabel1.AutoSize = True
        NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel1.ForeColor = System.Drawing.Color.LightSlateGray
        NOMBRELabel1.Location = New System.Drawing.Point(112, 151)
        NOMBRELabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NOMBRELabel1.Name = "NOMBRELabel1"
        NOMBRELabel1.Size = New System.Drawing.Size(76, 18)
        NOMBRELabel1.TabIndex = 4
        NOMBRELabel1.Text = "Colonia :"
        AddHandler NOMBRELabel1.Click, AddressOf Me.NOMBRELabel1_Click
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(283, 279)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(90, 18)
        Label2.TabIndex = 6
        Label2.Text = "Colonias  :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(93, 116)
        Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(90, 18)
        Label3.TabIndex = 162
        Label3.Text = "Localidad :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(117, 79)
        Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(70, 18)
        Label4.TabIndex = 164
        Label4.Text = "Ciudad :"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(119, 44)
        Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(71, 18)
        Label5.TabIndex = 166
        Label5.Text = "Estado :"
        '
        'Clv_CalleTextBox
        '
        Me.Clv_CalleTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_CalleTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_CalleTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCALLESBindingSource, "Clv_Calle", True))
        Me.Clv_CalleTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CalleTextBox.Location = New System.Drawing.Point(87, 55)
        Me.Clv_CalleTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_CalleTextBox.Name = "Clv_CalleTextBox"
        Me.Clv_CalleTextBox.ReadOnly = True
        Me.Clv_CalleTextBox.Size = New System.Drawing.Size(133, 24)
        Me.Clv_CalleTextBox.TabIndex = 0
        Me.Clv_CalleTextBox.TabStop = False
        '
        'CONCALLESBindingSource
        '
        Me.CONCALLESBindingSource.DataMember = "CONCALLES"
        Me.CONCALLESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NOMBRETextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCALLESBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(108, 100)
        Me.NOMBRETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(549, 24)
        Me.NOMBRETextBox.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CONCALLESBindingNavigator)
        Me.Panel1.Controls.Add(Me.NOMBRETextBox)
        Me.Panel1.Controls.Add(Clv_CalleLabel)
        Me.Panel1.Controls.Add(NOMBRELabel)
        Me.Panel1.Controls.Add(Me.Clv_CalleTextBox)
        Me.Panel1.Location = New System.Drawing.Point(16, 36)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(721, 158)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'CONCALLESBindingNavigator
        '
        Me.CONCALLESBindingNavigator.AddNewItem = Nothing
        Me.CONCALLESBindingNavigator.BindingSource = Me.CONCALLESBindingSource
        Me.CONCALLESBindingNavigator.CountItem = Nothing
        Me.CONCALLESBindingNavigator.DeleteItem = Nothing
        Me.CONCALLESBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONCALLESBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.BindingNavigatorSeparator2, Me.CONCALLESBindingNavigatorSaveItem})
        Me.CONCALLESBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCALLESBindingNavigator.MoveFirstItem = Nothing
        Me.CONCALLESBindingNavigator.MoveLastItem = Nothing
        Me.CONCALLESBindingNavigator.MoveNextItem = Nothing
        Me.CONCALLESBindingNavigator.MovePreviousItem = Nothing
        Me.CONCALLESBindingNavigator.Name = "CONCALLESBindingNavigator"
        Me.CONCALLESBindingNavigator.PositionItem = Nothing
        Me.CONCALLESBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONCALLESBindingNavigator.Size = New System.Drawing.Size(721, 27)
        Me.CONCALLESBindingNavigator.TabIndex = 200
        Me.CONCALLESBindingNavigator.Text = "BindingNavigator1"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(89, 24)
        Me.ToolStripLabel1.Text = "&CANCELAR"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'CONCALLESBindingNavigatorSaveItem
        '
        Me.CONCALLESBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCALLESBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCALLESBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCALLESBindingNavigatorSaveItem.Name = "CONCALLESBindingNavigatorSaveItem"
        Me.CONCALLESBindingNavigatorSaveItem.Size = New System.Drawing.Size(107, 24)
        Me.CONCALLESBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cbEstado)
        Me.Panel2.Controls.Add(Label5)
        Me.Panel2.Controls.Add(Me.cbCiudad)
        Me.Panel2.Controls.Add(Label4)
        Me.Panel2.Controls.Add(Me.cbLocalidad)
        Me.Panel2.Controls.Add(Label3)
        Me.Panel2.Controls.Add(Me.DataGridView1)
        Me.Panel2.Controls.Add(Me.ComboBox1)
        Me.Panel2.Controls.Add(Label2)
        Me.Panel2.Controls.Add(NOMBRELabel1)
        Me.Panel2.Controls.Add(Me.Button2)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Location = New System.Drawing.Point(16, 231)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(721, 427)
        Me.Panel2.TabIndex = 120
        Me.Panel2.TabStop = True
        '
        'cbEstado
        '
        Me.cbEstado.DisplayMember = "Nombre"
        Me.cbEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbEstado.FormattingEnabled = True
        Me.cbEstado.Location = New System.Drawing.Point(205, 39)
        Me.cbEstado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Size = New System.Drawing.Size(425, 26)
        Me.cbEstado.TabIndex = 167
        Me.cbEstado.ValueMember = "Clv_Estado"
        '
        'cbCiudad
        '
        Me.cbCiudad.DisplayMember = "Nombre"
        Me.cbCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCiudad.FormattingEnabled = True
        Me.cbCiudad.Location = New System.Drawing.Point(205, 75)
        Me.cbCiudad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbCiudad.Name = "cbCiudad"
        Me.cbCiudad.Size = New System.Drawing.Size(425, 26)
        Me.cbCiudad.TabIndex = 165
        Me.cbCiudad.ValueMember = "Clv_Ciudad"
        '
        'cbLocalidad
        '
        Me.cbLocalidad.DisplayMember = "NOMBRE"
        Me.cbLocalidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbLocalidad.FormattingEnabled = True
        Me.cbLocalidad.Location = New System.Drawing.Point(205, 111)
        Me.cbLocalidad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbLocalidad.Name = "cbLocalidad"
        Me.cbLocalidad.Size = New System.Drawing.Size(425, 26)
        Me.cbLocalidad.TabIndex = 163
        Me.cbLocalidad.ValueMember = "Clv_Localidad"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clv_colonia, Me.nombrecolonia, Me.clv_localidad, Me.nombrelocalidad, Me.clv_ciudad, Me.nombreciudad, Me.clv_estado, Me.Estado})
        Me.DataGridView1.Location = New System.Drawing.Point(16, 182)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(543, 223)
        Me.DataGridView1.TabIndex = 161
        '
        'clv_colonia
        '
        Me.clv_colonia.DataPropertyName = "clv_colonia"
        Me.clv_colonia.HeaderText = "clv_colonia"
        Me.clv_colonia.Name = "clv_colonia"
        Me.clv_colonia.ReadOnly = True
        Me.clv_colonia.Visible = False
        '
        'nombrecolonia
        '
        Me.nombrecolonia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nombrecolonia.DataPropertyName = "colonia"
        Me.nombrecolonia.HeaderText = "Colonia"
        Me.nombrecolonia.Name = "nombrecolonia"
        Me.nombrecolonia.ReadOnly = True
        '
        'clv_localidad
        '
        Me.clv_localidad.DataPropertyName = "clv_localidad"
        Me.clv_localidad.HeaderText = "clv_localidad"
        Me.clv_localidad.Name = "clv_localidad"
        Me.clv_localidad.ReadOnly = True
        Me.clv_localidad.Visible = False
        '
        'nombrelocalidad
        '
        Me.nombrelocalidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nombrelocalidad.DataPropertyName = "localidad"
        Me.nombrelocalidad.HeaderText = "Localidad"
        Me.nombrelocalidad.Name = "nombrelocalidad"
        Me.nombrelocalidad.ReadOnly = True
        '
        'clv_ciudad
        '
        Me.clv_ciudad.DataPropertyName = "clv_ciudad"
        Me.clv_ciudad.HeaderText = "clv_ciudad"
        Me.clv_ciudad.Name = "clv_ciudad"
        Me.clv_ciudad.ReadOnly = True
        Me.clv_ciudad.Visible = False
        '
        'nombreciudad
        '
        Me.nombreciudad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nombreciudad.DataPropertyName = "ciudad"
        Me.nombreciudad.HeaderText = "Ciudad"
        Me.nombreciudad.Name = "nombreciudad"
        Me.nombreciudad.ReadOnly = True
        '
        'clv_estado
        '
        Me.clv_estado.DataPropertyName = "clv_estado"
        Me.clv_estado.HeaderText = "clv_estado"
        Me.clv_estado.Name = "clv_estado"
        Me.clv_estado.ReadOnly = True
        Me.clv_estado.Visible = False
        '
        'Estado
        '
        Me.Estado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Estado.DataPropertyName = "estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "nombre"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(205, 146)
        Me.ComboBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(425, 26)
        Me.ComboBox1.TabIndex = 120
        Me.ComboBox1.ValueMember = "CLV_COLONIA"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(567, 226)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(128, 37)
        Me.Button2.TabIndex = 160
        Me.Button2.Text = "&Quitar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(567, 182)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(128, 37)
        Me.Button1.TabIndex = 140
        Me.Button1.Text = "&Agregar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.OrangeRed
        Me.Label1.Location = New System.Drawing.Point(4, 2)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(359, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Relación de la Calle con la Colonias"
        '
        'MUECOLONIASCompaniaBindingSource
        '
        Me.MUECOLONIASCompaniaBindingSource.DataMember = "MUECOLONIASCompania"
        Me.MUECOLONIASCompaniaBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(16, 644)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(543, 245)
        Me.TreeView1.TabIndex = 8
        Me.TreeView1.TabStop = False
        Me.TreeView1.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(556, 666)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 300
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'MUECOLONIASCompaniaTableAdapter
        '
        Me.MUECOLONIASCompaniaTableAdapter.ClearBeforeFill = True
        '
        'MUECOLONIASBindingSource
        '
        Me.MUECOLONIASBindingSource.DataMember = "MUECOLONIAS"
        Me.MUECOLONIASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONCVECAROLBindingSource
        '
        Me.CONCVECAROLBindingSource.DataMember = "CONCVECAROL"
        Me.CONCVECAROLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONCALLESTableAdapter
        '
        Me.CONCALLESTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACALLESBindingSource
        '
        Me.MUESTRACALLESBindingSource.DataMember = "MUESTRACALLES"
        Me.MUESTRACALLESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACALLESTableAdapter
        '
        Me.MUESTRACALLESTableAdapter.ClearBeforeFill = True
        '
        'CONCVECAROLTableAdapter
        '
        Me.CONCVECAROLTableAdapter.ClearBeforeFill = True
        '
        'MUECOLONIASTableAdapter
        '
        Me.MUECOLONIASTableAdapter.ClearBeforeFill = True
        '
        'DAMECOLONIA_CALLEBindingSource
        '
        Me.DAMECOLONIA_CALLEBindingSource.DataMember = "DAMECOLONIA_CALLE"
        Me.DAMECOLONIA_CALLEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DAMECOLONIA_CALLETableAdapter
        '
        Me.DAMECOLONIA_CALLETableAdapter.ClearBeforeFill = True
        '
        'FrmCalles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(764, 721)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmCalles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catalógo de Calles"
        CType(Me.CONCALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONCALLESBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCALLESBindingNavigator.ResumeLayout(False)
        Me.CONCALLESBindingNavigator.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUECOLONIASCompaniaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUECOLONIASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONCVECAROLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECOLONIA_CALLEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONCALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCALLESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCALLESTableAdapter
    Friend WithEvents Clv_CalleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CONCALLESBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CONCALLESBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MUESTRACALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACALLESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACALLESTableAdapter
    Friend WithEvents CONCVECAROLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCVECAROLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCVECAROLTableAdapter
    Friend WithEvents MUECOLONIASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUECOLONIASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUECOLONIASTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DAMECOLONIA_CALLEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECOLONIA_CALLETableAdapter As sofTV.NewSofTvDataSetTableAdapters.DAMECOLONIA_CALLETableAdapter
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents MUECOLONIASCompaniaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents MUECOLONIASCompaniaTableAdapter As sofTV.DataSetEDGARTableAdapters.MUECOLONIASCompaniaTableAdapter
    Friend WithEvents cbLocalidad As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents cbEstado As System.Windows.Forms.ComboBox
    Friend WithEvents cbCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents clv_colonia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombrecolonia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_localidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombrelocalidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_ciudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombreciudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
