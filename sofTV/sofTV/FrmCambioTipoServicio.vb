﻿Public Class FrmCambioTipoServicio
    Dim LocMensaje As String = ""

#Region "Llena Combos"
    Private Function llenaCmbTipoServicio(ByVal prmClvTipSer As Integer) As DataTable
        Dim clase As New classCambioTipoServicio
        clase.clvTipSer = prmClvTipSer
        llenaCmbTipoServicio = clase.uspMuestraTiposServicios()
    End Function
    Private Function llenaCmbTrabajos(ByVal prmClvTipSer As Integer) As DataTable
        Dim clase As New classCambioTipoServicio
        clase.clvTipSer = prmClvTipSer
        llenaCmbTrabajos = clase.uspMostrarTrabajos()
    End Function
#End Region

#Region "Llena DataGridView"
    Private Sub muestragridposiblescambios()
        Dim clase As New classCambioTipoServicio
        Me.dgvPosiblesCambios.DataSource = clase.uspMuestraPosiblesCambios()
    End Sub
    Private Function muestragridtrabajos(ByVal prmclvcombinacion As Integer, ByVal prmop As Integer) As DataTable
        Dim clase As New classCambioTipoServicio
        clase.clvCombinacion = prmclvcombinacion
        clase.Op = prmop
        muestragridtrabajos = clase.uspMostrarTabajosGrid()
    End Function
#End Region

#Region "Grabar"
    Private Sub GuardaTrabajos(ByVal prmclvcombinacion As Integer, ByVal prmClvtrabajoAct As Integer, ByVal prmClvtrabajoDes As Integer, ByVal prmop As Integer)
        Dim clase As New classCambioTipoServicio
        clase.clvCombinacion = prmclvcombinacion
        clase.clvTrabajoAct = prmClvtrabajoAct
        clase.clvTrabajoDes = prmClvtrabajoDes
        clase.Op = prmop
        clase.uspGrabaTabajos()
    End Sub
    Private Function GuardaCombinacion(ByVal prmclvtipser As Integer, ByVal prmclvtipserpos As Integer) As DataTable
        Dim clase As New classCambioTipoServicio
        clase.clvTipSer = prmclvtipser
        clase.clvtipserpos = prmclvtipserpos
        GuardaCombinacion = clase.uspGrabaCombinacion()
        For Each row As DataRow In GuardaCombinacion.Rows
            LocMensaje = row("Mensaje").ToString
        Next
    End Function
#End Region

#Region "Borrar"
    Private Sub BorraTrabajos(ByVal prmclvcombinacion As Integer, ByVal prmClvtrabajoAct As Integer, ByVal prmClvtrabajoDes As Integer, ByVal prmop As Integer)
        Dim clase As New classCambioTipoServicio
        clase.clvCombinacion = prmclvcombinacion
        clase.clvTrabajoAct = prmClvtrabajoAct
        clase.clvTrabajoDes = prmClvtrabajoDes
        clase.Op = prmop
        clase.uspBorraTrabajos()
    End Sub
    Private Sub BorraCombinacion(ByVal prmclvcombinacion As Integer)
        Dim clase As New classCambioTipoServicio
        clase.clvCombinacion = prmclvcombinacion
        clase.uspBorrarCombinacion()
    End Sub
#End Region

#Region "Controles"
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        If dgvPosiblesCambios.Rows.Count > 0 Then
            If dgvOrdenerActivacion.Rows.Count = 0 Then
                MsgBox("Debe de Agregar las Ordenes de Activación del Servicio")
                Exit Sub
            End If
            If dgvOrdenesDesacivacion.Rows.Count = 0 Then
                MsgBox("Debe de Agregar las Ordenes de Desactivación del Servicio")
                Exit Sub
            End If
        End If
        Me.Close()

    End Sub

    Private Sub FrmCambioTipoServicio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.cmbTipoServicio.DataSource = llenaCmbTipoServicio(0)
        muestragridposiblescambios()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)

    End Sub

    Private Sub cmbTipoServicio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipoServicio.SelectedIndexChanged
        If IsNumeric(Me.cmbTipoServicio.SelectedValue) Then
            Me.cmbPosibleCambio.DataSource = llenaCmbTipoServicio(Me.cmbTipoServicio.SelectedValue)
        End If
    End Sub

    Private Sub dgvPosiblesCambios_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvPosiblesCambios.SelectionChanged
        If Me.dgvPosiblesCambios.Rows.Count > 0 Then
            If Me.dgvPosiblesCambios.SelectedRows.Count > 0 Then
                Me.cmbTrabajosActivacion.DataSource = llenaCmbTrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(2).Value.ToString))
                Me.cmbTrabajosDesactivacion.DataSource = llenaCmbTrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(1).Value.ToString))
                Me.dgvOrdenerActivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 1)
                Me.dgvOrdenesDesacivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 2)
            End If
        End If
    End Sub
    Private Sub btnAgregaTrabajoAct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregaTrabajoAct.Click
        GuardaTrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), Me.cmbTrabajosActivacion.SelectedValue, 0, 1)
        Me.dgvOrdenerActivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 1)
    End Sub

    Private Sub btnAgregaTrabajoDes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregaTrabajoDes.Click
        GuardaTrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 0, Me.cmbTrabajosDesactivacion.SelectedValue, 2)
        Me.dgvOrdenesDesacivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 2)
    End Sub

    Private Sub btnEliminaTrabajoAct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminaTrabajoAct.Click
        BorraTrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), CInt(Me.dgvOrdenerActivacion.SelectedCells(1).Value.ToString), 0, 1)
        Me.dgvOrdenerActivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 1)
    End Sub

    Private Sub btnEliminarTrabajoDes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarTrabajoDes.Click
        BorraTrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 0, CInt(Me.dgvOrdenesDesacivacion.SelectedCells(2).Value.ToString), 2)
        'Me.dgvOrdenerActivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 1)
        Me.dgvOrdenesDesacivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 2)
    End Sub

    Private Sub btnAgregaCombinacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregaCombinacion.Click
        GuardaCombinacion(Me.cmbTipoServicio.SelectedValue, Me.cmbPosibleCambio.SelectedValue)
        MsgBox(LocMensaje)
        muestragridposiblescambios()
        Me.dgvOrdenerActivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 1)
        Me.dgvOrdenesDesacivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 2)
    End Sub

    Private Sub btnEliminarCombinacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarCombinacion.Click
        BorraCombinacion(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString))
        muestragridposiblescambios()
        If Me.dgvPosiblesCambios.SelectedRows.Count > 0 Then
            Me.dgvOrdenerActivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 1)
            Me.dgvOrdenesDesacivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 2)
        Else
            If dgvPosiblesCambios.Rows.Count = 0 Then
                Me.dgvOrdenerActivacion.DataSource = muestragridtrabajos(0, 1)
                Me.dgvOrdenesDesacivacion.DataSource = muestragridtrabajos(0, 2)
            End If
        End If
    End Sub
#End Region

    Private Sub FrmCambioTipoServicio_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        muestragridposiblescambios()
        If Me.dgvPosiblesCambios.SelectedRows.Count > 0 Then
            Me.dgvOrdenerActivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 1)
            Me.dgvOrdenesDesacivacion.DataSource = muestragridtrabajos(CInt(Me.dgvPosiblesCambios.SelectedCells(0).Value.ToString), 2)
        Else
            If dgvPosiblesCambios.Rows.Count = 0 Then
                Me.dgvOrdenerActivacion.DataSource = muestragridtrabajos(0, 1)
                Me.dgvOrdenesDesacivacion.DataSource = muestragridtrabajos(0, 2)
            End If
        End If
    End Sub
End Class