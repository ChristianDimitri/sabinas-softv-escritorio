﻿Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class FrmConfiguracionServiciosPlaza
    Dim id_companiaLocal As Integer
    Dim Clv_EstadoLocal As Integer
    Dim Clv_CiudadLocal As Integer
    Dim clv_servicioPorEliminarLocal As Integer

    Private Sub FrmConfiguracionServiciosPlaza_Load(sender As Object, e As EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
            'TODO: esta línea de código carga datos en la tabla 'DataSetEDGAR.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
            Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.DataSetEDGAR.MuestraTipSerPrincipal)
            CON.Close()
            llenaRelacion()

            If opcion = "C" Then
                BtnAgregar.Enabled = False
                Button2.Enabled = False
                Button1.Enabled = False
                Button3.Enabled = False
            End If


            UspDesactivaBotones(Me, Me.Name)
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.ToString())
        End Try
        

    End Sub

    Private Sub llenaRelacion()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.BigInt, GloClvPlazaAux)
        DataGridView1.DataSource = BaseII.ConsultaDT("DameRelCompaniaEstadoCiudad")
    End Sub

    Private Sub DameServiciosRelCompaniaEstadoCiudad()
        If Not IsNumeric(id_companiaLocal) Or Not IsNumeric(Clv_EstadoLocal) Or Not IsNumeric(Clv_CiudadLocal) Or Not IsNumeric(ComboBoxTipSer.SelectedValue) Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.BigInt, GloClvPlazaAux)
        BaseII.CreateMyParameter("@id_compania", SqlDbType.BigInt, id_companiaLocal)
        BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.BigInt, Clv_EstadoLocal)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.BigInt, Clv_CiudadLocal)
        BaseII.CreateMyParameter("@Clv_Tipser", SqlDbType.BigInt, ComboBoxTipSer.SelectedValue)

        Dim DS As New DataSet
        DS.Clear()
        Dim listatablas As New List(Of String)
        listatablas.Add("SinAsignar")
        listatablas.Add("Asignada")

        DS = BaseII.ConsultaDS("DameServiciosRelCompaniaEstadoCiudad", listatablas)

        ComboBoxServicios.DataSource = DS.Tables.Item("SinAsignar")
        ComboBoxServicios.DisplayMember = "Descripcion"
        ComboBoxServicios.ValueMember = "Clv_Servicio"

        DataGridView2.DataSource = DS.Tables.Item("Asignada")
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            id_companiaLocal = DataGridView1.SelectedRows(0).Cells("id_compania").Value.ToString()
            Clv_EstadoLocal = DataGridView1.SelectedRows(0).Cells("Clv_Estado").Value.ToString()
            Clv_CiudadLocal = DataGridView1.SelectedRows(0).Cells("Clv_Ciudad").Value.ToString()

            DameServiciosRelCompaniaEstadoCiudad()
        Catch ex As Exception

        End Try
    End Sub


    Private Sub ComboBoxTipSer_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxTipSer.SelectedValueChanged
        DameServiciosRelCompaniaEstadoCiudad()
    End Sub

    Private Sub BtnAgregar_Click(sender As Object, e As EventArgs) Handles BtnAgregar.Click
        If Not IsNumeric(ComboBoxServicios.SelectedValue) Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.BigInt, GloClvPlazaAux)
        BaseII.CreateMyParameter("@id_compania", SqlDbType.BigInt, id_companiaLocal)
        BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.BigInt, Clv_EstadoLocal)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.BigInt, Clv_CiudadLocal)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, ComboBoxServicios.SelectedValue)
        BaseII.Inserta("insertaServiciosRelCompaniaEstadoCiudad")
        DameServiciosRelCompaniaEstadoCiudad()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Not IsNumeric(ComboBoxServicios.SelectedValue) Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.BigInt, GloClvPlazaAux)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, ComboBoxServicios.SelectedValue)
        BaseII.Inserta("insertaServiciosRelCompaniaEstadoCiudadATodos")
        DameServiciosRelCompaniaEstadoCiudad()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Not IsNumeric(clv_servicioPorEliminarLocal) Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.BigInt, GloClvPlazaAux)
        BaseII.CreateMyParameter("@id_compania", SqlDbType.BigInt, id_companiaLocal)
        BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.BigInt, Clv_EstadoLocal)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.BigInt, Clv_CiudadLocal)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, clv_servicioPorEliminarLocal)
        BaseII.Inserta("eliminaServiciosRelCompaniaEstadoCiudad")
        DameServiciosRelCompaniaEstadoCiudad()
    End Sub

   

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If Not IsNumeric(clv_servicioPorEliminarLocal) Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.BigInt, GloClvPlazaAux)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, clv_servicioPorEliminarLocal)
        BaseII.Inserta("eliminaServiciosRelCompaniaEstadoCiudadATodos")
        DameServiciosRelCompaniaEstadoCiudad()
    End Sub

    Private Sub DataGridView2_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView2.SelectionChanged
        Try
            clv_servicioPorEliminarLocal = DataGridView2.SelectedRows(0).Cells("Clv_Servicio").Value.ToString()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.BigInt, GloClvPlazaAux)
        BaseII.CreateMyParameter("@id_compania", SqlDbType.BigInt, id_companiaLocal)
        BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.BigInt, Clv_EstadoLocal)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.BigInt, Clv_CiudadLocal)
        BaseII.CreateMyParameter("@Clv_Tipser", SqlDbType.BigInt, ComboBoxTipSer.SelectedValue)
        BaseII.Inserta("insertaServiciosRelCompaniaEstadoCiudadTodosLosServicios")
        DameServiciosRelCompaniaEstadoCiudad()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.BigInt, GloClvPlazaAux)
        BaseII.CreateMyParameter("@id_compania", SqlDbType.BigInt, id_companiaLocal)
        BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.BigInt, Clv_EstadoLocal)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.BigInt, Clv_CiudadLocal)
        BaseII.CreateMyParameter("@Clv_Tipser", SqlDbType.BigInt, ComboBoxTipSer.SelectedValue)
        BaseII.Inserta("eliminaServiciosRelCompaniaEstadoCiudadTodosLosServicios")
        DameServiciosRelCompaniaEstadoCiudad()
    End Sub
End Class