<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaqueteAdicional
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_PaqAdiLabel As System.Windows.Forms.Label
        Dim PaqueteAdicionalLabel As System.Windows.Forms.Label
        Dim ContratacionLabel As System.Windows.Forms.Label
        Dim MensualidadLabel As System.Windows.Forms.Label
        Dim ContratacionLabel1 As System.Windows.Forms.Label
        Dim MensualidadLabel1 As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim DescripcionLabel1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPaqueteAdicional))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ConPaqueteAdicionalBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ConPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ConPaqueteAdicionalBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_PaqAdiTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_Tipo_Paquete_AdiocionalTextBox = New System.Windows.Forms.TextBox()
        Me.TipoCobroTextBox = New System.Windows.Forms.TextBox()
        Me.NumeroTextBox = New System.Windows.Forms.TextBox()
        Me.ContratacionTextBox = New System.Windows.Forms.TextBox()
        Me.MensualidadTextBox = New System.Windows.Forms.TextBox()
        Me.ConPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.ConPaqueteAdicionalTableAdapter()
        Me.ModPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.ModPaqueteAdicionalTableAdapter()
        Me.BorPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.BorPaqueteAdicionalTableAdapter()
        Me.ConDetPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConDetPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.ConDetPaqueteAdicionalTableAdapter()
        Me.ConDetPaqueteAdicionalDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NueDetPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueDetPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.NueDetPaqueteAdicionalTableAdapter()
        Me.ModDetPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModDetPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.ModDetPaqueteAdicionalTableAdapter()
        Me.BorDetPaqueteActivadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorDetPaqueteActivadoTableAdapter = New sofTV.DataSetEric2TableAdapters.BorDetPaqueteActivadoTableAdapter()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox()
        Me.ContratacionTextBox1 = New System.Windows.Forms.TextBox()
        Me.MensualidadTextBox1 = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DescripcionComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraServiciosEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.NuePaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NuePaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.NuePaqueteAdicionalTableAdapter()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PaqueteAdicionalTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraTipo_Paquetes_AdicionalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Tipo_CobroTextBox = New System.Windows.Forms.TextBox()
        Me.MuestraTipo_Paquetes_AdicionalesTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraTipo_Paquetes_AdicionalesTableAdapter()
        Me.MuestraServiciosEricTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraServiciosEricTableAdapter()
        Clv_PaqAdiLabel = New System.Windows.Forms.Label()
        PaqueteAdicionalLabel = New System.Windows.Forms.Label()
        ContratacionLabel = New System.Windows.Forms.Label()
        MensualidadLabel = New System.Windows.Forms.Label()
        ContratacionLabel1 = New System.Windows.Forms.Label()
        MensualidadLabel1 = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        DescripcionLabel1 = New System.Windows.Forms.Label()
        CType(Me.ConPaqueteAdicionalBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConPaqueteAdicionalBindingNavigator.SuspendLayout()
        CType(Me.ConPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConDetPaqueteAdicionalDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorDetPaqueteActivadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.MuestraServiciosEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NuePaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipo_Paquetes_AdicionalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_PaqAdiLabel
        '
        Clv_PaqAdiLabel.AutoSize = True
        Clv_PaqAdiLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_PaqAdiLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_PaqAdiLabel.Location = New System.Drawing.Point(216, 44)
        Clv_PaqAdiLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_PaqAdiLabel.Name = "Clv_PaqAdiLabel"
        Clv_PaqAdiLabel.Size = New System.Drawing.Size(60, 18)
        Clv_PaqAdiLabel.TabIndex = 2
        Clv_PaqAdiLabel.Text = "Clave :"
        '
        'PaqueteAdicionalLabel
        '
        PaqueteAdicionalLabel.AutoSize = True
        PaqueteAdicionalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PaqueteAdicionalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PaqueteAdicionalLabel.Location = New System.Drawing.Point(3, 78)
        PaqueteAdicionalLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        PaqueteAdicionalLabel.Name = "PaqueteAdicionalLabel"
        PaqueteAdicionalLabel.Size = New System.Drawing.Size(244, 18)
        PaqueteAdicionalLabel.TabIndex = 8
        PaqueteAdicionalLabel.Text = "Nombre del Paquete Adicional :"
        '
        'ContratacionLabel
        '
        ContratacionLabel.AutoSize = True
        ContratacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratacionLabel.Location = New System.Drawing.Point(995, 218)
        ContratacionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ContratacionLabel.Name = "ContratacionLabel"
        ContratacionLabel.Size = New System.Drawing.Size(110, 18)
        ContratacionLabel.TabIndex = 12
        ContratacionLabel.Text = "Contratación:"
        '
        'MensualidadLabel
        '
        MensualidadLabel.AutoSize = True
        MensualidadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensualidadLabel.Location = New System.Drawing.Point(1171, 218)
        MensualidadLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        MensualidadLabel.Name = "MensualidadLabel"
        MensualidadLabel.Size = New System.Drawing.Size(107, 18)
        MensualidadLabel.TabIndex = 14
        MensualidadLabel.Text = "Mensualidad:"
        '
        'ContratacionLabel1
        '
        ContratacionLabel1.AutoSize = True
        ContratacionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratacionLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        ContratacionLabel1.Location = New System.Drawing.Point(17, 9)
        ContratacionLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ContratacionLabel1.Name = "ContratacionLabel1"
        ContratacionLabel1.Size = New System.Drawing.Size(110, 18)
        ContratacionLabel1.TabIndex = 25
        ContratacionLabel1.Text = "Contratación:"
        '
        'MensualidadLabel1
        '
        MensualidadLabel1.AutoSize = True
        MensualidadLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensualidadLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        MensualidadLabel1.Location = New System.Drawing.Point(193, 9)
        MensualidadLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        MensualidadLabel1.Name = "MensualidadLabel1"
        MensualidadLabel1.Size = New System.Drawing.Size(107, 18)
        MensualidadLabel1.TabIndex = 27
        MensualidadLabel1.Text = "Mensualidad:"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(47, 111)
        DescripcionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(206, 18)
        DescripcionLabel.TabIndex = 20
        DescripcionLabel.Text = "Tarifa de Larga Distancia :"
        AddHandler DescripcionLabel.Click, AddressOf Me.DescripcionLabel_Click
        '
        'DescripcionLabel1
        '
        DescripcionLabel1.AutoSize = True
        DescripcionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel1.Location = New System.Drawing.Point(19, 9)
        DescripcionLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel1.Name = "DescripcionLabel1"
        DescripcionLabel1.Size = New System.Drawing.Size(74, 18)
        DescripcionLabel1.TabIndex = 0
        DescripcionLabel1.Text = "Servicio:"
        '
        'ConPaqueteAdicionalBindingNavigator
        '
        Me.ConPaqueteAdicionalBindingNavigator.AddNewItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.BindingSource = Me.ConPaqueteAdicionalBindingSource
        Me.ConPaqueteAdicionalBindingNavigator.CountItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.DeleteItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConPaqueteAdicionalBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ConPaqueteAdicionalBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConPaqueteAdicionalBindingNavigatorSaveItem})
        Me.ConPaqueteAdicionalBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConPaqueteAdicionalBindingNavigator.MoveFirstItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.MoveLastItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.MoveNextItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.MovePreviousItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.Name = "ConPaqueteAdicionalBindingNavigator"
        Me.ConPaqueteAdicionalBindingNavigator.PositionItem = Nothing
        Me.ConPaqueteAdicionalBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConPaqueteAdicionalBindingNavigator.Size = New System.Drawing.Size(1342, 27)
        Me.ConPaqueteAdicionalBindingNavigator.TabIndex = 0
        Me.ConPaqueteAdicionalBindingNavigator.Text = "BindingNavigator1"
        '
        'ConPaqueteAdicionalBindingSource
        '
        Me.ConPaqueteAdicionalBindingSource.DataMember = "ConPaqueteAdicional"
        Me.ConPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(106, 24)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConPaqueteAdicionalBindingNavigatorSaveItem
        '
        Me.ConPaqueteAdicionalBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConPaqueteAdicionalBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConPaqueteAdicionalBindingNavigatorSaveItem.Name = "ConPaqueteAdicionalBindingNavigatorSaveItem"
        Me.ConPaqueteAdicionalBindingNavigatorSaveItem.Size = New System.Drawing.Size(102, 24)
        Me.ConPaqueteAdicionalBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_PaqAdiTextBox
        '
        Me.Clv_PaqAdiTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_PaqAdiTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Clv_PaqAdi", True))
        Me.Clv_PaqAdiTextBox.Enabled = False
        Me.Clv_PaqAdiTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_PaqAdiTextBox.Location = New System.Drawing.Point(291, 41)
        Me.Clv_PaqAdiTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_PaqAdiTextBox.Name = "Clv_PaqAdiTextBox"
        Me.Clv_PaqAdiTextBox.ReadOnly = True
        Me.Clv_PaqAdiTextBox.Size = New System.Drawing.Size(132, 24)
        Me.Clv_PaqAdiTextBox.TabIndex = 3
        Me.Clv_PaqAdiTextBox.TabStop = False
        '
        'Clv_Tipo_Paquete_AdiocionalTextBox
        '
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Clv_Tipo_Paquete_Adiocional", True))
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Location = New System.Drawing.Point(981, 6)
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Name = "Clv_Tipo_Paquete_AdiocionalTextBox"
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.ReadOnly = True
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.Size = New System.Drawing.Size(12, 24)
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.TabIndex = 5
        Me.Clv_Tipo_Paquete_AdiocionalTextBox.TabStop = False
        '
        'TipoCobroTextBox
        '
        Me.TipoCobroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "TipoCobro", True))
        Me.TipoCobroTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoCobroTextBox.Location = New System.Drawing.Point(1003, 5)
        Me.TipoCobroTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TipoCobroTextBox.Name = "TipoCobroTextBox"
        Me.TipoCobroTextBox.ReadOnly = True
        Me.TipoCobroTextBox.Size = New System.Drawing.Size(12, 24)
        Me.TipoCobroTextBox.TabIndex = 7
        Me.TipoCobroTextBox.TabStop = False
        '
        'NumeroTextBox
        '
        Me.NumeroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Numero", True))
        Me.NumeroTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumeroTextBox.Location = New System.Drawing.Point(291, 150)
        Me.NumeroTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumeroTextBox.Name = "NumeroTextBox"
        Me.NumeroTextBox.Size = New System.Drawing.Size(167, 24)
        Me.NumeroTextBox.TabIndex = 11
        '
        'ContratacionTextBox
        '
        Me.ContratacionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratacionTextBox.Location = New System.Drawing.Point(999, 240)
        Me.ContratacionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ContratacionTextBox.Name = "ContratacionTextBox"
        Me.ContratacionTextBox.Size = New System.Drawing.Size(167, 24)
        Me.ContratacionTextBox.TabIndex = 13
        '
        'MensualidadTextBox
        '
        Me.MensualidadTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensualidadTextBox.Location = New System.Drawing.Point(1175, 240)
        Me.MensualidadTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MensualidadTextBox.Name = "MensualidadTextBox"
        Me.MensualidadTextBox.Size = New System.Drawing.Size(167, 24)
        Me.MensualidadTextBox.TabIndex = 15
        '
        'ConPaqueteAdicionalTableAdapter
        '
        Me.ConPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'ModPaqueteAdicionalBindingSource
        '
        Me.ModPaqueteAdicionalBindingSource.DataMember = "ModPaqueteAdicional"
        Me.ModPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'ModPaqueteAdicionalTableAdapter
        '
        Me.ModPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'BorPaqueteAdicionalBindingSource
        '
        Me.BorPaqueteAdicionalBindingSource.DataMember = "BorPaqueteAdicional"
        Me.BorPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'BorPaqueteAdicionalTableAdapter
        '
        Me.BorPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'ConDetPaqueteAdicionalBindingSource
        '
        Me.ConDetPaqueteAdicionalBindingSource.DataMember = "ConDetPaqueteAdicional"
        Me.ConDetPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'ConDetPaqueteAdicionalTableAdapter
        '
        Me.ConDetPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'ConDetPaqueteAdicionalDataGridView
        '
        Me.ConDetPaqueteAdicionalDataGridView.AllowUserToAddRows = False
        Me.ConDetPaqueteAdicionalDataGridView.AllowUserToDeleteRows = False
        Me.ConDetPaqueteAdicionalDataGridView.AutoGenerateColumns = False
        Me.ConDetPaqueteAdicionalDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConDetPaqueteAdicionalDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConDetPaqueteAdicionalDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5})
        Me.ConDetPaqueteAdicionalDataGridView.DataSource = Me.ConDetPaqueteAdicionalBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConDetPaqueteAdicionalDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConDetPaqueteAdicionalDataGridView.Location = New System.Drawing.Point(85, 238)
        Me.ConDetPaqueteAdicionalDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ConDetPaqueteAdicionalDataGridView.Name = "ConDetPaqueteAdicionalDataGridView"
        Me.ConDetPaqueteAdicionalDataGridView.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConDetPaqueteAdicionalDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.ConDetPaqueteAdicionalDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConDetPaqueteAdicionalDataGridView.Size = New System.Drawing.Size(687, 282)
        Me.ConDetPaqueteAdicionalDataGridView.TabIndex = 16
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Descripcion"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Servicio"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 200
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clave"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clave"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_PaqAdi"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_PaqAdi"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Contratacion"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Contratación"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 130
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Mensualidad"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Mensualidad"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 130
        '
        'NueDetPaqueteAdicionalBindingSource
        '
        Me.NueDetPaqueteAdicionalBindingSource.DataMember = "NueDetPaqueteAdicional"
        Me.NueDetPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'NueDetPaqueteAdicionalTableAdapter
        '
        Me.NueDetPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'ModDetPaqueteAdicionalBindingSource
        '
        Me.ModDetPaqueteAdicionalBindingSource.DataMember = "ModDetPaqueteAdicional"
        Me.ModDetPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'ModDetPaqueteAdicionalTableAdapter
        '
        Me.ModDetPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'BorDetPaqueteActivadoBindingSource
        '
        Me.BorDetPaqueteActivadoBindingSource.DataMember = "BorDetPaqueteActivado"
        Me.BorDetPaqueteActivadoBindingSource.DataSource = Me.DataSetEric2
        '
        'BorDetPaqueteActivadoTableAdapter
        '
        Me.BorDetPaqueteActivadoTableAdapter.ClearBeforeFill = True
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetPaqueteAdicionalBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(796, 1)
        Me.ClaveTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(12, 22)
        Me.ClaveTextBox.TabIndex = 20
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetPaqueteAdicionalBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(817, 1)
        Me.Clv_ServicioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.ReadOnly = True
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(12, 22)
        Me.Clv_ServicioTextBox.TabIndex = 24
        Me.Clv_ServicioTextBox.TabStop = False
        '
        'ContratacionTextBox1
        '
        Me.ContratacionTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetPaqueteAdicionalBindingSource, "Contratacion", True))
        Me.ContratacionTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratacionTextBox1.Location = New System.Drawing.Point(839, 1)
        Me.ContratacionTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ContratacionTextBox1.Name = "ContratacionTextBox1"
        Me.ContratacionTextBox1.ReadOnly = True
        Me.ContratacionTextBox1.Size = New System.Drawing.Size(12, 24)
        Me.ContratacionTextBox1.TabIndex = 26
        Me.ContratacionTextBox1.TabStop = False
        '
        'MensualidadTextBox1
        '
        Me.MensualidadTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetPaqueteAdicionalBindingSource, "Mensualidad", True))
        Me.MensualidadTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensualidadTextBox1.Location = New System.Drawing.Point(860, 1)
        Me.MensualidadTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MensualidadTextBox1.Name = "MensualidadTextBox1"
        Me.MensualidadTextBox1.ReadOnly = True
        Me.MensualidadTextBox1.Size = New System.Drawing.Size(12, 24)
        Me.MensualidadTextBox1.TabIndex = 28
        Me.MensualidadTextBox1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.ConDetPaqueteAdicionalDataGridView)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Location = New System.Drawing.Point(40, 266)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(873, 527)
        Me.Panel1.TabIndex = 33
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(69, 27)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(198, 18)
        Me.Label2.TabIndex = 44
        Me.Label2.Text = "Costo con Plan Tarifario:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.TextBox2)
        Me.Panel3.Controls.Add(ContratacionLabel1)
        Me.Panel3.Controls.Add(Me.TextBox1)
        Me.Panel3.Controls.Add(MensualidadLabel1)
        Me.Panel3.Location = New System.Drawing.Point(408, 65)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(385, 65)
        Me.Panel3.TabIndex = 43
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(197, 31)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(167, 24)
        Me.TextBox2.TabIndex = 41
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(21, 31)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(167, 24)
        Me.TextBox1.TabIndex = 40
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(DescripcionLabel1)
        Me.Panel2.Controls.Add(Me.DescripcionComboBox1)
        Me.Panel2.Location = New System.Drawing.Point(73, 65)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(327, 65)
        Me.Panel2.TabIndex = 42
        '
        'DescripcionComboBox1
        '
        Me.DescripcionComboBox1.DataSource = Me.MuestraServiciosEricBindingSource
        Me.DescripcionComboBox1.DisplayMember = "Descripcion"
        Me.DescripcionComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox1.FormattingEnabled = True
        Me.DescripcionComboBox1.Location = New System.Drawing.Point(23, 28)
        Me.DescripcionComboBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionComboBox1.Name = "DescripcionComboBox1"
        Me.DescripcionComboBox1.Size = New System.Drawing.Size(299, 26)
        Me.DescripcionComboBox1.TabIndex = 1
        Me.DescripcionComboBox1.ValueMember = "Clv_Servicio"
        '
        'MuestraServiciosEricBindingSource
        '
        Me.MuestraServiciosEricBindingSource.DataMember = "MuestraServiciosEric"
        Me.MuestraServiciosEricBindingSource.DataSource = Me.DataSetEric2
        '
        'Button5
        '
        Me.Button5.Enabled = False
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(597, 162)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(104, 39)
        Me.Button5.TabIndex = 38
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(484, 162)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(104, 39)
        Me.Button4.TabIndex = 37
        Me.Button4.Text = "&Eliminar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Enabled = False
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(372, 162)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(104, 39)
        Me.Button3.TabIndex = 36
        Me.Button3.Text = "&Guardar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Enabled = False
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(257, 164)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(104, 39)
        Me.Button2.TabIndex = 35
        Me.Button2.Text = "&Modificar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(145, 164)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(104, 38)
        Me.Button1.TabIndex = 34
        Me.Button1.Text = "&Nuevo"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(732, 806)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 44)
        Me.Button6.TabIndex = 39
        Me.Button6.Text = "&SALIR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'NuePaqueteAdicionalBindingSource
        '
        Me.NuePaqueteAdicionalBindingSource.DataMember = "NuePaqueteAdicional"
        Me.NuePaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'NuePaqueteAdicionalTableAdapter
        '
        Me.NuePaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.NumericUpDown1)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Controls.Add(Me.TextBox3)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.PaqueteAdicionalTextBox)
        Me.Panel4.Controls.Add(DescripcionLabel)
        Me.Panel4.Controls.Add(Me.DescripcionComboBox)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Controls.Add(PaqueteAdicionalLabel)
        Me.Panel4.Controls.Add(Clv_PaqAdiLabel)
        Me.Panel4.Controls.Add(Me.NumeroTextBox)
        Me.Panel4.Controls.Add(Me.Clv_PaqAdiTextBox)
        Me.Panel4.Location = New System.Drawing.Point(40, 34)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(873, 231)
        Me.Panel4.TabIndex = 40
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConPaqueteAdicionalBindingSource, "Minutos", True))
        Me.NumericUpDown1.Location = New System.Drawing.Point(741, 148)
        Me.NumericUpDown1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(71, 22)
        Me.NumericUpDown1.TabIndex = 49
        Me.NumericUpDown1.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(563, 148)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(150, 18)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Minutos Incluidos :"
        Me.Label5.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(216, 187)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 18)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Costo :"
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Costo", True))
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(291, 183)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(145, 24)
        Me.TextBox3.TabIndex = 46
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(69, 16)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(147, 18)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "Paquete Adicional:"
        '
        'PaqueteAdicionalTextBox
        '
        Me.PaqueteAdicionalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "PaqueteAdicional", True))
        Me.PaqueteAdicionalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PaqueteAdicionalTextBox.Location = New System.Drawing.Point(291, 74)
        Me.PaqueteAdicionalTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PaqueteAdicionalTextBox.Name = "PaqueteAdicionalTextBox"
        Me.PaqueteAdicionalTextBox.Size = New System.Drawing.Size(275, 24)
        Me.PaqueteAdicionalTextBox.TabIndex = 23
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.DataSource = Me.MuestraTipo_Paquetes_AdicionalesBindingSource
        Me.DescripcionComboBox.DisplayMember = "Descripcion"
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(291, 107)
        Me.DescripcionComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(275, 26)
        Me.DescripcionComboBox.TabIndex = 21
        Me.DescripcionComboBox.ValueMember = "Clv_Tipo_Paquete_Adiocional"
        '
        'MuestraTipo_Paquetes_AdicionalesBindingSource
        '
        Me.MuestraTipo_Paquetes_AdicionalesBindingSource.DataMember = "MuestraTipo_Paquetes_Adicionales"
        Me.MuestraTipo_Paquetes_AdicionalesBindingSource.DataSource = Me.DataSetEric2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(201, 154)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 18)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Xxxxx:"
        '
        'Tipo_CobroTextBox
        '
        Me.Tipo_CobroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTipo_Paquetes_AdicionalesBindingSource, "Tipo_Cobro", True))
        Me.Tipo_CobroTextBox.Location = New System.Drawing.Point(1024, 6)
        Me.Tipo_CobroTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Tipo_CobroTextBox.Name = "Tipo_CobroTextBox"
        Me.Tipo_CobroTextBox.ReadOnly = True
        Me.Tipo_CobroTextBox.Size = New System.Drawing.Size(12, 22)
        Me.Tipo_CobroTextBox.TabIndex = 22
        Me.Tipo_CobroTextBox.TabStop = False
        '
        'MuestraTipo_Paquetes_AdicionalesTableAdapter
        '
        Me.MuestraTipo_Paquetes_AdicionalesTableAdapter.ClearBeforeFill = True
        '
        'MuestraServiciosEricTableAdapter
        '
        Me.MuestraServiciosEricTableAdapter.ClearBeforeFill = True
        '
        'FrmPaqueteAdicional
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(936, 874)
        Me.Controls.Add(Me.ConPaqueteAdicionalBindingNavigator)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MensualidadTextBox1)
        Me.Controls.Add(Me.TipoCobroTextBox)
        Me.Controls.Add(Me.MensualidadTextBox)
        Me.Controls.Add(MensualidadLabel)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.ContratacionTextBox)
        Me.Controls.Add(Me.Clv_Tipo_Paquete_AdiocionalTextBox)
        Me.Controls.Add(ContratacionLabel)
        Me.Controls.Add(Me.Tipo_CobroTextBox)
        Me.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Controls.Add(Me.ContratacionTextBox1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmPaqueteAdicional"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Paquete Adicional"
        CType(Me.ConPaqueteAdicionalBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConPaqueteAdicionalBindingNavigator.ResumeLayout(False)
        Me.ConPaqueteAdicionalBindingNavigator.PerformLayout()
        CType(Me.ConPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConDetPaqueteAdicionalDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModDetPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorDetPaqueteActivadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.MuestraServiciosEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NuePaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipo_Paquetes_AdicionalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.ConPaqueteAdicionalTableAdapter
    Friend WithEvents ConPaqueteAdicionalBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConPaqueteAdicionalBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_PaqAdiTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_Tipo_Paquete_AdiocionalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TipoCobroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MensualidadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ModPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.ModPaqueteAdicionalTableAdapter
    Friend WithEvents BorPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.BorPaqueteAdicionalTableAdapter
    Friend WithEvents ConDetPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConDetPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.ConDetPaqueteAdicionalTableAdapter
    Friend WithEvents ConDetPaqueteAdicionalDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents NueDetPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueDetPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.NueDetPaqueteAdicionalTableAdapter
    Friend WithEvents ModDetPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModDetPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.ModDetPaqueteAdicionalTableAdapter
    Friend WithEvents BorDetPaqueteActivadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorDetPaqueteActivadoTableAdapter As sofTV.DataSetEric2TableAdapters.BorDetPaqueteActivadoTableAdapter
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratacionTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents MensualidadTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents NuePaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NuePaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.NuePaqueteAdicionalTableAdapter
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MuestraTipo_Paquetes_AdicionalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipo_Paquetes_AdicionalesTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraTipo_Paquetes_AdicionalesTableAdapter
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraServiciosEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServiciosEricTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraServiciosEricTableAdapter
    Friend WithEvents DescripcionComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Tipo_CobroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PaqueteAdicionalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
End Class
