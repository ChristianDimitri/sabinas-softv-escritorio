﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCablemodems
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_CableModemLabel As System.Windows.Forms.Label
        Dim EstadoAparatoLabel As System.Windows.Forms.Label
        Dim MarcaLabel As System.Windows.Forms.Label
        Dim Fecha_solicitudLabel As System.Windows.Forms.Label
        Dim Fecha_instalacioLabel As System.Windows.Forms.Label
        Dim Fecha_bajaLabel As System.Windows.Forms.Label
        Dim MacCablemodemLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCablemodems))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.ConTecnicoCatCableModemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.mac = New System.Windows.Forms.MaskedTextBox()
        Me.CONAPARATOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.StatusAparatosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.CMBTextBox4 = New System.Windows.Forms.TextBox()
        Me.CONAPARATOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONAPARATOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_CableModemTextBox = New System.Windows.Forms.TextBox()
        Me.MarcaTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_solicitudTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_instalacioTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_bajaTextBox = New System.Windows.Forms.TextBox()
        Me.EstadoAparatoTextBox = New System.Windows.Forms.TextBox()
        Me.TipoAparatoTextBox = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.MaskedTextBox1 = New System.Windows.Forms.MaskedTextBox()
        Me.TiposAparatosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CONAPARATOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONAPARATOSTableAdapter()
        Me.DAMECONTRATOS_CABLEMODEMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMECONTRATOS_CABLEMODEMTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DAMECONTRATOS_CABLEMODEMTableAdapter()
        Me.StatusAparatosTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.StatusAparatosTableAdapter()
        Me.TiposAparatosTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.TiposAparatosTableAdapter()
        Me.ModCatCableModemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModCatCableModemTableAdapter = New sofTV.DataSetEricTableAdapters.ModCatCableModemTableAdapter()
        Me.ConTecnicoCatCableModemTableAdapter = New sofTV.DataSetEricTableAdapters.ConTecnicoCatCableModemTableAdapter()
        Clv_CableModemLabel = New System.Windows.Forms.Label()
        EstadoAparatoLabel = New System.Windows.Forms.Label()
        MarcaLabel = New System.Windows.Forms.Label()
        Fecha_solicitudLabel = New System.Windows.Forms.Label()
        Fecha_instalacioLabel = New System.Windows.Forms.Label()
        Fecha_bajaLabel = New System.Windows.Forms.Label()
        MacCablemodemLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.ConTecnicoCatCableModemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.CONAPARATOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusAparatosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONAPARATOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONAPARATOSBindingNavigator.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.TiposAparatosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECONTRATOS_CABLEMODEMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModCatCableModemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_CableModemLabel
        '
        Clv_CableModemLabel.AutoSize = True
        Clv_CableModemLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CableModemLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_CableModemLabel.Location = New System.Drawing.Point(168, 69)
        Clv_CableModemLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_CableModemLabel.Name = "Clv_CableModemLabel"
        Clv_CableModemLabel.Size = New System.Drawing.Size(60, 18)
        Clv_CableModemLabel.TabIndex = 0
        Clv_CableModemLabel.Text = "Clave :"
        '
        'EstadoAparatoLabel
        '
        EstadoAparatoLabel.AutoSize = True
        EstadoAparatoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EstadoAparatoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        EstadoAparatoLabel.Location = New System.Drawing.Point(91, 172)
        EstadoAparatoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        EstadoAparatoLabel.Name = "EstadoAparatoLabel"
        EstadoAparatoLabel.Size = New System.Drawing.Size(134, 18)
        EstadoAparatoLabel.TabIndex = 4
        EstadoAparatoLabel.Text = "Estado Aparato :"
        '
        'MarcaLabel
        '
        MarcaLabel.AutoSize = True
        MarcaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MarcaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        MarcaLabel.Location = New System.Drawing.Point(167, 135)
        MarcaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        MarcaLabel.Name = "MarcaLabel"
        MarcaLabel.Size = New System.Drawing.Size(65, 18)
        MarcaLabel.TabIndex = 6
        MarcaLabel.Text = "Marca :"
        '
        'Fecha_solicitudLabel
        '
        Fecha_solicitudLabel.AutoSize = True
        Fecha_solicitudLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_solicitudLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_solicitudLabel.Location = New System.Drawing.Point(581, 105)
        Fecha_solicitudLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Fecha_solicitudLabel.Name = "Fecha_solicitudLabel"
        Fecha_solicitudLabel.Size = New System.Drawing.Size(83, 18)
        Fecha_solicitudLabel.TabIndex = 8
        Fecha_solicitudLabel.Text = "Solicitud :"
        '
        'Fecha_instalacioLabel
        '
        Fecha_instalacioLabel.AutoSize = True
        Fecha_instalacioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_instalacioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_instalacioLabel.Location = New System.Drawing.Point(563, 138)
        Fecha_instalacioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Fecha_instalacioLabel.Name = "Fecha_instalacioLabel"
        Fecha_instalacioLabel.Size = New System.Drawing.Size(99, 18)
        Fecha_instalacioLabel.TabIndex = 10
        Fecha_instalacioLabel.Text = "Instalación :"
        '
        'Fecha_bajaLabel
        '
        Fecha_bajaLabel.AutoSize = True
        Fecha_bajaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_bajaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_bajaLabel.Location = New System.Drawing.Point(617, 169)
        Fecha_bajaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Fecha_bajaLabel.Name = "Fecha_bajaLabel"
        Fecha_bajaLabel.Size = New System.Drawing.Size(51, 18)
        Fecha_bajaLabel.TabIndex = 12
        Fecha_bajaLabel.Text = "Baja :"
        '
        'MacCablemodemLabel
        '
        MacCablemodemLabel.AutoSize = True
        MacCablemodemLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MacCablemodemLabel.ForeColor = System.Drawing.Color.LightSlateGray
        MacCablemodemLabel.Location = New System.Drawing.Point(24, 5)
        MacCablemodemLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        MacCablemodemLabel.Name = "MacCablemodemLabel"
        MacCablemodemLabel.Size = New System.Drawing.Size(181, 18)
        MacCablemodemLabel.TabIndex = 14
        MacCablemodemLabel.Text = "Mac del Cablemodem :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(131, 6)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(89, 18)
        Label2.TabIndex = 14
        Label2.Text = "No. Serie :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(148, 210)
        NombreLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(78, 18)
        NombreLabel.TabIndex = 200
        NombreLabel.Text = "Técnico :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.ComboBox2)
        Me.Panel1.Controls.Add(Me.CMBLabel1)
        Me.Panel1.Controls.Add(Me.TreeView1)
        Me.Panel1.Controls.Add(Me.CMBTextBox4)
        Me.Panel1.Controls.Add(Me.CONAPARATOSBindingNavigator)
        Me.Panel1.Controls.Add(Clv_CableModemLabel)
        Me.Panel1.Controls.Add(Me.Clv_CableModemTextBox)
        Me.Panel1.Controls.Add(EstadoAparatoLabel)
        Me.Panel1.Controls.Add(MarcaLabel)
        Me.Panel1.Controls.Add(Me.MarcaTextBox)
        Me.Panel1.Controls.Add(Fecha_solicitudLabel)
        Me.Panel1.Controls.Add(Me.Fecha_solicitudTextBox)
        Me.Panel1.Controls.Add(Fecha_instalacioLabel)
        Me.Panel1.Controls.Add(Me.Fecha_instalacioTextBox)
        Me.Panel1.Controls.Add(Fecha_bajaLabel)
        Me.Panel1.Controls.Add(Me.Fecha_bajaTextBox)
        Me.Panel1.Controls.Add(Me.EstadoAparatoTextBox)
        Me.Panel1.Controls.Add(Me.TipoAparatoTextBox)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Location = New System.Drawing.Point(16, 15)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(845, 450)
        Me.Panel1.TabIndex = 0
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTecnicoCatCableModemBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(243, 203)
        Me.NombreTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.ReadOnly = True
        Me.NombreTextBox.Size = New System.Drawing.Size(339, 24)
        Me.NombreTextBox.TabIndex = 201
        Me.NombreTextBox.TabStop = False
        '
        'ConTecnicoCatCableModemBindingSource
        '
        Me.ConTecnicoCatCableModemBindingSource.DataMember = "ConTecnicoCatCableModem"
        Me.ConTecnicoCatCableModemBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.EnforceConstraints = False
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.mac)
        Me.Panel2.Controls.Add(MacCablemodemLabel)
        Me.Panel2.Location = New System.Drawing.Point(4, 97)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(517, 31)
        Me.Panel2.TabIndex = 53
        '
        'mac
        '
        Me.mac.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mac.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONAPARATOSBindingSource, "MacCablemodem", True))
        Me.mac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mac.Location = New System.Drawing.Point(239, 4)
        Me.mac.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.mac.Mask = "aaaa.aaaa.aaaa"
        Me.mac.Name = "mac"
        Me.mac.ReadOnly = True
        Me.mac.Size = New System.Drawing.Size(165, 24)
        Me.mac.TabIndex = 10
        Me.mac.TabStop = False
        Me.mac.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals
        '
        'CONAPARATOSBindingSource
        '
        Me.CONAPARATOSBindingSource.DataMember = "CONAPARATOS"
        Me.CONAPARATOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox2
        '
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONAPARATOSBindingSource, "EstadoAparato", True))
        Me.ComboBox2.DataSource = Me.StatusAparatosBindingSource
        Me.ComboBox2.DisplayMember = "Concepto"
        Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(243, 169)
        Me.ComboBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(208, 26)
        Me.ComboBox2.TabIndex = 100
        Me.ComboBox2.ValueMember = "Clv_StatusCableModem"
        '
        'StatusAparatosBindingSource
        '
        Me.StatusAparatosBindingSource.DataMember = "StatusAparatos"
        Me.StatusAparatosBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.OrangeRed
        Me.CMBLabel1.Location = New System.Drawing.Point(56, 254)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(512, 20)
        Me.CMBLabel1.TabIndex = 50
        Me.CMBLabel1.Text = "# de Contrato en los que el Cablemodem a estado Asigando"
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(60, 277)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(255, 153)
        Me.TreeView1.TabIndex = 49
        Me.TreeView1.TabStop = False
        '
        'CMBTextBox4
        '
        Me.CMBTextBox4.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox4.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox4.Location = New System.Drawing.Point(529, 65)
        Me.CMBTextBox4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTextBox4.Name = "CMBTextBox4"
        Me.CMBTextBox4.ReadOnly = True
        Me.CMBTextBox4.Size = New System.Drawing.Size(288, 23)
        Me.CMBTextBox4.TabIndex = 48
        Me.CMBTextBox4.TabStop = False
        Me.CMBTextBox4.Text = "Fechas de "
        Me.CMBTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CONAPARATOSBindingNavigator
        '
        Me.CONAPARATOSBindingNavigator.AddNewItem = Nothing
        Me.CONAPARATOSBindingNavigator.BindingSource = Me.CONAPARATOSBindingSource
        Me.CONAPARATOSBindingNavigator.CountItem = Nothing
        Me.CONAPARATOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONAPARATOSBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONAPARATOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONAPARATOSBindingNavigatorSaveItem})
        Me.CONAPARATOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONAPARATOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONAPARATOSBindingNavigator.MoveLastItem = Nothing
        Me.CONAPARATOSBindingNavigator.MoveNextItem = Nothing
        Me.CONAPARATOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONAPARATOSBindingNavigator.Name = "CONAPARATOSBindingNavigator"
        Me.CONAPARATOSBindingNavigator.PositionItem = Nothing
        Me.CONAPARATOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONAPARATOSBindingNavigator.Size = New System.Drawing.Size(845, 28)
        Me.CONAPARATOSBindingNavigator.TabIndex = 200
        Me.CONAPARATOSBindingNavigator.TabStop = True
        Me.CONAPARATOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Enabled = False
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(122, 25)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        Me.BindingNavigatorDeleteItem.Visible = False
        '
        'CONAPARATOSBindingNavigatorSaveItem
        '
        Me.CONAPARATOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONAPARATOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONAPARATOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONAPARATOSBindingNavigatorSaveItem.Name = "CONAPARATOSBindingNavigatorSaveItem"
        Me.CONAPARATOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 25)
        Me.CONAPARATOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_CableModemTextBox
        '
        Me.Clv_CableModemTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_CableModemTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONAPARATOSBindingSource, "Clv_CableModem", True))
        Me.Clv_CableModemTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CableModemTextBox.Location = New System.Drawing.Point(243, 66)
        Me.Clv_CableModemTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_CableModemTextBox.Name = "Clv_CableModemTextBox"
        Me.Clv_CableModemTextBox.ReadOnly = True
        Me.Clv_CableModemTextBox.Size = New System.Drawing.Size(133, 24)
        Me.Clv_CableModemTextBox.TabIndex = 1
        Me.Clv_CableModemTextBox.TabStop = False
        '
        'MarcaTextBox
        '
        Me.MarcaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MarcaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONAPARATOSBindingSource, "Marca", True))
        Me.MarcaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MarcaTextBox.Location = New System.Drawing.Point(243, 133)
        Me.MarcaTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MarcaTextBox.Name = "MarcaTextBox"
        Me.MarcaTextBox.ReadOnly = True
        Me.MarcaTextBox.Size = New System.Drawing.Size(133, 24)
        Me.MarcaTextBox.TabIndex = 99
        Me.MarcaTextBox.TabStop = False
        '
        'Fecha_solicitudTextBox
        '
        Me.Fecha_solicitudTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_solicitudTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONAPARATOSBindingSource, "fecha_solicitud", True))
        Me.Fecha_solicitudTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_solicitudTextBox.Location = New System.Drawing.Point(684, 102)
        Me.Fecha_solicitudTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Fecha_solicitudTextBox.Name = "Fecha_solicitudTextBox"
        Me.Fecha_solicitudTextBox.ReadOnly = True
        Me.Fecha_solicitudTextBox.Size = New System.Drawing.Size(133, 24)
        Me.Fecha_solicitudTextBox.TabIndex = 5
        Me.Fecha_solicitudTextBox.TabStop = False
        '
        'Fecha_instalacioTextBox
        '
        Me.Fecha_instalacioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_instalacioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONAPARATOSBindingSource, "fecha_instalacio", True))
        Me.Fecha_instalacioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_instalacioTextBox.Location = New System.Drawing.Point(684, 135)
        Me.Fecha_instalacioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Fecha_instalacioTextBox.Name = "Fecha_instalacioTextBox"
        Me.Fecha_instalacioTextBox.ReadOnly = True
        Me.Fecha_instalacioTextBox.Size = New System.Drawing.Size(133, 24)
        Me.Fecha_instalacioTextBox.TabIndex = 6
        Me.Fecha_instalacioTextBox.TabStop = False
        '
        'Fecha_bajaTextBox
        '
        Me.Fecha_bajaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_bajaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONAPARATOSBindingSource, "fecha_baja", True))
        Me.Fecha_bajaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_bajaTextBox.Location = New System.Drawing.Point(684, 169)
        Me.Fecha_bajaTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Fecha_bajaTextBox.Name = "Fecha_bajaTextBox"
        Me.Fecha_bajaTextBox.ReadOnly = True
        Me.Fecha_bajaTextBox.Size = New System.Drawing.Size(133, 24)
        Me.Fecha_bajaTextBox.TabIndex = 7
        Me.Fecha_bajaTextBox.TabStop = False
        '
        'EstadoAparatoTextBox
        '
        Me.EstadoAparatoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EstadoAparatoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONAPARATOSBindingSource, "EstadoAparato", True))
        Me.EstadoAparatoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EstadoAparatoTextBox.Location = New System.Drawing.Point(408, 171)
        Me.EstadoAparatoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.EstadoAparatoTextBox.Name = "EstadoAparatoTextBox"
        Me.EstadoAparatoTextBox.Size = New System.Drawing.Size(26, 24)
        Me.EstadoAparatoTextBox.TabIndex = 4
        Me.EstadoAparatoTextBox.TabStop = False
        '
        'TipoAparatoTextBox
        '
        Me.TipoAparatoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TipoAparatoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TipoAparatoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONAPARATOSBindingSource, "TipoAparato", True))
        Me.TipoAparatoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoAparatoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.TipoAparatoTextBox.Location = New System.Drawing.Point(425, 34)
        Me.TipoAparatoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TipoAparatoTextBox.Name = "TipoAparatoTextBox"
        Me.TipoAparatoTextBox.Size = New System.Drawing.Size(27, 17)
        Me.TipoAparatoTextBox.TabIndex = 3
        Me.TipoAparatoTextBox.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.MaskedTextBox1)
        Me.Panel3.Controls.Add(Label2)
        Me.Panel3.Location = New System.Drawing.Point(4, 97)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(517, 31)
        Me.Panel3.TabIndex = 54
        '
        'MaskedTextBox1
        '
        Me.MaskedTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MaskedTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONAPARATOSBindingSource, "MacCablemodem", True))
        Me.MaskedTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaskedTextBox1.Location = New System.Drawing.Point(239, 4)
        Me.MaskedTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaskedTextBox1.Mask = "99999999999999999999"
        Me.MaskedTextBox1.Name = "MaskedTextBox1"
        Me.MaskedTextBox1.ReadOnly = True
        Me.MaskedTextBox1.Size = New System.Drawing.Size(237, 24)
        Me.MaskedTextBox1.TabIndex = 10
        Me.MaskedTextBox1.TabStop = False
        '
        'TiposAparatosBindingSource
        '
        Me.TiposAparatosBindingSource.DataMember = "TiposAparatos"
        Me.TiposAparatosBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(680, 492)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 300
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONAPARATOSTableAdapter
        '
        Me.CONAPARATOSTableAdapter.ClearBeforeFill = True
        '
        'DAMECONTRATOS_CABLEMODEMBindingSource
        '
        Me.DAMECONTRATOS_CABLEMODEMBindingSource.DataMember = "DAMECONTRATOS_CABLEMODEM"
        Me.DAMECONTRATOS_CABLEMODEMBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DAMECONTRATOS_CABLEMODEMTableAdapter
        '
        Me.DAMECONTRATOS_CABLEMODEMTableAdapter.ClearBeforeFill = True
        '
        'StatusAparatosTableAdapter
        '
        Me.StatusAparatosTableAdapter.ClearBeforeFill = True
        '
        'TiposAparatosTableAdapter
        '
        Me.TiposAparatosTableAdapter.ClearBeforeFill = True
        '
        'ModCatCableModemBindingSource
        '
        Me.ModCatCableModemBindingSource.DataMember = "ModCatCableModem"
        Me.ModCatCableModemBindingSource.DataSource = Me.DataSetEric
        '
        'ModCatCableModemTableAdapter
        '
        Me.ModCatCableModemTableAdapter.ClearBeforeFill = True
        '
        'ConTecnicoCatCableModemTableAdapter
        '
        Me.ConTecnicoCatCableModemTableAdapter.ClearBeforeFill = True
        '
        'FrmCablemodems
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(888, 550)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmCablemodems"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Cablemodems"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConTecnicoCatCableModemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.CONAPARATOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusAparatosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONAPARATOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONAPARATOSBindingNavigator.ResumeLayout(False)
        Me.CONAPARATOSBindingNavigator.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.TiposAparatosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECONTRATOS_CABLEMODEMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModCatCableModemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONAPARATOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONAPARATOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONAPARATOSTableAdapter
    Friend WithEvents Clv_CableModemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TipoAparatoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EstadoAparatoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MarcaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_solicitudTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_instalacioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_bajaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONAPARATOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONAPARATOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents mac As System.Windows.Forms.MaskedTextBox
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents CMBTextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents DAMECONTRATOS_CABLEMODEMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECONTRATOS_CABLEMODEMTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DAMECONTRATOS_CABLEMODEMTableAdapter
    Friend WithEvents StatusAparatosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusAparatosTableAdapter As sofTV.NewSofTvDataSetTableAdapters.StatusAparatosTableAdapter
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents TiposAparatosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TiposAparatosTableAdapter As sofTV.NewSofTvDataSetTableAdapters.TiposAparatosTableAdapter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents MaskedTextBox1 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ModCatCableModemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModCatCableModemTableAdapter As sofTV.DataSetEricTableAdapters.ModCatCableModemTableAdapter
    Friend WithEvents ConTecnicoCatCableModemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConTecnicoCatCableModemTableAdapter As sofTV.DataSetEricTableAdapters.ConTecnicoCatCableModemTableAdapter
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
End Class
