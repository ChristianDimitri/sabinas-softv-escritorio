<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRelCablemodemClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Contratonet = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MacCableModem = New System.Windows.Forms.TextBox()
        Me.Contratonet1 = New System.Windows.Forms.TextBox()
        Me.MacCableModem1 = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.MUESTRACABLEMODEMSDELCLIBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMSDELCLITableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLITableAdapter()
        Me.MUESTRACONTNETBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTNETTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter()
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter()
        Me.BorICABMSOLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorICABM_SOLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.borICABM_SOLTableAdapter()
        Me.NUEICABMSOLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEICABM_SOLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUEICABM_SOLTableAdapter()
        Me.MUESTRAICAMporSOLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAICAM_porSOLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRAICAM_porSOLTableAdapter()
        Me.BUSCADetOrdSerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BUSCADetOrdSerTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCADetOrdSerTableAdapter()
        Me.BORDetOrdSerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSerTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSerTableAdapter()
        Me.BORDetOrdSer_INTELIGENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSer_INTELIGENTETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.GuardaMotivoCanServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GuardaMotivoCanServTableAdapter = New sofTV.DataSetEricTableAdapters.GuardaMotivoCanServTableAdapter()
        Me.BorraMotivoCanServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraMotivoCanServTableAdapter = New sofTV.DataSetEricTableAdapters.BorraMotivoCanServTableAdapter()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTNETBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorICABMSOLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEICABMSOLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAICAMporSOLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCADetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GuardaMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorraMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.ForeColor = System.Drawing.Color.Navy
        Me.TreeView1.Location = New System.Drawing.Point(16, 39)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(389, 331)
        Me.TreeView1.TabIndex = 1
        Me.TreeView1.TabStop = False
        '
        'TreeView2
        '
        Me.TreeView2.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView2.ForeColor = System.Drawing.Color.Navy
        Me.TreeView2.Location = New System.Drawing.Point(675, 39)
        Me.TreeView2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(404, 331)
        Me.TreeView2.TabIndex = 2
        Me.TreeView2.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(441, 46)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(191, 37)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&Agregar >"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(441, 90)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(191, 37)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "< &Quitar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Contratonet
        '
        Me.Contratonet.Location = New System.Drawing.Point(107, 113)
        Me.Contratonet.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Contratonet.Name = "Contratonet"
        Me.Contratonet.Size = New System.Drawing.Size(168, 22)
        Me.Contratonet.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(44, 84)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 17)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "ContratoNet"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(237, 122)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 17)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Mac Cablemodem"
        '
        'MacCableModem
        '
        Me.MacCableModem.Location = New System.Drawing.Point(215, 185)
        Me.MacCableModem.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MacCableModem.Name = "MacCableModem"
        Me.MacCableModem.Size = New System.Drawing.Size(144, 22)
        Me.MacCableModem.TabIndex = 8
        Me.MacCableModem.TabStop = False
        '
        'Contratonet1
        '
        Me.Contratonet1.Location = New System.Drawing.Point(704, 161)
        Me.Contratonet1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Contratonet1.Name = "Contratonet1"
        Me.Contratonet1.Size = New System.Drawing.Size(168, 22)
        Me.Contratonet1.TabIndex = 9
        '
        'MacCableModem1
        '
        Me.MacCableModem1.Location = New System.Drawing.Point(704, 193)
        Me.MacCableModem1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MacCableModem1.Name = "MacCableModem1"
        Me.MacCableModem1.Size = New System.Drawing.Size(168, 22)
        Me.MacCableModem1.TabIndex = 10
        Me.MacCableModem1.TabStop = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.Control
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(441, 134)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(191, 37)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Agregar &Todo >>"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.SystemColors.Control
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(441, 181)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(191, 37)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "<< Quitar To&do "
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(16, 16)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(326, 20)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Cablemodems del Cliente por Asignar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(671, 16)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(349, 20)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Instalar y Asignar a estos Cablemodems"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(897, 398)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRACABLEMODEMSDELCLIBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLIBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI"
        Me.MUESTRACABLEMODEMSDELCLIBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACABLEMODEMSDELCLITableAdapter
        '
        Me.MUESTRACABLEMODEMSDELCLITableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTNETBindingSource
        '
        Me.MUESTRACONTNETBindingSource.DataMember = "MUESTRACONTNET"
        Me.MUESTRACONTNETBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACONTNETTableAdapter
        '
        Me.MUESTRACONTNETTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI_porOpcion"
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.ClearBeforeFill = True
        '
        'BorICABMSOLBindingSource
        '
        Me.BorICABMSOLBindingSource.DataMember = "borICABM_SOL"
        Me.BorICABMSOLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BorICABM_SOLTableAdapter
        '
        Me.BorICABM_SOLTableAdapter.ClearBeforeFill = True
        '
        'NUEICABMSOLBindingSource
        '
        Me.NUEICABMSOLBindingSource.DataMember = "NUEICABM_SOL"
        Me.NUEICABMSOLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUEICABM_SOLTableAdapter
        '
        Me.NUEICABM_SOLTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAICAMporSOLBindingSource
        '
        Me.MUESTRAICAMporSOLBindingSource.DataMember = "MUESTRAICAM_porSOL"
        Me.MUESTRAICAMporSOLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRAICAM_porSOLTableAdapter
        '
        Me.MUESTRAICAM_porSOLTableAdapter.ClearBeforeFill = True
        '
        'BUSCADetOrdSerBindingSource
        '
        Me.BUSCADetOrdSerBindingSource.DataMember = "BUSCADetOrdSer"
        Me.BUSCADetOrdSerBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BUSCADetOrdSerTableAdapter
        '
        Me.BUSCADetOrdSerTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSerBindingSource
        '
        Me.BORDetOrdSerBindingSource.DataMember = "BORDetOrdSer"
        Me.BORDetOrdSerBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSerTableAdapter
        '
        Me.BORDetOrdSerTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSer_INTELIGENTEBindingSource
        '
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataMember = "BORDetOrdSer_INTELIGENTE"
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSer_INTELIGENTETableAdapter
        '
        Me.BORDetOrdSer_INTELIGENTETableAdapter.ClearBeforeFill = True
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.EnforceConstraints = False
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GuardaMotivoCanServBindingSource
        '
        Me.GuardaMotivoCanServBindingSource.DataMember = "GuardaMotivoCanServ"
        Me.GuardaMotivoCanServBindingSource.DataSource = Me.DataSetEric
        '
        'GuardaMotivoCanServTableAdapter
        '
        Me.GuardaMotivoCanServTableAdapter.ClearBeforeFill = True
        '
        'BorraMotivoCanServBindingSource
        '
        Me.BorraMotivoCanServBindingSource.DataMember = "BorraMotivoCanServ"
        Me.BorraMotivoCanServBindingSource.DataSource = Me.DataSetEric
        '
        'BorraMotivoCanServTableAdapter
        '
        Me.BorraMotivoCanServTableAdapter.ClearBeforeFill = True
        '
        'FrmRelCablemodemClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1105, 475)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.TreeView2)
        Me.Controls.Add(Me.MacCableModem1)
        Me.Controls.Add(Me.Contratonet1)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.MacCableModem)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Contratonet)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmRelCablemodemClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Instalación de Cablemodems"
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTNETBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorICABMSOLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEICABMSOLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAICAMporSOLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCADetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GuardaMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorraMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRACABLEMODEMSDELCLIBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMSDELCLITableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLITableAdapter
    Friend WithEvents MUESTRACONTNETBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTNETTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Contratonet As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents MacCableModem As System.Windows.Forms.TextBox
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter
    Friend WithEvents BorICABMSOLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorICABM_SOLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.borICABM_SOLTableAdapter
    Friend WithEvents NUEICABMSOLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEICABM_SOLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUEICABM_SOLTableAdapter
    Friend WithEvents MUESTRAICAMporSOLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAICAM_porSOLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAICAM_porSOLTableAdapter
    Friend WithEvents Contratonet1 As System.Windows.Forms.TextBox
    Friend WithEvents MacCableModem1 As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents BUSCADetOrdSerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCADetOrdSerTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCADetOrdSerTableAdapter
    Friend WithEvents BORDetOrdSerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSerTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSerTableAdapter
    Friend WithEvents BORDetOrdSer_INTELIGENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSer_INTELIGENTETableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents GuardaMotivoCanServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GuardaMotivoCanServTableAdapter As sofTV.DataSetEricTableAdapters.GuardaMotivoCanServTableAdapter
    Friend WithEvents BorraMotivoCanServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraMotivoCanServTableAdapter As sofTV.DataSetEricTableAdapters.BorraMotivoCanServTableAdapter
End Class
