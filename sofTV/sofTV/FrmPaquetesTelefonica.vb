﻿Imports System.Data.SqlClient
Public Class FrmPaquetesTelefonica
    Public opcionP As String
    Public clv_paquete As Integer
    Public locIdBeam As Integer

    Private Sub FrmLocalidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        ComboBoxProveedor.DataSource = BaseII.ConsultaDT("sp_dameProveedoresInternet")
        'MuestraServiciosBase2(2, 1)
        BaseII.limpiaParametros()
        cbCobertura.DataSource = BaseII.ConsultaDT("DameCoberturas")

        Servicio.DataSource = MuestraServiciosBase2Dataset(2, 1)
        Servicio.ValueMember = "Clv_Servicio"
        Servicio.DisplayMember = "Descripcion"


        If opcionP = "N" Then
            BindingNavigatorDeleteItem.Enabled = False
            tbidBeam_Paquete.Text = "0"
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("DameClv_Session")
            LocClv_session = BaseII.dicoPar("@Clv_Session")
            llenarComboPlaza()
        ElseIf opcionP = "M" Then
            BindingNavigatorDeleteItem.Enabled = True
            CheckBoxEsEmpresarial.Enabled = False
            'ComboBoxProveedor.Enabled = False
            'ComboBoxServicio.Enabled = False
            tbidBeam_Paquete.Text = locIdBeam.ToString()
            llenarComboPlaza()
            LlenaDatos()
        ElseIf opcionP = "C" Then
            tbidBeam_Paquete.Text = locIdBeam.ToString()
            llenarComboPlaza()
            LlenaDatos()
            CheckBoxEsEmpresarial.Enabled = False
            ComboBoxPlaza.Enabled = False
            btnAgregar.Enabled = False
            btnEliminar.Enabled = False
            PaquetesBindingNavigator.Enabled = False
            TextBox1.Enabled = False
            ComboBoxProveedor.Enabled = False
            tbBeam.Enabled = False
            cbCobertura.Enabled = False
            'tbnombre.Enabled = False
        End If

    End Sub

    Private Sub llenarComboPlaza()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@idBeam_Paquete", SqlDbType.Int, tbidBeam_Paquete.Text)
        ComboBoxPlaza.DataSource = BaseII.ConsultaDT("sp_damePlazasPorPaquete")
    End Sub

    Private Sub LlenaDatos()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idBeamPaquete", SqlDbType.Int, locIdBeam)
            BaseII.CreateMyParameter("@nombre", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.CreateMyParameter("@idProveedor", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@clv_servicio", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@beamFisico", ParameterDirection.Output, SqlDbType.VarChar, 10)
            BaseII.CreateMyParameter("@EsEmpresarial", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.CreateMyParameter("@idCobertura", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("sp_dameDatosBeam")
            TextBox1.Text = BaseII.dicoPar("@nombre").ToString
            ComboBoxProveedor.SelectedValue = BaseII.dicoPar("@idProveedor")
            ComboBoxServicio.SelectedValue = BaseII.dicoPar("@clv_servicio")
            tbBeam.Text = BaseII.dicoPar("@beamFisico")
            CheckBoxEsEmpresarial.Checked = CBool(BaseII.dicoPar("@EsEmpresarial"))
            cbCobertura.SelectedValue = BaseII.dicoPar("@idCobertura")
            llenaListPlaza()
            llenarComboPlaza()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idBeamPaquete", SqlDbType.Int, tbidBeam_Paquete.Text)
        BaseII.CreateMyParameter("@resultado", ParameterDirection.Output, SqlDbType.VarChar, 200)
        BaseII.ProcedimientoOutPut("sp_eliminaBeamPaquete")
        If BaseII.dicoPar("@resultado").ToString = 1 Then
            MsgBox("Se eliminó correctamente.")
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "", "", "Eliminó Cobertura/Paquete #" + tbidBeam_Paquete.Text, LocClv_Ciudad)
            Me.Close()
            BrwPaquetesTelefonica.entraActivate = 1
        Else
            MsgBox("No se puede eliminar ya que está asignado a algún cliente.")
        End If
    End Sub

    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorSaveItem.Click
        DataGridViewPlazas.EndEdit()
        If TextBox1.Text = "" Then
            MsgBox("Necesita asginar un nombre.")
            Exit Sub
        End If
        If cbCobertura.SelectedValue < 1 Or cbCobertura.Items.Count = 0 Then
            MsgBox("Necesita asignar el beam al cual pertenece.") 'kjbh
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            If opcionP = "N" Then
                BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, TextBox1.Text)
                BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
                BaseII.CreateMyParameter("@idBeamPaquete", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@idProveedor", SqlDbType.Int, ComboBoxProveedor.SelectedValue)
                BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@beamFisico", SqlDbType.VarChar, tbBeam.Text)
                BaseII.CreateMyParameter("@msj", ParameterDirection.Output, SqlDbType.VarChar, 200)
                BaseII.CreateMyParameter("@EsEmpresarial", SqlDbType.Bit, CheckBoxEsEmpresarial.Checked)
                BaseII.CreateMyParameter("@idCobertura", SqlDbType.Int, cbCobertura.SelectedValue)
                BaseII.ProcedimientoOutPut("sp_insertaBeamPaquete")
                tbidBeam_Paquete.Text = BaseII.dicoPar("@idBeamPaquete").ToString
            Else
                opcionP = "M"
                BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, TextBox1.Text)
                BaseII.CreateMyParameter("@idBeamPaquete", SqlDbType.Int, tbidBeam_Paquete.Text)
                BaseII.CreateMyParameter("@idProveedor", SqlDbType.Int, ComboBoxProveedor.SelectedValue)
                BaseII.CreateMyParameter("@beamFisico", SqlDbType.VarChar, tbBeam.Text)
                BaseII.CreateMyParameter("@idCobertura", SqlDbType.Int, cbCobertura.SelectedValue)
                BaseII.CreateMyParameter("@msj", ParameterDirection.Output, SqlDbType.VarChar, 200)
                BaseII.ProcedimientoOutPut("sp_updateBeamPaquete")                
            End If
            MsgBox(BaseII.dicoPar("@msj").ToString)
            BrwPaquetesTelefonica.entraActivate = 1
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        BrwPaquetesTelefonica.entraActivate = 1
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub

    Private Sub FrmLocalidad_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        BrwPaquetesTelefonica.entraActivate = 1
    End Sub

    Private Sub PaquetesBindingNavigator_RefreshItems(sender As Object, e As EventArgs) Handles PaquetesBindingNavigator.RefreshItems

    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        If ComboBoxPlaza.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idPaqueteBeam", SqlDbType.Int, tbidBeam_Paquete.Text)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.Inserta("sp_insertaRelCompaniaBeam")
        llenarComboPlaza()
        llenaListPlaza()
    End Sub

    Private Sub llenaListPlaza()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idPaqueteBeam", SqlDbType.Int, tbidBeam_Paquete.Text)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
        DataGridViewPlazas.DataSource = BaseII.ConsultaDT("sp_damePlazasDeBeam")
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If Not DataGridViewPlazas.CurrentRow.Cells(0).Value > 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idPaqueteBeam", SqlDbType.Int, tbidBeam_Paquete.Text)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, DataGridViewPlazas.CurrentRow.Cells(1).Value)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@id", SqlDbType.BigInt, DataGridViewPlazas.CurrentRow.Cells(0).Value)
        BaseII.Inserta("sp_eliminaRelCompaniaBeam")
        llenarComboPlaza()
        llenaListPlaza()
    End Sub

    Private Sub MuestraServiciosBase2(ByVal tipser As Integer, ByVal idcompania As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_tipser", SqlDbType.Int, tipser)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 1)
        ComboBoxServicio.DataSource = BaseII.ConsultaDT("MuestraServiciosBase2")
    End Sub

    Private Function MuestraServiciosBase2Dataset(ByVal tipser As Integer, ByVal idcompania As Integer) As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_tipser", SqlDbType.Int, tipser)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 1)
        Return BaseII.ConsultaDT("MuestraServiciosBase2ParaBeam")
    End Function


    Private Sub DataGridViewPlazas_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridViewPlazas.CellValueChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdCompania", SqlDbType.Int, CInt(DataGridViewPlazas.Rows(e.RowIndex).Cells(1).Value))
            BaseII.CreateMyParameter("@Id_Beam_Paquete", SqlDbType.Int, tbidBeam_Paquete.Text)
            BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, CInt(DataGridViewPlazas.Rows(e.RowIndex).Cells(3).Value))
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, LocClv_session)
            BaseII.CreateMyParameter("@id", SqlDbType.BigInt, CInt(DataGridViewPlazas.Rows(e.RowIndex).Cells(0).Value))
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("updateRel_CompaniaBeam")
            If CBool(BaseII.dicoPar("@error").ToString) Then
                MsgBox("La plaza y servicio ya se encontraban relacionados anteriormente.", MsgBoxStyle.Exclamation)
                llenaListPlaza()
            End If

            
        Catch ex As System.Exception
        End Try
    End Sub

    Private Sub Servicio_Disposed(sender As Object, e As EventArgs) Handles Servicio.Disposed

    End Sub
End Class