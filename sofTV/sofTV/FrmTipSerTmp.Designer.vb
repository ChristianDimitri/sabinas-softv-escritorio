﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTipServTmp
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bnAceptar = New System.Windows.Forms.Button()
        Me.label1 = New System.Windows.Forms.Label()
        Me.label1CMB = New System.Windows.Forms.Label()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.button4 = New System.Windows.Forms.Button()
        Me.button3 = New System.Windows.Forms.Button()
        Me.button2 = New System.Windows.Forms.Button()
        Me.button1 = New System.Windows.Forms.Button()
        Me.lbDerecha = New System.Windows.Forms.ListBox()
        Me.lbIzquierda = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'bnAceptar
        '
        Me.bnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAceptar.Location = New System.Drawing.Point(477, 386)
        Me.bnAceptar.Name = "bnAceptar"
        Me.bnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.bnAceptar.TabIndex = 14
        Me.bnAceptar.Text = "&ACEPTAR"
        Me.bnAceptar.UseVisualStyleBackColor = True
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(443, 22)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(101, 15)
        Me.label1.TabIndex = 18
        Me.label1.Text = "Seleccionados"
        '
        'label1CMB
        '
        Me.label1CMB.AutoSize = True
        Me.label1CMB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1CMB.Location = New System.Drawing.Point(29, 22)
        Me.label1CMB.Name = "label1CMB"
        Me.label1CMB.Size = New System.Drawing.Size(108, 15)
        Me.label1CMB.TabIndex = 16
        Me.label1CMB.Text = "Sin Seleccionar"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(619, 386)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 15
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'button4
        '
        Me.button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button4.Location = New System.Drawing.Point(350, 252)
        Me.button4.Name = "button4"
        Me.button4.Size = New System.Drawing.Size(64, 25)
        Me.button4.TabIndex = 13
        Me.button4.Text = "<<"
        Me.button4.UseVisualStyleBackColor = True
        '
        'button3
        '
        Me.button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button3.Location = New System.Drawing.Point(350, 223)
        Me.button3.Name = "button3"
        Me.button3.Size = New System.Drawing.Size(64, 25)
        Me.button3.TabIndex = 12
        Me.button3.Text = "<"
        Me.button3.UseVisualStyleBackColor = True
        '
        'button2
        '
        Me.button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button2.Location = New System.Drawing.Point(350, 127)
        Me.button2.Name = "button2"
        Me.button2.Size = New System.Drawing.Size(64, 25)
        Me.button2.TabIndex = 11
        Me.button2.Text = ">>"
        Me.button2.UseVisualStyleBackColor = True
        '
        'button1
        '
        Me.button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button1.Location = New System.Drawing.Point(350, 98)
        Me.button1.Name = "button1"
        Me.button1.Size = New System.Drawing.Size(64, 25)
        Me.button1.TabIndex = 9
        Me.button1.Text = ">"
        Me.button1.UseVisualStyleBackColor = True
        '
        'lbDerecha
        '
        Me.lbDerecha.DisplayMember = "Concepto"
        Me.lbDerecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbDerecha.FormattingEnabled = True
        Me.lbDerecha.ItemHeight = 15
        Me.lbDerecha.Location = New System.Drawing.Point(446, 40)
        Me.lbDerecha.Name = "lbDerecha"
        Me.lbDerecha.Size = New System.Drawing.Size(276, 319)
        Me.lbDerecha.TabIndex = 20
        Me.lbDerecha.TabStop = False
        Me.lbDerecha.ValueMember = "Clv_TipSer"
        '
        'lbIzquierda
        '
        Me.lbIzquierda.DisplayMember = "Concepto"
        Me.lbIzquierda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbIzquierda.FormattingEnabled = True
        Me.lbIzquierda.ItemHeight = 15
        Me.lbIzquierda.Location = New System.Drawing.Point(32, 40)
        Me.lbIzquierda.Name = "lbIzquierda"
        Me.lbIzquierda.Size = New System.Drawing.Size(276, 319)
        Me.lbIzquierda.TabIndex = 19
        Me.lbIzquierda.TabStop = False
        Me.lbIzquierda.ValueMember = "Clv_TipSer"
        '
        'FrmTipServTmp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 445)
        Me.Controls.Add(Me.lbDerecha)
        Me.Controls.Add(Me.lbIzquierda)
        Me.Controls.Add(Me.bnAceptar)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.label1CMB)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.button4)
        Me.Controls.Add(Me.button3)
        Me.Controls.Add(Me.button2)
        Me.Controls.Add(Me.button1)
        Me.Name = "FrmTipServTmp"
        Me.Text = "Selecciona Tipo de Servicios"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents bnAceptar As System.Windows.Forms.Button
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents lbDerecha As System.Windows.Forms.ListBox
    Private WithEvents label1CMB As System.Windows.Forms.Label
    Private WithEvents bnSalir As System.Windows.Forms.Button
    Private WithEvents button4 As System.Windows.Forms.Button
    Private WithEvents button3 As System.Windows.Forms.Button
    Private WithEvents button2 As System.Windows.Forms.Button
    Private WithEvents button1 As System.Windows.Forms.Button
    Private WithEvents lbIzquierda As System.Windows.Forms.ListBox
End Class
