﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmUsuarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_UsuarioLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim DomicilioLabel As System.Windows.Forms.Label
        Dim ColoniaLabel As System.Windows.Forms.Label
        Dim FechaIngresoLabel As System.Windows.Forms.Label
        Dim FechaSalidaLabel As System.Windows.Forms.Label
        Dim ActivoLabel As System.Windows.Forms.Label
        Dim PasaporteLabel As System.Windows.Forms.Label
        Dim Clv_TipoUsuarioLabel As System.Windows.Forms.Label
        Dim CATVLabel As System.Windows.Forms.Label
        Dim FacturacionLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmUsuarios))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnPlazaAdic = New System.Windows.Forms.Button()
        Me.CheckBoxRecibeAlerta = New System.Windows.Forms.CheckBox()
        Me.lblRecibeAlerta = New System.Windows.Forms.Label()
        Me.cbIdentificacion = New System.Windows.Forms.ComboBox()
        Me.ClaveLabel1 = New System.Windows.Forms.Label()
        Me.CONUSUARIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ComboBoxPlaza = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ComboBoxCiudad = New System.Windows.Forms.ComboBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.dgvcompania = New System.Windows.Forms.DataGridView()
        Me.IdCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Plaza = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Razon_Social = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GrupoComboBox = New System.Windows.Forms.ComboBox()
        Me.ConGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.ConRelUsuarioEmailBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATIPOUSUARIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.CMBTextBox4 = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxNotadecredito = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CATVCheckBox = New System.Windows.Forms.CheckBox()
        Me.FacturacionCheckBox = New System.Windows.Forms.CheckBox()
        Me.CONUSUARIOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONUSUARIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.DomicilioTextBox = New System.Windows.Forms.TextBox()
        Me.ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.FechaIngresoMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.FechaSalidaMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.ActivoCheckBox = New System.Windows.Forms.CheckBox()
        Me.PasaporteTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipoUsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.dgvciudad = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DAMEFECHADELSERVIDORBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONUSUARIOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONUSUARIOSTableAdapter()
        Me.DAMEFECHADELSERVIDORTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DAMEFECHADELSERVIDORTableAdapter()
        Me.MUESTRATIPOUSUARIOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOUSUARIOSTableAdapter()
        Me.ConRelUsuarioEmailTableAdapter = New sofTV.DataSetEricTableAdapters.ConRelUsuarioEmailTableAdapter()
        Me.ConGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.ConGrupoVentasTableAdapter()
        Me.NueRelUsuarioGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueRelUsuarioGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.NueRelUsuarioGrupoVentasTableAdapter()
        Me.ConRelUsuarioGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConRelUsuarioGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.ConRelUsuarioGrupoVentasTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Clv_UsuarioLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        DomicilioLabel = New System.Windows.Forms.Label()
        ColoniaLabel = New System.Windows.Forms.Label()
        FechaIngresoLabel = New System.Windows.Forms.Label()
        FechaSalidaLabel = New System.Windows.Forms.Label()
        ActivoLabel = New System.Windows.Forms.Label()
        PasaporteLabel = New System.Windows.Forms.Label()
        Clv_TipoUsuarioLabel = New System.Windows.Forms.Label()
        CATVLabel = New System.Windows.Forms.Label()
        FacturacionLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.CONUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvcompania, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelUsuarioEmailBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATIPOUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.CONUSUARIOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONUSUARIOSBindingNavigator.SuspendLayout()
        CType(Me.dgvciudad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueRelUsuarioGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelUsuarioGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_UsuarioLabel
        '
        Clv_UsuarioLabel.AutoSize = True
        Clv_UsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_UsuarioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_UsuarioLabel.Location = New System.Drawing.Point(76, 37)
        Clv_UsuarioLabel.Name = "Clv_UsuarioLabel"
        Clv_UsuarioLabel.Size = New System.Drawing.Size(50, 15)
        Clv_UsuarioLabel.TabIndex = 2
        Clv_UsuarioLabel.Text = "Clave :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(60, 63)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(66, 15)
        NombreLabel.TabIndex = 4
        NombreLabel.Text = "Nombre :"
        '
        'DomicilioLabel
        '
        DomicilioLabel.AutoSize = True
        DomicilioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DomicilioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DomicilioLabel.Location = New System.Drawing.Point(50, 89)
        DomicilioLabel.Name = "DomicilioLabel"
        DomicilioLabel.Size = New System.Drawing.Size(76, 15)
        DomicilioLabel.TabIndex = 6
        DomicilioLabel.Text = "Domicilio :"
        '
        'ColoniaLabel
        '
        ColoniaLabel.AutoSize = True
        ColoniaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ColoniaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ColoniaLabel.Location = New System.Drawing.Point(62, 116)
        ColoniaLabel.Name = "ColoniaLabel"
        ColoniaLabel.Size = New System.Drawing.Size(64, 15)
        ColoniaLabel.TabIndex = 8
        ColoniaLabel.Text = "Colonia :"
        '
        'FechaIngresoLabel
        '
        FechaIngresoLabel.AutoSize = True
        FechaIngresoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaIngresoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaIngresoLabel.Location = New System.Drawing.Point(762, 65)
        FechaIngresoLabel.Name = "FechaIngresoLabel"
        FechaIngresoLabel.Size = New System.Drawing.Size(63, 15)
        FechaIngresoLabel.TabIndex = 10
        FechaIngresoLabel.Text = "Ingreso :"
        '
        'FechaSalidaLabel
        '
        FechaSalidaLabel.AutoSize = True
        FechaSalidaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaSalidaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaSalidaLabel.Location = New System.Drawing.Point(781, 92)
        FechaSalidaLabel.Name = "FechaSalidaLabel"
        FechaSalidaLabel.Size = New System.Drawing.Size(44, 15)
        FechaSalidaLabel.TabIndex = 12
        FechaSalidaLabel.Text = "Baja :"
        '
        'ActivoLabel
        '
        ActivoLabel.AutoSize = True
        ActivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ActivoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ActivoLabel.Location = New System.Drawing.Point(74, 145)
        ActivoLabel.Name = "ActivoLabel"
        ActivoLabel.Size = New System.Drawing.Size(52, 15)
        ActivoLabel.TabIndex = 14
        ActivoLabel.Text = "Activo :"
        '
        'PasaporteLabel
        '
        PasaporteLabel.AutoSize = True
        PasaporteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PasaporteLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PasaporteLabel.Location = New System.Drawing.Point(46, 173)
        PasaporteLabel.Name = "PasaporteLabel"
        PasaporteLabel.Size = New System.Drawing.Size(80, 15)
        PasaporteLabel.TabIndex = 16
        PasaporteLabel.Text = "Pasaporte :"
        '
        'Clv_TipoUsuarioLabel
        '
        Clv_TipoUsuarioLabel.AutoSize = True
        Clv_TipoUsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TipoUsuarioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TipoUsuarioLabel.Location = New System.Drawing.Point(29, 206)
        Clv_TipoUsuarioLabel.Name = "Clv_TipoUsuarioLabel"
        Clv_TipoUsuarioLabel.Size = New System.Drawing.Size(97, 15)
        Clv_TipoUsuarioLabel.TabIndex = 18
        Clv_TipoUsuarioLabel.Text = "Tipo Usuario :"
        '
        'CATVLabel
        '
        CATVLabel.AutoSize = True
        CATVLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CATVLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CATVLabel.Location = New System.Drawing.Point(27, 38)
        CATVLabel.Name = "CATVLabel"
        CATVLabel.Size = New System.Drawing.Size(137, 15)
        CATVLabel.TabIndex = 20
        CATVLabel.Text = "Control de Clientes :"
        '
        'FacturacionLabel
        '
        FacturacionLabel.AutoSize = True
        FacturacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FacturacionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FacturacionLabel.Location = New System.Drawing.Point(78, 68)
        FacturacionLabel.Name = "FacturacionLabel"
        FacturacionLabel.Size = New System.Drawing.Size(86, 15)
        FacturacionLabel.TabIndex = 22
        FacturacionLabel.Text = "Facturación:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(25, 241)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(101, 15)
        Label2.TabIndex = 54
        Label2.Text = "Grupo Ventas :"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(78, 68)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(86, 15)
        Label7.TabIndex = 22
        Label7.Text = "Facturación:"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(27, 38)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(137, 15)
        Label8.TabIndex = 20
        Label8.Text = "Control de Clientes :"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(55, 98)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(109, 15)
        Label13.TabIndex = 24
        Label13.Text = "Nota de crédito:"
        Label13.Visible = False
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(28, 297)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(101, 15)
        Label14.TabIndex = 137
        Label14.Text = "Identificación :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.btnPlazaAdic)
        Me.Panel1.Controls.Add(Me.CheckBoxRecibeAlerta)
        Me.Panel1.Controls.Add(Me.lblRecibeAlerta)
        Me.Panel1.Controls.Add(Label14)
        Me.Panel1.Controls.Add(Me.cbIdentificacion)
        Me.Panel1.Controls.Add(Me.ClaveLabel1)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.CheckBox3)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.ComboBoxPlaza)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.ComboBoxCiudad)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Button8)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Controls.Add(Me.dgvcompania)
        Me.Panel1.Controls.Add(Me.GrupoComboBox)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.ComboBoxCompanias)
        Me.Panel1.Controls.Add(Me.EmailTextBox)
        Me.Panel1.Controls.Add(Me.Button7)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.CMBTextBox4)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.CONUSUARIOSBindingNavigator)
        Me.Panel1.Controls.Add(Clv_UsuarioLabel)
        Me.Panel1.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Controls.Add(DomicilioLabel)
        Me.Panel1.Controls.Add(Me.DomicilioTextBox)
        Me.Panel1.Controls.Add(ColoniaLabel)
        Me.Panel1.Controls.Add(Me.ColoniaTextBox)
        Me.Panel1.Controls.Add(FechaIngresoLabel)
        Me.Panel1.Controls.Add(Me.FechaIngresoMaskedTextBox)
        Me.Panel1.Controls.Add(FechaSalidaLabel)
        Me.Panel1.Controls.Add(Me.FechaSalidaMaskedTextBox)
        Me.Panel1.Controls.Add(ActivoLabel)
        Me.Panel1.Controls.Add(Me.ActivoCheckBox)
        Me.Panel1.Controls.Add(PasaporteLabel)
        Me.Panel1.Controls.Add(Me.PasaporteTextBox)
        Me.Panel1.Controls.Add(Clv_TipoUsuarioLabel)
        Me.Panel1.Controls.Add(Me.Clv_TipoUsuarioTextBox)
        Me.Panel1.Controls.Add(Me.dgvciudad)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1018, 652)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'btnPlazaAdic
        '
        Me.btnPlazaAdic.BackColor = System.Drawing.Color.DarkOrange
        Me.btnPlazaAdic.Enabled = False
        Me.btnPlazaAdic.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPlazaAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPlazaAdic.ForeColor = System.Drawing.Color.Black
        Me.btnPlazaAdic.Location = New System.Drawing.Point(865, 253)
        Me.btnPlazaAdic.Name = "btnPlazaAdic"
        Me.btnPlazaAdic.Size = New System.Drawing.Size(137, 59)
        Me.btnPlazaAdic.TabIndex = 140
        Me.btnPlazaAdic.Text = "Asignación Distribuidores Adicionales"
        Me.btnPlazaAdic.UseVisualStyleBackColor = False
        Me.btnPlazaAdic.Visible = False
        '
        'CheckBoxRecibeAlerta
        '
        Me.CheckBoxRecibeAlerta.AutoSize = True
        Me.CheckBoxRecibeAlerta.Location = New System.Drawing.Point(368, 272)
        Me.CheckBoxRecibeAlerta.Name = "CheckBoxRecibeAlerta"
        Me.CheckBoxRecibeAlerta.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxRecibeAlerta.TabIndex = 139
        Me.CheckBoxRecibeAlerta.UseVisualStyleBackColor = True
        Me.CheckBoxRecibeAlerta.Visible = False
        '
        'lblRecibeAlerta
        '
        Me.lblRecibeAlerta.AutoSize = True
        Me.lblRecibeAlerta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblRecibeAlerta.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblRecibeAlerta.Location = New System.Drawing.Point(181, 271)
        Me.lblRecibeAlerta.Name = "lblRecibeAlerta"
        Me.lblRecibeAlerta.Size = New System.Drawing.Size(181, 15)
        Me.lblRecibeAlerta.TabIndex = 138
        Me.lblRecibeAlerta.Text = "Recibe Alerta Documentos:"
        Me.lblRecibeAlerta.Visible = False
        '
        'cbIdentificacion
        '
        Me.cbIdentificacion.DisplayMember = "Nombre"
        Me.cbIdentificacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbIdentificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbIdentificacion.FormattingEnabled = True
        Me.cbIdentificacion.Location = New System.Drawing.Point(131, 294)
        Me.cbIdentificacion.Name = "cbIdentificacion"
        Me.cbIdentificacion.Size = New System.Drawing.Size(270, 23)
        Me.cbIdentificacion.TabIndex = 136
        Me.cbIdentificacion.ValueMember = "Clv_IdentificacionUsuario"
        '
        'ClaveLabel1
        '
        Me.ClaveLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "Clave", True))
        Me.ClaveLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveLabel1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.ClaveLabel1.Location = New System.Drawing.Point(249, 29)
        Me.ClaveLabel1.Name = "ClaveLabel1"
        Me.ClaveLabel1.Size = New System.Drawing.Size(104, 23)
        Me.ClaveLabel1.TabIndex = 1
        '
        'CONUSUARIOSBindingSource
        '
        Me.CONUSUARIOSBindingSource.DataMember = "CONUSUARIOS"
        Me.CONUSUARIOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label12.Location = New System.Drawing.Point(14, 271)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(115, 15)
        Me.Label12.TabIndex = 135
        Me.Label12.Text = "Recibe Mensaje:"
        '
        'CheckBox3
        '
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(135, 267)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(27, 24)
        Me.CheckBox3.TabIndex = 134
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label11.Location = New System.Drawing.Point(12, 354)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(88, 16)
        Me.Label11.TabIndex = 133
        Me.Label11.Text = "Distribuidor"
        '
        'ComboBoxPlaza
        '
        Me.ComboBoxPlaza.DisplayMember = "Nombre"
        Me.ComboBoxPlaza.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxPlaza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxPlaza.FormattingEnabled = True
        Me.ComboBoxPlaza.Location = New System.Drawing.Point(14, 374)
        Me.ComboBoxPlaza.Name = "ComboBoxPlaza"
        Me.ComboBoxPlaza.Size = New System.Drawing.Size(307, 23)
        Me.ComboBoxPlaza.TabIndex = 132
        Me.ComboBoxPlaza.ValueMember = "Clv_Plaza"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label5.Location = New System.Drawing.Point(49, 544)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(236, 20)
        Me.Label5.TabIndex = 112
        Me.Label5.Text = "Relación Ciudades-Usuarios"
        Me.Label5.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label6.Location = New System.Drawing.Point(50, 585)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(74, 16)
        Me.Label6.TabIndex = 111
        Me.Label6.Text = "Ciudades"
        Me.Label6.Visible = False
        '
        'ComboBoxCiudad
        '
        Me.ComboBoxCiudad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCiudad.FormattingEnabled = True
        Me.ComboBoxCiudad.Location = New System.Drawing.Point(52, 604)
        Me.ComboBoxCiudad.Name = "ComboBoxCiudad"
        Me.ComboBoxCiudad.Size = New System.Drawing.Size(250, 23)
        Me.ComboBoxCiudad.TabIndex = 108
        Me.ComboBoxCiudad.Visible = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(166, 644)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 33)
        Me.Button3.TabIndex = 107
        Me.Button3.Text = "AGREGAR"
        Me.Button3.UseVisualStyleBackColor = False
        Me.Button3.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label10.Location = New System.Drawing.Point(49, 325)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(215, 20)
        Me.Label10.TabIndex = 106
        Me.Label10.Text = "Relación Plaza - Usuarios"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label9.Location = New System.Drawing.Point(12, 414)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(47, 16)
        Me.Label9.TabIndex = 105
        Me.Label9.Text = "Plaza"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label3.Location = New System.Drawing.Point(12, 415)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(105, 16)
        Me.Label3.TabIndex = 105
        Me.Label3.Text = "Distribuidores"
        Me.Label3.Visible = False
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(866, 610)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 33)
        Me.Button8.TabIndex = 10
        Me.Button8.Text = "&SALIR"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(866, 610)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 10
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'dgvcompania
        '
        Me.dgvcompania.AllowUserToAddRows = False
        Me.dgvcompania.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvcompania.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvcompania.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvcompania.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdCompania, Me.Plaza, Me.Razon_Social})
        Me.dgvcompania.Location = New System.Drawing.Point(336, 373)
        Me.dgvcompania.Name = "dgvcompania"
        Me.dgvcompania.ReadOnly = True
        Me.dgvcompania.RowHeadersVisible = False
        Me.dgvcompania.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvcompania.Size = New System.Drawing.Size(522, 270)
        Me.dgvcompania.TabIndex = 103
        '
        'IdCompania
        '
        Me.IdCompania.DataPropertyName = "idcompania"
        Me.IdCompania.HeaderText = "idcompania"
        Me.IdCompania.Name = "IdCompania"
        Me.IdCompania.ReadOnly = True
        Me.IdCompania.Visible = False
        '
        'Plaza
        '
        Me.Plaza.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Plaza.DataPropertyName = "Nombre"
        Me.Plaza.HeaderText = "Distribuidor"
        Me.Plaza.Name = "Plaza"
        Me.Plaza.ReadOnly = True
        '
        'Razon_Social
        '
        Me.Razon_Social.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Razon_Social.DataPropertyName = "razon_social"
        Me.Razon_Social.HeaderText = "Plaza"
        Me.Razon_Social.Name = "Razon_Social"
        Me.Razon_Social.ReadOnly = True
        '
        'GrupoComboBox
        '
        Me.GrupoComboBox.DataSource = Me.ConGrupoVentasBindingSource
        Me.GrupoComboBox.DisplayMember = "Grupo"
        Me.GrupoComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GrupoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrupoComboBox.FormattingEnabled = True
        Me.GrupoComboBox.Location = New System.Drawing.Point(128, 238)
        Me.GrupoComboBox.Name = "GrupoComboBox"
        Me.GrupoComboBox.Size = New System.Drawing.Size(270, 23)
        Me.GrupoComboBox.TabIndex = 53
        Me.GrupoComboBox.ValueMember = "Clv_Grupo"
        '
        'ConGrupoVentasBindingSource
        '
        Me.ConGrupoVentasBindingSource.DataMember = "ConGrupoVentas"
        Me.ConGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(526, 114)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 15)
        Me.Label1.TabIndex = 52
        Me.Label1.Text = "Email :"
        Me.Label1.Visible = False
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(14, 434)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(306, 23)
        Me.ComboBoxCompanias.TabIndex = 93
        '
        'EmailTextBox
        '
        Me.EmailTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRelUsuarioEmailBindingSource, "Email", True))
        Me.EmailTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailTextBox.Location = New System.Drawing.Point(529, 130)
        Me.EmailTextBox.MaxLength = 50
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(329, 21)
        Me.EmailTextBox.TabIndex = 51
        Me.EmailTextBox.Visible = False
        '
        'ConRelUsuarioEmailBindingSource
        '
        Me.ConRelUsuarioEmailBindingSource.DataMember = "ConRelUsuarioEmail"
        Me.ConRelUsuarioEmailBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.EnforceConstraints = False
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(185, 474)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(136, 33)
        Me.Button7.TabIndex = 55
        Me.Button7.Text = "AGREGAR"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(184, 474)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 33)
        Me.Button1.TabIndex = 55
        Me.Button1.Text = "AGREGAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONUSUARIOSBindingSource, "Clv_TipoUsuario", True))
        Me.ComboBox1.DataSource = Me.MUESTRATIPOUSUARIOSBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.ForeColor = System.Drawing.Color.Red
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(128, 200)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(270, 23)
        Me.ComboBox1.TabIndex = 6
        Me.ComboBox1.ValueMember = "clv_tipousuario"
        '
        'MUESTRATIPOUSUARIOSBindingSource
        '
        Me.MUESTRATIPOUSUARIOSBindingSource.DataMember = "MUESTRATIPOUSUARIOS"
        Me.MUESTRATIPOUSUARIOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(866, 395)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 33)
        Me.Button6.TabIndex = 95
        Me.Button6.Text = "QUITAR"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(866, 395)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 33)
        Me.Button2.TabIndex = 95
        Me.Button2.Text = "QUITAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'CMBTextBox4
        '
        Me.CMBTextBox4.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox4.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox4.Location = New System.Drawing.Point(719, 37)
        Me.CMBTextBox4.Name = "CMBTextBox4"
        Me.CMBTextBox4.Size = New System.Drawing.Size(232, 19)
        Me.CMBTextBox4.TabIndex = 48
        Me.CMBTextBox4.TabStop = False
        Me.CMBTextBox4.Text = "Fechas de "
        Me.CMBTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBoxNotadecredito)
        Me.GroupBox2.Controls.Add(Label13)
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Controls.Add(Me.CheckBox2)
        Me.GroupBox2.Controls.Add(Label7)
        Me.GroupBox2.Controls.Add(Label8)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.DarkOrange
        Me.GroupBox2.Location = New System.Drawing.Point(576, 179)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(235, 138)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Acceso a Modulos"
        '
        'CheckBoxNotadecredito
        '
        Me.CheckBoxNotadecredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxNotadecredito.Location = New System.Drawing.Point(170, 94)
        Me.CheckBoxNotadecredito.Name = "CheckBoxNotadecredito"
        Me.CheckBoxNotadecredito.Size = New System.Drawing.Size(59, 24)
        Me.CheckBoxNotadecredito.TabIndex = 23
        Me.CheckBoxNotadecredito.Visible = False
        '
        'CheckBox1
        '
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.CONUSUARIOSBindingSource, "CATV", True))
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(170, 34)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(59, 24)
        Me.CheckBox1.TabIndex = 7
        '
        'CheckBox2
        '
        Me.CheckBox2.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.CONUSUARIOSBindingSource, "Facturacion", True))
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(170, 64)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(59, 24)
        Me.CheckBox2.TabIndex = 8
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CATVCheckBox)
        Me.GroupBox1.Controls.Add(Me.FacturacionCheckBox)
        Me.GroupBox1.Controls.Add(FacturacionLabel)
        Me.GroupBox1.Controls.Add(CATVLabel)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.DarkOrange
        Me.GroupBox1.Location = New System.Drawing.Point(576, 178)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(235, 110)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Acceso a Modulos"
        '
        'CATVCheckBox
        '
        Me.CATVCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONUSUARIOSBindingSource, "CATV", True))
        Me.CATVCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CATVCheckBox.Location = New System.Drawing.Point(170, 34)
        Me.CATVCheckBox.Name = "CATVCheckBox"
        Me.CATVCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.CATVCheckBox.TabIndex = 7
        '
        'FacturacionCheckBox
        '
        Me.FacturacionCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONUSUARIOSBindingSource, "Facturacion", True))
        Me.FacturacionCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FacturacionCheckBox.Location = New System.Drawing.Point(170, 64)
        Me.FacturacionCheckBox.Name = "FacturacionCheckBox"
        Me.FacturacionCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.FacturacionCheckBox.TabIndex = 8
        '
        'CONUSUARIOSBindingNavigator
        '
        Me.CONUSUARIOSBindingNavigator.AddNewItem = Nothing
        Me.CONUSUARIOSBindingNavigator.BindingSource = Me.CONUSUARIOSBindingSource
        Me.CONUSUARIOSBindingNavigator.CountItem = Nothing
        Me.CONUSUARIOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONUSUARIOSBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONUSUARIOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONUSUARIOSBindingNavigatorSaveItem})
        Me.CONUSUARIOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONUSUARIOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONUSUARIOSBindingNavigator.MoveLastItem = Nothing
        Me.CONUSUARIOSBindingNavigator.MoveNextItem = Nothing
        Me.CONUSUARIOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONUSUARIOSBindingNavigator.Name = "CONUSUARIOSBindingNavigator"
        Me.CONUSUARIOSBindingNavigator.PositionItem = Nothing
        Me.CONUSUARIOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONUSUARIOSBindingNavigator.Size = New System.Drawing.Size(1018, 27)
        Me.CONUSUARIOSBindingNavigator.TabIndex = 9
        Me.CONUSUARIOSBindingNavigator.TabStop = True
        Me.CONUSUARIOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(94, 24)
        Me.BindingNavigatorDeleteItem.Text = "ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 24)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONUSUARIOSBindingNavigatorSaveItem
        '
        Me.CONUSUARIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONUSUARIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONUSUARIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONUSUARIOSBindingNavigatorSaveItem.Name = "CONUSUARIOSBindingNavigatorSaveItem"
        Me.CONUSUARIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(95, 24)
        Me.CONUSUARIOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_UsuarioTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_UsuarioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "Clv_Usuario", True))
        Me.Clv_UsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(128, 35)
        Me.Clv_UsuarioTextBox.MaxLength = 5
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(104, 21)
        Me.Clv_UsuarioTextBox.TabIndex = 0
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(128, 61)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(332, 21)
        Me.NombreTextBox.TabIndex = 1
        '
        'DomicilioTextBox
        '
        Me.DomicilioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DomicilioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "Domicilio", True))
        Me.DomicilioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DomicilioTextBox.Location = New System.Drawing.Point(128, 87)
        Me.DomicilioTextBox.Name = "DomicilioTextBox"
        Me.DomicilioTextBox.Size = New System.Drawing.Size(389, 21)
        Me.DomicilioTextBox.TabIndex = 2
        '
        'ColoniaTextBox
        '
        Me.ColoniaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "Colonia", True))
        Me.ColoniaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ColoniaTextBox.Location = New System.Drawing.Point(128, 114)
        Me.ColoniaTextBox.Name = "ColoniaTextBox"
        Me.ColoniaTextBox.Size = New System.Drawing.Size(332, 21)
        Me.ColoniaTextBox.TabIndex = 3
        '
        'FechaIngresoMaskedTextBox
        '
        Me.FechaIngresoMaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FechaIngresoMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "FechaIngreso", True))
        Me.FechaIngresoMaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaIngresoMaskedTextBox.Location = New System.Drawing.Point(831, 63)
        Me.FechaIngresoMaskedTextBox.Mask = "00/00/0000"
        Me.FechaIngresoMaskedTextBox.Name = "FechaIngresoMaskedTextBox"
        Me.FechaIngresoMaskedTextBox.Size = New System.Drawing.Size(86, 21)
        Me.FechaIngresoMaskedTextBox.TabIndex = 11
        Me.FechaIngresoMaskedTextBox.TabStop = False
        Me.FechaIngresoMaskedTextBox.ValidatingType = GetType(Date)
        '
        'FechaSalidaMaskedTextBox
        '
        Me.FechaSalidaMaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FechaSalidaMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "FechaSalida", True))
        Me.FechaSalidaMaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaSalidaMaskedTextBox.Location = New System.Drawing.Point(831, 90)
        Me.FechaSalidaMaskedTextBox.Mask = "00/00/0000"
        Me.FechaSalidaMaskedTextBox.Name = "FechaSalidaMaskedTextBox"
        Me.FechaSalidaMaskedTextBox.Size = New System.Drawing.Size(86, 21)
        Me.FechaSalidaMaskedTextBox.TabIndex = 13
        Me.FechaSalidaMaskedTextBox.TabStop = False
        Me.FechaSalidaMaskedTextBox.ValidatingType = GetType(Date)
        '
        'ActivoCheckBox
        '
        Me.ActivoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONUSUARIOSBindingSource, "Activo", True))
        Me.ActivoCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivoCheckBox.Location = New System.Drawing.Point(128, 141)
        Me.ActivoCheckBox.Name = "ActivoCheckBox"
        Me.ActivoCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.ActivoCheckBox.TabIndex = 4
        '
        'PasaporteTextBox
        '
        Me.PasaporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PasaporteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "Pasaporte", True))
        Me.PasaporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasaporteTextBox.Location = New System.Drawing.Point(128, 171)
        Me.PasaporteTextBox.Name = "PasaporteTextBox"
        Me.PasaporteTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasaporteTextBox.Size = New System.Drawing.Size(104, 21)
        Me.PasaporteTextBox.TabIndex = 5
        '
        'Clv_TipoUsuarioTextBox
        '
        Me.Clv_TipoUsuarioTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoUsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipoUsuarioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "Clv_TipoUsuario", True))
        Me.Clv_TipoUsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TipoUsuarioTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoUsuarioTextBox.Location = New System.Drawing.Point(432, 204)
        Me.Clv_TipoUsuarioTextBox.Name = "Clv_TipoUsuarioTextBox"
        Me.Clv_TipoUsuarioTextBox.Size = New System.Drawing.Size(17, 14)
        Me.Clv_TipoUsuarioTextBox.TabIndex = 19
        Me.Clv_TipoUsuarioTextBox.TabStop = False
        '
        'dgvciudad
        '
        Me.dgvciudad.AllowUserToAddRows = False
        Me.dgvciudad.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvciudad.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvciudad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvciudad.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.dgvciudad.Location = New System.Drawing.Point(397, 550)
        Me.dgvciudad.Name = "dgvciudad"
        Me.dgvciudad.ReadOnly = True
        Me.dgvciudad.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvciudad.Size = New System.Drawing.Size(340, 153)
        Me.dgvciudad.TabIndex = 110
        Me.dgvciudad.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "clv_ciudad"
        Me.DataGridViewTextBoxColumn1.HeaderText = "clv_ciudad"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "nombre"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Ciudad"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label4.Location = New System.Drawing.Point(373, 356)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(272, 20)
        Me.Label4.TabIndex = 106
        Me.Label4.Text = "Relación Distribuidores-Usuarios"
        '
        'DAMEFECHADELSERVIDORBindingSource
        '
        Me.DAMEFECHADELSERVIDORBindingSource.DataMember = "DAMEFECHADELSERVIDOR"
        Me.DAMEFECHADELSERVIDORBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONUSUARIOSTableAdapter
        '
        Me.CONUSUARIOSTableAdapter.ClearBeforeFill = True
        '
        'DAMEFECHADELSERVIDORTableAdapter
        '
        Me.DAMEFECHADELSERVIDORTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOUSUARIOSTableAdapter
        '
        Me.MUESTRATIPOUSUARIOSTableAdapter.ClearBeforeFill = True
        '
        'ConRelUsuarioEmailTableAdapter
        '
        Me.ConRelUsuarioEmailTableAdapter.ClearBeforeFill = True
        '
        'ConGrupoVentasTableAdapter
        '
        Me.ConGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'NueRelUsuarioGrupoVentasBindingSource
        '
        Me.NueRelUsuarioGrupoVentasBindingSource.DataMember = "NueRelUsuarioGrupoVentas"
        Me.NueRelUsuarioGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'NueRelUsuarioGrupoVentasTableAdapter
        '
        Me.NueRelUsuarioGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'ConRelUsuarioGrupoVentasBindingSource
        '
        Me.ConRelUsuarioGrupoVentasBindingSource.DataMember = "ConRelUsuarioGrupoVentas"
        Me.ConRelUsuarioGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'ConRelUsuarioGrupoVentasTableAdapter
        '
        Me.ConRelUsuarioGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1042, 672)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmUsuarios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Usuarios"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvcompania, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelUsuarioEmailBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATIPOUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.CONUSUARIOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONUSUARIOSBindingNavigator.ResumeLayout(False)
        Me.CONUSUARIOSBindingNavigator.PerformLayout()
        CType(Me.dgvciudad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueRelUsuarioGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelUsuarioGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONUSUARIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONUSUARIOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONUSUARIOSTableAdapter
    Friend WithEvents ClaveLabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DomicilioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaIngresoMaskedTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents FechaSalidaMaskedTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents ActivoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PasaporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipoUsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CATVCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents FacturacionCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CONUSUARIOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONUSUARIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CMBTextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents DAMEFECHADELSERVIDORBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEFECHADELSERVIDORTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DAMEFECHADELSERVIDORTableAdapter
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRATIPOUSUARIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATIPOUSUARIOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOUSUARIOSTableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConRelUsuarioEmailBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelUsuarioEmailTableAdapter As sofTV.DataSetEricTableAdapters.ConRelUsuarioEmailTableAdapter
    Friend WithEvents EmailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.ConGrupoVentasTableAdapter
    Friend WithEvents GrupoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents NueRelUsuarioGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueRelUsuarioGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.NueRelUsuarioGrupoVentasTableAdapter
    Friend WithEvents ConRelUsuarioGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelUsuarioGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.ConRelUsuarioGrupoVentasTableAdapter
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents dgvcompania As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dgvciudad As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComboBoxCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxPlaza As System.Windows.Forms.ComboBox
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CheckBoxNotadecredito As System.Windows.Forms.CheckBox
    Friend WithEvents IdCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Plaza As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Razon_Social As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cbIdentificacion As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBoxRecibeAlerta As System.Windows.Forms.CheckBox
    Friend WithEvents lblRecibeAlerta As System.Windows.Forms.Label
    Friend WithEvents btnPlazaAdic As System.Windows.Forms.Button
End Class
