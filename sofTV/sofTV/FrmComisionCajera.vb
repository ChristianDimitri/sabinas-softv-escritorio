﻿Public Class FrmComisionCajera

    Private Sub FrmComisionCajera_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        colorea(Me, Me.Name)

        Usp_MuestraGruposComisionCajera()
        llenaConceptosComisionCajera()

        If opRanCoCaj = "N" Then
            idRanCoCaj = 0
        End If
        If (opRanCoCaj = "M" Or opRanCoCaj = "C") And idRanCoCaj > 0 Then

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idComision", SqlDbType.BigInt, idRanCoCaj)
            BaseII.CreateMyParameter("@idGrupo", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@idConcepto", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@limiteInf", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@limiteSup", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@pago", ParameterDirection.Output, SqlDbType.Money)
            BaseII.ProcedimientoOutPut("Usp_ConRangosComisionCajera")
            ComboBox2.SelectedValue = CInt(BaseII.dicoPar("@idGrupo"))
            ComboBox1.SelectedValue = CInt(BaseII.dicoPar("@idConcepto"))
            TextBox1.Text = BaseII.dicoPar("@limiteInf")
            TextBox2.Text = BaseII.dicoPar("@limiteSup")
            TextBox3.Text = BaseII.dicoPar("@pago")

            If opRanCoCaj = "C" Then
                TextBox1.Enabled = False
                TextBox2.Enabled = False
                TextBox3.Enabled = False
                ComboBox1.Enabled = False
                ComboBox2.Enabled = False
                'Button1.Enabled = False
                Button5.Enabled = False
            End If

        End If

    End Sub

    Private Sub llenaConceptosComisionCajera()
        Try
            BaseII.limpiaParametros()
            ComboBox1.DataSource = BaseII.ConsultaDT("Usp_MuestraConceptosComisionCajera")
            ComboBox1.DisplayMember = "Concepto"
            ComboBox1.ValueMember = "idConcepto"

            If ComboBox1.Items.Count > 0 Then
                ComboBox1.SelectedIndex = 0
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Usp_MuestraGruposComisionCajera()
        Try
            BaseII.limpiaParametros()
            ComboBox2.DataSource = BaseII.ConsultaDT("Usp_MuestraGruposComisionCajera")
            ComboBox2.DisplayMember = "Grupo"
            ComboBox2.ValueMember = "idGrupo"

            If ComboBox2.Items.Count > 0 Then
                ComboBox2.SelectedIndex = 0
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox1, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TextBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox2, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox3.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox3, Asc(e.KeyChar), "L"))
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        If TextBox1.Text.Length = 0 Then
            MsgBox("Capture el limite inferior")
            Exit Sub
        End If

        If TextBox2.Text.Length = 0 Then
            MsgBox("Capture el limite Superior")
            Exit Sub
        End If

        If TextBox3.Text.Length = 0 Then
            MsgBox("Capture el Pago")
            Exit Sub
        End If

        If CInt(TextBox1.Text) >= CInt(TextBox2.Text) Then
            MsgBox("El limite inferior no puede ser mayor o igual al limite superior")
            Exit Sub
        End If

        Dim Msg As String = Nothing

        If opRanCoCaj = "N" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idGrupo", SqlDbType.Int, ComboBox2.SelectedValue)
            BaseII.CreateMyParameter("@idConcepto", SqlDbType.Int, ComboBox1.SelectedValue)
            BaseII.CreateMyParameter("@limiteInf", SqlDbType.Int, TextBox1.Text)
            BaseII.CreateMyParameter("@limiteSup", SqlDbType.Int, TextBox2.Text)
            BaseII.CreateMyParameter("@pago", SqlDbType.Money, TextBox3.Text)
            BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 100)
            BaseII.ProcedimientoOutPut("Usp_NueRangosComisionCajera")
            Msg = BaseII.dicoPar("@Msg")
        ElseIf opRanCoCaj = "M" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.BigInt, idRanCoCaj)
            BaseII.CreateMyParameter("@idGrupo", SqlDbType.Int, ComboBox2.SelectedValue)
            BaseII.CreateMyParameter("@idConcepto", SqlDbType.Int, ComboBox1.SelectedValue)
            BaseII.CreateMyParameter("@limiteInf", SqlDbType.Int, TextBox1.Text)
            BaseII.CreateMyParameter("@limiteSup", SqlDbType.Int, TextBox2.Text)
            BaseII.CreateMyParameter("@pago", SqlDbType.Money, TextBox3.Text)
            BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 100)
            BaseII.ProcedimientoOutPut("Usp_ModRangosComisionCajera")
            Msg = BaseII.dicoPar("@Msg")
        End If
        

        If Msg.Length > 0 Then
            MsgBox(Msg, MsgBoxStyle.Information)
            Exit Sub
        Else
            MsgBox("Guardado con exito", MsgBoxStyle.Information)
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class