
Imports System.Data.SqlClient
Public Class FrmSelVendedor

    
    Public vieneDeVendedores As Boolean
    Private Sub FrmSelVendedor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        CON.Open()
        If varfrmselcompania <> "graficasdosopciones" Then
            Me.Dame_clv_session_ReportesTableAdapter.Connection = CON
            Me.Dame_clv_session_ReportesTableAdapter.Fill(Me.DataSetEric.Dame_clv_session_Reportes, eClv_Session)
        End If
        If vieneDeVendedores = True Then
            Me.ConVentasVendedoresProTableAdapter.Connection = CON
            Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric.ConVentasVendedoresPro, eClv_Session, 3, GloClvUsuario)
            vieneDeVendedores = False
        Else
            Me.ConVentasVendedoresProTableAdapter.Connection = CON
            Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric.ConVentasVendedoresPro, eClv_Session, 0, identificador)
        End If

        
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.ConVentasVendedoresProListBox.Items.Count > 0 Then
            CON.Open()
            Me.InsertarVendedorTmpTableAdapter.Connection = CON
            Me.InsertarVendedorTmpTableAdapter.Fill(Me.DataSetEric.InsertarVendedorTmp, CLng(Me.ConVentasVendedoresProListBox.SelectedValue), eClv_Session, 0)
            Me.ConVentasVendedoresProTableAdapter.Connection = CON
            Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric.ConVentasVendedoresPro, eClv_Session, 1, identificador)
            Me.ConVentasVendedoresTmpTableAdapter.Connection = CON
            Me.ConVentasVendedoresTmpTableAdapter.Fill(Me.DataSetEric.ConVentasVendedoresTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.ConVentasVendedoresProListBox.Items.Count > 0 Then
            CON.Open()
            Me.InsertarVendedorTmpTableAdapter.Connection = CON
            Me.InsertarVendedorTmpTableAdapter.Fill(Me.DataSetEric.InsertarVendedorTmp, CLng(Me.ConVentasVendedoresProListBox.SelectedValue), eClv_Session, 1)
            Me.ConVentasVendedoresProTableAdapter.Connection = CON
            Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric.ConVentasVendedoresPro, eClv_Session, 1, identificador)
            Me.ConVentasVendedoresTmpTableAdapter.Connection = CON
            Me.ConVentasVendedoresTmpTableAdapter.Fill(Me.DataSetEric.ConVentasVendedoresTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.ConVentasVendedoresTmpListBox.Items.Count > 0 Then
            CON.Open()
            Me.BorrarVendedorTmpTableAdapter.Connection = CON
            Me.BorrarVendedorTmpTableAdapter.Fill(Me.DataSetEric.BorrarVendedorTmp, CLng(Me.ConVentasVendedoresTmpListBox.SelectedValue), eClv_Session, 0)
            Me.ConVentasVendedoresProTableAdapter.Connection = CON
            Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric.ConVentasVendedoresPro, eClv_Session, 1, identificador)
            Me.ConVentasVendedoresTmpTableAdapter.Connection = CON
            Me.ConVentasVendedoresTmpTableAdapter.Fill(Me.DataSetEric.ConVentasVendedoresTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.ConVentasVendedoresTmpListBox.Items.Count > 0 Then
            CON.Open()
            Me.BorrarVendedorTmpTableAdapter.Connection = CON
            Me.BorrarVendedorTmpTableAdapter.Fill(Me.DataSetEric.BorrarVendedorTmp, CLng(Me.ConVentasVendedoresTmpListBox.SelectedValue), eClv_Session, 1)
            Me.ConVentasVendedoresProTableAdapter.Connection = CON
            Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric.ConVentasVendedoresPro, eClv_Session, 1, identificador)
            Me.ConVentasVendedoresTmpTableAdapter.Connection = CON
            Me.ConVentasVendedoresTmpTableAdapter.Fill(Me.DataSetEric.ConVentasVendedoresTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Aceptar.Click
        If Me.ConVentasVendedoresTmpListBox.Items.Count > 0 Then
            'If Me.ConVentasVendedoresTmpListBox.Items.Count = 1 And eBndVen = True Then
            '    eBndVen = False
            '    eClv_Vendedor = Me.ConVentasVendedoresTmpListBox.SelectedValue
            '    eNombre = Me.ConVentasVendedoresTmpListBox.Text
            '    eOpVentas = 15
            'End If
            If varfrmselcompania = "graficasventas" Or varfrmselcompania = "graficasdosopciones" Then
                FrmSelServicioE.Show()
                Me.Close()
                Exit Sub
            End If
            If (eOpVentas = 2309) Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
            ElseIf (eOpVentas = 2310) Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Else
                If eOpVentas = 2 Or eOpVentas = 3 Or eOpVentas = 20 Or eOpVentas = 22 Or eOpVentas = 41 Or eOpVentas = 44 Or eOpVentas = 47 Or eOpVentas = 48 Then
                    FrmSelServicioE.Show()
                Else
                    FrmImprimirComision.Show()
                End If
            End If
            Me.Close()
        End If
    End Sub

    
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class