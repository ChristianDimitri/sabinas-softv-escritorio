﻿Public Class BrwPaquetesTelefonica

    Private Sub BrwLocalidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        NombreTextBox.BackColor = Button4.BackColor
        NombreTextBox.ForeColor = Button4.ForeColor
        llenaComboProveedor()
        busca(0, "")
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub llenaComboProveedor()
        Try
            BaseII.limpiaParametros()
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("sp_dameProveedoresInternet")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub busca(ByVal id As Integer, ByVal Descripcion As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idBeamPaquete", SqlDbType.Int, id)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, Descripcion)
        BaseII.CreateMyParameter("@idProveedor", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
        DataGridView1.DataSource = BaseII.ConsultaDT("sp_buscaBeamPaquete")
        TextBox2.Text = ""
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            If DataGridView1.Rows.Count = 0 Then
                Exit Sub
            End If
            Clv_LocalidadLabel.Text = DataGridView1.SelectedCells(0).Value.ToString
            NombreTextBox.Text = DataGridView1.SelectedCells(1).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If TextBox2.Text = "" Then
            Exit Sub
        End If
        busca(0, TextBox2.Text)
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TextBox2.Text = "" Then
                Exit Sub
            End If
            busca(TextBox2.Text, "")
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FrmPaquetesTelefonica.opcionP = "N"
        FrmPaquetesTelefonica.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        FrmPaquetesTelefonica.opcionP = "C"
        FrmPaquetesTelefonica.locIdBeam = Clv_LocalidadLabel.Text
        FrmPaquetesTelefonica.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        FrmPaquetesTelefonica.opcionP = "M"
        FrmPaquetesTelefonica.locIdBeam = Clv_LocalidadLabel.Text
        FrmPaquetesTelefonica.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Public entraActivate As Integer = 0
    Private Sub BrwLocalidad_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If entraActivate = 1 Then
            busca(0, "")
            entraActivate = 0
        End If
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs)
        'If TextBox1.Text = "" Then
        '    Exit Sub
        'End If
        'busca("", TextBox1.Text)
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            'If TextBox1.Text = "" Then
            '    Exit Sub
            'End If
            'busca("", TextBox1.Text)
        End If
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        busca(0, "")
    End Sub
End Class