<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCnrPPE
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label12 As System.Windows.Forms.Label
        Dim ConsecutivoLabel As System.Windows.Forms.Label
        Dim Numero_de_contratoLabel As System.Windows.Forms.Label
        Dim Mac_addresLabel As System.Windows.Forms.Label
        Dim PaqueteLabel As System.Windows.Forms.Label
        Dim ComandoLabel As System.Windows.Forms.Label
        Dim ResultadoLabel As System.Windows.Forms.Label
        Dim Descripcion_transaccionLabel As System.Windows.Forms.Label
        Dim Clv_OrdenLabel As System.Windows.Forms.Label
        Dim Fec_EjeLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnModifica = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblFechaH = New System.Windows.Forms.Label()
        Me.lblConsecutivo = New System.Windows.Forms.Label()
        Me.lblContrato = New System.Windows.Forms.Label()
        Me.lblMac = New System.Windows.Forms.Label()
        Me.lblPaquete = New System.Windows.Forms.Label()
        Me.lblComando = New System.Windows.Forms.Label()
        Me.lblResultado = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblClv = New System.Windows.Forms.Label()
        Me.lblFechaE = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.dtpFechaH = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaE = New System.Windows.Forms.DateTimePicker()
        Me.btnFechaH = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cbResultado = New System.Windows.Forms.ComboBox()
        Me.btnFechaE = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnClv = New System.Windows.Forms.Button()
        Me.txtOrden = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnResultado = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnMac = New System.Windows.Forms.Button()
        Me.txtMac = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnContrato = New System.Windows.Forms.Button()
        Me.btnConsecutivo = New System.Windows.Forms.Button()
        Me.txtContrato = New System.Windows.Forms.TextBox()
        Me.txtConsecutivo = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.gvPPE = New System.Windows.Forms.DataGridView()
        Label12 = New System.Windows.Forms.Label()
        ConsecutivoLabel = New System.Windows.Forms.Label()
        Numero_de_contratoLabel = New System.Windows.Forms.Label()
        Mac_addresLabel = New System.Windows.Forms.Label()
        PaqueteLabel = New System.Windows.Forms.Label()
        ComandoLabel = New System.Windows.Forms.Label()
        ResultadoLabel = New System.Windows.Forms.Label()
        Descripcion_transaccionLabel = New System.Windows.Forms.Label()
        Clv_OrdenLabel = New System.Windows.Forms.Label()
        Fec_EjeLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.gvPPE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.Location = New System.Drawing.Point(23, 535)
        Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(126, 18)
        Label12.TabIndex = 29
        Label12.Text = "Fecha Habilitar:"
        '
        'ConsecutivoLabel
        '
        ConsecutivoLabel.AutoSize = True
        ConsecutivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConsecutivoLabel.Location = New System.Drawing.Point(11, 48)
        ConsecutivoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ConsecutivoLabel.Name = "ConsecutivoLabel"
        ConsecutivoLabel.Size = New System.Drawing.Size(112, 18)
        ConsecutivoLabel.TabIndex = 5
        ConsecutivoLabel.Text = "Consecutivo :"
        '
        'Numero_de_contratoLabel
        '
        Numero_de_contratoLabel.AutoSize = True
        Numero_de_contratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_de_contratoLabel.Location = New System.Drawing.Point(39, 84)
        Numero_de_contratoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Numero_de_contratoLabel.Name = "Numero_de_contratoLabel"
        Numero_de_contratoLabel.Size = New System.Drawing.Size(84, 18)
        Numero_de_contratoLabel.TabIndex = 7
        Numero_de_contratoLabel.Text = "Contrato :"
        '
        'Mac_addresLabel
        '
        Mac_addresLabel.AutoSize = True
        Mac_addresLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Mac_addresLabel.Location = New System.Drawing.Point(12, 118)
        Mac_addresLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Mac_addresLabel.Name = "Mac_addresLabel"
        Mac_addresLabel.Size = New System.Drawing.Size(116, 18)
        Mac_addresLabel.TabIndex = 11
        Mac_addresLabel.Text = "Mac Address :"
        '
        'PaqueteLabel
        '
        PaqueteLabel.AutoSize = True
        PaqueteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PaqueteLabel.Location = New System.Drawing.Point(12, 178)
        PaqueteLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        PaqueteLabel.Name = "PaqueteLabel"
        PaqueteLabel.Size = New System.Drawing.Size(79, 18)
        PaqueteLabel.TabIndex = 13
        PaqueteLabel.Text = "Paquete: "
        '
        'ComandoLabel
        '
        ComandoLabel.AutoSize = True
        ComandoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ComandoLabel.Location = New System.Drawing.Point(20, 241)
        ComandoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ComandoLabel.Name = "ComandoLabel"
        ComandoLabel.Size = New System.Drawing.Size(91, 18)
        ComandoLabel.TabIndex = 15
        ComandoLabel.Text = "Comando :"
        '
        'ResultadoLabel
        '
        ResultadoLabel.AutoSize = True
        ResultadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ResultadoLabel.Location = New System.Drawing.Point(15, 277)
        ResultadoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ResultadoLabel.Name = "ResultadoLabel"
        ResultadoLabel.Size = New System.Drawing.Size(94, 18)
        ResultadoLabel.TabIndex = 17
        ResultadoLabel.Text = "Resultado :"
        '
        'Descripcion_transaccionLabel
        '
        Descripcion_transaccionLabel.AutoSize = True
        Descripcion_transaccionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Descripcion_transaccionLabel.Location = New System.Drawing.Point(19, 313)
        Descripcion_transaccionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Descripcion_transaccionLabel.Name = "Descripcion_transaccionLabel"
        Descripcion_transaccionLabel.Size = New System.Drawing.Size(108, 18)
        Descripcion_transaccionLabel.TabIndex = 19
        Descripcion_transaccionLabel.Text = "Descripción :"
        '
        'Clv_OrdenLabel
        '
        Clv_OrdenLabel.AutoSize = True
        Clv_OrdenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_OrdenLabel.Location = New System.Drawing.Point(7, 469)
        Clv_OrdenLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_OrdenLabel.Name = "Clv_OrdenLabel"
        Clv_OrdenLabel.Size = New System.Drawing.Size(111, 18)
        Clv_OrdenLabel.TabIndex = 21
        Clv_OrdenLabel.Text = "Clave Orden :"
        '
        'Fec_EjeLabel
        '
        Fec_EjeLabel.AutoSize = True
        Fec_EjeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fec_EjeLabel.Location = New System.Drawing.Point(7, 505)
        Fec_EjeLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Fec_EjeLabel.Name = "Fec_EjeLabel"
        Fec_EjeLabel.Size = New System.Drawing.Size(143, 18)
        Fec_EjeLabel.TabIndex = 25
        Fec_EjeLabel.Text = "Fecha Ejecución :"
        '
        'btnModifica
        '
        Me.btnModifica.BackColor = System.Drawing.Color.DarkOrange
        Me.btnModifica.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModifica.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModifica.ForeColor = System.Drawing.Color.Black
        Me.btnModifica.Location = New System.Drawing.Point(1105, 762)
        Me.btnModifica.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnModifica.Name = "btnModifica"
        Me.btnModifica.Size = New System.Drawing.Size(181, 44)
        Me.btnModifica.TabIndex = 25
        Me.btnModifica.Text = "&MODIFICA"
        Me.btnModifica.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Chocolate
        Me.Panel1.Controls.Add(Label12)
        Me.Panel1.Controls.Add(Me.lblFechaH)
        Me.Panel1.Controls.Add(ConsecutivoLabel)
        Me.Panel1.Controls.Add(Me.lblConsecutivo)
        Me.Panel1.Controls.Add(Numero_de_contratoLabel)
        Me.Panel1.Controls.Add(Me.lblContrato)
        Me.Panel1.Controls.Add(Mac_addresLabel)
        Me.Panel1.Controls.Add(Me.lblMac)
        Me.Panel1.Controls.Add(PaqueteLabel)
        Me.Panel1.Controls.Add(Me.lblPaquete)
        Me.Panel1.Controls.Add(ComandoLabel)
        Me.Panel1.Controls.Add(Me.lblComando)
        Me.Panel1.Controls.Add(ResultadoLabel)
        Me.Panel1.Controls.Add(Me.lblResultado)
        Me.Panel1.Controls.Add(Descripcion_transaccionLabel)
        Me.Panel1.Controls.Add(Me.lblDescripcion)
        Me.Panel1.Controls.Add(Clv_OrdenLabel)
        Me.Panel1.Controls.Add(Me.lblClv)
        Me.Panel1.Controls.Add(Fec_EjeLabel)
        Me.Panel1.Controls.Add(Me.lblFechaE)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(1017, 2)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(336, 575)
        Me.Panel1.TabIndex = 27
        '
        'lblFechaH
        '
        Me.lblFechaH.BackColor = System.Drawing.Color.SandyBrown
        Me.lblFechaH.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaH.Location = New System.Drawing.Point(175, 535)
        Me.lblFechaH.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFechaH.Name = "lblFechaH"
        Me.lblFechaH.Size = New System.Drawing.Size(133, 28)
        Me.lblFechaH.TabIndex = 30
        '
        'lblConsecutivo
        '
        Me.lblConsecutivo.BackColor = System.Drawing.Color.SandyBrown
        Me.lblConsecutivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConsecutivo.Location = New System.Drawing.Point(136, 43)
        Me.lblConsecutivo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblConsecutivo.Name = "lblConsecutivo"
        Me.lblConsecutivo.Size = New System.Drawing.Size(133, 28)
        Me.lblConsecutivo.TabIndex = 6
        '
        'lblContrato
        '
        Me.lblContrato.BackColor = System.Drawing.Color.SandyBrown
        Me.lblContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContrato.Location = New System.Drawing.Point(136, 79)
        Me.lblContrato.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblContrato.Name = "lblContrato"
        Me.lblContrato.Size = New System.Drawing.Size(133, 28)
        Me.lblContrato.TabIndex = 8
        '
        'lblMac
        '
        Me.lblMac.BackColor = System.Drawing.Color.SandyBrown
        Me.lblMac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMac.Location = New System.Drawing.Point(12, 144)
        Me.lblMac.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMac.Name = "lblMac"
        Me.lblMac.Size = New System.Drawing.Size(297, 28)
        Me.lblMac.TabIndex = 12
        '
        'lblPaquete
        '
        Me.lblPaquete.BackColor = System.Drawing.Color.SandyBrown
        Me.lblPaquete.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaquete.Location = New System.Drawing.Point(15, 202)
        Me.lblPaquete.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPaquete.Name = "lblPaquete"
        Me.lblPaquete.Size = New System.Drawing.Size(295, 28)
        Me.lblPaquete.TabIndex = 14
        '
        'lblComando
        '
        Me.lblComando.BackColor = System.Drawing.Color.SandyBrown
        Me.lblComando.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComando.Location = New System.Drawing.Point(128, 238)
        Me.lblComando.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblComando.Name = "lblComando"
        Me.lblComando.Size = New System.Drawing.Size(181, 28)
        Me.lblComando.TabIndex = 16
        '
        'lblResultado
        '
        Me.lblResultado.BackColor = System.Drawing.Color.SandyBrown
        Me.lblResultado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultado.Location = New System.Drawing.Point(128, 273)
        Me.lblResultado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblResultado.Name = "lblResultado"
        Me.lblResultado.Size = New System.Drawing.Size(39, 28)
        Me.lblResultado.TabIndex = 18
        '
        'lblDescripcion
        '
        Me.lblDescripcion.BackColor = System.Drawing.Color.SandyBrown
        Me.lblDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescripcion.Location = New System.Drawing.Point(24, 338)
        Me.lblDescripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(285, 117)
        Me.lblDescripcion.TabIndex = 20
        '
        'lblClv
        '
        Me.lblClv.BackColor = System.Drawing.Color.SandyBrown
        Me.lblClv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClv.Location = New System.Drawing.Point(136, 464)
        Me.lblClv.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblClv.Name = "lblClv"
        Me.lblClv.Size = New System.Drawing.Size(133, 28)
        Me.lblClv.TabIndex = 22
        '
        'lblFechaE
        '
        Me.lblFechaE.BackColor = System.Drawing.Color.SandyBrown
        Me.lblFechaE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaE.Location = New System.Drawing.Point(175, 500)
        Me.lblFechaE.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFechaE.Name = "lblFechaE"
        Me.lblFechaE.Size = New System.Drawing.Size(133, 28)
        Me.lblFechaE.TabIndex = 26
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(4, 0)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(187, 25)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos del Servicio"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(1105, 827)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(181, 44)
        Me.btnSalir.TabIndex = 26
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(1, 2)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.dtpFechaH)
        Me.SplitContainer1.Panel1.Controls.Add(Me.dtpFechaE)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnFechaH)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label11)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cbResultado)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnFechaE)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label10)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnClv)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtOrden)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnResultado)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnMac)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtMac)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnContrato)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnConsecutivo)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtContrato)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtConsecutivo)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.gvPPE)
        Me.SplitContainer1.Size = New System.Drawing.Size(1009, 854)
        Me.SplitContainer1.SplitterDistance = 334
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 28
        Me.SplitContainer1.TabStop = False
        '
        'dtpFechaH
        '
        Me.dtpFechaH.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaH.Location = New System.Drawing.Point(29, 751)
        Me.dtpFechaH.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dtpFechaH.Name = "dtpFechaH"
        Me.dtpFechaH.Size = New System.Drawing.Size(184, 22)
        Me.dtpFechaH.TabIndex = 37
        '
        'dtpFechaE
        '
        Me.dtpFechaE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaE.Location = New System.Drawing.Point(29, 641)
        Me.dtpFechaE.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dtpFechaE.Name = "dtpFechaE"
        Me.dtpFechaE.Size = New System.Drawing.Size(184, 22)
        Me.dtpFechaE.TabIndex = 36
        '
        'btnFechaH
        '
        Me.btnFechaH.BackColor = System.Drawing.Color.DarkOrange
        Me.btnFechaH.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFechaH.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFechaH.ForeColor = System.Drawing.Color.Black
        Me.btnFechaH.Location = New System.Drawing.Point(29, 783)
        Me.btnFechaH.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnFechaH.Name = "btnFechaH"
        Me.btnFechaH.Size = New System.Drawing.Size(117, 28)
        Me.btnFechaH.TabIndex = 21
        Me.btnFechaH.Text = "&Buscar"
        Me.btnFechaH.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(25, 727)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(131, 18)
        Me.Label11.TabIndex = 35
        Me.Label11.Text = "Fecha Habilitar :"
        '
        'cbResultado
        '
        Me.cbResultado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbResultado.FormattingEnabled = True
        Me.cbResultado.Location = New System.Drawing.Point(29, 418)
        Me.cbResultado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbResultado.Name = "cbResultado"
        Me.cbResultado.Size = New System.Drawing.Size(277, 28)
        Me.cbResultado.TabIndex = 10
        '
        'btnFechaE
        '
        Me.btnFechaE.BackColor = System.Drawing.Color.DarkOrange
        Me.btnFechaE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFechaE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFechaE.ForeColor = System.Drawing.Color.Black
        Me.btnFechaE.Location = New System.Drawing.Point(29, 673)
        Me.btnFechaE.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnFechaE.Name = "btnFechaE"
        Me.btnFechaE.Size = New System.Drawing.Size(117, 28)
        Me.btnFechaE.TabIndex = 19
        Me.btnFechaE.Text = "&Buscar"
        Me.btnFechaE.UseVisualStyleBackColor = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(25, 618)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(143, 18)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Fecha Ejecución :"
        '
        'btnClv
        '
        Me.btnClv.BackColor = System.Drawing.Color.DarkOrange
        Me.btnClv.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClv.ForeColor = System.Drawing.Color.Black
        Me.btnClv.Location = New System.Drawing.Point(29, 560)
        Me.btnClv.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnClv.Name = "btnClv"
        Me.btnClv.Size = New System.Drawing.Size(117, 28)
        Me.btnClv.TabIndex = 13
        Me.btnClv.Text = "&Buscar"
        Me.btnClv.UseVisualStyleBackColor = False
        '
        'txtOrden
        '
        Me.txtOrden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrden.Location = New System.Drawing.Point(29, 527)
        Me.txtOrden.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(118, 26)
        Me.txtOrden.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(25, 505)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(111, 18)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Clave Orden :"
        '
        'btnResultado
        '
        Me.btnResultado.BackColor = System.Drawing.Color.DarkOrange
        Me.btnResultado.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnResultado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnResultado.ForeColor = System.Drawing.Color.Black
        Me.btnResultado.Location = New System.Drawing.Point(29, 452)
        Me.btnResultado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnResultado.Name = "btnResultado"
        Me.btnResultado.Size = New System.Drawing.Size(117, 28)
        Me.btnResultado.TabIndex = 11
        Me.btnResultado.Text = "&Buscar"
        Me.btnResultado.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(25, 396)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 18)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Resultado :"
        '
        'btnMac
        '
        Me.btnMac.BackColor = System.Drawing.Color.DarkOrange
        Me.btnMac.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMac.ForeColor = System.Drawing.Color.Black
        Me.btnMac.Location = New System.Drawing.Point(29, 341)
        Me.btnMac.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnMac.Name = "btnMac"
        Me.btnMac.Size = New System.Drawing.Size(117, 28)
        Me.btnMac.TabIndex = 9
        Me.btnMac.Text = "&Buscar"
        Me.btnMac.UseVisualStyleBackColor = False
        '
        'txtMac
        '
        Me.txtMac.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMac.Location = New System.Drawing.Point(29, 308)
        Me.txtMac.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtMac.Name = "txtMac"
        Me.txtMac.Size = New System.Drawing.Size(185, 26)
        Me.txtMac.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(25, 286)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 18)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Mac Adress :"
        '
        'btnContrato
        '
        Me.btnContrato.BackColor = System.Drawing.Color.DarkOrange
        Me.btnContrato.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContrato.ForeColor = System.Drawing.Color.Black
        Me.btnContrato.Location = New System.Drawing.Point(29, 231)
        Me.btnContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnContrato.Name = "btnContrato"
        Me.btnContrato.Size = New System.Drawing.Size(117, 28)
        Me.btnContrato.TabIndex = 7
        Me.btnContrato.Text = "&Buscar"
        Me.btnContrato.UseVisualStyleBackColor = False
        '
        'btnConsecutivo
        '
        Me.btnConsecutivo.BackColor = System.Drawing.Color.DarkOrange
        Me.btnConsecutivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConsecutivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsecutivo.ForeColor = System.Drawing.Color.Black
        Me.btnConsecutivo.Location = New System.Drawing.Point(29, 122)
        Me.btnConsecutivo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnConsecutivo.Name = "btnConsecutivo"
        Me.btnConsecutivo.Size = New System.Drawing.Size(117, 28)
        Me.btnConsecutivo.TabIndex = 5
        Me.btnConsecutivo.Text = "&Buscar"
        Me.btnConsecutivo.UseVisualStyleBackColor = False
        '
        'txtContrato
        '
        Me.txtContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContrato.Location = New System.Drawing.Point(29, 198)
        Me.txtContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtContrato.Name = "txtContrato"
        Me.txtContrato.Size = New System.Drawing.Size(118, 26)
        Me.txtContrato.TabIndex = 6
        '
        'txtConsecutivo
        '
        Me.txtConsecutivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtConsecutivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConsecutivo.Location = New System.Drawing.Point(29, 89)
        Me.txtConsecutivo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtConsecutivo.Name = "txtConsecutivo"
        Me.txtConsecutivo.Size = New System.Drawing.Size(117, 26)
        Me.txtConsecutivo.TabIndex = 4
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(24, 1)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(162, 29)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar  Por :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(25, 66)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(112, 18)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Consecutivo :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 176)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 18)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Contrato :"
        '
        'gvPPE
        '
        Me.gvPPE.AllowUserToAddRows = False
        Me.gvPPE.AllowUserToDeleteRows = False
        Me.gvPPE.AllowUserToOrderColumns = True
        Me.gvPPE.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvPPE.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gvPPE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvPPE.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gvPPE.Location = New System.Drawing.Point(0, 0)
        Me.gvPPE.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gvPPE.Name = "gvPPE"
        Me.gvPPE.ReadOnly = True
        Me.gvPPE.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvPPE.Size = New System.Drawing.Size(670, 854)
        Me.gvPPE.TabIndex = 1
        Me.gvPPE.TabStop = False
        '
        'FrmCnrPPE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 912)
        Me.Controls.Add(Me.btnModifica)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmCnrPPE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CNR PPE"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.gvPPE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnModifica As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblFechaH As System.Windows.Forms.Label
    Friend WithEvents lblConsecutivo As System.Windows.Forms.Label
    Friend WithEvents lblContrato As System.Windows.Forms.Label
    Friend WithEvents lblMac As System.Windows.Forms.Label
    Friend WithEvents lblPaquete As System.Windows.Forms.Label
    Friend WithEvents lblComando As System.Windows.Forms.Label
    Friend WithEvents lblResultado As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblClv As System.Windows.Forms.Label
    Friend WithEvents lblFechaE As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnFechaH As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cbResultado As System.Windows.Forms.ComboBox
    Friend WithEvents btnFechaE As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnClv As System.Windows.Forms.Button
    Friend WithEvents txtOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnResultado As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnMac As System.Windows.Forms.Button
    Friend WithEvents txtMac As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnContrato As System.Windows.Forms.Button
    Friend WithEvents btnConsecutivo As System.Windows.Forms.Button
    Friend WithEvents txtContrato As System.Windows.Forms.TextBox
    Friend WithEvents txtConsecutivo As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents gvPPE As System.Windows.Forms.DataGridView
    Friend WithEvents dtpFechaH As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaE As System.Windows.Forms.DateTimePicker
End Class
