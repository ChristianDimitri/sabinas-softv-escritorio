﻿Public Class FrmRelClientesExtensiones

    Private Sub CONRelClientesExtensiones(ByVal Contrato As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        dTable = BaseII.ConsultaDT("CONRelClientesExtensiones")
        nudExt.Value = 0
        If dTable.Rows.Count = 0 Then Exit Sub
        nudExt.Value = dTable.Rows(0)(0).ToString
    End Sub

    Private Sub NUERelClientesExtensiones(ByVal Contrato As Integer, ByVal Extensiones As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Extensiones", SqlDbType.Int, Extensiones)
        BaseII.Inserta("NUERelClientesExtensiones")
    End Sub

    Private Sub BORRelClientesExtensiones(ByVal Contrato As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.Inserta("BORRelClientesExtensiones")
    End Sub

    Private Sub FrmRelClientesExtensiones_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'FrmClientes.nudExtensioesCableadas.Text = nudExt.Value
    End Sub

    Private Sub FrmRelClientesExtensiones_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        CONRelClientesExtensiones(Contrato)
        tsbGuardar.Enabled = True
        tsbEliminar.Enabled = True
    End Sub

    Private Sub tsbGuardar_Click(sender As System.Object, e As System.EventArgs) Handles tsbGuardar.Click
        NUERelClientesExtensiones(Contrato, nudExt.Value)
        MessageBox.Show("Se guardó con éxito.")
        Me.Close()
    End Sub

    Private Sub tsbEliminar_Click(sender As System.Object, e As System.EventArgs) Handles tsbEliminar.Click
        BORRelClientesExtensiones(Contrato)
        MessageBox.Show("Se eliminó con éxito.")
        CONRelClientesExtensiones(Contrato)
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class