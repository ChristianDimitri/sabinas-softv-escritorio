﻿Imports System.Data.SqlClient
Public Class FrmSelCompaniaXML

    Private Sub FrmSelCompaniaXML_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CargarElementosCompaniaXML()
        llenalistboxs()
        If loquehay.Items.Count = 1 Or PasarTodoXML Then
            llevamealotro()
            Me.Close()
        End If
    End Sub

    Private Sub llenalistboxs()
        Dim dt As New DataTable
        dt.Columns.Add("IdCompania", Type.GetType("System.String"))
        dt.Columns.Add("Nombre", Type.GetType("System.String"))

        Dim dt2 As New DataTable
        dt2.Columns.Add("IdCompania", Type.GetType("System.String"))
        dt2.Columns.Add("Nombre", Type.GetType("System.String"))

        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Seleccion") = 0 Then
                dt.Rows.Add(elem.GetAttribute("IdCompania"), elem.GetAttribute("Nombre"))
            End If
            If elem.GetAttribute("Seleccion") = 1 Then
                dt2.Rows.Add(elem.GetAttribute("IdCompania"), elem.GetAttribute("Nombre"))
            End If
        Next
        loquehay.DataSource = New BindingSource(dt, Nothing)
        seleccion.DataSource = New BindingSource(dt2, Nothing)
    End Sub

    Private Sub agregar_Click(sender As Object, e As EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("IdCompania") = loquehay.SelectedValue Then
                elem.SetAttribute("Seleccion", 1)
            End If
        Next
        llenalistboxs()
    End Sub

    Private Sub llevamealotro()
        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        If GloOpFiltrosXML = "VariosDesconectados" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaListado" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaOrdenes" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "VariosAlCorriente" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "VariosContrataciones" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCortesia" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCiudad" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "AtencionTelefonica" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "PendientesDeRealizar" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "ResumenOrdenReporte" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "DetalleOrdenes" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "ListadoQuejas" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "ListaCumpleanos" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "EnviarSMS" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "QuejasConcetoDetallado" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "InterfazDigital" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "InstalacionesEfectuadas" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "bitacoradepruebas" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "Hoteles" Then
            FrmImprimirComision.Show()
        End If
        If GloOpFiltrosXML = "Permanencia" Then
            FrmRepPermanencia.Show()
        End If
        If GloOpFiltrosXML = "BitacoraEdoCuenta" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "ListadoActividadesTecnico" Then
            FrmSelTecnicoXML.Show()
        End If
        If GloOpFiltrosXML = "AgentaTecnico" Then
            FrmSelRepAgendaTecnico.Show()
        End If
        If GloOpFiltrosXML = "reporteSuscriptoresBruno" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "sindocumentos" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "MensajesVarios" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "Carteras" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "CarteraDistribuidor" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "QuejasPorIdentificador" Then
            FrmSelConceptoXml.Show()
        End If
        If GloOpFiltrosXML = "OrdenesZonaNorte" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "ResumenClientesCobertura" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "CuentasClabe" Then
            FrmImprimirPPE.Show()
        End If
        If GloOpFiltrosXML = "BitacoraDatalogic" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "ResumenCajas" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "ReporteToken" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "PruebasDeInternet" Then
            FrmSelEstadoXML.Show()
        End If
    End Sub
    Private Sub agregartodo_Click(sender As Object, e As EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(sender As Object, e As EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("IdCompania") = seleccion.SelectedValue Then
                elem.SetAttribute("Seleccion", 0)
            End If
        Next
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(sender As Object, e As EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 0)
        Next
        llenalistboxs()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        If GloOpFiltrosXML = "VariosDesconectados" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaListado" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "AreaTecnicaOrdenes" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "VariosAlCorriente" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "VariosContrataciones" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCortesia" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "VariosCiudad" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "AtencionTelefonica" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "PendientesDeRealizar" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "ResumenOrdenReporte" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "DetalleOrdenes" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "ListadoQuejas" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "ListaCumpleanos" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "EnviarSMS" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "QuejasConcetoDetallado" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "InterfazDigital" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "InstalacionesEfectuadas" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "bitacoradepruebas" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "Hoteles" Then
            FrmImprimirComision.Show()
        End If
        If GloOpFiltrosXML = "Permanencia" Then
            FrmRepPermanencia.Show()
        End If
        If GloOpFiltrosXML = "BitacoraEdoCuenta" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "ListadoActividadesTecnico" Then
            FrmSelTecnicoXML.Show()
        End If
        If GloOpFiltrosXML = "AgentaTecnico" Then
            FrmSelRepAgendaTecnico.Show()
        End If
        If GloOpFiltrosXML = "reporteSuscriptoresBruno" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "sindocumentos" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "MensajesVarios" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "Carteras" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "CarteraDistribuidor" Then
            FrmSelEstadoXML.Show()
        End If
        If GloOpFiltrosXML = "QuejasPorIdentificador" Then
            FrmSelConceptoXml.Show()
        End If
        If GloOpFiltrosXML = "OrdenesZonaNorte" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "ResumenClientesCobertura" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "CuentasClabe" Then
            FrmImprimirPPE.Show()
        End If
        If GloOpFiltrosXML = "BitacoraDatalogic" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "ResumenCajas" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "ReporteToken" Then
            FrmSelFechasPPE.Show()
        End If
        If GloOpFiltrosXML = "PruebasDeInternet" Then
            FrmSelEstadoXML.Show()
        End If
        Me.Close()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class