﻿Public Class FrmSelCalleJ

    Private Sub FrmSelCalleJ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCalle")
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCalle")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCalle")
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCalle")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCalle")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCalle")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCalle")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count > 0 Then
            bndjano = 1
            Me.Close()
        End If  
    End Sub
End Class