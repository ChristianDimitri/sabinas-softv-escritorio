﻿Imports System.Data.SqlClient
Public Class FrmCompania
    Private Sub Llena_ciudades()
        Try
            BaseII.limpiaParametros()
            
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
            BaseII.CreateMyParameter("@clvestado", SqlDbType.Int, ComboBoxEstado.SelectedValue)
            ComboBoxCiudades.DataSource = BaseII.ConsultaDT("Muestra_Ciudad_RelCompania")
            ComboBoxCiudades.DisplayMember = "Nombre"
            ComboBoxCiudades.ValueMember = "Clv_Ciudad"
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llena_plaza(ByRef Clv_plaza As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_plaza", SqlDbType.Int, Clv_plaza)
            ComboBoxPlazas.DataSource = BaseII.ConsultaDT("Muestra_Plazas")
            ComboBoxPlazas.DisplayMember = "Nombre"
            ComboBoxPlazas.ValueMember = "clv_plaza"

            'If ComboBoxPlazas.Items.Count > 0 Then
            '    ComboBoxPlazas.SelectedIndex = 0
            'End If
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Llena_Estados()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            ComboBoxEstado.DataSource = BaseII.ConsultaDT("MuestraEstadosFrmCompania")
            ComboBoxEstado.DisplayMember = "Nombre"
            ComboBoxEstado.ValueMember = "Clv_Estado"
        Catch ex As Exception

        End Try
    End Sub
    Private Sub llena_dgvciudades()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        dgvciudades.DataSource = BaseII.ConsultaDT("AgregaEliminaRelCompaniaCiudad2")
    End Sub

    Private Sub FrmCompania_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llena_plaza(0)
        If opcion = "N" Then
            BindingNavigatorDeleteItem.Enabled = False
            Llena_Estados()
            Llena_ciudades()
            GroupBox1.Enabled = False
        End If
        If opcion = "C" Then
            BindingNavigatorDeleteItem.Enabled = False
            CONSERVICIOSBindingNavigatorSaveItem.Enabled = False
            tbrazonsocial.Enabled = False
            tbrfc.Enabled = False
            tbcalle.Enabled = False
            tbentrecalles.Enabled = False
            tbexterior.Enabled = False
            tbinterior.Enabled = False
            tbtelefono.Enabled = False
            tbcolonia.Enabled = False
            tbfax.Enabled = False
            tbcodigop.Enabled = False
            tbemail.Enabled = False
            tblocalidad.Enabled = False
            tbmunicipio.Enabled = False
            tbestado.Enabled = False
            tbpais.Enabled = False
            llenacampos()
            Llena_Estados()
            Llena_ciudades()
            llena_dgvciudades()
            GroupBox1.Enabled = False
            ComboBoxPlazas.Enabled = False
        End If
        If opcion = "M" Then
            BindingNavigatorDeleteItem.Enabled = True
            ComboBoxPlazas.Enabled = False
            llenacampos()
            Llena_Estados()
            Llena_ciudades()
            llena_dgvciudades()

        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
    Private Sub llenacampos()
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim comando As New SqlCommand()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "DameDatosCompaniaLong"
        Dim p1 As New SqlParameter("@idcompania", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = GloIdCompania
        comando.Parameters.Add(p1)
        Dim reader As SqlDataReader = comando.ExecuteReader()
        reader.Read()
        tbidcompania.Text = reader(0).ToString
        tbrazonsocial.Text = reader(1).ToString
        tbrfc.Text = ""
        tbcalle.Text = reader(3).ToString
        tbentrecalles.Text = reader(4).ToString
        tbexterior.Text = reader(5).ToString
        tbinterior.Text = reader(6).ToString
        tbtelefono.Text = reader(7).ToString
        tbcolonia.Text = reader(8).ToString
        tbfax.Text = reader(9).ToString
        tbcodigop.Text = reader(10).ToString
        tbemail.Text = reader(11).ToString
        tblocalidad.Text = reader(12).ToString
        tbmunicipio.Text = reader(13).ToString
        tbestado.Text = reader(14).ToString
        tbpais.Text = reader(15).ToString
        ComboBoxPlazas.SelectedValue = reader(16).ToString
        tbcontacto.Text = reader(17).ToString
        reader.Close()
        conexion.Close()


        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
        BaseII.CreateMyParameter("@calle", ParameterDirection.Output, SqlDbType.VarChar, 255)
        BaseII.CreateMyParameter("@entrecalles", ParameterDirection.Output, SqlDbType.VarChar, 255)
        BaseII.CreateMyParameter("@exterior", ParameterDirection.Output, SqlDbType.VarChar, 15)
        BaseII.CreateMyParameter("@interior", ParameterDirection.Output, SqlDbType.VarChar, 15)
        BaseII.CreateMyParameter("@colonia", ParameterDirection.Output, SqlDbType.VarChar, 255)
        BaseII.CreateMyParameter("@codigopostal", ParameterDirection.Output, SqlDbType.VarChar, 5)
        BaseII.CreateMyParameter("@localidad", ParameterDirection.Output, SqlDbType.VarChar, 255)
        BaseII.CreateMyParameter("@municipio", ParameterDirection.Output, SqlDbType.VarChar, 255)
        BaseII.CreateMyParameter("@estado", ParameterDirection.Output, SqlDbType.VarChar, 255)
        BaseII.CreateMyParameter("@pais", ParameterDirection.Output, SqlDbType.VarChar, 255)

        BaseII.ProcedimientoOutPut("muestraAlmacenDireccionPlaza")

        tbCalleAlm.Text = BaseII.dicoPar("@calle").ToString
        tbEntreCallesAlm.Text = BaseII.dicoPar("@entrecalles").ToString
        tbExtAlm.Text = BaseII.dicoPar("@exterior").ToString
        tbIntAlm.Text = BaseII.dicoPar("@interior").ToString
        tbColAlm.Text = BaseII.dicoPar("@colonia").ToString
        tbCPAlm.Text = BaseII.dicoPar("@codigopostal").ToString
        tbLocAlm.Text = BaseII.dicoPar("@localidad").ToString
        tbMunALm.Text = BaseII.dicoPar("@municipio").ToString
        tbEstAlm.Text = BaseII.dicoPar("@estado").ToString
        tbPaisAlm.Text = BaseII.dicoPar("@pais").ToString
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim comando As New SqlCommand()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "EliminaCompania"
        Dim p1 As New SqlParameter("@idcompania", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = GloIdCompania
        comando.Parameters.Add(p1)
        Dim bnddelete As Integer = comando.ExecuteScalar()
        If bnddelete = 0 Then
            MsgBox("Plaza eliminada exitosamente")
        ElseIf bnddelete = 1 Then
            MsgBox("La plaza ya tiene clientes asignados. No se puede eliminar")
        End If
        conexion.Close()
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        'If (opcion = "N" Or opcion = "M") And (tbrazonsocial.Text = "" Or tbcalle.Text = "" Or tbcodigop.Text = "" Or tbcolonia.Text = "" Or tbestado.Text = "" Or tbexterior.Text = "" Or tbmunicipio.Text = "" Or tbpais.Text = "") Then
        '    MsgBox("Aún faltan campos por completar")
        '    Exit Sub
        'End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, tbrazonsocial.Text)
        BaseII.CreateMyParameter("@mismoNombre", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
        BaseII.ProcedimientoOutPut("ValidaNombreCompania")
        Dim mismoNombre = BaseII.dicoPar("@mismoNombre")
        If mismoNombre = 1 Then
            MsgBox("Ya existe una plaza con el mismo nombre.")
            Exit Sub
        End If
        If (opcion = "N" Or opcion = "M") And (tbrazonsocial.Text = "" Or tbcalle.Text.Trim.Length = 0 Or tbentrecalles.Text.Trim.Length = 0 Or tbexterior.Text.Trim.Length = 0 Or tbtelefono.Text.Trim.Length = 0 Or tbcolonia.Text.Trim.Length = 0 Or tbcodigop.Text.Trim.Length = 0 Or tbemail.Text.Trim.Length = 0 Or tblocalidad.Text.Trim.Length = 0 Or tbmunicipio.Text.Trim.Length = 0 Or tbestado.Text.Trim.Length = 0 Or tbestado.Text.Trim.Length = 0 Or tbpais.Text.Trim.Length = 0 Or tbcontacto.Text.Trim.Length = 0 Or (tbCalleAlm.Text.Trim.Length = 0 Or tbEntreCallesAlm.Text.Trim.Length = 0 Or tbExtAlm.Text.Trim.Length = 0 Or tbIntAlm.Text.Trim.Length = 0 Or tbColAlm.Text.Trim.Length = 0 Or tbCPAlm.Text.Trim.Length = 0 Or tbLocAlm.Text.Trim.Length = 0 Or tbMunALm.Text.Trim.Length = 0 Or tbEstAlm.Text.Trim.Length = 0 Or tbPaisAlm.Text.Trim.Length = 0)) Then
            MsgBox("Aún faltan campos por completar")
            Exit Sub
        End If
        If opcion = "N" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
            BaseII.CreateMyParameter("@razonsocial", SqlDbType.VarChar, tbrazonsocial.Text)
            BaseII.CreateMyParameter("@rfc", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, tbcalle.Text)
            BaseII.CreateMyParameter("@entrecalles", SqlDbType.VarChar, tbentrecalles.Text)
            BaseII.CreateMyParameter("@exterior", SqlDbType.VarChar, tbexterior.Text)
            BaseII.CreateMyParameter("@interior", SqlDbType.VarChar, tbinterior.Text)
            BaseII.CreateMyParameter("@telefono", SqlDbType.VarChar, tbtelefono.Text)
            BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, tbcolonia.Text)
            BaseII.CreateMyParameter("@fax", SqlDbType.VarChar, tbfax.Text)
            BaseII.CreateMyParameter("@codigopostal", SqlDbType.VarChar, tbcodigop.Text)
            BaseII.CreateMyParameter("@email", SqlDbType.VarChar, tbemail.Text)
            BaseII.CreateMyParameter("@localidad", SqlDbType.VarChar, tblocalidad.Text)
            BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, tbmunicipio.Text)
            BaseII.CreateMyParameter("@estado", SqlDbType.VarChar, tbestado.Text)
            BaseII.CreateMyParameter("@pais", SqlDbType.VarChar, tbpais.Text)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlazas.SelectedValue)
            BaseII.CreateMyParameter("@contacto", SqlDbType.VarChar, tbcontacto.Text, 150)
            Dim dt As DataTable = BaseII.ConsultaDT("InsertaUpdateCompania")
            tbidcompania.Text = dt.Rows(0)(0).ToString

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, tbCalleAlm.Text)
            BaseII.CreateMyParameter("@entrecalles", SqlDbType.VarChar, tbEntreCallesAlm.Text)
            BaseII.CreateMyParameter("@exterior", SqlDbType.VarChar, tbExtAlm.Text)
            BaseII.CreateMyParameter("@interior", SqlDbType.VarChar, tbIntAlm.Text)
            BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, tbColAlm.Text)
            BaseII.CreateMyParameter("@codigopostal", SqlDbType.VarChar, tbCPAlm.Text)
            BaseII.CreateMyParameter("@localidad", SqlDbType.VarChar, tbLocAlm.Text)
            BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, tbMunALm.Text)
            BaseII.CreateMyParameter("@estado", SqlDbType.VarChar, tbEstAlm.Text)
            BaseII.CreateMyParameter("@pais", SqlDbType.VarChar, tbPaisAlm.Text)
            BaseII.Inserta("InsertaAlmacenDireccionPlaza")


            MsgBox("Plaza guardada con éxito")
            opcion = "M"
            GroupBox1.Enabled = True
        End If
        If opcion = "M" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
            BaseII.CreateMyParameter("@razonsocial", SqlDbType.VarChar, tbrazonsocial.Text)
            BaseII.CreateMyParameter("@rfc", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, tbcalle.Text)
            BaseII.CreateMyParameter("@entrecalles", SqlDbType.VarChar, tbentrecalles.Text)
            BaseII.CreateMyParameter("@exterior", SqlDbType.VarChar, tbexterior.Text)
            BaseII.CreateMyParameter("@interior", SqlDbType.VarChar, tbinterior.Text)
            BaseII.CreateMyParameter("@telefono", SqlDbType.VarChar, tbtelefono.Text)
            BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, tbcolonia.Text)
            BaseII.CreateMyParameter("@fax", SqlDbType.VarChar, tbfax.Text)
            BaseII.CreateMyParameter("@codigopostal", SqlDbType.VarChar, tbcodigop.Text)
            BaseII.CreateMyParameter("@email", SqlDbType.VarChar, tbemail.Text)
            BaseII.CreateMyParameter("@localidad", SqlDbType.VarChar, tblocalidad.Text)
            BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, tbmunicipio.Text)
            BaseII.CreateMyParameter("@estado", SqlDbType.VarChar, tbestado.Text)
            BaseII.CreateMyParameter("@pais", SqlDbType.VarChar, tbpais.Text)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlazas.SelectedValue)
            BaseII.CreateMyParameter("@contacto", SqlDbType.VarChar, tbcontacto.Text, 150)
            BaseII.Inserta("InsertaUpdateCompania")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, tbCalleAlm.Text)
            BaseII.CreateMyParameter("@entrecalles", SqlDbType.VarChar, tbEntreCallesAlm.Text)
            BaseII.CreateMyParameter("@exterior", SqlDbType.VarChar, tbExtAlm.Text)
            BaseII.CreateMyParameter("@interior", SqlDbType.VarChar, tbIntAlm.Text)
            BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, tbColAlm.Text)
            BaseII.CreateMyParameter("@codigopostal", SqlDbType.VarChar, tbCPAlm.Text)
            BaseII.CreateMyParameter("@localidad", SqlDbType.VarChar, tbLocAlm.Text)
            BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, tbMunALm.Text)
            BaseII.CreateMyParameter("@estado", SqlDbType.VarChar, tbEstAlm.Text)
            BaseII.CreateMyParameter("@pais", SqlDbType.VarChar, tbPaisAlm.Text)
            BaseII.Inserta("InsertaAlmacenDireccionPlaza")

            MsgBox("Datos guardados exitosamente")
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ComboBoxCiudades.SelectedValue = 0 Then
            Exit Sub
        End If
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand()
        conexion.Open()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "AgregaEliminaRelCompaniaCiudad2"
        Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
        p4.Direction = ParameterDirection.Input
        p4.Value = 1
        comando.Parameters.Add(p4)
        Dim p1 As New SqlParameter("@clv_ciudad", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = ComboBoxCiudades.SelectedValue
        comando.Parameters.Add(p1)
        Dim p2 As New SqlParameter("@idcompania", SqlDbType.Int)
        p2.Direction = ParameterDirection.Input
        p2.Value = tbidcompania.Text
        comando.Parameters.Add(p2)
        Dim p3 As New SqlParameter("@clv_estado", SqlDbType.Int)
        p3.Direction = ParameterDirection.Input
        p3.Value = ComboBoxEstado.SelectedValue
        comando.Parameters.Add(p3)
        Dim resultado As Integer = comando.ExecuteScalar()
        If resultado = 1 Then
            MsgBox("La plaza ya pertenece a la relación.")
        Else
            llena_dgvciudades()
            Llena_ciudades()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If dgvciudades.Rows.Count = 0 Then
            Exit Sub
        End If

        ValidaEliminarRelCompaniaCiudad(1)
        If Len(eMsj) > 0 Then
            MsgBox(eMsj, MsgBoxStyle.Information)
            Exit Sub
        End If

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand()
        conexion.Open()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "AgregaEliminaRelCompaniaCiudad2"
        Dim p4 As New SqlParameter("@opcion", SqlDbType.Int)
        p4.Direction = ParameterDirection.Input
        p4.Value = 2
        comando.Parameters.Add(p4)
        Dim p1 As New SqlParameter("@clv_ciudad", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = dgvciudades.SelectedCells(0).Value.ToString
        comando.Parameters.Add(p1)
        Dim p2 As New SqlParameter("@idcompania", SqlDbType.Int)
        p2.Direction = ParameterDirection.Input
        p2.Value = tbidcompania.Text
        comando.Parameters.Add(p2)
        Dim p3 As New SqlParameter("@clv_estado", SqlDbType.Int)
        p3.Direction = ParameterDirection.Input
        p3.Value = dgvciudades.SelectedCells(3).Value.ToString
        comando.Parameters.Add(p3)
        comando.ExecuteNonQuery()
        llena_dgvciudades()
        Llena_ciudades()
    End Sub
    Private Sub ValidaEliminarRelCompaniaCiudad(ByVal Op As Integer)
        Dim clv_ciudad As Integer = 0

        If dgvciudades.Rows.Count > 0 Then
            clv_ciudad = dgvciudades.SelectedCells(0).Value.ToString
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, clv_ciudad)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, tbidcompania.Text)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("ValidaEliminarRelCompaniaCiudad")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub CONSERVICIOSBindingNavigator_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSERVICIOSBindingNavigator.RefreshItems

    End Sub

    Private Sub ComboBoxEstado_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxEstado.SelectedIndexChanged
        Try
            Llena_ciudades()
        Catch ex As Exception

        End Try
    End Sub


End Class
