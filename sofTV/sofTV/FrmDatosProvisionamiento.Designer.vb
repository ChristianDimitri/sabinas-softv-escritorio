﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDatosProvisionamiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.tbNodo = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TbEquipo = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(203, 140)
        Me.Button5.MaximumSize = New System.Drawing.Size(136, 33)
        Me.Button5.MinimumSize = New System.Drawing.Size(136, 33)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 108
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'tbNodo
        '
        Me.tbNodo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNodo.CausesValidation = False
        Me.tbNodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNodo.Location = New System.Drawing.Point(145, 47)
        Me.tbNodo.Name = "tbNodo"
        Me.tbNodo.ReadOnly = True
        Me.tbNodo.Size = New System.Drawing.Size(194, 21)
        Me.tbNodo.TabIndex = 105
        Me.tbNodo.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(12, 90)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(129, 15)
        Me.Label9.TabIndex = 107
        Me.Label9.Text = "Equipo del Cliente:"
        '
        'TbEquipo
        '
        Me.TbEquipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TbEquipo.CausesValidation = False
        Me.TbEquipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbEquipo.Location = New System.Drawing.Point(145, 88)
        Me.TbEquipo.Name = "TbEquipo"
        Me.TbEquipo.ReadOnly = True
        Me.TbEquipo.Size = New System.Drawing.Size(194, 21)
        Me.TbEquipo.TabIndex = 106
        Me.TbEquipo.TabStop = False
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label35.Location = New System.Drawing.Point(12, 47)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(95, 15)
        Me.Label35.TabIndex = 104
        Me.Label35.Text = "Nodo de Red:"
        '
        'FrmDatosProvisionamiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(351, 195)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.tbNodo)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.TbEquipo)
        Me.Controls.Add(Me.Label35)
        Me.Name = "FrmDatosProvisionamiento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos Aprovisionamiento"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents tbNodo As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TbEquipo As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
End Class
