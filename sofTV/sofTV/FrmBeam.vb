﻿Public Class FrmBeam
    Public idCobertura As Integer = 0
    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        If tbNombre.Text = "" Then
            MsgBox("Tiene que llenar el campo Nombre para poder continuar.")
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, tbNombre.Text)
        BaseII.CreateMyParameter("@idCobertura", SqlDbType.Int, tbClave.Text)
        BaseII.CreateMyParameter("@zonaNorte", SqlDbType.Bit, ckbZonaNorte.Checked)
        BaseII.CreateMyParameter("@errorMsg", ParameterDirection.Output, SqlDbType.VarChar, 500)
        BaseII.ProcedimientoOutPut("GuardaCobertura")
        If BaseII.dicoPar("@errorMsg") = "" Then
            MsgBox("Guardado con éxito.")
            Me.Close()
            BrwBeam.actualiza = True
        Else
            MsgBox(BaseII.dicoPar("@errorMsg"))
        End If
    End Sub

    Private Sub FrmBeam_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tbClave.Text = idCobertura.ToString()
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        If opcion = "N" Then
            BindingNavigatorDeleteItem.Enabled = False
        ElseIf opcion = "M" Then
            BaseII.CreateMyParameter("@idCobertura", SqlDbType.Int, tbClave.Text)
            BaseII.CreateMyParameter("@nombre", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.CreateMyParameter("@zonaNorte", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("DameDatosCobertura")
            tbNombre.Text = BaseII.dicoPar("@nombre")
            ckbZonaNorte.Checked = BaseII.dicoPar("@zonaNorte")
            BindingNavigatorDeleteItem.Enabled = True
        ElseIf opcion = "C" Then
            BaseII.CreateMyParameter("@idCobertura", SqlDbType.Int, tbClave.Text)
            BaseII.CreateMyParameter("@nombre", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.CreateMyParameter("@zonaNorte", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("DameDatosCobertura")
            tbNombre.Text = BaseII.dicoPar("@nombre")
            ckbZonaNorte.Checked = BaseII.dicoPar("@zonaNorte")
            ckbZonaNorte.Enabled = False
            tbNombre.Enabled = False
            CONSERVICIOSBindingNavigatorSaveItem.Enabled = False
            BindingNavigatorDeleteItem.Enabled = False
            BindingNavigatorDeleteItem.Enabled = False
        End If
    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idCobertura", SqlDbType.Int, tbClave.Text)
        BaseII.CreateMyParameter("@errorMsg", ParameterDirection.Output, SqlDbType.VarChar, 500)
        BaseII.ProcedimientoOutPut("EliminaCobertura")
        If BaseII.dicoPar("@errorMsg") = "" Then
            MsgBox("Eliminado con éxito.")
            Me.Close()
            BrwBeam.actualiza = True
        Else
            MsgBox(BaseII.dicoPar("@errorMsg"))
        End If
    End Sub
End Class