﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPromocionPrimerMensualidad
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPromocionPrimerMensualidad))
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.dgvPromocion = New System.Windows.Forms.DataGridView()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descuento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescuentoCombo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cbTipSer = New System.Windows.Forms.ComboBox()
        Me.bnPromocion = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.bnSalir = New System.Windows.Forms.Button()
        CType(Me.dgvPromocion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnPromocion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnPromocion.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(68, 49)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(130, 18)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Tipo de Servicio"
        '
        'dgvPromocion
        '
        Me.dgvPromocion.AllowUserToAddRows = False
        Me.dgvPromocion.AllowUserToDeleteRows = False
        Me.dgvPromocion.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPromocion.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPromocion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPromocion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Servicio, Me.Servicio, Me.Importe, Me.Descuento, Me.DescuentoCombo})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPromocion.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvPromocion.Location = New System.Drawing.Point(72, 153)
        Me.dgvPromocion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvPromocion.Name = "dgvPromocion"
        Me.dgvPromocion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPromocion.Size = New System.Drawing.Size(1019, 416)
        Me.dgvPromocion.TabIndex = 3
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.Visible = False
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.Width = 300
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle2
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.Width = 120
        '
        'Descuento
        '
        Me.Descuento.DataPropertyName = "Descuento"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Format = "C2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Descuento.DefaultCellStyle = DataGridViewCellStyle3
        Me.Descuento.HeaderText = "Descuento Servicio Único"
        Me.Descuento.Name = "Descuento"
        Me.Descuento.Width = 140
        '
        'DescuentoCombo
        '
        Me.DescuentoCombo.DataPropertyName = "DescuentoCombo"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DescuentoCombo.DefaultCellStyle = DataGridViewCellStyle4
        Me.DescuentoCombo.HeaderText = "Descuento Combo"
        Me.DescuentoCombo.Name = "DescuentoCombo"
        Me.DescuentoCombo.Width = 140
        '
        'cbTipSer
        '
        Me.cbTipSer.DisplayMember = "Concepto"
        Me.cbTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipSer.FormattingEnabled = True
        Me.cbTipSer.Location = New System.Drawing.Point(72, 71)
        Me.cbTipSer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbTipSer.Name = "cbTipSer"
        Me.cbTipSer.Size = New System.Drawing.Size(420, 26)
        Me.cbTipSer.TabIndex = 4
        Me.cbTipSer.ValueMember = "Clv_TipSer"
        '
        'bnPromocion
        '
        Me.bnPromocion.AddNewItem = Nothing
        Me.bnPromocion.CountItem = Nothing
        Me.bnPromocion.DeleteItem = Nothing
        Me.bnPromocion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnPromocion.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.bnPromocion.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbGuardar})
        Me.bnPromocion.Location = New System.Drawing.Point(0, 0)
        Me.bnPromocion.MoveFirstItem = Nothing
        Me.bnPromocion.MoveLastItem = Nothing
        Me.bnPromocion.MoveNextItem = Nothing
        Me.bnPromocion.MovePreviousItem = Nothing
        Me.bnPromocion.Name = "bnPromocion"
        Me.bnPromocion.PositionItem = Nothing
        Me.bnPromocion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnPromocion.Size = New System.Drawing.Size(1207, 27)
        Me.bnPromocion.TabIndex = 5
        Me.bnPromocion.Text = "BindingNavigator1"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(107, 24)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(1009, 608)
        Me.bnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(181, 44)
        Me.bnSalir.TabIndex = 6
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'FrmPromocionPrimerMensualidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1207, 667)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnPromocion)
        Me.Controls.Add(Me.cbTipSer)
        Me.Controls.Add(Me.dgvPromocion)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmPromocionPrimerMensualidad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promoción Primer Mensualidad"
        CType(Me.dgvPromocion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnPromocion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnPromocion.ResumeLayout(False)
        Me.bnPromocion.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents dgvPromocion As System.Windows.Forms.DataGridView
    Friend WithEvents cbTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents bnPromocion As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descuento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescuentoCombo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
