﻿Public Class FrmCoordenadas
    Private Sub FrmCoordenadas_Load(sender As Object, e As EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click        
        If Not (IsNumeric(tblat.Text) And IsNumeric(TbLatMin.Text) And IsNumeric(tblong.Text) And IsNumeric(TbLongMin.Text)) Then
            Exit Sub
        End If
        Dim lat As Double = (tblat.Text) + (TbLatMin.Text / 60)
        Dim longitud As Double = (tblong.Text) - (TbLongMin.Text / 60)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, GloDetClave)
            BaseII.CreateMyParameter("@latitud", SqlDbType.Float, tblat.Text)
            BaseII.CreateMyParameter("@longitud", SqlDbType.Float, tblong.Text)
            BaseII.CreateMyParameter("@latMin", SqlDbType.Float, TbLatMin.Text)
            BaseII.CreateMyParameter("@longMin", SqlDbType.Float, TbLongMin.Text)
            BaseII.Inserta("SP_GuardaCoordenadasPorIANTX")
            Me.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        'If Not (IsNumeric(tblat.Text) And IsNumeric(TbLatMin.Text) And IsNumeric(tblong.Text) And IsNumeric(TbLongMin.Text)) Then
        '    MsgBox("Es necesario registrar las coordenadas para continuar.")
        '    Exit Sub
        'End If
        Me.Close()
    End Sub


    'Private Sub tbLatDecimal_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tbLatDecimal.KeyUp
    '    If Not IsNumeric(tbLatDecimal.Text) Then
    '        Exit Sub     
    '    End If
    '    tblat.Text = Fix(tbLatDecimal.Text) ' \ 1
    '    TbLatMin.Text = (tbLatDecimal.Text Mod 1) * 60
    'End Sub

    'Private Sub tbLongDecimal_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tbLongDecimal.KeyUp
    '    If Not IsNumeric(tbLongDecimal.Text) Then
    '        Exit Sub        
    '    End If
    '    tblong.Text = tbLongDecimal.Text \ 1
    '    TbLongMin.Text = (tbLongDecimal.Text Mod 1) * -60
    'End Sub

    'Private Sub TbLongMin_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles TbLongMin.KeyUp
    '    If Not (IsNumeric(tblong.Text) And IsNumeric(TbLongMin.Text)) Then
    '        Exit Sub
    '    End If
    '    tbLongDecimal.Text = (tblong.Text) - (TbLongMin.Text / 60)
    'End Sub

    'Private Sub tblong_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tblong.KeyUp
    '    If Not (IsNumeric(tblong.Text) And IsNumeric(TbLongMin.Text)) Then          
    '        Exit Sub
    '    End If
    '    tbLongDecimal.Text = (tblong.Text) - (TbLongMin.Text / 60)
    'End Sub

    'Private Sub TbLatMin_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles TbLatMin.KeyUp
    '    If Not (IsNumeric(tblat.Text) And IsNumeric(TbLatMin.Text)) Then
    '        Exit Sub
    '    End If
    '    tbLatDecimal.Text = (tblat.Text) + (TbLatMin.Text / 60)
    'End Sub

    'Private Sub tblat_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tblat.KeyUp
    '    If Not (IsNumeric(tblat.Text) And IsNumeric(TbLatMin.Text)) Then
    '        Exit Sub     
    '    End If
    '    tbLatDecimal.Text = (tblat.Text) + (TbLatMin.Text / 60)
    'End Sub
End Class