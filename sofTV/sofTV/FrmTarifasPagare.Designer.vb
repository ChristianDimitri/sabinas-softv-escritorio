﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTarifasPagare
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.TipServCombo = New System.Windows.Forms.ComboBox()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.HDRadioBut = New System.Windows.Forms.RadioButton()
        Me.NormalRadioBut = New System.Windows.Forms.RadioButton()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.AparatoCombo = New System.Windows.Forms.ComboBox()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.CostoText = New System.Windows.Forms.TextBox()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBLabel6 = New System.Windows.Forms.Label()
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.RentaPrincipalText = New System.Windows.Forms.TextBox()
        Me.RentaAdicionalText = New System.Windows.Forms.TextBox()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.RentaAdicionalText2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RentaAdicionalText3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CBoxSERVICIO = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 36)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(109, 16)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Tipo Servicio :"
        '
        'TipServCombo
        '
        Me.TipServCombo.DisplayMember = "CONCEPTO"
        Me.TipServCombo.FormattingEnabled = True
        Me.TipServCombo.Location = New System.Drawing.Point(124, 35)
        Me.TipServCombo.Name = "TipServCombo"
        Me.TipServCombo.Size = New System.Drawing.Size(484, 21)
        Me.TipServCombo.TabIndex = 1
        Me.TipServCombo.ValueMember = "CLV_TIPSER"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(413, 210)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(110, 16)
        Me.CMBLabel3.TabIndex = 3
        Me.CMBLabel3.Text = "Tipo Paquete :"
        Me.CMBLabel3.Visible = False
        '
        'HDRadioBut
        '
        Me.HDRadioBut.AutoSize = True
        Me.HDRadioBut.Location = New System.Drawing.Point(529, 236)
        Me.HDRadioBut.Name = "HDRadioBut"
        Me.HDRadioBut.Size = New System.Drawing.Size(41, 17)
        Me.HDRadioBut.TabIndex = 3
        Me.HDRadioBut.Text = "HD"
        Me.HDRadioBut.UseVisualStyleBackColor = True
        Me.HDRadioBut.Visible = False
        '
        'NormalRadioBut
        '
        Me.NormalRadioBut.AutoSize = True
        Me.NormalRadioBut.Checked = True
        Me.NormalRadioBut.Location = New System.Drawing.Point(529, 213)
        Me.NormalRadioBut.Name = "NormalRadioBut"
        Me.NormalRadioBut.Size = New System.Drawing.Size(58, 17)
        Me.NormalRadioBut.TabIndex = 2
        Me.NormalRadioBut.TabStop = True
        Me.NormalRadioBut.Text = "Normal"
        Me.NormalRadioBut.UseVisualStyleBackColor = True
        Me.NormalRadioBut.Visible = False
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(32, 113)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(86, 16)
        Me.CMBLabel4.TabIndex = 5
        Me.CMBLabel4.Text = "Setup Box :"
        '
        'AparatoCombo
        '
        Me.AparatoCombo.DisplayMember = "Descripcion"
        Me.AparatoCombo.FormattingEnabled = True
        Me.AparatoCombo.Location = New System.Drawing.Point(124, 112)
        Me.AparatoCombo.Name = "AparatoCombo"
        Me.AparatoCombo.Size = New System.Drawing.Size(405, 21)
        Me.AparatoCombo.TabIndex = 4
        Me.AparatoCombo.ValueMember = "NoArticulo"
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(8, 149)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(155, 16)
        Me.CMBLabel5.TabIndex = 8
        Me.CMBLabel5.Text = "Costo del Equipo :   $"
        '
        'CostoText
        '
        Me.CostoText.Location = New System.Drawing.Point(160, 149)
        Me.CostoText.Name = "CostoText"
        Me.CostoText.Size = New System.Drawing.Size(154, 20)
        Me.CostoText.TabIndex = 5
        '
        'SaveButton
        '
        Me.SaveButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.Location = New System.Drawing.Point(153, 357)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(142, 35)
        Me.SaveButton.TabIndex = 10
        Me.SaveButton.Text = "&Guardar"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'ExitButton
        '
        Me.ExitButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitButton.Location = New System.Drawing.Point(359, 357)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(140, 35)
        Me.ExitButton.TabIndex = 11
        Me.ExitButton.Text = "&Cancelar"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'CMBLabel6
        '
        Me.CMBLabel6.AutoSize = True
        Me.CMBLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel6.Location = New System.Drawing.Point(21, 186)
        Me.CMBLabel6.Name = "CMBLabel6"
        Me.CMBLabel6.Size = New System.Drawing.Size(142, 16)
        Me.CMBLabel6.TabIndex = 12
        Me.CMBLabel6.Text = "Renta Principal :   $"
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.Location = New System.Drawing.Point(17, 225)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(176, 16)
        Me.CMBLabel7.TabIndex = 13
        Me.CMBLabel7.Text = "Renta Adicional 1ra. :   $"
        '
        'RentaPrincipalText
        '
        Me.RentaPrincipalText.Location = New System.Drawing.Point(160, 186)
        Me.RentaPrincipalText.Name = "RentaPrincipalText"
        Me.RentaPrincipalText.Size = New System.Drawing.Size(154, 20)
        Me.RentaPrincipalText.TabIndex = 6
        '
        'RentaAdicionalText
        '
        Me.RentaAdicionalText.Location = New System.Drawing.Point(203, 225)
        Me.RentaAdicionalText.Name = "RentaAdicionalText"
        Me.RentaAdicionalText.Size = New System.Drawing.Size(154, 20)
        Me.RentaAdicionalText.TabIndex = 7
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(413, 189)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 16)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Compañía :"
        Me.Label1.Visible = False
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(505, 186)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(295, 21)
        Me.ComboBoxCompanias.TabIndex = 0
        Me.ComboBoxCompanias.Visible = False
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'RentaAdicionalText2
        '
        Me.RentaAdicionalText2.Location = New System.Drawing.Point(203, 258)
        Me.RentaAdicionalText2.Name = "RentaAdicionalText2"
        Me.RentaAdicionalText2.Size = New System.Drawing.Size(154, 20)
        Me.RentaAdicionalText2.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(17, 259)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(180, 16)
        Me.Label2.TabIndex = 112
        Me.Label2.Text = "Renta Adicional 2da. :   $"
        '
        'RentaAdicionalText3
        '
        Me.RentaAdicionalText3.Location = New System.Drawing.Point(203, 295)
        Me.RentaAdicionalText3.Name = "RentaAdicionalText3"
        Me.RentaAdicionalText3.Size = New System.Drawing.Size(154, 20)
        Me.RentaAdicionalText3.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(17, 295)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(176, 16)
        Me.Label3.TabIndex = 114
        Me.Label3.Text = "Renta Adicional 3ra. :   $"
        '
        'CBoxSERVICIO
        '
        Me.CBoxSERVICIO.DisplayMember = "SERVICIO"
        Me.CBoxSERVICIO.FormattingEnabled = True
        Me.CBoxSERVICIO.Location = New System.Drawing.Point(124, 74)
        Me.CBoxSERVICIO.Name = "CBoxSERVICIO"
        Me.CBoxSERVICIO.Size = New System.Drawing.Size(484, 21)
        Me.CBoxSERVICIO.TabIndex = 116
        Me.CBoxSERVICIO.ValueMember = "CLV_SERVICIO"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(45, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 16)
        Me.Label4.TabIndex = 115
        Me.Label4.Text = "Servicio :"
        '
        'FrmTarifasPagare
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(644, 404)
        Me.Controls.Add(Me.CBoxSERVICIO)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.RentaAdicionalText3)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.RentaAdicionalText2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboBoxCompanias)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.RentaAdicionalText)
        Me.Controls.Add(Me.RentaPrincipalText)
        Me.Controls.Add(Me.CMBLabel7)
        Me.Controls.Add(Me.CMBLabel6)
        Me.Controls.Add(Me.HDRadioBut)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.TipServCombo)
        Me.Controls.Add(Me.AparatoCombo)
        Me.Controls.Add(Me.NormalRadioBut)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.SaveButton)
        Me.Controls.Add(Me.CostoText)
        Me.Controls.Add(Me.CMBLabel5)
        Me.MaximizeBox = False
        Me.Name = "FrmTarifasPagare"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tarifas Pagaré"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TipServCombo As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents HDRadioBut As System.Windows.Forms.RadioButton
    Friend WithEvents NormalRadioBut As System.Windows.Forms.RadioButton
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents AparatoCombo As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents CostoText As System.Windows.Forms.TextBox
    Friend WithEvents SaveButton As System.Windows.Forms.Button
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBLabel6 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents RentaPrincipalText As System.Windows.Forms.TextBox
    Friend WithEvents RentaAdicionalText As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents RentaAdicionalText2 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents RentaAdicionalText3 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents CBoxSERVICIO As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
