﻿Public Class FrmDatosProvisionamiento

    Private Sub FrmDatosProvisionamiento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        muestraDatosProvisionamiento()

    End Sub

    Private Sub muestraDatosProvisionamiento()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, GloContrato)
        BaseII.CreateMyParameter("@Nodo", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@Equipo", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("muestraDatosProvisionamiento")
        tbNodo.Text = BaseII.dicoPar("@Nodo").ToString()
        TbEquipo.Text = BaseII.dicoPar("@Equipo").ToString()
     
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class