﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaquetesTelefonica
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PaquetesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBoxProveedor = New System.Windows.Forms.ComboBox()
        Me.ClaveLabel = New System.Windows.Forms.Label()
        Me.tbidBeam_Paquete = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbPlazas = New System.Windows.Forms.ListBox()
        Me.ComboBoxPlaza = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.ComboBoxServicio = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbBeam = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CheckBoxEsEmpresarial = New System.Windows.Forms.CheckBox()
        Me.cbCobertura = New System.Windows.Forms.ComboBox()
        Me.DataGridViewPlazas = New System.Windows.Forms.DataGridView()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewComboBoxColumn()
        CType(Me.PaquetesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PaquetesBindingNavigator.SuspendLayout()
        CType(Me.DataGridViewPlazas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PaquetesBindingNavigator
        '
        Me.PaquetesBindingNavigator.AddNewItem = Nothing
        Me.PaquetesBindingNavigator.CountItem = Nothing
        Me.PaquetesBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.PaquetesBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.PaquetesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.PaquetesBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.PaquetesBindingNavigator.MoveFirstItem = Nothing
        Me.PaquetesBindingNavigator.MoveLastItem = Nothing
        Me.PaquetesBindingNavigator.MoveNextItem = Nothing
        Me.PaquetesBindingNavigator.MovePreviousItem = Nothing
        Me.PaquetesBindingNavigator.Name = "PaquetesBindingNavigator"
        Me.PaquetesBindingNavigator.PositionItem = Nothing
        Me.PaquetesBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PaquetesBindingNavigator.Size = New System.Drawing.Size(715, 28)
        Me.PaquetesBindingNavigator.TabIndex = 41
        Me.PaquetesBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(102, 25)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(105, 25)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'BindingNavigatorSaveItem
        '
        Me.BindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem"
        Me.BindingNavigatorSaveItem.Size = New System.Drawing.Size(101, 25)
        Me.BindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(488, 558)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 104
        Me.Button5.Text = "SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(152, 96)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.MaxLength = 255
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(429, 24)
        Me.TextBox1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(61, 102)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 18)
        Me.Label2.TabIndex = 105
        Me.Label2.Text = "Nombre:"
        '
        'ComboBoxProveedor
        '
        Me.ComboBoxProveedor.DisplayMember = "nombre"
        Me.ComboBoxProveedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxProveedor.FormattingEnabled = True
        Me.ComboBoxProveedor.Location = New System.Drawing.Point(152, 137)
        Me.ComboBoxProveedor.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxProveedor.Name = "ComboBoxProveedor"
        Me.ComboBoxProveedor.Size = New System.Drawing.Size(329, 26)
        Me.ComboBoxProveedor.TabIndex = 1
        Me.ComboBoxProveedor.ValueMember = "idProveedor"
        '
        'ClaveLabel
        '
        Me.ClaveLabel.AutoSize = True
        Me.ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.ClaveLabel.Location = New System.Drawing.Point(113, 59)
        Me.ClaveLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ClaveLabel.Name = "ClaveLabel"
        Me.ClaveLabel.Size = New System.Drawing.Size(26, 18)
        Me.ClaveLabel.TabIndex = 37
        Me.ClaveLabel.Text = "Id:"
        '
        'tbidBeam_Paquete
        '
        Me.tbidBeam_Paquete.BackColor = System.Drawing.Color.White
        Me.tbidBeam_Paquete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbidBeam_Paquete.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbidBeam_Paquete.Enabled = False
        Me.tbidBeam_Paquete.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbidBeam_Paquete.Location = New System.Drawing.Point(152, 57)
        Me.tbidBeam_Paquete.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbidBeam_Paquete.MaxLength = 5
        Me.tbidBeam_Paquete.Name = "tbidBeam_Paquete"
        Me.tbidBeam_Paquete.Size = New System.Drawing.Size(133, 24)
        Me.tbidBeam_Paquete.TabIndex = 38
        Me.tbidBeam_Paquete.Text = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(43, 144)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 18)
        Me.Label1.TabIndex = 210
        Me.Label1.Text = "Proveedor:"
        '
        'lbPlazas
        '
        Me.lbPlazas.DisplayMember = "Nombre"
        Me.lbPlazas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbPlazas.FormattingEnabled = True
        Me.lbPlazas.ItemHeight = 20
        Me.lbPlazas.Location = New System.Drawing.Point(21, 558)
        Me.lbPlazas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.lbPlazas.Name = "lbPlazas"
        Me.lbPlazas.Size = New System.Drawing.Size(443, 44)
        Me.lbPlazas.TabIndex = 211
        Me.lbPlazas.ValueMember = "IdCompania"
        Me.lbPlazas.Visible = False
        '
        'ComboBoxPlaza
        '
        Me.ComboBoxPlaza.DisplayMember = "Nombre"
        Me.ComboBoxPlaza.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxPlaza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxPlaza.FormattingEnabled = True
        Me.ComboBoxPlaza.Location = New System.Drawing.Point(88, 284)
        Me.ComboBoxPlaza.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxPlaza.Name = "ComboBoxPlaza"
        Me.ComboBoxPlaza.Size = New System.Drawing.Size(329, 26)
        Me.ComboBoxPlaza.TabIndex = 3
        Me.ComboBoxPlaza.ValueMember = "IdCompania"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(17, 288)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 18)
        Me.Label3.TabIndex = 213
        Me.Label3.Text = "Plaza:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(163, 229)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(353, 25)
        Me.Label4.TabIndex = 214
        Me.Label4.Text = "Relación Plaza-Cobertura/Paquete:"
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Location = New System.Drawing.Point(427, 271)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(105, 42)
        Me.btnAgregar.TabIndex = 4
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.ForeColor = System.Drawing.Color.Black
        Me.btnEliminar.Location = New System.Drawing.Point(588, 320)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(111, 41)
        Me.btnEliminar.TabIndex = 5
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'ComboBoxServicio
        '
        Me.ComboBoxServicio.DisplayMember = "Descripcion"
        Me.ComboBoxServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxServicio.FormattingEnabled = True
        Me.ComboBoxServicio.Location = New System.Drawing.Point(101, 558)
        Me.ComboBoxServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxServicio.Name = "ComboBoxServicio"
        Me.ComboBoxServicio.Size = New System.Drawing.Size(329, 33)
        Me.ComboBoxServicio.TabIndex = 217
        Me.ComboBoxServicio.TabStop = False
        Me.ComboBoxServicio.ValueMember = "Clv_Servicio"
        Me.ComboBoxServicio.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(3, 565)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(83, 18)
        Me.Label5.TabIndex = 218
        Me.Label5.Text = "Servicios:"
        Me.Label5.Visible = False
        '
        'tbBeam
        '
        Me.tbBeam.BackColor = System.Drawing.Color.White
        Me.tbBeam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbBeam.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbBeam.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbBeam.Location = New System.Drawing.Point(488, 180)
        Me.tbBeam.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbBeam.MaxLength = 5
        Me.tbBeam.Name = "tbBeam"
        Me.tbBeam.Size = New System.Drawing.Size(133, 24)
        Me.tbBeam.TabIndex = 220
        Me.tbBeam.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(45, 187)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 18)
        Me.Label6.TabIndex = 219
        Me.Label6.Text = "Cobertura:"
        '
        'CheckBoxEsEmpresarial
        '
        Me.CheckBoxEsEmpresarial.AutoSize = True
        Me.CheckBoxEsEmpresarial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CheckBoxEsEmpresarial.Location = New System.Drawing.Point(417, 59)
        Me.CheckBoxEsEmpresarial.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBoxEsEmpresarial.Name = "CheckBoxEsEmpresarial"
        Me.CheckBoxEsEmpresarial.Size = New System.Drawing.Size(143, 22)
        Me.CheckBoxEsEmpresarial.TabIndex = 221
        Me.CheckBoxEsEmpresarial.Text = "Es empresarial"
        Me.CheckBoxEsEmpresarial.UseVisualStyleBackColor = True
        Me.CheckBoxEsEmpresarial.Visible = False
        '
        'cbCobertura
        '
        Me.cbCobertura.DisplayMember = "Nombre"
        Me.cbCobertura.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCobertura.FormattingEnabled = True
        Me.cbCobertura.Location = New System.Drawing.Point(151, 175)
        Me.cbCobertura.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbCobertura.Name = "cbCobertura"
        Me.cbCobertura.Size = New System.Drawing.Size(329, 33)
        Me.cbCobertura.TabIndex = 2
        Me.cbCobertura.TabStop = False
        Me.cbCobertura.ValueMember = "IdCobertura"
        '
        'DataGridViewPlazas
        '
        Me.DataGridViewPlazas.AllowUserToAddRows = False
        Me.DataGridViewPlazas.AllowUserToDeleteRows = False
        Me.DataGridViewPlazas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewPlazas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.IdCompania, Me.Nombre, Me.Servicio})
        Me.DataGridViewPlazas.Location = New System.Drawing.Point(21, 320)
        Me.DataGridViewPlazas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridViewPlazas.Name = "DataGridViewPlazas"
        Me.DataGridViewPlazas.Size = New System.Drawing.Size(560, 209)
        Me.DataGridViewPlazas.TabIndex = 223
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.Visible = False
        '
        'IdCompania
        '
        Me.IdCompania.DataPropertyName = "IdCompania"
        Me.IdCompania.HeaderText = "IdCompania"
        Me.IdCompania.Name = "IdCompania"
        Me.IdCompania.Visible = False
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 180
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.Width = 180
        '
        'FrmPaquetesTelefonica
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(715, 618)
        Me.Controls.Add(Me.DataGridViewPlazas)
        Me.Controls.Add(Me.cbCobertura)
        Me.Controls.Add(Me.CheckBoxEsEmpresarial)
        Me.Controls.Add(Me.tbBeam)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ComboBoxServicio)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboBoxPlaza)
        Me.Controls.Add(Me.lbPlazas)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxProveedor)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.PaquetesBindingNavigator)
        Me.Controls.Add(Me.tbidBeam_Paquete)
        Me.Controls.Add(Me.ClaveLabel)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmPaquetesTelefonica"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cobertura/Paquete"
        CType(Me.PaquetesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PaquetesBindingNavigator.ResumeLayout(False)
        Me.PaquetesBindingNavigator.PerformLayout()
        CType(Me.DataGridViewPlazas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PaquetesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxProveedor As System.Windows.Forms.ComboBox
    Friend WithEvents ClaveLabel As System.Windows.Forms.Label
    Friend WithEvents tbidBeam_Paquete As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lbPlazas As System.Windows.Forms.ListBox
    Friend WithEvents ComboBoxPlaza As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents ComboBoxServicio As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbBeam As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxEsEmpresarial As System.Windows.Forms.CheckBox
    Friend WithEvents cbCobertura As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewPlazas As System.Windows.Forms.DataGridView
    Friend WithEvents id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewComboBoxColumn
End Class
