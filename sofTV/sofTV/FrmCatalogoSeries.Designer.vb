<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCatalogoSeries
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim SerieLabel As System.Windows.Forms.Label
        Dim Folios_ImpresosLabel As System.Windows.Forms.Label
        Dim UltimoFolio_UsadoLabel As System.Windows.Forms.Label
        Dim Clv_VendedorLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCatalogoSeries))
        Dim Label1 As System.Windows.Forms.Label
        Me.CONCatalogoSeriesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONCatalogoSeriesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New sofTV.NewsoftvDataSet1()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONCatalogoSeriesBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.SerieTextBox = New System.Windows.Forms.TextBox()
        Me.Folios_ImpresosTextBox = New System.Windows.Forms.TextBox()
        Me.UltimoFolio_UsadoTextBox = New System.Windows.Forms.TextBox()
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRAVENDORESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CONCatalogoSeriesTableAdapter = New sofTV.NewsoftvDataSet1TableAdapters.CONCatalogoSeriesTableAdapter()
        Me.MUESTRAVENDORESTableAdapter = New sofTV.NewsoftvDataSet1TableAdapters.MUESTRAVENDORESTableAdapter()
        Me.VALIDACatalogoSeriesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VALIDACatalogoSeriesTableAdapter = New sofTV.NewsoftvDataSet1TableAdapters.VALIDACatalogoSeriesTableAdapter()
        Me.rbCobro = New System.Windows.Forms.RadioButton()
        Me.rbVenta = New System.Windows.Forms.RadioButton()
        ClaveLabel = New System.Windows.Forms.Label()
        SerieLabel = New System.Windows.Forms.Label()
        Folios_ImpresosLabel = New System.Windows.Forms.Label()
        UltimoFolio_UsadoLabel = New System.Windows.Forms.Label()
        Clv_VendedorLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.CONCatalogoSeriesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCatalogoSeriesBindingNavigator.SuspendLayout()
        CType(Me.CONCatalogoSeriesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripContainer1.ContentPanel.SuspendLayout()
        Me.ToolStripContainer1.TopToolStripPanel.SuspendLayout()
        Me.ToolStripContainer1.SuspendLayout()
        CType(Me.MUESTRAVENDORESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VALIDACatalogoSeriesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ClaveLabel.Location = New System.Drawing.Point(155, 43)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(47, 13)
        ClaveLabel.TabIndex = 2
        ClaveLabel.Text = "Clave :"
        '
        'SerieLabel
        '
        SerieLabel.AutoSize = True
        SerieLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SerieLabel.ForeColor = System.Drawing.Color.LightSlateGray
        SerieLabel.Location = New System.Drawing.Point(158, 69)
        SerieLabel.Name = "SerieLabel"
        SerieLabel.Size = New System.Drawing.Size(44, 13)
        SerieLabel.TabIndex = 4
        SerieLabel.Text = "Serie :"
        '
        'Folios_ImpresosLabel
        '
        Folios_ImpresosLabel.AutoSize = True
        Folios_ImpresosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Folios_ImpresosLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Folios_ImpresosLabel.Location = New System.Drawing.Point(35, 95)
        Folios_ImpresosLabel.Name = "Folios_ImpresosLabel"
        Folios_ImpresosLabel.Size = New System.Drawing.Size(167, 13)
        Folios_ImpresosLabel.TabIndex = 6
        Folios_ImpresosLabel.Text = "Numero de Folios Impresos :"
        '
        'UltimoFolio_UsadoLabel
        '
        UltimoFolio_UsadoLabel.AutoSize = True
        UltimoFolio_UsadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        UltimoFolio_UsadoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        UltimoFolio_UsadoLabel.Location = New System.Drawing.Point(71, 121)
        UltimoFolio_UsadoLabel.Name = "UltimoFolio_UsadoLabel"
        UltimoFolio_UsadoLabel.Size = New System.Drawing.Size(129, 13)
        UltimoFolio_UsadoLabel.TabIndex = 8
        UltimoFolio_UsadoLabel.Text = "Ultimo Folio Impreso :"
        '
        'Clv_VendedorLabel
        '
        Clv_VendedorLabel.AutoSize = True
        Clv_VendedorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_VendedorLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_VendedorLabel.Location = New System.Drawing.Point(137, 150)
        Clv_VendedorLabel.Name = "Clv_VendedorLabel"
        Clv_VendedorLabel.Size = New System.Drawing.Size(69, 13)
        Clv_VendedorLabel.TabIndex = 10
        Clv_VendedorLabel.Text = "Vendedor :"
        '
        'CONCatalogoSeriesBindingNavigator
        '
        Me.CONCatalogoSeriesBindingNavigator.AddNewItem = Nothing
        Me.CONCatalogoSeriesBindingNavigator.BindingSource = Me.CONCatalogoSeriesBindingSource
        Me.CONCatalogoSeriesBindingNavigator.CountItem = Nothing
        Me.CONCatalogoSeriesBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONCatalogoSeriesBindingNavigator.Dock = System.Windows.Forms.DockStyle.None
        Me.CONCatalogoSeriesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CONCatalogoSeriesBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.CONCatalogoSeriesBindingNavigator.Location = New System.Drawing.Point(3, 0)
        Me.CONCatalogoSeriesBindingNavigator.MoveFirstItem = Nothing
        Me.CONCatalogoSeriesBindingNavigator.MoveLastItem = Nothing
        Me.CONCatalogoSeriesBindingNavigator.MoveNextItem = Nothing
        Me.CONCatalogoSeriesBindingNavigator.MovePreviousItem = Nothing
        Me.CONCatalogoSeriesBindingNavigator.Name = "CONCatalogoSeriesBindingNavigator"
        Me.CONCatalogoSeriesBindingNavigator.PositionItem = Nothing
        Me.CONCatalogoSeriesBindingNavigator.Size = New System.Drawing.Size(236, 25)
        Me.CONCatalogoSeriesBindingNavigator.TabIndex = 4
        Me.CONCatalogoSeriesBindingNavigator.TabStop = True
        Me.CONCatalogoSeriesBindingNavigator.Text = "BindingNavigator1"
        '
        'CONCatalogoSeriesBindingSource
        '
        Me.CONCatalogoSeriesBindingSource.DataMember = "CONCatalogoSeries"
        Me.CONCatalogoSeriesBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONCatalogoSeriesBindingNavigatorSaveItem
        '
        Me.CONCatalogoSeriesBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCatalogoSeriesBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCatalogoSeriesBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCatalogoSeriesBindingNavigatorSaveItem.Name = "CONCatalogoSeriesBindingNavigatorSaveItem"
        Me.CONCatalogoSeriesBindingNavigatorSaveItem.Size = New System.Drawing.Size(134, 22)
        Me.CONCatalogoSeriesBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoSeriesBindingSource, "Clave", True))
        Me.ClaveTextBox.Enabled = False
        Me.ClaveTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveTextBox.Location = New System.Drawing.Point(208, 41)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ClaveTextBox.TabIndex = 300
        Me.ClaveTextBox.TabStop = False
        '
        'SerieTextBox
        '
        Me.SerieTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoSeriesBindingSource, "Serie", True))
        Me.SerieTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieTextBox.Location = New System.Drawing.Point(208, 67)
        Me.SerieTextBox.Name = "SerieTextBox"
        Me.SerieTextBox.Size = New System.Drawing.Size(100, 20)
        Me.SerieTextBox.TabIndex = 0
        '
        'Folios_ImpresosTextBox
        '
        Me.Folios_ImpresosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Folios_ImpresosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoSeriesBindingSource, "Folios_Impresos", True))
        Me.Folios_ImpresosTextBox.Enabled = False
        Me.Folios_ImpresosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Folios_ImpresosTextBox.Location = New System.Drawing.Point(208, 93)
        Me.Folios_ImpresosTextBox.Name = "Folios_ImpresosTextBox"
        Me.Folios_ImpresosTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Folios_ImpresosTextBox.TabIndex = 1
        '
        'UltimoFolio_UsadoTextBox
        '
        Me.UltimoFolio_UsadoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.UltimoFolio_UsadoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCatalogoSeriesBindingSource, "UltimoFolio_Usado", True))
        Me.UltimoFolio_UsadoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltimoFolio_UsadoTextBox.Location = New System.Drawing.Point(208, 119)
        Me.UltimoFolio_UsadoTextBox.Name = "UltimoFolio_UsadoTextBox"
        Me.UltimoFolio_UsadoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.UltimoFolio_UsadoTextBox.TabIndex = 2
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Label1)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.rbVenta)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.rbCobro)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.ComboBox1)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.ClaveTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(ClaveLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Clv_VendedorLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(SerieLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.UltimoFolio_UsadoTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.SerieTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(UltimoFolio_UsadoLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Folios_ImpresosLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.Folios_ImpresosTextBox)
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(595, 216)
        Me.ToolStripContainer1.Location = New System.Drawing.Point(12, 12)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.Size = New System.Drawing.Size(595, 241)
        Me.ToolStripContainer1.TabIndex = 12
        Me.ToolStripContainer1.TabStop = False
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        '
        'ToolStripContainer1.TopToolStripPanel
        '
        Me.ToolStripContainer1.TopToolStripPanel.Controls.Add(Me.CONCatalogoSeriesBindingNavigator)
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONCatalogoSeriesBindingSource, "Clv_Vendedor", True))
        Me.ComboBox1.DataSource = Me.MUESTRAVENDORESBindingSource
        Me.ComboBox1.DisplayMember = "Nombre"
        Me.ComboBox1.Enabled = False
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(208, 147)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(333, 21)
        Me.ComboBox1.TabIndex = 3
        Me.ComboBox1.ValueMember = "Clv_Vendedor"
        '
        'MUESTRAVENDORESBindingSource
        '
        Me.MUESTRAVENDORESBindingSource.DataMember = "MUESTRAVENDORES"
        Me.MUESTRAVENDORESBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(471, 268)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONCatalogoSeriesTableAdapter
        '
        Me.CONCatalogoSeriesTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAVENDORESTableAdapter
        '
        Me.MUESTRAVENDORESTableAdapter.ClearBeforeFill = True
        '
        'VALIDACatalogoSeriesBindingSource
        '
        Me.VALIDACatalogoSeriesBindingSource.DataMember = "VALIDACatalogoSeries"
        Me.VALIDACatalogoSeriesBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'VALIDACatalogoSeriesTableAdapter
        '
        Me.VALIDACatalogoSeriesTableAdapter.ClearBeforeFill = True
        '
        'rbCobro
        '
        Me.rbCobro.AutoSize = True
        Me.rbCobro.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rbCobro.ForeColor = System.Drawing.Color.LightSlateGray
        Me.rbCobro.Location = New System.Drawing.Point(280, 174)
        Me.rbCobro.Name = "rbCobro"
        Me.rbCobro.Size = New System.Drawing.Size(58, 17)
        Me.rbCobro.TabIndex = 301
        Me.rbCobro.TabStop = True
        Me.rbCobro.Text = "Cobro"
        Me.rbCobro.UseVisualStyleBackColor = True
        '
        'rbVenta
        '
        Me.rbVenta.AutoSize = True
        Me.rbVenta.Checked = True
        Me.rbVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rbVenta.ForeColor = System.Drawing.Color.LightSlateGray
        Me.rbVenta.Location = New System.Drawing.Point(212, 174)
        Me.rbVenta.Name = "rbVenta"
        Me.rbVenta.Size = New System.Drawing.Size(58, 17)
        Me.rbVenta.TabIndex = 302
        Me.rbVenta.TabStop = True
        Me.rbVenta.Text = "Venta"
        Me.rbVenta.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(166, 178)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(40, 13)
        Label1.TabIndex = 303
        Label1.Text = "Tipo :"
        '
        'FrmCatalogoSeries
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(629, 323)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.Name = "FrmCatalogoSeries"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Series"
        CType(Me.CONCatalogoSeriesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCatalogoSeriesBindingNavigator.ResumeLayout(False)
        Me.CONCatalogoSeriesBindingNavigator.PerformLayout()
        CType(Me.CONCatalogoSeriesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripContainer1.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer1.ContentPanel.PerformLayout()
        Me.ToolStripContainer1.TopToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer1.TopToolStripPanel.PerformLayout()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        CType(Me.MUESTRAVENDORESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VALIDACatalogoSeriesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewsoftvDataSet1 As sofTV.NewsoftvDataSet1
    Friend WithEvents CONCatalogoSeriesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCatalogoSeriesTableAdapter As sofTV.NewsoftvDataSet1TableAdapters.CONCatalogoSeriesTableAdapter
    Friend WithEvents CONCatalogoSeriesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONCatalogoSeriesBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SerieTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Folios_ImpresosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UltimoFolio_UsadoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRAVENDORESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAVENDORESTableAdapter As sofTV.NewsoftvDataSet1TableAdapters.MUESTRAVENDORESTableAdapter
    Friend WithEvents VALIDACatalogoSeriesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VALIDACatalogoSeriesTableAdapter As sofTV.NewsoftvDataSet1TableAdapters.VALIDACatalogoSeriesTableAdapter
    Friend WithEvents rbVenta As System.Windows.Forms.RadioButton
    Friend WithEvents rbCobro As System.Windows.Forms.RadioButton
End Class
