
Imports System.Data.SqlClient
Public Class BrwCarterasPorPlaza
    Dim opcion As Integer = 0
    Public Sub BuscaCarterasBaseII(ByVal oclv_ciudad As String, ByVal oClv_TipSer As Integer, ByVal oClv_Cartera As Long, ByVal oFecha As Date, ByVal oHora As Date, ByVal oOp As Integer, ByVal oidcompania As String)
        Try
            ' @clv_ciudad varchar(100),@Clv_TipSer int,@Clv_Cartera bigint,@Fecha Datetime,@Hora Datetime,@Op int, @idcompania int=0

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@tipo", SqlDbType.Int, opcion)
            'BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.VarChar, oclv_ciudad, 100)
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, oClv_TipSer)
            BaseII.CreateMyParameter("@Clv_Cartera", SqlDbType.BigInt, oClv_Cartera)
            BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, oFecha)
            BaseII.CreateMyParameter("@Hora", SqlDbType.DateTime, oHora)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, oOp)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
            BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxDistribuidor.SelectedValue)
            If ComboBoxDistribuidor.SelectedValue = 9999 Then
                BuscaCarterasDataGridView.DataSource = BaseII.ConsultaDT("BuscaCarterasPorPlazaGlobales")
            Else
                BuscaCarterasDataGridView.DataSource = BaseII.ConsultaDT("BuscaCarterasPorPlaza")
            End If

            BuscaCarterasDataGridView.Columns.Item("CLV_CARTERA").Width = 120 'Columnas y nombres de encabezados
            BuscaCarterasDataGridView.Columns.Item("FECHA").Width = 120 'Sustituir BuscarCarterasBaseII por BuscarCarterasTableAdapter
            BuscaCarterasDataGridView.Columns.Item("HORA").Width = 120
            BuscaCarterasDataGridView.Columns.Item("TIPO").Width = 120
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bUSCA(ByVal OP As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If gloPorClv_Periodo = 0 Then
                If OP = 0 Then 'NUM. CARTERA
                    If IsNumeric(Me.TextBox1.Text) = True Then
                        'Me.BuscaCarterasBaseII.Connection = CON
                        BuscaCarterasBaseII(ComboBox1.SelectedValue, GloClv_TipSer, Me.TextBox1.Text, "01/01/1900", "01/01/1900", OP, ComboBox2.SelectedValue)
                    Else
                        MsgBox("Capture datos v�lidos para realizar la b�squeda ", MsgBoxStyle.Information)
                    End If
                ElseIf OP = 1 Then 'FECHA
                    If IsDate(Me.DateTimePicker1.Text) = True Then
                        'Me.BuscaCarterasBaseII.Connection = CON
                        BuscaCarterasBaseII(ComboBox1.SelectedValue, GloClv_TipSer, 0, Me.DateTimePicker1.Text, "01/01/1900", OP, ComboBox2.SelectedValue)
                    Else
                        MsgBox("Capture datos v�lidos para realizar la b�squeda ", MsgBoxStyle.Information)
                    End If
                ElseIf OP = 3 Then 'TODOS
                    'Me.BuscaCarterasBaseII.Connection = CON
                    BuscaCarterasBaseII(ComboBox1.SelectedValue, GloClv_TipSer, 0, "01/01/1900", "01/01/1900", OP, ComboBox2.SelectedValue)
                End If
            Else
                If OP = 0 Then 'NUM. CARTERA
                    If IsNumeric(Me.TextBox1.Text) = True Then
                        OP = gloPorClv_Periodo & OP
                        'Me.BuscaCarterasBaseII.Connection = CON
                        BuscaCarterasBaseII(ComboBox1.SelectedValue, GloClv_TipSer, Me.TextBox1.Text, "01/01/1900", "01/01/1900", OP, ComboBox2.SelectedValue)
                    Else
                        MsgBox("Capture datos v�lidos para realizar la b�squeda ", MsgBoxStyle.Information)
                    End If
                ElseIf OP = 1 Then 'FECHA
                    OP = gloPorClv_Periodo & OP
                    If IsDate(Me.DateTimePicker1.Text) = True Then
                        'Me.BuscaCarterasBaseII.Connection = CON
                        BuscaCarterasBaseII(ComboBox1.SelectedValue, GloClv_TipSer, 0, Me.DateTimePicker1.Text, "01/01/1900", OP, ComboBox2.SelectedValue)
                    Else
                        MsgBox("Capture datos v�lidos para realizar la b�squeda ", MsgBoxStyle.Information)
                    End If
                ElseIf OP = 3 Then 'TODOS
                    OP = gloPorClv_Periodo & OP
                    'Me.BuscaCarterasBaseII.Connection = CON
                    'Me.BuscaCarterasBaseII.Fill(Me.DataSetEDGAR.BuscaCarteras, glocdscartera, GloClv_TipSer, 0, "01/01/1900", "01/01/1900", OP, 1)
                    BuscaCarterasBaseII(ComboBox1.SelectedValue, GloClv_TipSer, 0, "01/01/1900", "01/01/1900", OP, ComboBox2.SelectedValue)
                End If
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BrwCarteras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'llenacombociudades()
        'llenacombocompanias()
        llenaDistribuidores()
        llenaPlazas()
        opcion = 1
        Me.bUSCA(3)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
    Private Sub llenacombociudades()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvusuario", SqlDbType.Int, GloClvUsuario)
            ComboBox1.DataSource = BaseII.ConsultaDT("ElegirCuidadesCartera")
            ComboBox1.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub llenacombocompanias()
        Try
            If ComboBox1.Items.Count > 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clave_cds", SqlDbType.VarChar, ComboBox1.SelectedValue)
                ComboBox2.DataSource = BaseII.ConsultaDT("ElegirCompaniasCartera")
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndGenCartera = False
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If BuscaCarterasDataGridView.Rows.Count = 0 Then
            Exit Sub
        End If
        GloBndGenCartera = False
        Clv_CarteraTextBox.Text = BuscaCarterasDataGridView.SelectedCells(0).Value.ToString
        If IsNumeric(Me.Clv_CarteraTextBox.Text) = True Then
            GLOClv_Cartera = Me.Clv_CarteraTextBox.Text
            GloBndGenCartera = True
            execar = True
            Me.Close()
        Else
            MsgBox("Seleccione la Cartera ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        bUSCA(0)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bUSCA(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            bUSCA(0)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub DateTimePicker1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DateTimePicker1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            bUSCA(1)
        End If
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        bUSCA(3)
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub Panel3_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel3.Paint

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        llenacombocompanias()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.bUSCA(3)
    End Sub

    Private Sub Panel4_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel4.Paint

    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles rbPlaza.CheckedChanged
        If rbPlaza.Checked Then
            lbPlaza.Visible = True
            ComboBoxPlaza.Visible = True
            opcion = 2
            Me.bUSCA(3)
        Else
            lbPlaza.Visible = False
            ComboBoxPlaza.Visible = False
            opcion = 1
            Me.bUSCA(3)
        End If
    End Sub

    Private Sub llenaDistribuidores()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_plaza", SqlDbType.Int, 0)
            ComboBoxDistribuidor.DataSource = BaseII.ConsultaDT("Muestra_PlazasCarteras")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaPlazas()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvplaza", SqlDbType.Int, ComboBoxDistribuidor.SelectedValue)
            ComboBoxPlaza.DataSource = BaseII.ConsultaDT("Muestra_Compania_PorPlaza")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxDistribuidor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxDistribuidor.SelectedIndexChanged

        If rbPlaza.Checked Then
            llenaPlazas()
        End If
        Me.bUSCA(3)
    End Sub

    Private Sub ComboBoxPlaza_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxPlaza.SelectedIndexChanged
        If rbPlaza.Checked Then
            Me.bUSCA(3)
        End If
    End Sub
End Class