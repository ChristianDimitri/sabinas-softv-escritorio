﻿Public Class FrmPuntosRecuperacion

    Private Sub FrmPuntosRecuperacion_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        colorea(Me, Me.Name)

        llenaConceptosPuntosRecuperacion()

        If opPtsRec = "N" Then
            idPtsRec = 0
            agregar.Enabled = False
            quitar.Enabled = False
        End If
        If (opPtsRec = "M" Or opPtsRec = "C") And idPtsRec > 0 Then

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.BigInt, idPtsRec)
            BaseII.CreateMyParameter("@Concepto", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.CreateMyParameter("@puntos", ParameterDirection.Output, SqlDbType.Money)
            BaseII.CreateMyParameter("@EsOrden", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("Usp_ConPuntosRecuperacion")
            ComboBox1.SelectedValue = BaseII.dicoPar("@Concepto")
            TextBox3.Text = BaseII.dicoPar("@puntos")
            CheckBox1.Checked = BaseII.dicoPar("@EsOrden")

            If opPtsRec = "C" Then
                TextBox3.Enabled = False
                ComboBox1.Enabled = False
                'Button1.Enabled = False
                Button5.Enabled = False
            End If

            If CheckBox1.Checked = False Then
                agregar.Enabled = False
                quitar.Enabled = False
            End If

        End If

        llenalistboxs()


    End Sub

    Private Sub llenaConceptosPuntosRecuperacion()
        Try
            BaseII.limpiaParametros()
            ComboBox1.DataSource = BaseII.ConsultaDT("Usp_MuestraConceptosptsRec")
            ComboBox1.DisplayMember = "Concepto"
            ComboBox1.ValueMember = "idConcepto"

            If ComboBox1.Items.Count > 0 Then
                ComboBox1.SelectedIndex = 0
            End If

        Catch ex As Exception

        End Try
    End Sub

   

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox3.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox3, Asc(e.KeyChar), "L"))
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click


        If TextBox3.Text.Length = 0 Then
            MsgBox("Capture los Puntos")
            Exit Sub
        End If

       
        Dim Msg As String = ""

        If opPtsRec = "N" And idPtsRec = 0 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Concepto", SqlDbType.VarChar, ComboBox1.SelectedValue, 500)
            BaseII.CreateMyParameter("@Puntos", SqlDbType.Decimal, TextBox3.Text)
            BaseII.CreateMyParameter("@EsOrden", SqlDbType.Bit, CheckBox1.Checked)
            BaseII.CreateMyParameter("@id", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 100)
            BaseII.ProcedimientoOutPut("Usp_NuePuntosRecuperacion")
            idPtsRec = BaseII.dicoPar("@id")
            Msg = BaseII.dicoPar("@Msg")
        ElseIf opPtsRec = "M" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.BigInt, idPtsRec)
            BaseII.CreateMyParameter("@Puntos", SqlDbType.Decimal, TextBox3.Text)
            BaseII.CreateMyParameter("@EsOrden", SqlDbType.Bit, CheckBox1.Checked)
            BaseII.ProcedimientoOutPut("Usp_ModPuntosRecuperacion")
            Msg = ""
        End If


        If Msg.Length > 0 Then
            MsgBox(Msg, MsgBoxStyle.Information)
            Me.Close()
        Else
            MsgBox("Guardado con exito", MsgBoxStyle.Information)
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub agregar_Click(sender As Object, e As EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@Idpuntos", SqlDbType.Int, idPtsRec)
        BaseII.CreateMyParameter("@clv_Trabajo", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.Inserta("Usp_RelPuntosRecuTrabajo")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click_1(sender As Object, e As EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@Idpuntos", SqlDbType.Int, idPtsRec)
        BaseII.CreateMyParameter("@clv_Trabajo", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.Inserta("Usp_RelPuntosRecuTrabajo")
        llenalistboxs()
    End Sub

    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@Idpuntos", SqlDbType.Int, idPtsRec)
        BaseII.CreateMyParameter("@clv_Trabajo", SqlDbType.Int, 0)
        loquehay.DataSource = BaseII.ConsultaDT("Usp_RelPuntosRecuTrabajo")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@Idpuntos", SqlDbType.Int, idPtsRec)
        BaseII.CreateMyParameter("@clv_Trabajo", SqlDbType.Int, 0)
        seleccion.DataSource = BaseII.ConsultaDT("Usp_RelPuntosRecuTrabajo")
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Dim Msg As String = Nothing
            If opPtsRec = "N" And idPtsRec = 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Concepto", SqlDbType.VarChar, ComboBox1.SelectedValue, 500)
                BaseII.CreateMyParameter("@Puntos", SqlDbType.Decimal, TextBox3.Text)
                BaseII.CreateMyParameter("@EsOrden", SqlDbType.Decimal, CheckBox1.Checked)
                BaseII.CreateMyParameter("@id", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 100)
                BaseII.ProcedimientoOutPut("Usp_NuePuntosRecuperacion")
                idPtsRec = BaseII.dicoPar("@id")
                Msg = BaseII.dicoPar("@Msg")
            End If
            loquehay.Enabled = True
            agregar.Enabled = True
            quitar.Enabled = True
            seleccion.Enabled = True
        Else
            loquehay.Enabled = False
            agregar.Enabled = False
            quitar.Enabled = False
            seleccion.Enabled = False
        End If
    End Sub

    Private Sub TextBox3_TextChanged(sender As Object, e As EventArgs) Handles TextBox3.TextChanged

    End Sub
End Class