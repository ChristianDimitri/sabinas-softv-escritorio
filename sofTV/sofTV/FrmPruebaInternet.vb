﻿Imports System.Data.SqlClient
Public Class FrmPruebaInternet

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedValue = 0

            End If
            GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub MUESTRACablemodesDelCliente(ByVal Contrato As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        dgvCablemodems.DataSource = BaseII.ConsultaDT("MUESTRACablemodesDelClientePrueba")
    End Sub

    Private Sub MuestraServiciosEric(ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Op As Integer, ByVal Clv_UnicaNet As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
        BaseII.CreateMyParameter("@Clv_Unicanet", SqlDbType.BigInt, Clv_UnicaNet)
        cbServicio.DataSource = BaseII.ConsultaDT("MuestraServiciosPrueba")
    End Sub

    Private Sub NUEtblPruebaInternet(ByVal Contrato As Integer, ByVal Clv_UnicaNet As Integer, ByVal Clv_Cablemodem As Integer, ByVal Clv_Servicio As Integer, ByVal Clv_ServicioPrueba As Integer, ByVal FechaInicio As DateTime, ByVal FechaFin As DateTime, ByVal Clv_Usuario As String)
        Try
            'NUEtblPruebaInternet(tbContrato.Text, dgvCablemodems.SelectedCells(0).Value, dgvCablemodems.SelectedCells(1).Value, dgvCablemodems.SelectedCells(3).Value, cbServicio.SelectedValue, dtpInicio.Value, dtpFin.Value, GloUsuario)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
            BaseII.CreateMyParameter("@Clv_UnicaNet", SqlDbType.Int, Clv_UnicaNet)
            BaseII.CreateMyParameter("@Clv_Cablemodem", SqlDbType.Int, Clv_Cablemodem)
            BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
            BaseII.CreateMyParameter("@Clv_ServicioPrueba", SqlDbType.Int, Clv_ServicioPrueba)
            BaseII.CreateMyParameter("@FechaInicio", SqlDbType.Date, FechaInicio)
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
            BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, Clv_Usuario, 5)
            BaseII.Inserta("NUEtblPruebaInternet")

            MessageBox.Show("Se realizó con éxito.")
            tbContrato.Text = ""
            tbContratoCompania.Text = ""
            MUESTRACablemodesDelCliente(0)
            'dgvCablemodems.DataSource = Nothing
            MuestraServiciosEric(2, 0, 7, 0)
            'cbServicio.DataSource = Nothing
            DAMEFECHADELSERVIDOR()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub DAMEFECHADELSERVIDOR()
        Dim Fecha As DateTime
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha", ParameterDirection.Output, SqlDbType.DateTime)
        BaseII.ProcedimientoOutPut("DAMEFECHADELSERVIDOR")
        Fecha = DateTime.Parse(BaseII.dicoPar("@Fecha").ToString)
        dtpInicio.Value = Fecha
        dtpFin.Value = Fecha
    End Sub

    Private Sub FrmPruebaInternet_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Llena_companias()
        'MuestraServiciosEric(2, 0, 7)
        DAMEFECHADELSERVIDOR()
    End Sub
    Dim loccon As Integer = 0
    Private Sub tbContrato_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbContrato.KeyDown, tbContratoCompania.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbContratoCompania.Text.Length = 0 Then Exit Sub
        'If IsNumeric(tbContratoCompania.Text) = False Then Exit Sub
        Try
            Dim array As String() = tbContratoCompania.Text.Trim.Split("-")
            loccon = array(0).Trim
            GloIdCompania = array(1).Trim
            Dim comando As New SqlCommand()
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            comando.Connection = conexion
            comando.CommandText = "select count(idcompania) from Rel_Usuario_Compania where clave=" + GloClvUsuario.ToString + " and idcompania=" + GloIdCompania.ToString
            Dim res As Integer = comando.ExecuteScalar()
            If res = 0 Then
                MsgBox("Contrato inválido. Inténtalo nuevamente")
                tbContrato.Text = ""
                tbContratoCompania.Text = ""
                MUESTRACablemodesDelCliente(0)
                'MuestraServiciosEric(2, 0, 7)
                Exit Sub
            End If
            conexion.Close()

        Catch ex As Exception
            MsgBox("Contrato inválido. Inténtalo nuevamente")
            Exit Sub
        End Try

        MUESTRACablemodesDelCliente(tbContrato.Text)
        If dgvCablemodems.Rows.Count = 0 Then
            MsgBox("El cliente no cuenta con servicio de internet activo.")
            Exit Sub
        End If
        MuestraServiciosEric(2, 0, 7, dgvCablemodems.SelectedCells(1).Value)
        If dgvCablemodems.Rows.Count > 0 Then
            tbContrato.Text = dgvCablemodems.SelectedCells(0).Value
        End If
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If dgvCablemodems.RowCount = 0 Then
            MessageBox.Show("Selecciona un cablemodem del Cliente.")
            Exit Sub
        End If
        If cbServicio.Text.Length = 0 Then
            MessageBox.Show("Selecciona un Servicio de prueba.")
            Exit Sub
        End If
        If cbServicio.SelectedValue = 0 Then
            MessageBox.Show("Selecciona un Servicio de prueba.")
            Exit Sub
        End If
        If dtpInicio.Value > dtpFin.Value Then
            MessageBox.Show("La fecha inico no se puede ser mayor a la fecha fin.")
            Exit Sub
        End If

        NUEtblPruebaInternet(tbContrato.Text, dgvCablemodems.SelectedCells(1).Value, dgvCablemodems.SelectedCells(2).Value, dgvCablemodems.SelectedCells(4).Value, cbServicio.SelectedValue, dtpInicio.Value, dtpFin.Value, GloUsuario)
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            'GloIdCompania = ComboBoxCompanias.SelectedValue
            'tbContrato.Text = ""
            'tbContratoCompania.Text = ""
            ''MuestraServiciosEric(2, 0, 7)
            'MUESTRACablemodesDelCliente(0)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        GloClv_TipSer = 9885 'cambiar
        FrmSelCliente.ShowDialog()
        If GLOCONTRATOSEL > 0 Then
            tbContratoCompania.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString

            'MuestraServiciosEric(2, 0, 7, tbContrato.Text)

            MUESTRACablemodesDelCliente(tbContrato.Text)
            MuestraServiciosEric(2, 0, 7, dgvCablemodems.SelectedCells(1).Value)
            If dgvCablemodems.Rows.Count > 0 Then
                tbContrato.Text = dgvCablemodems.SelectedCells(0).Value
            End If
        End If
    End Sub

    Private Sub tbContratoCompania_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbContratoCompania.KeyPress

    End Sub

    Private Sub tbContratoCompania_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbContratoCompania.TextChanged
        Try
            Dim array As String() = tbContratoCompania.Text.Trim.Split("-")
            loccon = array(0).Trim
            GloIdCompania = array(1).Trim
            Dim comando2 As New SqlCommand()
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            comando2.Connection = conexion
            comando2.CommandText = "select contrato from rel_contratos_companias where contratocompania=" + loccon.ToString + " and idcompania=" + GloIdCompania.ToString
            tbContrato.Text = comando2.ExecuteScalar().ToString
            conexion.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvCablemodems_SelectionChanged(sender As Object, e As EventArgs) Handles dgvCablemodems.SelectionChanged
        'If dgvCablemodems.Rows.Count = 0 Or tbContratoCompania.Text = "" Then
        '    Exit Sub
        'End If
        'If dgvCablemodems.SelectedCells(1).Value = Nothing Then
        '    Exit Sub
        'End If
        'MuestraServiciosEric(2, 0, 7, dgvCablemodems.SelectedCells(1).Value)
    End Sub
End Class