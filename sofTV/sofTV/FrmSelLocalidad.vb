﻿Imports System.Data.SqlClient
Public Class FrmSelLocalidad
    Public VieneDeIdentificador As Integer = 0
    Private Sub FrmSelLocalidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If VieneDeIdentificador = 1 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
            BaseII.Inserta("ProcedureSeleccionLocalidad")
        Else
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
            BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
            BaseII.Inserta("ProcedureSeleccionLocalidad")
        End If
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionLocalidad")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionLocalidad")
    End Sub
    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionLocalidad")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionLocalidad")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionLocalidad")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionLocalidad")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        VieneDeIdentificador = 0
        If varfrmselcompania.Equals("cartera") Then
            If BndDesPagAde = True Then
                BndDesPagAde = False
                FrmSelFechaDesPagAde.Show()
            Else
                'If IdSistema = "AG" Then
                '    FrmSelPeriodoCartera.Show()
                'Else
                'execar = True
                'End If
                FrmSelPeriodo.Show()
            End If
        End If

        If varfrmselcompania = "opcion1varios" Then
            FrmSelServRep.Show()
        End If
        If varfrmselcompania = "normal" Then
            FrmSelColoniaJ.Show()
        ElseIf varfrmselcompania = "hastacolonias" Then
            FrmSelColoniaJ.Show()
        ElseIf varfrmselcompania = "hastacompania1" Then
            FrmSelUsuariosE.Show()
        ElseIf varfrmselcompania = "hastacompania2" Then
            FrmSelTrabajosE.Show()
        ElseIf varfrmselcompania = "hastacompaniaselvendedor" Then
            FrmSelVendedor.Show()
        ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
            FrmSelUsuario.Show()
        ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
            FrmSelUsuariosVentas.Show()
        ElseIf varfrmselcompania = "hastacompaniasreportefolios" Then
            FormReporteFolios.Show()
        ElseIf varfrmselcompania = "hastacompaniasresventas" Then
            FrmSelFechasPPE.Show()
        ElseIf varfrmselcompania = "hastacompaniaselsucursal" Then
            FrmSelSucursal.Show()
        ElseIf varfrmselcompania = "hastacompaniaimprimircomision" Then
            FrmImprimirComision.Show()
        ElseIf varfrmselcompania = "hastacompaniacumpleanos" Then
            FrmRepCumpleanosDeLosClientes.Show()
        ElseIf varfrmselcompania = "hastacompaniapruebaint" Then
            'FrmRepPruebaInternet.Show()
            eOpVentas = 95
            FrmImprimirComision.Show()
        ElseIf varfrmselcompania = "normalrecordatorios" Then
            FrmSelColoniaJ.Show()
        ElseIf varfrmselcompania = "hastacompaniamensualidades" Then
            FrmImprimirComision.Show()
        ElseIf varfrmselcompania = "hastacompaniareprecontratacion" Then
            'SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
            'SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
            'SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
            'SoftvMod.VariablesGlobales.MiConexion = MiConexion
            'SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
            'SoftvMod.VariablesGlobales.IdCompania = identificador
            'Dim frm As New SoftvMod.FrmRepRecontratacion
            'frm.ShowDialog()
            FrmRepRecontratacion.Show()
        ElseIf varfrmselcompania = "movimientoscartera" Then
            FrmSelCompaniaCartera.Show()
        ElseIf varfrmselcompania = "pendientesderealizar" Then
            FrmImprimirComision.Show()
        ElseIf varfrmselcompania = "resumenordenes" Then
            FrmFiltroReporteResumen.Show()
        ElseIf varfrmselcompania = "graficassucursal" Then
            FrmSelSucursal.Show()

        ElseIf varfrmselcompania = "graficasventas" Then
            FrmSelVendedor.Show()
        ElseIf varfrmselcompania = "graficasdosopciones" Then
            FrmSelSucursal.Show()
        ElseIf varfrmselcompania = "sindocumentos" Then
            FrmImprimirPPE.Show()
        ElseIf varfrmselcompania = "OrdenesConcepto" Then
            FrmImprimirPPE.Show()
        ElseIf varfrmselcompania = "OrdenesConceptoDetallado" Then
            FrmSeleccionaTrabajo.Show()
        ElseIf varfrmselcompania = "QuejasConcepto" Then
            FrmImprimirPPE.Show()
        ElseIf varfrmselcompania = "QuejasConceptoDetallado" Then
            FrmSeleccionaProblema.Show()
        End If
        Me.Close()
    End Sub
End Class