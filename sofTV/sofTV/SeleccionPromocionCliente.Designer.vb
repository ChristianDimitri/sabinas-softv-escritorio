<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SeleccionPromocionCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBBtnSalir = New System.Windows.Forms.Button()
        Me.CMBBtnAceptar = New System.Windows.Forms.Button()
        Me.DGVPromociones = New System.Windows.Forms.DataGridView()
        Me.clv_promocion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promocion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoMesesSinInte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Inicial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RestoMeses = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Plazo_Forzoso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CMBLblPromocion = New System.Windows.Forms.Label()
        CType(Me.DGVPromociones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBBtnSalir
        '
        Me.CMBBtnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnSalir.Location = New System.Drawing.Point(872, 414)
        Me.CMBBtnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnSalir.Name = "CMBBtnSalir"
        Me.CMBBtnSalir.Size = New System.Drawing.Size(171, 41)
        Me.CMBBtnSalir.TabIndex = 5
        Me.CMBBtnSalir.Text = "&SALIR"
        Me.CMBBtnSalir.UseVisualStyleBackColor = True
        '
        'CMBBtnAceptar
        '
        Me.CMBBtnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnAceptar.Location = New System.Drawing.Point(647, 414)
        Me.CMBBtnAceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnAceptar.Name = "CMBBtnAceptar"
        Me.CMBBtnAceptar.Size = New System.Drawing.Size(184, 41)
        Me.CMBBtnAceptar.TabIndex = 4
        Me.CMBBtnAceptar.Text = "&GUARDAR"
        Me.CMBBtnAceptar.UseVisualStyleBackColor = True
        '
        'DGVPromociones
        '
        Me.DGVPromociones.AllowUserToAddRows = False
        Me.DGVPromociones.AllowUserToDeleteRows = False
        Me.DGVPromociones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVPromociones.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVPromociones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVPromociones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clv_promocion, Me.Promocion, Me.NoMesesSinInte, Me.Inicial, Me.RestoMeses, Me.Plazo_Forzoso})
        Me.DGVPromociones.Location = New System.Drawing.Point(28, 59)
        Me.DGVPromociones.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DGVPromociones.Name = "DGVPromociones"
        Me.DGVPromociones.ReadOnly = True
        Me.DGVPromociones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVPromociones.Size = New System.Drawing.Size(1001, 267)
        Me.DGVPromociones.TabIndex = 6
        '
        'clv_promocion
        '
        Me.clv_promocion.DataPropertyName = "clv_promocion"
        Me.clv_promocion.HeaderText = "clv_promocion"
        Me.clv_promocion.Name = "clv_promocion"
        Me.clv_promocion.ReadOnly = True
        Me.clv_promocion.Visible = False
        Me.clv_promocion.Width = 134
        '
        'Promocion
        '
        Me.Promocion.DataPropertyName = "Promocion"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Promocion.DefaultCellStyle = DataGridViewCellStyle2
        Me.Promocion.HeaderText = "Promoción"
        Me.Promocion.Name = "Promocion"
        Me.Promocion.ReadOnly = True
        Me.Promocion.Width = 127
        '
        'NoMesesSinInte
        '
        Me.NoMesesSinInte.DataPropertyName = "NoMesesSinInte"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoMesesSinInte.DefaultCellStyle = DataGridViewCellStyle3
        Me.NoMesesSinInte.HeaderText = "# Meses Sin Intereses"
        Me.NoMesesSinInte.Name = "NoMesesSinInte"
        Me.NoMesesSinInte.ReadOnly = True
        Me.NoMesesSinInte.Width = 206
        '
        'Inicial
        '
        Me.Inicial.DataPropertyName = "Inicial"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.Inicial.DefaultCellStyle = DataGridViewCellStyle4
        Me.Inicial.HeaderText = "Porcentaje Inicial"
        Me.Inicial.Name = "Inicial"
        Me.Inicial.ReadOnly = True
        Me.Inicial.Width = 168
        '
        'RestoMeses
        '
        Me.RestoMeses.DataPropertyName = "RestoMeses"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RestoMeses.DefaultCellStyle = DataGridViewCellStyle5
        Me.RestoMeses.HeaderText = "Resto A Pagar En Meses"
        Me.RestoMeses.Name = "RestoMeses"
        Me.RestoMeses.ReadOnly = True
        Me.RestoMeses.Width = 177
        '
        'Plazo_Forzoso
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Plazo_Forzoso.DefaultCellStyle = DataGridViewCellStyle6
        Me.Plazo_Forzoso.HeaderText = "Meses Forzosos"
        Me.Plazo_Forzoso.Name = "Plazo_Forzoso"
        Me.Plazo_Forzoso.ReadOnly = True
        Me.Plazo_Forzoso.Width = 162
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CMBLblPromocion)
        Me.Panel1.Controls.Add(Me.DGVPromociones)
        Me.Panel1.Location = New System.Drawing.Point(13, 39)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1047, 345)
        Me.Panel1.TabIndex = 7
        '
        'CMBLblPromocion
        '
        Me.CMBLblPromocion.AutoSize = True
        Me.CMBLblPromocion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblPromocion.Location = New System.Drawing.Point(23, 17)
        Me.CMBLblPromocion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLblPromocion.Name = "CMBLblPromocion"
        Me.CMBLblPromocion.Size = New System.Drawing.Size(272, 25)
        Me.CMBLblPromocion.TabIndex = 7
        Me.CMBLblPromocion.Text = "Seleccione Una Promoción"
        '
        'SeleccionPromocionCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1069, 463)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CMBBtnSalir)
        Me.Controls.Add(Me.CMBBtnAceptar)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SeleccionPromocionCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promociones Clientes"
        Me.TopMost = True
        CType(Me.DGVPromociones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CMBBtnSalir As System.Windows.Forms.Button
    Friend WithEvents CMBBtnAceptar As System.Windows.Forms.Button
    Friend WithEvents DGVPromociones As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBLblPromocion As System.Windows.Forms.Label
    Friend WithEvents clv_promocion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promocion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoMesesSinInte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Inicial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RestoMeses As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Plazo_Forzoso As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
