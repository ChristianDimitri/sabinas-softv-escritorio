﻿Public Class FrmMUESTRAClientesCambioDeTipoDeServicio
    Private Sub MUESTRAClientesCambioDeTipoDeServicio(ByVal Op As Integer, ByVal Clv_TipSer As Integer, ByVal Contrato As Integer, ByVal Nombre As String)
        'op=0 Todos
        'op=1 Contrato
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Nombre, 250)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
        dgvClientes.DataSource = BaseII.ConsultaDT("MUESTRAClientesCambioDeTipoDeServicio")

    End Sub

    Private Sub FrmMUESTRAClientesCambioDeTipoDeServicio_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        eContrato = 0
        eContratoCompania = 0
        MUESTRAClientesCambioDeTipoDeServicio(0, eClv_TipSer, 0, "")
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)

    End Sub

    Private Sub tbContrato_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbContrato.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbContrato.Text.Length = 0 Then Exit Sub
        Try
            Dim array As String()
            array = tbContrato.Text.Trim.Split("-")
            GloIdCompania = array(1).Trim
            MUESTRAClientesCambioDeTipoDeServicio(1, eClv_TipSer, array(0), "")
        Catch ex As Exception
            MsgBox("Contrato inválido. Inténtalo nuevamente.")
        End Try
    End Sub

    Private Sub bnBuscarContrato_Click(sender As System.Object, e As System.EventArgs) Handles bnBuscarContrato.Click
        If tbContrato.Text.Length = 0 Then Exit Sub
        Try
            Dim array As String()
            array = tbContrato.Text.Trim.Split("-")
            GloIdCompania = array(1).Trim
            MUESTRAClientesCambioDeTipoDeServicio(1, eClv_TipSer, array(0), "")
        Catch ex As Exception
            MsgBox("Contrato inválido. Inténtalo nuevamente.")
        End Try

    End Sub

    Private Sub tbNombre_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbNombre.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbNombre.Text.Length = 0 Then Exit Sub
        MUESTRAClientesCambioDeTipoDeServicio(2, eClv_TipSer, 0, tbNombre.Text)
    End Sub

    Private Sub bnBuscarNombre_Click(sender As System.Object, e As System.EventArgs) Handles bnBuscarNombre.Click
        If tbNombre.Text.Length = 0 Then Exit Sub
        MUESTRAClientesCambioDeTipoDeServicio(2, eClv_TipSer, 0, tbNombre.Text)
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If dgvClientes.RowCount = 0 Then Exit Sub
        Dim array As String() = dgvClientes.SelectedCells(0).Value.ToString.Split("-")
        GloIdCompania = array(1)
        eContratoCompania = array(0)
        eContrato = dgvClientes.SelectedCells(3).Value
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        eContrato = 0
        eContratoCompania = 0
        Me.Close()
    End Sub

    Private Sub dgvClientes_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvClientes.CellContentClick
        If dgvClientes.RowCount = 0 Then Exit Sub
        Dim array As String() = dgvClientes.SelectedCells(0).Value.ToString.Split("-")
        eContratoCompania = array(0)
        GloIdCompania = array(1)
        eContrato = dgvClientes.SelectedCells(3).Value
    End Sub
End Class