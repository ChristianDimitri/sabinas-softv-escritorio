<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPromocionesTel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CLAVELabel As System.Windows.Forms.Label
        Dim DESCRIPCIONLabel As System.Windows.Forms.Label
        Dim Clv_TipSerLabel As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPromocionesTel))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CONTipoPromocionBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONTipoPromocionBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CLAVETextBox = New System.Windows.Forms.TextBox()
        Me.CONTipoPromocionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet2 = New sofTV.NewSofTvDataSet()
        Me.DESCRIPCIONTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.CONTipoPromocionBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MuestracoloniassegunCiudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.CMBButton4 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraciudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.ClvpromocionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvciudadDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvcoloniaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColoniaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CiudadDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MuestraRelPromocionColoniasCiudadtmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CMBButton3 = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Clv_Promocion3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoMeses = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoMesesForzoso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.CMBButton2 = New System.Windows.Forms.Button()
        Me.CMBButton1 = New System.Windows.Forms.Button()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown()
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Plazo_Forzoso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Muestra_Rel_Promocion_NoPagos_tmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBBtnPagosEliminar = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.DateTimePicker3 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker4 = New System.Windows.Forms.DateTimePicker()
        Me.CMBBtnPagosAgregar = New System.Windows.Forms.Button()
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.MuestraRelPromocionMesesSinInteTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Inserta_Rel_Promocion_Meses_AplicaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Promocion_Meses_AplicaTableAdapter()
        Me.Inserta_Rel_Promocion_Meses_AplicaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Rel_Promocion_Meses_AplicaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Rel_Promocion_Meses_AplicaTableAdapter()
        Me.Muestra_ciudadTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_ciudadTableAdapter()
        Me.Muestra_colonias_segun_CiudadTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_colonias_segun_CiudadTableAdapter()
        Me.Muestra_Rel_Promocion_Colonias_Ciudad_tmpTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Rel_Promocion_Colonias_Ciudad_tmpTableAdapter()
        Me.Muestra_Rel_Promocion_MesesSinInte_TmpTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Rel_Promocion_MesesSinInte_TmpTableAdapter()
        Me.Muestra_Rel_Promocion_NoPagos_tmpTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Rel_Promocion_NoPagos_tmpTableAdapter()
        Me.CMBTabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.DGVServInt = New System.Windows.Forms.DataGridView()
        Me.clv_promocion2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clv_servicio2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CMBBtnDelInt = New System.Windows.Forms.Button()
        Me.CMBBtnAddInt = New System.Windows.Forms.Button()
        Me.LblServInt = New System.Windows.Forms.Label()
        Me.CmbBoxServInt = New System.Windows.Forms.ComboBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.DGVServTel = New System.Windows.Forms.DataGridView()
        Me.clv_promocion1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clv_servicio1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LblServTel = New System.Windows.Forms.Label()
        Me.CMBBtnDelTel = New System.Windows.Forms.Button()
        Me.CMBBtnAddTel = New System.Windows.Forms.Button()
        Me.CmbBoxServTel = New System.Windows.Forms.ComboBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.DGVCombo = New System.Windows.Forms.DataGridView()
        Me.clv_promocion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clv_servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LblCombos = New System.Windows.Forms.Label()
        Me.CMBBtnDelCombo = New System.Windows.Forms.Button()
        Me.CMBBtnAddCombo = New System.Windows.Forms.Button()
        Me.CmbBoxCombos = New System.Windows.Forms.ComboBox()
        Me.MuestraServiciosPromocionesBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraServiciosPromocionesBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONTipoPromocionTableAdapter2 = New sofTV.NewSofTvDataSetTableAdapters.CONTipoPromocionTableAdapter()
        CLAVELabel = New System.Windows.Forms.Label()
        DESCRIPCIONLabel = New System.Windows.Forms.Label()
        Clv_TipSerLabel = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        CType(Me.CONTipoPromocionBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONTipoPromocionBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.CONTipoPromocionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONTipoPromocionBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.MuestracoloniassegunCiudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraciudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraRelPromocionColoniasCiudadtmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Rel_Promocion_NoPagos_tmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraRelPromocionMesesSinInteTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_Promocion_Meses_AplicaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBTabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.DGVServInt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel6.SuspendLayout()
        CType(Me.DGVServTel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.DGVCombo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServiciosPromocionesBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServiciosPromocionesBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CLAVELabel
        '
        CLAVELabel.AutoSize = True
        CLAVELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CLAVELabel.Location = New System.Drawing.Point(167, 53)
        CLAVELabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CLAVELabel.Name = "CLAVELabel"
        CLAVELabel.Size = New System.Drawing.Size(60, 18)
        CLAVELabel.TabIndex = 0
        CLAVELabel.Text = "Clave :"
        '
        'DESCRIPCIONLabel
        '
        DESCRIPCIONLabel.AutoSize = True
        DESCRIPCIONLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DESCRIPCIONLabel.Location = New System.Drawing.Point(113, 86)
        DESCRIPCIONLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DESCRIPCIONLabel.Name = "DESCRIPCIONLabel"
        DESCRIPCIONLabel.Size = New System.Drawing.Size(108, 18)
        DESCRIPCIONLabel.TabIndex = 2
        DESCRIPCIONLabel.Text = "Descripción :"
        '
        'Clv_TipSerLabel
        '
        Clv_TipSerLabel.AutoSize = True
        Clv_TipSerLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        Clv_TipSerLabel.Location = New System.Drawing.Point(572, 305)
        Clv_TipSerLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_TipSerLabel.Name = "Clv_TipSerLabel"
        Clv_TipSerLabel.Size = New System.Drawing.Size(81, 17)
        Clv_TipSerLabel.TabIndex = 20
        Clv_TipSerLabel.Text = "Clv Tip Ser:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.Location = New System.Drawing.Point(299, 20)
        Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(100, 18)
        Label4.TabIndex = 9
        Label4.Text = "Fecha Final:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.Location = New System.Drawing.Point(45, 20)
        Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(107, 18)
        Label5.TabIndex = 7
        Label5.Text = "Fecha Inicial:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.Location = New System.Drawing.Point(37, 28)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(121, 18)
        Label1.TabIndex = 22
        Label1.Text = "No. De Meses:"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.Location = New System.Drawing.Point(299, 20)
        Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(100, 18)
        Label6.TabIndex = 9
        Label6.Text = "Fecha Final:"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.Location = New System.Drawing.Point(45, 20)
        Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(107, 18)
        Label7.TabIndex = 7
        Label7.Text = "Fecha Inicial:"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.Location = New System.Drawing.Point(7, 41)
        Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(196, 18)
        Label3.TabIndex = 26
        Label3.Text = "Resto En No. De Meses:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.Location = New System.Drawing.Point(16, 11)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(173, 18)
        Label2.TabIndex = 26
        Label2.Text = "Porcentaje Inicial (%):"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.Location = New System.Drawing.Point(19, 74)
        Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(141, 18)
        Label10.TabIndex = 26
        Label10.Text = "Meses Forzosos:"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.Location = New System.Drawing.Point(69, 79)
        Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(141, 18)
        Label11.TabIndex = 29
        Label11.Text = "Meses Forzosos:"
        '
        'CONTipoPromocionBindingNavigator
        '
        Me.CONTipoPromocionBindingNavigator.AddNewItem = Nothing
        Me.CONTipoPromocionBindingNavigator.CountItem = Nothing
        Me.CONTipoPromocionBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONTipoPromocionBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONTipoPromocionBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONTipoPromocionBindingNavigatorSaveItem})
        Me.CONTipoPromocionBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONTipoPromocionBindingNavigator.MoveFirstItem = Nothing
        Me.CONTipoPromocionBindingNavigator.MoveLastItem = Nothing
        Me.CONTipoPromocionBindingNavigator.MoveNextItem = Nothing
        Me.CONTipoPromocionBindingNavigator.MovePreviousItem = Nothing
        Me.CONTipoPromocionBindingNavigator.Name = "CONTipoPromocionBindingNavigator"
        Me.CONTipoPromocionBindingNavigator.PositionItem = Nothing
        Me.CONTipoPromocionBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONTipoPromocionBindingNavigator.Size = New System.Drawing.Size(1332, 28)
        Me.CONTipoPromocionBindingNavigator.TabIndex = 1
        Me.CONTipoPromocionBindingNavigator.TabStop = True
        Me.CONTipoPromocionBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(122, 25)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(105, 25)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONTipoPromocionBindingNavigatorSaveItem
        '
        Me.CONTipoPromocionBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTipoPromocionBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONTipoPromocionBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONTipoPromocionBindingNavigatorSaveItem.Name = "CONTipoPromocionBindingNavigatorSaveItem"
        Me.CONTipoPromocionBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 25)
        Me.CONTipoPromocionBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.CONTipoPromocionBindingNavigator)
        Me.Panel1.Controls.Add(CLAVELabel)
        Me.Panel1.Controls.Add(Me.CLAVETextBox)
        Me.Panel1.Controls.Add(DESCRIPCIONLabel)
        Me.Panel1.Controls.Add(Me.DESCRIPCIONTextBox)
        Me.Panel1.Controls.Add(Clv_TipSerLabel)
        Me.Panel1.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1332, 122)
        Me.Panel1.TabIndex = 7
        '
        'CLAVETextBox
        '
        Me.CLAVETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CLAVETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTipoPromocionBindingSource, "CLAVE", True))
        Me.CLAVETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CLAVETextBox.Location = New System.Drawing.Point(241, 53)
        Me.CLAVETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CLAVETextBox.Name = "CLAVETextBox"
        Me.CLAVETextBox.ReadOnly = True
        Me.CLAVETextBox.Size = New System.Drawing.Size(133, 24)
        Me.CLAVETextBox.TabIndex = 1
        Me.CLAVETextBox.TabStop = False
        '
        'CONTipoPromocionBindingSource
        '
        Me.CONTipoPromocionBindingSource.DataMember = "CONTipoPromocion"
        Me.CONTipoPromocionBindingSource.DataSource = Me.NewSofTvDataSet2
        '
        'NewSofTvDataSet2
        '
        Me.NewSofTvDataSet2.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DESCRIPCIONTextBox
        '
        Me.DESCRIPCIONTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DESCRIPCIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTipoPromocionBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCRIPCIONTextBox.Location = New System.Drawing.Point(239, 86)
        Me.DESCRIPCIONTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DESCRIPCIONTextBox.Name = "DESCRIPCIONTextBox"
        Me.DESCRIPCIONTextBox.Size = New System.Drawing.Size(557, 24)
        Me.DESCRIPCIONTextBox.TabIndex = 0
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipSerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipSerTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(663, 305)
        Me.Clv_TipSerTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(133, 15)
        Me.Clv_TipSerTextBox.TabIndex = 21
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'CONTipoPromocionBindingSource1
        '
        Me.CONTipoPromocionBindingSource1.DataMember = "CONTipoPromocion"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(1180, 869)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(159, 31)
        Me.Button5.TabIndex = 8
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.Panel4)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.CheckBox1)
        Me.Panel2.Controls.Add(Me.Panel5)
        Me.Panel2.Controls.Add(Me.CheckBox2)
        Me.Panel2.Enabled = False
        Me.Panel2.Location = New System.Drawing.Point(0, 4)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1313, 697)
        Me.Panel2.TabIndex = 9
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel4.Controls.Add(Me.Label9)
        Me.Panel4.Controls.Add(Me.ComboBox2)
        Me.Panel4.Controls.Add(Me.CMBButton4)
        Me.Panel4.Controls.Add(Me.ComboBox1)
        Me.Panel4.Controls.Add(Me.DataGridView2)
        Me.Panel4.Controls.Add(Me.Label8)
        Me.Panel4.Controls.Add(Me.CMBButton3)
        Me.Panel4.Location = New System.Drawing.Point(12, 31)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1311, 181)
        Me.Panel4.TabIndex = 33
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(31, 58)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(182, 18)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "Colonia En Que Aplica:"
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.MuestracoloniassegunCiudadBindingSource
        Me.ComboBox2.DisplayMember = "Nombre"
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(244, 57)
        Me.ComboBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(312, 28)
        Me.ComboBox2.TabIndex = 24
        Me.ComboBox2.ValueMember = "clv_colonia"
        '
        'MuestracoloniassegunCiudadBindingSource
        '
        Me.MuestracoloniassegunCiudadBindingSource.DataMember = "Muestra_colonias_segun_Ciudad"
        Me.MuestracoloniassegunCiudadBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBButton4
        '
        Me.CMBButton4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBButton4.Location = New System.Drawing.Point(460, 103)
        Me.CMBButton4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBButton4.Name = "CMBButton4"
        Me.CMBButton4.Size = New System.Drawing.Size(97, 43)
        Me.CMBButton4.TabIndex = 27
        Me.CMBButton4.Text = "&Eliminar"
        Me.CMBButton4.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MuestraciudadBindingSource
        Me.ComboBox1.DisplayMember = "Nombre"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(244, 23)
        Me.ComboBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(556, 28)
        Me.ComboBox1.TabIndex = 23
        Me.ComboBox1.ValueMember = "clv_ciudad"
        '
        'MuestraciudadBindingSource
        '
        Me.MuestraciudadBindingSource.DataMember = "Muestra_ciudad"
        Me.MuestraciudadBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'DataGridView2
        '
        Me.DataGridView2.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvpromocionDataGridViewTextBoxColumn, Me.ClvciudadDataGridViewTextBoxColumn, Me.ClvcoloniaDataGridViewTextBoxColumn, Me.ColoniaDataGridViewTextBoxColumn, Me.CiudadDataGridViewTextBoxColumn})
        Me.DataGridView2.DataSource = Me.MuestraRelPromocionColoniasCiudadtmpBindingSource
        Me.DataGridView2.Location = New System.Drawing.Point(565, 58)
        Me.DataGridView2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView2.MultiSelect = False
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView2.Size = New System.Drawing.Size(668, 111)
        Me.DataGridView2.TabIndex = 26
        '
        'ClvpromocionDataGridViewTextBoxColumn
        '
        Me.ClvpromocionDataGridViewTextBoxColumn.DataPropertyName = "clv_promocion"
        Me.ClvpromocionDataGridViewTextBoxColumn.HeaderText = "clv_promocion"
        Me.ClvpromocionDataGridViewTextBoxColumn.Name = "ClvpromocionDataGridViewTextBoxColumn"
        Me.ClvpromocionDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvpromocionDataGridViewTextBoxColumn.Visible = False
        '
        'ClvciudadDataGridViewTextBoxColumn
        '
        Me.ClvciudadDataGridViewTextBoxColumn.DataPropertyName = "clv_ciudad"
        Me.ClvciudadDataGridViewTextBoxColumn.HeaderText = "clv_ciudad"
        Me.ClvciudadDataGridViewTextBoxColumn.Name = "ClvciudadDataGridViewTextBoxColumn"
        Me.ClvciudadDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvciudadDataGridViewTextBoxColumn.Visible = False
        '
        'ClvcoloniaDataGridViewTextBoxColumn
        '
        Me.ClvcoloniaDataGridViewTextBoxColumn.DataPropertyName = "clv_colonia"
        Me.ClvcoloniaDataGridViewTextBoxColumn.HeaderText = "clv_colonia"
        Me.ClvcoloniaDataGridViewTextBoxColumn.Name = "ClvcoloniaDataGridViewTextBoxColumn"
        Me.ClvcoloniaDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvcoloniaDataGridViewTextBoxColumn.Visible = False
        '
        'ColoniaDataGridViewTextBoxColumn
        '
        Me.ColoniaDataGridViewTextBoxColumn.DataPropertyName = "Colonia"
        Me.ColoniaDataGridViewTextBoxColumn.HeaderText = "Colonia"
        Me.ColoniaDataGridViewTextBoxColumn.Name = "ColoniaDataGridViewTextBoxColumn"
        Me.ColoniaDataGridViewTextBoxColumn.ReadOnly = True
        Me.ColoniaDataGridViewTextBoxColumn.Width = 200
        '
        'CiudadDataGridViewTextBoxColumn
        '
        Me.CiudadDataGridViewTextBoxColumn.DataPropertyName = "Ciudad"
        Me.CiudadDataGridViewTextBoxColumn.HeaderText = "Ciudad"
        Me.CiudadDataGridViewTextBoxColumn.Name = "CiudadDataGridViewTextBoxColumn"
        Me.CiudadDataGridViewTextBoxColumn.ReadOnly = True
        Me.CiudadDataGridViewTextBoxColumn.Width = 200
        '
        'MuestraRelPromocionColoniasCiudadtmpBindingSource
        '
        Me.MuestraRelPromocionColoniasCiudadtmpBindingSource.DataMember = "Muestra_Rel_Promocion_Colonias_Ciudad_tmp"
        Me.MuestraRelPromocionColoniasCiudadtmpBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(9, 25)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(199, 18)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Ciudad En La Que Aplica:"
        '
        'CMBButton3
        '
        Me.CMBButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBButton3.Location = New System.Drawing.Point(355, 105)
        Me.CMBButton3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBButton3.Name = "CMBButton3"
        Me.CMBButton3.Size = New System.Drawing.Size(97, 43)
        Me.CMBButton3.TabIndex = 26
        Me.CMBButton3.Text = "&Agregar"
        Me.CMBButton3.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.DataGridView1)
        Me.Panel3.Controls.Add(Me.NumericUpDown3)
        Me.Panel3.Controls.Add(Label10)
        Me.Panel3.Controls.Add(Me.GroupBox2)
        Me.Panel3.Controls.Add(Me.CMBButton2)
        Me.Panel3.Controls.Add(Me.CMBButton1)
        Me.Panel3.Controls.Add(Me.NumericUpDown1)
        Me.Panel3.Controls.Add(Label1)
        Me.Panel3.Enabled = False
        Me.Panel3.Location = New System.Drawing.Point(9, 246)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1313, 213)
        Me.Panel3.TabIndex = 29
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Promocion3, Me.NoMeses, Me.Fecha1, Me.Fecha2, Me.NoMesesForzoso})
        Me.DataGridView1.Location = New System.Drawing.Point(488, 15)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(787, 188)
        Me.DataGridView1.TabIndex = 28
        '
        'Clv_Promocion3
        '
        Me.Clv_Promocion3.DataPropertyName = "Clv_Promocion"
        Me.Clv_Promocion3.HeaderText = "Clv_Promocion"
        Me.Clv_Promocion3.Name = "Clv_Promocion3"
        Me.Clv_Promocion3.ReadOnly = True
        Me.Clv_Promocion3.Visible = False
        Me.Clv_Promocion3.Width = 137
        '
        'NoMeses
        '
        Me.NoMeses.DataPropertyName = "NoMeses"
        Me.NoMeses.HeaderText = "No. De Meses"
        Me.NoMeses.Name = "NoMeses"
        Me.NoMeses.ReadOnly = True
        Me.NoMeses.Width = 144
        '
        'Fecha1
        '
        Me.Fecha1.DataPropertyName = "Fecha1"
        Me.Fecha1.HeaderText = "Fecha Inicial"
        Me.Fecha1.Name = "Fecha1"
        Me.Fecha1.ReadOnly = True
        Me.Fecha1.Width = 133
        '
        'Fecha2
        '
        Me.Fecha2.DataPropertyName = "Fecha2"
        Me.Fecha2.HeaderText = "Fecha Final"
        Me.Fecha2.Name = "Fecha2"
        Me.Fecha2.ReadOnly = True
        Me.Fecha2.Width = 125
        '
        'NoMesesForzoso
        '
        Me.NoMesesForzoso.DataPropertyName = "NoMesesForzoso"
        Me.NoMesesForzoso.HeaderText = "Meses Forzosos"
        Me.NoMesesForzoso.Name = "NoMesesForzoso"
        Me.NoMesesForzoso.ReadOnly = True
        Me.NoMesesForzoso.Width = 162
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDown3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown3.Location = New System.Drawing.Point(179, 66)
        Me.NumericUpDown3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown3.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(60, 24)
        Me.NumericUpDown3.TabIndex = 27
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox2.Controls.Add(Label4)
        Me.GroupBox2.Controls.Add(Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(11, 113)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(457, 90)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Clientes que Contratan Entre:"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = "YYYYMMDD"
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(25, 53)
        Me.DateTimePicker1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(156, 24)
        Me.DateTimePicker1.TabIndex = 8
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.CustomFormat = "YYYYMMDD"
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(268, 53)
        Me.DateTimePicker2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(152, 24)
        Me.DateTimePicker2.TabIndex = 10
        '
        'CMBButton2
        '
        Me.CMBButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBButton2.Location = New System.Drawing.Point(371, 49)
        Me.CMBButton2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBButton2.Name = "CMBButton2"
        Me.CMBButton2.Size = New System.Drawing.Size(97, 43)
        Me.CMBButton2.TabIndex = 24
        Me.CMBButton2.Text = "&Eliminar"
        Me.CMBButton2.UseVisualStyleBackColor = True
        '
        'CMBButton1
        '
        Me.CMBButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBButton1.Location = New System.Drawing.Point(265, 49)
        Me.CMBButton1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBButton1.Name = "CMBButton1"
        Me.CMBButton1.Size = New System.Drawing.Size(97, 43)
        Me.CMBButton1.TabIndex = 23
        Me.CMBButton1.Text = "&Agregar"
        Me.CMBButton1.UseVisualStyleBackColor = True
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown1.Location = New System.Drawing.Point(179, 21)
        Me.NumericUpDown1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(60, 24)
        Me.NumericUpDown1.TabIndex = 22
        Me.NumericUpDown1.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(12, 219)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(203, 24)
        Me.CheckBox1.TabIndex = 30
        Me.CheckBox1.Text = "Meses Sin Intereses"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel5.Controls.Add(Me.NumericUpDown4)
        Me.Panel5.Controls.Add(Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView)
        Me.Panel5.Controls.Add(Label11)
        Me.Panel5.Controls.Add(Me.CMBBtnPagosEliminar)
        Me.Panel5.Controls.Add(Me.GroupBox3)
        Me.Panel5.Controls.Add(Me.CMBBtnPagosAgregar)
        Me.Panel5.Controls.Add(Me.NumericUpDown2)
        Me.Panel5.Controls.Add(Label3)
        Me.Panel5.Controls.Add(Me.TextBox1)
        Me.Panel5.Controls.Add(Label2)
        Me.Panel5.Enabled = False
        Me.Panel5.Location = New System.Drawing.Point(9, 491)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1313, 202)
        Me.Panel5.TabIndex = 32
        '
        'NumericUpDown4
        '
        Me.NumericUpDown4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDown4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown4.Location = New System.Drawing.Point(232, 74)
        Me.NumericUpDown4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown4.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown4.Name = "NumericUpDown4"
        Me.NumericUpDown4.Size = New System.Drawing.Size(60, 24)
        Me.NumericUpDown4.TabIndex = 30
        '
        'Muestra_Rel_Promocion_NoPagos_tmpDataGridView
        '
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.AllowUserToAddRows = False
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.AllowUserToDeleteRows = False
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.AllowUserToOrderColumns = True
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.Plazo_Forzoso})
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.DataSource = Me.Muestra_Rel_Promocion_NoPagos_tmpBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.DefaultCellStyle = DataGridViewCellStyle5
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.Location = New System.Drawing.Point(529, 20)
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.Name = "Muestra_Rel_Promocion_NoPagos_tmpDataGridView"
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.ReadOnly = True
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.Size = New System.Drawing.Size(767, 172)
        Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.TabIndex = 27
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "clv_promocion"
        Me.DataGridViewTextBoxColumn1.HeaderText = "clv_promocion"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Porcentaje_inicial"
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn2.HeaderText = "Porcentaje Inicial"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "resto"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Resto De Meses"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "fecha1"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Fecha Inicial"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "fecha2"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Fecha Final"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'Plazo_Forzoso
        '
        Me.Plazo_Forzoso.DataPropertyName = "Plazo_Forzoso"
        Me.Plazo_Forzoso.HeaderText = "Meses Forzosos"
        Me.Plazo_Forzoso.Name = "Plazo_Forzoso"
        Me.Plazo_Forzoso.ReadOnly = True
        '
        'Muestra_Rel_Promocion_NoPagos_tmpBindingSource
        '
        Me.Muestra_Rel_Promocion_NoPagos_tmpBindingSource.DataMember = "Muestra_Rel_Promocion_NoPagos_tmp"
        Me.Muestra_Rel_Promocion_NoPagos_tmpBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'CMBBtnPagosEliminar
        '
        Me.CMBBtnPagosEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnPagosEliminar.Location = New System.Drawing.Point(424, 20)
        Me.CMBBtnPagosEliminar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnPagosEliminar.Name = "CMBBtnPagosEliminar"
        Me.CMBBtnPagosEliminar.Size = New System.Drawing.Size(97, 43)
        Me.CMBBtnPagosEliminar.TabIndex = 27
        Me.CMBBtnPagosEliminar.Text = "&Eliminar"
        Me.CMBBtnPagosEliminar.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.DateTimePicker3)
        Me.GroupBox3.Controls.Add(Me.DateTimePicker4)
        Me.GroupBox3.Controls.Add(Label6)
        Me.GroupBox3.Controls.Add(Label7)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(16, 106)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Size = New System.Drawing.Size(439, 86)
        Me.GroupBox3.TabIndex = 18
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Clientes que Contratan Entre:"
        '
        'DateTimePicker3
        '
        Me.DateTimePicker3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker3.Location = New System.Drawing.Point(25, 53)
        Me.DateTimePicker3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePicker3.Name = "DateTimePicker3"
        Me.DateTimePicker3.Size = New System.Drawing.Size(156, 24)
        Me.DateTimePicker3.TabIndex = 8
        '
        'DateTimePicker4
        '
        Me.DateTimePicker4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker4.Location = New System.Drawing.Point(268, 53)
        Me.DateTimePicker4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(152, 24)
        Me.DateTimePicker4.TabIndex = 10
        '
        'CMBBtnPagosAgregar
        '
        Me.CMBBtnPagosAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnPagosAgregar.Location = New System.Drawing.Point(311, 20)
        Me.CMBBtnPagosAgregar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnPagosAgregar.Name = "CMBBtnPagosAgregar"
        Me.CMBBtnPagosAgregar.Size = New System.Drawing.Size(97, 43)
        Me.CMBBtnPagosAgregar.TabIndex = 26
        Me.CMBBtnPagosAgregar.Text = "&Agregar"
        Me.CMBBtnPagosAgregar.UseVisualStyleBackColor = True
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDown2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown2.Location = New System.Drawing.Point(232, 41)
        Me.NumericUpDown2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown2.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(60, 24)
        Me.NumericUpDown2.TabIndex = 26
        Me.NumericUpDown2.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(231, 7)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.MaxLength = 3
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(59, 26)
        Me.TextBox1.TabIndex = 27
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(9, 464)
        Me.CheckBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(83, 24)
        Me.CheckBox2.TabIndex = 31
        Me.CheckBox2.Text = "Pagos"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'MuestraRelPromocionMesesSinInteTmpBindingSource
        '
        Me.MuestraRelPromocionMesesSinInteTmpBindingSource.DataMember = "Muestra_Rel_Promocion_MesesSinInte_Tmp"
        Me.MuestraRelPromocionMesesSinInteTmpBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Consulta_Rel_Promocion_Meses_AplicaBindingSource
        '
        Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource.DataMember = "Consulta_Rel_Promocion_Meses_Aplica"
        Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_Rel_Promocion_Meses_AplicaTableAdapter
        '
        Me.Inserta_Rel_Promocion_Meses_AplicaTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Rel_Promocion_Meses_AplicaBindingSource
        '
        Me.Inserta_Rel_Promocion_Meses_AplicaBindingSource.DataMember = "Inserta_Rel_Promocion_Meses_Aplica"
        Me.Inserta_Rel_Promocion_Meses_AplicaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Rel_Promocion_Meses_AplicaTableAdapter
        '
        Me.Consulta_Rel_Promocion_Meses_AplicaTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ciudadTableAdapter
        '
        Me.Muestra_ciudadTableAdapter.ClearBeforeFill = True
        '
        'Muestra_colonias_segun_CiudadTableAdapter
        '
        Me.Muestra_colonias_segun_CiudadTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Rel_Promocion_Colonias_Ciudad_tmpTableAdapter
        '
        Me.Muestra_Rel_Promocion_Colonias_Ciudad_tmpTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Rel_Promocion_MesesSinInte_TmpTableAdapter
        '
        Me.Muestra_Rel_Promocion_MesesSinInte_TmpTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Rel_Promocion_NoPagos_tmpTableAdapter
        '
        Me.Muestra_Rel_Promocion_NoPagos_tmpTableAdapter.ClearBeforeFill = True
        '
        'CMBTabControl1
        '
        Me.CMBTabControl1.Controls.Add(Me.TabPage1)
        Me.CMBTabControl1.Controls.Add(Me.TabPage2)
        Me.CMBTabControl1.Controls.Add(Me.TabPage3)
        Me.CMBTabControl1.Controls.Add(Me.TabPage4)
        Me.CMBTabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTabControl1.Location = New System.Drawing.Point(16, 130)
        Me.CMBTabControl1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTabControl1.Name = "CMBTabControl1"
        Me.CMBTabControl1.SelectedIndex = 0
        Me.CMBTabControl1.Size = New System.Drawing.Size(1328, 731)
        Me.CMBTabControl1.TabIndex = 10
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage1.Location = New System.Drawing.Point(4, 29)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage1.Size = New System.Drawing.Size(1320, 698)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Promoción"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage2.Controls.Add(Me.Panel9)
        Me.TabPage2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(4, 29)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage2.Size = New System.Drawing.Size(1320, 698)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Servicios Internet"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel9.Controls.Add(Me.Panel7)
        Me.Panel9.Location = New System.Drawing.Point(0, 4)
        Me.Panel9.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(1309, 658)
        Me.Panel9.TabIndex = 32
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel7.Controls.Add(Me.DGVServInt)
        Me.Panel7.Controls.Add(Me.CMBBtnDelInt)
        Me.Panel7.Controls.Add(Me.CMBBtnAddInt)
        Me.Panel7.Controls.Add(Me.LblServInt)
        Me.Panel7.Controls.Add(Me.CmbBoxServInt)
        Me.Panel7.Enabled = False
        Me.Panel7.Location = New System.Drawing.Point(4, 4)
        Me.Panel7.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(1293, 651)
        Me.Panel7.TabIndex = 31
        '
        'DGVServInt
        '
        Me.DGVServInt.AllowUserToAddRows = False
        Me.DGVServInt.AllowUserToDeleteRows = False
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVServInt.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.DGVServInt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVServInt.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clv_promocion2, Me.clv_servicio2, Me.Servicio2, Me.Tipo2})
        Me.DGVServInt.Location = New System.Drawing.Point(532, 23)
        Me.DGVServInt.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DGVServInt.Name = "DGVServInt"
        Me.DGVServInt.ReadOnly = True
        Me.DGVServInt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVServInt.Size = New System.Drawing.Size(739, 286)
        Me.DGVServInt.TabIndex = 30
        '
        'clv_promocion2
        '
        Me.clv_promocion2.DataPropertyName = "clv_promocion"
        Me.clv_promocion2.HeaderText = "Clv_promocion"
        Me.clv_promocion2.Name = "clv_promocion2"
        Me.clv_promocion2.ReadOnly = True
        Me.clv_promocion2.Visible = False
        '
        'clv_servicio2
        '
        Me.clv_servicio2.DataPropertyName = "clv_servicio"
        Me.clv_servicio2.HeaderText = "clv_servicio"
        Me.clv_servicio2.Name = "clv_servicio2"
        Me.clv_servicio2.ReadOnly = True
        Me.clv_servicio2.Visible = False
        '
        'Servicio2
        '
        Me.Servicio2.DataPropertyName = "Servicio"
        Me.Servicio2.HeaderText = "Servicio"
        Me.Servicio2.Name = "Servicio2"
        Me.Servicio2.ReadOnly = True
        Me.Servicio2.Width = 300
        '
        'Tipo2
        '
        Me.Tipo2.DataPropertyName = "Tipo"
        Me.Tipo2.HeaderText = "Tipo"
        Me.Tipo2.Name = "Tipo2"
        Me.Tipo2.ReadOnly = True
        Me.Tipo2.Visible = False
        '
        'CMBBtnDelInt
        '
        Me.CMBBtnDelInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnDelInt.Location = New System.Drawing.Point(372, 86)
        Me.CMBBtnDelInt.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnDelInt.Name = "CMBBtnDelInt"
        Me.CMBBtnDelInt.Size = New System.Drawing.Size(97, 43)
        Me.CMBBtnDelInt.TabIndex = 29
        Me.CMBBtnDelInt.Text = "&Eliminar"
        Me.CMBBtnDelInt.UseVisualStyleBackColor = True
        '
        'CMBBtnAddInt
        '
        Me.CMBBtnAddInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnAddInt.Location = New System.Drawing.Point(267, 87)
        Me.CMBBtnAddInt.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnAddInt.Name = "CMBBtnAddInt"
        Me.CMBBtnAddInt.Size = New System.Drawing.Size(97, 43)
        Me.CMBBtnAddInt.TabIndex = 28
        Me.CMBBtnAddInt.Text = "&Agregar"
        Me.CMBBtnAddInt.UseVisualStyleBackColor = True
        '
        'LblServInt
        '
        Me.LblServInt.AutoSize = True
        Me.LblServInt.Location = New System.Drawing.Point(11, 15)
        Me.LblServInt.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LblServInt.Name = "LblServInt"
        Me.LblServInt.Size = New System.Drawing.Size(187, 20)
        Me.LblServInt.TabIndex = 1
        Me.LblServInt.Text = "Servicios De Internet"
        '
        'CmbBoxServInt
        '
        Me.CmbBoxServInt.DisplayMember = "Descripcion"
        Me.CmbBoxServInt.FormattingEnabled = True
        Me.CmbBoxServInt.Location = New System.Drawing.Point(15, 38)
        Me.CmbBoxServInt.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CmbBoxServInt.Name = "CmbBoxServInt"
        Me.CmbBoxServInt.Size = New System.Drawing.Size(460, 28)
        Me.CmbBoxServInt.TabIndex = 0
        Me.CmbBoxServInt.ValueMember = "Clv_Servicio"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage3.Controls.Add(Me.Panel10)
        Me.TabPage3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage3.Location = New System.Drawing.Point(4, 29)
        Me.TabPage3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage3.Size = New System.Drawing.Size(1320, 698)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Servicios Teléfonia"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.Panel6)
        Me.Panel10.Location = New System.Drawing.Point(0, 0)
        Me.Panel10.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(1313, 658)
        Me.Panel10.TabIndex = 37
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel6.Controls.Add(Me.DGVServTel)
        Me.Panel6.Controls.Add(Me.LblServTel)
        Me.Panel6.Controls.Add(Me.CMBBtnDelTel)
        Me.Panel6.Controls.Add(Me.CMBBtnAddTel)
        Me.Panel6.Controls.Add(Me.CmbBoxServTel)
        Me.Panel6.Enabled = False
        Me.Panel6.Location = New System.Drawing.Point(4, 4)
        Me.Panel6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(1305, 651)
        Me.Panel6.TabIndex = 36
        '
        'DGVServTel
        '
        Me.DGVServTel.AllowUserToAddRows = False
        Me.DGVServTel.AllowUserToDeleteRows = False
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVServTel.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.DGVServTel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVServTel.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clv_promocion1, Me.clv_servicio1, Me.Servicio1, Me.Tipo1})
        Me.DGVServTel.Location = New System.Drawing.Point(533, 46)
        Me.DGVServTel.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DGVServTel.Name = "DGVServTel"
        Me.DGVServTel.ReadOnly = True
        Me.DGVServTel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVServTel.Size = New System.Drawing.Size(677, 258)
        Me.DGVServTel.TabIndex = 35
        '
        'clv_promocion1
        '
        Me.clv_promocion1.DataPropertyName = "clv_promocion"
        Me.clv_promocion1.HeaderText = "clv_promocion"
        Me.clv_promocion1.Name = "clv_promocion1"
        Me.clv_promocion1.ReadOnly = True
        Me.clv_promocion1.Visible = False
        '
        'clv_servicio1
        '
        Me.clv_servicio1.DataPropertyName = "clv_servicio"
        Me.clv_servicio1.HeaderText = "clv_servicio"
        Me.clv_servicio1.Name = "clv_servicio1"
        Me.clv_servicio1.ReadOnly = True
        Me.clv_servicio1.Visible = False
        '
        'Servicio1
        '
        Me.Servicio1.DataPropertyName = "Servicio"
        Me.Servicio1.HeaderText = "Servicio"
        Me.Servicio1.Name = "Servicio1"
        Me.Servicio1.ReadOnly = True
        Me.Servicio1.Width = 300
        '
        'Tipo1
        '
        Me.Tipo1.DataPropertyName = "Tipo"
        Me.Tipo1.HeaderText = "Tipo"
        Me.Tipo1.Name = "Tipo1"
        Me.Tipo1.ReadOnly = True
        Me.Tipo1.Visible = False
        '
        'LblServTel
        '
        Me.LblServTel.AutoSize = True
        Me.LblServTel.Location = New System.Drawing.Point(4, 17)
        Me.LblServTel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LblServTel.Name = "LblServTel"
        Me.LblServTel.Size = New System.Drawing.Size(200, 20)
        Me.LblServTel.TabIndex = 32
        Me.LblServTel.Text = "Servicios De Teléfonia"
        '
        'CMBBtnDelTel
        '
        Me.CMBBtnDelTel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnDelTel.Location = New System.Drawing.Point(373, 94)
        Me.CMBBtnDelTel.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnDelTel.Name = "CMBBtnDelTel"
        Me.CMBBtnDelTel.Size = New System.Drawing.Size(97, 43)
        Me.CMBBtnDelTel.TabIndex = 34
        Me.CMBBtnDelTel.Text = "&Eliminar"
        Me.CMBBtnDelTel.UseVisualStyleBackColor = True
        '
        'CMBBtnAddTel
        '
        Me.CMBBtnAddTel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnAddTel.Location = New System.Drawing.Point(268, 95)
        Me.CMBBtnAddTel.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnAddTel.Name = "CMBBtnAddTel"
        Me.CMBBtnAddTel.Size = New System.Drawing.Size(97, 43)
        Me.CMBBtnAddTel.TabIndex = 33
        Me.CMBBtnAddTel.Text = "&Agregar"
        Me.CMBBtnAddTel.UseVisualStyleBackColor = True
        '
        'CmbBoxServTel
        '
        Me.CmbBoxServTel.DisplayMember = "Descripcion"
        Me.CmbBoxServTel.FormattingEnabled = True
        Me.CmbBoxServTel.Location = New System.Drawing.Point(8, 41)
        Me.CmbBoxServTel.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CmbBoxServTel.Name = "CmbBoxServTel"
        Me.CmbBoxServTel.Size = New System.Drawing.Size(460, 28)
        Me.CmbBoxServTel.TabIndex = 31
        Me.CmbBoxServTel.ValueMember = "Clv_Servicio"
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage4.Controls.Add(Me.Panel11)
        Me.TabPage4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage4.Location = New System.Drawing.Point(4, 29)
        Me.TabPage4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage4.Size = New System.Drawing.Size(1320, 698)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Combos"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.Panel8)
        Me.Panel11.Location = New System.Drawing.Point(4, 4)
        Me.Panel11.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(1309, 658)
        Me.Panel11.TabIndex = 42
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel8.Controls.Add(Me.DGVCombo)
        Me.Panel8.Controls.Add(Me.LblCombos)
        Me.Panel8.Controls.Add(Me.CMBBtnDelCombo)
        Me.Panel8.Controls.Add(Me.CMBBtnAddCombo)
        Me.Panel8.Controls.Add(Me.CmbBoxCombos)
        Me.Panel8.Enabled = False
        Me.Panel8.Location = New System.Drawing.Point(4, 4)
        Me.Panel8.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(1301, 651)
        Me.Panel8.TabIndex = 41
        '
        'DGVCombo
        '
        Me.DGVCombo.AllowUserToAddRows = False
        Me.DGVCombo.AllowUserToDeleteRows = False
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVCombo.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.DGVCombo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVCombo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clv_promocion, Me.clv_servicio, Me.Servicio, Me.Tipo})
        Me.DGVCombo.Location = New System.Drawing.Point(517, 23)
        Me.DGVCombo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DGVCombo.Name = "DGVCombo"
        Me.DGVCombo.ReadOnly = True
        Me.DGVCombo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVCombo.Size = New System.Drawing.Size(677, 258)
        Me.DGVCombo.TabIndex = 40
        '
        'clv_promocion
        '
        Me.clv_promocion.DataPropertyName = "clv_promocion"
        Me.clv_promocion.HeaderText = "clv_promocion"
        Me.clv_promocion.Name = "clv_promocion"
        Me.clv_promocion.ReadOnly = True
        Me.clv_promocion.Visible = False
        '
        'clv_servicio
        '
        Me.clv_servicio.DataPropertyName = "clv_servicio"
        Me.clv_servicio.HeaderText = "clv_servicio"
        Me.clv_servicio.Name = "clv_servicio"
        Me.clv_servicio.ReadOnly = True
        Me.clv_servicio.Visible = False
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.ReadOnly = True
        Me.Servicio.Width = 300
        '
        'Tipo
        '
        Me.Tipo.DataPropertyName = "Tipo"
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.ReadOnly = True
        Me.Tipo.Visible = False
        '
        'LblCombos
        '
        Me.LblCombos.AutoSize = True
        Me.LblCombos.Location = New System.Drawing.Point(11, 11)
        Me.LblCombos.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LblCombos.Name = "LblCombos"
        Me.LblCombos.Size = New System.Drawing.Size(77, 20)
        Me.LblCombos.TabIndex = 37
        Me.LblCombos.Text = "Combos"
        '
        'CMBBtnDelCombo
        '
        Me.CMBBtnDelCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnDelCombo.Location = New System.Drawing.Point(377, 70)
        Me.CMBBtnDelCombo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnDelCombo.Name = "CMBBtnDelCombo"
        Me.CMBBtnDelCombo.Size = New System.Drawing.Size(97, 43)
        Me.CMBBtnDelCombo.TabIndex = 39
        Me.CMBBtnDelCombo.Text = "&Eliminar"
        Me.CMBBtnDelCombo.UseVisualStyleBackColor = True
        '
        'CMBBtnAddCombo
        '
        Me.CMBBtnAddCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnAddCombo.Location = New System.Drawing.Point(272, 71)
        Me.CMBBtnAddCombo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBBtnAddCombo.Name = "CMBBtnAddCombo"
        Me.CMBBtnAddCombo.Size = New System.Drawing.Size(97, 43)
        Me.CMBBtnAddCombo.TabIndex = 38
        Me.CMBBtnAddCombo.Text = "&Agregar"
        Me.CMBBtnAddCombo.UseVisualStyleBackColor = True
        '
        'CmbBoxCombos
        '
        Me.CmbBoxCombos.DisplayMember = "Combo"
        Me.CmbBoxCombos.FormattingEnabled = True
        Me.CmbBoxCombos.Location = New System.Drawing.Point(11, 34)
        Me.CmbBoxCombos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CmbBoxCombos.Name = "CmbBoxCombos"
        Me.CmbBoxCombos.Size = New System.Drawing.Size(460, 28)
        Me.CmbBoxCombos.TabIndex = 36
        Me.CmbBoxCombos.ValueMember = "clv_descuento"
        '
        'MuestraServiciosPromocionesBindingSource1
        '
        Me.MuestraServiciosPromocionesBindingSource1.DataMember = "Muestra_Servicios_Promociones"
        Me.MuestraServiciosPromocionesBindingSource1.DataSource = Me.Procedimientosarnoldo4
        '
        'MuestraServiciosPromocionesBindingSource2
        '
        Me.MuestraServiciosPromocionesBindingSource2.DataMember = "Muestra_Servicios_Promociones"
        Me.MuestraServiciosPromocionesBindingSource2.DataSource = Me.Procedimientosarnoldo4
        '
        'CONTipoPromocionTableAdapter2
        '
        Me.CONTipoPromocionTableAdapter2.ClearBeforeFill = True
        '
        'FrmPromocionesTel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 903)
        Me.Controls.Add(Me.CMBTabControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmPromocionesTel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promociones Telefonía"
        CType(Me.CONTipoPromocionBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONTipoPromocionBindingNavigator.ResumeLayout(False)
        Me.CONTipoPromocionBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONTipoPromocionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONTipoPromocionBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.MuestracoloniassegunCiudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraciudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraRelPromocionColoniasCiudadtmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Rel_Promocion_NoPagos_tmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraRelPromocionMesesSinInteTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_Promocion_Meses_AplicaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBTabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.DGVServInt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.DGVServTel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.Panel11.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        CType(Me.DGVCombo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServiciosPromocionesBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServiciosPromocionesBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Consulta_Rel_Promocion_Meses_AplicaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents CONTipoPromocionBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONTipoPromocionBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Inserta_Rel_Promocion_Meses_AplicaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Promocion_Meses_AplicaTableAdapter
    Friend WithEvents Inserta_Rel_Promocion_Meses_AplicaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONTipoPromocionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONTipoPromocionTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CLAVETextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Consulta_Rel_Promocion_Meses_AplicaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Rel_Promocion_Meses_AplicaTableAdapter
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBButton4 As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CMBButton3 As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents CMBButton2 As System.Windows.Forms.Button
    Friend WithEvents CMBButton1 As System.Windows.Forms.Button
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents DateTimePicker3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents MuestraciudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_ciudadTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_ciudadTableAdapter
    Friend WithEvents MuestracoloniassegunCiudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_colonias_segun_CiudadTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_colonias_segun_CiudadTableAdapter
    Friend WithEvents ClvpromocionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvciudadDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvcoloniaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColoniaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CiudadDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MuestraRelPromocionColoniasCiudadtmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Rel_Promocion_Colonias_Ciudad_tmpTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Rel_Promocion_Colonias_Ciudad_tmpTableAdapter
    Friend WithEvents MuestraRelPromocionMesesSinInteTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Rel_Promocion_MesesSinInte_TmpTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Rel_Promocion_MesesSinInte_TmpTableAdapter
    Friend WithEvents CMBBtnPagosEliminar As System.Windows.Forms.Button
    Friend WithEvents CMBBtnPagosAgregar As System.Windows.Forms.Button
    Friend WithEvents Muestra_Rel_Promocion_NoPagos_tmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Rel_Promocion_NoPagos_tmpTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Rel_Promocion_NoPagos_tmpTableAdapter
    Friend WithEvents Muestra_Rel_Promocion_NoPagos_tmpDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents CMBTabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents DGVServInt As System.Windows.Forms.DataGridView
    Friend WithEvents CMBBtnDelInt As System.Windows.Forms.Button
    Friend WithEvents CMBBtnAddInt As System.Windows.Forms.Button
    Friend WithEvents LblServInt As System.Windows.Forms.Label
    Friend WithEvents CmbBoxServInt As System.Windows.Forms.ComboBox
    Friend WithEvents DGVServTel As System.Windows.Forms.DataGridView
    Friend WithEvents CMBBtnDelTel As System.Windows.Forms.Button
    Friend WithEvents CMBBtnAddTel As System.Windows.Forms.Button
    Friend WithEvents LblServTel As System.Windows.Forms.Label
    Friend WithEvents CmbBoxServTel As System.Windows.Forms.ComboBox
    Friend WithEvents DGVCombo As System.Windows.Forms.DataGridView
    Friend WithEvents CMBBtnDelCombo As System.Windows.Forms.Button
    Friend WithEvents CMBBtnAddCombo As System.Windows.Forms.Button
    Friend WithEvents LblCombos As System.Windows.Forms.Label
    Friend WithEvents CmbBoxCombos As System.Windows.Forms.ComboBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents CONTipoPromocionBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents NewSofTvDataSet1 As sofTV.NewSofTvDataSet
    Friend WithEvents CONTipoPromocionTableAdapter1 As sofTV.NewSofTvDataSetTableAdapters.CONTipoPromocionTableAdapter
    Friend WithEvents MuestraServiciosPromocionesBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServiciosPromocionesBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents clv_promocion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_promocion1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_servicio1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_promocion2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_servicio2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CONTipoPromocionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewSofTvDataSet2 As sofTV.NewSofTvDataSet
    Friend WithEvents CONTipoPromocionTableAdapter2 As sofTV.NewSofTvDataSetTableAdapters.CONTipoPromocionTableAdapter
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents NumericUpDown3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents NumericUpDown4 As System.Windows.Forms.NumericUpDown
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Plazo_Forzoso As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Promocion3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoMeses As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoMesesForzoso As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
