Imports System.Data.SqlClient
Public Class BwrEquipoVenta
    'Public KeyAscii As Short
    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function
    Private Sub Busca(ByVal opt As Integer)

        Try
            Dim Conbwr As New SqlConnection(MiConexion)
            If opt = 1 Then
                Conbwr.Open()
                Me.Busca_EquiposVentaTableAdapter.Connection = Conbwr
                Me.Busca_EquiposVentaTableAdapter.Fill(Me.DataSetLidia2.Busca_EquiposVenta, 0, "", "", opt)
                Conbwr.Close()
            ElseIf (opt = 2) Then
                Conbwr.Open()
                Me.Busca_EquiposVentaTableAdapter.Connection = Conbwr
                Me.Busca_EquiposVentaTableAdapter.Fill(Me.DataSetLidia2.Busca_EquiposVenta, Me.TextBox1.Text, "", "", opt)
                Conbwr.Close()
            ElseIf opt = 3 Then
                Conbwr.Open()
                Me.Busca_EquiposVentaTableAdapter.Connection = Conbwr
                Me.Busca_EquiposVentaTableAdapter.Fill(Me.DataSetLidia2.Busca_EquiposVenta, 0, Me.TextBox2.Text, "", opt)
                Conbwr.Close()
            ElseIf (opt = 4) Then
                Conbwr.Open()
                Me.Busca_EquiposVentaTableAdapter.Connection = Conbwr
                Me.Busca_EquiposVentaTableAdapter.Fill(Me.DataSetLidia2.Busca_EquiposVenta, 0, "", "", opt)
                Conbwr.Close()
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmEquipoVenta.Show()
    End Sub

    Private Sub BwrEquipoVenta_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bnd = True Then
            bec_bnd = False
            Busca(1)
        End If
    End Sub

    Private Sub BwrEquipoVenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busca(1)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If IsNumeric(Me.TextBox1.Text) = True Then
            Busca(2)
        Else
            MsgBox("Capture una Clave Correcta", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

  
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.TextBox1.Text.Trim.Length > 0 Then
            Busca(3)
        Else
            MsgBox("Capture un Nombre", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            opcion = "C"
            Clv_Equipo = Me.Clv_calleLabel2.Text
            FrmEquipoVenta.Show()
        Else
            MsgBox("Seleccione un Equipo, Por Favor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            opcion = "M"
            Clv_Equipo = Me.Clv_calleLabel2.Text
            FrmEquipoVenta.Show()
        End If
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            opcion = "C"
            Clv_Equipo = Me.Clv_calleLabel2.Text
            FrmEquipoVenta.Show()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub
End Class