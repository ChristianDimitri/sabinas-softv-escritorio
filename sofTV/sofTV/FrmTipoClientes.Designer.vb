<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTipoClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.ListBox3 = New System.Windows.Forms.ListBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.MuestraSeleccionTipoClienteCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.MuestraSeleccionaTipoClienteTmpCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRATIPOCLIENTESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.MUESTRA_TIPOCLIENTESTableAdapter = New sofTV.DataSetarnoldoTableAdapters.MUESTRA_TIPOCLIENTESTableAdapter()
        Me.LLena_Tabla_TipoClientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LLena_Tabla_TipoClientesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.LLena_Tabla_TipoClientesTableAdapter()
        Me.ProcedimientosArnoldo2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSeleccionaTipoClienteTmpNUEVOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_TipoClienteTmpNUEVOTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_TipoClienteTmpNUEVOTableAdapter()
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter()
        Me.MuestraSeleccion_TipoClienteCONSULTATableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSeleccion_TipoClienteCONSULTATableAdapter()
        Me.InsertaTOSelecciona_TipoClientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSelecciona_TipoClientesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_TipoClientesTableAdapter()
        Me.InsertaTOSelecciona_TipoClientesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSelecciona_TipoClientesTmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_TipoClientesTmpTableAdapter()
        Me.Insertauno_Seleccion_TipoClientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_TipoClientesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_TipoClientesTableAdapter()
        Me.Insertauno_Seleccion_TipoClientestmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_TipoClientestmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_TipoClientestmpTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.MuestraSeleccionTipoClienteCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionaTipoClienteTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATIPOCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LLena_Tabla_TipoClientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionaTipoClienteTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSelecciona_TipoClientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSelecciona_TipoClientesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_TipoClientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_TipoClientestmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel2.Location = New System.Drawing.Point(366, 9)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(220, 15)
        Me.CMBLabel2.TabIndex = 42
        Me.CMBLabel2.Text = "Tipos de Clientes Seleccionados:"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(239, 15)
        Me.CMBLabel1.TabIndex = 41
        Me.CMBLabel1.Text = "Tipos de Clientes  para Seleccionar:"
        '
        'ListBox3
        '
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.Location = New System.Drawing.Point(15, 260)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.ScrollAlwaysVisible = True
        Me.ListBox3.Size = New System.Drawing.Size(202, 43)
        Me.ListBox3.TabIndex = 40
        Me.ListBox3.TabStop = False
        Me.ListBox3.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(475, 267)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(296, 267)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 4
        Me.Button6.Text = "&Aceptar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(262, 154)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(262, 125)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(262, 57)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(262, 28)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.DataSource = Me.MuestraSeleccionTipoClienteCONSULTABindingSource
        Me.ListBox2.DisplayMember = "Descripcion"
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(369, 28)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(209, 225)
        Me.ListBox2.TabIndex = 33
        Me.ListBox2.TabStop = False
        Me.ListBox2.ValueMember = "clv_tipocliente"
        '
        'MuestraSeleccionTipoClienteCONSULTABindingSource
        '
        Me.MuestraSeleccionTipoClienteCONSULTABindingSource.DataMember = "MuestraSeleccion_TipoClienteCONSULTA"
        Me.MuestraSeleccionTipoClienteCONSULTABindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.MuestraSeleccionaTipoClienteTmpCONSULTABindingSource
        Me.ListBox1.DisplayMember = "Descripcion"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(15, 28)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(202, 225)
        Me.ListBox1.TabIndex = 32
        Me.ListBox1.TabStop = False
        Me.ListBox1.ValueMember = "clv_Tip_Ciente"
        Me.ListBox1.Visible = False
        '
        'MuestraSeleccionaTipoClienteTmpCONSULTABindingSource
        '
        Me.MuestraSeleccionaTipoClienteTmpCONSULTABindingSource.DataMember = "MuestraSelecciona_TipoClienteTmpCONSULTA"
        Me.MuestraSeleccionaTipoClienteTmpCONSULTABindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MUESTRATIPOCLIENTESBindingSource
        '
        Me.MUESTRATIPOCLIENTESBindingSource.DataMember = "MUESTRA_TIPOCLIENTES"
        Me.MUESTRATIPOCLIENTESBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRA_TIPOCLIENTESTableAdapter
        '
        Me.MUESTRA_TIPOCLIENTESTableAdapter.ClearBeforeFill = True
        '
        'LLena_Tabla_TipoClientesBindingSource
        '
        Me.LLena_Tabla_TipoClientesBindingSource.DataMember = "LLena_Tabla_TipoClientes"
        Me.LLena_Tabla_TipoClientesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'LLena_Tabla_TipoClientesTableAdapter
        '
        Me.LLena_Tabla_TipoClientesTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2BindingSource
        '
        Me.ProcedimientosArnoldo2BindingSource.DataSource = Me.ProcedimientosArnoldo2
        Me.ProcedimientosArnoldo2BindingSource.Position = 0
        '
        'MuestraSeleccionaTipoClienteTmpNUEVOBindingSource
        '
        Me.MuestraSeleccionaTipoClienteTmpNUEVOBindingSource.DataMember = "MuestraSelecciona_TipoClienteTmpNUEVO"
        Me.MuestraSeleccionaTipoClienteTmpNUEVOBindingSource.DataSource = Me.ProcedimientosArnoldo2BindingSource
        '
        'MuestraSelecciona_TipoClienteTmpNUEVOTableAdapter
        '
        Me.MuestraSelecciona_TipoClienteTmpNUEVOTableAdapter.ClearBeforeFill = True
        '
        'MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter
        '
        Me.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter.ClearBeforeFill = True
        '
        'MuestraSeleccion_TipoClienteCONSULTATableAdapter
        '
        Me.MuestraSeleccion_TipoClienteCONSULTATableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSelecciona_TipoClientesBindingSource
        '
        Me.InsertaTOSelecciona_TipoClientesBindingSource.DataMember = "InsertaTOSelecciona_TipoClientes"
        Me.InsertaTOSelecciona_TipoClientesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSelecciona_TipoClientesTableAdapter
        '
        Me.InsertaTOSelecciona_TipoClientesTableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSelecciona_TipoClientesTmpBindingSource
        '
        Me.InsertaTOSelecciona_TipoClientesTmpBindingSource.DataMember = "InsertaTOSelecciona_TipoClientesTmp"
        Me.InsertaTOSelecciona_TipoClientesTmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSelecciona_TipoClientesTmpTableAdapter
        '
        Me.InsertaTOSelecciona_TipoClientesTmpTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_TipoClientesBindingSource
        '
        Me.Insertauno_Seleccion_TipoClientesBindingSource.DataMember = "Insertauno_Seleccion_TipoClientes"
        Me.Insertauno_Seleccion_TipoClientesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_TipoClientesTableAdapter
        '
        Me.Insertauno_Seleccion_TipoClientesTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_TipoClientestmpBindingSource
        '
        Me.Insertauno_Seleccion_TipoClientestmpBindingSource.DataMember = "Insertauno_Seleccion_TipoClientestmp"
        Me.Insertauno_Seleccion_TipoClientestmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_TipoClientestmpTableAdapter
        '
        Me.Insertauno_Seleccion_TipoClientestmpTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmTipoClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(633, 330)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.ListBox3)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.MaximizeBox = False
        Me.Name = "FrmTipoClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección Tipos de Cliente"
        Me.TopMost = True
        CType(Me.MuestraSeleccionTipoClienteCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionaTipoClienteTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATIPOCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LLena_Tabla_TipoClientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionaTipoClienteTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSelecciona_TipoClientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSelecciona_TipoClientesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_TipoClientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_TipoClientestmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents MUESTRATIPOCLIENTESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents MUESTRA_TIPOCLIENTESTableAdapter As sofTV.DataSetarnoldoTableAdapters.MUESTRA_TIPOCLIENTESTableAdapter
    Friend WithEvents LLena_Tabla_TipoClientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LLena_Tabla_TipoClientesTableAdapter As sofTV.DataSetarnoldoTableAdapters.LLena_Tabla_TipoClientesTableAdapter
    Friend WithEvents MuestraSeleccionTipoClienteCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents MuestraSeleccionaTipoClienteTmpCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSeleccionaTipoClienteTmpNUEVOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_TipoClienteTmpNUEVOTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_TipoClienteTmpNUEVOTableAdapter
    Friend WithEvents MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_TipoClienteTmpCONSULTATableAdapter
    Friend WithEvents MuestraSeleccion_TipoClienteCONSULTATableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSeleccion_TipoClienteCONSULTATableAdapter
    Friend WithEvents InsertaTOSelecciona_TipoClientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSelecciona_TipoClientesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_TipoClientesTableAdapter
    Friend WithEvents InsertaTOSelecciona_TipoClientesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSelecciona_TipoClientesTmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_TipoClientesTmpTableAdapter
    Friend WithEvents Insertauno_Seleccion_TipoClientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_TipoClientesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_TipoClientesTableAdapter
    Friend WithEvents Insertauno_Seleccion_TipoClientestmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_TipoClientestmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_TipoClientestmpTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
