﻿Public Class FrmRentaPromociones

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmRentaPromociones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaComboTipser()
        llenaComboModelo()
        llenaGrid()
    End Sub

    Private Sub llenaComboTipser()
        Try
            BaseII.limpiaParametros()
            ComboBoxTipSer.DataSource = BaseII.ConsultaDT("MuestraTipSerPrincipal")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        
    End Sub

    Private Sub llenaComboServicio()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_tipser", SqlDbType.Int, ComboBoxTipSer.SelectedValue)
        ComboBoxServicios.DataSource = BaseII.ConsultaDT("MuestraServicioPromocionRenta")
    End Sub
    Private Sub llenaComboModelo()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_promocion", SqlDbType.Int, ClvPromocion)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, ComboBoxServicios.SelectedValue)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
        cbModelo.DataSource = BaseII.ConsultaDT("MuestraModelosSTB")
        If cbModelo.Items.Count = 0 Then
            cbModelo.Text = ""
        End If
    End Sub

    Private Sub AgregarServicio_Click(sender As Object, e As EventArgs) Handles AgregarServicio.Click
        If cbModelo.Items.Count = 0 Then
            Exit Sub
        End If
        If TextBoxPrincipal.Text.Length = 0 Then
            TextBoxPrincipal.Text = "0"
        End If
        If tbAdicional1.Text.Length = 0 Then
            tbAdicional1.Text = "0"
        End If
        If tbAdicional2.Text.Length = 0 Then
            tbAdicional2.Text = "0"
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_promocion", SqlDbType.Int, ClvPromocion)
        BaseII.CreateMyParameter("@idArticulo", SqlDbType.Int, cbModelo.SelectedValue)
        BaseII.CreateMyParameter("@principal", SqlDbType.Float, TextBoxPrincipal.Text)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, ComboBoxServicios.SelectedValue)
        BaseII.CreateMyParameter("@adicional1", SqlDbType.Float, tbAdicional1.Text)
        BaseII.CreateMyParameter("@adicional2", SqlDbType.Float, tbAdicional2.Text)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.Inserta("InsertaPromocionRenta")
        llenaGrid()
        llenaComboModelo()
        TextBoxPrincipal.Text = ""
        tbAdicional1.Text = ""
        tbAdicional2.Text = ""
    End Sub

    Private Sub llenaGrid()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_promocion", SqlDbType.Int, ClvPromocion)
            BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, ComboBoxServicios.SelectedValue)
            BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
            DataGridView1.DataSource = BaseII.ConsultaDT("MuestraRentaPromocion")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idArticulo", SqlDbType.Int, DataGridView1.SelectedCells(1).Value)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, ComboBoxServicios.SelectedValue)
        BaseII.CreateMyParameter("@clv_promocion", SqlDbType.Int, ClvPromocion)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.Inserta("EliminaPromocionRenta")
        llenaGrid()
        llenaComboModelo()
    End Sub

    Private Sub ComboBoxTipSer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxTipSer.SelectedIndexChanged
        llenaComboServicio()
    End Sub

    Private Sub ComboBoxServicios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxServicios.SelectedIndexChanged
        llenaComboModelo()
        llenaGrid()
    End Sub
End Class