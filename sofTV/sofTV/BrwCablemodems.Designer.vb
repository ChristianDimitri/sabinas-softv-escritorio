<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCablemodems
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Clv_calleLabel1 As System.Windows.Forms.Label
        Dim CMBTipoAparatoLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.bnBuscarColonia = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbColonia = New System.Windows.Forms.ComboBox()
        Me.bnBuscarNombre = New System.Windows.Forms.Button()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.bnBuscarContrato = New System.Windows.Forms.Button()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.bnBuscarMac = New System.Windows.Forms.Button()
        Me.tbMac = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvAparatos = New System.Windows.Forms.DataGridView()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.DescripcionListBox = New System.Windows.Forms.ListBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CMBNombreTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Clv_calleLabel2 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.bnConsultar = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.campoClave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.campoStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Colonia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.campoUbicacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.campoDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        NombreLabel = New System.Windows.Forms.Label()
        Clv_calleLabel1 = New System.Windows.Forms.Label()
        CMBTipoAparatoLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.dgvAparatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.White
        NombreLabel.Location = New System.Drawing.Point(17, 68)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(102, 15)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Serie Aparato :"
        '
        'Clv_calleLabel1
        '
        Clv_calleLabel1.AutoSize = True
        Clv_calleLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_calleLabel1.ForeColor = System.Drawing.Color.White
        Clv_calleLabel1.Location = New System.Drawing.Point(17, 36)
        Clv_calleLabel1.Name = "Clv_calleLabel1"
        Clv_calleLabel1.Size = New System.Drawing.Size(50, 15)
        Clv_calleLabel1.TabIndex = 1
        Clv_calleLabel1.Text = "Clave :"
        '
        'CMBTipoAparatoLabel
        '
        CMBTipoAparatoLabel.AutoSize = True
        CMBTipoAparatoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBTipoAparatoLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        CMBTipoAparatoLabel.Location = New System.Drawing.Point(146, 248)
        CMBTipoAparatoLabel.Name = "CMBTipoAparatoLabel"
        CMBTipoAparatoLabel.Size = New System.Drawing.Size(122, 20)
        CMBTipoAparatoLabel.TabIndex = 52
        CMBTipoAparatoLabel.Text = "Tipo Aparato :"
        CMBTipoAparatoLabel.Visible = False
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.White
        Label1.Location = New System.Drawing.Point(212, 481)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(91, 15)
        Label1.TabIndex = 8
        Label1.Text = "Información :"
        Label1.Visible = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBoxCompanias)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label12)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnBuscarColonia)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cbColonia)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnBuscarNombre)
        Me.SplitContainer1.Panel1.Controls.Add(Me.tbNombre)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnBuscarContrato)
        Me.SplitContainer1.Panel1.Controls.Add(Me.tbContrato)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnBuscarMac)
        Me.SplitContainer1.Panel1.Controls.Add(Me.tbMac)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgvAparatos)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button7)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel2.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel2.Controls.Add(CMBTipoAparatoLabel)
        Me.SplitContainer1.Panel2.Controls.Add(Me.DescripcionListBox)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ComboBox1)
        Me.SplitContainer1.Panel2.Controls.Add(Label1)
        Me.SplitContainer1.Size = New System.Drawing.Size(836, 717)
        Me.SplitContainer1.SplitterDistance = 278
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(10, 61)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(243, 23)
        Me.ComboBoxCompanias.TabIndex = 204
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(9, 40)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(65, 18)
        Me.Label12.TabIndex = 205
        Me.Label12.Text = "Plaza  :"
        '
        'bnBuscarColonia
        '
        Me.bnBuscarColonia.BackColor = System.Drawing.Color.DarkOrange
        Me.bnBuscarColonia.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnBuscarColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarColonia.ForeColor = System.Drawing.Color.Black
        Me.bnBuscarColonia.Location = New System.Drawing.Point(10, 400)
        Me.bnBuscarColonia.Name = "bnBuscarColonia"
        Me.bnBuscarColonia.Size = New System.Drawing.Size(88, 23)
        Me.bnBuscarColonia.TabIndex = 59
        Me.bnBuscarColonia.Text = "Buscar"
        Me.bnBuscarColonia.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(7, 355)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 15)
        Me.Label7.TabIndex = 58
        Me.Label7.Text = "Colonia :"
        '
        'cbColonia
        '
        Me.cbColonia.FormattingEnabled = True
        Me.cbColonia.Location = New System.Drawing.Point(10, 373)
        Me.cbColonia.Name = "cbColonia"
        Me.cbColonia.Size = New System.Drawing.Size(243, 21)
        Me.cbColonia.TabIndex = 57
        '
        'bnBuscarNombre
        '
        Me.bnBuscarNombre.BackColor = System.Drawing.Color.DarkOrange
        Me.bnBuscarNombre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnBuscarNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarNombre.ForeColor = System.Drawing.Color.Black
        Me.bnBuscarNombre.Location = New System.Drawing.Point(10, 310)
        Me.bnBuscarNombre.Name = "bnBuscarNombre"
        Me.bnBuscarNombre.Size = New System.Drawing.Size(88, 23)
        Me.bnBuscarNombre.TabIndex = 5
        Me.bnBuscarNombre.Text = "&Buscar"
        Me.bnBuscarNombre.UseVisualStyleBackColor = False
        '
        'tbNombre
        '
        Me.tbNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombre.Location = New System.Drawing.Point(10, 283)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(243, 21)
        Me.tbNombre.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(7, 265)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(139, 15)
        Me.Label6.TabIndex = 56
        Me.Label6.Text = "Nombre del Cliente :"
        '
        'bnBuscarContrato
        '
        Me.bnBuscarContrato.BackColor = System.Drawing.Color.DarkOrange
        Me.bnBuscarContrato.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnBuscarContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarContrato.ForeColor = System.Drawing.Color.Black
        Me.bnBuscarContrato.Location = New System.Drawing.Point(10, 221)
        Me.bnBuscarContrato.Name = "bnBuscarContrato"
        Me.bnBuscarContrato.Size = New System.Drawing.Size(88, 23)
        Me.bnBuscarContrato.TabIndex = 3
        Me.bnBuscarContrato.Text = "&Buscar"
        Me.bnBuscarContrato.UseVisualStyleBackColor = False
        '
        'tbContrato
        '
        Me.tbContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContrato.Location = New System.Drawing.Point(10, 194)
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.Size = New System.Drawing.Size(243, 21)
        Me.tbContrato.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(7, 176)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 15)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "Contrato  :"
        '
        'bnBuscarMac
        '
        Me.bnBuscarMac.BackColor = System.Drawing.Color.DarkOrange
        Me.bnBuscarMac.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnBuscarMac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarMac.ForeColor = System.Drawing.Color.Black
        Me.bnBuscarMac.Location = New System.Drawing.Point(10, 136)
        Me.bnBuscarMac.Name = "bnBuscarMac"
        Me.bnBuscarMac.Size = New System.Drawing.Size(88, 23)
        Me.bnBuscarMac.TabIndex = 1
        Me.bnBuscarMac.Text = "&Buscar"
        Me.bnBuscarMac.UseVisualStyleBackColor = False
        '
        'tbMac
        '
        Me.tbMac.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbMac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMac.Location = New System.Drawing.Point(10, 109)
        Me.tbMac.Name = "tbMac"
        Me.tbMac.Size = New System.Drawing.Size(243, 21)
        Me.tbMac.TabIndex = 0
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(6, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(202, 24)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar Aparato Por :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(7, 91)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(119, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Mac de Aparato  :"
        '
        'dgvAparatos
        '
        Me.dgvAparatos.AllowUserToAddRows = False
        Me.dgvAparatos.AllowUserToDeleteRows = False
        Me.dgvAparatos.AllowUserToOrderColumns = True
        Me.dgvAparatos.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAparatos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvAparatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAparatos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.campoClave, Me.campoStatus, Me.Contrato, Me.Nombre, Me.Colonia, Me.campoUbicacion, Me.campoDescripcion})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAparatos.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvAparatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAparatos.Location = New System.Drawing.Point(0, 0)
        Me.dgvAparatos.Name = "dgvAparatos"
        Me.dgvAparatos.ReadOnly = True
        Me.dgvAparatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAparatos.Size = New System.Drawing.Size(554, 717)
        Me.dgvAparatos.TabIndex = 1
        Me.dgvAparatos.TabStop = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.Enabled = False
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(150, 362)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(88, 23)
        Me.Button7.TabIndex = 7
        Me.Button7.Text = "&Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        Me.Button7.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Enabled = False
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(147, 317)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Clave :"
        Me.Label3.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Enabled = False
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(150, 335)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(88, 21)
        Me.TextBox1.TabIndex = 6
        Me.TextBox1.Visible = False
        '
        'DescripcionListBox
        '
        Me.DescripcionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionListBox.FormattingEnabled = True
        Me.DescripcionListBox.Location = New System.Drawing.Point(204, 438)
        Me.DescripcionListBox.Name = "DescripcionListBox"
        Me.DescripcionListBox.Size = New System.Drawing.Size(99, 95)
        Me.DescripcionListBox.TabIndex = 7
        Me.DescripcionListBox.TabStop = False
        Me.DescripcionListBox.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.CMBNombreTextBox)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Clv_calleLabel1)
        Me.Panel1.Controls.Add(Me.Clv_calleLabel2)
        Me.Panel1.Location = New System.Drawing.Point(135, 418)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(273, 169)
        Me.Panel1.TabIndex = 8
        Me.Panel1.Visible = False
        '
        'CMBNombreTextBox
        '
        Me.CMBNombreTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNombreTextBox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CMBNombreTextBox.Location = New System.Drawing.Point(20, 86)
        Me.CMBNombreTextBox.Multiline = True
        Me.CMBNombreTextBox.Name = "CMBNombreTextBox"
        Me.CMBNombreTextBox.ReadOnly = True
        Me.CMBNombreTextBox.Size = New System.Drawing.Size(237, 31)
        Me.CMBNombreTextBox.TabIndex = 6
        Me.CMBNombreTextBox.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(165, 20)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos del Aparato :"
        '
        'Clv_calleLabel2
        '
        Me.Clv_calleLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_calleLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Clv_calleLabel2.Location = New System.Drawing.Point(75, 36)
        Me.Clv_calleLabel2.Name = "Clv_calleLabel2"
        Me.Clv_calleLabel2.Size = New System.Drawing.Size(182, 23)
        Me.Clv_calleLabel2.TabIndex = 2
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "Clv_TiposAparatos"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(149, 271)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(243, 26)
        Me.ComboBox1.TabIndex = 5
        Me.ComboBox1.ValueMember = "Clv_TiposAparatos"
        Me.ComboBox1.Visible = False
        '
        'bnSalir
        '
        Me.bnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.bnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.ForeColor = System.Drawing.Color.Black
        Me.bnSalir.Location = New System.Drawing.Point(868, 667)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 4
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = False
        '
        'bnConsultar
        '
        Me.bnConsultar.BackColor = System.Drawing.Color.Orange
        Me.bnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnConsultar.ForeColor = System.Drawing.Color.Black
        Me.bnConsultar.Location = New System.Drawing.Point(868, 15)
        Me.bnConsultar.Name = "bnConsultar"
        Me.bnConsultar.Size = New System.Drawing.Size(136, 36)
        Me.bnConsultar.TabIndex = 1
        Me.bnConsultar.Text = "&CONSULTAR"
        Me.bnConsultar.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'campoClave
        '
        Me.campoClave.DataPropertyName = "clvCableModem"
        Me.campoClave.HeaderText = "Clave"
        Me.campoClave.Name = "campoClave"
        Me.campoClave.ReadOnly = True
        Me.campoClave.Visible = False
        '
        'campoStatus
        '
        Me.campoStatus.DataPropertyName = "status"
        Me.campoStatus.HeaderText = "Status"
        Me.campoStatus.Name = "campoStatus"
        Me.campoStatus.ReadOnly = True
        Me.campoStatus.Visible = False
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        Me.Contrato.Width = 70
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 200
        '
        'Colonia
        '
        Me.Colonia.DataPropertyName = "COLONIA"
        Me.Colonia.HeaderText = "Colonia"
        Me.Colonia.Name = "Colonia"
        Me.Colonia.ReadOnly = True
        Me.Colonia.Width = 110
        '
        'campoUbicacion
        '
        Me.campoUbicacion.DataPropertyName = "ubicacion"
        Me.campoUbicacion.HeaderText = "Ubicación"
        Me.campoUbicacion.Name = "campoUbicacion"
        Me.campoUbicacion.ReadOnly = True
        Me.campoUbicacion.Visible = False
        '
        'campoDescripcion
        '
        Me.campoDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.campoDescripcion.DataPropertyName = "Marca"
        Me.campoDescripcion.HeaderText = "Descripción"
        Me.campoDescripcion.Name = "campoDescripcion"
        Me.campoDescripcion.ReadOnly = True
        Me.campoDescripcion.Visible = False
        '
        'BrwCablemodems
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnConsultar)
        Me.Name = "BrwCablemodems"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Tipos de Aparatos"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.dgvAparatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents bnBuscarMac As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBNombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Clv_calleLabel2 As System.Windows.Forms.Label
    Friend WithEvents tbMac As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvAparatos As System.Windows.Forms.DataGridView
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents bnConsultar As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DescripcionListBox As System.Windows.Forms.ListBox
    Friend WithEvents bnBuscarNombre As System.Windows.Forms.Button
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents bnBuscarContrato As System.Windows.Forms.Button
    Friend WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents bnBuscarColonia As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbColonia As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents campoClave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents campoStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Colonia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents campoUbicacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents campoDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
