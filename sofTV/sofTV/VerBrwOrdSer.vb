Imports System.Data.SqlClient
Public Class VerBrwOrdSer
    Dim autom As Boolean
    Dim bndbuscastatus As Boolean = False
    Private Sub VerBrwOrdSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        'GloClv_TipSer = Me.ComboBox4.SelectedValue
        CON.Close()
        Busca(30)
        bndbuscastatus = True
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub



    Private Sub consultar()
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            If gloClave > 0 And Me.Clv_calleLabel2.Text > 0 Then
                opcion = "C"
                GloBnd = False
                gloClave = Me.Clv_calleLabel2.Text
                'GloClv_TipSer = Me.Label9.Text
                FrmOrdSer.Show()
            Else
                MsgBox(mensaje2)
            End If
        Else
            MsgBox("No tiene una Orden seleccionada", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Dim array As String() = ContratoLabel1.Text.Split("-")
            eContratoCompania = array(0)
            GloIdCompania = array(1)
            loccontratoordenes = contratobueno.Text
            consultar()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            GloBnd = False
            gloClave = Me.Clv_calleLabel2.Text
            'GloClv_TipSer = Me.ComboBox4.SelectedValue
            FrmOrdSer.Show()
        End If
    End Sub


    Private Sub Busca(ByVal op As Integer)
        Dim sTATUS As String = "P"
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            'If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
            If Me.RadioButton1.Checked = True Then
                sTATUS = "P"
            ElseIf Me.RadioButton2.Checked = True Then
                sTATUS = "E"
            ElseIf Me.RadioButton3.Checked = True Then
                sTATUS = "V"
            End If
            '(@Clv_TipSer int,@Clv_Orden bigint,@Contrato bigint,@NOMBRE VARCHAR(150),@CALLE VARCHAR (150),@NUMERO VARCHAR(150) ,@OP INT,@procauto bit, @idcompania int)
            BaseII.limpiaParametros()
            If op = 30 Then 'contrato

                If IsNumeric(GloContratoVer) = True Then
                    Me.BUSCAORDSERTableAdapter.Connection = CON
                    'Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, 0, GloContratoVer, sTATUS, "", "", 300, False)
                    BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContratoVer)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, sTATUS)
                    BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 300)
                    BaseII.CreateMyParameter("@procauto", SqlDbType.Bit, False)
                    'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 499 Then 'contrato
                Me.BUSCAORDSERTableAdapter.Connection = CON
                'Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, 0, GloContratoVer, sTATUS, "", "", 4990, False)
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContratoVer)
                BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, sTATUS)
                BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 4990)
                BaseII.CreateMyParameter("@procauto", SqlDbType.Bit, False)
                'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            ElseIf op = 31 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSCAORDSERTableAdapter.Connection = CON
                    'Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, 0, GloContratoVer, Me.TextBox2.Text, "", "", 310, False)
                    BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContratoVer)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, Me.TextBox2.Text)
                    BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 310)
                    BaseII.CreateMyParameter("@procauto", SqlDbType.Bit, False)
                    'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 32 Then 'Calle y numero
                Me.BUSCAORDSERTableAdapter.Connection = CON
                'Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, 0, GloContratoVer, "", Me.BCALLE.Text, Me.BNUMERO.Text, 320, False)
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContratoVer)
                BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, Me.BCALLE.Text)
                BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, Me.BNUMERO.Text)
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 320)
                BaseII.CreateMyParameter("@procauto", SqlDbType.Bit, False)
                'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            ElseIf op = 33 Then 'clv_Orden
                If IsNumeric(Me.TextBox3.Text) = True Then
                    Me.BUSCAORDSERTableAdapter.Connection = CON
                    'Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, Me.TextBox3.Text, GloContratoVer, "", "", "", 330, False)
                    BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, Me.TextBox3.Text)
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContratoVer)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 330)
                    BaseII.CreateMyParameter("@procauto", SqlDbType.Bit, False)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            Else
                Me.BUSCAORDSERTableAdapter.Connection = CON
                'Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, 0, 0, "", "", "", 340, False)
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 340)
                BaseII.CreateMyParameter("@procauto", SqlDbType.Bit, False)
                'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            End If
            'DataGridView1.Rows.Clear()
            DataGridView1.DataSource = BaseII.ConsultaDT("BUSCAORDSER")
            If DataGridView1.Rows.Count > 0 Then
                cambioseleccion()
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Me.TextBox3.Clear()
            Me.BNUMERO.Clear()
            Me.BCALLE.Clear()
            'End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub
    Private Sub cambioseleccion()
        Try
            Clv_calleLabel2.Text = DataGridView1.SelectedCells(0).Value
            ContratoLabel1.Text = DataGridView1.SelectedCells(2).Value
            NombreTextBox.Text = DataGridView1.SelectedCells(3).Value
            CALLELabel1.Text = DataGridView1.SelectedCells(4).Value
            NUMEROLabel1.Text = DataGridView1.SelectedCells(5).Value
        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(30)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(31)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(30)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(31)
        End If
    End Sub

    Private Sub BRWORDSER_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            'GloClv_TipSer = Me.ComboBox4.SelectedValue
            Busca(30)
        End If
    End Sub





    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    'Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.ComboBox4.SelectedIndex <> -1 Then
    '        GloClv_TipSer = Me.ComboBox4.SelectedValue
    '        Busca(30)
    '    End If
    'End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(33)
        End If
    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(33)
    End Sub

   


    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            If bndbuscastatus = True Then
                Busca(499)
            End If
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            If bndbuscastatus = True Then
                Busca(499)
            End If
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            If bndbuscastatus = True Then
                Busca(499)
            End If
        End If
    End Sub

    Private Sub ContratoLabel1_Click(sender As System.Object, e As System.EventArgs) Handles ContratoLabel1.Click

    End Sub

    Private Sub contratobueno_Click(sender As System.Object, e As System.EventArgs) Handles contratobueno.Click
        'Try
        '    Dim CON As New SqlConnection(MiConexion)
        '    Dim comando As New SqlCommand()
        '    CON.Open()
        '    comando.Connection = CON
        '    comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + ContratoLabel1.Text + " and idcompania=" + GloIdCompania.ToString()
        '    contratobueno.Text = comando.ExecuteScalar().ToString
        '    CON.Close()
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub ContratoLabel1_TextChanged(sender As System.Object, e As System.EventArgs) Handles ContratoLabel1.TextChanged
        Try
            Dim CON As New SqlConnection(MiConexion)
            Dim comando As New SqlCommand()
            CON.Open()
            comando.Connection = CON
            Dim array As String() = ContratoLabel1.Text.Split("-")
            comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + array(0) + " and idcompania=" + array(1)
            contratobueno.Text = comando.ExecuteScalar().ToString
            CON.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles DataGridView1.SelectionChanged
        cambioseleccion()
    End Sub
End Class