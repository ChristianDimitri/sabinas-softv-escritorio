<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelTrabajosE
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ConSelTrabajosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSelTrabajosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSelTrabajosTmpTableAdapter()
        Me.DescripcionListBox = New System.Windows.Forms.ListBox()
        Me.ConSelTrabajosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSelTrabajosTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSelTrabajosTableAdapter()
        Me.DescripcionListBox1 = New System.Windows.Forms.ListBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.InsertarSelTrabajosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarSelTrabajosTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarSelTrabajosTableAdapter()
        Me.EliminarSelTrabajosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EliminarSelTrabajosTableAdapter = New sofTV.DataSetEric2TableAdapters.EliminarSelTrabajosTableAdapter()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSelTrabajosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSelTrabajosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarSelTrabajosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EliminarSelTrabajosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConSelTrabajosTmpBindingSource
        '
        Me.ConSelTrabajosTmpBindingSource.DataMember = "ConSelTrabajosTmp"
        Me.ConSelTrabajosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSelTrabajosTmpTableAdapter
        '
        Me.ConSelTrabajosTmpTableAdapter.ClearBeforeFill = True
        '
        'DescripcionListBox
        '
        Me.DescripcionListBox.DataSource = Me.ConSelTrabajosTmpBindingSource
        Me.DescripcionListBox.DisplayMember = "Descripcion"
        Me.DescripcionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionListBox.FormattingEnabled = True
        Me.DescripcionListBox.ItemHeight = 18
        Me.DescripcionListBox.Location = New System.Drawing.Point(48, 52)
        Me.DescripcionListBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionListBox.Name = "DescripcionListBox"
        Me.DescripcionListBox.Size = New System.Drawing.Size(367, 364)
        Me.DescripcionListBox.TabIndex = 3
        Me.DescripcionListBox.ValueMember = "Clv_Trabajo"
        '
        'ConSelTrabajosBindingSource
        '
        Me.ConSelTrabajosBindingSource.DataMember = "ConSelTrabajos"
        Me.ConSelTrabajosBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSelTrabajosTableAdapter
        '
        Me.ConSelTrabajosTableAdapter.ClearBeforeFill = True
        '
        'DescripcionListBox1
        '
        Me.DescripcionListBox1.DataSource = Me.ConSelTrabajosBindingSource
        Me.DescripcionListBox1.DisplayMember = "Descripcion"
        Me.DescripcionListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionListBox1.FormattingEnabled = True
        Me.DescripcionListBox1.ItemHeight = 18
        Me.DescripcionListBox1.Location = New System.Drawing.Point(595, 52)
        Me.DescripcionListBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionListBox1.Name = "DescripcionListBox1"
        Me.DescripcionListBox1.Size = New System.Drawing.Size(367, 364)
        Me.DescripcionListBox1.TabIndex = 5
        Me.DescripcionListBox1.ValueMember = "Clv_Trabajo"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(459, 119)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 31)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(459, 158)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(85, 31)
        Me.Button2.TabIndex = 7
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(459, 304)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(85, 31)
        Me.Button3.TabIndex = 8
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(459, 342)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(85, 31)
        Me.Button4.TabIndex = 9
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(651, 489)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(185, 44)
        Me.Button5.TabIndex = 10
        Me.Button5.Text = "&CONTINUAR"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(844, 489)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(185, 44)
        Me.Button6.TabIndex = 11
        Me.Button6.Text = "&SALIR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'InsertarSelTrabajosBindingSource
        '
        Me.InsertarSelTrabajosBindingSource.DataMember = "InsertarSelTrabajos"
        Me.InsertarSelTrabajosBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarSelTrabajosTableAdapter
        '
        Me.InsertarSelTrabajosTableAdapter.ClearBeforeFill = True
        '
        'EliminarSelTrabajosBindingSource
        '
        Me.EliminarSelTrabajosBindingSource.DataMember = "EliminarSelTrabajos"
        Me.EliminarSelTrabajosBindingSource.DataSource = Me.DataSetEric2
        '
        'EliminarSelTrabajosTableAdapter
        '
        Me.EliminarSelTrabajosTableAdapter.ClearBeforeFill = True
        '
        'FrmSelTrabajosE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1045, 548)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DescripcionListBox1)
        Me.Controls.Add(Me.DescripcionListBox)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelTrabajosE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Servicios al Cliente"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSelTrabajosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSelTrabajosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarSelTrabajosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EliminarSelTrabajosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConSelTrabajosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSelTrabajosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConSelTrabajosTmpTableAdapter
    Friend WithEvents DescripcionListBox As System.Windows.Forms.ListBox
    Friend WithEvents ConSelTrabajosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSelTrabajosTableAdapter As sofTV.DataSetEric2TableAdapters.ConSelTrabajosTableAdapter
    Friend WithEvents DescripcionListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents InsertarSelTrabajosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarSelTrabajosTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarSelTrabajosTableAdapter
    Friend WithEvents EliminarSelTrabajosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EliminarSelTrabajosTableAdapter As sofTV.DataSetEric2TableAdapters.EliminarSelTrabajosTableAdapter
End Class
