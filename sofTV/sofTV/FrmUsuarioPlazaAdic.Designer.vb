﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmUsuarioPlazaAdic
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ComboBoxPlaza = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dgvcompania = New System.Windows.Forms.DataGridView()
        Me.IdCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Plaza = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Razon_Social = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ckbFac = New System.Windows.Forms.CheckBox()
        Me.ckbQueja = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        CType(Me.dgvcompania, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label11.Location = New System.Drawing.Point(14, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(88, 16)
        Me.Label11.TabIndex = 135
        Me.Label11.Text = "Distribuidor"
        '
        'ComboBoxPlaza
        '
        Me.ComboBoxPlaza.DisplayMember = "Nombre"
        Me.ComboBoxPlaza.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxPlaza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxPlaza.FormattingEnabled = True
        Me.ComboBoxPlaza.Location = New System.Drawing.Point(16, 41)
        Me.ComboBoxPlaza.Name = "ComboBoxPlaza"
        Me.ComboBoxPlaza.Size = New System.Drawing.Size(307, 23)
        Me.ComboBoxPlaza.TabIndex = 134
        Me.ComboBoxPlaza.ValueMember = "Clv_Plaza"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label9.Location = New System.Drawing.Point(15, 71)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(47, 16)
        Me.Label9.TabIndex = 139
        Me.Label9.Text = "Plaza"
        '
        'dgvcompania
        '
        Me.dgvcompania.AllowUserToAddRows = False
        Me.dgvcompania.AllowUserToDeleteRows = False
        Me.dgvcompania.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvcompania.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdCompania, Me.Plaza, Me.Razon_Social})
        Me.dgvcompania.Location = New System.Drawing.Point(6, 19)
        Me.dgvcompania.Name = "dgvcompania"
        Me.dgvcompania.ReadOnly = True
        Me.dgvcompania.RowHeadersVisible = False
        Me.dgvcompania.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvcompania.Size = New System.Drawing.Size(653, 258)
        Me.dgvcompania.TabIndex = 138
        '
        'IdCompania
        '
        Me.IdCompania.DataPropertyName = "idcompania"
        Me.IdCompania.HeaderText = "idcompania"
        Me.IdCompania.Name = "IdCompania"
        Me.IdCompania.ReadOnly = True
        Me.IdCompania.Visible = False
        '
        'Plaza
        '
        Me.Plaza.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Plaza.DataPropertyName = "Nombre"
        Me.Plaza.HeaderText = "Distribuidor"
        Me.Plaza.Name = "Plaza"
        Me.Plaza.ReadOnly = True
        '
        'Razon_Social
        '
        Me.Razon_Social.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Razon_Social.DataPropertyName = "razon_social"
        Me.Razon_Social.HeaderText = "Plaza"
        Me.Razon_Social.Name = "Razon_Social"
        Me.Razon_Social.ReadOnly = True
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(17, 91)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(306, 23)
        Me.ComboBoxCompanias.TabIndex = 137
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(329, 85)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(111, 32)
        Me.Button7.TabIndex = 136
        Me.Button7.Text = "AGREGAR"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(665, 19)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 38)
        Me.Button1.TabIndex = 140
        Me.Button1.Text = "QUITAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button7)
        Me.GroupBox1.Controls.Add(Me.ComboBoxPlaza)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.ComboBoxCompanias)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(453, 129)
        Me.GroupBox1.TabIndex = 142
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Plazas por asignar"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ckbFac)
        Me.GroupBox2.Controls.Add(Me.ckbQueja)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.GroupBox2.Location = New System.Drawing.Point(473, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(212, 106)
        Me.GroupBox2.TabIndex = 143
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Acceso a módulos"
        '
        'ckbFac
        '
        Me.ckbFac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbFac.ForeColor = System.Drawing.SystemColors.GrayText
        Me.ckbFac.Location = New System.Drawing.Point(46, 33)
        Me.ckbFac.Name = "ckbFac"
        Me.ckbFac.Size = New System.Drawing.Size(120, 24)
        Me.ckbFac.TabIndex = 23
        Me.ckbFac.Text = "Facturación"
        '
        'ckbQueja
        '
        Me.ckbQueja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbQueja.ForeColor = System.Drawing.SystemColors.GrayText
        Me.ckbQueja.Location = New System.Drawing.Point(46, 63)
        Me.ckbQueja.Name = "ckbQueja"
        Me.ckbQueja.Size = New System.Drawing.Size(99, 24)
        Me.ckbQueja.TabIndex = 24
        Me.ckbQueja.Text = "Reportes"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvcompania)
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 157)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(760, 289)
        Me.GroupBox3.TabIndex = 144
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Plazas adicionales asignadas al usuario"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(686, 452)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(86, 38)
        Me.Button2.TabIndex = 141
        Me.Button2.Text = "SALIR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'FrmUsuarioPlazaAdic
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(781, 501)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.MaximumSize = New System.Drawing.Size(797, 539)
        Me.MinimumSize = New System.Drawing.Size(797, 539)
        Me.Name = "FrmUsuarioPlazaAdic"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asignación de Distribuidores Externos a Usuarios"
        CType(Me.dgvcompania, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxPlaza As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dgvcompania As System.Windows.Forms.DataGridView
    Friend WithEvents IdCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Plaza As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Razon_Social As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ckbFac As System.Windows.Forms.CheckBox
    Friend WithEvents ckbQueja As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
