Imports System.Data.SqlClient
Imports System.Text

Public Class FrmRangosComisiones
    Private conexion As New SqlConnection(MiConexion)

    Private Sub Llena_companias()
        GloIdCompania = 0

        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                GloIdCompania = ComboBoxCompanias.SelectedValue
            End If

            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConComision(ByVal Clv_Servicio As Integer, ByVal Clv_Rango As Integer)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC CONCOMISION ")
        strSQL.Append(CStr(Clv_Servicio) & ", ")
        strSQL.Append(CStr(Clv_Rango) & ", ")
        strSQL.Append(CStr(GloIdCompania))


        Try
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()
            Me.DataGridView1.DataSource = bindingSource

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub NueComision(ByVal Clv_Grupo As Integer, ByVal Clv_Servicio As Integer, ByVal Clv_Rango As Integer, ByVal Comision As Single)
        Dim comando As New SqlCommand("NUECOMISION", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Grupo", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Grupo
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Servicio
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Rango", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Rango
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Comision", SqlDbType.Float)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Comision
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub BorComision(ByVal Clv_Servicio As Integer, ByVal Clv_Rango As Integer)
        Dim comando As New SqlCommand("BORCOMISION", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Servicio
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Rango", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Rango
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub MuestraTipServEric()
        Dim strSQl As New StringBuilder
        strSQl.Append("EXEC MuestraTipServEric 0,0")

        Try
            Dim dataAdapter As New SqlDataAdapter(strSQl.ToString, conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()
            Me.ComboBox1.DataSource = bindingSource
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MuestraServiciosEric(ByVal Clv_TipSer As Integer)
        Dim strSQl As New StringBuilder
        strSQl.Append("EXEC MuestraServiciosEric2 ")
        strSQl.Append(CStr(Clv_TipSer) & ",0,2, 1 ")

        Try
            Dim dataAdapter As New SqlDataAdapter(strSQl.ToString, conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()
            Me.ComboBox2.DataSource = bindingSource
            If ComboBox2.Items.Count = 0 Then
                ComboBox2.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConRangos(ByVal Clv_Rango As Integer, ByVal Op As Integer)
        Dim strSQl As New StringBuilder
        strSQl.Append("EXEC CONRANGOS ")
        strSQl.Append(CStr(Clv_Rango) & ", ")
        strSQl.Append(CStr(Op) & ", ")
        strSQl.Append(CStr(ComboBoxCompanias.SelectedValue))

        Try
            Dim dataAdapter As New SqlDataAdapter(strSQl.ToString, conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()
            Me.ComboBox3.DataSource = bindingSource

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ConCatalogosRangosPrecioBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConCatalogosRangosPrecioBindingNavigatorSaveItem.Click
        Guardar()
    End Sub

    Private Sub FrmRangosComisiones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Llena_companias()

        MuestraTipServEric()
        ConRangos(0, 0)
        eBndComisiones = False
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim r As MsgBoxResult = MsgBoxResult.No
        If eBndComisiones = True Then
            eBndComisiones = False
            r = MsgBox("Se han realizado cambios. �Deseas guardar?", MsgBoxStyle.YesNo)
            If r = MsgBoxResult.Yes Then
                Guardar()
            End If
        End If
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        BorComision(CInt(Me.ComboBox2.SelectedValue), CInt(Me.ComboBox3.SelectedValue))
        MsgBox(mensaje6, MsgBoxStyle.Information)
        ConComision(CInt(Me.ComboBox2.SelectedValue), CInt(Me.ComboBox3.SelectedValue))
        eBndComisiones = False
    End Sub


    Private Sub ComboBox1_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedValueChanged
        Dim r As MsgBoxResult = MsgBoxResult.No
        If eBndComisiones = True Then
            eBndComisiones = False
            r = MsgBox("Se han realizado cambios. �Deseas guardar?", MsgBoxStyle.YesNo)
            If r = MsgBoxResult.Yes Then
                Guardar()
            End If
        End If
        MuestraServiciosEric(CInt(Me.ComboBox1.SelectedValue))
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Dim r As MsgBoxResult = MsgBoxResult.No
        If eBndComisiones = True Then
            eBndComisiones = False
            r = MsgBox("Se han realizado cambios. �Deseas guardar?", MsgBoxStyle.YesNo)
            If r = MsgBoxResult.Yes Then
                Guardar()
            End If
        End If
        ConComision(CInt(Me.ComboBox2.SelectedValue), CInt(Me.ComboBox3.SelectedValue))
    End Sub

    Private Sub ComboBox3_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedValueChanged
        Dim r As MsgBoxResult = MsgBoxResult.No
        If eBndComisiones = True Then
            eBndComisiones = False
            r = MsgBox("Se han realizado cambios. �Deseas guardar?", MsgBoxStyle.YesNo)
            If r = MsgBoxResult.Yes Then
                Guardar()
            End If
        End If
        ConComision(CInt(Me.ComboBox2.SelectedValue), CInt(Me.ComboBox3.SelectedValue))
    End Sub

    Private Sub DataGridView1_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellEndEdit
        eBndComisiones = True
    End Sub

    Private Sub Guardar()
        eBndComisiones = False
        Dim x As Integer

        For x = 0 To Me.DataGridView1.RowCount - 1
            NueComision(Me.DataGridView1.Item(1, x).Value, Me.DataGridView1.Item(2, x).Value, Me.DataGridView1.Item(3, x).Value, Me.DataGridView1.Item(4, x).Value)
        Next
        MsgBox(mensaje5, MsgBoxStyle.Information)
        ConComision(CInt(Me.ComboBox2.SelectedValue), CInt(Me.ComboBox3.SelectedValue))
        eBndComisiones = False
    End Sub


    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        If GloIdCompania = 0 Then
            Exit Sub
        End If
        GloIdCompania = ComboBoxCompanias.SelectedValue
        MuestraServiciosEric(CInt(Me.ComboBox1.SelectedValue))
        ConRangos(0, 0)
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub ComboBoxCompanias_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedValueChanged

    End Sub
End Class