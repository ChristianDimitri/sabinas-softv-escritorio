﻿Public Class FrmUsuarioPlazaAdic

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
    Private Sub Llena_Plaza()
        BaseII.limpiaParametros()
        ComboBoxPlaza.DataSource = BaseII.ConsultaDT("Muestra_PlazasFrmUsuario")
        If ComboBoxPlaza.Items.Count <> 0 Then
            ComboBoxPlaza.SelectedIndex = 0
        End If
    End Sub

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, gloClave)
            BaseII.CreateMyParameter("@clvplaza", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_FrmUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            Else
                ComboBoxCompanias.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llena_dgvcompanias()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        dgvcompania.DataSource = BaseII.ConsultaDT("AgregaEliminaCompaniaAdicUsuario")
    End Sub
    Dim fac As Boolean = 0
    Dim quejas As Boolean = 0
    Private Sub FrmUsuarioPlazaAdic_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Llena_Plaza()
        Llena_companias()
        llena_dgvcompanias()
        If opcion = "C" Then
            Button7.Enabled = False
            Button1.Enabled = False
            ckbFac.Enabled = False
            ckbQueja.Enabled = False
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clave", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@fac", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@queja", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("ObtieneChecksCompaniaAdic")
        fac = BaseII.dicoPar("@fac")
        quejas = BaseII.dicoPar("@queja")
        ckbFac.Checked = fac
        ckbQueja.Checked = quejas
        UspDesactivaBotones(Me, Me.Name)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        If ComboBoxCompanias.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
        BaseII.Inserta("AgregaEliminaCompaniaAdicUsuario")
        llena_dgvcompanias()
        Llena_companias()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If dgvcompania.Rows.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, dgvcompania.SelectedCells(0).Value)
        BaseII.Inserta("AgregaEliminaCompaniaAdicUsuario")
        llena_dgvcompanias()
        Llena_companias()
    End Sub

    Private Sub FrmUsuarioPlazaAdic_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            If fac <> ckbFac.Checked Or quejas <> ckbQueja.Checked Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clave", SqlDbType.Int, gloClave)
                BaseII.CreateMyParameter("@fac", SqlDbType.Bit, ckbFac.Checked)
                BaseII.CreateMyParameter("@queja", SqlDbType.Bit, ckbQueja.Checked)
                BaseII.Inserta("GuardaOpcionesCompaniaAdic")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxPlaza_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxPlaza.SelectedIndexChanged
        Try
            Llena_companias()
        Catch ex As Exception

        End Try
    End Sub
End Class