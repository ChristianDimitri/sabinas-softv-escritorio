Imports System.Data.SqlClient
Public Class FrmBuscaOrdSer

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Dim Fila As Integer = 0
            Fila = DataGridView1.CurrentRow.Index

        
            If IsNumeric(Me.DataGridView1.Item(0, Fila).Value) = True Then
                GLOCLV_oRDENbus = Me.DataGridView1.Item(0, Fila).Value
                Me.Close()
            Else
                MsgBox("Seleccione la Orden ", MsgBoxStyle.Information)
            End If
        Catch ex As Exception

        End Try
    End Sub



    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim sTATUS As String = "P"
        Try
            If IsNumeric(GloClv_TipSer) = True Then
                If op = 0 Then 'contrato
                    If IsNumeric(Me.TextBox1.Text) = True Then
                        'Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                        'Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, 0, Me.TextBox1.Text, "", "", "", op)
                        BusquedaOrdSerDynamic(0, GloPendientes, GloEjecutadas, GloVisita, 0, Me.TextBox1.Text, "", "", "", op, GloIdCompania)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 1 Then
                    If Len(Trim(Me.TextBox2.Text)) > 0 Then
                        'Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                        'Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox2.Text, "", "", op)
                        BusquedaOrdSerDynamic(0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox2.Text, "", "", op, GloIdCompania)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 2 Then 'Calle y numero
                    'Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                    'Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, op)
                    BusquedaOrdSerDynamic(0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, op, GloIdCompania)
                ElseIf op = 3 Then 'clv_Orden
                    If IsNumeric(Me.TextBox3.Text) = True Then
                        'Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                        'Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, Me.TextBox3.Text, 0, "", "", "", op)
                        BusquedaOrdSerDynamic(0, GloPendientes, GloEjecutadas, GloVisita, Me.TextBox3.Text, 0, "", "", "", op, GloIdCompania)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                Else
                    'Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                    'Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox1.Text, "", "", 4)
                    BusquedaOrdSerDynamic(0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox1.Text, "", "", 4, GloIdCompania)
                End If
                Me.TextBox1.Clear()
                Me.TextBox2.Clear()
                Me.TextBox3.Clear()
                Me.BNUMERO.Clear()
                Me.BCALLE.Clear()
            Else
                MsgBox("Seleccione el Tipo de Servicio")
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub BusquedaOrdSerDynamic(ByVal Clv_tipser As Integer, ByVal StatusPen As Integer, ByVal StatusEje As Integer, ByVal StatusVis As Integer, ByVal Clv_Orden As Integer,
                                       ByVal Contrato As Integer, ByVal Nombre As String, ByVal Calle As String, ByVal Numero As String, ByVal Op As Integer, ByVal Compa�ia As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_tipser", SqlDbType.Int, Clv_tipser)
        BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, StatusPen)
        BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, StatusEje)
        BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, StatusVis)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, Clv_Orden)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Nombre, 150)
        BaseII.CreateMyParameter("@Calle", SqlDbType.VarChar, Calle, 150)
        BaseII.CreateMyParameter("@Numero", SqlDbType.VarChar, Numero, 150)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Compania", SqlDbType.Int, Compa�ia)
        BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
        DataGridView1.DataSource = BaseII.ConsultaDT("BusquedaOrdSerDynamic")

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If ContratoCompania.Text.Length = 0 Then
            Exit Sub
        End If
        Try
            Dim conexion As New SqlConnection(MiConexion)
            Dim array As String() = ContratoCompania.Text.Trim.Split("-")
            Dim comando As New SqlCommand()
            conexion.Open()
            comando.Connection = conexion
            comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + array(0) + " and idcompania=" + array(1) + " and idcompania in (select idcompania from Rel_Usuario_Compania where Clave=" + GloClvUsuario.ToString() + ")"
            TextBox1.Text = comando.ExecuteScalar().ToString()
            conexion.Close()
            Busca(0)
        Catch ex As Exception
            MsgBox("Contrato no v�lido. Vuelve a intentarlo.")
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub FrmBuscaOrdSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        CON.Close()
        Busca(4)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub



    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(3)
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Clv_calleLabel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub



    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(2)
    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub BCALLE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCALLE.TextChanged

    End Sub

    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub BNUMERO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNUMERO.TextChanged

    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        Try

        
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value.ToString) = True Then
            GLOCLV_oRDENbus = Me.DataGridView1.SelectedCells(0).Value.ToString
            Me.Close()
        Else
            MsgBox("Seleccione la Orden ", MsgBoxStyle.Information)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    End Sub



    Private Sub ContratoCompania_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ContratoCompania.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If ContratoCompania.Text.Length = 0 Then
                Exit Sub
            End If
            Try
                Dim conexion As New SqlConnection(MiConexion)
                Dim array As String() = ContratoCompania.Text.Trim.Split("-")
                Dim comando As New SqlCommand()
                conexion.Open()
                comando.Connection = conexion
                comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + array(0) + " and idcompania=" + array(1) + " and idcompania in (select idcompania from Rel_Usuario_Compania where Clave=" + GloClvUsuario.ToString() + ")"
                TextBox1.Text = comando.ExecuteScalar().ToString()
                conexion.Close()
                Busca(0)
            Catch ex As Exception
                MsgBox("Contrato no v�lido. Vuelve a intentarlo.")
            End Try
        End If
    End Sub


End Class