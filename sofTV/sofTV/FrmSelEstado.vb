Imports System.Data.SqlClient

Public Class FrmSelEstado

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim COn As New SqlConnection(MiConexion)

        If Me.CMBCheckBox1.Checked = False And Me.CMBCheckBox2.Checked = False And Me.CMBCheckBox3.Checked = False And Me.CMBCheckBox4.Checked = False And Me.CMBCheckBox5.Checked = False And Me.CMBCheckBox6.Checked = False And Me.CMBCheckBox7.Checked = False Then
            MsgBox("Primero selecciona minimo un tipo de Status", MsgBoxStyle.Information)
        Else
            If Me.CMBCheckBox8.CheckState = CheckState.Checked Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    If Me.CMBCheckBox8.CheckState = CheckState.Checked And (Len(Me.TextBox1.Text) = 0 Or Me.TextBox1.Text = "") Then
                        MsgBox("No Se Ha Introducido un A�o", MsgBoxStyle.Information)
                        Exit Sub
                    Else
                        If Me.CaracterComboBox.Text = "" Then
                            MsgBox("No Se Ha Seleccionado El Rango de Busqueda", MsgBoxStyle.Information)
                            Exit Sub
                        Else
                            COn.Open()
                            Me.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter.Connection = COn
                            Me.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaRel_Reporte_Ciudad_Ultimo_mes, LocClv_session, CInt(Me.ComboBox1.SelectedValue), CInt(Me.TextBox1.Text), Me.CaracterComboBox.SelectedValue)
                            COn.Close()
                        End If
                    End If
                Else
                    MsgBox("No Se Ha Introducido un A�o", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If Me.CMBCheckBox2.CheckState = CheckState.Checked Then
                If Me.ComboBox2.Text = "" Then
                    MsgBox("No Se Ha Seleccionado El Tipo De Cancelacion")
                    Exit Sub
                Else
                    COn.Open()
                    Me.Inserta_Rel_Motcan_Rep_CiudadTableAdapter.Connection = COn
                    Me.Inserta_Rel_Motcan_Rep_CiudadTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_Rel_Motcan_Rep_Ciudad, LocClv_session, Me.ComboBox2.SelectedValue)
                    COn.Close()
                    Inserta_FecCan_Repciudad()
                End If
            End If



            If LocValidaHab = 0 Then
                If LocOp = 10 Or LocOp = 20 Or LocOp = 30 Or LocOp = 22 Or LocOp = 25 Then
                    'My.Forms.FrmSelPeriodo.Show()
                    Locreportcity = True
                    Me.Close()
                End If
            Else
                If LocOp = 10 Then
                    LocServicios = True
                    Me.Close()
                ElseIf LocOp = 20 Or LocOp = 25 Then
                    Locreportcity = True
                    oprepetiq = 0
                    Me.Close()
                ElseIf LocOp = 22 Then
                    Locreportcity = True
                    oprepetiq = 0
                    Me.Close()
                ElseIf LocOp = 30 Then
                    LocBndRepMix = True
                    LocGloOpRep = 2
                    FrmImprimirFac.Show()
                    Me.Close()
                End If

            End If
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox1.CheckedChanged
        If Me.CMBCheckBox1.CheckState = CheckState.Checked Then
            LocBndC = True
            'Me.Panel2.Visible = True
        Else
            LocBndC = False
            'If Me.CMBCheckBox2.CheckState = CheckState.Unchecked And Me.CMBCheckBox3.CheckState = CheckState.Unchecked Then

            'End If
        End If
    End Sub

    Private Sub Inserta_FecCan_Repciudad()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fec_Ini", SqlDbType.Date, DateTimePicker1.Text)
        BaseII.CreateMyParameter("@Fec_Fin", SqlDbType.Date, DateTimePicker2.Text)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, LocClv_session)
        BaseII.Inserta("Inserta_FecCan_Repciudad")
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox2.CheckedChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.CMBCheckBox2.CheckState = CheckState.Checked Then
            LocBndB = True
            Me.CMBPanel3.Enabled = True
            Me.Muestra_MotCanc_ReporteTableAdapter.Connection = CON
            Me.Muestra_MotCanc_ReporteTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_MotCanc_Reporte)
            'Me.Panel2.Visible = True
        Else
            Me.CMBPanel3.Enabled = False
            LocBndB = False
            'If Me.CMBCheckBox1.CheckState = CheckState.Unchecked And Me.CMBCheckBox3.CheckState = CheckState.Unchecked Then
            '    ' Me.Panel2.Visible = False
            'End If
            CON.Close()
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox3.CheckedChanged
        If Me.CMBCheckBox3.CheckState = CheckState.Checked Then
            LocBndI = True
            'Me.Panel2.Visible = True
        Else
            LocBndI = False
            'If Me.CMBCheckBox1.CheckState = CheckState.Unchecked And Me.CMBCheckBox2.CheckState = CheckState.Unchecked Then
            '    'Me.Panel2.Visible = False
            'End If
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox4.CheckedChanged
        If Me.CMBCheckBox4.CheckState = CheckState.Checked Then
            LocBndD = True
        Else
            LocBndD = False
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox5.CheckedChanged
        If Me.CMBCheckBox5.CheckState = CheckState.Checked Then
            LocBndS = True
        Else
            LocBndS = False
        End If
    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox6.CheckedChanged
        If Me.CMBCheckBox6.CheckState = CheckState.Checked Then
            LocBndF = True
        Else
            LocBndF = False
        End If
    End Sub

    Private Sub FrmSelEstado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'TODO: esta l�nea de c�digo carga datos en la tabla 'ProcedimientosArnoldo2.Muestra_Meses' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_MesesTableAdapter.Connection = CON
        Me.Muestra_MesesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Meses)
        CON.Close()
        colorea(Me, Me.Name)
        LocBndC = False
        LocBndB = False
        LocBndI = False
        LocBndD = False
        LocBndS = False
        LocBndF = False
        LocBndDT = False
        Me.CMBCheckBox7.Visible = True
        If LocOp = 30 Or LocOp = 25 Then
            Me.Button1.Visible = False
        End If
        'If IdSistema = "VA" Then
        '    Me.Button4.Visible = True
        'Else
        '    Me.Button4.Visible = False
        'End If
        'If GloClv_tipser2 = "2" Or GloClv_tipser2 = "3" Then
        '    Me.CheckBox4.Visible = False
        'End If
    End Sub

    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox7.CheckedChanged
        If Me.CMBCheckBox7.CheckState = CheckState.Checked Then
            LocBndDT = True
        Else
            LocBndDT = False
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.CMBCheckBox1.Checked = False And Me.CMBCheckBox2.Checked = False And Me.CMBCheckBox3.Checked = False And Me.CMBCheckBox4.Checked = False Then
            MsgBox("Primero selecciona minimo un tipo de Status", MsgBoxStyle.Information)
        Else
            If LocValidaHab = 0 Then
                If LocOp = 10 Or LocOp = 20 Or LocOp = 22 Then
                    My.Forms.FrmSelPeriodo.Show()
                    Me.Close()
                End If
            Else
                If LocOp = 20 Or LocOp = 22 Then
                    Locreportcity = True
                    oprepetiq = 1
                End If
                Me.Close()
            End If
        End If
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel1.Click

    End Sub

    Private Sub CheckBox8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox8.CheckedChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.CMBCheckBox8.CheckState = CheckState.Checked Then
            Me.CMBPanel2.Enabled = True
            Me.Muestra_CaracteresTableAdapter.Connection = CON
            Me.Muestra_CaracteresTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Caracteres, 0)
            Me.CaracterComboBox.Text = ""
        Else
            Me.CMBPanel2.Enabled = False
        End If
        CON.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
    End Sub




    'Private Sub FillToolStripButton_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '   Try
    '     Me.Muestra_CaracteresTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Caracteres, New System.Nullable(Of Integer)(CType(OpToolStripTextBox.Text, Integer)))
    'Catch ex As System.Exception
    'System.Windows.Forms.MessageBox.Show(ex.Message)
    'End Try

    'End Sub



    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim COn As New SqlConnection(MiConexion)

        If Me.CMBCheckBox1.Checked = False And Me.CMBCheckBox2.Checked = False And Me.CMBCheckBox3.Checked = False And Me.CMBCheckBox4.Checked = False And Me.CMBCheckBox5.Checked = False And Me.CMBCheckBox6.Checked = False And Me.CMBCheckBox7.Checked = False Then
            MsgBox("Primero selecciona m�nimo un tipo de Status", MsgBoxStyle.Information)
        Else
            If Me.CMBCheckBox8.CheckState = CheckState.Checked Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    If Me.CMBCheckBox8.CheckState = CheckState.Checked And (Len(Me.TextBox1.Text) = 0 Or Me.TextBox1.Text = "") Then
                        MsgBox("No Se Ha Introducido un A�o", MsgBoxStyle.Information)
                        Exit Sub
                    Else
                        If Me.CaracterComboBox.Text = "" Then
                            MsgBox("No Se Ha Seleccionado El Rango de B�squeda", MsgBoxStyle.Information)
                            Exit Sub
                        Else
                            COn.Open()
                            Me.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter.Connection = COn
                            Me.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaRel_Reporte_Ciudad_Ultimo_mes, LocClv_session, CInt(Me.ComboBox1.SelectedValue), CInt(Me.TextBox1.Text), Me.CaracterComboBox.SelectedValue)
                            COn.Close()
                        End If
                    End If
                Else
                    MsgBox("No Se Ha Introducido un A�o", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If Me.CMBCheckBox2.CheckState = CheckState.Checked Then
                If Me.ComboBox2.Text = "" Then
                    MsgBox("No Se Ha Seleccionado El Tipo De Cancelaci�n")
                    Exit Sub
                Else
                    COn.Open()
                    Me.Inserta_Rel_Motcan_Rep_CiudadTableAdapter.Connection = COn
                    Me.Inserta_Rel_Motcan_Rep_CiudadTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_Rel_Motcan_Rep_Ciudad, LocClv_session, Me.ComboBox2.SelectedValue)
                    COn.Close()
                End If
            End If

            If LocValidaHab = 0 Then
                If LocOp = 20 Then
                    bec_bnd = True
                    FrmSelPeriodo.Show()
                    Me.Close()
                End If

            End If
        End If
    End Sub
End Class