﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRedes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BuscaRedBtn = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.numA = New System.Windows.Forms.NumericUpDown()
        Me.numB = New System.Windows.Forms.NumericUpDown()
        Me.numC = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.mask = New System.Windows.Forms.NumericUpDown()
        Me.numD = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.seleccionPlaza = New System.Windows.Forms.ListBox()
        Me.quitartodo = New System.Windows.Forms.Button()
        Me.agregartodo = New System.Windows.Forms.Button()
        Me.quitar = New System.Windows.Forms.Button()
        Me.agregar = New System.Windows.Forms.Button()
        Me.loquehayPlaza = New System.Windows.Forms.ListBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.quitarTodoM = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.seleccion = New System.Windows.Forms.ListBox()
        Me.quitarM = New System.Windows.Forms.Button()
        Me.agregarTodoM = New System.Windows.Forms.Button()
        Me.agregarM = New System.Windows.Forms.Button()
        Me.loquehay = New System.Windows.Forms.ListBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        CType(Me.numA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mask, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'BuscaRedBtn
        '
        Me.BuscaRedBtn.BackColor = System.Drawing.Color.DarkOrange
        Me.BuscaRedBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BuscaRedBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BuscaRedBtn.ForeColor = System.Drawing.Color.Black
        Me.BuscaRedBtn.Location = New System.Drawing.Point(475, 54)
        Me.BuscaRedBtn.Name = "BuscaRedBtn"
        Me.BuscaRedBtn.Size = New System.Drawing.Size(88, 23)
        Me.BuscaRedBtn.TabIndex = 58
        Me.BuscaRedBtn.Text = "Guardar"
        Me.BuscaRedBtn.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(26, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 16)
        Me.Label3.TabIndex = 59
        Me.Label3.Text = "IP Red :"
        '
        'numA
        '
        Me.numA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.numA.Location = New System.Drawing.Point(92, 24)
        Me.numA.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.numA.Name = "numA"
        Me.numA.Size = New System.Drawing.Size(84, 21)
        Me.numA.TabIndex = 61
        '
        'numB
        '
        Me.numB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.numB.Location = New System.Drawing.Point(196, 24)
        Me.numB.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.numB.Name = "numB"
        Me.numB.Size = New System.Drawing.Size(84, 21)
        Me.numB.TabIndex = 62
        '
        'numC
        '
        Me.numC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.numC.Location = New System.Drawing.Point(308, 24)
        Me.numC.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.numC.Name = "numC"
        Me.numC.Size = New System.Drawing.Size(84, 21)
        Me.numC.TabIndex = 65
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(500, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 29)
        Me.Label1.TabIndex = 66
        Me.Label1.Text = "/"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(176, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(20, 29)
        Me.Label2.TabIndex = 67
        Me.Label2.Text = "."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(390, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(20, 29)
        Me.Label4.TabIndex = 70
        Me.Label4.Text = "."
        '
        'mask
        '
        Me.mask.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mask.Location = New System.Drawing.Point(527, 24)
        Me.mask.Maximum = New Decimal(New Integer() {32, 0, 0, 0})
        Me.mask.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.mask.Name = "mask"
        Me.mask.Size = New System.Drawing.Size(84, 21)
        Me.mask.TabIndex = 69
        Me.mask.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'numD
        '
        Me.numD.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.numD.Location = New System.Drawing.Point(410, 24)
        Me.numD.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.numD.Name = "numD"
        Me.numD.Size = New System.Drawing.Size(84, 21)
        Me.numD.TabIndex = 68
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(282, 18)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(20, 29)
        Me.Label5.TabIndex = 71
        Me.Label5.Text = "."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(3, 12)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(142, 16)
        Me.Label6.TabIndex = 148
        Me.Label6.Text = "Plazas Disponibles"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.seleccionPlaza)
        Me.Panel1.Controls.Add(Me.quitartodo)
        Me.Panel1.Controls.Add(Me.agregartodo)
        Me.Panel1.Controls.Add(Me.quitar)
        Me.Panel1.Controls.Add(Me.agregar)
        Me.Panel1.Controls.Add(Me.loquehayPlaza)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Location = New System.Drawing.Point(5, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(648, 286)
        Me.Panel1.TabIndex = 149
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(398, 12)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(121, 16)
        Me.Label7.TabIndex = 155
        Me.Label7.Text = "Plazas Incluidas"
        '
        'seleccionPlaza
        '
        Me.seleccionPlaza.DisplayMember = "Nombre"
        Me.seleccionPlaza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.seleccionPlaza.FormattingEnabled = True
        Me.seleccionPlaza.ImeMode = System.Windows.Forms.ImeMode.Katakana
        Me.seleccionPlaza.ItemHeight = 16
        Me.seleccionPlaza.Location = New System.Drawing.Point(396, 34)
        Me.seleccionPlaza.Name = "seleccionPlaza"
        Me.seleccionPlaza.Size = New System.Drawing.Size(245, 244)
        Me.seleccionPlaza.TabIndex = 154
        Me.seleccionPlaza.ValueMember = "Clv_Plaza"
        '
        'quitartodo
        '
        Me.quitartodo.BackColor = System.Drawing.Color.DarkRed
        Me.quitartodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.quitartodo.ForeColor = System.Drawing.Color.White
        Me.quitartodo.Location = New System.Drawing.Point(256, 195)
        Me.quitartodo.Name = "quitartodo"
        Me.quitartodo.Size = New System.Drawing.Size(134, 30)
        Me.quitartodo.TabIndex = 153
        Me.quitartodo.Text = "<< Quitar To&do "
        Me.quitartodo.UseVisualStyleBackColor = False
        '
        'agregartodo
        '
        Me.agregartodo.BackColor = System.Drawing.Color.DarkRed
        Me.agregartodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.agregartodo.ForeColor = System.Drawing.Color.White
        Me.agregartodo.Location = New System.Drawing.Point(256, 121)
        Me.agregartodo.Name = "agregartodo"
        Me.agregartodo.Size = New System.Drawing.Size(134, 30)
        Me.agregartodo.TabIndex = 152
        Me.agregartodo.Text = "Agregar &Todo >>"
        Me.agregartodo.UseVisualStyleBackColor = False
        '
        'quitar
        '
        Me.quitar.BackColor = System.Drawing.Color.DarkRed
        Me.quitar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.quitar.ForeColor = System.Drawing.Color.White
        Me.quitar.Location = New System.Drawing.Point(256, 158)
        Me.quitar.Name = "quitar"
        Me.quitar.Size = New System.Drawing.Size(134, 30)
        Me.quitar.TabIndex = 151
        Me.quitar.Text = "< &Quitar"
        Me.quitar.UseVisualStyleBackColor = False
        '
        'agregar
        '
        Me.agregar.BackColor = System.Drawing.Color.DarkRed
        Me.agregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.agregar.ForeColor = System.Drawing.Color.White
        Me.agregar.Location = New System.Drawing.Point(256, 85)
        Me.agregar.Name = "agregar"
        Me.agregar.Size = New System.Drawing.Size(134, 30)
        Me.agregar.TabIndex = 150
        Me.agregar.Text = "&Agregar >"
        Me.agregar.UseVisualStyleBackColor = False
        '
        'loquehayPlaza
        '
        Me.loquehayPlaza.DisplayMember = "Nombre"
        Me.loquehayPlaza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.loquehayPlaza.FormattingEnabled = True
        Me.loquehayPlaza.ItemHeight = 16
        Me.loquehayPlaza.Location = New System.Drawing.Point(6, 34)
        Me.loquehayPlaza.Name = "loquehayPlaza"
        Me.loquehayPlaza.Size = New System.Drawing.Size(245, 244)
        Me.loquehayPlaza.TabIndex = 149
        Me.loquehayPlaza.ValueMember = "Clv_Plaza"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(569, 54)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(88, 23)
        Me.Button3.TabIndex = 152
        Me.Button3.Text = "Lista de IP"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(29, 83)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(669, 326)
        Me.TabControl1.TabIndex = 153
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(661, 300)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabPage1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(661, 308)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.quitarTodoM)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.seleccion)
        Me.Panel3.Controls.Add(Me.quitarM)
        Me.Panel3.Controls.Add(Me.agregarTodoM)
        Me.Panel3.Controls.Add(Me.agregarM)
        Me.Panel3.Controls.Add(Me.loquehay)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Location = New System.Drawing.Point(5, 6)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(648, 293)
        Me.Panel3.TabIndex = 150
        '
        'quitarTodoM
        '
        Me.quitarTodoM.BackColor = System.Drawing.Color.DarkRed
        Me.quitarTodoM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.quitarTodoM.ForeColor = System.Drawing.Color.White
        Me.quitarTodoM.Location = New System.Drawing.Point(256, 195)
        Me.quitarTodoM.Name = "quitarTodoM"
        Me.quitarTodoM.Size = New System.Drawing.Size(134, 30)
        Me.quitarTodoM.TabIndex = 158
        Me.quitarTodoM.Text = "<< Quitar To&do "
        Me.quitarTodoM.UseVisualStyleBackColor = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label10.Location = New System.Drawing.Point(398, 12)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(125, 16)
        Me.Label10.TabIndex = 155
        Me.Label10.Text = "Medios Incluidos"
        '
        'seleccion
        '
        Me.seleccion.DisplayMember = "Nombre"
        Me.seleccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.seleccion.FormattingEnabled = True
        Me.seleccion.ImeMode = System.Windows.Forms.ImeMode.Katakana
        Me.seleccion.ItemHeight = 16
        Me.seleccion.Location = New System.Drawing.Point(396, 34)
        Me.seleccion.Name = "seleccion"
        Me.seleccion.Size = New System.Drawing.Size(245, 244)
        Me.seleccion.TabIndex = 154
        Me.seleccion.ValueMember = "Clv_Medio"
        '
        'quitarM
        '
        Me.quitarM.BackColor = System.Drawing.Color.DarkRed
        Me.quitarM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.quitarM.ForeColor = System.Drawing.Color.White
        Me.quitarM.Location = New System.Drawing.Point(256, 158)
        Me.quitarM.Name = "quitarM"
        Me.quitarM.Size = New System.Drawing.Size(134, 30)
        Me.quitarM.TabIndex = 156
        Me.quitarM.Text = "< &Quitar"
        Me.quitarM.UseVisualStyleBackColor = False
        '
        'agregarTodoM
        '
        Me.agregarTodoM.BackColor = System.Drawing.Color.DarkRed
        Me.agregarTodoM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.agregarTodoM.ForeColor = System.Drawing.Color.White
        Me.agregarTodoM.Location = New System.Drawing.Point(256, 121)
        Me.agregarTodoM.Name = "agregarTodoM"
        Me.agregarTodoM.Size = New System.Drawing.Size(134, 30)
        Me.agregarTodoM.TabIndex = 157
        Me.agregarTodoM.Text = "Agregar &Todo >>"
        Me.agregarTodoM.UseVisualStyleBackColor = False
        '
        'agregarM
        '
        Me.agregarM.BackColor = System.Drawing.Color.DarkRed
        Me.agregarM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.agregarM.ForeColor = System.Drawing.Color.White
        Me.agregarM.Location = New System.Drawing.Point(256, 85)
        Me.agregarM.Name = "agregarM"
        Me.agregarM.Size = New System.Drawing.Size(134, 30)
        Me.agregarM.TabIndex = 150
        Me.agregarM.Text = "&Agregar >"
        Me.agregarM.UseVisualStyleBackColor = False
        '
        'loquehay
        '
        Me.loquehay.DisplayMember = "Nombre"
        Me.loquehay.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.loquehay.FormattingEnabled = True
        Me.loquehay.ItemHeight = 16
        Me.loquehay.Location = New System.Drawing.Point(6, 34)
        Me.loquehay.Name = "loquehay"
        Me.loquehay.Size = New System.Drawing.Size(245, 244)
        Me.loquehay.TabIndex = 149
        Me.loquehay.ValueMember = "Clv_Medio"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label11.Location = New System.Drawing.Point(3, 12)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(146, 16)
        Me.Label11.TabIndex = 148
        Me.Label11.Text = "Medios Disponibles"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(575, 415)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(120, 36)
        Me.Button1.TabIndex = 152
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(431, 415)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 36)
        Me.Button2.TabIndex = 153
        Me.Button2.Text = "&GUARDAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'FrmRedes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(719, 461)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.mask)
        Me.Controls.Add(Me.numD)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.numC)
        Me.Controls.Add(Me.numB)
        Me.Controls.Add(Me.numA)
        Me.Controls.Add(Me.BuscaRedBtn)
        Me.Controls.Add(Me.Label3)
        Me.MaximizeBox = False
        Me.Name = "FrmRedes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nueva red"
        CType(Me.numA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mask, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BuscaRedBtn As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents numA As System.Windows.Forms.NumericUpDown
    Friend WithEvents numB As System.Windows.Forms.NumericUpDown
    Friend WithEvents numC As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents mask As System.Windows.Forms.NumericUpDown
    Friend WithEvents numD As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents seleccionPlaza As System.Windows.Forms.ListBox
    Friend WithEvents loquehayPlaza As System.Windows.Forms.ListBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents quitartodo As System.Windows.Forms.Button
    Friend WithEvents agregartodo As System.Windows.Forms.Button
    Friend WithEvents quitar As System.Windows.Forms.Button
    Friend WithEvents agregar As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents seleccion As System.Windows.Forms.ListBox
    Friend WithEvents agregarM As System.Windows.Forms.Button
    Friend WithEvents loquehay As System.Windows.Forms.ListBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents quitarTodoM As System.Windows.Forms.Button
    Friend WithEvents quitarM As System.Windows.Forms.Button
    Friend WithEvents agregarTodoM As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
End Class
