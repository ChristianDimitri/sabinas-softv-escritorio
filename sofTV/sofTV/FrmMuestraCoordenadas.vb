﻿Imports System.Data.SqlClient

Public Class FrmMuestraCoordenadas
    Public clv_servicioR As Integer = 0
    Public Clv_UnicaNetR As Integer = 0
    Public IdCoordenadaR As Integer = 0

    Private Sub Label3_Click(sender As System.Object, e As System.EventArgs) Handles Label3.Click

    End Sub

    Private Sub FrmMuestraCoordenadas_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'MuestraServiciosBase2ContratacionInternet(GloClv_TipSer, GloIdCompania)
            CONSULTACONTNETCOORDENADAS()
            'MuestraProveedorBase2()
            If IsNumeric(clv_servicioR) Then
                MuestraBeamBase2(clv_servicioR)
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CONSULTACONTNETCOORDENADAS()
        clv_servicioR = 0
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Clv_orden", SqlDbType.Int, gloClv_Orden)
        BaseII.CreateMyParameter("@DetClv_Orden", SqlDbType.Int, GloDetClave)
        dTable = BaseII.ConsultaDT("CONSULTACONTNETCOORDENADAS")
        If dTable.Rows.Count = 0 Then Exit Sub
        tBoxLongitud.Text = dTable.Rows(0)(0).ToString
        tBoxLatitud.Text = dTable.Rows(0)(1).ToString
        TextBox1.Text = dTable.Rows(0)(2).ToString
        clv_servicioR = dTable.Rows(0)(3).ToString
        Clv_UnicaNetR = dTable.Rows(0)(4).ToString
        IdCoordenadaR = dTable.Rows(0)(5).ToString
        'Fec_EjeTextBox.Enabled = False
    End Sub



    Private Sub MuestraBeamBase2(ByVal clv_servicio As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", SqlDbType.BigInt, GloIdCompania)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, clv_servicio)
        ComboBoxBeam.Text = ""
        ComboBoxBeam.DataSource = BaseII.ConsultaDT("sp_dameBeamPorServicio")

    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_UnicaNet", SqlDbType.BigInt, Clv_UnicaNetR)
        BaseII.CreateMyParameter("@Id_Beam_Paquete", SqlDbType.BigInt, ComboBoxBeam.SelectedValue)
        BaseII.Inserta("sp_actualizaBeamCliente")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdCoordenada", SqlDbType.BigInt, IdCoordenadaR)
        BaseII.CreateMyParameter("@Clv_orden", SqlDbType.BigInt, gloClv_Orden)
        BaseII.CreateMyParameter("@DetalleOrden", SqlDbType.BigInt, GloDetClave)
        BaseII.CreateMyParameter("@Proveedor", SqlDbType.BigInt, 1)
        BaseII.Inserta("sp_actualizaRelContnetCoordenadas")

        Me.Close()
    End Sub


    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class