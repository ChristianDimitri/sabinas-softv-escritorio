Imports System.Data.SqlClient

Public Class FrmrRelPaquetesdelCliente
    Private eContratoNet As Long = 0

    Public Sub CREAARBOL()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcion, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 14)
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcion.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                Me.TreeView1.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                If GLOTRABAJO = "IPAQU" Or GLOTRABAJO = "IPAQUT" Then
                    Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 1, gloClv_Orden, GloDetClave)
                ElseIf GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAAD" Or GLOTRABAJO = "BSEDI" Or GLOTRABAJO = "BPAQF" Then
                    Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 10, gloClv_Orden, GloDetClave)
                ElseIf GLOTRABAJO = "DPAQU" Or GLOTRABAJO = "DPAQT" Or GLOTRABAJO = "DPAQF" Then
                    Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 20, gloClv_Orden, GloDetClave)
                ElseIf GLOTRABAJO = "RPAQU" Or GLOTRABAJO = "RPAQT" Then
                    Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 21, gloClv_Orden, GloDetClave)
                End If
                Me.TreeView1.Nodes(I).ForeColor = Color.Black
                For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion.Rows
                    Me.TreeView1.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " : " & Trim(FilacontNet("STATUS").ToString()))
                    Me.TreeView1.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                    If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Olive
                    ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Navy
                    Else
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Red
                    End If
                    X += 1
                Next
                I += 1
            Next
            CON.Close()
            Me.TreeView1.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Sub CREAARBOL2()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.MUESTRAIPAQU_porSOLTableAdapter.Connection = CON
            Me.MUESTRAIPAQU_porSOLTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAIPAQU_porSOL, GloDetClave, gloClv_Orden, 0)
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView2.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.MUESTRAIPAQU_porSOL.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 2, gloClv_Orden, GloDetClave)
                Me.TreeView2.Nodes(I).ForeColor = Color.Black
                For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion.Rows
                    Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " : " & Trim(FilacontNet("STATUS").ToString()))
                    Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                    If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                    ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                    Else
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                    End If
                    X += 1
                Next
                I += 1
            Next
            CON.Close()
            Me.TreeView2.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Me.Close()
    End Sub

    Private Sub FrmrRelPaquetesdelCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        CON.Close()
        GloBndTrabajo = True
        GloBloqueaDetalle = True
    End Sub

    Private Sub FrmrRelPaquetesdelCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CREAARBOL()
        Me.CREAARBOL2()
        If GLOTRABAJO = "IPAQU" Or GLOTRABAJO = "IPAQUT" Then
            Me.Text = "Instalación de Servicios de Internet"
            Me.Label3.Text = "Servicios de internet Pendientes de Instalar"
            Me.Label4.Text = "Instalar estos Servicios de Internet"
        ElseIf GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAQF" Then
            Me.Text = "Baja de Servicios de Internet"
            Me.Label3.Text = "Servicios de internet Activos"
            Me.Label4.Text = "Pasar a Baja estos Servicios de Internet"
        ElseIf GLOTRABAJO = "RPAQU" Or GLOTRABAJO = "RPAQT" Then
            Me.Text = "Reconexión de Servicios de Internet"
            Me.Label3.Text = "Servicios de internet Suspendidos"
            Me.Label4.Text = "Activar estos Servicios de Internet"
        ElseIf GLOTRABAJO = "DPAQU" Or GLOTRABAJO = "DPAQT" Or GLOTRABAJO = "DPAQF" Then
            Me.Text = "Desconexión de Servicios de Internet"
            Me.Label3.Text = "Servicios de internet Activos"
            Me.Label4.Text = "Suspender estos Servicios de Internet"
        ElseIf GLOTRABAJO = "BPAAD" Then
            Me.Text = "Baja de Paquetes Adicionales"
            Me.Label3.Text = "Paquetes Adicionales Activos"
            Me.Label4.Text = "Pasar a Baja estos Paquetes Adicionales"
        ElseIf GLOTRABAJO = "BSEDI" Then
            Me.Text = "Baja de Servicios Digitales"
            Me.Label3.Text = "Servicios Digitales Activos"
            Me.Label4.Text = "Pasar a Baja estos Servicios Digitales"
        End If
        If Bloquea = True Or opcion = "M" Then
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
            Me.Button3.Enabled = False
            Me.Button4.Enabled = False
        End If
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet.Text = e.Node.Tag
                    MacCableModem.Text = e.Node.Text
                    Clv_Unicanet.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Clv_Unicanet.Text = e.Node.Tag
                    'If IsNumeric(Me.Contratonet.Text) = True Then
                    If eClv_TipSer = 5 Then
                        DameContratoNet2(CLng(Me.Clv_Unicanet.Text), eClv_TipSer)
                        Me.Contratonet.Text = eContratoNet
                    Else
                        Dim CON As New SqlConnection(MiConexion)
                        eContratoNet = 0
                        CON.Open()
                        Me.DameContratoNetTableAdapter.Connection = CON
                        Me.DameContratoNetTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNet, Me.Clv_Unicanet.Text, eContratoNet)
                        CON.Close()
                        Me.Contratonet.Text = eContratoNet
                    End If
                    'End If

                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
            End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.Clv_Unicanet.Text) = False Then Me.Clv_Unicanet.Text = 0
            If Me.Clv_Unicanet.Text > 0 Then
                If GLOTRABAJO = "IPAQU" Or GLOTRABAJO = "IPAQUT" Then
                    Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "I")
                ElseIf GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAAD" Or GLOTRABAJO = "BSEDI" Or GLOTRABAJO = "BPAQF" Then
                    Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "B")
                    Me.GuardaMotivoCanServTableAdapter.Connection = CON
                    Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 2, Me.Contratonet.Text, 0, 0)
                ElseIf GLOTRABAJO = "DPAQU" Or GLOTRABAJO = "DPAQT" Or GLOTRABAJO = "DPAQF" Then
                    Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "S")
                ElseIf GLOTRABAJO = "RPAQU" Or GLOTRABAJO = "RPAQT" Then
                    Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "I")
                End If
            Else
                If IsNumeric(Me.Contratonet.Text) = False Then Me.Contratonet.Text = 0
                If Me.Contratonet.Text > 0 Then
                    If GLOTRABAJO = "IPAQU" Or GLOTRABAJO = "IPAQUT" Then
                        Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                        Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 1, "I")
                    ElseIf GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAAD" Or GLOTRABAJO = "BSEDI" Or GLOTRABAJO = "BPAQF" Then
                        Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                        Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 3, "B")
                        Me.GuardaMotivoCanServTableAdapter.Connection = CON
                        Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 2, Me.Contratonet.Text, 0, 0)
                    ElseIf GLOTRABAJO = "DPAQU" Or GLOTRABAJO = "DPAQT" Or GLOTRABAJO = "DPAQF" Then
                        Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                        Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 3, "S")
                    ElseIf GLOTRABAJO = "RPAQU" Or GLOTRABAJO = "RPAQT" Then
                        Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                        Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 33, "I")
                    End If
                End If
            End If
            CON.Close()
            Me.CREAARBOL2()
            Me.Contratonet.Text = 0
            Me.MacCableModem.Text = ""
            Me.Clv_Unicanet.Text = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            If gloClv_Orden > 0 And GloDetClave > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()

                If GLOTRABAJO = "IPAQU" Or GLOTRABAJO = "IPAQUT" Then
                    Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, 0, 0, 2, "I")
                ElseIf GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAAD" Or GLOTRABAJO = "BSEDI" Or GLOTRABAJO = "BPAQF" Then
                    Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, 0, 0, 4, "B")
                    Me.GuardaMotivoCanServTableAdapter.Connection = CON
                    Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 2, 0, 0, 1)


                ElseIf GLOTRABAJO = "DPAQU" Or GLOTRABAJO = "DPAQT" Or GLOTRABAJO = "DPAQF" Then
                    Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, 0, 0, 4, "S")
                ElseIf GLOTRABAJO = "RPAQU" Or GLOTRABAJO = "RPAQT" Then
                    Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, 0, 0, 44, "I")
                End If
                CON.Close()
                Me.CREAARBOL2()
            End If
            Me.Contratonet.Text = 0
            Me.MacCableModem.Text = ""
            Me.Clv_Unicanet.Text = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.Clv_Unicanet1.Text) = False Then Me.Clv_Unicanet1.Text = 0
            If Me.Clv_Unicanet1.Text > 0 Then
                Me.BorIPAQU_SOLTableAdapter.Connection = CON
                Me.BorIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet1.Text, Me.Clv_Unicanet1.Text, 0)
                If GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAQF" Then
                    Me.BorraMotivoCanServTableAdapter.Connection = CON
                    Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, gloClv_Orden, 2, Me.Contratonet1.Text, 0, 0)
                End If
            Else
                If Me.Contratonet1.Text > 0 Then
                    Me.BorIPAQU_SOLTableAdapter.Connection = CON
                    Me.BorIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet1.Text, Me.Clv_Unicanet1.Text, 1)
                    If GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAQF" Then
                        Me.BorraMotivoCanServTableAdapter.Connection = CON
                        Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, gloClv_Orden, 2, Me.Contratonet1.Text, 0, 0)
                    End If
                End If
            End If
            CON.Close()
            Me.CREAARBOL2()
            Me.Contratonet1.Text = 0
            Me.MacCableModem1.Text = ""
            Me.Clv_Unicanet1.Text = 0
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet1.Text = e.Node.Tag
                    MacCableModem1.Text = e.Node.Text
                    Clv_Unicanet1.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))                    
                    Clv_Unicanet1.Text = e.Node.Tag
                    'If IsNumeric(Me.Contratonet1.Text) = True Then

                    If eClv_TipSer = 5 Then
                        DameContratoNet2(CLng(Me.Clv_Unicanet1.Text), eClv_TipSer)
                        Me.Contratonet1.Text = eContratoNet
                    Else
                        Dim CON As New SqlConnection(MiConexion)
                        eContratoNet = 0
                        CON.Open()
                        Me.DameContratoNetTableAdapter.Connection = CON
                        Me.DameContratoNetTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNet, Me.Clv_Unicanet1.Text, eContratoNet)
                        CON.Close()
                        Me.Contratonet1.Text = eContratoNet
                    End If

                    'End If
                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
            End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorIPAQU_SOLTableAdapter.Connection = CON
            Me.BorIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borIPAQU_SOL, GloDetClave, gloClv_Orden, 0, 0, 2)
            If GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAQF" Then
                Me.BorraMotivoCanServTableAdapter.Connection = CON
                Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, gloClv_Orden, 2, 0, 0, 1)
            End If
            CON.Close()
            Me.CREAARBOL2()
            Me.Contratonet1.Text = 0
            Me.MacCableModem1.Text = ""
            Me.Clv_Unicanet1.Text = 0
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub TreeView1_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseClick
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet.Text = e.Node.Tag
                    MacCableModem.Text = e.Node.Text
                    Clv_Unicanet.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Clv_Unicanet.Text = e.Node.Tag
                    'If IsNumeric(Me.Contratonet.Text) = True Then

                    If eClv_TipSer = 5 Then
                        DameContratoNet2(CLng(Me.Clv_Unicanet.Text), eClv_TipSer)
                        Me.Contratonet.Text = eContratoNet
                        'Else
                        Dim CON As New SqlConnection(MiConexion)
                        eContratoNet = 0
                        CON.Open()
                        Me.DameContratoNetTableAdapter.Connection = CON
                        Me.DameContratoNetTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNet, Me.Clv_Unicanet.Text, eContratoNet)
                        CON.Close()
                        Me.Contratonet.Text = eContratoNet
                        'End If

                        'MacCableModem.Text = e.Node.Text
                        '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                        'Else
                        '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                        'End If
                        'Else
                        'If IsNumeric(e.Node.Tag) Then
                        '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                        'Else
                        '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                        'End If
                    End If
            End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView2_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView2.NodeMouseClick
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet1.Text = e.Node.Tag
                    MacCableModem1.Text = e.Node.Text
                    Clv_Unicanet1.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))                    
                    Clv_Unicanet1.Text = e.Node.Tag
                    If IsNumeric(Me.Contratonet1.Text) = True Then

                        If eClv_TipSer = 5 Then
                            DameContratoNet2(CLng(Me.Clv_Unicanet1.Text), eClv_TipSer)
                            Me.Contratonet1.Text = eContratoNet
                        Else
                            Dim CON As New SqlConnection(MiConexion)
                            eContratoNet = 0
                            CON.Open()
                            Me.DameContratoNetTableAdapter.Connection = CON
                            Me.DameContratoNetTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNet, Me.Clv_Unicanet1.Text, eContratoNet)
                            CON.Close()
                            Me.Contratonet1.Text = eContratoNet
                        End If
                        
                    End If
                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DameContratoNet2(ByVal Clv_UnicaNet As Long, ByVal Clv_TipSer As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameContratoNet2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contratonet", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            eContratoNet = CLng(parametro2.Value.ToString)
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


End Class