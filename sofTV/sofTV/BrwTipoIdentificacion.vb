﻿Public Class BrwTipoIdentificacion

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Public Sub obtenerTipoIdentificacion()
        BaseII.limpiaParametros()
        DataGridView1.DataSource = BaseII.ConsultaDT("Sp_listadoTipoIdentificacion")
    End Sub

    Private Sub BrwVelInternet_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            obtenerTipoIdentificacion()
        End If
    End Sub

    Private Sub BrwVelInternet_Load(sender As Object, e As EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        obtenerTipoIdentificacion()
    End Sub


    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            lbClvEq.Text = DataGridView1.SelectedCells(1).Value
            
        Catch ex As Exception

        End Try

    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        opcion = "N"
        gloClave = 0
        FrmTipoIdentificacion.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If IsNumeric(DataGridView1.SelectedCells(0).Value) Then
            opcion = "C"
            gloClave = DataGridView1.SelectedCells(0).Value
            FrmTipoIdentificacion.Show()
        Else
            MsgBox("Selecciona un elemento válido")
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If IsNumeric(DataGridView1.SelectedCells(0).Value) Then
            opcion = "M"
            gloClave = DataGridView1.SelectedCells(0).Value
            FrmTipoIdentificacion.Show()
        Else
            MsgBox("Selecciona un elemento válido")
        End If

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@TipoIden", SqlDbType.VarChar, TextBox1.Text, 100)
        DataGridView1.DataSource = BaseII.ConsultaDT("Sp_filtroTipoIdentificacion")
    End Sub
End Class