﻿Public Class FrmTipoIdentificacion
    Private Sub damePolitica()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@TipoIden", ParameterDirection.Output, SqlDbType.VarChar, 100)


        BaseII.ProcedimientoOutPut("Sp_dameTipoIdentificacion")
        Clv_EqTextBox.Text = BaseII.dicoPar("@TipoIden").ToString()


    End Sub





    Private Sub FrmVelInternet_Load(sender As Object, e As EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        If opcion = "C" Or opcion = "M" Then
            damePolitica()
            If opcion = "C" Then
                GroupBox1.Enabled = False
                ToolStrip1.Enabled = False
            End If
        Else
            Clv_EqTextBox.Enabled = True
        End If


    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, gloClave)
        BaseII.Inserta("Sp_eliminaTipoIdentificacion")
    End Sub

    Private Sub CONMotivoCancelacionBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles CONMotivoCancelacionBindingNavigatorSaveItem.Click
        If Len(Clv_EqTextBox.Text) > 0 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id", SqlDbType.Int, gloClave)
            BaseII.CreateMyParameter("@tipoIden", SqlDbType.VarChar, Clv_EqTextBox.Text, 100)
            BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 200)
            BaseII.ProcedimientoOutPut("Sp_guardaTipoIdentificacion")
            MsgBox(BaseII.dicoPar("@Msj").ToString())
            GloBnd = True
            Me.Close()
        Else
            MsgBox("Datos incompletos")
            Exit Sub
        End If


    End Sub


    Private Sub VelBajTextBox_KeyPress(sender As Object, e As KeyPressEventArgs)
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros 
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub VelSubTextBox_KeyPress(sender As Object, e As KeyPressEventArgs)
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros 
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
End Class