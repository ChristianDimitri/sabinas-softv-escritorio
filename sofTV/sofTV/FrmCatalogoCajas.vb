﻿Imports System.Data.SqlClient
Public Class FrmCatalogoCajas
    Private descripcion As String = Nothing
    Private ipmaq As String = Nothing
    Private sucursal As String = Nothing
    Private imprtickets As String = Nothing
    Private factnormal As String = Nothing
    Private factticket As String = Nothing
    Private ImprTermi As Integer = 0
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                descripcion = Me.DescripcionTextBox.Text
                ipmaq = Me.TextBox1.Text
                sucursal = Me.ComboBox1.Text
                If Me.ImprimeTicketsCheckBox.CheckState = CheckState.Checked Then
                    imprtickets = "True"
                Else
                    imprtickets = "False"
                End If
                If Me.RadioButton1.Checked = True Then
                    factnormal = "True"
                Else
                    factnormal = "False"
                End If
                If Me.RadioButton2.Checked = True Then
                    factticket = "True"
                Else
                    factticket = "False"
                End If
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Try
            Dim validacion1 As String = Nothing
            Dim validacion2 As String = Nothing
            Dim validacion3 As String = Nothing

            Select Case op
                Case 0
                    If opcion = "M" Then
                        'descripcion = Me.DescripcionTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DescripcionTextBox.Name, descripcion, Me.DescripcionTextBox.Text, LocClv_Ciudad)
                        'ipmaq = Me.TextBox1.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "ipMaquina, " + Me.DescripcionTextBox.Text, ipmaq, Me.TextBox1.Text, LocClv_Ciudad)
                        'sucursal = Me.ComboBox1.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "sucursal, " + Me.DescripcionTextBox.Text, sucursal, Me.ComboBox1.Text, LocClv_Ciudad)
                        'Imprime ticket
                        If Me.ImprimeTicketsCheckBox.CheckState = CheckState.Checked Then
                            'imprtickets = "True"
                            validacion1 = "True"
                        Else
                            'imprtickets = "False"
                            validacion1 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Imprimetickets, " + Me.DescripcionTextBox.Text, imprtickets, validacion1, LocClv_Ciudad)
                        'facturanormal
                        If Me.RadioButton1.Checked = True Then
                            'factnormal = "True"
                            validacion2 = "True"
                        Else
                            'factnormal = "False"
                            validacion2 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "facturanormal, " + Me.DescripcionTextBox.Text, factnormal, validacion2, LocClv_Ciudad)
                        'facturatickets

                        If Me.RadioButton2.Checked = True Then
                            'factticket = "True"
                            validacion3 = "True"
                        Else
                            'factticket = "False"
                            validacion3 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "facturatickets, " + Me.DescripcionTextBox.Text, factticket, validacion3, LocClv_Ciudad)


                    ElseIf opcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Agrego una nueva Caja", "", "Agrego Una Nueva Caja: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Elimino Una Caja", "", "Elimino Una Caja: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONCatalogoCajasTableAdapter.Connection = CON
        Me.CONCatalogoCajasTableAdapter.Delete(gloClave)
        guardabitacora(1)
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONCatalogoCajasBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub CONCatalogoCajasBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCatalogoCajasBindingNavigatorSaveItem.Click
        If ComboBox1.SelectedIndex = -1 Then
            MsgBox("Seleccione la Sucursal")
            Exit Sub
        End If
        If DescripcionTextBox.TextLength = 0 Then
            MsgBox("Capture el nombre de la Caja")
            Exit Sub
        End If
        If TextBox1.TextLength = 0 Then
            MsgBox("Capture el nombre de la IP de la Caja o el Nombre de la Maquina")
            Exit Sub
        End If

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.Validate()
            Me.CONCatalogoCajasBindingSource.EndEdit()
            Me.CONCatalogoCajasTableAdapter.Connection = CON
            Me.CONCatalogoCajasTableAdapter.Update(Me.NewSofTvDataSet.CONCatalogoCajas)
            usp_ActualzaImpresoraTermica()
            MsgBox(mensaje5)
            guardabitacora(0)
            GloBnd = True
            Me.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show("La Ip que capturo ya Exíste en el Catalogo ")
        End Try
        CON.Close()

    End Sub

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_caja", SqlDbType.Int, CLAVE)
            BaseII.CreateMyParameter("@clv_sucursal", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@idcompania", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("ObtieneInformacionCajas")
            ComboBoxCompanias.SelectedValue = BaseII.dicoPar("@idcompania")
            Me.CONCatalogoCajasTableAdapter.Connection = CON
            Me.CONCatalogoCajasTableAdapter.Fill(Me.NewSofTvDataSet.CONCatalogoCajas, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            damedatosbitacora()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub FrmCatalogoCajas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Llena_companias()
        'ComboBoxCompanias.SelectedValue = GloIdCompania
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRASUCURSALES' Puede moverla o quitarla según sea necesario.
        'Me.MUESTRASUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRASUCURSALES)
        Me.MUESTRASUCURSALES2TableAdapter.Connection = CON
        Me.MUESTRASUCURSALES2TableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRASUCURSALES2, GloIdCompania)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRATIPOUSUARIOS' Puede moverla o quitarla según sea necesario.
        If opcion = "N" Then
            ComboBoxCompanias.SelectedValue = GloIdCompania
            Me.CONCatalogoCajasBindingSource.AddNew()

            If Me.ImprimeTicketsCheckBox.CheckState = CheckState.Indeterminate Then
                Me.ImprimeTicketsCheckBox.CheckState = CheckState.Unchecked
            End If

            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            ComboBoxCompanias.Enabled = False
            BUSCA(gloClave)
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            ComboBoxCompanias.Enabled = False
            BUSCA(gloClave)
        End If
        CON.Close()

        usp_ImpresoraTermica()
        If ImprTermi = 1 Then
            ChBoxTermi.Checked = True
        Else
            ChBoxTermi.Checked = False
        End If
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.Clv_sucursalTextBox.Text = Me.ComboBox1.SelectedValue
    End Sub

    Private Sub DescripcionTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DescripcionTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.DescripcionTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub DescripcionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescripcionTextBox.TextChanged

    End Sub

    Private Sub ImprimeTicketsCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImprimeTicketsCheckBox.CheckedChanged

    End Sub

    Private Sub usp_ImpresoraTermica()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_caja", SqlDbType.BigInt, CInt(Me.ClaveTextBox.Text))
        BaseII.CreateMyParameter("@ImpTermi", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Usp_ImpresoraTermica")
        ImprTermi = CInt(BaseII.dicoPar("@ImpTermi").ToString)

    End Sub

    Private Sub usp_ActualzaImpresoraTermica()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_caja", SqlDbType.BigInt, CInt(Me.ClaveTextBox.Text))
        BaseII.CreateMyParameter("@ImpTermi", SqlDbType.BigInt, CInt(Me.ChBoxTermi.Checked))
        BaseII.ProcedimientoOutPut("Usp_actualizaImpresoraTermica")

    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            'If opcion = "N" Then

            GloIdCompania = ComboBoxCompanias.SelectedValue
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MUESTRASUCURSALES2TableAdapter.Connection = CON
            Me.MUESTRASUCURSALES2TableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRASUCURSALES2, GloIdCompania)
            CON.Close()
            'End If
        Catch ex As Exception

        End Try
       

    End Sub
End Class