﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwRedes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.buscaRedTxt = New System.Windows.Forms.TextBox()
        Me.BuscaRedBtn = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvRedes = New System.Windows.Forms.DataGridView()
        Me.IdRed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IPRed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Mask = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        CType(Me.dgvRedes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'buscaRedTxt
        '
        Me.buscaRedTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buscaRedTxt.Location = New System.Drawing.Point(59, 19)
        Me.buscaRedTxt.Name = "buscaRedTxt"
        Me.buscaRedTxt.Size = New System.Drawing.Size(165, 21)
        Me.buscaRedTxt.TabIndex = 57
        '
        'BuscaRedBtn
        '
        Me.BuscaRedBtn.BackColor = System.Drawing.Color.DarkOrange
        Me.BuscaRedBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BuscaRedBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BuscaRedBtn.ForeColor = System.Drawing.Color.Black
        Me.BuscaRedBtn.Location = New System.Drawing.Point(230, 18)
        Me.BuscaRedBtn.Name = "BuscaRedBtn"
        Me.BuscaRedBtn.Size = New System.Drawing.Size(88, 23)
        Me.BuscaRedBtn.TabIndex = 55
        Me.BuscaRedBtn.Text = "&Buscar"
        Me.BuscaRedBtn.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 15)
        Me.Label3.TabIndex = 56
        Me.Label3.Text = "Red :"
        '
        'dgvRedes
        '
        Me.dgvRedes.AllowUserToAddRows = False
        Me.dgvRedes.AllowUserToDeleteRows = False
        Me.dgvRedes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRedes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdRed, Me.IPRed, Me.Mask})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRedes.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvRedes.Location = New System.Drawing.Point(15, 52)
        Me.dgvRedes.MultiSelect = False
        Me.dgvRedes.Name = "dgvRedes"
        Me.dgvRedes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRedes.Size = New System.Drawing.Size(410, 561)
        Me.dgvRedes.TabIndex = 54
        '
        'IdRed
        '
        Me.IdRed.DataPropertyName = "IdRed"
        Me.IdRed.HeaderText = "Id"
        Me.IdRed.Name = "IdRed"
        Me.IdRed.Width = 90
        '
        'IPRed
        '
        Me.IPRed.DataPropertyName = "IPRed"
        Me.IPRed.FillWeight = 200.0!
        Me.IPRed.HeaderText = "IP Red"
        Me.IPRed.Name = "IPRed"
        Me.IPRed.Width = 200
        '
        'Mask
        '
        Me.Mask.DataPropertyName = "Mask"
        Me.Mask.HeaderText = "Mask"
        Me.Mask.Name = "Mask"
        Me.Mask.Width = 70
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(439, 577)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(120, 36)
        Me.Button1.TabIndex = 64
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Orange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(439, 136)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(120, 36)
        Me.Button6.TabIndex = 63
        Me.Button6.Text = "&MODIFICAR"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.Orange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(439, 94)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(120, 36)
        Me.Button7.TabIndex = 62
        Me.Button7.Text = "&CONSULTAR"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(439, 52)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(120, 36)
        Me.Button3.TabIndex = 67
        Me.Button3.Text = "&NUEVO"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'BrwRedes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(569, 620)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.buscaRedTxt)
        Me.Controls.Add(Me.BuscaRedBtn)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dgvRedes)
        Me.MaximizeBox = False
        Me.Name = "BrwRedes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Redes"
        CType(Me.dgvRedes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents buscaRedTxt As System.Windows.Forms.TextBox
    Friend WithEvents BuscaRedBtn As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvRedes As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents IdRed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IPRed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mask As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
