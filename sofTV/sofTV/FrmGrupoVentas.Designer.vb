<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGrupoVentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_GrupoLabel As System.Windows.Forms.Label
        Dim GrupoLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ConGrupoVentasDataGridView = New System.Windows.Forms.DataGridView()
        Me.ConGrupoVentas1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.GrupoTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_GrupoTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.ConGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.ConGrupoVentasTableAdapter()
        Me.NueGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.NueGrupoVentasTableAdapter()
        Me.ModGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.ModGrupoVentasTableAdapter()
        Me.BorGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.BorGrupoVentasTableAdapter()
        Me.ConGrupoVentas1TableAdapter = New sofTV.DataSetEric2TableAdapters.ConGrupoVentas1TableAdapter()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Activo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Clv_GrupoLabel = New System.Windows.Forms.Label()
        GrupoLabel = New System.Windows.Forms.Label()
        CType(Me.ConGrupoVentasDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGrupoVentas1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ConGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_GrupoLabel
        '
        Clv_GrupoLabel.AutoSize = True
        Clv_GrupoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_GrupoLabel.Location = New System.Drawing.Point(130, 34)
        Clv_GrupoLabel.Name = "Clv_GrupoLabel"
        Clv_GrupoLabel.Size = New System.Drawing.Size(46, 15)
        Clv_GrupoLabel.TabIndex = 11
        Clv_GrupoLabel.Text = "Clave:"
        '
        'GrupoLabel
        '
        GrupoLabel.AutoSize = True
        GrupoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        GrupoLabel.Location = New System.Drawing.Point(128, 64)
        GrupoLabel.Name = "GrupoLabel"
        GrupoLabel.Size = New System.Drawing.Size(50, 15)
        GrupoLabel.TabIndex = 12
        GrupoLabel.Text = "Grupo:"
        '
        'ConGrupoVentasDataGridView
        '
        Me.ConGrupoVentasDataGridView.AllowUserToAddRows = False
        Me.ConGrupoVentasDataGridView.AllowUserToDeleteRows = False
        Me.ConGrupoVentasDataGridView.AutoGenerateColumns = False
        Me.ConGrupoVentasDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConGrupoVentasDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConGrupoVentasDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.Activo})
        Me.ConGrupoVentasDataGridView.DataSource = Me.ConGrupoVentas1BindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConGrupoVentasDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConGrupoVentasDataGridView.Location = New System.Drawing.Point(39, 170)
        Me.ConGrupoVentasDataGridView.Name = "ConGrupoVentasDataGridView"
        Me.ConGrupoVentasDataGridView.ReadOnly = True
        Me.ConGrupoVentasDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConGrupoVentasDataGridView.Size = New System.Drawing.Size(454, 268)
        Me.ConGrupoVentasDataGridView.TabIndex = 2
        Me.ConGrupoVentasDataGridView.TabStop = False
        '
        'ConGrupoVentas1BindingSource
        '
        Me.ConGrupoVentas1BindingSource.DataMember = "ConGrupoVentas1"
        Me.ConGrupoVentas1BindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(79, 119)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "&Nuevo"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Enabled = False
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(188, 31)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(131, 21)
        Me.TextBox1.TabIndex = 5
        Me.TextBox1.TabStop = False
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(174, 119)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(86, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "&Modificar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.ComboBoxCompanias)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Clv_GrupoLabel)
        Me.Panel1.Controls.Add(GrupoLabel)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.GrupoTextBox)
        Me.Panel1.Controls.Add(Me.Clv_GrupoTextBox)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Controls.Add(Me.ConGrupoVentasDataGridView)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Location = New System.Drawing.Point(1, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(529, 503)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 15)
        Me.Label1.TabIndex = 209
        Me.Label1.Text = "Plaza :"
        Me.Label1.Visible = False
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(68, 34)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(34, 23)
        Me.ComboBoxCompanias.TabIndex = 208
        Me.ComboBoxCompanias.Visible = False
        '
        'TextBox2
        '
        Me.TextBox2.Enabled = False
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(188, 58)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(252, 21)
        Me.TextBox2.TabIndex = 1
        '
        'Button6
        '
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(390, 457)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 7
        Me.Button6.Text = "&SALIR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(263, 119)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(86, 23)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "&Guardar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'GrupoTextBox
        '
        Me.GrupoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGrupoVentas1BindingSource, "Grupo", True))
        Me.GrupoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrupoTextBox.Location = New System.Drawing.Point(189, 58)
        Me.GrupoTextBox.Name = "GrupoTextBox"
        Me.GrupoTextBox.Size = New System.Drawing.Size(252, 21)
        Me.GrupoTextBox.TabIndex = 2
        '
        'Clv_GrupoTextBox
        '
        Me.Clv_GrupoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGrupoVentas1BindingSource, "Clv_Grupo", True))
        Me.Clv_GrupoTextBox.Enabled = False
        Me.Clv_GrupoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_GrupoTextBox.Location = New System.Drawing.Point(189, 31)
        Me.Clv_GrupoTextBox.Name = "Clv_GrupoTextBox"
        Me.Clv_GrupoTextBox.ReadOnly = True
        Me.Clv_GrupoTextBox.Size = New System.Drawing.Size(131, 21)
        Me.Clv_GrupoTextBox.TabIndex = 12
        Me.Clv_GrupoTextBox.TabStop = False
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(355, 119)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(86, 23)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(483, 465)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(10, 23)
        Me.Button4.TabIndex = 8
        Me.Button4.TabStop = False
        Me.Button4.Text = "&Eliminar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'ConGrupoVentasBindingSource
        '
        Me.ConGrupoVentasBindingSource.DataMember = "ConGrupoVentas"
        Me.ConGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'ConGrupoVentasTableAdapter
        '
        Me.ConGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'NueGrupoVentasBindingSource
        '
        Me.NueGrupoVentasBindingSource.DataMember = "NueGrupoVentas"
        Me.NueGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'NueGrupoVentasTableAdapter
        '
        Me.NueGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'ModGrupoVentasBindingSource
        '
        Me.ModGrupoVentasBindingSource.DataMember = "ModGrupoVentas"
        Me.ModGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'ModGrupoVentasTableAdapter
        '
        Me.ModGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'BorGrupoVentasBindingSource
        '
        Me.BorGrupoVentasBindingSource.DataMember = "BorGrupoVentas"
        Me.BorGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'BorGrupoVentasTableAdapter
        '
        Me.BorGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'ConGrupoVentas1TableAdapter
        '
        Me.ConGrupoVentas1TableAdapter.ClearBeforeFill = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CheckBox1.Location = New System.Drawing.Point(188, 88)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(63, 19)
        Me.CheckBox1.TabIndex = 211
        Me.CheckBox1.Text = "Activo"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_Grupo"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clave"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Grupo"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Grupo"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 250
        '
        'Activo
        '
        Me.Activo.DataPropertyName = "activo"
        Me.Activo.HeaderText = "Activo"
        Me.Activo.Name = "Activo"
        Me.Activo.ReadOnly = True
        Me.Activo.Width = 60
        '
        'FrmGrupoVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(529, 505)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmGrupoVentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Grupo de Ventas"
        CType(Me.ConGrupoVentasDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGrupoVentas1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.ConGrupoVentasTableAdapter
    Friend WithEvents ConGrupoVentasDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents NueGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.NueGrupoVentasTableAdapter
    Friend WithEvents ModGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.ModGrupoVentasTableAdapter
    Friend WithEvents BorGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.BorGrupoVentasTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GrupoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_GrupoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents ConGrupoVentas1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGrupoVentas1TableAdapter As sofTV.DataSetEric2TableAdapters.ConGrupoVentas1TableAdapter
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Activo As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
