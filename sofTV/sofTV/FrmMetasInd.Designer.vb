<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMetasInd
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NombreLabel As System.Windows.Forms.Label
        Dim GrupoLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim AnioLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMetasInd))
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ConMetasIndBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConMetasIndTableAdapter = New sofTV.DataSetEric2TableAdapters.ConMetasIndTableAdapter()
        Me.ConMetasIndDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MuestraGrupoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraGrupoTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraGrupoTableAdapter()
        Me.NombreComboBox = New System.Windows.Forms.ComboBox()
        Me.ConGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.ConGrupoVentasTableAdapter()
        Me.GrupoComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraAniosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraAniosTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraAniosTableAdapter()
        Me.AnioComboBox = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ConMetasIndBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ConMetasIndBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Clv_GrupoTextBox = New System.Windows.Forms.TextBox()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox()
        Me.AnioTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_GrupoTextBox1 = New System.Windows.Forms.TextBox()
        NombreLabel = New System.Windows.Forms.Label()
        GrupoLabel = New System.Windows.Forms.Label()
        ConceptoLabel = New System.Windows.Forms.Label()
        AnioLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMetasIndBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMetasIndDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraGrupoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraAniosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ConMetasIndBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConMetasIndBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.Location = New System.Drawing.Point(327, 37)
        NombreLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(113, 18)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Comisionado:"
        '
        'GrupoLabel
        '
        GrupoLabel.AutoSize = True
        GrupoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        GrupoLabel.Location = New System.Drawing.Point(31, 37)
        GrupoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        GrupoLabel.Name = "GrupoLabel"
        GrupoLabel.Size = New System.Drawing.Size(139, 18)
        GrupoLabel.TabIndex = 6
        GrupoLabel.Text = "Grupo de Ventas:"
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConceptoLabel.Location = New System.Drawing.Point(703, 37)
        ConceptoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(135, 18)
        ConceptoLabel.TabIndex = 8
        ConceptoLabel.Text = "Tipo de Servicio:"
        '
        'AnioLabel
        '
        AnioLabel.AutoSize = True
        AnioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        AnioLabel.Location = New System.Drawing.Point(988, 37)
        AnioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        AnioLabel.Name = "AnioLabel"
        AnioLabel.Size = New System.Drawing.Size(42, 18)
        AnioLabel.TabIndex = 9
        AnioLabel.Text = "Año:"
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConMetasIndBindingSource
        '
        Me.ConMetasIndBindingSource.DataMember = "ConMetasInd"
        Me.ConMetasIndBindingSource.DataSource = Me.DataSetEric2
        '
        'ConMetasIndTableAdapter
        '
        Me.ConMetasIndTableAdapter.ClearBeforeFill = True
        '
        'ConMetasIndDataGridView
        '
        Me.ConMetasIndDataGridView.AutoGenerateColumns = False
        Me.ConMetasIndDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConMetasIndDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConMetasIndDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17})
        Me.ConMetasIndDataGridView.DataSource = Me.ConMetasIndBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConMetasIndDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConMetasIndDataGridView.Location = New System.Drawing.Point(4, 34)
        Me.ConMetasIndDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ConMetasIndDataGridView.Name = "ConMetasIndDataGridView"
        Me.ConMetasIndDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.ConMetasIndDataGridView.Size = New System.Drawing.Size(1189, 425)
        Me.ConMetasIndDataGridView.TabIndex = 2
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_Grupo"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clv_Grupo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clave"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clave"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Servicio"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Servicio"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Ene"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Ene"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 50
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Feb"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Feb"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 50
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Mar"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Mar"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 50
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Abr"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Abr"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 50
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "May"
        Me.DataGridViewTextBoxColumn9.HeaderText = "May"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 50
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Jun"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Jun"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Width = 50
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Jul"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Jul"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Width = 50
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Ago"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Ago"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Width = 50
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Sep"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Sep"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Width = 50
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Oct"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Oct"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Width = 50
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "Nov"
        Me.DataGridViewTextBoxColumn15.HeaderText = "Nov"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Width = 50
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "Dic"
        Me.DataGridViewTextBoxColumn16.HeaderText = "Dic"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Width = 50
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "Anio"
        Me.DataGridViewTextBoxColumn17.HeaderText = "Anio"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'MuestraGrupoBindingSource
        '
        Me.MuestraGrupoBindingSource.DataMember = "MuestraGrupo"
        Me.MuestraGrupoBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraGrupoTableAdapter
        '
        Me.MuestraGrupoTableAdapter.ClearBeforeFill = True
        '
        'NombreComboBox
        '
        Me.NombreComboBox.DataSource = Me.MuestraGrupoBindingSource
        Me.NombreComboBox.DisplayMember = "Nombre"
        Me.NombreComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreComboBox.FormattingEnabled = True
        Me.NombreComboBox.Location = New System.Drawing.Point(331, 59)
        Me.NombreComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreComboBox.Name = "NombreComboBox"
        Me.NombreComboBox.Size = New System.Drawing.Size(328, 26)
        Me.NombreComboBox.TabIndex = 4
        Me.NombreComboBox.ValueMember = "Clave"
        '
        'ConGrupoVentasBindingSource
        '
        Me.ConGrupoVentasBindingSource.DataMember = "ConGrupoVentas"
        Me.ConGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'ConGrupoVentasTableAdapter
        '
        Me.ConGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'GrupoComboBox
        '
        Me.GrupoComboBox.DataSource = Me.ConGrupoVentasBindingSource
        Me.GrupoComboBox.DisplayMember = "Grupo"
        Me.GrupoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrupoComboBox.FormattingEnabled = True
        Me.GrupoComboBox.Location = New System.Drawing.Point(35, 59)
        Me.GrupoComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GrupoComboBox.Name = "GrupoComboBox"
        Me.GrupoComboBox.Size = New System.Drawing.Size(247, 26)
        Me.GrupoComboBox.TabIndex = 7
        Me.GrupoComboBox.ValueMember = "Clv_Grupo"
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(707, 59)
        Me.ConceptoComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(253, 26)
        Me.ConceptoComboBox.TabIndex = 9
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'MuestraAniosBindingSource
        '
        Me.MuestraAniosBindingSource.DataMember = "MuestraAnios"
        Me.MuestraAniosBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraAniosTableAdapter
        '
        Me.MuestraAniosTableAdapter.ClearBeforeFill = True
        '
        'AnioComboBox
        '
        Me.AnioComboBox.DataSource = Me.MuestraAniosBindingSource
        Me.AnioComboBox.DisplayMember = "Anio"
        Me.AnioComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnioComboBox.FormattingEnabled = True
        Me.AnioComboBox.Location = New System.Drawing.Point(992, 59)
        Me.AnioComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.AnioComboBox.Name = "AnioComboBox"
        Me.AnioComboBox.Size = New System.Drawing.Size(160, 26)
        Me.AnioComboBox.TabIndex = 10
        Me.AnioComboBox.ValueMember = "Anio"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ConMetasIndBindingNavigator)
        Me.Panel1.Controls.Add(Me.ConMetasIndDataGridView)
        Me.Panel1.Location = New System.Drawing.Point(16, 118)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1197, 463)
        Me.Panel1.TabIndex = 11
        '
        'ConMetasIndBindingNavigator
        '
        Me.ConMetasIndBindingNavigator.AddNewItem = Nothing
        Me.ConMetasIndBindingNavigator.BindingSource = Me.ConMetasIndBindingSource
        Me.ConMetasIndBindingNavigator.CountItem = Nothing
        Me.ConMetasIndBindingNavigator.DeleteItem = Nothing
        Me.ConMetasIndBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConMetasIndBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ConMetasIndBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConMetasIndBindingNavigatorSaveItem})
        Me.ConMetasIndBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConMetasIndBindingNavigator.MoveFirstItem = Nothing
        Me.ConMetasIndBindingNavigator.MoveLastItem = Nothing
        Me.ConMetasIndBindingNavigator.MoveNextItem = Nothing
        Me.ConMetasIndBindingNavigator.MovePreviousItem = Nothing
        Me.ConMetasIndBindingNavigator.Name = "ConMetasIndBindingNavigator"
        Me.ConMetasIndBindingNavigator.PositionItem = Nothing
        Me.ConMetasIndBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConMetasIndBindingNavigator.Size = New System.Drawing.Size(1197, 27)
        Me.ConMetasIndBindingNavigator.TabIndex = 3
        Me.ConMetasIndBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(110, 24)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConMetasIndBindingNavigatorSaveItem
        '
        Me.ConMetasIndBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConMetasIndBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConMetasIndBindingNavigatorSaveItem.Name = "ConMetasIndBindingNavigatorSaveItem"
        Me.ConMetasIndBindingNavigatorSaveItem.Size = New System.Drawing.Size(107, 24)
        Me.ConMetasIndBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(1084, 639)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 44)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Clv_GrupoTextBox
        '
        Me.Clv_GrupoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMetasIndBindingSource, "Clv_Grupo", True))
        Me.Clv_GrupoTextBox.Location = New System.Drawing.Point(1155, 642)
        Me.Clv_GrupoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_GrupoTextBox.Name = "Clv_GrupoTextBox"
        Me.Clv_GrupoTextBox.ReadOnly = True
        Me.Clv_GrupoTextBox.Size = New System.Drawing.Size(12, 22)
        Me.Clv_GrupoTextBox.TabIndex = 13
        Me.Clv_GrupoTextBox.TabStop = False
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMetasIndBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(1133, 642)
        Me.ClaveTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(12, 22)
        Me.ClaveTextBox.TabIndex = 14
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMetasIndBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(1197, 642)
        Me.Clv_ServicioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.ReadOnly = True
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(12, 22)
        Me.Clv_ServicioTextBox.TabIndex = 15
        Me.Clv_ServicioTextBox.TabStop = False
        '
        'AnioTextBox
        '
        Me.AnioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMetasIndBindingSource, "Anio", True))
        Me.AnioTextBox.Location = New System.Drawing.Point(1176, 642)
        Me.AnioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.AnioTextBox.Name = "AnioTextBox"
        Me.AnioTextBox.ReadOnly = True
        Me.AnioTextBox.Size = New System.Drawing.Size(12, 22)
        Me.AnioTextBox.TabIndex = 17
        Me.AnioTextBox.TabStop = False
        '
        'Clv_GrupoTextBox1
        '
        Me.Clv_GrupoTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGrupoVentasBindingSource, "Clv_Grupo", True))
        Me.Clv_GrupoTextBox1.Location = New System.Drawing.Point(1112, 642)
        Me.Clv_GrupoTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_GrupoTextBox1.Name = "Clv_GrupoTextBox1"
        Me.Clv_GrupoTextBox1.ReadOnly = True
        Me.Clv_GrupoTextBox1.Size = New System.Drawing.Size(12, 22)
        Me.Clv_GrupoTextBox1.TabIndex = 18
        Me.Clv_GrupoTextBox1.TabStop = False
        '
        'FrmMetasInd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1281, 698)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Clv_GrupoTextBox1)
        Me.Controls.Add(Me.AnioTextBox)
        Me.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.Clv_GrupoTextBox)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(AnioLabel)
        Me.Controls.Add(Me.AnioComboBox)
        Me.Controls.Add(ConceptoLabel)
        Me.Controls.Add(Me.ConceptoComboBox)
        Me.Controls.Add(GrupoLabel)
        Me.Controls.Add(Me.GrupoComboBox)
        Me.Controls.Add(NombreLabel)
        Me.Controls.Add(Me.NombreComboBox)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmMetasInd"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Medidores Individuales de Servicios de Ventas"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMetasIndBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMetasIndDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraGrupoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraAniosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConMetasIndBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConMetasIndBindingNavigator.ResumeLayout(False)
        Me.ConMetasIndBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConMetasIndBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConMetasIndTableAdapter As sofTV.DataSetEric2TableAdapters.ConMetasIndTableAdapter
    Friend WithEvents ConMetasIndDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents MuestraGrupoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraGrupoTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraGrupoTableAdapter
    Friend WithEvents NombreComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ConGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.ConGrupoVentasTableAdapter
    Friend WithEvents GrupoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraAniosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraAniosTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraAniosTableAdapter
    Friend WithEvents AnioComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ConMetasIndBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMetasIndBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Clv_GrupoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AnioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_GrupoTextBox1 As System.Windows.Forms.TextBox
End Class
