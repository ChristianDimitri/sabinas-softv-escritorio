﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComisionesCobroADomicilio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tbRangoIni = New System.Windows.Forms.TextBox()
        Me.tbRangoFin = New System.Windows.Forms.TextBox()
        Me.tbComision = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvComisiones = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RangoIni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RangoFin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Comision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bnAgregar = New System.Windows.Forms.Button()
        Me.bnEliminar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        CType(Me.dgvComisiones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbRangoIni
        '
        Me.tbRangoIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRangoIni.Location = New System.Drawing.Point(584, 31)
        Me.tbRangoIni.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbRangoIni.Name = "tbRangoIni"
        Me.tbRangoIni.Size = New System.Drawing.Size(132, 24)
        Me.tbRangoIni.TabIndex = 0
        '
        'tbRangoFin
        '
        Me.tbRangoFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRangoFin.Location = New System.Drawing.Point(584, 63)
        Me.tbRangoFin.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbRangoFin.Name = "tbRangoFin"
        Me.tbRangoFin.Size = New System.Drawing.Size(132, 24)
        Me.tbRangoFin.TabIndex = 1
        '
        'tbComision
        '
        Me.tbComision.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbComision.Location = New System.Drawing.Point(584, 95)
        Me.tbComision.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbComision.Name = "tbComision"
        Me.tbComision.Size = New System.Drawing.Size(132, 24)
        Me.tbComision.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(448, 37)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 18)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Rango Inicial:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(457, 69)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 18)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Rango Final:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(481, 101)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 18)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Comisión:"
        '
        'dgvComisiones
        '
        Me.dgvComisiones.AllowUserToAddRows = False
        Me.dgvComisiones.AllowUserToDeleteRows = False
        Me.dgvComisiones.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvComisiones.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvComisiones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvComisiones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.RangoIni, Me.RangoFin, Me.Comision})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvComisiones.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvComisiones.Location = New System.Drawing.Point(56, 142)
        Me.dgvComisiones.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvComisiones.Name = "dgvComisiones"
        Me.dgvComisiones.ReadOnly = True
        Me.dgvComisiones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvComisiones.Size = New System.Drawing.Size(676, 271)
        Me.dgvComisiones.TabIndex = 6
        Me.dgvComisiones.TabStop = False
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        '
        'RangoIni
        '
        Me.RangoIni.DataPropertyName = "RangoIni"
        Me.RangoIni.HeaderText = "Rango Inicial"
        Me.RangoIni.Name = "RangoIni"
        Me.RangoIni.ReadOnly = True
        Me.RangoIni.Width = 150
        '
        'RangoFin
        '
        Me.RangoFin.DataPropertyName = "RangoFin"
        Me.RangoFin.HeaderText = "Rango Final"
        Me.RangoFin.Name = "RangoFin"
        Me.RangoFin.ReadOnly = True
        Me.RangoFin.Width = 150
        '
        'Comision
        '
        Me.Comision.DataPropertyName = "Comision"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Comision.DefaultCellStyle = DataGridViewCellStyle2
        Me.Comision.HeaderText = "Comisión"
        Me.Comision.Name = "Comision"
        Me.Comision.ReadOnly = True
        Me.Comision.Width = 150
        '
        'bnAgregar
        '
        Me.bnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAgregar.Location = New System.Drawing.Point(740, 95)
        Me.bnAgregar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnAgregar.Name = "bnAgregar"
        Me.bnAgregar.Size = New System.Drawing.Size(100, 28)
        Me.bnAgregar.TabIndex = 3
        Me.bnAgregar.Text = "&Agregar"
        Me.bnAgregar.UseVisualStyleBackColor = True
        '
        'bnEliminar
        '
        Me.bnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEliminar.Location = New System.Drawing.Point(740, 142)
        Me.bnEliminar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnEliminar.Name = "bnEliminar"
        Me.bnEliminar.Size = New System.Drawing.Size(100, 28)
        Me.bnEliminar.TabIndex = 4
        Me.bnEliminar.Text = "&Eliminar"
        Me.bnEliminar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(692, 458)
        Me.bnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(181, 44)
        Me.bnSalir.TabIndex = 5
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'FrmComisionesCobroADomicilio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(889, 517)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnEliminar)
        Me.Controls.Add(Me.bnAgregar)
        Me.Controls.Add(Me.dgvComisiones)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbComision)
        Me.Controls.Add(Me.tbRangoFin)
        Me.Controls.Add(Me.tbRangoIni)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmComisionesCobroADomicilio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisiones a Vendedores por Cobro a Domicilio"
        CType(Me.dgvComisiones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbRangoIni As System.Windows.Forms.TextBox
    Friend WithEvents tbRangoFin As System.Windows.Forms.TextBox
    Friend WithEvents tbComision As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvComisiones As System.Windows.Forms.DataGridView
    Friend WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RangoIni As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RangoFin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Comision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bnAgregar As System.Windows.Forms.Button
    Friend WithEvents bnEliminar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
End Class
