Imports System.Data.SqlClient
Public Class FrmMetasRep2

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Valida()
    End Sub

    Private Sub FrmMetasRep2_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            Me.ConGrupoVentasTableAdapter.Connection = CON
            Me.ConGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConGrupoVentas, 0, 3, GloClvUsuario)
            Me.MuestraTipServEricTableAdapter.Connection = CON
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)
            Me.MuestraMesesTableAdapter.Connection = CON
            Me.MuestraMesesTableAdapter.Fill(Me.DataSetEric2.MuestraMeses)
            Me.MuestraMeses1TableAdapter.Connection = CON
            Me.MuestraMeses1TableAdapter.Fill(Me.DataSetEric2.MuestraMeses1)
            Me.MuestraAniosTableAdapter.Connection = CON
            Me.MuestraAniosTableAdapter.Fill(Me.DataSetEric2.MuestraAnios)
            Me.MuestraAnios1TableAdapter.Connection = CON
            Me.MuestraAnios1TableAdapter.Fill(Me.DataSetEric2.MuestraAnios1)
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
            UspDesactivaBotones(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Consultar()
        Try
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.MuestraMesesTableAdapter.Connection = CON2
            Me.MuestraMesesTableAdapter.Fill(Me.DataSetEric2.MuestraMeses)
            Me.MuestraMeses1TableAdapter.Connection = CON2
            Me.MuestraMeses1TableAdapter.Fill(Me.DataSetEric2.MuestraMeses1)
            Me.MuestraAniosTableAdapter.Connection = CON2
            Me.MuestraAniosTableAdapter.Fill(Me.DataSetEric2.MuestraAnios)
            Me.MuestraAnios1TableAdapter.Connection = CON2
            Me.MuestraAnios1TableAdapter.Fill(Me.DataSetEric2.MuestraAnios1)
            CON2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            Me.GroupBox2.Enabled = True
            Me.GroupBox4.Enabled = True
            'Me.RadioButton7.Enabled = True

        Else
            Me.GroupBox2.Enabled = False
            Me.GroupBox4.Enabled = False
            'Me.RadioButton7.Enabled = False

        End If
    End Sub



    Private Sub Valida()
        Try
            eMesIni = Me.MesComboBox.SelectedValue
            eAnioIni = Me.AnioComboBox.SelectedValue
            eMesFin = Me.MesComboBox1.SelectedValue
            eAnioFin = Me.AnioComboBox1.SelectedValue
            eTipSer = Me.ConceptoComboBox.SelectedValue
            eServicio = Me.ConceptoComboBox.Text
            eStrMesIni = Me.MesComboBox.Text
            eStrMesFin = Me.MesComboBox1.Text
            eClv_Grupo = Me.GrupoComboBox.SelectedValue
            eGrupo = Me.GrupoComboBox.Text

            If (eMesIni > eMesFin And eAnioIni = eAnioFin) Or (eAnioIni > eAnioFin) Then
                MsgBox("La Fecha Inicial ( " & eStrMesIni & "/" & CStr(eAnioIni) & " ) no puede ser Mayor a la Fecha Final (" & eStrMesFin & "/" & CStr(eAnioFin) & ").", , "Error")
                Consultar()
            Else
                'Reporte
                If Me.RadioButton5.Checked = True Then
                    If Me.RadioButton1.Checked = True Then
                        If eClv_Grupo = 1 Then
                            eOpVentas = 41
                            FrmSelVendedor.Show()
                        ElseIf eClv_Grupo = 2 Then
                            eOpVentas = 42
                            FrmSelUsuario.Show()
                        ElseIf eClv_Grupo > 2 Then
                            eOpVentas = 43
                            FrmSelUsuario.Show()
                        End If
                    ElseIf Me.RadioButton2.Checked = True Then
                        eOpVentas = 30
                        FrmImprimirComision.Show()
                    ElseIf Me.RadioButton3.Checked = True Then
                        eOpVentas = 32
                        FrmImprimirComision.Show()
                    ElseIf Me.RadioButton4.Checked = True Then
                        eOpVentas = 34
                        FrmImprimirComision.Show()
                    End If


                    'Gr�fica
                ElseIf Me.RadioButton6.Checked = True Then
                    If Me.RadioButton1.Checked = True Then
                        If eClv_Grupo = 1 Then
                            eOpVentas = 44
                            FrmSelVendedor.Show()
                        ElseIf eClv_Grupo = 2 Then
                            eOpVentas = 45
                            FrmSelUsuario.Show()
                        ElseIf eClv_Grupo > 2 Then
                            eOpVentas = 46
                            FrmSelUsuario.Show()
                        End If
                    ElseIf Me.RadioButton2.Checked = True Then
                        eOpVentas = 31
                        FrmImprimirComision.Show()
                    ElseIf Me.RadioButton3.Checked = True Then
                        eOpVentas = 33
                        FrmImprimirComision.Show()
                    End If

                    'ElseIf Me.RadioButton7.Checked = True Then
                    '    If eClv_Grupo = 1 Then
                    '        eOpVentas = 47
                    '        FrmSelVendedor.Show()
                    '    ElseIf eClv_Grupo > 1 Then
                    '        eOpVentas = 48
                    '        FrmSelUsuario.Show()
                    '    End If
                End If

            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
        If Me.RadioButton4.Checked = True Then
            Me.RadioButton5.Checked = True
            Me.RadioButton6.Enabled = False
        Else
            Me.RadioButton6.Enabled = True
        End If
    End Sub

    Private Sub GroupBox3_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox3.Enter

    End Sub
End Class