Imports System.Data.SqlClient
Public Class FrmCAMDO

    Private opcionlocal As String = "N"
    Private Aparato815 As Boolean = False
    Private Sub CONCAMDOBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAMDOBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Calle por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Colonia por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Ciudad por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.cbLocalidad.SelectedValue) = False Then
            MsgBox("Seleccione la localidad por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.TELEFONOTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero Telef�nico por favor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        'If Aparato815 And (cbEquipo815.SelectedValue = 0 Or cbNodo.SelectedValue = 0) Then
        '    MsgBox("Seleccione el nodo o equipo del cliente ", MsgBoxStyle.Information, "Informaci�n")
        '    Exit Sub
        'End If

        Me.Validate()

        'Me.CONCAMDOBindingSource.EndEdit()
        'Me.CONCAMDOTableAdapter.Connection = CON
        'Me.CONCAMDOTableAdapter.Update(Me.NewSofTvDataSet.CONCAMDO)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLAVE", SqlDbType.BigInt, GloDetClave)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, gloClv_Orden)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, GloContratoord)
        BaseII.CreateMyParameter("@Clv_calle", SqlDbType.Int, Me.Clv_CalleComboBox.SelectedValue)
        BaseII.CreateMyParameter("@Numero", SqlDbType.VarChar, NUMEROTextBox.Text, 50)
        BaseII.CreateMyParameter("@NumeroInt", SqlDbType.VarChar, NumIntTbox.Text, 50)
        BaseII.CreateMyParameter("@EntreCalles", SqlDbType.VarChar, Me.ENTRECALLESTextBox.Text, 250)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, cbLocalidad.SelectedValue)
        BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, Me.TELEFONOTextBox.Text, 50)
        BaseII.CreateMyParameter("@ClvTecnica", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, Clv_CiudadComboBox.SelectedValue)
        BaseII.CreateMyParameter("@Clv_Sector", SqlDbType.Int, 0)
        BaseII.Inserta("NUECAMDONoInt")

        If Aparato815 Then
            NueNodoEquipCli()
        End If

        MsgBox(mensaje5)
        bndCAMDO = True
        Me.Close()
        CON.Close()
    End Sub

    Private Sub NueNodoEquipCli()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@NodoRed", SqlDbType.BigInt, cbNodo.SelectedValue)
        BaseII.CreateMyParameter("@EquipCli", SqlDbType.BigInt, cbEquipo815.SelectedValue)
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, GloDetClave)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, gloClv_Orden)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, 0)
        BaseII.Inserta("NueNodoEquipCli")
    End Sub

    Private Sub Busca()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCAMDOTableAdapter.Connection = CON
            Me.CONCAMDOTableAdapter.Fill(Me.NewSofTvDataSet.CONCAMDO, New System.Nullable(Of Long)(CType(GloDetClave, Long)), New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), New System.Nullable(Of Long)(CType(Contrato, Long)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub asignacolonia()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = True Then
                Me.MuestraCVECOLCIUTableAdapter.Connection = CON
                'Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCVECOLCIU, Me.Clv_ColoniaComboBox.SelectedValue)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
                'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                cbLocalidad.DataSource = BaseII.ConsultaDT("MuestraLocalidadColonia")
                If opcionlocal = "N" Then Me.Clv_CiudadComboBox.Text = ""
                Me.cbLocalidad.Enabled = True
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub asiganacalle()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = True Then
                Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
                'Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.Clv_CalleComboBox.SelectedValue, Integer)))
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_CALLE", SqlDbType.Int, Clv_CalleComboBox.SelectedValue)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                Clv_ColoniaComboBox.DataSource = BaseII.ConsultaDT("DAMECOLONIA_CALLECOMPANIA")
                Me.Clv_ColoniaComboBox.Enabled = True
                If opcionlocal = "N" Then Me.Clv_ColoniaComboBox.Text = ""
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FrmCAMDO_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        GloBndTrabajo = True
        GloBloqueaDetalle = True
        CON.Close()
    End Sub

    Private Sub FrmCAMDO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Aparato815 = False
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        'Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        'Clv_CalleComboBox.DataSource = BaseII.ConsultaDT("MUESTRACALLESCOMPANIAS")
        'Lo nuevo para llenar ciudad y localidad del cliente
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, CInt(GloContratoord))
        BaseII.CreateMyParameter("@clv_ciudad", ParameterDirection.Output, SqlDbType.Int)
        Clv_CiudadComboBox.DataSource = BaseII.ConsultaDT("sp_llenaCiudadCamdo")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, CInt(GloContratoord))
        BaseII.CreateMyParameter("@clv_ciudad", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("sp_llenaCiudadCamdo")
        Clv_CiudadComboBox.SelectedValue = BaseII.dicoPar("@clv_ciudad")
        'Me.Clv_ColoniaComboBox.Enabled = False
        'Me.Clv_CiudadComboBox.Enabled = False
        'Busca()
        dimeSiCamdo()
        'If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
        '    opcionlocal = "N"
        '    Me.CONCAMDOBindingSource.AddNew()
        '    Me.CONTRATOTextBox.Text = Contrato
        '    Me.Clv_OrdenTextBox.Text = gloClv_Orden
        '    Me.CLAVETextBox.Text = GloDetClave


        'Else
        '    opcionlocal = "M"
        '    Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
        '    Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, Me.Clv_CalleTextBox.Text)
        '    Me.MuestraCVECOLCIUTableAdapter.Connection = CON
        '    Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCVECOLCIU, Me.Clv_ColoniaTextBox.Text)
        '    Busca()
        'End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(GloContratoord))
        BaseII.CreateMyParameter("@BND", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("checaSiEsAparato815CAMDO")
        If CBool(BaseII.dicoPar("@BND").ToString) Then
            Aparato815 = True
        End If

        If Aparato815 Then
            LabelNodo.Visible = True
            LabelEquipo815.Visible = True
            cbNodo.Visible = True
            cbEquipo815.Visible = True

            Dame_NododeRed()
            Dame_EquiposCliente()
        Else
            LabelNodo.Visible = False
            LabelEquipo815.Visible = False
            cbNodo.Visible = False
            cbEquipo815.Visible = False
        End If


        If Bloquea = True Or opcion = "M" Then
            Me.CONCAMDOBindingNavigator.Enabled = False
            Me.Panel1.Enabled = False
        End If
        CON.Close()
        If opcionlocal = "N" Or opcionlocal = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub dimeSiCamdo()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@existe", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@clave", SqlDbType.Int, CInt(GloDetClave))
            BaseII.CreateMyParameter("@clv_orden", SqlDbType.Int, CInt(gloClv_Orden))
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, CInt(GloContratoord))
            BaseII.ProcedimientoOutPut("CONCAMDO")
            If BaseII.dicoPar("@existe") = 1 Then
                Dim conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim comando As New SqlCommand()
                comando.Connection = conexion
                comando.CommandText = "exec CONCAMDO " + GloDetClave.ToString + "," + gloClv_Orden.ToString + "," + Contrato.ToString + ",0"
                Dim reader As SqlDataReader = comando.ExecuteReader()
                reader.Read()
                NUMEROTextBox.Text = reader(4).ToString
                ENTRECALLESTextBox.Text = reader(5).ToString
                TELEFONOTextBox.Text = reader(7).ToString
                Clv_CiudadComboBox.SelectedValue = reader(9)
                cbLocalidad.SelectedValue = reader(10)
                Clv_ColoniaComboBox.SelectedValue = reader(6)
                Clv_CalleComboBox.SelectedValue = reader(3)
                reader.Close()
                conexion.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Clv_CalleComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CalleComboBox.SelectedIndexChanged
        'asiganacalle()
    End Sub

    Private Sub Clv_ColoniaComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ColoniaComboBox.SelectedIndexChanged
        'Me.asignacolonia()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, CInt(GloContratoord))
            Clv_CalleComboBox.DataSource = BaseII.ConsultaDT("sp_llenaCalleCamdo")
        Catch ex As Exception

        End Try
        'DameSector()
    End Sub

    Private Sub DameSector()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
            ComboBoxSector.DataSource = BaseII.ConsultaDT("MuestraSectoresNew")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.CLAVETextBox.Text) = False Then
            MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
            MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        Me.CONCAMDOTableAdapter.Connection = CON
        Me.CONCAMDOTableAdapter.Delete(Me.CLAVETextBox.Text, Me.Clv_OrdenTextBox.Text, Me.CONTRATOTextBox.Text)
        CON.Close()
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        bndCAMDO = True
        Me.Close()
    End Sub

    Private Sub Clv_CiudadComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadComboBox.SelectedIndexChanged
        'If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = True Then
        '    Me.Clv_CiudadComboBox.Enabled = True
        'End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, CInt(GloContratoord))
            BaseII.CreateMyParameter("@clv_localidad", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter ("@clv_ciudad",SqlDbType .Int ,Clv_CiudadComboBox .SelectedValue)
            cbLocalidad.DataSource = BaseII.ConsultaDT("sp_llenaLocalidadCamdo")
            BaseII.ProcedimientoOutPut("sp_llenaLocalidadCamdo")
            cbLocalidad.SelectedValue = BaseII.dicoPar("@clv_localidad")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Clv_CiudadTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadTextBox.TextChanged

    End Sub

    Private Sub Clv_CiudadLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


    Private Sub cbLocalidad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbLocalidad.SelectedIndexChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, cbLocalidad.SelectedValue)
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, CInt(GloContratoord))
            Clv_ColoniaComboBox.DataSource = BaseII.ConsultaDT("sp_llenaColoniaCamdo")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Dame_EquiposCliente()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, gloClv_Orden)
            cbEquipo815.DataSource = BaseII.ConsultaDT("Dame_EquiposCliente")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Dame_NododeRed()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, gloClv_Orden)
            cbNodo.DataSource = BaseII.ConsultaDT("Dame_NododeRed")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class