Imports System.Data.SqlClient
Public Class FrmSelPeriodoCartera

    Private Sub FrmSelPeriodoCartera_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Me.CatalogoPeriodosTableAdapter.Connection = con
        Me.CatalogoPeriodosTableAdapter.Fill(Me.Procedimientosarnoldo4.CatalogoPeriodos, glosessioncar)
        Me.Muestra_Sel_periodoCarteratmpConsultaTableAdapter.Connection = con
        Me.Muestra_Sel_periodoCarteratmpConsultaTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Sel_periodoCarteratmpConsulta, glosessioncar)
        con.Close()
    End Sub

    Private Sub MuestraList()
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.Muestra_Sel_periodoCarteratmpConsultaTableAdapter.Connection = CON2
        Me.Muestra_Sel_periodoCarteratmpConsultaTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Sel_periodoCarteratmpConsulta, glosessioncar)
        Me.Muestra_Sel_periodoCarteraConsultaTableAdapter.Connection = CON2
        Me.Muestra_Sel_periodoCarteraConsultaTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Sel_periodoCarteraConsulta, glosessioncar)
        CON2.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON1 As New SqlConnection(MiConexion)
        CON1.Open()
        Me.InsertaUno_Sel_periodocarteraTableAdapter.Connection = CON1
        Me.InsertaUno_Sel_periodocarteraTableAdapter.Fill(Me.Procedimientosarnoldo4.InsertaUno_Sel_periodocartera, glosessioncar, CInt(Me.ListBox1.SelectedValue))
        CON1.Close()
        MuestraList()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON3 As New SqlConnection(MiConexion)
        CON3.Open()
        Me.InsertaUno_Sel_periodocarteratmpTableAdapter.Connection = CON3
        Me.InsertaUno_Sel_periodocarteratmpTableAdapter.Fill(Me.Procedimientosarnoldo4.InsertaUno_Sel_periodocarteratmp, glosessioncar, CInt(Me.ListBox2.SelectedValue))
        CON3.Close()
        MuestraList()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON4 As New SqlConnection(MiConexion)
        CON4.Open()
        Me.InsertaTodo_Sel_PeriodoCarteraTableAdapter.Connection = CON4
        Me.InsertaTodo_Sel_PeriodoCarteraTableAdapter.Fill(Me.Procedimientosarnoldo4.InsertaTodo_Sel_PeriodoCartera, glosessioncar)
        MuestraList()
        CON4.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON5 As New SqlConnection(MiConexion)
        CON5.Open()
        Me.InsertaTodo_Sel_PeriodoCarteratmpTableAdapter.Connection = CON5
        Me.InsertaTodo_Sel_PeriodoCarteratmpTableAdapter.Fill(Me.Procedimientosarnoldo4.InsertaTodo_Sel_PeriodoCarteratmp, glosessioncar)
        MuestraList()
        CON5.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click

        If CInt(Me.ListBox2.Items.Count()) > 0 Then
            execar = True
            Me.Close()
        ElseIf CInt(Me.ListBox2.Items.Count()) = 0 Then
            MsgBox("Debes Seleccionar Al Menos un Periodo", MsgBoxStyle.Information)
        End If

    End Sub
End Class