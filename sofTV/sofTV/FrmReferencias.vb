Imports System.Data.SqlClient
Public Class FrmReferencias
    Dim opcion As Integer
    Public contratoRef As Long
    Public contratoRefCom As String
    Public tipoRef As String
    Dim IdReferencia As Integer

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Len(Trim(Me.TextBox1.Text)) = 0 Then
            MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Len(Trim(Me.TextBox4.Text)) = 0 Then
            MsgBox("Se Requiere la Direccion ", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Len(Trim(Me.TextBox3.Text)) = 0 Then
            MsgBox("Se Requiere el N�mero Telef�nico ", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.TextBox3.Text) = False Then
            MsgBox("Se requieren solo digitos en telefono", MsgBoxStyle.Information)
            Exit Sub
        End If

        If opcion = 1 Then
            sp_InsertaReferencia(contratoRef, Me.TextBox1.Text, Me.TextBox4.Text, Me.TextBox2.Text, Me.TextBox3.Text, 0, 0, tipoRef)
        Else
            sp_InsertaReferencia(contratoRef, Me.TextBox1.Text, Me.TextBox4.Text, Me.TextBox2.Text, Me.TextBox3.Text, CInt(IdReferencia), 1, tipoRef)
        End If

        If tipoRef = "P" Then
            sp_ConsultaReferenciaDataGrid(contratoRef, "P") 'pendiente
        Else
            sp_ConsultaReferenciaDataGrid(contratoRef, "C")
        End If


        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
        Me.TextBox3.Clear()
        Me.TextBox4.Clear()
        Me.Button4.Enabled = True
        Me.Button3.Enabled = True
        Me.Button5.Enabled = True
        Me.Button2.Enabled = False
        Me.GroupBox1.Enabled = False
    End Sub

    Private Sub FrmReferencias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If OpcionCli = "C" Then
            Me.Button3.Enabled = False
            Me.Button5.Enabled = False
            Me.Button4.Enabled = False
        End If
        Me.Label1.Text = "Referencias del Contrato: " + contratoRefCom

        If tipoRef = "P" Then
            sp_ConsultaReferenciaDataGrid(contratoRef, "P")
        Else
            sp_ConsultaReferenciaDataGrid(contratoRef, "C")
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
        'sp_ConsultaReferenciaDataGrid(contratoRef, 0) 'pendiente
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'eliminacion de referencias gus
        If Me.Consulta_ReferenciaClienteDataGridView.Rows.Count > 0 Then
            If IsNumeric(Me.Consulta_ReferenciaClienteDataGridView.SelectedCells(0).Value) = True Then


                If tipoRef = "P" Then
                    sp_EliminaReferencia(Me.Consulta_ReferenciaClienteDataGridView.SelectedCells(0).Value)
                    sp_ConsultaReferenciaDataGrid(contratoRef, "P")
                Else
                    sp_EliminaReferencia(Me.Consulta_ReferenciaClienteDataGridView.SelectedCells(0).Value)
                    sp_ConsultaReferenciaDataGrid(contratoRef, "C")
                End If

                'sp_EliminaReferencia(Me.Consulta_ReferenciaClienteDataGridView.SelectedCells(0).Value, 0) 'pendiente
                'sp_ConsultaReferenciaDataGrid(contratoRef, 0) 'pendiente
            Else
                MsgBox("Seleccione una referencia a eliminar", MsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            MsgBox("No hay referencias en la lista", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        opcion = 1
        Me.GroupBox1.Enabled = True
        Me.Button5.Enabled = False
        Me.Button3.Enabled = False
        Me.Button4.Enabled = False
        Me.Button2.Enabled = True
        Me.Button6.Enabled = True

        'habilitacion de botones gus
        Me.TextBox1.Enabled = True
        Me.TextBox2.Enabled = True
        Me.TextBox3.Enabled = True
        Me.TextBox4.Enabled = True

        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
        Me.TextBox3.Clear()
        Me.TextBox4.Clear()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.Consulta_ReferenciaClienteDataGridView.Rows.Count = 0 Then
            MsgBox("No hay referencias en la lista", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.Consulta_ReferenciaClienteDataGridView.SelectedCells(0).Value) = False Then
            MsgBox("No ha seleccionado un registro a modificar", MsgBoxStyle.Information)
            Exit Sub
        End If

        opcion = 0

        Me.TextBox1.Enabled = True
        Me.TextBox2.Enabled = True
        Me.TextBox3.Enabled = True
        Me.TextBox4.Enabled = True

        Me.Button3.Enabled = False
        Me.Button5.Enabled = False
        Me.Button4.Enabled = False
        Me.Button2.Enabled = True
        Me.Button6.Enabled = True
        Me.GroupBox1.Enabled = True

        'manda llamar los sig procedimientos para la consulta de referencias
        If tipoRef = "P" Then
            sp_ConsultaReferenciaTextBox(Me.Consulta_ReferenciaClienteDataGridView.SelectedCells(0).Value)
            sp_ConsultaReferenciaDataGrid(contratoRef, "P")
        Else
            sp_ConsultaReferenciaTextBox(Me.Consulta_ReferenciaClienteDataGridView.SelectedCells(0).Value)
            sp_ConsultaReferenciaDataGrid(contratoRef, "C")
        End If
        'sp_ConsultaReferenciaTextBox(Me.Consulta_ReferenciaClienteDataGridView.SelectedCells(0).Value, 0) 'pendiente
        'sp_ConsultaReferenciaDataGrid(contratoRef, 0) 'pendiente
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.TextBox1.Enabled = False
        Me.TextBox2.Enabled = False
        Me.TextBox3.Enabled = False
        Me.TextBox4.Enabled = False

        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
        Me.TextBox3.Clear()
        Me.TextBox4.Clear()

        Me.Button5.Enabled = True
        Me.Button3.Enabled = True
        Me.Button2.Enabled = False
        Me.Button6.Enabled = False
        Me.Button4.Enabled = True
        Me.GroupBox1.Enabled = False
    End Sub


    Public Sub sp_InsertaReferencia(ByVal prmContrato As Integer, ByVal prmNombre As String, ByVal prmDireccion As String, ByVal prmEmail As String, _
                                  ByVal prmTelefono As String, ByVal prmIdReferencia As Integer, ByVal prmOpcion As Integer, ByVal prmTipo As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("sp_InsertaReferencia", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Contrato", prmContrato)
        CMD.Parameters.AddWithValue("@Nombre", prmNombre)
        CMD.Parameters.AddWithValue("@Direccion", prmDireccion)
        CMD.Parameters.AddWithValue("@Email", prmEmail)
        CMD.Parameters.AddWithValue("@Telefeno", prmTelefono)
        CMD.Parameters.AddWithValue("@id_referencia", prmIdReferencia)
        CMD.Parameters.AddWithValue("@opcion", prmOpcion)
        CMD.Parameters.AddWithValue("@Tipo", prmTipo)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Public Sub sp_EliminaReferencia(ByVal prmContrato As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("sp_EliminaReferencia", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Id_Referencia", prmContrato)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()

        End Try
    End Sub

    Public Sub sp_ConsultaReferenciaDataGrid(ByVal prmContrato As Integer, ByVal prmTipo As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("sp_ConsultaReferenciaGrid", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Contrato", prmContrato)
        CMD.Parameters.AddWithValue("@Tipo", prmTipo)

        Dim DA As New SqlDataAdapter(CMD)
        Dim DT As New DataTable()

        Try
            CON.Open()
            DA.Fill(DT)
            Me.Consulta_ReferenciaClienteDataGridView.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Public Sub sp_ConsultaReferenciaTextBox(ByVal prmidReferencia As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("sp_ConsultaReferencia", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@idReferencia", prmidReferencia)

        Dim reader As SqlDataReader

        Try
            CON.Open()
            reader = CMD.ExecuteReader

            While reader.Read
                IdReferencia = reader(0).ToString
                Me.TextBox1.Text = reader(2).ToString
                Me.TextBox4.Text = reader(3).ToString
                Me.TextBox3.Text = reader(4).ToString
                Me.TextBox2.Text = reader(5).ToString

            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub


End Class