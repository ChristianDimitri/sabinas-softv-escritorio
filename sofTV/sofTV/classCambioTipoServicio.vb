﻿Public Class classCambioTipoServicio

#Region "Propiedades"
    Private _clvTipSer As Integer
    Public Property clvTipSer As Integer
        Get
            Return _clvTipSer
        End Get
        Set(ByVal value As Integer)
            _clvTipSer = value
        End Set
    End Property
    Private _Op As Integer
    Public Property Op As Integer
        Get
            Return _Op
        End Get
        Set(ByVal value As Integer)
            _Op = value
        End Set
    End Property
    Private _clvCombinacion As Integer
    Public Property clvCombinacion As Integer
        Get
            Return _clvCombinacion
        End Get
        Set(ByVal value As Integer)
            _clvCombinacion = value
        End Set
    End Property
    Private _clvTrabajoAct As Integer
    Public Property clvTrabajoAct As Integer
        Get
            Return _clvTrabajoAct
        End Get
        Set(ByVal value As Integer)
            _clvTrabajoAct = value
        End Set
    End Property
    Private _clvTrabajoDes As Integer
    Public Property clvTrabajoDes As Integer
        Get
            Return _clvTrabajoDes
        End Get
        Set(ByVal value As Integer)
            _clvTrabajoDes = value
        End Set
    End Property
    Private _clvtipserpos As Integer
    Public Property clvtipserpos As String
        Get
            Return _clvtipserpos
        End Get
        Set(ByVal value As String)
            _clvtipserpos = value
        End Set
    End Property

#End Region

#Region "Constructor"
    Public Sub New()
        _clvTipSer = 0
        _Op = 0
        _clvCombinacion = 0
        _clvTrabajoAct = 0
        _clvTrabajoDes = 0
        _clvtipserpos = 0
    End Sub
#End Region

#Region "Métodos"
    Dim cambio As New BaseIII

    Public Function uspMuestraTiposServicios() As DataTable
        Try
            cambio.limpiaParametros()
            cambio.CreateMyParameter("@clvTipSer", SqlDbType.Int, _clvTipSer)
            uspMuestraTiposServicios = cambio.ConsultaDT("uspMuestraTiposServicios")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function uspMuestraPosiblesCambios() As DataTable
        Try
            uspMuestraPosiblesCambios = cambio.ConsultaDT("uspMuestraPosiblesCambios")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function uspMostrarTrabajos() As DataTable
        Try
            cambio.limpiaParametros()
            cambio.CreateMyParameter("@ClvTipSer", SqlDbType.Int, _clvTipSer)
            uspMostrarTrabajos = cambio.ConsultaDT("uspMostrarTrabajos")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function uspMostrarTabajosGrid() As DataTable
        Try
            cambio.limpiaParametros()
            cambio.CreateMyParameter("@ClvCombinacion", SqlDbType.Int, _clvCombinacion)
            cambio.CreateMyParameter("@Op", SqlDbType.Int, _Op)
            uspMostrarTabajosGrid = cambio.ConsultaDT("uspMostrarTabajosGrid")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub uspGrabaTabajos()
        Try
            cambio.limpiaParametros()
            cambio.CreateMyParameter("@ClvCombinacion", SqlDbType.Int, _clvCombinacion)
            cambio.CreateMyParameter("@ClvTrabajoAct", SqlDbType.Int, _clvTrabajoAct)
            cambio.CreateMyParameter("@ClvTrabajoDes", SqlDbType.Int, _clvTrabajoDes)
            cambio.CreateMyParameter("@Op", SqlDbType.Int, _Op)
            cambio.Inserta("uspGrabaTabajos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub uspBorraTrabajos()
        Try
            cambio.limpiaParametros()
            cambio.CreateMyParameter("@ClvCombinacion", SqlDbType.Int, _clvCombinacion)
            cambio.CreateMyParameter("@ClvTrabajoAct", SqlDbType.Int, _clvTrabajoAct)
            cambio.CreateMyParameter("@ClvTrabajoDes", SqlDbType.Int, _clvTrabajoDes)
            cambio.CreateMyParameter("@Op", SqlDbType.Int, _Op)
            cambio.Inserta("uspBorraTrabajos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function uspGrabaCombinacion() As DataTable
        Try
            cambio.limpiaParametros()
            cambio.CreateMyParameter("@ClvTipser", SqlDbType.Int, _clvTipSer)
            cambio.CreateMyParameter("@ClvTipserPos", SqlDbType.Int, _clvtipserpos)
            uspGrabaCombinacion = cambio.ConsultaDT("uspGrabaCombinacion")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub uspBorrarCombinacion()
        Try
            cambio.limpiaParametros()
            cambio.CreateMyParameter("@Clvpromocion", SqlDbType.Int, _clvCombinacion)
            cambio.Inserta("uspBorrarCombinacion")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class
