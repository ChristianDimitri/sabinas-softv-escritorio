﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCatalogoGastos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBlblBusqueda = New System.Windows.Forms.Label()
        Me.CMBlblBuscaDescripcion = New System.Windows.Forms.Label()
        Me.txtBuscaDescripcion = New System.Windows.Forms.TextBox()
        Me.btnBuscaDescripcion = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnConsulta = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.dgvTipoGastos = New System.Windows.Forms.DataGridView()
        Me.ClaveGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ActivoGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmbBuscaActivo = New System.Windows.Forms.ComboBox()
        Me.CMBlblBuscaActivo = New System.Windows.Forms.Label()
        Me.CMBlblMuestraDescripcion = New System.Windows.Forms.Label()
        Me.CMBlblMuestraIdTipoPago = New System.Windows.Forms.Label()
        Me.CMBlblIdTipoPago = New System.Windows.Forms.Label()
        Me.pnlDatosGenerales = New System.Windows.Forms.Panel()
        Me.cbMuestraActivo = New System.Windows.Forms.CheckBox()
        Me.CMBlblMuestraActivo = New System.Windows.Forms.Label()
        Me.CMBlblDescripcion = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.dgvTipoGastos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDatosGenerales.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBlblBusqueda
        '
        Me.CMBlblBusqueda.AutoSize = True
        Me.CMBlblBusqueda.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBusqueda.Location = New System.Drawing.Point(16, 22)
        Me.CMBlblBusqueda.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBusqueda.Name = "CMBlblBusqueda"
        Me.CMBlblBusqueda.Size = New System.Drawing.Size(155, 29)
        Me.CMBlblBusqueda.TabIndex = 0
        Me.CMBlblBusqueda.Text = "Buscar Por :"
        '
        'CMBlblBuscaDescripcion
        '
        Me.CMBlblBuscaDescripcion.AutoSize = True
        Me.CMBlblBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBuscaDescripcion.Location = New System.Drawing.Point(12, 197)
        Me.CMBlblBuscaDescripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBuscaDescripcion.Name = "CMBlblBuscaDescripcion"
        Me.CMBlblBuscaDescripcion.Size = New System.Drawing.Size(122, 20)
        Me.CMBlblBuscaDescripcion.TabIndex = 1
        Me.CMBlblBuscaDescripcion.Text = "Descripción :"
        '
        'txtBuscaDescripcion
        '
        Me.txtBuscaDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscaDescripcion.Location = New System.Drawing.Point(16, 217)
        Me.txtBuscaDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtBuscaDescripcion.Name = "txtBuscaDescripcion"
        Me.txtBuscaDescripcion.Size = New System.Drawing.Size(229, 26)
        Me.txtBuscaDescripcion.TabIndex = 2
        '
        'btnBuscaDescripcion
        '
        Me.btnBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscaDescripcion.Location = New System.Drawing.Point(137, 249)
        Me.btnBuscaDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnBuscaDescripcion.Name = "btnBuscaDescripcion"
        Me.btnBuscaDescripcion.Size = New System.Drawing.Size(109, 31)
        Me.btnBuscaDescripcion.TabIndex = 3
        Me.btnBuscaDescripcion.Text = "&Buscar"
        Me.btnBuscaDescripcion.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(1179, 15)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(163, 43)
        Me.btnAceptar.TabIndex = 4
        Me.btnAceptar.Text = "&Nuevo"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnConsulta
        '
        Me.btnConsulta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConsulta.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsulta.Location = New System.Drawing.Point(1179, 65)
        Me.btnConsulta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnConsulta.Name = "btnConsulta"
        Me.btnConsulta.Size = New System.Drawing.Size(163, 41)
        Me.btnConsulta.TabIndex = 5
        Me.btnConsulta.Text = "&Consulta"
        Me.btnConsulta.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.Location = New System.Drawing.Point(1179, 113)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(163, 42)
        Me.btnModificar.TabIndex = 6
        Me.btnModificar.Text = "&Modifica"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(1179, 847)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(160, 42)
        Me.btnSalir.TabIndex = 7
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'dgvTipoGastos
        '
        Me.dgvTipoGastos.AllowUserToAddRows = False
        Me.dgvTipoGastos.AllowUserToDeleteRows = False
        Me.dgvTipoGastos.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTipoGastos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTipoGastos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTipoGastos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClaveGasto, Me.DescripcionGasto, Me.ActivoGasto})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTipoGastos.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvTipoGastos.Location = New System.Drawing.Point(283, 11)
        Me.dgvTipoGastos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvTipoGastos.Name = "dgvTipoGastos"
        Me.dgvTipoGastos.ReadOnly = True
        Me.dgvTipoGastos.RowHeadersVisible = False
        Me.dgvTipoGastos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTipoGastos.Size = New System.Drawing.Size(888, 878)
        Me.dgvTipoGastos.TabIndex = 9
        '
        'ClaveGasto
        '
        Me.ClaveGasto.DataPropertyName = "ID"
        Me.ClaveGasto.HeaderText = "Clave"
        Me.ClaveGasto.Name = "ClaveGasto"
        Me.ClaveGasto.ReadOnly = True
        '
        'DescripcionGasto
        '
        Me.DescripcionGasto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DescripcionGasto.DataPropertyName = "DESCRIPCION"
        Me.DescripcionGasto.HeaderText = "Descripción"
        Me.DescripcionGasto.Name = "DescripcionGasto"
        Me.DescripcionGasto.ReadOnly = True
        '
        'ActivoGasto
        '
        Me.ActivoGasto.DataPropertyName = "ACTIVO"
        Me.ActivoGasto.HeaderText = "Activo"
        Me.ActivoGasto.Name = "ActivoGasto"
        Me.ActivoGasto.ReadOnly = True
        '
        'cmbBuscaActivo
        '
        Me.cmbBuscaActivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscaActivo.FormattingEnabled = True
        Me.cmbBuscaActivo.Location = New System.Drawing.Point(15, 133)
        Me.cmbBuscaActivo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbBuscaActivo.Name = "cmbBuscaActivo"
        Me.cmbBuscaActivo.Size = New System.Drawing.Size(231, 28)
        Me.cmbBuscaActivo.TabIndex = 10
        '
        'CMBlblBuscaActivo
        '
        Me.CMBlblBuscaActivo.AutoSize = True
        Me.CMBlblBuscaActivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBuscaActivo.Location = New System.Drawing.Point(12, 113)
        Me.CMBlblBuscaActivo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBuscaActivo.Name = "CMBlblBuscaActivo"
        Me.CMBlblBuscaActivo.Size = New System.Drawing.Size(73, 20)
        Me.CMBlblBuscaActivo.TabIndex = 11
        Me.CMBlblBuscaActivo.Text = "Activo :"
        '
        'CMBlblMuestraDescripcion
        '
        Me.CMBlblMuestraDescripcion.AutoSize = True
        Me.CMBlblMuestraDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraDescripcion.Location = New System.Drawing.Point(4, 85)
        Me.CMBlblMuestraDescripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraDescripcion.Name = "CMBlblMuestraDescripcion"
        Me.CMBlblMuestraDescripcion.Size = New System.Drawing.Size(122, 20)
        Me.CMBlblMuestraDescripcion.TabIndex = 9
        Me.CMBlblMuestraDescripcion.Text = "Descripción :"
        '
        'CMBlblMuestraIdTipoPago
        '
        Me.CMBlblMuestraIdTipoPago.AutoSize = True
        Me.CMBlblMuestraIdTipoPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraIdTipoPago.Location = New System.Drawing.Point(4, 16)
        Me.CMBlblMuestraIdTipoPago.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraIdTipoPago.Name = "CMBlblMuestraIdTipoPago"
        Me.CMBlblMuestraIdTipoPago.Size = New System.Drawing.Size(36, 20)
        Me.CMBlblMuestraIdTipoPago.TabIndex = 9
        Me.CMBlblMuestraIdTipoPago.Text = "Id :"
        '
        'CMBlblIdTipoPago
        '
        Me.CMBlblIdTipoPago.AutoSize = True
        Me.CMBlblIdTipoPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblIdTipoPago.Location = New System.Drawing.Point(16, 49)
        Me.CMBlblIdTipoPago.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblIdTipoPago.Name = "CMBlblIdTipoPago"
        Me.CMBlblIdTipoPago.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblIdTipoPago.TabIndex = 10
        '
        'pnlDatosGenerales
        '
        Me.pnlDatosGenerales.Controls.Add(Me.cbMuestraActivo)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraActivo)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblDescripcion)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblIdTipoPago)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraIdTipoPago)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraDescripcion)
        Me.pnlDatosGenerales.Location = New System.Drawing.Point(15, 673)
        Me.pnlDatosGenerales.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pnlDatosGenerales.Name = "pnlDatosGenerales"
        Me.pnlDatosGenerales.Size = New System.Drawing.Size(260, 215)
        Me.pnlDatosGenerales.TabIndex = 8
        '
        'cbMuestraActivo
        '
        Me.cbMuestraActivo.AutoSize = True
        Me.cbMuestraActivo.Enabled = False
        Me.cbMuestraActivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbMuestraActivo.Location = New System.Drawing.Point(91, 156)
        Me.cbMuestraActivo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbMuestraActivo.Name = "cbMuestraActivo"
        Me.cbMuestraActivo.Size = New System.Drawing.Size(14, 13)
        Me.cbMuestraActivo.TabIndex = 12
        Me.cbMuestraActivo.UseVisualStyleBackColor = True
        '
        'CMBlblMuestraActivo
        '
        Me.CMBlblMuestraActivo.AutoSize = True
        Me.CMBlblMuestraActivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraActivo.Location = New System.Drawing.Point(4, 153)
        Me.CMBlblMuestraActivo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraActivo.Name = "CMBlblMuestraActivo"
        Me.CMBlblMuestraActivo.Size = New System.Drawing.Size(73, 20)
        Me.CMBlblMuestraActivo.TabIndex = 12
        Me.CMBlblMuestraActivo.Text = "Activo :"
        '
        'CMBlblDescripcion
        '
        Me.CMBlblDescripcion.AutoSize = True
        Me.CMBlblDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblDescripcion.Location = New System.Drawing.Point(4, 117)
        Me.CMBlblDescripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblDescripcion.Name = "CMBlblDescripcion"
        Me.CMBlblDescripcion.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblDescripcion.TabIndex = 12
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'BrwCatalogoGastos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 903)
        Me.Controls.Add(Me.CMBlblBuscaActivo)
        Me.Controls.Add(Me.cmbBuscaActivo)
        Me.Controls.Add(Me.dgvTipoGastos)
        Me.Controls.Add(Me.pnlDatosGenerales)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnConsulta)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnBuscaDescripcion)
        Me.Controls.Add(Me.txtBuscaDescripcion)
        Me.Controls.Add(Me.CMBlblBuscaDescripcion)
        Me.Controls.Add(Me.CMBlblBusqueda)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwCatalogoGastos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Gastos"
        CType(Me.dgvTipoGastos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDatosGenerales.ResumeLayout(False)
        Me.pnlDatosGenerales.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBlblBusqueda As System.Windows.Forms.Label
    Friend WithEvents CMBlblBuscaDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtBuscaDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscaDescripcion As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnConsulta As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents dgvTipoGastos As System.Windows.Forms.DataGridView
    Friend WithEvents cmbBuscaActivo As System.Windows.Forms.ComboBox
    Friend WithEvents CMBlblBuscaActivo As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraDescripcion As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraIdTipoPago As System.Windows.Forms.Label
    Friend WithEvents CMBlblIdTipoPago As System.Windows.Forms.Label
    Friend WithEvents pnlDatosGenerales As System.Windows.Forms.Panel
    Friend WithEvents CMBlblMuestraActivo As System.Windows.Forms.Label
    Friend WithEvents CMBlblDescripcion As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ClaveGasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionGasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ActivoGasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cbMuestraActivo As System.Windows.Forms.CheckBox
End Class
