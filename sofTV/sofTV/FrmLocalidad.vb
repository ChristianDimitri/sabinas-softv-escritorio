﻿Imports System.Data.SqlClient
Public Class FrmLocalidad
    Public opcionL As String
    Public clv_localidad As Integer
    Private Sub FrmLocalidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcionL = "N" Then
            ComboBoxCiudad.Enabled = False
            Button1.Enabled = False
            Button2.Enabled = False
            BindingNavigatorDeleteItem.Enabled = False
        ElseIf opcionL = "M" Then
            BindingNavigatorDeleteItem.Enabled = True
            LlenaDatos()
            LlenaCombo()
            LlenaGrid()
        ElseIf opcionL = "C" Then
            LlenaDatos()
            LlenaCombo()
            LlenaGrid()
            LocalidadBindingNavigator.Enabled = False
            tbnombre.Enabled = False
            ComboBoxCiudad.Enabled = False
            Button1.Enabled = False
            Button2.Enabled = False
        End If
    End Sub

    Private Sub LlenaCombo()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, tbclvlocalidad.Text)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
        ComboBoxCiudad.DataSource = BaseII.ConsultaDT("SPRelCiudadLocalidad")
    End Sub
    Private Sub LlenaGrid()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, tbclvlocalidad.Text)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        dgvciudad.DataSource = BaseII.ConsultaDT("SPRelCiudadLocalidad")
    End Sub
    Private Sub LlenaDatos()
        Try
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            Dim comando As New SqlCommand()
            comando.Connection = conexion
            comando.CommandText = "exec DameLocalidad " + clv_localidad.ToString
            Dim reader As SqlDataReader = comando.ExecuteReader()
            reader.Read()
            tbclvlocalidad.Text = reader(0).ToString
            tbnombre.Text = reader(1).ToString
            reader.Close()
            conexion.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        If dgvciudad.Rows.Count = 0 Then
            Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_localidad", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
                BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, tbnombre.Text)
                BaseII.CreateMyParameter("@clvnuevo", SqlDbType.Int, tbclvlocalidad.Text)
                BaseII.ProcedimientoOutPut("SPLocalidad")
                If BaseII.dicoPar("@clv_localidad") = 1 Then
                    MsgBox("Hay clientes registrados en esa localidad, no se puede eliminar.")
                Else
                    Me.Close()
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        If tbnombre.Text = "" Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, tbnombre.Text)
        BaseII.CreateMyParameter("@mismoNombre", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.VarChar, tbclvlocalidad.Text)
        BaseII.ProcedimientoOutPut("ValidaNombreLocalidad")
        Dim mismoNombre = BaseII.dicoPar("@mismoNombre")
        If mismoNombre = 1 Then
            MsgBox("Ya existe una localidad con el mismo nombre.")
            Exit Sub
        End If
        If opcionL = "N" Then
            Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_localidad", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, tbnombre.Text)
                BaseII.CreateMyParameter("@clvnuevo", SqlDbType.Int, tbclvlocalidad.Text)
                BaseII.ProcedimientoOutPut("SPLocalidad")
                tbclvlocalidad.Text = BaseII.dicoPar("@clv_localidad").ToString
                BindingNavigatorDeleteItem.Enabled = True
                MsgBox("Datos guardados.")
                ComboBoxCiudad.Enabled = True
                Button1.Enabled = True
                Button2.Enabled = True
                opcionL = "M"
                LlenaCombo()
            Catch ex As Exception

            End Try
        ElseIf opcionL = "M" Then
            Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_localidad", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
                BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, tbnombre.Text)
                BaseII.CreateMyParameter("@clvnuevo", SqlDbType.Int, tbclvlocalidad.Text)
                BaseII.ProcedimientoOutPut("SPLocalidad")
                MsgBox("Datos guardados.")
            Catch ex As Exception

            End Try
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ComboBoxCiudad.Items.Count = 0 Then
            Exit Sub
        End If
        Try
            

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, tbclvlocalidad.Text)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("SPRelCiudadLocalidad")
            If ComboBoxCiudad.Items.Count = 0 Then
                ComboBoxCiudad.Text = ""
            End If
            LlenaGrid()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If dgvciudad.Rows.Count = 0 Then
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, dgvciudad.SelectedCells(0).Value)
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, tbclvlocalidad.Text)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.VarChar, 500)
            BaseII.ProcedimientoOutPut("ValidaEliminaRelLocalidadCiudad")
            eMsj = BaseII.dicoPar("@error")
            If Len(eMsj) > 0 Then
                MsgBox(eMsj, MsgBoxStyle.Information)
                Exit Sub
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, tbclvlocalidad.Text)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, dgvciudad.SelectedCells(0).Value)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("SPRelCiudadLocalidad")
            LlenaGrid()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub

    Private Sub FrmLocalidad_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        BrwLocalidad.entraActivate = 1
    End Sub
End Class