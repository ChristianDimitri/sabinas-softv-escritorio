﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLineaCaptura
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.tbReferencia = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.tbContratoCompania = New System.Windows.Forms.TextBox()
        Me.rbCobra = New System.Windows.Forms.RadioButton()
        Me.rbImporte = New System.Windows.Forms.RadioButton()
        Me.tbMonto = New System.Windows.Forms.TextBox()
        Me.lbMonto = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'tbContrato
        '
        Me.tbContrato.BackColor = System.Drawing.Color.LightGray
        Me.tbContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContrato.Location = New System.Drawing.Point(32, 39)
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.Size = New System.Drawing.Size(124, 24)
        Me.tbContrato.TabIndex = 22
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(29, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 15)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Contrato:"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(162, 40)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(38, 23)
        Me.Button1.TabIndex = 23
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(302, 98)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 51)
        Me.Button2.TabIndex = 24
        Me.Button2.Text = "Generar Referencia"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'tbReferencia
        '
        Me.tbReferencia.BackColor = System.Drawing.Color.LightGray
        Me.tbReferencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbReferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbReferencia.Location = New System.Drawing.Point(32, 183)
        Me.tbReferencia.Name = "tbReferencia"
        Me.tbReferencia.ReadOnly = True
        Me.tbReferencia.Size = New System.Drawing.Size(275, 24)
        Me.tbReferencia.TabIndex = 26
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(29, 161)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 15)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Referencia:"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(345, 183)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(77, 33)
        Me.Button3.TabIndex = 27
        Me.Button3.Text = "Salir"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'tbContratoCompania
        '
        Me.tbContratoCompania.BackColor = System.Drawing.Color.LightGray
        Me.tbContratoCompania.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbContratoCompania.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbContratoCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContratoCompania.Location = New System.Drawing.Point(32, 39)
        Me.tbContratoCompania.Name = "tbContratoCompania"
        Me.tbContratoCompania.Size = New System.Drawing.Size(124, 24)
        Me.tbContratoCompania.TabIndex = 28
        '
        'rbCobra
        '
        Me.rbCobra.AutoSize = True
        Me.rbCobra.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.rbCobra.Location = New System.Drawing.Point(32, 76)
        Me.rbCobra.Name = "rbCobra"
        Me.rbCobra.Size = New System.Drawing.Size(100, 19)
        Me.rbCobra.TabIndex = 29
        Me.rbCobra.TabStop = True
        Me.rbCobra.Text = "Facturación"
        Me.rbCobra.UseVisualStyleBackColor = True
        '
        'rbImporte
        '
        Me.rbImporte.AutoSize = True
        Me.rbImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.rbImporte.Location = New System.Drawing.Point(150, 76)
        Me.rbImporte.Name = "rbImporte"
        Me.rbImporte.Size = New System.Drawing.Size(102, 19)
        Me.rbImporte.TabIndex = 30
        Me.rbImporte.TabStop = True
        Me.rbImporte.Text = "Importe Fijo"
        Me.rbImporte.UseVisualStyleBackColor = True
        '
        'tbMonto
        '
        Me.tbMonto.BackColor = System.Drawing.Color.LightGray
        Me.tbMonto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbMonto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMonto.Location = New System.Drawing.Point(32, 120)
        Me.tbMonto.Name = "tbMonto"
        Me.tbMonto.Size = New System.Drawing.Size(124, 24)
        Me.tbMonto.TabIndex = 32
        Me.tbMonto.Visible = False
        '
        'lbMonto
        '
        Me.lbMonto.AutoSize = True
        Me.lbMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbMonto.Location = New System.Drawing.Point(29, 98)
        Me.lbMonto.Name = "lbMonto"
        Me.lbMonto.Size = New System.Drawing.Size(60, 15)
        Me.lbMonto.TabIndex = 31
        Me.lbMonto.Text = "Importe:"
        Me.lbMonto.Visible = False
        '
        'FrmLineaCaptura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(434, 247)
        Me.Controls.Add(Me.tbMonto)
        Me.Controls.Add(Me.lbMonto)
        Me.Controls.Add(Me.rbImporte)
        Me.Controls.Add(Me.rbCobra)
        Me.Controls.Add(Me.tbContratoCompania)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.tbReferencia)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbContrato)
        Me.Controls.Add(Me.Label3)
        Me.Name = "FrmLineaCaptura"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generación Referencia de Pago"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents tbReferencia As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents tbContratoCompania As System.Windows.Forms.TextBox
    Friend WithEvents rbCobra As System.Windows.Forms.RadioButton
    Friend WithEvents rbImporte As System.Windows.Forms.RadioButton
    Friend WithEvents tbMonto As System.Windows.Forms.TextBox
    Friend WithEvents lbMonto As System.Windows.Forms.Label
End Class
