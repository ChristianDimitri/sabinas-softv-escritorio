Imports System.Data.SqlClient
Public Class BrwSucursales
    Private Sub llena_companias()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"
            'If ComboBoxCompanias.Items.Count > 0 Then
            '    ComboBoxCompanias.SelectedIndex = 0

            'End If
            GloIdCompania = 0
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmSucursales2.Show()
    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            gloClave = Me.Clv_calleLabel2.Text
            FrmSucursales2.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        consultar()



    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            gloClave = Me.Clv_calleLabel2.Text
            FrmSucursales2.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        modificar()


    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try

            If op = 0 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    'Me.BUSCASUCURSALESTableAdapter.Connection = CON
                    'Me.BUSCASUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASUCURSALES, New System.Nullable(Of Integer)(CType(Me.TextBox1.Text, Integer)), "", New System.Nullable(Of Integer)(CType(0, Integer)))
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, TextBox1.Text)
                    BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "", 150)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASUCURSALES")
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            ElseIf op = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    'Me.BUSCASUCURSALESTableAdapter.Connection = CON
                    'Me.BUSCASUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASUCURSALES, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)))
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, TextBox2.Text, 150)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 1)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASUCURSALES")
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 2 Then
                'Me.BUSCASUCURSALESTableAdapter.Connection = CON
                'Me.BUSCASUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASUCURSALES, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(2, Integer)))
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "", 150)
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 2)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASUCURSALES")
            Else
                'Me.BUSCASUCURSALESTableAdapter.Connection = CON
                'Me.BUSCASUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASUCURSALES, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(2, Integer)))
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "", 150)
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 3)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASUCURSALES")

            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()

        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            'CON.Close()
            TryAgain(op)
            Exit Sub
        End Try
        CON.Close()

    End Sub
    Private Sub TryAgain(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try

            If op = 0 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    'Me.BUSCASUCURSALESTableAdapter.Connection = CON
                    'Me.BUSCASUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASUCURSALES, New System.Nullable(Of Integer)(CType(Me.TextBox1.Text, Integer)), "", New System.Nullable(Of Integer)(CType(0, Integer)))
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, TextBox1.Text)
                    BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "", 150)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASUCURSALES")
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            ElseIf op = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    'Me.BUSCASUCURSALESTableAdapter.Connection = CON
                    'Me.BUSCASUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASUCURSALES, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)))
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, TextBox2.Text, 150)
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 1)
                    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASUCURSALES")
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 2 Then
                'Me.BUSCASUCURSALESTableAdapter.Connection = CON
                'Me.BUSCASUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASUCURSALES, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(2, Integer)))
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "", 150)
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 2)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASUCURSALES")
            Else
                'Me.BUSCASUCURSALESTableAdapter.Connection = CON
                'Me.BUSCASUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASUCURSALES, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(2, Integer)))
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "", 150)
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 3)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                DataGridView1.DataSource = BaseII.ConsultaDT("BUSCASUCURSALES")

            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()

        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            'CON.Close()

            Exit Sub
        End Try
        CON.Close()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            gloClave = Me.Clv_calleLabel2.Text
        End If
    End Sub

    Private Sub BrwSucursales_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Busca(3)
        End If
    End Sub

    Private Sub BrwSucursales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llena_companias()
        'If ComboBoxCompanias.Items.Count > 0 Then
        '    ComboBoxCompanias.SelectedIndex = 0
        'End If
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Busca(3)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub



    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub


    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        If DataGridView1.Rows.Count > 0 Then
            Clv_calleLabel2.Text = DataGridView1.SelectedCells(0).Value
            CMBNombreTextBox.Text = DataGridView1.SelectedCells(1).Value
        End If
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        If ComboBoxCompanias.Items.Count > 0 Then
            'ComboBoxCompanias.SelectedIndex = 0
            Busca(2)
        End If


    End Sub
End Class