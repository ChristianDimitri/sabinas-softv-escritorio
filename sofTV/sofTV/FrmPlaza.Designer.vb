﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPlaza
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim MOTCANLabel As System.Windows.Forms.Label
        Dim Clv_Label As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label23 As System.Windows.Forms.Label
        Dim Label24 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label27 As System.Windows.Forms.Label
        Dim Label28 As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Dim Label31 As System.Windows.Forms.Label
        Dim Label33 As System.Windows.Forms.Label
        Dim Label36 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label26 As System.Windows.Forms.Label
        Dim Label38 As System.Windows.Forms.Label
        Dim Label42 As System.Windows.Forms.Label
        Dim Label43 As System.Windows.Forms.Label
        Dim Label45 As System.Windows.Forms.Label
        Dim Label51 As System.Windows.Forms.Label
        Dim Label41 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPlaza))
        Me.Clv_TextBox = New System.Windows.Forms.TextBox()
        Me.MuestraPlazasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tbFiscales = New System.Windows.Forms.TabPage()
        Me.tbNombreComercial = New System.Windows.Forms.TextBox()
        Me.tblada2 = New System.Windows.Forms.TextBox()
        Me.TextImportePagare = New System.Windows.Forms.TextBox()
        Me.tbContactoNombre = New System.Windows.Forms.TextBox()
        Me.tblada = New System.Windows.Forms.TextBox()
        Me.tbtelefono2 = New System.Windows.Forms.TextBox()
        Me.tbrfc = New System.Windows.Forms.TextBox()
        Me.tbemail = New System.Windows.Forms.TextBox()
        Me.tbfax = New System.Windows.Forms.TextBox()
        Me.tbcalle = New System.Windows.Forms.TextBox()
        Me.tbtelefono = New System.Windows.Forms.TextBox()
        Me.tbexterior = New System.Windows.Forms.TextBox()
        Me.tbcodigop = New System.Windows.Forms.TextBox()
        Me.tbinterior = New System.Windows.Forms.TextBox()
        Me.tbpais = New System.Windows.Forms.TextBox()
        Me.tbcolonia = New System.Windows.Forms.TextBox()
        Me.tbestado = New System.Windows.Forms.TextBox()
        Me.tbentrecalles = New System.Windows.Forms.TextBox()
        Me.tbmunicipio = New System.Windows.Forms.TextBox()
        Me.tblocalidad = New System.Windows.Forms.TextBox()
        Me.tbComerciales = New System.Windows.Forms.TabPage()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.tbCalleComercial = New System.Windows.Forms.TextBox()
        Me.tbExteriorComercial = New System.Windows.Forms.TextBox()
        Me.tbCPComercial = New System.Windows.Forms.TextBox()
        Me.tbInteriorComercial = New System.Windows.Forms.TextBox()
        Me.tbColoniaComercial = New System.Windows.Forms.TextBox()
        Me.tbEstadoComercial = New System.Windows.Forms.TextBox()
        Me.tbEntrecallesComercial = New System.Windows.Forms.TextBox()
        Me.tbMunicipioComercial = New System.Windows.Forms.TextBox()
        Me.tbLocalidadComercial = New System.Windows.Forms.TextBox()
        Me.tpContacto = New System.Windows.Forms.TabPage()
        Me.tbNombreRG = New System.Windows.Forms.TextBox()
        Me.tbTelefonoRG = New System.Windows.Forms.TextBox()
        Me.tbCelularRG = New System.Windows.Forms.TextBox()
        Me.tbResponsableComercial = New System.Windows.Forms.TextBox()
        Me.tbEmailRG = New System.Windows.Forms.TextBox()
        Me.tbtbResponsableOperaciones = New System.Windows.Forms.TextBox()
        Me.tbResponsableAtencion = New System.Windows.Forms.TextBox()
        Me.ComboBoxTipoDis = New System.Windows.Forms.ComboBox()
        Me.BindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.Muestra_PlazasTableAdapter = New sofTV.DataSetEDGARTableAdapters.Muestra_PlazasTableAdapter()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ButtonConfigurarServicios = New System.Windows.Forms.Button()
        MOTCANLabel = New System.Windows.Forms.Label()
        Clv_Label = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label21 = New System.Windows.Forms.Label()
        Label23 = New System.Windows.Forms.Label()
        Label24 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label27 = New System.Windows.Forms.Label()
        Label28 = New System.Windows.Forms.Label()
        Label30 = New System.Windows.Forms.Label()
        Label31 = New System.Windows.Forms.Label()
        Label33 = New System.Windows.Forms.Label()
        Label36 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label26 = New System.Windows.Forms.Label()
        Label38 = New System.Windows.Forms.Label()
        Label42 = New System.Windows.Forms.Label()
        Label43 = New System.Windows.Forms.Label()
        Label45 = New System.Windows.Forms.Label()
        Label51 = New System.Windows.Forms.Label()
        Label41 = New System.Windows.Forms.Label()
        CType(Me.MuestraPlazasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tbFiscales.SuspendLayout()
        Me.tbComerciales.SuspendLayout()
        Me.tpContacto.SuspendLayout()
        CType(Me.BindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'MOTCANLabel
        '
        MOTCANLabel.AutoSize = True
        MOTCANLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MOTCANLabel.ForeColor = System.Drawing.Color.LightSlateGray
        MOTCANLabel.Location = New System.Drawing.Point(11, 25)
        MOTCANLabel.Name = "MOTCANLabel"
        MOTCANLabel.Size = New System.Drawing.Size(66, 15)
        MOTCANLabel.TabIndex = 2
        MOTCANLabel.Text = "Nombre :"
        MOTCANLabel.UseWaitCursor = True
        '
        'Clv_Label
        '
        Clv_Label.AutoSize = True
        Clv_Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_Label.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_Label.Location = New System.Drawing.Point(9, 32)
        Clv_Label.Name = "Clv_Label"
        Clv_Label.Size = New System.Drawing.Size(50, 15)
        Clv_Label.TabIndex = 0
        Clv_Label.Text = "Clave :"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(324, 295)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(52, 15)
        Label16.TabIndex = 60
        Label16.Text = "Email :"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(14, 298)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(38, 15)
        Label15.TabIndex = 59
        Label15.Text = "Fax :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(309, 241)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(79, 15)
        Label14.TabIndex = 58
        Label14.Text = "Teléfono 1:"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(11, 133)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(104, 15)
        Label13.TabIndex = 57
        Label13.Text = "Código Postal :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Label12.Location = New System.Drawing.Point(327, 215)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(43, 15)
        Label12.TabIndex = 56
        Label12.Text = "País :"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(14, 213)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(59, 15)
        Label11.TabIndex = 55
        Label11.Text = "Estado :"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(314, 188)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(78, 15)
        Label10.TabIndex = 54
        Label10.Text = "Municipio :"
        AddHandler Label10.Click, AddressOf Me.Label10_Click
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(14, 187)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(78, 15)
        Label8.TabIndex = 53
        Label8.Text = "Localidad :"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(11, 106)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(64, 15)
        Label7.TabIndex = 52
        Label7.Text = "Colonia :"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(14, 160)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(91, 15)
        Label6.TabIndex = 51
        Label6.Text = "Entre calles :"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(213, 81)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(83, 15)
        Label5.TabIndex = 50
        Label5.Text = "No Interior :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(14, 79)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(86, 15)
        Label4.TabIndex = 49
        Label4.Text = "No exterior :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(11, 56)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(48, 15)
        Label3.TabIndex = 48
        Label3.Text = "Calle :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(488, 25)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(42, 15)
        Label2.TabIndex = 75
        Label2.Text = "RFC :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(308, 268)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(79, 15)
        Label1.TabIndex = 77
        Label1.Text = "Teléfono 2:"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(14, 242)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(55, 15)
        Label9.TabIndex = 79
        Label9.Text = "Lada 1:"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Label17.Location = New System.Drawing.Point(14, 271)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(55, 15)
        Label17.TabIndex = 81
        Label17.Text = "Lada 2:"
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Label18.Location = New System.Drawing.Point(14, 321)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(71, 15)
        Label18.TabIndex = 83
        Label18.Text = "Contacto :"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.LightSlateGray
        Label19.Location = New System.Drawing.Point(416, 56)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(114, 15)
        Label19.TabIndex = 85
        Label19.Text = "Importe Pagare :"
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Label20.Location = New System.Drawing.Point(249, 36)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(138, 15)
        Label20.TabIndex = 86
        Label20.Text = "Tipo de Distribuidor:"
        Label20.Visible = False
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label21.ForeColor = System.Drawing.Color.LightSlateGray
        Label21.Location = New System.Drawing.Point(11, 25)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(66, 15)
        Label21.TabIndex = 87
        Label21.Text = "Nombre :"
        Label21.UseWaitCursor = True
        '
        'Label23
        '
        Label23.AutoSize = True
        Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label23.ForeColor = System.Drawing.Color.LightSlateGray
        Label23.Location = New System.Drawing.Point(11, 56)
        Label23.Name = "Label23"
        Label23.Size = New System.Drawing.Size(48, 15)
        Label23.TabIndex = 107
        Label23.Text = "Calle :"
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.ForeColor = System.Drawing.Color.LightSlateGray
        Label24.Location = New System.Drawing.Point(14, 79)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(86, 15)
        Label24.TabIndex = 108
        Label24.Text = "No exterior :"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.ForeColor = System.Drawing.Color.LightSlateGray
        Label25.Location = New System.Drawing.Point(213, 81)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(83, 15)
        Label25.TabIndex = 109
        Label25.Text = "No Interior :"
        '
        'Label27
        '
        Label27.AutoSize = True
        Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label27.ForeColor = System.Drawing.Color.LightSlateGray
        Label27.Location = New System.Drawing.Point(14, 160)
        Label27.Name = "Label27"
        Label27.Size = New System.Drawing.Size(91, 15)
        Label27.TabIndex = 110
        Label27.Text = "Entre calles :"
        '
        'Label28
        '
        Label28.AutoSize = True
        Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label28.ForeColor = System.Drawing.Color.LightSlateGray
        Label28.Location = New System.Drawing.Point(11, 106)
        Label28.Name = "Label28"
        Label28.Size = New System.Drawing.Size(64, 15)
        Label28.TabIndex = 111
        Label28.Text = "Colonia :"
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.ForeColor = System.Drawing.Color.LightSlateGray
        Label30.Location = New System.Drawing.Point(14, 187)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(78, 15)
        Label30.TabIndex = 112
        Label30.Text = "Localidad :"
        '
        'Label31
        '
        Label31.AutoSize = True
        Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label31.ForeColor = System.Drawing.Color.LightSlateGray
        Label31.Location = New System.Drawing.Point(314, 188)
        Label31.Name = "Label31"
        Label31.Size = New System.Drawing.Size(78, 15)
        Label31.TabIndex = 113
        Label31.Text = "Municipio :"
        '
        'Label33
        '
        Label33.AutoSize = True
        Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label33.ForeColor = System.Drawing.Color.LightSlateGray
        Label33.Location = New System.Drawing.Point(14, 213)
        Label33.Name = "Label33"
        Label33.Size = New System.Drawing.Size(59, 15)
        Label33.TabIndex = 114
        Label33.Text = "Estado :"
        '
        'Label36
        '
        Label36.AutoSize = True
        Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label36.ForeColor = System.Drawing.Color.LightSlateGray
        Label36.Location = New System.Drawing.Point(11, 133)
        Label36.Name = "Label36"
        Label36.Size = New System.Drawing.Size(104, 15)
        Label36.TabIndex = 116
        Label36.Text = "Código Postal :"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label22.ForeColor = System.Drawing.Color.LightSlateGray
        Label22.Location = New System.Drawing.Point(42, 61)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(66, 15)
        Label22.TabIndex = 125
        Label22.Text = "Nombre :"
        Label22.UseWaitCursor = True
        '
        'Label26
        '
        Label26.AutoSize = True
        Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label26.ForeColor = System.Drawing.Color.LightSlateGray
        Label26.Location = New System.Drawing.Point(42, 92)
        Label26.Name = "Label26"
        Label26.Size = New System.Drawing.Size(71, 15)
        Label26.TabIndex = 142
        Label26.Text = "Teléfono :"
        '
        'Label38
        '
        Label38.AutoSize = True
        Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label38.ForeColor = System.Drawing.Color.LightSlateGray
        Label38.Location = New System.Drawing.Point(45, 115)
        Label38.Name = "Label38"
        Label38.Size = New System.Drawing.Size(61, 15)
        Label38.TabIndex = 143
        Label38.Text = "Celular :"
        '
        'Label42
        '
        Label42.AutoSize = True
        Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label42.ForeColor = System.Drawing.Color.LightSlateGray
        Label42.Location = New System.Drawing.Point(35, 222)
        Label42.Name = "Label42"
        Label42.Size = New System.Drawing.Size(204, 15)
        Label42.TabIndex = 145
        Label42.Text = "Responsable de Operaciones :"
        '
        'Label43
        '
        Label43.AutoSize = True
        Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label43.ForeColor = System.Drawing.Color.LightSlateGray
        Label43.Location = New System.Drawing.Point(42, 142)
        Label43.Name = "Label43"
        Label43.Size = New System.Drawing.Size(52, 15)
        Label43.TabIndex = 146
        Label43.Text = "Email :"
        '
        'Label45
        '
        Label45.AutoSize = True
        Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label45.ForeColor = System.Drawing.Color.LightSlateGray
        Label45.Location = New System.Drawing.Point(35, 250)
        Label45.Name = "Label45"
        Label45.Size = New System.Drawing.Size(178, 15)
        Label45.TabIndex = 147
        Label45.Text = "Responsable de Atención :"
        AddHandler Label45.Click, AddressOf Me.Label45_Click
        '
        'Label51
        '
        Label51.AutoSize = True
        Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label51.ForeColor = System.Drawing.Color.LightSlateGray
        Label51.Location = New System.Drawing.Point(35, 194)
        Label51.Name = "Label51"
        Label51.Size = New System.Drawing.Size(168, 15)
        Label51.TabIndex = 151
        Label51.Text = "Responsable Comercial :"
        '
        'Label41
        '
        Label41.AutoSize = True
        Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label41.ForeColor = System.Drawing.Color.LightSlateGray
        Label41.Location = New System.Drawing.Point(42, 30)
        Label41.Name = "Label41"
        Label41.Size = New System.Drawing.Size(170, 18)
        Label41.TabIndex = 152
        Label41.Text = "Responsable General"
        Label41.UseWaitCursor = True
        '
        'Clv_TextBox
        '
        Me.Clv_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraPlazasBindingSource, "clv_plaza", True))
        Me.Clv_TextBox.Enabled = False
        Me.Clv_TextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TextBox.Location = New System.Drawing.Point(113, 32)
        Me.Clv_TextBox.Name = "Clv_TextBox"
        Me.Clv_TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_TextBox.TabIndex = 0
        Me.Clv_TextBox.TabStop = False
        Me.Clv_TextBox.Text = "0"
        '
        'MuestraPlazasBindingSource
        '
        Me.MuestraPlazasBindingSource.DataMember = "Muestra_Plazas"
        Me.MuestraPlazasBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.TabControl1)
        Me.Panel1.Controls.Add(Me.ComboBoxTipoDis)
        Me.Panel1.Controls.Add(Label20)
        Me.Panel1.Controls.Add(Me.BindingNavigator)
        Me.Panel1.Controls.Add(Clv_Label)
        Me.Panel1.Controls.Add(Me.Clv_TextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(698, 474)
        Me.Panel1.TabIndex = 4
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tbFiscales)
        Me.TabControl1.Controls.Add(Me.tbComerciales)
        Me.TabControl1.Controls.Add(Me.tpContacto)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(0, 77)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(698, 397)
        Me.TabControl1.TabIndex = 5
        '
        'tbFiscales
        '
        Me.tbFiscales.Controls.Add(Me.tbNombreComercial)
        Me.tbFiscales.Controls.Add(MOTCANLabel)
        Me.tbFiscales.Controls.Add(Label19)
        Me.tbFiscales.Controls.Add(Me.tblada2)
        Me.tbFiscales.Controls.Add(Label15)
        Me.tbFiscales.Controls.Add(Label3)
        Me.tbFiscales.Controls.Add(Me.TextImportePagare)
        Me.tbFiscales.Controls.Add(Label4)
        Me.tbFiscales.Controls.Add(Me.tbContactoNombre)
        Me.tbFiscales.Controls.Add(Label5)
        Me.tbFiscales.Controls.Add(Label18)
        Me.tbFiscales.Controls.Add(Label6)
        Me.tbFiscales.Controls.Add(Label7)
        Me.tbFiscales.Controls.Add(Label17)
        Me.tbFiscales.Controls.Add(Label8)
        Me.tbFiscales.Controls.Add(Me.tblada)
        Me.tbFiscales.Controls.Add(Label10)
        Me.tbFiscales.Controls.Add(Label9)
        Me.tbFiscales.Controls.Add(Label11)
        Me.tbFiscales.Controls.Add(Me.tbtelefono2)
        Me.tbFiscales.Controls.Add(Label12)
        Me.tbFiscales.Controls.Add(Label1)
        Me.tbFiscales.Controls.Add(Label13)
        Me.tbFiscales.Controls.Add(Me.tbrfc)
        Me.tbFiscales.Controls.Add(Label14)
        Me.tbFiscales.Controls.Add(Label2)
        Me.tbFiscales.Controls.Add(Me.tbemail)
        Me.tbFiscales.Controls.Add(Label16)
        Me.tbFiscales.Controls.Add(Me.tbfax)
        Me.tbFiscales.Controls.Add(Me.tbcalle)
        Me.tbFiscales.Controls.Add(Me.tbtelefono)
        Me.tbFiscales.Controls.Add(Me.tbexterior)
        Me.tbFiscales.Controls.Add(Me.tbcodigop)
        Me.tbFiscales.Controls.Add(Me.tbinterior)
        Me.tbFiscales.Controls.Add(Me.tbpais)
        Me.tbFiscales.Controls.Add(Me.tbcolonia)
        Me.tbFiscales.Controls.Add(Me.tbestado)
        Me.tbFiscales.Controls.Add(Me.tbentrecalles)
        Me.tbFiscales.Controls.Add(Me.tbmunicipio)
        Me.tbFiscales.Controls.Add(Me.tblocalidad)
        Me.tbFiscales.Location = New System.Drawing.Point(4, 27)
        Me.tbFiscales.Name = "tbFiscales"
        Me.tbFiscales.Padding = New System.Windows.Forms.Padding(3)
        Me.tbFiscales.Size = New System.Drawing.Size(690, 366)
        Me.tbFiscales.TabIndex = 0
        Me.tbFiscales.Text = "Datos Físcales"
        Me.tbFiscales.UseVisualStyleBackColor = True
        '
        'tbNombreComercial
        '
        Me.tbNombreComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNombreComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombreComercial.Location = New System.Drawing.Point(114, 19)
        Me.tbNombreComercial.Name = "tbNombreComercial"
        Me.tbNombreComercial.Size = New System.Drawing.Size(278, 21)
        Me.tbNombreComercial.TabIndex = 86
        Me.tbNombreComercial.TabStop = False
        '
        'tblada2
        '
        Me.tblada2.BackColor = System.Drawing.Color.White
        Me.tblada2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tblada2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tblada2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblada2.Location = New System.Drawing.Point(115, 295)
        Me.tblada2.MaxLength = 255
        Me.tblada2.Name = "tblada2"
        Me.tblada2.Size = New System.Drawing.Size(185, 21)
        Me.tblada2.TabIndex = 16
        '
        'TextImportePagare
        '
        Me.TextImportePagare.BackColor = System.Drawing.Color.White
        Me.TextImportePagare.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextImportePagare.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextImportePagare.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextImportePagare.Location = New System.Drawing.Point(536, 54)
        Me.TextImportePagare.MaxLength = 15
        Me.TextImportePagare.Name = "TextImportePagare"
        Me.TextImportePagare.Size = New System.Drawing.Size(140, 21)
        Me.TextImportePagare.TabIndex = 4
        '
        'tbContactoNombre
        '
        Me.tbContactoNombre.BackColor = System.Drawing.Color.White
        Me.tbContactoNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbContactoNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbContactoNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContactoNombre.Location = New System.Drawing.Point(114, 319)
        Me.tbContactoNombre.MaxLength = 255
        Me.tbContactoNombre.Name = "tbContactoNombre"
        Me.tbContactoNombre.Size = New System.Drawing.Size(469, 21)
        Me.tbContactoNombre.TabIndex = 20
        '
        'tblada
        '
        Me.tblada.BackColor = System.Drawing.Color.White
        Me.tblada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tblada.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tblada.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblada.Location = New System.Drawing.Point(115, 239)
        Me.tblada.MaxLength = 255
        Me.tblada.Name = "tblada"
        Me.tblada.Size = New System.Drawing.Size(185, 21)
        Me.tblada.TabIndex = 14
        '
        'tbtelefono2
        '
        Me.tbtelefono2.BackColor = System.Drawing.Color.White
        Me.tbtelefono2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbtelefono2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbtelefono2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbtelefono2.Location = New System.Drawing.Point(398, 265)
        Me.tbtelefono2.MaxLength = 255
        Me.tbtelefono2.Name = "tbtelefono2"
        Me.tbtelefono2.Size = New System.Drawing.Size(185, 21)
        Me.tbtelefono2.TabIndex = 17
        '
        'tbrfc
        '
        Me.tbrfc.BackColor = System.Drawing.Color.White
        Me.tbrfc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbrfc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrfc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrfc.Location = New System.Drawing.Point(536, 23)
        Me.tbrfc.MaxLength = 15
        Me.tbrfc.Name = "tbrfc"
        Me.tbrfc.Size = New System.Drawing.Size(140, 21)
        Me.tbrfc.TabIndex = 2
        '
        'tbemail
        '
        Me.tbemail.BackColor = System.Drawing.Color.White
        Me.tbemail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbemail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.tbemail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbemail.Location = New System.Drawing.Point(398, 292)
        Me.tbemail.MaxLength = 255
        Me.tbemail.Name = "tbemail"
        Me.tbemail.Size = New System.Drawing.Size(278, 21)
        Me.tbemail.TabIndex = 19
        '
        'tbfax
        '
        Me.tbfax.BackColor = System.Drawing.Color.White
        Me.tbfax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbfax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbfax.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbfax.Location = New System.Drawing.Point(114, 268)
        Me.tbfax.MaxLength = 255
        Me.tbfax.Name = "tbfax"
        Me.tbfax.Size = New System.Drawing.Size(185, 21)
        Me.tbfax.TabIndex = 18
        '
        'tbcalle
        '
        Me.tbcalle.BackColor = System.Drawing.Color.White
        Me.tbcalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcalle.Location = New System.Drawing.Point(114, 50)
        Me.tbcalle.MaxLength = 255
        Me.tbcalle.Name = "tbcalle"
        Me.tbcalle.Size = New System.Drawing.Size(278, 21)
        Me.tbcalle.TabIndex = 3
        '
        'tbtelefono
        '
        Me.tbtelefono.BackColor = System.Drawing.Color.White
        Me.tbtelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbtelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbtelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbtelefono.Location = New System.Drawing.Point(398, 238)
        Me.tbtelefono.MaxLength = 255
        Me.tbtelefono.Name = "tbtelefono"
        Me.tbtelefono.Size = New System.Drawing.Size(185, 21)
        Me.tbtelefono.TabIndex = 15
        '
        'tbexterior
        '
        Me.tbexterior.BackColor = System.Drawing.Color.White
        Me.tbexterior.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbexterior.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbexterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbexterior.Location = New System.Drawing.Point(114, 77)
        Me.tbexterior.MaxLength = 15
        Me.tbexterior.Name = "tbexterior"
        Me.tbexterior.Size = New System.Drawing.Size(79, 21)
        Me.tbexterior.TabIndex = 5
        '
        'tbcodigop
        '
        Me.tbcodigop.BackColor = System.Drawing.Color.White
        Me.tbcodigop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcodigop.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcodigop.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcodigop.Location = New System.Drawing.Point(115, 131)
        Me.tbcodigop.MaxLength = 5
        Me.tbcodigop.Name = "tbcodigop"
        Me.tbcodigop.Size = New System.Drawing.Size(191, 21)
        Me.tbcodigop.TabIndex = 8
        '
        'tbinterior
        '
        Me.tbinterior.BackColor = System.Drawing.Color.White
        Me.tbinterior.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbinterior.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbinterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbinterior.Location = New System.Drawing.Point(305, 77)
        Me.tbinterior.MaxLength = 15
        Me.tbinterior.Name = "tbinterior"
        Me.tbinterior.Size = New System.Drawing.Size(85, 21)
        Me.tbinterior.TabIndex = 6
        '
        'tbpais
        '
        Me.tbpais.BackColor = System.Drawing.Color.White
        Me.tbpais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbpais.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbpais.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbpais.Location = New System.Drawing.Point(398, 211)
        Me.tbpais.MaxLength = 255
        Me.tbpais.Name = "tbpais"
        Me.tbpais.Size = New System.Drawing.Size(185, 21)
        Me.tbpais.TabIndex = 13
        '
        'tbcolonia
        '
        Me.tbcolonia.BackColor = System.Drawing.Color.White
        Me.tbcolonia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcolonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcolonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcolonia.Location = New System.Drawing.Point(114, 104)
        Me.tbcolonia.MaxLength = 255
        Me.tbcolonia.Name = "tbcolonia"
        Me.tbcolonia.Size = New System.Drawing.Size(322, 21)
        Me.tbcolonia.TabIndex = 7
        '
        'tbestado
        '
        Me.tbestado.BackColor = System.Drawing.Color.White
        Me.tbestado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbestado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbestado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbestado.Location = New System.Drawing.Point(115, 211)
        Me.tbestado.MaxLength = 255
        Me.tbestado.Name = "tbestado"
        Me.tbestado.Size = New System.Drawing.Size(185, 21)
        Me.tbestado.TabIndex = 12
        '
        'tbentrecalles
        '
        Me.tbentrecalles.BackColor = System.Drawing.Color.White
        Me.tbentrecalles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbentrecalles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbentrecalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbentrecalles.Location = New System.Drawing.Point(114, 158)
        Me.tbentrecalles.MaxLength = 255
        Me.tbentrecalles.Name = "tbentrecalles"
        Me.tbentrecalles.Size = New System.Drawing.Size(322, 21)
        Me.tbentrecalles.TabIndex = 9
        '
        'tbmunicipio
        '
        Me.tbmunicipio.BackColor = System.Drawing.Color.White
        Me.tbmunicipio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbmunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbmunicipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbmunicipio.Location = New System.Drawing.Point(398, 185)
        Me.tbmunicipio.MaxLength = 255
        Me.tbmunicipio.Name = "tbmunicipio"
        Me.tbmunicipio.Size = New System.Drawing.Size(278, 21)
        Me.tbmunicipio.TabIndex = 11
        '
        'tblocalidad
        '
        Me.tblocalidad.BackColor = System.Drawing.Color.White
        Me.tblocalidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tblocalidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tblocalidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblocalidad.Location = New System.Drawing.Point(114, 185)
        Me.tblocalidad.MaxLength = 255
        Me.tblocalidad.Name = "tblocalidad"
        Me.tblocalidad.Size = New System.Drawing.Size(185, 21)
        Me.tblocalidad.TabIndex = 10
        '
        'tbComerciales
        '
        Me.tbComerciales.Controls.Add(Label21)
        Me.tbComerciales.Controls.Add(Me.TextBox1)
        Me.tbComerciales.Controls.Add(Label23)
        Me.tbComerciales.Controls.Add(Label24)
        Me.tbComerciales.Controls.Add(Label25)
        Me.tbComerciales.Controls.Add(Label27)
        Me.tbComerciales.Controls.Add(Label28)
        Me.tbComerciales.Controls.Add(Label30)
        Me.tbComerciales.Controls.Add(Label31)
        Me.tbComerciales.Controls.Add(Label33)
        Me.tbComerciales.Controls.Add(Label36)
        Me.tbComerciales.Controls.Add(Me.tbCalleComercial)
        Me.tbComerciales.Controls.Add(Me.tbExteriorComercial)
        Me.tbComerciales.Controls.Add(Me.tbCPComercial)
        Me.tbComerciales.Controls.Add(Me.tbInteriorComercial)
        Me.tbComerciales.Controls.Add(Me.tbColoniaComercial)
        Me.tbComerciales.Controls.Add(Me.tbEstadoComercial)
        Me.tbComerciales.Controls.Add(Me.tbEntrecallesComercial)
        Me.tbComerciales.Controls.Add(Me.tbMunicipioComercial)
        Me.tbComerciales.Controls.Add(Me.tbLocalidadComercial)
        Me.tbComerciales.Location = New System.Drawing.Point(4, 27)
        Me.tbComerciales.Name = "tbComerciales"
        Me.tbComerciales.Padding = New System.Windows.Forms.Padding(3)
        Me.tbComerciales.Size = New System.Drawing.Size(690, 366)
        Me.tbComerciales.TabIndex = 1
        Me.tbComerciales.Text = "Datos Comerciales"
        Me.tbComerciales.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(115, 23)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(278, 21)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.TabStop = False
        '
        'tbCalleComercial
        '
        Me.tbCalleComercial.BackColor = System.Drawing.Color.White
        Me.tbCalleComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbCalleComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCalleComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCalleComercial.Location = New System.Drawing.Point(114, 50)
        Me.tbCalleComercial.MaxLength = 255
        Me.tbCalleComercial.Name = "tbCalleComercial"
        Me.tbCalleComercial.Size = New System.Drawing.Size(278, 21)
        Me.tbCalleComercial.TabIndex = 89
        '
        'tbExteriorComercial
        '
        Me.tbExteriorComercial.BackColor = System.Drawing.Color.White
        Me.tbExteriorComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbExteriorComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbExteriorComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbExteriorComercial.Location = New System.Drawing.Point(114, 77)
        Me.tbExteriorComercial.MaxLength = 15
        Me.tbExteriorComercial.Name = "tbExteriorComercial"
        Me.tbExteriorComercial.Size = New System.Drawing.Size(79, 21)
        Me.tbExteriorComercial.TabIndex = 91
        '
        'tbCPComercial
        '
        Me.tbCPComercial.BackColor = System.Drawing.Color.White
        Me.tbCPComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbCPComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCPComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCPComercial.Location = New System.Drawing.Point(115, 131)
        Me.tbCPComercial.MaxLength = 5
        Me.tbCPComercial.Name = "tbCPComercial"
        Me.tbCPComercial.Size = New System.Drawing.Size(191, 21)
        Me.tbCPComercial.TabIndex = 94
        '
        'tbInteriorComercial
        '
        Me.tbInteriorComercial.BackColor = System.Drawing.Color.White
        Me.tbInteriorComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbInteriorComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbInteriorComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbInteriorComercial.Location = New System.Drawing.Point(305, 77)
        Me.tbInteriorComercial.MaxLength = 15
        Me.tbInteriorComercial.Name = "tbInteriorComercial"
        Me.tbInteriorComercial.Size = New System.Drawing.Size(85, 21)
        Me.tbInteriorComercial.TabIndex = 92
        '
        'tbColoniaComercial
        '
        Me.tbColoniaComercial.BackColor = System.Drawing.Color.White
        Me.tbColoniaComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbColoniaComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbColoniaComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbColoniaComercial.Location = New System.Drawing.Point(114, 104)
        Me.tbColoniaComercial.MaxLength = 255
        Me.tbColoniaComercial.Name = "tbColoniaComercial"
        Me.tbColoniaComercial.Size = New System.Drawing.Size(322, 21)
        Me.tbColoniaComercial.TabIndex = 93
        '
        'tbEstadoComercial
        '
        Me.tbEstadoComercial.BackColor = System.Drawing.Color.White
        Me.tbEstadoComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbEstadoComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbEstadoComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEstadoComercial.Location = New System.Drawing.Point(115, 211)
        Me.tbEstadoComercial.MaxLength = 255
        Me.tbEstadoComercial.Name = "tbEstadoComercial"
        Me.tbEstadoComercial.Size = New System.Drawing.Size(185, 21)
        Me.tbEstadoComercial.TabIndex = 98
        '
        'tbEntrecallesComercial
        '
        Me.tbEntrecallesComercial.BackColor = System.Drawing.Color.White
        Me.tbEntrecallesComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbEntrecallesComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbEntrecallesComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEntrecallesComercial.Location = New System.Drawing.Point(114, 158)
        Me.tbEntrecallesComercial.MaxLength = 255
        Me.tbEntrecallesComercial.Name = "tbEntrecallesComercial"
        Me.tbEntrecallesComercial.Size = New System.Drawing.Size(322, 21)
        Me.tbEntrecallesComercial.TabIndex = 95
        '
        'tbMunicipioComercial
        '
        Me.tbMunicipioComercial.BackColor = System.Drawing.Color.White
        Me.tbMunicipioComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbMunicipioComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbMunicipioComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMunicipioComercial.Location = New System.Drawing.Point(398, 185)
        Me.tbMunicipioComercial.MaxLength = 255
        Me.tbMunicipioComercial.Name = "tbMunicipioComercial"
        Me.tbMunicipioComercial.Size = New System.Drawing.Size(278, 21)
        Me.tbMunicipioComercial.TabIndex = 97
        '
        'tbLocalidadComercial
        '
        Me.tbLocalidadComercial.BackColor = System.Drawing.Color.White
        Me.tbLocalidadComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbLocalidadComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbLocalidadComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLocalidadComercial.Location = New System.Drawing.Point(114, 185)
        Me.tbLocalidadComercial.MaxLength = 255
        Me.tbLocalidadComercial.Name = "tbLocalidadComercial"
        Me.tbLocalidadComercial.Size = New System.Drawing.Size(185, 21)
        Me.tbLocalidadComercial.TabIndex = 96
        '
        'tpContacto
        '
        Me.tpContacto.Controls.Add(Label41)
        Me.tpContacto.Controls.Add(Me.tbNombreRG)
        Me.tpContacto.Controls.Add(Label22)
        Me.tpContacto.Controls.Add(Label26)
        Me.tpContacto.Controls.Add(Label38)
        Me.tpContacto.Controls.Add(Label42)
        Me.tpContacto.Controls.Add(Label43)
        Me.tpContacto.Controls.Add(Label45)
        Me.tpContacto.Controls.Add(Label51)
        Me.tpContacto.Controls.Add(Me.tbTelefonoRG)
        Me.tpContacto.Controls.Add(Me.tbCelularRG)
        Me.tpContacto.Controls.Add(Me.tbResponsableComercial)
        Me.tpContacto.Controls.Add(Me.tbEmailRG)
        Me.tpContacto.Controls.Add(Me.tbtbResponsableOperaciones)
        Me.tpContacto.Controls.Add(Me.tbResponsableAtencion)
        Me.tpContacto.Location = New System.Drawing.Point(4, 27)
        Me.tpContacto.Name = "tpContacto"
        Me.tpContacto.Padding = New System.Windows.Forms.Padding(3)
        Me.tpContacto.Size = New System.Drawing.Size(690, 366)
        Me.tpContacto.TabIndex = 2
        Me.tpContacto.Text = "Contácto Distribuidor"
        Me.tpContacto.UseVisualStyleBackColor = True
        '
        'tbNombreRG
        '
        Me.tbNombreRG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNombreRG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombreRG.Location = New System.Drawing.Point(145, 59)
        Me.tbNombreRG.Name = "tbNombreRG"
        Me.tbNombreRG.Size = New System.Drawing.Size(278, 21)
        Me.tbNombreRG.TabIndex = 124
        Me.tbNombreRG.TabStop = False
        '
        'tbTelefonoRG
        '
        Me.tbTelefonoRG.BackColor = System.Drawing.Color.White
        Me.tbTelefonoRG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbTelefonoRG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbTelefonoRG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTelefonoRG.Location = New System.Drawing.Point(145, 86)
        Me.tbTelefonoRG.MaxLength = 255
        Me.tbTelefonoRG.Name = "tbTelefonoRG"
        Me.tbTelefonoRG.Size = New System.Drawing.Size(153, 21)
        Me.tbTelefonoRG.TabIndex = 126
        '
        'tbCelularRG
        '
        Me.tbCelularRG.BackColor = System.Drawing.Color.White
        Me.tbCelularRG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbCelularRG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCelularRG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCelularRG.Location = New System.Drawing.Point(145, 113)
        Me.tbCelularRG.MaxLength = 15
        Me.tbCelularRG.Name = "tbCelularRG"
        Me.tbCelularRG.Size = New System.Drawing.Size(153, 21)
        Me.tbCelularRG.TabIndex = 127
        '
        'tbResponsableComercial
        '
        Me.tbResponsableComercial.BackColor = System.Drawing.Color.White
        Me.tbResponsableComercial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbResponsableComercial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbResponsableComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbResponsableComercial.Location = New System.Drawing.Point(248, 192)
        Me.tbResponsableComercial.MaxLength = 255
        Me.tbResponsableComercial.Name = "tbResponsableComercial"
        Me.tbResponsableComercial.Size = New System.Drawing.Size(277, 21)
        Me.tbResponsableComercial.TabIndex = 130
        '
        'tbEmailRG
        '
        Me.tbEmailRG.BackColor = System.Drawing.Color.White
        Me.tbEmailRG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbEmailRG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEmailRG.Location = New System.Drawing.Point(145, 140)
        Me.tbEmailRG.MaxLength = 255
        Me.tbEmailRG.Name = "tbEmailRG"
        Me.tbEmailRG.Size = New System.Drawing.Size(278, 21)
        Me.tbEmailRG.TabIndex = 129
        '
        'tbtbResponsableOperaciones
        '
        Me.tbtbResponsableOperaciones.BackColor = System.Drawing.Color.White
        Me.tbtbResponsableOperaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbtbResponsableOperaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbtbResponsableOperaciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbtbResponsableOperaciones.Location = New System.Drawing.Point(248, 219)
        Me.tbtbResponsableOperaciones.MaxLength = 255
        Me.tbtbResponsableOperaciones.Name = "tbtbResponsableOperaciones"
        Me.tbtbResponsableOperaciones.Size = New System.Drawing.Size(322, 21)
        Me.tbtbResponsableOperaciones.TabIndex = 131
        '
        'tbResponsableAtencion
        '
        Me.tbResponsableAtencion.BackColor = System.Drawing.Color.White
        Me.tbResponsableAtencion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbResponsableAtencion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbResponsableAtencion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbResponsableAtencion.Location = New System.Drawing.Point(248, 246)
        Me.tbResponsableAtencion.MaxLength = 255
        Me.tbResponsableAtencion.Name = "tbResponsableAtencion"
        Me.tbResponsableAtencion.Size = New System.Drawing.Size(185, 21)
        Me.tbResponsableAtencion.TabIndex = 132
        '
        'ComboBoxTipoDis
        '
        Me.ComboBoxTipoDis.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxTipoDis.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxTipoDis.FormattingEnabled = True
        Me.ComboBoxTipoDis.Location = New System.Drawing.Point(397, 32)
        Me.ComboBoxTipoDis.Name = "ComboBoxTipoDis"
        Me.ComboBoxTipoDis.Size = New System.Drawing.Size(278, 23)
        Me.ComboBoxTipoDis.TabIndex = 199
        Me.ComboBoxTipoDis.Visible = False
        '
        'BindingNavigator
        '
        Me.BindingNavigator.AddNewItem = Nothing
        Me.BindingNavigator.CountItem = Nothing
        Me.BindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.BindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.BindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONSERVICIOSBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.BindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator.MoveFirstItem = Nothing
        Me.BindingNavigator.MoveLastItem = Nothing
        Me.BindingNavigator.MoveNextItem = Nothing
        Me.BindingNavigator.MovePreviousItem = Nothing
        Me.BindingNavigator.Name = "BindingNavigator"
        Me.BindingNavigator.PositionItem = Nothing
        Me.BindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator.Size = New System.Drawing.Size(698, 27)
        Me.BindingNavigator.TabIndex = 19
        Me.BindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(94, 24)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 24)
        Me.ToolStripButton1.Text = "&CANCELAR"
        Me.ToolStripButton1.Visible = False
        '
        'CONSERVICIOSBindingNavigatorSaveItem
        '
        Me.CONSERVICIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSERVICIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSERVICIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Name = "CONSERVICIOSBindingNavigatorSaveItem"
        Me.CONSERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(95, 24)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.Padding = New System.Windows.Forms.Padding(0, 0, 25, 25)
        Me.TopToolStripPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'ContentPanel
        '
        Me.ContentPanel.Size = New System.Drawing.Size(431, 0)
        '
        'Muestra_PlazasTableAdapter
        '
        Me.Muestra_PlazasTableAdapter.ClearBeforeFill = True
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(574, 492)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 1
        Me.Button5.Text = "&Salir"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(16, 492)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 33)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "&Pagaré"
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'ButtonConfigurarServicios
        '
        Me.ButtonConfigurarServicios.BackColor = System.Drawing.Color.DarkOrange
        Me.ButtonConfigurarServicios.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonConfigurarServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonConfigurarServicios.ForeColor = System.Drawing.Color.Black
        Me.ButtonConfigurarServicios.Location = New System.Drawing.Point(290, 492)
        Me.ButtonConfigurarServicios.Name = "ButtonConfigurarServicios"
        Me.ButtonConfigurarServicios.Size = New System.Drawing.Size(161, 33)
        Me.ButtonConfigurarServicios.TabIndex = 6
        Me.ButtonConfigurarServicios.Text = "&Configurar Servicios"
        Me.ButtonConfigurarServicios.UseVisualStyleBackColor = False
        '
        'FrmPlaza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(717, 543)
        Me.Controls.Add(Me.ButtonConfigurarServicios)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmPlaza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Distribuidor"
        CType(Me.MuestraPlazasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tbFiscales.ResumeLayout(False)
        Me.tbFiscales.PerformLayout()
        Me.tbComerciales.ResumeLayout(False)
        Me.tbComerciales.PerformLayout()
        Me.tpContacto.ResumeLayout(False)
        Me.tpContacto.PerformLayout()
        CType(Me.BindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator.ResumeLayout(False)
        Me.BindingNavigator.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Clv_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BottomToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents TopToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents RightToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents LeftToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ContentPanel As System.Windows.Forms.ToolStripContentPanel
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents MuestraPlazasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents Muestra_PlazasTableAdapter As sofTV.DataSetEDGARTableAdapters.Muestra_PlazasTableAdapter
    Friend WithEvents BindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents tbemail As System.Windows.Forms.TextBox
    Friend WithEvents tbfax As System.Windows.Forms.TextBox
    Friend WithEvents tbtelefono As System.Windows.Forms.TextBox
    Friend WithEvents tbcodigop As System.Windows.Forms.TextBox
    Friend WithEvents tbpais As System.Windows.Forms.TextBox
    Friend WithEvents tbestado As System.Windows.Forms.TextBox
    Friend WithEvents tbmunicipio As System.Windows.Forms.TextBox
    Friend WithEvents tblocalidad As System.Windows.Forms.TextBox
    Friend WithEvents tbentrecalles As System.Windows.Forms.TextBox
    Friend WithEvents tbcolonia As System.Windows.Forms.TextBox
    Friend WithEvents tbinterior As System.Windows.Forms.TextBox
    Friend WithEvents tbexterior As System.Windows.Forms.TextBox
    Friend WithEvents tbcalle As System.Windows.Forms.TextBox
    Friend WithEvents tbrfc As System.Windows.Forms.TextBox
    Friend WithEvents tblada2 As System.Windows.Forms.TextBox
    Friend WithEvents tblada As System.Windows.Forms.TextBox
    Friend WithEvents tbtelefono2 As System.Windows.Forms.TextBox
    Friend WithEvents tbContactoNombre As System.Windows.Forms.TextBox
    Friend WithEvents TextImportePagare As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxTipoDis As System.Windows.Forms.ComboBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tbFiscales As System.Windows.Forms.TabPage
    Friend WithEvents tbComerciales As System.Windows.Forms.TabPage
    Friend WithEvents tbNombreComercial As System.Windows.Forms.TextBox
    Friend WithEvents tbCalleComercial As System.Windows.Forms.TextBox
    Friend WithEvents tbExteriorComercial As System.Windows.Forms.TextBox
    Friend WithEvents tbCPComercial As System.Windows.Forms.TextBox
    Friend WithEvents tbInteriorComercial As System.Windows.Forms.TextBox
    Friend WithEvents tbColoniaComercial As System.Windows.Forms.TextBox
    Friend WithEvents tbEstadoComercial As System.Windows.Forms.TextBox
    Friend WithEvents tbEntrecallesComercial As System.Windows.Forms.TextBox
    Friend WithEvents tbMunicipioComercial As System.Windows.Forms.TextBox
    Friend WithEvents tbLocalidadComercial As System.Windows.Forms.TextBox
    Friend WithEvents tpContacto As System.Windows.Forms.TabPage
    Friend WithEvents tbNombreRG As System.Windows.Forms.TextBox
    Friend WithEvents tbTelefonoRG As System.Windows.Forms.TextBox
    Friend WithEvents tbCelularRG As System.Windows.Forms.TextBox
    Friend WithEvents tbResponsableComercial As System.Windows.Forms.TextBox
    Friend WithEvents tbEmailRG As System.Windows.Forms.TextBox
    Friend WithEvents tbtbResponsableOperaciones As System.Windows.Forms.TextBox
    Friend WithEvents tbResponsableAtencion As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ButtonConfigurarServicios As System.Windows.Forms.Button
End Class
