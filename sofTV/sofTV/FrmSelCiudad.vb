Imports System.Data.SqlClient

Public Class FrmSelCiudad

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim x As Integer = 0
        'Dim y As Integer
        'x = Me.ListBox2.Items.Count()
        'If x > 0 Then
        '    For y = 0 To (x - 1)
        '        Me.ListBox2.SelectedIndex = y
        '        If (Me.ListBox1.Text = Me.ListBox2.Text) Then
        '            MsgBox("La Ciudad ya esta en la lista", MsgBoxStyle.Information)
        '            Exit Sub
        '            'Else
        '            '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '            '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        '            '    Exit Sub
        '        End If
        '    Next
        '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        'Else
        '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        'End If
        x = CInt(Me.ListBox1.SelectedValue)
        If x > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Connection = CON
            Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Fill(Me.DataSetLidia.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmp, LocClv_session, CInt(Me.ListBox1.SelectedValue))
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            ListBox1.DataSource = BaseII.ConsultaDT("MuestraSelecciona_CiudadTmpCONSULTA")
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSeleccion_CiudadCONSULTA, LocClv_session)
            CON.Close()
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim x As Integer
        'Dim y As Integer
        'x = Me.ListBox1.Items.Count()
        'If Me.ListBox2.Items.Count() > 0 Then
        '    MsgBox("Primero Borre la(s) Ciudad(es) Seleccionadas", MsgBoxStyle.Information)
        'Else
        '    Me.ListBox1.SelectedIndex = 0
        '    For y = 1 To x
        '        Me.ListBox1.SelectedIndex = (y - 1)
        '        Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '        Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        '    Next
        'End If
        x = CInt(Me.ListBox1.SelectedValue)
        If x > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Connection = CON
            Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Fill(Me.DataSetLidia.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmp, LocClv_session)
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            ListBox1.DataSource = BaseII.ConsultaDT("MuestraSelecciona_CiudadTmpCONSULTA")
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSeleccion_CiudadCONSULTA, LocClv_session)
            CON.Close()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim x As Integer
        'If (Me.ListBox2.SelectedIndex <> -1) Then
        '    Me.ListBox3.SelectedIndex = Me.ListBox2.SelectedIndex
        '    Me.ListBox2.Items.RemoveAt(Me.ListBox2.SelectedIndex)
        '    Me.ListBox3.Items.RemoveAt(Me.ListBox3.SelectedIndex)
        'Else
        '    MsgBox("Selecciona primero un valor a borrar")
        'End If
        x = CInt(Me.ListBox2.SelectedValue)
        If x > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Connection = CON
            Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Fill(Me.DataSetLidia.PONUNOSelecciona_CiudadTmp_Seleccion_Ciudad, LocClv_session, CInt(Me.ListBox2.SelectedValue))
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            ListBox1.DataSource = BaseII.ConsultaDT("MuestraSelecciona_CiudadTmpCONSULTA")
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSeleccion_CiudadCONSULTA, LocClv_session)
            CON.Close()
        End If

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim x As Integer
        'Dim y As Integer
        'x = Me.ListBox2.Items.Count()
        'For y = 0 To (x - 1)
        '    Me.ListBox2.Items.RemoveAt(0)
        '    Me.ListBox3.Items.RemoveAt(0)
        'Next
        x = CInt(Me.ListBox2.SelectedValue)
        If x > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Connection = CON
            Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Fill(Me.DataSetLidia.PONTODOSSelecciona_CiudadTmp_Seleccion_Ciudad, LocClv_session)
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            ListBox1.DataSource = BaseII.ConsultaDT("MuestraSelecciona_CiudadTmpCONSULTA")
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSeleccion_CiudadCONSULTA, LocClv_session)
            CON.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Borra_Seleccion_CiudadTmpTableAdapter.Connection = CON
        Me.Borra_Seleccion_CiudadTmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_Seleccion_CiudadTmp, LocClv_session)
        CON.Close()
        eBndDeco = False
        LEdo_Cuenta = False
        LEdo_Cuenta2 = False
        bndjano = 0
        Me.Close()


    End Sub

    Private Sub FrmSelCiudad_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

    End Sub


    Private Sub FrmSelCiudad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If EntradasSelCiudad = 0 Then
                EntradasSelCiudad = 1
                SelCiudadSinJ = 1
                Me.Close()
                FrmSelPlaza.Show()
                Exit Sub
            End If
            If EntradasSelCiudad = 1 Then
                EntradasSelCiudad = 0
            End If
            If rMensajes = True Then
                LocClv_session = rSession
            End If
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'Solo una
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@numero", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("DameNumCiudadesUsuario")
            Dim numciudades As Integer = BaseII.dicoPar("@numero")
            If numciudades = 1 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
                BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
                BaseII.Inserta("InsertaSeleccionCiudad")
                llevamealotro()
                Exit Sub
            End If
            'Fin solo una
            '  Me.Muestra_ciudadTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_ciudad)
            'Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.Connection = CON
            'Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpNUEVO, LocClv_session)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.Inserta("MuestraSelecciona_CiudadTmpNUEVO")
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            ListBox1.DataSource = BaseII.ConsultaDT("MuestraSelecciona_CiudadTmpCONSULTA")
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim x As Integer = 0
        Dim y As Integer
        x = Me.ListBox2.Items.Count()
        If x = 0 Then
            MsgBox("Seleccione al Menos una ciudad", MsgBoxStyle.Information)
        Else
            'For y = 0 To (x - 1)
            '    Me.ListBox3.SelectedIndex = y
            '    Me.Inserta_Sel_ciudadTableAdapter.Fill(Me.ProcedimientosArnoldo2.inserta_Sel_ciudad, LocClv_session, CInt(Me.ListBox3.Text))
            'Next


            'If eBndDeco = True Then
            '    FrmSelCalles.Show()
            '    Me.Close()
            'End If
            'If varfrmselcompania = "movimientoscartera" Then
            '    Me.Close()
            '    Exit Sub
            'End If
            'If varfrmselcompania = "ordenes" Then
            '    Me.Visible = False
            '    FrmSelCompaniaCartera.Show()
            '    Me.Close()
            '    Exit Sub
            'End If
            'If rMensajes = True Or eBndDeco = True Then
            '    FrmSelColonia_Rep21.Show()
            'End If
            'If LocbndPolizaCiudad = True Then
            '    FrmSelFechas.Show()
            'Else
            '    'FrmSelColonia_Rep21.Show() 'opcion1
            '    varfrmselcompania = "opcion1varios"
            '    FrmSelCompaniaCartera.Show()

            'End If
            '''Nuevo
            'If varfrmselcompania.Equals("cartera") Then
            '    If BndDesPagAde = True Then
            '        BndDesPagAde = False
            '        FrmSelFechaDesPagAde.Show()
            '    Else
            '        'If IdSistema = "AG" Then
            '        '    FrmSelPeriodoCartera.Show()
            '        'Else
            '        'execar = True
            '        'End If
            '        FrmSelPeriodo.Show()
            '    End If
            'End If

            'If varfrmselcompania = "opcion1varios" Then
            '    FrmSelServRep.Show()
            'End If
            'If varfrmselcompania = "normal" Then
            '    FrmSelColoniaJ.Show()
            'ElseIf varfrmselcompania = "hastacolonias" Then
            '    FrmSelColoniaJ.Show()
            'ElseIf varfrmselcompania = "hastacompania1" Then
            '    FrmSelUsuariosE.Show()
            'ElseIf varfrmselcompania = "hastacompania2" Then
            '    FrmSelTrabajosE.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselvendedor" Then
            '    FrmSelVendedor.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
            '    FrmSelUsuario.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
            '    FrmSelUsuariosVentas.Show()
            'ElseIf varfrmselcompania = "hastacompaniasreportefolios" Then
            '    FormReporteFolios.Show()
            'ElseIf varfrmselcompania = "hastacompaniasresventas" Then
            '    FrmSelFechasPPE.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselsucursal" Then
            '    FrmSelSucursal.Show()
            'ElseIf varfrmselcompania = "hastacompaniaimprimircomision" Then
            '    FrmImprimirComision.Show()
            'ElseIf varfrmselcompania = "hastacompaniacumpleanos" Then
            '    FrmRepCumpleanosDeLosClientes.Show()
            'ElseIf varfrmselcompania = "hastacompaniapruebaint" Then
            '    FrmRepPruebaInternet.Show()
            'ElseIf varfrmselcompania = "normalrecordatorios" Then
            '    FrmSelColoniaJ.Show()
            'ElseIf varfrmselcompania = "hastacompaniamensualidades" Then
            '    FrmImprimirComision.Show()
            'ElseIf varfrmselcompania = "hastacompaniareprecontratacion" Then
            '    SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
            '    SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
            '    SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
            '    SoftvMod.VariablesGlobales.MiConexion = MiConexion
            '    SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
            '    SoftvMod.VariablesGlobales.IdCompania = identificador
            '    Dim frm As New SoftvMod.FrmRepRecontratacion
            '    frm.ShowDialog()
            'ElseIf varfrmselcompania = "movimientoscartera" Then
            '    FrmSelCiudadJ.Show()
            'ElseIf varfrmselcompania = "pendientesderealizar" Then
            '    FrmImprimirComision.Show()
            'ElseIf varfrmselcompania = "resumenordenes" Then
            '    FrmFiltroReporteResumen.Show()
            'End If
            FrmSelLocalidad.Show()
            Me.Close()
        End If

    End Sub

    Private Sub llevamealotro()
        'If varfrmselcompania = "movimientoscartera" Then
        '    Me.Close()
        '    Exit Sub
        'End If
        'If varfrmselcompania = "ordenes" Then
        '    Me.Visible = False
        '    FrmSelCompaniaCartera.Show()
        '    Me.Close()
        '    Exit Sub
        'End If
        'If rMensajes = True Or eBndDeco = True Then
        '    FrmSelColonia_Rep21.Show()
        'End If
        'If LocbndPolizaCiudad = True Then
        '    FrmSelFechas.Show()
        'Else
        '    'FrmSelColonia_Rep21.Show() 'opcion1
        '    varfrmselcompania = "opcion1varios"
        '    FrmSelCompaniaCartera.Show()

        'End If
        '''Nuevo
        'If varfrmselcompania.Equals("cartera") Then
        '    If BndDesPagAde = True Then
        '        BndDesPagAde = False
        '        FrmSelFechaDesPagAde.Show()
        '    Else
        '        'If IdSistema = "AG" Then
        '        '    FrmSelPeriodoCartera.Show()
        '        'Else
        '        'execar = True
        '        'End If
        '        FrmSelPeriodo.Show()
        '    End If
        'End If

        'If varfrmselcompania = "opcion1varios" Then
        '    FrmSelServRep.Show()
        'End If
        'If varfrmselcompania = "normal" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacolonias" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacompania1" Then
        '    FrmSelUsuariosE.Show()
        'ElseIf varfrmselcompania = "hastacompania2" Then
        '    FrmSelTrabajosE.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselvendedor" Then
        '    FrmSelVendedor.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
        '    FrmSelUsuario.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
        '    FrmSelUsuariosVentas.Show()
        'ElseIf varfrmselcompania = "hastacompaniasreportefolios" Then
        '    FormReporteFolios.Show()
        'ElseIf varfrmselcompania = "hastacompaniasresventas" Then
        '    FrmSelFechasPPE.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselsucursal" Then
        '    FrmSelSucursal.Show()
        'ElseIf varfrmselcompania = "hastacompaniaimprimircomision" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "hastacompaniacumpleanos" Then
        '    FrmRepCumpleanosDeLosClientes.Show()
        'ElseIf varfrmselcompania = "hastacompaniapruebaint" Then
        '    FrmRepPruebaInternet.Show()
        'ElseIf varfrmselcompania = "normalrecordatorios" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacompaniamensualidades" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "hastacompaniareprecontratacion" Then
        '    SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        '    SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        '    SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        '    SoftvMod.VariablesGlobales.MiConexion = MiConexion
        '    SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        '    SoftvMod.VariablesGlobales.IdCompania = identificador
        '    Dim frm As New SoftvMod.FrmRepRecontratacion
        '    frm.ShowDialog()
        'ElseIf varfrmselcompania = "movimientoscartera" Then
        '    FrmSelCiudadJ.Show()
        'ElseIf varfrmselcompania = "pendientesderealizar" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "resumenordenes" Then
        '    FrmFiltroReporteResumen.Show()
        'End If
        FrmSelLocalidad.Show()
        Me.Close()
    End Sub


End Class