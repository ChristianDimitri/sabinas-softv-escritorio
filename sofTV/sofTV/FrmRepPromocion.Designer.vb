﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRepPromocion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dtFechaIni = New System.Windows.Forms.DateTimePicker
        Me.dtFechaFin = New System.Windows.Forms.DateTimePicker
        Me.cbC = New System.Windows.Forms.CheckBox
        Me.cbF = New System.Windows.Forms.CheckBox
        Me.cbB = New System.Windows.Forms.CheckBox
        Me.cbS = New System.Windows.Forms.CheckBox
        Me.cbD = New System.Windows.Forms.CheckBox
        Me.cbI = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.btAceptar = New System.Windows.Forms.Button
        Me.btCancelar = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtFechaIni
        '
        Me.dtFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFechaIni.Location = New System.Drawing.Point(87, 28)
        Me.dtFechaIni.Name = "dtFechaIni"
        Me.dtFechaIni.Size = New System.Drawing.Size(101, 21)
        Me.dtFechaIni.TabIndex = 0
        '
        'dtFechaFin
        '
        Me.dtFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFechaFin.Location = New System.Drawing.Point(237, 28)
        Me.dtFechaFin.Name = "dtFechaFin"
        Me.dtFechaFin.Size = New System.Drawing.Size(101, 21)
        Me.dtFechaFin.TabIndex = 1
        '
        'cbC
        '
        Me.cbC.AutoSize = True
        Me.cbC.Location = New System.Drawing.Point(14, 29)
        Me.cbC.Name = "cbC"
        Me.cbC.Size = New System.Drawing.Size(96, 19)
        Me.cbC.TabIndex = 2
        Me.cbC.Text = "Contratado"
        Me.cbC.UseVisualStyleBackColor = True
        '
        'cbF
        '
        Me.cbF.AutoSize = True
        Me.cbF.Location = New System.Drawing.Point(262, 54)
        Me.cbF.Name = "cbF"
        Me.cbF.Size = New System.Drawing.Size(116, 19)
        Me.cbF.TabIndex = 3
        Me.cbF.Text = "Fuéra de Área"
        Me.cbF.UseVisualStyleBackColor = True
        '
        'cbB
        '
        Me.cbB.AutoSize = True
        Me.cbB.Location = New System.Drawing.Point(262, 29)
        Me.cbB.Name = "cbB"
        Me.cbB.Size = New System.Drawing.Size(55, 19)
        Me.cbB.TabIndex = 4
        Me.cbB.Text = "Baja"
        Me.cbB.UseVisualStyleBackColor = True
        '
        'cbS
        '
        Me.cbS.AutoSize = True
        Me.cbS.Location = New System.Drawing.Point(129, 54)
        Me.cbS.Name = "cbS"
        Me.cbS.Size = New System.Drawing.Size(102, 19)
        Me.cbS.TabIndex = 5
        Me.cbS.Text = "Suspendido"
        Me.cbS.UseVisualStyleBackColor = True
        '
        'cbD
        '
        Me.cbD.AutoSize = True
        Me.cbD.Location = New System.Drawing.Point(129, 29)
        Me.cbD.Name = "cbD"
        Me.cbD.Size = New System.Drawing.Size(117, 19)
        Me.cbD.TabIndex = 6
        Me.cbD.Text = "Desconectado"
        Me.cbD.UseVisualStyleBackColor = True
        '
        'cbI
        '
        Me.cbI.AutoSize = True
        Me.cbI.Location = New System.Drawing.Point(14, 54)
        Me.cbI.Name = "cbI"
        Me.cbI.Size = New System.Drawing.Size(85, 19)
        Me.cbI.TabIndex = 7
        Me.cbI.Text = "Instalado"
        Me.cbI.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.dtFechaFin)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.dtFechaIni)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(389, 63)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Fechas de Contratación"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbC)
        Me.GroupBox2.Controls.Add(Me.cbI)
        Me.GroupBox2.Controls.Add(Me.cbB)
        Me.GroupBox2.Controls.Add(Me.cbS)
        Me.GroupBox2.Controls.Add(Me.cbF)
        Me.GroupBox2.Controls.Add(Me.cbD)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 81)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(389, 100)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Status"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(54, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 15)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "del"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(212, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 15)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "al"
        '
        'btAceptar
        '
        Me.btAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAceptar.Location = New System.Drawing.Point(51, 212)
        Me.btAceptar.Name = "btAceptar"
        Me.btAceptar.Size = New System.Drawing.Size(136, 36)
        Me.btAceptar.TabIndex = 10
        Me.btAceptar.Text = "&ACEPTAR"
        Me.btAceptar.UseVisualStyleBackColor = True
        '
        'btCancelar
        '
        Me.btCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btCancelar.Location = New System.Drawing.Point(214, 212)
        Me.btCancelar.Name = "btCancelar"
        Me.btCancelar.Size = New System.Drawing.Size(136, 36)
        Me.btCancelar.TabIndex = 11
        Me.btCancelar.Text = "&CANCELAR"
        Me.btCancelar.UseVisualStyleBackColor = True
        '
        'FrmRepPromocion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(417, 284)
        Me.Controls.Add(Me.btCancelar)
        Me.Controls.Add(Me.btAceptar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmRepPromocion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte Promociones"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dtFechaIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents cbC As System.Windows.Forms.CheckBox
    Friend WithEvents cbF As System.Windows.Forms.CheckBox
    Friend WithEvents cbB As System.Windows.Forms.CheckBox
    Friend WithEvents cbS As System.Windows.Forms.CheckBox
    Friend WithEvents cbD As System.Windows.Forms.CheckBox
    Friend WithEvents cbI As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btAceptar As System.Windows.Forms.Button
    Friend WithEvents btCancelar As System.Windows.Forms.Button
End Class
