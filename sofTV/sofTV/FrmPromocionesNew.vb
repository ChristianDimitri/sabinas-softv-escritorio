﻿Imports System.Data.SqlClient
Public Class FrmPromocionesNew

    Private Sub FrmPromocionesNew_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            ComboBoxPlazas.Enabled = False
            ComboBoxServicios.Enabled = False
            ListBoxPlazas.Enabled = False
            ListBoxServicios.Enabled = False
            AgregarDistribuidor.Enabled = False
            AgregarServicio.Enabled = False
            EliminarDistribuidor.Enabled = False
            EliminarServicio.Enabled = False
            BindingNavigatorDeleteItem.Enabled = False
            CheckBoxRenta.Checked = False
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("DameClv_Session")
            LocClv_session = BaseII.dicoPar("@Clv_Session")
        ElseIf opcion = "C" Then
            ComboBoxPlazas.Enabled = False
            ComboBoxServicios.Enabled = False
            ListBoxPlazas.Enabled = False
            ListBoxServicios.Enabled = False
            AgregarDistribuidor.Enabled = False
            AgregarServicio.Enabled = False
            EliminarDistribuidor.Enabled = False
            EliminarServicio.Enabled = False
            BindingNavigatorPromocion.Enabled = False
            TextBoxContratacion.Enabled = False
            TextBoxNombre.Enabled = False
            TextBoxMensualidad.Enabled = False
            TextBoxPlazoForzoso.Enabled = False
            DateTimePickerFinal.Enabled = False
            DateTimePickerInicio.Enabled = False
            CheckBoxActiva.Enabled = False
            LlenaComboPlaza()
            LlenaComboServicio()
            LlenaListPlaza()
            LlenaListServicio()
            LlenaDatosPrincipales()
        ElseIf opcion = "M" Then
            BindingNavigatorDeleteItem.Enabled = True
            LlenaComboPlaza()
            LlenaComboServicio()
            LlenaListPlaza()
            LlenaListServicio()
            LlenaDatosPrincipales()
        End If
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub BindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorSaveItem.Click
        If TextBoxNombre.Text.Trim.Length = 0 Then
            MsgBox("No se puede guardar los cambios con datos incompletos.")
            Exit Sub
        End If
        If CheckBoxMensualidad.CheckState = CheckState.Checked And (TextBoxMensualidad.Text.Trim.Length = 0 Or tbMAdicional1.Text.Trim.Length = 0 Or tbMAdicional2.Text.Trim.Length = 0) Then
            MsgBox("No se puede guardar los cambios con datos incompletos.")
            Exit Sub
        End If
        If CheckBoxContratacion.CheckState = CheckState.Checked And (TextBoxContratacion.Text.Trim.Length = 0 Or tbCAdicional1.Text.Trim.Length = 0 Or tbCAdicional2.Text.Trim.Length = 0) Then
            MsgBox("No se puede guardar los cambios con datos incompletos.")
            Exit Sub
        End If
        
        If IsNumeric(TextBoxContratacion.Text) = False Then TextBoxContratacion.Text = 0
        If IsNumeric(TextBoxMensualidad.Text) = False Then TextBoxMensualidad.Text = 0
        If IsNumeric(tbCAdicional1.Text) = False Then tbCAdicional1.Text = 0
        If IsNumeric(tbCAdicional2.Text) = False Then tbCAdicional2.Text = 0
        If IsNumeric(tbMAdicional1.Text) = False Then tbMAdicional1.Text = 0
        If IsNumeric(tbMAdicional2.Text) = False Then tbMAdicional2.Text = 0
        Try
            If ClvPromocion = 0 Or opcion = "N" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, TextBoxNombre.Text)
                BaseII.CreateMyParameter("@contratacion", SqlDbType.Float, TextBoxContratacion.Text)
                BaseII.CreateMyParameter("@mensualidad", SqlDbType.Float, TextBoxMensualidad.Text)
                BaseII.CreateMyParameter("@plazoforzoso", SqlDbType.Float, UPplazo.Text)
                BaseII.CreateMyParameter("@clvpromocion", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@activa", SqlDbType.Bit, CheckBoxActiva.CheckState)
                BaseII.CreateMyParameter("@fechainicial", SqlDbType.DateTime, DateTimePickerInicio.Value)
                BaseII.CreateMyParameter("@fechafinal", SqlDbType.DateTime, DateTimePickerFinal.Value)
                BaseII.CreateMyParameter("@ConAdic1", SqlDbType.Float, tbCAdicional1.Text)
                BaseII.CreateMyParameter("@ConAdic2", SqlDbType.Float, tbCAdicional2.Text)
                BaseII.CreateMyParameter("@MenAdic1", SqlDbType.Float, tbMAdicional1.Text)
                BaseII.CreateMyParameter("@MenAdic2", SqlDbType.Float, tbMAdicional2.Text)
                BaseII.CreateMyParameter("@BndContratacion", SqlDbType.Bit, CheckBoxContratacion.CheckState)
                BaseII.CreateMyParameter("@BndMensualidad", SqlDbType.Bit, CheckBoxMensualidad.CheckState)
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, LocClv_session)
                BaseII.ProcedimientoOutPut("InsertaPromocionNew")
                ClvPromocion = BaseII.dicoPar("@clvpromocion")
                TextBoxClave.Text = ClvPromocion.ToString
                ComboBoxPlazas.Enabled = True
                ComboBoxServicios.Enabled = True
                ListBoxPlazas.Enabled = True
                ListBoxServicios.Enabled = True
                AgregarDistribuidor.Enabled = True
                AgregarServicio.Enabled = True
                EliminarDistribuidor.Enabled = True
                EliminarServicio.Enabled = True
                BindingNavigatorDeleteItem.Enabled = True
                LlenaComboPlaza()
                LlenaComboServicio()
            Else
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, TextBoxNombre.Text)
                BaseII.CreateMyParameter("@contratacion", SqlDbType.Float, TextBoxContratacion.Text)
                BaseII.CreateMyParameter("@mensualidad", SqlDbType.Float, TextBoxMensualidad.Text)
                BaseII.CreateMyParameter("@plazoforzoso", SqlDbType.Float, UPplazo.Text)
                BaseII.CreateMyParameter("@clvpromocion", SqlDbType.Int, ClvPromocion)
                BaseII.CreateMyParameter("@activa", SqlDbType.Bit, CheckBoxActiva.CheckState)
                BaseII.CreateMyParameter("@fechainicial", SqlDbType.DateTime, DateTimePickerInicio.Value)
                BaseII.CreateMyParameter("@fechafinal", SqlDbType.DateTime, DateTimePickerFinal.Value)
                BaseII.CreateMyParameter("@ConAdic1", SqlDbType.Float, tbCAdicional1.Text)
                BaseII.CreateMyParameter("@ConAdic2", SqlDbType.Float, tbCAdicional2.Text)
                BaseII.CreateMyParameter("@MenAdic1", SqlDbType.Float, tbMAdicional1.Text)
                BaseII.CreateMyParameter("@MenAdic2", SqlDbType.Float, tbMAdicional2.Text)
                BaseII.CreateMyParameter("@BndContratacion", SqlDbType.Bit, CheckBoxContratacion.CheckState)
                BaseII.CreateMyParameter("@BndMensualidad", SqlDbType.Bit, CheckBoxMensualidad.CheckState)
                BaseII.Inserta("ModificaPromocion")
            End If
            MsgBox("Datos guardados exitosamente.")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub AgregarDistribuidor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarDistribuidor.Click
        If ComboBoxPlazas.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlazas.SelectedValue)
        BaseII.CreateMyParameter("@clvpromocion", SqlDbType.Int, ClvPromocion)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.Inserta("SP_Rel_Promocion_Plaza")
        LlenaComboPlaza()
        LlenaListPlaza()
    End Sub

    Private Sub EliminarDistribuidor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarDistribuidor.Click
        If ListBoxPlazas.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ListBoxPlazas.SelectedValue)
        BaseII.CreateMyParameter("@clvpromocion", SqlDbType.Int, ClvPromocion)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.Inserta("SP_Rel_Promocion_Plaza")
        LlenaComboPlaza()
        LlenaListPlaza()
    End Sub

    Private Sub AgregarServicio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarServicio.Click
        If ComboBoxServicios.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, ComboBoxServicios.SelectedValue)
        BaseII.CreateMyParameter("@clvpromocion", SqlDbType.Int, ClvPromocion)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.Inserta("SP_Rel_Promocion_Servicio")
       
        LlenaComboServicio()
        LlenaListServicio()
    End Sub

    Private Sub EliminarServicio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarServicio.Click
        If ListBoxServicios.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, ListBoxServicios.SelectedValue)
        BaseII.CreateMyParameter("@clvpromocion", SqlDbType.Int, ClvPromocion)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.Inserta("SP_Rel_Promocion_Servicio")
        LlenaComboServicio()
        LlenaListServicio()
    End Sub

    Private Sub LlenaComboPlaza()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_promocion", SqlDbType.Int, ClvPromocion)
        ComboBoxPlazas.DataSource = BaseII.ConsultaDT("MuestraPlazaPromocion")
        If ComboBoxPlazas.Items.Count = 0 Then
            ComboBoxPlazas.Text = ""
        End If
    End Sub

    Private Sub LlenaComboServicio()
        BaseII.limpiaParametros()
        ComboBoxTipSer.DataSource = BaseII.ConsultaDT("MuestraTipSerPrincipalPromociones")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_promocion", SqlDbType.Int, ClvPromocion)
        BaseII.CreateMyParameter("@clv_tipser", SqlDbType.Int, ComboBoxTipSer.SelectedValue)
        ComboBoxServicios.DataSource = BaseII.ConsultaDT("MuestraServicioPromocion")
        If ComboBoxServicios.Items.Count = 0 Then
            ComboBoxServicios.Text = ""
        End If
    End Sub

    Private Sub LlenaListPlaza()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_promocion", SqlDbType.Int, ClvPromocion)
        ListBoxPlazas.DataSource = BaseII.ConsultaDT("MuestraPlazaTienePromocion")
    End Sub

    Private Sub LlenaListServicio()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_promocion", SqlDbType.Int, ClvPromocion)
        BaseII.CreateMyParameter("@clv_tipser", SqlDbType.Int, ComboBoxTipSer.SelectedValue)
        ListBoxServicios.DataSource = BaseII.ConsultaDT("MuestraServicioTienePromocion")
        
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_promocion", SqlDbType.Int, ClvPromocion)
        BaseII.Inserta("EliminaPromocion")
        MsgBox("Promoción eliminada exitosamente.")
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub LlenaDatosPrincipales()
        Dim comando As New SqlCommand()
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        comando.Connection = conexion
        comando.CommandText = "exec DameDatosPromocion " + ClvPromocion.ToString
        Dim reader As SqlDataReader = comando.ExecuteReader()
        reader.Read()
        TextBoxNombre.Text = reader(1).ToString
        TextBoxContratacion.Text = reader(2).ToString
        TextBoxMensualidad.Text = reader(3).ToString
        UPplazo.Value = reader(4).ToString
        CheckBoxActiva.Checked = reader(5)
        DateTimePickerInicio.Value = reader(6)
        DateTimePickerFinal.Value = reader(7)
        tbCAdicional1.Text = reader(8).ToString
        tbCAdicional2.Text = reader(9).ToString
        tbMAdicional1.Text = reader(10).ToString
        tbMAdicional2.Text = reader(11).ToString
        TextBoxClave.Text = ClvPromocion
        CheckBoxContratacion.Checked = reader(12)
        CheckBoxMensualidad.Checked = reader(13)
        CheckBoxRenta.Checked = reader(14)
        reader.Close()
        conexion.Close()
    End Sub

    Private Function quitaCeros(ByVal cadena As String)

    End Function

    Private Sub Label9_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub DateTimePickerInicio_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePickerInicio.ValueChanged
        DateTimePickerFinal.MinDate = DateTimePickerInicio.Value
        'DateTimePickerInicio.MaxDate = DateTimePickerFinal.Value
    End Sub

    Private Sub DateTimePickerFinal_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePickerFinal.ValueChanged
        DateTimePickerInicio.MaxDate = DateTimePickerFinal.Value
    End Sub

    Private Sub CheckBoxContratacion_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBoxContratacion.CheckedChanged
        If CheckBoxContratacion.CheckState = CheckState.Checked Then
            PanelContratacion.Enabled = True
        Else
            PanelContratacion.Enabled = False
        End If
    End Sub

    Private Sub CheckBoxMensualidad_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBoxMensualidad.CheckedChanged
        If CheckBoxMensualidad.CheckState = CheckState.Checked Then
            PanelMensualidad.Enabled = True
        Else
            PanelMensualidad.Enabled = False
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBoxRenta.CheckedChanged
        If TextBoxClave.Text = "0" Then
            CheckBoxRenta.Checked = False

        End If
        If CheckBoxRenta.CheckState = CheckState.Checked Then
            PanelRenta.Enabled = True
        Else
            PanelRenta.Enabled = False
        End If
    End Sub

    Private Sub btnGuardarModelo_Click(sender As Object, e As EventArgs) Handles btnGuardarModelo.Click
        'If ListBoxServicios.Items.Count = 0 Then
        '    MsgBox("Debe haber al menos un servicio agregado a la promoción.")
        '    Exit Sub
        'End If
        listaServ = New ArrayList
        If ListBoxServicios.Items.Count > 0 Then
            Dim total As Integer = ListBoxServicios.Items.Count
            Dim i As Integer = 0
            While i < total
                ListBoxServicios.SelectedIndex = i
                Dim ls As ListaServicios = New ListaServicios(ListBoxServicios.Text, ListBoxServicios.SelectedValue)
                listaServ.Add(ls)
                i = i + 1
            End While
        End If
        FrmRentaPromociones.ShowDialog()
    End Sub

    Private Sub ComboBoxTipSer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxTipSer.SelectedIndexChanged
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_promocion", SqlDbType.Int, ClvPromocion)
        BaseII.CreateMyParameter("@clv_tipser", SqlDbType.Int, ComboBoxTipSer.SelectedValue)
        ComboBoxServicios.DataSource = BaseII.ConsultaDT("MuestraServicioPromocion")
    End Sub
End Class