<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwQuiz
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.IDEncuesta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Encuesta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ButtonNuevo = New System.Windows.Forms.Button()
        Me.ButtonConsultar = New System.Windows.Forms.Button()
        Me.ButtonSalir = New System.Windows.Forms.Button()
        Me.ButtonModificar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ButtonBusContrato = New System.Windows.Forms.Button()
        Me.ButtonBusNombre = New System.Windows.Forms.Button()
        Me.TextBoxBusNombre = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxBusContrato = New System.Windows.Forms.TextBox()
        Me.ComboBoxEncuesta = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ButtonBusEncuesta = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDEncuesta, Me.Contrato, Me.Nombre, Me.Encuesta})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Location = New System.Drawing.Point(368, 41)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(752, 816)
        Me.DataGridView1.TabIndex = 0
        '
        'IDEncuesta
        '
        Me.IDEncuesta.DataPropertyName = "IDEncuesta"
        Me.IDEncuesta.HeaderText = "IDEncuesta"
        Me.IDEncuesta.Name = "IDEncuesta"
        Me.IDEncuesta.ReadOnly = True
        Me.IDEncuesta.Visible = False
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        Me.Contrato.Width = 70
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 250
        '
        'Encuesta
        '
        Me.Encuesta.DataPropertyName = "Encuesta"
        Me.Encuesta.HeaderText = "Encuesta"
        Me.Encuesta.Name = "Encuesta"
        Me.Encuesta.ReadOnly = True
        Me.Encuesta.Width = 250
        '
        'ButtonNuevo
        '
        Me.ButtonNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonNuevo.Location = New System.Drawing.Point(1145, 41)
        Me.ButtonNuevo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonNuevo.Name = "ButtonNuevo"
        Me.ButtonNuevo.Size = New System.Drawing.Size(181, 44)
        Me.ButtonNuevo.TabIndex = 1
        Me.ButtonNuevo.Text = "&NUEVO"
        Me.ButtonNuevo.UseVisualStyleBackColor = True
        '
        'ButtonConsultar
        '
        Me.ButtonConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonConsultar.Location = New System.Drawing.Point(1145, 92)
        Me.ButtonConsultar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonConsultar.Name = "ButtonConsultar"
        Me.ButtonConsultar.Size = New System.Drawing.Size(181, 44)
        Me.ButtonConsultar.TabIndex = 3
        Me.ButtonConsultar.Text = "&CONSULTAR"
        Me.ButtonConsultar.UseVisualStyleBackColor = True
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(1145, 812)
        Me.ButtonSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(181, 44)
        Me.ButtonSalir.TabIndex = 4
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'ButtonModificar
        '
        Me.ButtonModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonModificar.Location = New System.Drawing.Point(1145, 144)
        Me.ButtonModificar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonModificar.Name = "ButtonModificar"
        Me.ButtonModificar.Size = New System.Drawing.Size(181, 44)
        Me.ButtonModificar.TabIndex = 5
        Me.ButtonModificar.Text = "&MODIFICAR"
        Me.ButtonModificar.UseVisualStyleBackColor = True
        Me.ButtonModificar.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 89)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 18)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Contrato"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 206)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 18)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Nombre"
        '
        'ButtonBusContrato
        '
        Me.ButtonBusContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusContrato.Location = New System.Drawing.Point(16, 144)
        Me.ButtonBusContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBusContrato.Name = "ButtonBusContrato"
        Me.ButtonBusContrato.Size = New System.Drawing.Size(100, 28)
        Me.ButtonBusContrato.TabIndex = 16
        Me.ButtonBusContrato.Text = "Buscar"
        Me.ButtonBusContrato.UseVisualStyleBackColor = True
        '
        'ButtonBusNombre
        '
        Me.ButtonBusNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusNombre.Location = New System.Drawing.Point(17, 261)
        Me.ButtonBusNombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBusNombre.Name = "ButtonBusNombre"
        Me.ButtonBusNombre.Size = New System.Drawing.Size(100, 28)
        Me.ButtonBusNombre.TabIndex = 15
        Me.ButtonBusNombre.Text = "Buscar"
        Me.ButtonBusNombre.UseVisualStyleBackColor = True
        '
        'TextBoxBusNombre
        '
        Me.TextBoxBusNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxBusNombre.Location = New System.Drawing.Point(17, 228)
        Me.TextBoxBusNombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxBusNombre.Name = "TextBoxBusNombre"
        Me.TextBoxBusNombre.Size = New System.Drawing.Size(328, 24)
        Me.TextBoxBusNombre.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 27)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 29)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Buscar:"
        '
        'TextBoxBusContrato
        '
        Me.TextBoxBusContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxBusContrato.Location = New System.Drawing.Point(16, 111)
        Me.TextBoxBusContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxBusContrato.Name = "TextBoxBusContrato"
        Me.TextBoxBusContrato.Size = New System.Drawing.Size(193, 24)
        Me.TextBoxBusContrato.TabIndex = 14
        '
        'ComboBoxEncuesta
        '
        Me.ComboBoxEncuesta.DisplayMember = "Nombre"
        Me.ComboBoxEncuesta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxEncuesta.FormattingEnabled = True
        Me.ComboBoxEncuesta.Location = New System.Drawing.Point(16, 352)
        Me.ComboBoxEncuesta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxEncuesta.Name = "ComboBoxEncuesta"
        Me.ComboBoxEncuesta.Size = New System.Drawing.Size(329, 26)
        Me.ComboBoxEncuesta.TabIndex = 19
        Me.ComboBoxEncuesta.ValueMember = "IDEncuesta"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(16, 330)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 18)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Encuesta"
        '
        'ButtonBusEncuesta
        '
        Me.ButtonBusEncuesta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusEncuesta.Location = New System.Drawing.Point(16, 388)
        Me.ButtonBusEncuesta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBusEncuesta.Name = "ButtonBusEncuesta"
        Me.ButtonBusEncuesta.Size = New System.Drawing.Size(100, 28)
        Me.ButtonBusEncuesta.TabIndex = 21
        Me.ButtonBusEncuesta.Text = "Buscar"
        Me.ButtonBusEncuesta.UseVisualStyleBackColor = True
        '
        'BrwQuiz
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1355, 912)
        Me.Controls.Add(Me.ButtonBusEncuesta)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ComboBoxEncuesta)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ButtonBusContrato)
        Me.Controls.Add(Me.ButtonBusNombre)
        Me.Controls.Add(Me.TextBoxBusContrato)
        Me.Controls.Add(Me.TextBoxBusNombre)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ButtonModificar)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.ButtonConsultar)
        Me.Controls.Add(Me.ButtonNuevo)
        Me.Controls.Add(Me.DataGridView1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwQuiz"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Encuestas a Clientes"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents IDEncuesta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Encuesta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ButtonNuevo As System.Windows.Forms.Button
    Friend WithEvents ButtonConsultar As System.Windows.Forms.Button
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents ButtonModificar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ButtonBusContrato As System.Windows.Forms.Button
    Friend WithEvents ButtonBusNombre As System.Windows.Forms.Button
    Friend WithEvents TextBoxBusNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBoxBusContrato As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxEncuesta As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ButtonBusEncuesta As System.Windows.Forms.Button
End Class
