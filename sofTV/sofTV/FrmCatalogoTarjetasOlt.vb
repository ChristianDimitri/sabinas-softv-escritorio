﻿Imports System.Data.SqlClient
Imports System.Text


Public Class FrmCatalogoTarjetasOlt


    Private Sub FrmCatalogoPostes_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        MuestraDescPoste(ClaveOlt)
    End Sub


    Private Sub FrmCatalogoPostes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        MuestraDescPoste(ClaveOlt)
        Me.TxtDescripcion.ReadOnly = True
        Me.txtPuertos.ReadOnly = True
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
    Private Sub MuestraDescPoste(ByVal op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraDescTarjetaOlt ")
        strSQL.Append(CStr(op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.DataGridView1.DataSource = bindingSource
            Me.DataGridView1.Columns(1).Width = 250
            'Me.CONTRATOLabel1.Text = CStr(Folio)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub InsertaNueDescPoste(ByVal clave As Long, ByVal descripcion As String, ByVal puertos As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("InsertaNueCatalogoTarjetasOlt", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@Clave", SqlDbType.BigInt)
        Dim par2 As New SqlParameter("@descripcion", SqlDbType.VarChar, 250)
        Dim par3 As New SqlParameter("@PUERTOS", SqlDbType.Int)
        Dim par4 As New SqlParameter("@IdOlt", SqlDbType.Int)

        par1.Direction = ParameterDirection.Input
        par2.Direction = ParameterDirection.Input
        par3.Direction = ParameterDirection.Input
        par4.Direction = ParameterDirection.Input

        par1.Value = clave
        par2.Value = descripcion
        par3.Value = puertos
        par4.Value = ClaveOlt

        com.Parameters.Add(par1)
        com.Parameters.Add(par2)
        com.Parameters.Add(par3)
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub

    Private Sub Modificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Modificar.Click
        Me.TxtDescripcion.ReadOnly = False
        Me.txtPuertos.ReadOnly = False
        Me.Agregar.Text = "GUARDAR"
        If IsNumeric(Me.DataGridView1.Item(0, Me.DataGridView1.CurrentRow.Index).Value) = True Then
            Me.TxtClave.Text = (Me.DataGridView1.Item(0, Me.DataGridView1.CurrentRow.Index).Value)
            Me.TxtDescripcion.Text = (Me.DataGridView1.Item(1, Me.DataGridView1.CurrentRow.Index).Value.ToString)
            Me.txtPuertos.Text = (Me.DataGridView1.Item(2, Me.DataGridView1.CurrentRow.Index).Value.ToString)

        End If

    End Sub

    Private Sub Agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Agregar.Click
        Me.TxtDescripcion.ReadOnly = False
        Me.txtPuertos.ReadOnly = False
        If Me.TxtDescripcion.Text.Length > 0 And Me.txtPuertos.Text.Length > 0 And IsNumeric(txtPuertos.Text) Then
            InsertaNueDescPoste(CLng(Me.TxtClave.Text), Me.TxtDescripcion.Text, Me.txtPuertos.Text)
            Me.TxtClave.Text = "0"
            Me.TxtDescripcion.Text = ""
            Me.txtPuertos.Text = ""
            MuestraDescPoste(ClaveOlt)
            Me.TxtDescripcion.ReadOnly = True
            Me.txtPuertos.ReadOnly = True
            Me.Agregar.Text = "AGREGAR"
        End If
    End Sub

    Private Sub CONTECNICOSBindingNavigator_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub


    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        Me.Close()
    End Sub


End Class