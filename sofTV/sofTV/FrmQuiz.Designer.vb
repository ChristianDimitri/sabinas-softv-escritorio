<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmQuiz
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmQuiz))
        Me.ButtonSig = New System.Windows.Forms.Button()
        Me.LabelPregunta = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.LabelContrato = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CheckBoxSoloInternet = New System.Windows.Forms.CheckBox()
        Me.LabelCelular = New System.Windows.Forms.Label()
        Me.LabelTelefono = New System.Windows.Forms.Label()
        Me.LabelCiudad = New System.Windows.Forms.Label()
        Me.LabelColonia = New System.Windows.Forms.Label()
        Me.LabelNumero = New System.Windows.Forms.Label()
        Me.LabelCalle = New System.Windows.Forms.Label()
        Me.LabelNombre = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBoxContrato = New System.Windows.Forms.TextBox()
        Me.ButtonBuscar = New System.Windows.Forms.Button()
        Me.ButtonAnt = New System.Windows.Forms.Button()
        Me.GroupBoxRespuestas = New System.Windows.Forms.GroupBox()
        Me.LabelNDeM = New System.Windows.Forms.Label()
        Me.ButtonSalir = New System.Windows.Forms.Button()
        Me.BindingNavigatorQuiz = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.TSBCancelar = New System.Windows.Forms.ToolStripButton()
        Me.TSBGuardar = New System.Windows.Forms.ToolStripButton()
        Me.TSBNuevo = New System.Windows.Forms.ToolStripButton()
        Me.LabelEncuesta = New System.Windows.Forms.Label()
        Me.LabelRespuesta = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.BindingNavigatorQuiz, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigatorQuiz.SuspendLayout()
        Me.SuspendLayout()
        '
        'ButtonSig
        '
        Me.ButtonSig.Enabled = False
        Me.ButtonSig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSig.Location = New System.Drawing.Point(628, 830)
        Me.ButtonSig.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonSig.Name = "ButtonSig"
        Me.ButtonSig.Size = New System.Drawing.Size(135, 44)
        Me.ButtonSig.TabIndex = 0
        Me.ButtonSig.Text = "Siguiente >"
        Me.ButtonSig.UseVisualStyleBackColor = True
        '
        'LabelPregunta
        '
        Me.LabelPregunta.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelPregunta.ForeColor = System.Drawing.Color.Red
        Me.LabelPregunta.Location = New System.Drawing.Point(203, 439)
        Me.LabelPregunta.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelPregunta.Name = "LabelPregunta"
        Me.LabelPregunta.Size = New System.Drawing.Size(827, 84)
        Me.LabelPregunta.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Controls.Add(Me.LabelContrato)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.CheckBoxSoloInternet)
        Me.GroupBox1.Controls.Add(Me.LabelCelular)
        Me.GroupBox1.Controls.Add(Me.LabelTelefono)
        Me.GroupBox1.Controls.Add(Me.LabelCiudad)
        Me.GroupBox1.Controls.Add(Me.LabelColonia)
        Me.GroupBox1.Controls.Add(Me.LabelNumero)
        Me.GroupBox1.Controls.Add(Me.LabelCalle)
        Me.GroupBox1.Controls.Add(Me.LabelNombre)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(16, 97)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(1168, 272)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(624, 25)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(517, 230)
        Me.TreeView1.TabIndex = 19
        '
        'LabelContrato
        '
        Me.LabelContrato.Location = New System.Drawing.Point(203, 31)
        Me.LabelContrato.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelContrato.Name = "LabelContrato"
        Me.LabelContrato.Size = New System.Drawing.Size(400, 23)
        Me.LabelContrato.TabIndex = 18
        Me.LabelContrato.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label1.Location = New System.Drawing.Point(71, 31)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(133, 23)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Contrato: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'CheckBoxSoloInternet
        '
        Me.CheckBoxSoloInternet.AutoSize = True
        Me.CheckBoxSoloInternet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxSoloInternet.Enabled = False
        Me.CheckBoxSoloInternet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxSoloInternet.ForeColor = System.Drawing.Color.SteelBlue
        Me.CheckBoxSoloInternet.Location = New System.Drawing.Point(61, 233)
        Me.CheckBoxSoloInternet.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBoxSoloInternet.Name = "CheckBoxSoloInternet"
        Me.CheckBoxSoloInternet.Size = New System.Drawing.Size(126, 22)
        Me.CheckBoxSoloInternet.TabIndex = 16
        Me.CheckBoxSoloInternet.Text = "Sólo Internet"
        Me.CheckBoxSoloInternet.UseVisualStyleBackColor = True
        '
        'LabelCelular
        '
        Me.LabelCelular.Location = New System.Drawing.Point(203, 171)
        Me.LabelCelular.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelCelular.Name = "LabelCelular"
        Me.LabelCelular.Size = New System.Drawing.Size(400, 23)
        Me.LabelCelular.TabIndex = 15
        Me.LabelCelular.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelTelefono
        '
        Me.LabelTelefono.Location = New System.Drawing.Point(203, 194)
        Me.LabelTelefono.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelTelefono.Name = "LabelTelefono"
        Me.LabelTelefono.Size = New System.Drawing.Size(400, 23)
        Me.LabelTelefono.TabIndex = 14
        Me.LabelTelefono.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCiudad
        '
        Me.LabelCiudad.Location = New System.Drawing.Point(203, 148)
        Me.LabelCiudad.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelCiudad.Name = "LabelCiudad"
        Me.LabelCiudad.Size = New System.Drawing.Size(400, 23)
        Me.LabelCiudad.TabIndex = 13
        Me.LabelCiudad.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelColonia
        '
        Me.LabelColonia.Location = New System.Drawing.Point(203, 124)
        Me.LabelColonia.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelColonia.Name = "LabelColonia"
        Me.LabelColonia.Size = New System.Drawing.Size(400, 23)
        Me.LabelColonia.TabIndex = 12
        Me.LabelColonia.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNumero
        '
        Me.LabelNumero.Location = New System.Drawing.Point(203, 101)
        Me.LabelNumero.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelNumero.Name = "LabelNumero"
        Me.LabelNumero.Size = New System.Drawing.Size(400, 23)
        Me.LabelNumero.TabIndex = 11
        Me.LabelNumero.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCalle
        '
        Me.LabelCalle.Location = New System.Drawing.Point(203, 78)
        Me.LabelCalle.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelCalle.Name = "LabelCalle"
        Me.LabelCalle.Size = New System.Drawing.Size(400, 23)
        Me.LabelCalle.TabIndex = 10
        Me.LabelCalle.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNombre
        '
        Me.LabelNombre.Location = New System.Drawing.Point(203, 54)
        Me.LabelNombre.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelNombre.Name = "LabelNombre"
        Me.LabelNombre.Size = New System.Drawing.Size(400, 23)
        Me.LabelNombre.TabIndex = 9
        Me.LabelNombre.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label7.Location = New System.Drawing.Point(71, 171)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(133, 23)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Celular: "
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label6.Location = New System.Drawing.Point(71, 194)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(133, 23)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Teléfono: "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label5.Location = New System.Drawing.Point(71, 148)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(133, 23)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Ciudad: "
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label4.Location = New System.Drawing.Point(71, 124)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(133, 23)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Colonia: "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label3.Location = New System.Drawing.Point(71, 101)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(135, 23)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "#: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(71, 78)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(133, 23)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Calle: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label8.Location = New System.Drawing.Point(71, 54)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(133, 23)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Nombre: "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(96, 48)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(107, 28)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Contrato:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextBoxContrato
        '
        Me.TextBoxContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxContrato.Location = New System.Drawing.Point(208, 50)
        Me.TextBoxContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxContrato.Name = "TextBoxContrato"
        Me.TextBoxContrato.Size = New System.Drawing.Size(132, 24)
        Me.TextBoxContrato.TabIndex = 19
        '
        'ButtonBuscar
        '
        Me.ButtonBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBuscar.Location = New System.Drawing.Point(349, 49)
        Me.ButtonBuscar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBuscar.Name = "ButtonBuscar"
        Me.ButtonBuscar.Size = New System.Drawing.Size(57, 28)
        Me.ButtonBuscar.TabIndex = 18
        Me.ButtonBuscar.Text = "..."
        Me.ButtonBuscar.UseVisualStyleBackColor = True
        '
        'ButtonAnt
        '
        Me.ButtonAnt.Enabled = False
        Me.ButtonAnt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAnt.Location = New System.Drawing.Point(485, 830)
        Me.ButtonAnt.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonAnt.Name = "ButtonAnt"
        Me.ButtonAnt.Size = New System.Drawing.Size(135, 44)
        Me.ButtonAnt.TabIndex = 21
        Me.ButtonAnt.Text = "< Anterior"
        Me.ButtonAnt.UseVisualStyleBackColor = True
        '
        'GroupBoxRespuestas
        '
        Me.GroupBoxRespuestas.Location = New System.Drawing.Point(203, 562)
        Me.GroupBoxRespuestas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBoxRespuestas.Name = "GroupBoxRespuestas"
        Me.GroupBoxRespuestas.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBoxRespuestas.Size = New System.Drawing.Size(827, 260)
        Me.GroupBoxRespuestas.TabIndex = 22
        Me.GroupBoxRespuestas.TabStop = False
        '
        'LabelNDeM
        '
        Me.LabelNDeM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelNDeM.Location = New System.Drawing.Point(800, 535)
        Me.LabelNDeM.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelNDeM.Name = "LabelNDeM"
        Me.LabelNDeM.Size = New System.Drawing.Size(229, 23)
        Me.LabelNDeM.TabIndex = 24
        Me.LabelNDeM.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(1004, 853)
        Me.ButtonSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(181, 44)
        Me.ButtonSalir.TabIndex = 25
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'BindingNavigatorQuiz
        '
        Me.BindingNavigatorQuiz.AddNewItem = Nothing
        Me.BindingNavigatorQuiz.CountItem = Nothing
        Me.BindingNavigatorQuiz.DeleteItem = Nothing
        Me.BindingNavigatorQuiz.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorQuiz.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.BindingNavigatorQuiz.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSBCancelar, Me.TSBGuardar, Me.TSBNuevo})
        Me.BindingNavigatorQuiz.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigatorQuiz.MoveFirstItem = Nothing
        Me.BindingNavigatorQuiz.MoveLastItem = Nothing
        Me.BindingNavigatorQuiz.MoveNextItem = Nothing
        Me.BindingNavigatorQuiz.MovePreviousItem = Nothing
        Me.BindingNavigatorQuiz.Name = "BindingNavigatorQuiz"
        Me.BindingNavigatorQuiz.PositionItem = Nothing
        Me.BindingNavigatorQuiz.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigatorQuiz.Size = New System.Drawing.Size(1201, 27)
        Me.BindingNavigatorQuiz.TabIndex = 26
        Me.BindingNavigatorQuiz.Text = "BindingNavigator1"
        '
        'TSBCancelar
        '
        Me.TSBCancelar.Enabled = False
        Me.TSBCancelar.Image = CType(resources.GetObject("TSBCancelar.Image"), System.Drawing.Image)
        Me.TSBCancelar.Name = "TSBCancelar"
        Me.TSBCancelar.RightToLeftAutoMirrorImage = True
        Me.TSBCancelar.Size = New System.Drawing.Size(113, 24)
        Me.TSBCancelar.Text = "&CANCELAR"
        '
        'TSBGuardar
        '
        Me.TSBGuardar.Enabled = False
        Me.TSBGuardar.Image = CType(resources.GetObject("TSBGuardar.Image"), System.Drawing.Image)
        Me.TSBGuardar.Name = "TSBGuardar"
        Me.TSBGuardar.Size = New System.Drawing.Size(107, 24)
        Me.TSBGuardar.Text = "&GUARDAR"
        '
        'TSBNuevo
        '
        Me.TSBNuevo.Image = CType(resources.GetObject("TSBNuevo.Image"), System.Drawing.Image)
        Me.TSBNuevo.Name = "TSBNuevo"
        Me.TSBNuevo.RightToLeftAutoMirrorImage = True
        Me.TSBNuevo.Size = New System.Drawing.Size(86, 24)
        Me.TSBNuevo.Text = "&NUEVO"
        Me.TSBNuevo.Visible = False
        '
        'LabelEncuesta
        '
        Me.LabelEncuesta.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelEncuesta.ForeColor = System.Drawing.Color.DarkBlue
        Me.LabelEncuesta.Location = New System.Drawing.Point(203, 385)
        Me.LabelEncuesta.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelEncuesta.Name = "LabelEncuesta"
        Me.LabelEncuesta.Size = New System.Drawing.Size(827, 52)
        Me.LabelEncuesta.TabIndex = 27
        Me.LabelEncuesta.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelRespuesta
        '
        Me.LabelRespuesta.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelRespuesta.ForeColor = System.Drawing.Color.Black
        Me.LabelRespuesta.Location = New System.Drawing.Point(203, 524)
        Me.LabelRespuesta.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelRespuesta.Name = "LabelRespuesta"
        Me.LabelRespuesta.Size = New System.Drawing.Size(55, 42)
        Me.LabelRespuesta.TabIndex = 28
        Me.LabelRespuesta.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'FrmQuiz
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1201, 912)
        Me.Controls.Add(Me.LabelNDeM)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.LabelRespuesta)
        Me.Controls.Add(Me.LabelEncuesta)
        Me.Controls.Add(Me.BindingNavigatorQuiz)
        Me.Controls.Add(Me.GroupBoxRespuestas)
        Me.Controls.Add(Me.ButtonAnt)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.TextBoxContrato)
        Me.Controls.Add(Me.ButtonBuscar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.LabelPregunta)
        Me.Controls.Add(Me.ButtonSig)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmQuiz"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cuestionario"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.BindingNavigatorQuiz, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigatorQuiz.ResumeLayout(False)
        Me.BindingNavigatorQuiz.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButtonSig As System.Windows.Forms.Button
    Friend WithEvents LabelPregunta As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxSoloInternet As System.Windows.Forms.CheckBox
    Friend WithEvents LabelCelular As System.Windows.Forms.Label
    Friend WithEvents LabelTelefono As System.Windows.Forms.Label
    Friend WithEvents LabelCiudad As System.Windows.Forms.Label
    Friend WithEvents LabelColonia As System.Windows.Forms.Label
    Friend WithEvents LabelNumero As System.Windows.Forms.Label
    Friend WithEvents LabelCalle As System.Windows.Forms.Label
    Friend WithEvents LabelNombre As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBoxContrato As System.Windows.Forms.TextBox
    Friend WithEvents ButtonBuscar As System.Windows.Forms.Button
    Friend WithEvents ButtonAnt As System.Windows.Forms.Button
    Friend WithEvents GroupBoxRespuestas As System.Windows.Forms.GroupBox
    Friend WithEvents LabelNDeM As System.Windows.Forms.Label
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents BindingNavigatorQuiz As System.Windows.Forms.BindingNavigator
    Friend WithEvents TSBCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSBGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSBNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents LabelEncuesta As System.Windows.Forms.Label
    Friend WithEvents LabelContrato As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LabelRespuesta As System.Windows.Forms.Label
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
End Class
