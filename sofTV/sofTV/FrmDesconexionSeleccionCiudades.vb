﻿Public Class FrmDesconexionSeleccionCiudades

    Private Sub MUESTRATBLDESCONEXIONSELECCIONCIUDADES(ByVal CLV_SESSION As Integer, ByVal OP As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        If OP = 0 Then dgvIzq.DataSource = BaseII.ConsultaDT("MUESTRATBLDESCONEXIONSELECCIONCIUDADES")
        If OP = 1 Then dgvDer.DataSource = BaseII.ConsultaDT("MUESTRATBLDESCONEXIONSELECCIONCIUDADES")
    End Sub

    Private Sub INSERTATBLDESCONEXIONSELECCIONCIUDADES(ByVal CLV_SESSION As Integer, ByVal OP As Integer, ByVal CLV_CIUDAD As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@CLV_CIUDAD", SqlDbType.Int, CLV_CIUDAD)
        BaseII.Inserta("INSERTATBLDESCONEXIONSELECCIONCIUDADES")
    End Sub

    Private Sub ELIMINATBLDESCONEXIONSELECCIONCIUDADES(ByVal CLV_SESSION As Integer, ByVal OP As Integer, ByVal CLV_CIUDAD As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@CLV_CIUDAD", SqlDbType.Int, CLV_CIUDAD)
        BaseII.Inserta("ELIMINATBLDESCONEXIONSELECCIONCIUDADES")
    End Sub

    Private Sub DameClv_Session()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameClv_Session")
        eClv_Session = 0
        eClv_Session = CInt(BaseII.dicoPar("@CLV_SESSION").ToString)
    End Sub

    Private Sub FrmDesconexionSeleccionCiudades_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        DameClv_Session()
        MUESTRATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 1)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        INSERTATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 0, dgvIzq.SelectedCells(0).Value)
        MUESTRATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 1)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        INSERTATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 1, 0)
        MUESTRATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 1)
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 0, dgvDer.SelectedCells(0).Value)
        MUESTRATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 1)
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 1, dgvDer.SelectedCells(0).Value)
        MUESTRATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCIUDADES(eClv_Session, 1)
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona una Ciudad.")
            Exit Sub
        End If
        FrmDesconexionSeleccionColonias.Show()
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class