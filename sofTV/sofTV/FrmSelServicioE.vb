Imports System.Data.SqlClient
Public Class FrmSelServicioE

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox.Items.Count > 0 Then
            CON.Open()
            Me.InsertarServiciosTmpTableAdapter.Connection = CON
            Me.InsertarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.InsertarServiciosTmp, CInt(Me.DescripcionListBox.SelectedValue), eClv_Session, 0)
            Me.ConServiciosProTableAdapter.Connection = CON
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, eClv_Session)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            'Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, 0, eClv_Session, 1)
            DescripcionListBox.DataSource = BaseII.ConsultaDT("ConServiciosPro")
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox.Items.Count > 0 Then
            CON.Open()
            Me.InsertarServiciosTmpTableAdapter.Connection = CON
            Me.InsertarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.InsertarServiciosTmp, 0, eClv_Session, 1)
            Me.ConServiciosProTableAdapter.Connection = CON
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, eClv_Session)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            'Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, 0, eClv_Session, 1)
            DescripcionListBox.DataSource = BaseII.ConsultaDT("ConServiciosPro")
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox1.Items.Count > 0 Then
            CON.Open()
            Me.BorrarServiciosTmpTableAdapter.Connection = CON
            Me.BorrarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.BorrarServiciosTmp, CInt(Me.DescripcionListBox1.SelectedValue), eClv_Session, 0)
            Me.ConServiciosProTableAdapter.Connection = CON
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, eClv_Session)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            'Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, 0, eClv_Session, 1)
            DescripcionListBox.DataSource = BaseII.ConsultaDT("ConServiciosPro")
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox1.Items.Count > 0 Then
            CON.Open()
            Me.BorrarServiciosTmpTableAdapter.Connection = CON
            Me.BorrarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.BorrarServiciosTmp, 0, eClv_Session, 1)
            Me.ConServiciosProTableAdapter.Connection = CON
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, eClv_Session)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            'Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, 0, eClv_Session, 1)
            DescripcionListBox.DataSource = BaseII.ConsultaDT("ConServiciosPro")
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub FrmSelServicioE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If rMensajes = True Then
            eClv_Session = rSession
            eTipSer = 3
        End If
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If eEjecutivo = True Or eBndPenetracion = True Then
            eEjecutivo = False
            Me.ConServiciosProTableAdapter.Connection = CON
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, eTipSer)
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, eClv_Session)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 2)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            'Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, eTipSer, eClv_Session, 2)
            DescripcionListBox.DataSource = BaseII.ConsultaDT("ConServiciosPro")

        Else
            Me.ConServiciosProTableAdapter.Connection = CON
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, eTipSer)
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, eClv_Session)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            'Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, eTipSer, eClv_Session, 0)
            DescripcionListBox.DataSource = BaseII.ConsultaDT("ConServiciosPro")
        End If
        
        CON.Close()
    End Sub

    Private Sub Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Aceptar.Click
        If varfrmselcompania = "graficassucursal" Or varfrmselcompania = "graficasdosopciones" Or varfrmselcompania = "graficasventas" Then
            FrmGraficas.entra = 1
            Me.Close()
            varfrmselcompania = ""
            Exit Sub
        End If
        If rMensajes = True Then
            FrmProgramacionMsjPer.Show()
            Me.Close()
            Return
        End If
        If Locbndpen1 = True Then
            FrmRepPenetracion.Show()
            Me.Close()
        End If

        If Me.DescripcionListBox1.Items.Count > 0 Then 'And (eOpVentas = 2 Or eOpVentas = 3 Or eOpVentas = 20 Or eOpVentas = 21) Then
            FrmImprimirComision.Show()
            Me.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Locbndpen1 = False
        Me.Close()
    End Sub
End Class