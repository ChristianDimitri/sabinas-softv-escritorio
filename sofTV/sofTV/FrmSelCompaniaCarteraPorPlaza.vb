﻿Imports System.Data.SqlClient
Public Class FrmSelCompaniaCarteraPorPlaza

    Private Sub FrmSelCompaniaCartera_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

    End Sub
    Private Sub Llena_Plaza()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvusuario", SqlDbType.Int, GloClvUsuario)
        ComboBoxPlaza.DataSource = BaseII.ConsultaDT("Muestra_PlazasPorUsuario")
        If ComboBoxPlaza.Items.Count <> 0 Then
            ComboBoxPlaza.SelectedIndex = 0
        End If
    End Sub
    Private Sub FrmSelCompaniaCartera_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Por si es uno
        'Dim CONar As New SqlConnection(MiConexion)
        'Dim cmd As New SqlCommand()
        'Dim numero As Integer
        'Try
        '    cmd = New SqlCommand()
        '    CONar.Open()
        '    '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
        '    With cmd
        '        .CommandText = "DameNumCompaniasUsuario"
        '        .Connection = CONar
        '        .CommandTimeout = 0
        '        .CommandType = CommandType.StoredProcedure

        '        Dim prm As New SqlParameter("@ClaveUsuario", SqlDbType.Int)
        '        prm.Direction = ParameterDirection.Input
        '        prm.Value = GloClvUsuario
        '        .Parameters.Add(prm)

        '        Dim prm1 As New SqlParameter("@numero", SqlDbType.Int)
        '        prm1.Direction = ParameterDirection.Output
        '        prm1.Value = 0
        '        .Parameters.Add(prm1)

        '        Dim prm2 As New SqlParameter("@identificador", SqlDbType.BigInt)
        '        prm2.Direction = ParameterDirection.Input
        '        prm2.Value = identificador
        '        .Parameters.Add(prm2)

        '        Dim ia As Integer = .ExecuteNonQuery()

        '        numero = prm1.Value
        '    End With
        '    CONar.Close()
        'Catch ex As Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
        'If numero = 1 Then
        '    'Dim conexion As New SqlConnection(MiConexion)
        '    'conexion.Open()
        '    'Dim comando As New SqlCommand()
        '    'comando.Connection = conexion
        '    'comando.CommandText = " exec DameIdentificador"
        '    'identificador = comando.ExecuteScalar()
        '    'conexion.Close()
        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@Identificador", SqlDbType.BigInt, identificador)
        '    BaseII.CreateMyParameter("@claveusuario", SqlDbType.Int, GloClvUsuario)
        '    BaseII.Inserta("InsertaSeleccionCompaniatblPorPlaza")
        '    llevamealotro()
        '    Exit Sub
        'End If
        'Por si es uni
        colorea(Me, Me.Name)
        Llena_Plaza()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        'Dim conexion2 As New SqlConnection(MiConexion)
        'conexion2.Open()
        'Dim comando2 As New SqlCommand()
        'comando2.Connection = conexion2
        'comando2.CommandText = " exec DameIdentificador"
        'identificador = comando2.ExecuteScalar()
        'conexion2.Close()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(GloClvUsuario))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
        BaseII.Inserta("ProcedureSeleccionCompaniaPorPlaza")
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCompaniaPorPlaza")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCompaniaPorPlaza")
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompaniaPorPlaza")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompaniaPorPlaza")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
        BaseII.Inserta("ProcedureSeleccionCompaniaPorPlaza")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
        BaseII.Inserta("ProcedureSeleccionCompaniaPorPlaza")
        llenalistboxs()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        bgMovCartera = False
        EntradasSelCiudad = 0
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count > 0 Then
            'If varfrmselcompania.Equals("cartera") Then
            '    If BndDesPagAde = True Then
            '        BndDesPagAde = False
            '        FrmSelFechaDesPagAde.Show()
            '    Else
            '        'If IdSistema = "AG" Then
            '        '    FrmSelPeriodoCartera.Show()
            '        'Else
            '        'execar = True
            '        'End If
            '        FrmSelPeriodo.Show()
            '    End If
            'End If

            'If varfrmselcompania = "opcion1varios" Then
            '    FrmSelServRep.Show()
            'End If
            'If varfrmselcompania = "normal" Then
            '    FrmSelColoniaJ.Show()
            'ElseIf varfrmselcompania = "hastacolonias" Then
            '    FrmSelColoniaJ.Show()
            'ElseIf varfrmselcompania = "hastacompania1" Then
            '    FrmSelUsuariosE.Show()
            'ElseIf varfrmselcompania = "hastacompania2" Then
            '    FrmSelTrabajosE.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselvendedor" Then
            '    FrmSelVendedor.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
            '    FrmSelUsuario.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
            '    FrmSelUsuariosVentas.Show()
            'ElseIf varfrmselcompania = "hastacompaniasreportefolios" Then
            '    FormReporteFolios.Show()
            'ElseIf varfrmselcompania = "hastacompaniasresventas" Then
            '    FrmSelFechasPPE.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselsucursal" Then
            '    FrmSelSucursal.Show()
            'ElseIf varfrmselcompania = "hastacompaniaimprimircomision" Then
            '    FrmImprimirComision.Show()
            'ElseIf varfrmselcompania = "hastacompaniacumpleanos" Then
            '    FrmRepCumpleanosDeLosClientes.Show()
            'ElseIf varfrmselcompania = "hastacompaniapruebaint" Then
            '    FrmRepPruebaInternet.Show()
            'ElseIf varfrmselcompania = "normalrecordatorios" Then
            '    FrmSelColoniaJ.Show()
            'ElseIf varfrmselcompania = "hastacompaniamensualidades" Then
            '    FrmImprimirComision.Show()
            'ElseIf varfrmselcompania = "hastacompaniareprecontratacion" Then
            '    SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
            '    SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
            '    SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
            '    SoftvMod.VariablesGlobales.MiConexion = MiConexion
            '    SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
            '    SoftvMod.VariablesGlobales.IdCompania = identificador
            '    Dim frm As New SoftvMod.FrmRepRecontratacion
            '    frm.ShowDialog()
            'ElseIf varfrmselcompania = "movimientoscartera" Then
            '    FrmSelCiudadJ.Show()
            'ElseIf varfrmselcompania = "pendientesderealizar" Then
            '    FrmImprimirComision.Show()
            'ElseIf varfrmselcompania = "resumenordenes" Then
            '    FrmFiltroReporteResumen.Show()
            'End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'If SelCiudadSinJ = 0 Then
            '    If varfrmselcompania = "movimientoscartera" Then
            '        bgMovCartera = True
            '        Me.Close()
            '    Else
            '        FrmSelCiudadJ.Show()
            '        Me.Close()
            '    End If
            'ElseIf SelCiudadSinJ = 1 Then
            '    If varfrmselcompania = "movimientoscartera" Then
            '        Me.Close()
            '        Exit Sub
            '    End If
            '    If varfrmselcompania = "ordenes" Then
            '        Me.Visible = False
            '        FrmSelCiudad.Show()
            '        Me.Close()
            '        Exit Sub
            '    End If
            '    If rMensajes = True Or eBndDeco = True Then
            '        FrmSelColonia_Rep21.Show()
            '    End If
            '    If LocbndPolizaCiudad = True Then
            '        FrmSelFechas.Show()
            '    Else
            '        'FrmSelColonia_Rep21.Show() 'opcion1
            '        varfrmselcompania = "opcion1varios"
            '        FrmSelCiudad.Show()

            '    End If
            '    SelCiudadSinJ = 0
            'ElseIf SelCiudadSinJ = 2 Then
            '    FrmSelCd_Cartera.Show()
            '    SelCiudadSinJ = 0
            'ElseIf SelCiudadSinJ = 3 Then
            '    eOpVentas = 65
            '    FrmImprimirComision.Show()
            '    SelCiudadSinJ = 0
            'ElseIf SelCiudadSinJ = 4 Then
            '    eOpVentas = 66
            '    FrmImprimirComision.Show()
            '    SelCiudadSinJ = 0
            'End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'FrmSelEstadosFil.Show()
            FrmSelEstadosFilPorPlaza.Show()
        End If
        Me.Close()

    End Sub
    Private Sub llevamealotro()
        'If varfrmselcompania.Equals("cartera") Then
        '    If BndDesPagAde = True Then
        '        BndDesPagAde = False
        '        FrmSelFechaDesPagAde.Show()
        '    Else
        '        'If IdSistema = "AG" Then
        '        '    FrmSelPeriodoCartera.Show()
        '        'Else
        '        'execar = True
        '        'End If
        '        FrmSelPeriodo.Show()
        '    End If
        'End If

        'If varfrmselcompania = "opcion1varios" Then
        '    FrmSelServRep.Show()
        'End If
        'If varfrmselcompania = "normal" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacolonias" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacompania1" Then
        '    FrmSelUsuariosE.Show()
        'ElseIf varfrmselcompania = "hastacompania2" Then
        '    FrmSelTrabajosE.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselvendedor" Then
        '    FrmSelVendedor.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
        '    FrmSelUsuario.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
        '    FrmSelUsuariosVentas.Show()
        'ElseIf varfrmselcompania = "hastacompaniasreportefolios" Then
        '    FormReporteFolios.Show()
        'ElseIf varfrmselcompania = "hastacompaniasresventas" Then
        '    FrmSelFechasPPE.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselsucursal" Then
        '    FrmSelSucursal.Show()
        'ElseIf varfrmselcompania = "hastacompaniaimprimircomision" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "hastacompaniacumpleanos" Then
        '    FrmRepCumpleanosDeLosClientes.Show()
        'ElseIf varfrmselcompania = "hastacompaniapruebaint" Then
        '    FrmRepPruebaInternet.Show()
        'ElseIf varfrmselcompania = "normalrecordatorios" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacompaniamensualidades" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "hastacompaniareprecontratacion" Then
        '    SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        '    SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        '    SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        '    SoftvMod.VariablesGlobales.MiConexion = MiConexion
        '    SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        '    SoftvMod.VariablesGlobales.IdCompania = identificador
        '    Dim frm As New SoftvMod.FrmRepRecontratacion
        '    frm.ShowDialog()
        'ElseIf varfrmselcompania = "movimientoscartera" Then
        '    FrmSelCiudadJ.Show()
        'ElseIf varfrmselcompania = "pendientesderealizar" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "resumenordenes" Then
        '    FrmFiltroReporteResumen.Show()
        'End If
        'Me.Close()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'If SelCiudadSinJ = 0 Then
        '    If varfrmselcompania = "movimientoscartera" Then
        '        bgMovCartera = True
        '        Me.Close()
        '    Else
        '        FrmSelCiudadJ.Show()
        '        Me.Close()
        '    End If
        'ElseIf SelCiudadSinJ = 1 Then
        '    If varfrmselcompania = "movimientoscartera" Then
        '        Me.Close()
        '        Exit Sub
        '    End If
        '    If varfrmselcompania = "ordenes" Then
        '        Me.Visible = False
        '        FrmSelCiudad.Show()
        '        Me.Close()
        '        Exit Sub
        '    End If
        '    If rMensajes = True Or eBndDeco = True Then
        '        FrmSelColonia_Rep21.Show()
        '    End If
        '    If LocbndPolizaCiudad = True Then
        '        FrmSelFechas.Show()
        '    Else
        '        'FrmSelColonia_Rep21.Show() 'opcion1
        '        varfrmselcompania = "opcion1varios"
        '        FrmSelCiudad.Show()

        '    End If
        '    SelCiudadSinJ = 0
        'ElseIf SelCiudadSinJ = 2 Then
        '    FrmSelCd_Cartera.Show()
        '    SelCiudadSinJ = 0
        'ElseIf SelCiudadSinJ = 3 Then
        '    eOpVentas = 65
        '    FrmImprimirComision.Show()
        '    SelCiudadSinJ = 0
        'ElseIf SelCiudadSinJ = 4 Then
        '    eOpVentas = 66
        '    FrmImprimirComision.Show()
        '    SelCiudadSinJ = 0
        'End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        FrmSelEstadosFil.Show()
        Me.Close()
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub ComboBoxPlaza_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxPlaza.SelectedIndexChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(GloClvUsuario))
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
            BaseII.Inserta("ProcedureSeleccionCompaniaPorPlaza")
            llenalistboxs()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub
End Class