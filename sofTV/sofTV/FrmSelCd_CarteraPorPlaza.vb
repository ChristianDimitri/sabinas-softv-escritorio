Imports System.Data.SqlClient
Public Class FrmSelCd_CarteraPorPlaza
    Public Sub CREAARBOL()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            'Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            Dim dt As DataTable = BaseII.ConsultaDT("MuestraSelecciona_CiudadTmpCONSULTA")
            Dim FilaRow As DataRow
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In dt.Rows
                Me.TreeView1.Nodes.Add(Trim(FilaRow("Clv_ciudad").ToString()), Trim(FilaRow("nombre").ToString()))
                Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("Clv_ciudad").ToString())
                I += 1
            Next
            CON.Close()
            Me.TreeView1.ExpandAll()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Sub CREAARBOL2()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSeleccion_CiudadCONSULTA, LocClv_session)
            Dim FilaRow As DataRow
            Me.TreeView2.Nodes.Clear()
            For Each FilaRow In Me.DataSetLidia.MuestraSeleccion_CiudadCONSULTA.Rows
                Me.TreeView2.Nodes.Add(Trim(FilaRow("Clv_ciudad").ToString()), Trim(FilaRow("nombre").ToString()))
                Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("Clv_ciudad").ToString())
                I += 1
            Next
            CON.Close()
            Me.TreeView2.ExpandAll()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmSelCd_Cartera_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            If EntradasSelCiudad = 0 Then
                EntradasSelCiudad = 1
                SelCiudadSinJ = 2
                Me.Close()
                FrmSelPlazaPorPlaza.Show()
                Exit Sub
            End If
            If EntradasSelCiudad = 1 Then
                EntradasSelCiudad = 0
            End If
            If rMensajes = True Then
                LocClv_session = rSession
            End If
            colorea(Me, Me.Name)

            'Solo una
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@numero", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("DameNumCiudadesUsuarioPorPlaza")
            Dim numciudades As Integer = BaseII.dicoPar("@numero")
            If numciudades = 1 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
                BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
                BaseII.Inserta("InsertaSeleccionCiudadPorPlaza")
                llevamealotro()
                Exit Sub
            End If
            'Fin solo una
            '  Me.Muestra_ciudadTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_ciudad)
            'Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.Connection = CON
            'Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpNUEVO, LocClv_session)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.Inserta("MuestraSelecciona_CiudadTmpNUEVOPorPlaza")
            CON.Close()
            CREAARBOL()
            CREAARBOL2()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.Contratonet.Text) Then
            Try
                Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Connection = CON
                Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Fill(Me.DataSetLidia.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmp, LocClv_session, Contratonet.Text)
                CON.Close()
                Me.CREAARBOL()
                Me.CREAARBOL2()
                Me.Contratonet.Text = 0
            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    Contratonet.Text = e.Node.Tag
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    Clv_Unicanet.Text = e.Node.Tag
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    Contratonet1.Text = e.Node.Tag
                    Clv_Unicanet1.Text = 0
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    Clv_Unicanet1.Text = e.Node.Tag
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub TreeView2_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView2.NodeMouseClick
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    Contratonet1.Text = e.Node.Tag
                    Clv_Unicanet1.Text = 0
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    Clv_Unicanet1.Text = e.Node.Tag
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView1_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseClick
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    Contratonet.Text = e.Node.Tag
                    Clv_Unicanet.Text = 0
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    Clv_Unicanet.Text = e.Node.Tag
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.Contratonet1.Text) = True Then
            Try
                Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Connection = CON
                Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Fill(Me.DataSetLidia.PONUNOSelecciona_CiudadTmp_Seleccion_Ciudad, LocClv_session, Me.Contratonet1.Text)
                CON.Close()
                Me.CREAARBOL()
                Me.CREAARBOL2()
                Me.Contratonet1.Text = 0
                Me.Clv_Unicanet1.Text = 0
            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Connection = CON
            Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Fill(Me.DataSetLidia.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmp, LocClv_session)
            CON.Close()
            Me.CREAARBOL()
            Me.CREAARBOL2()
            Me.Contratonet1.Text = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Connection = CON
            Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Fill(Me.DataSetLidia.PONTODOSSelecciona_CiudadTmp_Seleccion_Ciudad, LocClv_session)
            CON.Close()
            Me.CREAARBOL()
            Me.CREAARBOL2()
            Me.Contratonet.Text = 0
            Clv_Unicanet.Text = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Borra_Seleccion_CiudadTableAdapter.Connection = CON
        Me.Borra_Seleccion_CiudadTableAdapter.Fill(Me.DataSetLidia.Borra_Seleccion_Ciudad, LocClv_session)
        CON.Close()
        LocClv_session = 0
        execar = False
        CON.Close()
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'If BndDesPagAde = True Then
        '    BndDesPagAde = False
        '    FrmSelFechaDesPagAde.Show()
        'Else
        '    'If IdSistema = "AG" Then
        '    '    FrmSelPeriodoCartera.Show()
        '    'Else
        '    'execar = True
        '    'End If
        '    FrmSelPeriodo.Show()
        'End If

        'varfrmselcompania = "cartera"
        'FrmSelCompaniaCartera.Show()
        ''''Nuevo
        'If varfrmselcompania = "cartera" Then
        '    If BndDesPagAde = True Then
        '        BndDesPagAde = False
        '        FrmSelFechaDesPagAde.Show()
        '    Else
        '        'If IdSistema = "AG" Then
        '        '    FrmSelPeriodoCartera.Show()
        '        'Else
        '        'execar = True
        '        'End If
        '        FrmSelPeriodo.Show()
        '    End If
        'End If

        'If varfrmselcompania = "opcion1varios" Then
        '    FrmSelServRep.Show()
        'End If
        'If varfrmselcompania = "normal" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacolonias" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacompania1" Then
        '    FrmSelUsuariosE.Show()
        'ElseIf varfrmselcompania = "hastacompania2" Then
        '    FrmSelTrabajosE.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselvendedor" Then
        '    FrmSelVendedor.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
        '    FrmSelUsuario.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
        '    FrmSelUsuariosVentas.Show()
        'ElseIf varfrmselcompania = "hastacompaniasreportefolios" Then
        '    FormReporteFolios.Show()
        'ElseIf varfrmselcompania = "hastacompaniasresventas" Then
        '    FrmSelFechasPPE.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselsucursal" Then
        '    FrmSelSucursal.Show()
        'ElseIf varfrmselcompania = "hastacompaniaimprimircomision" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "hastacompaniacumpleanos" Then
        '    FrmRepCumpleanosDeLosClientes.Show()
        'ElseIf varfrmselcompania = "hastacompaniapruebaint" Then
        '    FrmRepPruebaInternet.Show()
        'ElseIf varfrmselcompania = "normalrecordatorios" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacompaniamensualidades" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "hastacompaniareprecontratacion" Then
        '    SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        '    SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        '    SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        '    SoftvMod.VariablesGlobales.MiConexion = MiConexion
        '    SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        '    SoftvMod.VariablesGlobales.IdCompania = identificador
        '    Dim frm As New SoftvMod.FrmRepRecontratacion
        '    frm.ShowDialog()
        'ElseIf varfrmselcompania = "movimientoscartera" Then
        '    FrmSelCiudadJ.Show()
        'ElseIf varfrmselcompania = "pendientesderealizar" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "resumenordenes" Then
        '    FrmFiltroReporteResumen.Show()
        'End If
        FrmSelLocalidad.Show()
        Me.Close()
    End Sub
    Private Sub llevamealotro()
        'If varfrmselcompania.Equals("cartera") Then
        '    If BndDesPagAde = True Then
        '        BndDesPagAde = False
        '        FrmSelFechaDesPagAde.Show()
        '    Else
        '        'If IdSistema = "AG" Then
        '        '    FrmSelPeriodoCartera.Show()
        '        'Else
        '        'execar = True
        '        'End If
        '        FrmSelPeriodo.Show()
        '    End If
        'End If

        'If varfrmselcompania = "opcion1varios" Then
        '    FrmSelServRep.Show()
        'End If
        'If varfrmselcompania = "normal" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacolonias" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacompania1" Then
        '    FrmSelUsuariosE.Show()
        'ElseIf varfrmselcompania = "hastacompania2" Then
        '    FrmSelTrabajosE.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselvendedor" Then
        '    FrmSelVendedor.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
        '    FrmSelUsuario.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
        '    FrmSelUsuariosVentas.Show()
        'ElseIf varfrmselcompania = "hastacompaniasreportefolios" Then
        '    FormReporteFolios.Show()
        'ElseIf varfrmselcompania = "hastacompaniasresventas" Then
        '    FrmSelFechasPPE.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselsucursal" Then
        '    FrmSelSucursal.Show()
        'ElseIf varfrmselcompania = "hastacompaniaimprimircomision" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "hastacompaniacumpleanos" Then
        '    FrmRepCumpleanosDeLosClientes.Show()
        'ElseIf varfrmselcompania = "hastacompaniapruebaint" Then
        '    FrmRepPruebaInternet.Show()
        'ElseIf varfrmselcompania = "normalrecordatorios" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacompaniamensualidades" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "hastacompaniareprecontratacion" Then
        '    SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        '    SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        '    SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        '    SoftvMod.VariablesGlobales.MiConexion = MiConexion
        '    SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        '    SoftvMod.VariablesGlobales.IdCompania = identificador
        '    Dim frm As New SoftvMod.FrmRepRecontratacion
        '    frm.ShowDialog()
        'ElseIf varfrmselcompania = "movimientoscartera" Then
        '    FrmSelCiudadJ.Show()
        'ElseIf varfrmselcompania = "pendientesderealizar" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "resumenordenes" Then
        '    FrmFiltroReporteResumen.Show()
        'End If
        FrmSelLocalidad.Show()
        Me.Close()
    End Sub
End Class
