﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmProspectos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmProspectos))
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.ComboBoxCiudad = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBoxColonias = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBoxCalles = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBoxEstados = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBoxLocalidad = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBoxNext = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBoxNint = New System.Windows.Forms.TextBox()
        Me.TextBoxPnombre = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBoxSnombre = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextBoxApePa = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBoxApeMa = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBoxCel = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TextBoxTel = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBoxCorreo = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.TextBoxClave = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(721, 78)
        Me.ComboBoxCompanias.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(329, 28)
        Me.ComboBoxCompanias.TabIndex = 200
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label12.Location = New System.Drawing.Point(717, 50)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(72, 18)
        Me.Label12.TabIndex = 201
        Me.Label12.Text = "*Plaza  :"
        '
        'ComboBoxCiudad
        '
        Me.ComboBoxCiudad.DisplayMember = "Clv_Ciudad"
        Me.ComboBoxCiudad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCiudad.FormattingEnabled = True
        Me.ComboBoxCiudad.Location = New System.Drawing.Point(365, 196)
        Me.ComboBoxCiudad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxCiudad.Name = "ComboBoxCiudad"
        Me.ComboBoxCiudad.Size = New System.Drawing.Size(329, 28)
        Me.ComboBoxCiudad.TabIndex = 5
        Me.ComboBoxCiudad.ValueMember = "Clv_Ciudad"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(367, 169)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 18)
        Me.Label1.TabIndex = 203
        Me.Label1.Text = "*Ciudad  :"
        '
        'ComboBoxColonias
        '
        Me.ComboBoxColonias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxColonias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxColonias.FormattingEnabled = True
        Me.ComboBoxColonias.Location = New System.Drawing.Point(16, 255)
        Me.ComboBoxColonias.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxColonias.Name = "ComboBoxColonias"
        Me.ComboBoxColonias.Size = New System.Drawing.Size(329, 28)
        Me.ComboBoxColonias.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(16, 228)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 18)
        Me.Label2.TabIndex = 205
        Me.Label2.Text = "*Colonia  :"
        '
        'ComboBoxCalles
        '
        Me.ComboBoxCalles.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCalles.FormattingEnabled = True
        Me.ComboBoxCalles.Location = New System.Drawing.Point(363, 255)
        Me.ComboBoxCalles.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxCalles.Name = "ComboBoxCalles"
        Me.ComboBoxCalles.Size = New System.Drawing.Size(329, 28)
        Me.ComboBoxCalles.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(363, 228)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 18)
        Me.Label3.TabIndex = 207
        Me.Label3.Text = "*Calle  :"
        '
        'ComboBoxEstados
        '
        Me.ComboBoxEstados.DisplayMember = "Clv_Ciudad"
        Me.ComboBoxEstados.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxEstados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxEstados.FormattingEnabled = True
        Me.ComboBoxEstados.Location = New System.Drawing.Point(16, 196)
        Me.ComboBoxEstados.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxEstados.Name = "ComboBoxEstados"
        Me.ComboBoxEstados.Size = New System.Drawing.Size(329, 28)
        Me.ComboBoxEstados.TabIndex = 4
        Me.ComboBoxEstados.ValueMember = "Clv_Ciudad"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(16, 169)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 18)
        Me.Label4.TabIndex = 209
        Me.Label4.Text = "*Estado :"
        '
        'ComboBoxLocalidad
        '
        Me.ComboBoxLocalidad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxLocalidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxLocalidad.FormattingEnabled = True
        Me.ComboBoxLocalidad.Location = New System.Drawing.Point(717, 196)
        Me.ComboBoxLocalidad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxLocalidad.Name = "ComboBoxLocalidad"
        Me.ComboBoxLocalidad.Size = New System.Drawing.Size(329, 28)
        Me.ComboBoxLocalidad.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(717, 169)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(102, 18)
        Me.Label5.TabIndex = 211
        Me.Label5.Text = "*Localidad  :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(717, 228)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(109, 18)
        Me.Label6.TabIndex = 212
        Me.Label6.Text = "*Número Ext."
        '
        'TextBoxNext
        '
        Me.TextBoxNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxNext.Location = New System.Drawing.Point(721, 255)
        Me.TextBoxNext.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNext.Name = "TextBoxNext"
        Me.TextBoxNext.Size = New System.Drawing.Size(141, 26)
        Me.TextBoxNext.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(875, 256)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(17, 24)
        Me.Label7.TabIndex = 214
        Me.Label7.Text = "-"
        '
        'TextBoxNint
        '
        Me.TextBoxNint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxNint.Location = New System.Drawing.Point(905, 254)
        Me.TextBoxNint.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNint.Name = "TextBoxNint"
        Me.TextBoxNint.Size = New System.Drawing.Size(141, 26)
        Me.TextBoxNint.TabIndex = 10
        '
        'TextBoxPnombre
        '
        Me.TextBoxPnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxPnombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxPnombre.Location = New System.Drawing.Point(20, 138)
        Me.TextBoxPnombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxPnombre.Name = "TextBoxPnombre"
        Me.TextBoxPnombre.Size = New System.Drawing.Size(155, 26)
        Me.TextBoxPnombre.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(16, 112)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(127, 18)
        Me.Label8.TabIndex = 216
        Me.Label8.Text = "*Primer nombre"
        '
        'TextBoxSnombre
        '
        Me.TextBoxSnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxSnombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxSnombre.Location = New System.Drawing.Point(184, 138)
        Me.TextBoxSnombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxSnombre.Name = "TextBoxSnombre"
        Me.TextBoxSnombre.Size = New System.Drawing.Size(161, 26)
        Me.TextBoxSnombre.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(179, 112)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(136, 18)
        Me.Label9.TabIndex = 218
        Me.Label9.Text = "Segundo nombre"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(901, 228)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(31, 18)
        Me.Label10.TabIndex = 220
        Me.Label10.Text = "Int."
        '
        'TextBoxApePa
        '
        Me.TextBoxApePa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxApePa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxApePa.Location = New System.Drawing.Point(363, 138)
        Me.TextBoxApePa.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxApePa.Name = "TextBoxApePa"
        Me.TextBoxApePa.Size = New System.Drawing.Size(329, 26)
        Me.TextBoxApePa.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label11.Location = New System.Drawing.Point(373, 112)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(131, 18)
        Me.Label11.TabIndex = 221
        Me.Label11.Text = "Apellido Paterno"
        '
        'TextBoxApeMa
        '
        Me.TextBoxApeMa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxApeMa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxApeMa.Location = New System.Drawing.Point(721, 138)
        Me.TextBoxApeMa.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxApeMa.Name = "TextBoxApeMa"
        Me.TextBoxApeMa.Size = New System.Drawing.Size(329, 26)
        Me.TextBoxApeMa.TabIndex = 3
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label13.Location = New System.Drawing.Point(725, 112)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(134, 18)
        Me.Label13.TabIndex = 223
        Me.Label13.Text = "Apellido Materno"
        '
        'TextBoxCel
        '
        Me.TextBoxCel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxCel.Location = New System.Drawing.Point(184, 326)
        Me.TextBoxCel.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxCel.Name = "TextBoxCel"
        Me.TextBoxCel.Size = New System.Drawing.Size(161, 26)
        Me.TextBoxCel.TabIndex = 12
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label14.Location = New System.Drawing.Point(209, 300)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(61, 18)
        Me.Label14.TabIndex = 227
        Me.Label14.Text = "Celular"
        '
        'TextBoxTel
        '
        Me.TextBoxTel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxTel.Location = New System.Drawing.Point(20, 326)
        Me.TextBoxTel.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxTel.Name = "TextBoxTel"
        Me.TextBoxTel.Size = New System.Drawing.Size(155, 26)
        Me.TextBoxTel.TabIndex = 11
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label15.Location = New System.Drawing.Point(16, 300)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(81, 18)
        Me.Label15.TabIndex = 225
        Me.Label15.Text = "*Teléfono"
        '
        'TextBoxCorreo
        '
        Me.TextBoxCorreo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxCorreo.Location = New System.Drawing.Point(363, 326)
        Me.TextBoxCorreo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxCorreo.Name = "TextBoxCorreo"
        Me.TextBoxCorreo.Size = New System.Drawing.Size(329, 26)
        Me.TextBoxCorreo.TabIndex = 13
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label16.Location = New System.Drawing.Point(373, 300)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(61, 18)
        Me.Label16.TabIndex = 229
        Me.Label16.Text = "Correo"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorSaveItem})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(1088, 27)
        Me.BindingNavigator1.TabIndex = 230
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton1.Size = New System.Drawing.Size(93, 24)
        Me.ToolStripButton1.Text = "&CANCELAR"
        Me.ToolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'BindingNavigatorSaveItem
        '
        Me.BindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorSaveItem.Image = CType(resources.GetObject("BindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem"
        Me.BindingNavigatorSaveItem.Size = New System.Drawing.Size(107, 24)
        Me.BindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'TextBoxClave
        '
        Me.TextBoxClave.Enabled = False
        Me.TextBoxClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxClave.Location = New System.Drawing.Point(20, 78)
        Me.TextBoxClave.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxClave.Name = "TextBoxClave"
        Me.TextBoxClave.Size = New System.Drawing.Size(155, 26)
        Me.TextBoxClave.TabIndex = 231
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label17.Location = New System.Drawing.Point(16, 52)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(50, 18)
        Me.Label17.TabIndex = 232
        Me.Label17.Text = "Clave"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(871, 313)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 233
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmProspectos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1088, 369)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.TextBoxClave)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.TextBoxCorreo)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.TextBoxCel)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.TextBoxTel)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.TextBoxApeMa)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.TextBoxApePa)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.TextBoxSnombre)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.TextBoxPnombre)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.TextBoxNint)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TextBoxNext)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.ComboBoxLocalidad)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ComboBoxEstados)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ComboBoxCalles)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboBoxColonias)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboBoxCiudad)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxCompanias)
        Me.Controls.Add(Me.Label12)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmProspectos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Prospectos"
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxColonias As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCalles As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxEstados As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxLocalidad As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNext As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNint As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxPnombre As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBoxSnombre As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBoxApePa As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBoxApeMa As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBoxCel As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBoxTel As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBoxCorreo As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents TextBoxClave As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
