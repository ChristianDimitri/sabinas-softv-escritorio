Imports System.Data.SqlClient
Imports System.Text
Public Class FrmQuiz

    Dim Op As Integer = 0
    Dim ClvSession As Long = 0
    Dim Res As Integer = 0
    Dim Msj As String = String.Empty
    Dim Habilitado As Boolean = False
    Dim HabilitadoTxt As Boolean = False

    Dim Actual As Integer = 0
    Dim Total As Integer = 0
    Dim Terminado As Boolean = False

    Dim IDPregunta As Integer = 0
    Dim IDPreguntaAnt As Integer = 0
    Dim IDPreguntaSig As Integer = 0

    Dim TipoObjeto As String = String.Empty
    Dim Radio As RadioButton
    Dim Check As CheckBox
    Dim Txt As TextBox

    'x = Posicion Horizontal Radio,Check,TextBox
    Dim x As Integer = 60
    'y = Posicion Verical Radio,Check,TextBox
    Dim y As Integer = 15
    'z = Posicion Horizontal TextBox (Respuestas Si/No)
    Dim z As Integer = 180
    'Incrementos a la Posici�n Veritcal de Radio, Check, TextBox
    Dim Pos As Integer = 0

    Dim Fuente As New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))


    Private Sub MuestraInfoCliente(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        strSQL.Append("EXEC MuestraInfoCliente ")
        strSQL.Append(CStr(Contrato))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable


        Try
            dataAdapter.Fill(dataTable)
            LabelContrato.Text = Contrato
            LabelNombre.Text = dataTable.Rows(0)(0).ToString
            LabelCalle.Text = dataTable.Rows(0)(1).ToString()
            LabelNumero.Text = dataTable.Rows(0)(2).ToString()
            LabelColonia.Text = dataTable.Rows(0)(3).ToString()
            LabelCiudad.Text = dataTable.Rows(0)(4).ToString()
            LabelTelefono.Text = dataTable.Rows(0)(5).ToString()
            LabelCelular.Text = dataTable.Rows(0)(6).ToString()
            CheckBoxSoloInternet.Checked = dataTable.Rows(0)(7).ToString
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        End Try
    End Sub

    Private Sub ConPreguntasQuiz(ByVal ClvSession As Long, ByVal Contrato As Long, ByVal IDEncuesta As Integer, ByVal IDPregunta As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC ConPreguntasQuiz ")
        strSQL.Append(CStr(ClvSession) & ", ")
        strSQL.Append(CStr(Contrato) & ", ")
        strSQL.Append(CStr(IDEncuesta) & ", ")
        strSQL.Append(CStr(IDPregunta) & ", ")
        strSQL.Append(CStr(Op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try
            dataAdapter.Fill(dataTable)
            Me.IDPregunta = CInt(dataTable.Rows(0)(0).ToString())
            LabelPregunta.Text = dataTable.Rows(0)(1).ToString()
            IDPreguntaAnt = CInt(dataTable.Rows(0)(2).ToString())
            IDPreguntaSig = CInt(dataTable.Rows(0)(3).ToString())
            Actual = CInt(dataTable.Rows(0)(4).ToString())
            Total = CInt(dataTable.Rows(0)(5).ToString())

            'If Actual < Total Then
            '    ButtonAnt.Enabled = True
            '    ButtonSig.Enabled = True
            'End If

            'If Actual = Total Then ButtonSig.Enabled = False

            TSBGuardar.Enabled = True
            LabelNDeM.Text = "Pregunta " & CStr(Actual) & " de " & CStr(Total)
            If Actual = Total Then Terminado = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ConRespuestasQuiz(ByVal ClvSession As Long, ByVal Contrato As Long, ByVal IDEncuesta As Integer, ByVal IDPregunta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRespuestasQuiz", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@IDEncuesta", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = IDEncuesta
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = IDPregunta
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            reader = comando.ExecuteReader()

            While (reader.Read())
                AgregarRespuestas(reader(0).ToString(), reader(1).ToString(), reader(2).ToString(), reader(3).ToString(), reader(4).ToString())
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub AgregarRespuestas(ByVal IDRespuesta As Integer, ByVal Respuesta As String, ByVal Tipo As String, ByVal Seleccionada As Boolean, ByVal Texto As String)

        If Tipo = "RADIO" Then

            Radio = New RadioButton
            Radio.Name = IDRespuesta
            Radio.Text = Respuesta
            Radio.Checked = Seleccionada
            Radio.ForeColor = Color.Black
            Radio.AutoSize = True
            Radio.Location = New System.Drawing.Point(x, y + Pos)
            Radio.Font = Fuente
            Radio.Enabled = Habilitado
            GroupBoxRespuestas.Controls.Add(Radio)

        ElseIf Tipo = "CHECK" Then

            Check = New CheckBox
            Check.Name = IDRespuesta
            Check.Text = Respuesta
            Check.Checked = Seleccionada
            Check.ForeColor = Color.Black
            Check.AutoSize = True
            Check.Location = New System.Drawing.Point(x, y + Pos)
            Check.Font = Fuente
            Check.Enabled = Habilitado
            GroupBoxRespuestas.Controls.Add(Check)


        ElseIf Tipo = "TEXT" Then

            Txt = New TextBox
            Txt.Name = IDRespuesta
            Txt.ForeColor = Color.Black
            Txt.Size = New System.Drawing.Size(400, 20)
            Txt.Location = New System.Drawing.Point(x, y + Pos)
            Txt.Font = Fuente
            Txt.CharacterCasing = CharacterCasing.Upper
            Txt.Text = Texto
            Txt.ReadOnly = HabilitadoTxt
            GroupBoxRespuestas.Controls.Add(Txt)

        ElseIf Tipo = "SN" Or Tipo = "NS" Then

            Radio = New RadioButton
            Radio.Name = IDRespuesta
            Radio.Text = Respuesta
            Radio.Checked = Seleccionada
            Radio.ForeColor = Color.Black
            Radio.AutoSize = True
            Radio.Location = New System.Drawing.Point(x, y + Pos)
            Radio.Font = Fuente
            Radio.Enabled = Habilitado
            GroupBoxRespuestas.Controls.Add(Radio)

            If Tipo = "SN" Then
                If Respuesta <> "S�" Then
                    Txt = New TextBox
                    Txt.Name = "Respuesta"
                    Txt.Text = Texto
                    Txt.ForeColor = Color.Black
                    Txt.Size = New System.Drawing.Size(400, 20)
                    Txt.Location = New System.Drawing.Point(z, y + Pos)
                    Txt.Font = Fuente
                    Txt.CharacterCasing = CharacterCasing.Upper
                    Txt.Text = Texto
                    Txt.ReadOnly = HabilitadoTxt
                    GroupBoxRespuestas.Controls.Add(Txt)
                End If
            ElseIf Tipo = "NS" Then
                If Respuesta <> "No" Then
                    Txt = New TextBox
                    Txt.Name = "Respuesta"
                    Txt.Text = Texto
                    Txt.ForeColor = Color.Black
                    Txt.Size = New System.Drawing.Size(400, 20)
                    Txt.Location = New System.Drawing.Point(z, y + Pos)
                    Txt.Font = Fuente
                    Txt.CharacterCasing = CharacterCasing.Upper
                    Txt.Text = Texto
                    Txt.ReadOnly = HabilitadoTxt
                    GroupBoxRespuestas.Controls.Add(Txt)
                End If
            End If

        End If

        TipoObjeto = Tipo
        Pos = Pos + 20

    End Sub

    Function Valida(ByVal Tipo As String) As Boolean

        Dim Bnd As Boolean = False

        Radio = New RadioButton
        Check = New CheckBox
        Txt = New TextBox

        For Each Objeto As Object In GroupBoxRespuestas.Controls
            If Object.ReferenceEquals(Radio.GetType(), Objeto.GetType()) = True Then
                Radio = Objeto
                If Radio.Checked = True Then
                    If (TipoObjeto = "SN" And Radio.Text <> "S�") Or (TipoObjeto = "NS" And Radio.Text <> "No") Then
                        Bnd = True
                    Else
                        Return True
                    End If
                End If
            ElseIf Object.ReferenceEquals(Check.GetType(), Objeto.GetType()) = True Then
                Check = Objeto
                If Check.Checked = True Then Return True
            ElseIf Object.ReferenceEquals(Txt.GetType(), Objeto.GetType()) = True Then
                Txt = Objeto
                If (TipoObjeto = "SN" Or TipoObjeto = "NS") And Bnd = True Then
                    If Txt.Text.Length > 0 Then Return True
                Else
                    If Txt.Text.Length > 0 Then Return True
                End If
            End If
        Next



        Return False

    End Function

    Private Sub NueTmpDetClientesPreguntas(ByVal ClvSession As Long, ByVal Contrato As Long, ByVal IDPregunta As Integer, ByVal IDRespuesta As Integer, ByVal Respuesta As String, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueTmpDetClientesPreguntas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@IDPregunta", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = IDPregunta
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@IDRespuesta", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = IDRespuesta
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Respuesta", SqlDbType.VarChar, 250)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Respuesta
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Op", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Op
        comando.Parameters.Add(parametro6)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub Grabar(ByVal ClvSession As Long, ByVal Contrato As Long, ByVal IDPregunta As Integer)
        Dim IDRespuestaAux As Integer = 0
        Dim Bnd As Boolean = False

        NueTmpDetClientesPreguntas(ClvSession, Contrato, IDPregunta, 0, String.Empty, 0)

        For Each Objeto As Object In GroupBoxRespuestas.Controls
            'Objetos del tipo RadioButtom
            If Object.ReferenceEquals(Radio.GetType(), Objeto.GetType()) = True Then
                Radio = Objeto
                If Radio.Checked = True Then
                    If TipoObjeto = "SN" And Radio.Text <> "S�" Then
                        IDRespuestaAux = Radio.Name
                        Bnd = True
                    ElseIf TipoObjeto = "NS" And Radio.Text <> "No" Then
                        IDRespuestaAux = Radio.Name
                        Bnd = True
                    Else
                        'Inserta cuando la opci�n NO obliga el �Porqu�?
                        NueTmpDetClientesPreguntas(ClvSession, Contrato, IDPregunta, Radio.Name, String.Empty, 1)
                    End If
                End If
                'Objetos del tipo CheckBox
            ElseIf Object.ReferenceEquals(Check.GetType(), Objeto.GetType()) = True Then
                Check = Objeto
                If Check.Checked = True Then NueTmpDetClientesPreguntas(ClvSession, Contrato, IDPregunta, Check.Name, String.Empty, 1)
                'Objetos del tipo TextBox
            ElseIf Object.ReferenceEquals(Txt.GetType(), Objeto.GetType()) = True Then
                Txt = Objeto
                If (TipoObjeto = "SN" Or TipoObjeto = "NS") Then
                    'Inserta cuandl la opci�n OGLIBA el �Porqu�?
                    If Bnd = True Then NueTmpDetClientesPreguntas(ClvSession, Contrato, IDPregunta, IDRespuestaAux, Txt.Text, 1)
                Else
                    NueTmpDetClientesPreguntas(ClvSession, Contrato, IDPregunta, Txt.Name, Txt.Text, 1)
                End If
            End If
        Next
    End Sub

    Private Sub DameClvSession()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Dame_clv_session_Reportes", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            ClvSession = CLng(parametro.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueDetClientesPreguntas(ByVal ClvSession As Long, ByVal Contrato As Long, ByVal ClvUsuario As Integer, ByVal IDEncuesta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetClientesPreguntas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ClvUsuario", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = ClvUsuario
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@IDEncuesta", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = IDEncuesta
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
        End Try
    End Sub

    Function DameSerDelCli(ByVal Contrato As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC dameSerDELCli " & CStr(Contrato))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try
            dataAdapter.Fill(dataTable)
            Return dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Function

    Private Sub CrearArbol(ByVal Contrato As Long)

        Try


            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True





            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False

            Dim PasaJNet As Boolean = False
            Dim jNet As Integer = -1
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In DameSerDelCli(Contrato).Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then
                    I = I + 1
                    pasa = False
                End If

            Next

            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Limpiar()
        Op = 0
        Pos = 0
        ClvSession = 0
        Actual = 0
        Total = 0
        IDPregunta = 0
        IDPreguntaAnt = 0
        IDPreguntaSig = 0
        Terminado = False
        TextBoxContrato.Clear()
        LabelContrato.Text = ""
        LabelNombre.Text = ""
        LabelCalle.Text = ""
        LabelNumero.Text = ""
        LabelColonia.Text = ""
        LabelCiudad.Text = ""
        LabelTelefono.Text = ""
        LabelCelular.Text = ""
        LabelEncuesta.Text = ""
        LabelPregunta.Text = ""
        LabelNDeM.Text = ""
        LabelRespuesta.Text = ""
        CheckBoxSoloInternet.Checked = False
        GroupBoxRespuestas.Controls.Clear()
        ButtonAnt.Enabled = False
        ButtonSig.Enabled = False
        TreeView1.Nodes.Clear()
    End Sub

    Private Sub FrmQuiz_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOCONTRATOSEL > 0 Then
            Me.TextBoxContrato.Text = GLOCONTRATOSEL
            eContrato = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0

            Limpiar()
            MuestraInfoCliente(eContrato)
            CrearArbol(eContrato)
            FrmSelEncuesta.Show()

        End If

        If eBndEncuesta = True Then
            eBndEncuesta = False
            LabelEncuesta.Text = eEncuestaNombre
            'Se habilitan hasta que se haya seleccionado la Encuesta
            TextBoxContrato.Enabled = True
            ButtonBuscar.Enabled = True
            LabelRespuesta.Text = "R."
            Op = 0
            DameClvSession()
            ConPreguntasQuiz(ClvSession, eContrato, eIDEncuesta, 0, Op)
            ConRespuestasQuiz(ClvSession, eContrato, eIDEncuesta, IDPregunta)
            Op = 1
            '----------Opciones del men�
            TSBNuevo.Enabled = False
            TSBGuardar.Enabled = True
            TSBCancelar.Enabled = True
            ButtonAnt.Enabled = True
            ButtonSig.Enabled = True
        End If
    End Sub

    Private Sub FrmQuiz_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Op = 0

        If eOpcion = "N" Then
            Habilitado = True
            HabilitadoTxt = False
            ButtonAnt.Enabled = False
            ButtonSig.Enabled = False
        ElseIf eOpcion = "C" Then
            Habilitado = False
            HabilitadoTxt = True
            BindingNavigatorQuiz.Enabled = False
            MuestraInfoCliente(eContrato)
            CrearArbol(eContrato)
            LabelEncuesta.Text = eEncuestaNombre
            ClvSession = 0
            'Se habilitan hasta que se haya seleccionado la Encuesta
            TextBoxContrato.Enabled = False
            ButtonBuscar.Enabled = False
            LabelRespuesta.Text = "R."
            Op = 0
            ConPreguntasQuiz(ClvSession, eContrato, eIDEncuesta, 0, Op)
            ConRespuestasQuiz(ClvSession, eContrato, eIDEncuesta, IDPregunta)
            Op = 1
            '----------Opciones del men�
            ButtonAnt.Enabled = True
            ButtonSig.Enabled = True
        ElseIf eOpcion = "M" Then

        End If
        If eOpcion = "N" Or eOpcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub TextBoxContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxContrato.KeyPress
        e.KeyChar = Chr(ValidaKey(TextBoxContrato, Asc(LCase(e.KeyChar)), "N"))
    End Sub

    Private Sub TextBoxContrato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxContrato.KeyDown
        If (e.KeyCode <> Keys.Enter) Then
            Exit Sub
        End If
        If IsNumeric(Me.TextBoxContrato.Text) = False Then
            Exit Sub
        End If
        If CInt(Me.TextBoxContrato.Text) <= 0 Then
            Exit Sub
        End If

        eContrato = TextBoxContrato.Text
        Limpiar()
        MuestraInfoCliente(eContrato)
        CrearArbol(eContrato)
        FrmSelEncuesta.Show()

    End Sub

    Private Sub ButtonAnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAnt.Click
        If Valida(TipoObjeto) = False Then
            MsgBox("Selecciona al menos una respuesta / Captura la informaci�n Solicitada", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Actual = 1 Then
            MsgBox("Inicio del Cuestionario.", MsgBoxStyle.Information)
            Exit Sub
        End If


        If eOpcion <> "C" Then Grabar(ClvSession, eContrato, IDPregunta)
        GroupBoxRespuestas.Controls.Clear()
        ConPreguntasQuiz(ClvSession, eContrato, eIDEncuesta, IDPreguntaAnt, Op) 'Consulto la anterior pregunta con el IDPreguntaAnt
        Pos = 0
        ConRespuestasQuiz(ClvSession, eContrato, eIDEncuesta, IDPregunta) 'Consulto la anterior pregunta con el IDPregunta (Ya es el IDPreguntaAnt que us� en ConPreguntasQuiz
    End Sub

    Private Sub ButtonSig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSig.Click
        If Valida(TipoObjeto) = False Then
            MsgBox("Selecciona al menos una respuesta / Captura la informaci�n Solicitada", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Actual = Total Then
            MsgBox("Fin del Cuestionario.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If eOpcion <> "C" Then Grabar(ClvSession, eContrato, IDPregunta)
        GroupBoxRespuestas.Controls.Clear()
        ConPreguntasQuiz(ClvSession, eContrato, eIDEncuesta, IDPreguntaSig, Op) 'Consulto la siguiente pregunta con el IDPreguntaSig
        Pos = 0
        ConRespuestasQuiz(ClvSession, eContrato, eIDEncuesta, IDPregunta) 'Consulto la siguiente pregunta con el IDPregunta (Ya es el IDPreguntaSig que us� en ConPreguntasQuiz
    End Sub

    Private Sub ButtonBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBuscar.Click
        GLOCONTRATOSEL = 0
        FrmSelCliente.Show()
    End Sub

    Private Sub ButtonSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

    Private Sub TSBGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBGuardar.Click
        If Terminado = False Then
            MsgBox("No se ha terminado el Cuestionario", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If Valida(TipoObjeto) = False Then
            MsgBox("Selecciona al menos una respuesta / Captura la informaci�n Solicitada", MsgBoxStyle.Information)
            Exit Sub
        End If

        Grabar(ClvSession, eContrato, IDPregunta)

        NueDetClientesPreguntas(ClvSession, eContrato, eClv_Usuario, eIDEncuesta)
        MsgBox(mensaje5)

        Limpiar()

        'Para refrescar el Browse
        eBnd = True

        '-----Opciones del Menu
        TSBNuevo.Enabled = True
        TSBGuardar.Enabled = False
        TSBCancelar.Enabled = False

    End Sub

    Private Sub TSBNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBNuevo.Click
        eOpcion = "N"
        Limpiar()
    End Sub


    Private Sub TSBCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBCancelar.Click
        eOpcion = "N"
        Limpiar()
        'Opciones del Men�
        TSBNuevo.Enabled = True
        TSBGuardar.Enabled = False
        TSBCancelar.Enabled = False
    End Sub


End Class