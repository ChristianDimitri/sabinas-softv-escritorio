﻿Public Class frmRelIngresosConceptosConf

    Private Sub frmRelIngresosConceptosConf_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        llenaDistribuidor()
        Llena_companias()
        Llena_servicios()
        muestraConceptosCuenta()

    End Sub

    Private Sub llenaDistribuidor()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Clv_plaza", SqlDbType.Int, 0)
            ComboBoxDis.DataSource = BaseII.ConsultaDT("Muestra_Plazas")
            ComboBoxDis.DisplayMember = "Nombre"
            ComboBoxDis.ValueMember = "clv_plaza"
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@clvplaza", SqlDbType.Int, ComboBoxDis.SelectedValue)
            ComboBoxPlaza.DataSource = BaseII.ConsultaDT("Muestra_Compania_PorPlaza")
            ComboBoxPlaza.DisplayMember = "razon_social"
            ComboBoxPlaza.ValueMember = "id_compania"

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxDis_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxDis.SelectedValueChanged
        If IsNumeric(ComboBoxDis.SelectedValue) Then
            Llena_companias()
        End If
    End Sub

    Private Sub Llena_servicios()
        Try
            BaseII.limpiaParametros()
            ComboBoxServicio.DataSource = BaseII.ConsultaDT("Muestra_ServiciosTarifados")
            ComboBoxServicio.DisplayMember = "Descripcion"
            ComboBoxServicio.ValueMember = "Clave"



        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Len(txtCuenta.Text) > 0 And IsNumeric(ComboBoxDis.SelectedValue) And IsNumeric(ComboBoxPlaza.SelectedValue) And Len(txtPosicion.Text) > 0 And ((Len(ComboBoxServicio.Text) > 0 And RadioServicio.Checked) Or (RadioOtro.Checked And Len(txtOtro.Text) > 0)) Then
            Dim array As String()
            BaseII.limpiaParametros()
            If RadioServicio.Checked Then
                array = ComboBoxServicio.SelectedValue.Trim.Split("-")
                BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, ComboBoxServicio.Text, 250)
            Else
                array = {0, 0}
                BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, txtOtro.Text, 250)
            End If
            BaseII.CreateMyParameter("@Clv_Plaza", SqlDbType.Int, ComboBoxDis.SelectedValue)
            BaseII.CreateMyParameter("@IdCompania", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
            'BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, ComboBoxServicio.Text, 250)
            BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, array(0).Trim)
            BaseII.CreateMyParameter("@Clave", SqlDbType.Int, array(1).Trim)
            BaseII.CreateMyParameter("@Cuenta", SqlDbType.VarChar, txtCuenta.Text, 250)
            BaseII.CreateMyParameter("@posicion", SqlDbType.Int, txtPosicion.Text)
            If rbEfectivo.Checked Then
                BaseII.CreateMyParameter("@tipodepago", SqlDbType.Int, 1)
            Else
                BaseII.CreateMyParameter("@tipodepago", SqlDbType.Int, 2)
            End If
            BaseII.CreateMyParameter("@existe", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("insertConceptosCuenta")
            If BaseII.dicoPar("@existe") = 1 Then
                MsgBox("Ya existe una cuenta para este servicio en la plaza seleccionada.")
            End If
            txtCuenta.Text = ""
            txtPosicion.Text = ""
            txtOtro.Text = ""
            muestraConceptosCuenta()
        End If
    End Sub

    Private Sub muestraConceptosCuenta()
        Try
            BaseII.limpiaParametros()
            If rbEfectivo.Checked Then
                BaseII.CreateMyParameter("@tipodepago", SqlDbType.Int, 1)
            Else
                BaseII.CreateMyParameter("@tipodepago", SqlDbType.Int, 2)
            End If
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
            If RadioOtro.Checked Then
                BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, -1)
            Else
                Dim clv_serv() = ComboBoxServicio.SelectedValue.ToString.Split("-")
                BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, CInt(clv_serv(0)))
            End If
            DataGridView1.DataSource = BaseII.ConsultaDT("muestraConceptosCuenta")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If IsNumeric(DataGridView1.SelectedCells(0).Value) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Concepto", SqlDbType.Int, DataGridView1.SelectedCells(0).Value)
            BaseII.Inserta("delteConceptoCuenta")
            muestraConceptosCuenta()
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If IsNumeric(DataGridView1.SelectedCells(0).Value) Then
            If DataGridView1.SelectedCells(0).RowIndex > 0 Then
                Dim aux As Integer = DataGridView1.SelectedCells(0).RowIndex
                'MsgBox(aux)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Concepto2", SqlDbType.Int, DataGridView1.SelectedCells(0).Value)
                BaseII.Inserta("SubirPosConceptosCuenta")

                muestraConceptosCuenta()
                DataGridView1.Rows(0).Selected = False
                DataGridView1.Rows(aux - 1).Selected = True
            End If
           
        End If

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If IsNumeric(DataGridView1.SelectedCells(0).Value) Then
            If DataGridView1.SelectedCells(0).RowIndex < DataGridView1.Rows.Count - 1 Then
                Dim aux As Integer = DataGridView1.SelectedCells(0).RowIndex
                'MsgBox(aux)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Concepto1", SqlDbType.Int, DataGridView1.SelectedCells(0).Value)
                BaseII.Inserta("BajarPosConceptosCuenta")

                muestraConceptosCuenta()
                DataGridView1.Rows(0).Selected = False
                DataGridView1.Rows(aux + 1).Selected = True
            End If

        End If
    End Sub

    Private Sub txtPosicion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPosicion.KeyPress
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros
        If (val >= 48 And val <= 57) Or val = 8 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()

    End Sub

    Private Sub RadioOtro_CheckedChanged(sender As Object, e As EventArgs) Handles RadioOtro.CheckedChanged
        If RadioOtro.Checked Then
            txtOtro.Enabled = True
        Else
            txtOtro.Enabled = False
        End If
        muestraConceptosCuenta()
    End Sub

    Private Sub RadioServicio_CheckedChanged(sender As Object, e As EventArgs) Handles RadioServicio.CheckedChanged
        If RadioServicio.Checked Then
            ComboBoxServicio.Enabled = True
        Else
            ComboBoxServicio.Enabled = False
        End If
    End Sub

    Private Sub rbOtros_CheckedChanged(sender As Object, e As EventArgs) Handles rbOtros.CheckedChanged
        muestraConceptosCuenta()
    End Sub

    Private Sub ComboBoxPlaza_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxPlaza.SelectedIndexChanged
        muestraConceptosCuenta()
    End Sub

    Private Sub ComboBoxServicio_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxServicio.SelectedIndexChanged
        muestraConceptosCuenta()
    End Sub
End Class