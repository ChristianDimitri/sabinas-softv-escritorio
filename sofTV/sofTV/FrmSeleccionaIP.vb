﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmSeleccionaIP

    Private Sub FrmSeleccionaIP_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ContratoNet", SqlDbType.Int, CType(GloContratonet, Integer))
        cbIP.DataSource = BaseII.ConsultaDT("Dame_IPsDisponibles")
        '
    End Sub

    Private Sub bAceptar_Click(sender As Object, e As EventArgs) Handles bAceptar.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ContratoNet", SqlDbType.Int, CType(GloContratonet, Integer))
        BaseII.CreateMyParameter("@IdIP", SqlDbType.Int, cbIP.SelectedValue)
        BaseII.Inserta("AsignarIPManual")

        frmInternet2.lDireccionIP.Text = cbIP.Text
        If frmInternet2.lDireccionIP.Text <> "" Then
            frmInternet2.bAsignarIP.Visible = False
        End If

        Me.Close()
    End Sub


End Class