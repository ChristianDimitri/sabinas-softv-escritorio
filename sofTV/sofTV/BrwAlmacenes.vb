﻿Imports System.IO
Imports System.Data.SqlClient
Public Class BrwAlmacenes
    Dim razon As String = ""
    Dim bnd As Integer = 0
    Dim idcom As Integer = 0
    Private Sub BrwAlmacenes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenadgvcompanias(0)
        llena_plazas()
        razon = ""
        bnd = 1
    End Sub

    Private Sub llena_plazas()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_plaza", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, "")
        ComboBoxPlazas.DataSource = BaseII.ConsultaDT("Muestra_Plazas")
    End Sub

    Private Sub llenadgvcompanias(ByVal opcion As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, opcion)
        BaseII.CreateMyParameter("@razon", SqlDbType.VarChar, razon)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, idcom)
        dgvcompanias.DataSource = BaseII.ConsultaDT("BrwMuestraCompanias")
        'If dgvcompanias.Rows.Count > 0 Then
        '    dgvcompanias.Rows(0).Selected = True
        'End If
    End Sub

    Private Sub dgvcompanias_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvcompanias.SelectionChanged
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim comando As New SqlCommand()
        comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "DameDatosCompaniaShort"
        Dim p1 As New SqlParameter("@idcompania", SqlDbType.Int)
        p1.Direction = ParameterDirection.Input
        p1.Value = dgvcompanias.CurrentRow.Cells(0).Value
        comando.Parameters.Add(p1)
        Dim reader As SqlDataReader = comando.ExecuteReader()
        If reader.Read() Then
            lbidcompania.Text = reader(0).ToString
            tbrazon.Text = reader(1).ToString
            lbclave.Text = reader(2).ToString
            lbrfc.Text = reader(3).ToString
        End If
        reader.Close()
        conexion.Close()
    End Sub

    Private Sub btnrazonsocial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnrazonsocial.Click
        razon = tbrazonsocial.Text.Trim()
        llenadgvcompanias(1)
    End Sub

    Private Sub tbrazonsocial_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbrazonsocial.TextChanged

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmCompania.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If lbidcompania.Text = "" Then
            MsgBox("No se ha seleccionado ninguna compañía")
            Exit Sub
        End If
        opcion = "C"
        GloIdCompania = lbidcompania.Text
        FrmCompania.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If lbidcompania.Text = "" Then
            MsgBox("No se ha seleccionado ninguna compañía")
            Exit Sub
        End If
        opcion = "M"
        GloIdCompania = lbidcompania.Text
        FrmCompania.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If tbidcompania.Text = "" Then
            Exit Sub
        End If
        If Not IsNumeric(tbidcompania.Text) Then
            Exit Sub
        End If
        idcom = tbidcompania.Text
        llenadgvcompanias(2)
    End Sub

    Private Sub BrwAlmacenes_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        llenadgvcompanias(0)
    End Sub

    Private Sub ComboBoxPlazas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxPlazas.SelectedIndexChanged
        If bnd = 0 Then
            Exit Sub
        End If
        Try
            razon = ""
            idcom = ComboBoxPlazas.SelectedValue
            llenadgvcompanias(3)
        Catch ex As Exception

        End Try
    End Sub
End Class