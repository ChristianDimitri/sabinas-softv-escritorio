Imports System.Data.SqlClient

Public Class FrmPaqueteActivado
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedValue = 0

            End If
            GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            eRes = 0
            If IsNumeric(Me.TextBox1.Text) = True Then
                If Me.MAcCableModemComboBox.Text.Length > 0 Then
                    If Me.DescripcionComboBox.Text.Length > 0 Then
                        'Me.ChecaPaqueteActivadoTableAdapter.Fill(Me.DataSetEric.ChecaPaqueteActivado, Me.TextBox1.Text, Me.MAcCableModemComboBox.SelectedValue, Me.DescripcionComboBox.SelectedValue, eRes)
                        Me.ChecaPaqueteActivadoTableAdapter.Connection = CON
                        Me.ChecaPaqueteActivadoTableAdapter.Fill(Me.DataSetEric.ChecaPaqueteActivado, Me.TextBox1.Text, Me.MAcCableModemComboBox.SelectedValue, Me.DescripcionComboBox.SelectedValue, eRes, eMsg)
                        If eRes = 0 Then
                            Me.MANDA_CNR_DIG_2TableAdapter.Connection = CON
                            Me.MANDA_CNR_DIG_2TableAdapter.Fill(Me.DataSetEric.MANDA_CNR_DIG_2, Me.TextBox1.Text, 0, Me.MAcCableModemComboBox.SelectedValue, Me.DescripcionComboBox.SelectedValue, "SCENTL", 0, 0, 1, eClv_Usuario, Me.NumericUpDown1.Value)
                            bitsist(GloUsuario, CLng(Me.TextBox1.Text), LocGloSistema, Me.Name, "Se Activo Un Paquete De Prueba", "Tarjeta: " + Me.MAcCableModemComboBox.Text, "Se Activo Un Paquete De Prueba: " + Me.DescripcionComboBox.Text, LocClv_Ciudad)
                            MsgBox("Orden Efectuada.")
                        Else
                            MsgBox(eMsg, , "Error")
                        End If
                        Me.TextBox1.Text = ""
                        Me.TextBox2.Text = ""
                        Me.DamePaquetesDigTableAdapter.Connection = CON
                        Me.DamePaquetesDigTableAdapter.Fill(Me.DataSetEric.DamePaquetesDig)
                        Me.NumericUpDown1.Value = 1
                    Else
                        MsgBox("Selecciona un Paquete.", , "Atenci�n")
                    End If
                Else
                    MsgBox("Selecciona el No. de la Tarjeta.", , "Atenci�n")
                End If
            Else
                MsgBox("Captura el Contrato.", , "Atenci�n")
            End If
            CON.Close()
        Catch
            MsgBox("Se ha producido un Error.")
        End Try
    End Sub

    Private Sub FrmPaqueteActivado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Llena_companias()
        Me.DamePaquetesDigTableAdapter.Connection = CON
        'Me.DamePaquetesDigTableAdapter.Fill(Me.DataSetEric.DamePaquetesDig)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 1)
        DescripcionComboBox.DataSource = BaseII.ConsultaDT("DamePaquetesDig")
        If DescripcionComboBox.Items.Count > 0 Then
            DescripcionComboBox.SelectedIndex = 0
        End If
        CON.Close()
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        'If ComboBoxCompanias.SelectedValue = 0 Then
        '    MsgBox("Selecciona una Compa��a")
        '    Exit Sub
        'End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.TextBox1.Text.Length = 0 Then
            Me.DameTarjetasClientesDigTableAdapter.Connection = CON
            Me.DameTarjetasClientesDigTableAdapter.Fill(Me.DataSetEric.DameTarjetasClientesDig, 0)
        Else
            If IsNumeric(Me.TextBox1.Text) = True Then
                Me.DameTarjetasClientesDigTableAdapter.Connection = CON
                Me.DameTarjetasClientesDigTableAdapter.Fill(Me.DataSetEric.DameTarjetasClientesDig, Me.TextBox1.Text)
            End If
        End If
        CON.Close()
    End Sub
    Dim loccon As Integer = 0
    Private Sub TextBox2_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox2.TextChanged
        Try
            'If ComboBoxCompanias.SelectedValue = 0 Then
            '    MsgBox("Selecciona una Compa��a")
            '    Exit Sub
            'End If
            If TextBox2.Text.Contains("-") Then

            Else
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim comando As New SqlCommand()
            Dim array As String()
            array = TextBox2.Text.Trim.Split("-")
            GloIdCompania = array(1)
            loccon = array(0)
            comando.Connection = CON
            'comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + Str(loccon) + " and idcompania=" + Str(GloIdCompania) + " and idcompania in (select idcompania from Rel_Usuario_Compania where Clave=" + GloClvUsuario + ")"
            comando.CommandText = "select contrato from Rel_Contratos_Companias where contratocompania=" + array(0).ToString() + " and idcompania=" + array(1).ToString() + " and idcompania in (select idcompania from Rel_Usuario_Compania where Clave=" + GloClvUsuario.ToString() + ")"
            TextBox1.Text = comando.ExecuteScalar().ToString()
            
            CON.Close()
        Catch ex As Exception
            'MsgBox("Contrato no v�lido. Int�ntelo nuevamente.")
            'TextBox2.Text = ""
            'MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            TextBox2.Text = ""
            TextBox1.Text = ""
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameTarjetasClientesDigTableAdapter.Connection = CON
            Me.DameTarjetasClientesDigTableAdapter.Fill(Me.DataSetEric.DameTarjetasClientesDig, 0)
            CON.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        GloClv_TipSer = 9875
        FrmSelCliente.ShowDialog()
        If GLOCONTRATOSEL > 0 Then
            TextBox2.Text = eContratoCompania.ToString + "-" + GloIdCompania.ToString
        End If
    End Sub
End Class