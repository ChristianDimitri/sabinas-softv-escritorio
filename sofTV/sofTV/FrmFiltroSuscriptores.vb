﻿Public Class FrmFiltroSuscriptores

    Private Sub FrmFiltroSuscriptores_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        If Not (IsNumeric(tbAnio.Text) And (rbPeriodo15.Checked Or rbPeriodo30.Checked)) Then
            MsgBox("Faltan datos por completar")
            Exit Sub
        End If
        If rbPeriodo15.Checked Then
            GloOp = 1
        Else
            GloOp = 2
        End If
        GloMes = nuMes.Value
        GloAnio = tbAnio.Text
        If nuMes.Value = 1 Then
            GloMes = 12
            GloAnio = tbAnio.Text - 1
        Else
            GloMes = nuMes.Value - 1
            GloAnio = tbAnio.Text
        End If
        Me.Close()
        eOpVentas = 109
        FrmImprimirComision.Show()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        If Not (IsNumeric(tbAnio.Text) And (rbPeriodo15.Checked Or rbPeriodo30.Checked)) Then
            MsgBox("Faltan datos por completar")
            Exit Sub
        End If
        If rbPeriodo15.Checked Then
            GloOp = 1
        Else
            GloOp = 2
        End If
        GloMes = nuMes.Value
        If nuMes.Value = 1 Then
            GloMes = 12
            GloAnio = tbAnio.Text - 1
        Else
            GloMes = nuMes.Value - 1
            GloAnio = tbAnio.Text
        End If

        Me.Close()
        eOpVentas = 108
        FrmImprimirComision.Show()
    End Sub


End Class