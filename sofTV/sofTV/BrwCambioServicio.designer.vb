<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCambioServicio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim Fecha_SolLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Fecha_SolLabel1 = New System.Windows.Forms.Label()
        Me.StatusLabel2 = New System.Windows.Forms.Label()
        Me.Servicio_ActualLabel2 = New System.Windows.Forms.Label()
        Me.Servicio_AnteriorLabel2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ConceptoLabel2 = New System.Windows.Forms.Label()
        Me.ContratoLabel2 = New System.Windows.Forms.Label()
        Me.NombreLabel2 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ConCambioServClienteDataGridView = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.RealizarTextBox = New System.Windows.Forms.TextBox()
        ContratoLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        ConceptoLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        Fecha_SolLabel = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ConCambioServClienteDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.Location = New System.Drawing.Point(11, 44)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(65, 15)
        ContratoLabel.TabIndex = 13
        ContratoLabel.Text = "Contrato:"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.Location = New System.Drawing.Point(11, 88)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(62, 15)
        NombreLabel.TabIndex = 14
        NombreLabel.Text = "Nombre:"
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConceptoLabel.Location = New System.Drawing.Point(11, 139)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(172, 15)
        ConceptoLabel.TabIndex = 15
        ConceptoLabel.Text = "Tipo de Servicio Anterior :"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel.Location = New System.Drawing.Point(11, 351)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(67, 15)
        StatusLabel.TabIndex = 18
        StatusLabel.Text = "Proceso :"
        '
        'Fecha_SolLabel
        '
        Fecha_SolLabel.AutoSize = True
        Fecha_SolLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_SolLabel.Location = New System.Drawing.Point(11, 410)
        Fecha_SolLabel.Name = "Fecha_SolLabel"
        Fecha_SolLabel.Size = New System.Drawing.Size(134, 15)
        Fecha_SolLabel.TabIndex = 12
        Fecha_SolLabel.Text = "Fecha de Solicitud :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(-12, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel2.Controls.Add(Me.ConCambioServClienteDataGridView)
        Me.SplitContainer1.Size = New System.Drawing.Size(860, 699)
        Me.SplitContainer1.SplitterDistance = 269
        Me.SplitContainer1.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(25, 165)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 16)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Nombre"
        '
        'Button6
        '
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(28, 212)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(80, 27)
        Me.Button6.TabIndex = 11
        Me.Button6.Text = "Buscar"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(28, 184)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(214, 22)
        Me.TextBox2.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 82)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Contrato"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Fecha_SolLabel1)
        Me.Panel1.Controls.Add(Fecha_SolLabel)
        Me.Panel1.Controls.Add(StatusLabel)
        Me.Panel1.Controls.Add(Me.StatusLabel2)
        Me.Panel1.Controls.Add(Me.Servicio_ActualLabel2)
        Me.Panel1.Controls.Add(ConceptoLabel)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.Servicio_AnteriorLabel2)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.ConceptoLabel2)
        Me.Panel1.Controls.Add(Me.ContratoLabel2)
        Me.Panel1.Controls.Add(Me.NombreLabel2)
        Me.Panel1.Location = New System.Drawing.Point(17, 295)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(237, 463)
        Me.Panel1.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 240)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(161, 15)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Tipo de Servicio Actual :"
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(14, 255)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(240, 23)
        Me.Label7.TabIndex = 20
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 185)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 15)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Servicio Anterior :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(11, 286)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 15)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Servicio Actual :"
        '
        'Fecha_SolLabel1
        '
        Me.Fecha_SolLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_SolLabel1.ForeColor = System.Drawing.Color.White
        Me.Fecha_SolLabel1.Location = New System.Drawing.Point(14, 425)
        Me.Fecha_SolLabel1.Name = "Fecha_SolLabel1"
        Me.Fecha_SolLabel1.Size = New System.Drawing.Size(240, 23)
        Me.Fecha_SolLabel1.TabIndex = 13
        '
        'StatusLabel2
        '
        Me.StatusLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusLabel2.ForeColor = System.Drawing.Color.White
        Me.StatusLabel2.Location = New System.Drawing.Point(14, 371)
        Me.StatusLabel2.Name = "StatusLabel2"
        Me.StatusLabel2.Size = New System.Drawing.Size(240, 23)
        Me.StatusLabel2.TabIndex = 12
        '
        'Servicio_ActualLabel2
        '
        Me.Servicio_ActualLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Servicio_ActualLabel2.ForeColor = System.Drawing.Color.White
        Me.Servicio_ActualLabel2.Location = New System.Drawing.Point(14, 301)
        Me.Servicio_ActualLabel2.Name = "Servicio_ActualLabel2"
        Me.Servicio_ActualLabel2.Size = New System.Drawing.Size(240, 23)
        Me.Servicio_ActualLabel2.TabIndex = 10
        '
        'Servicio_AnteriorLabel2
        '
        Me.Servicio_AnteriorLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Servicio_AnteriorLabel2.ForeColor = System.Drawing.Color.White
        Me.Servicio_AnteriorLabel2.Location = New System.Drawing.Point(14, 202)
        Me.Servicio_AnteriorLabel2.Name = "Servicio_AnteriorLabel2"
        Me.Servicio_AnteriorLabel2.Size = New System.Drawing.Size(240, 23)
        Me.Servicio_AnteriorLabel2.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(17, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(147, 20)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Datos del Cliente"
        '
        'ConceptoLabel2
        '
        Me.ConceptoLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoLabel2.ForeColor = System.Drawing.Color.White
        Me.ConceptoLabel2.Location = New System.Drawing.Point(14, 154)
        Me.ConceptoLabel2.Name = "ConceptoLabel2"
        Me.ConceptoLabel2.Size = New System.Drawing.Size(240, 23)
        Me.ConceptoLabel2.TabIndex = 6
        '
        'ContratoLabel2
        '
        Me.ContratoLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoLabel2.ForeColor = System.Drawing.Color.White
        Me.ContratoLabel2.Location = New System.Drawing.Point(14, 64)
        Me.ContratoLabel2.Name = "ContratoLabel2"
        Me.ContratoLabel2.Size = New System.Drawing.Size(100, 23)
        Me.ContratoLabel2.TabIndex = 2
        '
        'NombreLabel2
        '
        Me.NombreLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreLabel2.ForeColor = System.Drawing.Color.White
        Me.NombreLabel2.Location = New System.Drawing.Point(14, 103)
        Me.NombreLabel2.Name = "NombreLabel2"
        Me.NombreLabel2.Size = New System.Drawing.Size(240, 38)
        Me.NombreLabel2.TabIndex = 4
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(28, 129)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(80, 27)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "Buscar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(13, 26)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(195, 24)
        Me.CMBLabel1.TabIndex = 2
        Me.CMBLabel1.Text = "Buscar Cliente Por :"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(28, 101)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(142, 22)
        Me.TextBox1.TabIndex = 3
        '
        'ConCambioServClienteDataGridView
        '
        Me.ConCambioServClienteDataGridView.AllowUserToAddRows = False
        Me.ConCambioServClienteDataGridView.AllowUserToDeleteRows = False
        Me.ConCambioServClienteDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConCambioServClienteDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConCambioServClienteDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConCambioServClienteDataGridView.GridColor = System.Drawing.Color.WhiteSmoke
        Me.ConCambioServClienteDataGridView.Location = New System.Drawing.Point(0, 3)
        Me.ConCambioServClienteDataGridView.Name = "ConCambioServClienteDataGridView"
        Me.ConCambioServClienteDataGridView.ReadOnly = True
        Me.ConCambioServClienteDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConCambioServClienteDataGridView.Size = New System.Drawing.Size(581, 686)
        Me.ConCambioServClienteDataGridView.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(858, 17)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&NUEVO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(858, 59)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "&ELIMINAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(858, 681)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(136, 36)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "&SALIR"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.Location = New System.Drawing.Point(923, 70)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(10, 20)
        Me.ClaveTextBox.TabIndex = 2
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(939, 70)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_TipSerTextBox.TabIndex = 4
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'RealizarTextBox
        '
        Me.RealizarTextBox.Location = New System.Drawing.Point(955, 70)
        Me.RealizarTextBox.Name = "RealizarTextBox"
        Me.RealizarTextBox.ReadOnly = True
        Me.RealizarTextBox.Size = New System.Drawing.Size(10, 20)
        Me.RealizarTextBox.TabIndex = 6
        Me.RealizarTextBox.TabStop = False
        '
        'BrwCambioServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 740)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.RealizarTextBox)
        Me.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "BrwCambioServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Servicio"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConCambioServClienteDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ConCambioServClienteDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RealizarTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusLabel2 As System.Windows.Forms.Label
    Friend WithEvents Servicio_ActualLabel2 As System.Windows.Forms.Label
    Friend WithEvents Servicio_AnteriorLabel2 As System.Windows.Forms.Label
    Friend WithEvents ConceptoLabel2 As System.Windows.Forms.Label
    Friend WithEvents ContratoLabel2 As System.Windows.Forms.Label
    Friend WithEvents NombreLabel2 As System.Windows.Forms.Label
    Friend WithEvents Fecha_SolLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
