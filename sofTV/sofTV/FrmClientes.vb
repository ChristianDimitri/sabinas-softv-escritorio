﻿Imports System.Data.SqlClient
Imports System.Drawing.Drawing2D
Imports System.Text
Imports sofTV.BAL
Imports System.Collections.Generic
Imports CrystalDecisions.CrystalReports.Engin
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
'jaladas janop
'edgar

Public Class FrmClientes
    'Se Utiliza para la validacion de:-----------------------------------------------------
    '1. No Agregar más de 1 servicio principal a una Caja/Tarjeta
    '2. Si el cliente es de pagos recurrentes, tenga sus datos bancarios debidamente llenos
    '3. Cuando es Nuevo Cliente, verifique que el nombre de Cliente no exista en la BD

    '--------------------------------------------------------------------------------------
    Dim clv_sesionEntreCallesFrmClientes As Integer
    '''====
    Dim cortesiatv As Boolean = False
    Dim cortesiadig As Boolean = False
    Dim cortesiaint As Boolean = False
    Dim BanderaCambio As Integer = 0
    Dim LocClv_Calletmp As Integer = 0
    Public Shared frmnsb As New FrmNombresSeparadosBuro()
    'Dim LocBndAct As Boolean = False
    Dim eBndNombre As Boolean = True

    ''''''''''Variables Bitacoras'''''''''
    Private locclv_servicio As Integer = 0
    Private contratobit As Integer = 0
    Private nombre_cliente As String = Nothing
    Private Clv_Callebit As Integer = 0
    Private NUMERO_casabit As String = Nothing
    Private ENTRECALLESbit As String = Nothing
    Private Clv_Coloniabit As Integer = 0
    Private CodigoPostalbit As String = Nothing
    Private TELEFONObit As String = Nothing
    Private CELULARbit As String = Nothing
    Private DESGLOSA_Ivabit As Boolean = False
    Private SoloInternetbit As Boolean = False
    Private eshotelbit As Boolean = False
    Private clv_Ciudadbit As Integer = 0
    Private Email As String = Nothing
    Private clv_sectorbit As Integer = 0
    Private Clv_Periodobit As Integer = 0
    Private clv_tipo_pagobit As Integer = 0
    Private NombrePaqueteElimino As String = Nothing
    Private NombreMAC As String = Nothing
    Private GloOp As Integer = 0
    Private LocGloContratoNet As Long = 0
    Private LocGloClv_unianet As Long = 0
    Private GLoPaqueteAgrega As String = Nothing

    '''Digital e Internet'''''''''''
    Private status As String = Nothing
    Private fecha_solicitud As String = Nothing
    Private fecha_instalacio As String = Nothing
    Private fecha_suspension As String = Nothing
    Private fecha_baja As String = Nothing
    Private fecha_Fuera_Area As String = Nothing
    Private FECHA_ULT_PAGO As String = Nothing
    Private PrimerMensualidad As Boolean = False
    Private ultimo_mes As Integer = 0
    Private ultimo_anio As Integer = 0
    Private primerMesAnt As Boolean = False
    Private statusAnt As String = Nothing
    Private facturaAnt As String = Nothing
    Private GENERAOSINSTA As Boolean = False
    Private factura As String = Nothing
    Private Clv_Vendedor As Integer = 0
    Private Clv_Promocion As Integer = 0
    Private Obs As String = Nothing
    Private DESCRIPCION As String = Nothing
    Private Cortesia As Boolean = False
    Private Descuento As String = Nothing
    Private statusTarjeta As String = Nothing
    Private Activacion As String = Nothing
    Private Suspension As String = Nothing
    Private Baja As String = Nothing
    Private serenta As String = Nothing
    Private macasignada As String = Nothing

    'Internet
    Private Locmarca As String = Nothing
    Private LocTipoApar As String = Nothing
    Private LocTipservcabl As String = Nothing
    Private LocTipoCablemodem As String = Nothing
    Private LocTranspaso As String = Nothing
    Private Loc1pago As String = Nothing
    Private Loc2pago As String = Nothing
    Private BndEsInternet As Boolean = False

    'Tv Básica
    Private LocVendedorTv As String = Nothing
    Private LocTVSINPAGO As Integer = 0
    Private LocTVCONPAGO As Integer = 0
    Private LocFec_ULT_PAG_ANT As String = Nothing
    Private LocClv_MOTCAN As Integer = 0
    Private LocPRIMERMESANT As Boolean = False
    Private LocClv_TipoServicioTV As Integer = 0
    Private LocpuntosAcumulados As Integer = 0
    Private LocTipSerTv As String = Nothing
    Private LocMOTCAN As String = Nothing

    Private validacion As Integer = 0

    ''''''''''Fin Variables Bitacora '''''''''''''''
    Private BndMini As Short = 0
    Private lOC_CONTRATONETDIG As Long = 0
    Private loc_Clv_InicaDig As Long = 0
    Private bndbitacora As Boolean = False
    Private NomDig As Char
    Private BndDClientes As Boolean = False
    Private BndRobaSeñal As Boolean = False
    Private eUsuario As String = String.Empty

    '--------------Validaciones de Tipo de Servicios que se preteden agregar
    Dim Tv As Boolean = False
    Dim Internet As Boolean = False
    Dim Digital As Boolean = False

    'PARA TAPS
    Private locClvTapCliente As Integer
    Private locPlacaCliente As Integer
    Dim zonas2 As Boolean = False
    Public Sub DameClaveElector()
        'Try
        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
        '    BaseII.CreateMyParameter("@claveelector", ParameterDirection.Output, SqlDbType.VarChar)
        '    BaseII.ProcedimientoOutPut("DameClaveElector")
        '    tbclvelec.Text = BaseII.dicoPar("@claveelector")
        'Catch ex As Exception

        'End Try
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "DameClaveElector"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CONTRATOTextBox.Text
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@claveelector", SqlDbType.VarChar, 250)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = ""
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@idTipoIdentificación", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = ""
                .Parameters.Add(prm2)

                Dim ia As Integer = .ExecuteNonQuery()

                tbclvelec.Text = prm1.Value
                ComboBoxTipoIdenti.SelectedValue = prm2.Value
            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Function ValidaClaveElector(ByVal op As Integer)
        Dim i As Integer
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@ClaveElector", SqlDbType.VarChar, tbclvelec.Text)
            'BaseII.CreateMyParameter("@res", ParameterDirection.Output, SqlDbType.Int, 0)
            'BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            'BaseII.CreateMyParameter("@contrato", SqlDbType.Int, 0)
            'BaseII.ProcedimientoOutPut("ValidaClaveElector")
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ValidaClaveElector"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@ClaveElector", SqlDbType.VarChar)
                prm.Direction = ParameterDirection.Input
                prm.Value = tbclvelec.Text
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@res", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@opcion", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = op
                .Parameters.Add(prm2)
                Dim contrato As Integer
                If CONTRATOTextBox.Text = "" Then
                    contrato = 0
                Else
                    contrato = CONTRATOTextBox.Text
                End If
                Dim prm3 As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = contrato
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@idTipoIdentificación", SqlDbType.BigInt)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = ComboBoxTipoIdenti.SelectedValue
                .Parameters.Add(prm4)

                Dim ia As Integer = .ExecuteNonQuery()

                i = prm1.Value
            End With
            CONar.Close()
            Return i
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function
    Public Function SP_GuardaRel_Contratos_Companias(ByVal oContrato As Long, ByVal oIdCompania As Integer) As Long
        SP_GuardaRel_Contratos_Companias = 0
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "SP_GuardaRel_Contratos_Companias"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = oContrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@IdCompania", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = oIdCompania
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@ContratoCompania", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@esProspecto", SqlDbType.Bit)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = CheckBoxEsProspecto.Checked
                .Parameters.Add(prm3)

                Dim ia As Integer = .ExecuteNonQuery()

                SP_GuardaRel_Contratos_Companias = prm2.Value
            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function


    Public Sub SP_ConsultaRel_Contratos_Companias(ByVal oContrato As Long)

        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "SP_ConsultaRel_Contratos_Companias"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = oContrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@IdCompania", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@ContratoCompania", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim ia As Integer = .ExecuteNonQuery()
                ComboBoxCompanias.SelectedValue = prm1.Value
                GloIdCompania = ComboBoxCompanias.SelectedValue

                If EsProspecto Then
                    TextBoxNoContrato.Text = prm2.Value.ToString + "-" + "0"
                Else
                    TextBoxNoContrato.Text = prm2.Value.ToString + "-" + prm1.Value.ToString
                End If


                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@IdCompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                BaseII.CreateMyParameter("@permiteCapturarNuevo", ParameterDirection.Output, SqlDbType.Bit)
                BaseII.ProcedimientoOutPut("sp_permiteCapturarCalles")
                CapturaCalle = BaseII.dicoPar("@permiteCapturarNuevo")
            End With
            CONar.Close()
            CONar.Open()
            
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    'Private Sub Llena_companias()
    '    Try


    '        BaseII.limpiaParametros()
    '        'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
    '        'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
    '        ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
    '        ComboBoxCompanias.DisplayMember = "razon_social"
    '        ComboBoxCompanias.ValueMember = "id_compania"
    '        'ComboBoxCiudades.Text = ""
    '        'timercompania.Enabled = True
    '        'timercompania.Start()
    '    Catch ex As Exception

    '    End Try
    'End Sub
    Private Sub Llena_companias_nuevo()
        Try


            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 And (OpcionCli <> "M" And OpcionCli <> "C") Then
                ComboBoxCompanias.SelectedIndex = 0
            End If
            GloIdCompania = 0
            'If OpcionCli = "N" Then
            '    timercompania.Enabled = True
            '    timercompania.Start()
            'End If
            If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                If CONTRATOTextBox.Text > 0 Then
                    SP_ConsultaRel_Contratos_Companias(Me.CONTRATOTextBox.Text)
                End If
            End If
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Llena_companias_contrato()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_Contrato")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            End If
            GloIdCompania = 0
            'If OpcionCli = "N" Then
            '    timercompania.Enabled = True
            '    timercompania.Start()
            'End If
            If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                If CONTRATOTextBox.Text > 0 Then
                    SP_ConsultaRel_Contratos_Companias(Me.CONTRATOTextBox.Text)
                End If
            End If
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Asigna_taps()
        If IdSistema = "LO" Or IdSistema = "VA" Or IdSistema = "YU" Then
            Me.NOMBRETextBox.TabIndex = 1000
            Me.CONTRATOTextBox.TabIndex = 1001
            Me.TextBox4.TabIndex = 0
            Me.TextBox7.TabIndex = 1
            Me.TextBox27.TabIndex = 2

            'Comunes
            Me.CALLEComboBox.TabIndex = 3
            Me.NUMEROTextBox.TabIndex = 4
            Me.TxtNumeroInt.TabIndex = 5
            Me.ENTRECALLESTextBox.TabIndex = 6
            Me.CodigoPostalTextBox.TabIndex = 7
            Me.COLONIAComboBox.TabIndex = 8
            Me.ComboBox15.TabIndex = 9
            Me.CIUDADComboBox.TabIndex = 10
            Me.SoloInternetCheckBox.TabIndex = 11
            Me.DESGLOSA_IvaCheckBox.TabIndex = 12
            Me.TELEFONOTextBox.TabIndex = 13
            Me.CELULARTextBox.TabIndex = 14
            Me.EmailTextBox.TabIndex = 15
            Me.TextBox28.TabIndex = 16
            Me.ComboBox7.TabIndex = 17

        Else
            Me.NOMBRETextBox.TabIndex = 0
            Me.CONTRATOTextBox.TabIndex = 1001
            Me.TextBox4.TabIndex = 1002
            Me.TextBox7.TabIndex = 1003
            Me.TextBox27.TabIndex = 1004

            'Comunes
            Me.CALLEComboBox.TabIndex = 3
            Me.NUMEROTextBox.TabIndex = 4
            Me.TxtNumeroInt.TabIndex = 5
            Me.ENTRECALLESTextBox.TabIndex = 6
            Me.CodigoPostalTextBox.TabIndex = 7
            Me.COLONIAComboBox.TabIndex = 8
            Me.ComboBox15.TabIndex = 9
            Me.CIUDADComboBox.TabIndex = 10
            Me.SoloInternetCheckBox.TabIndex = 11
            Me.DESGLOSA_IvaCheckBox.TabIndex = 12
            Me.TELEFONOTextBox.TabIndex = 13
            Me.CELULARTextBox.TabIndex = 14
            Me.EmailTextBox.TabIndex = 15
            Me.TextBox28.TabIndex = 16
            Me.ComboBox7.TabIndex = 17
        End If
    End Sub
    Private Sub Guarda_No_Int(ByVal contrato As Long, ByVal noInt As String)
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            'Nuevo_Rel_Contrato_NoInt (@contrato bigint,@NoInt varchar(50))
            With cmd
                .CommandText = "Nuevo_Rel_Contrato_NoInt"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = contrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@NoInt", SqlDbType.VarChar, 50)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = noInt
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()
            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Consulta_No_Int(ByVal contrato As Long)
        Dim COnar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            COnar.Open()
            'Consulta_No_Int (@contrato bigint)
            With cmd
                .CommandText = "Consulta_No_Int"
                .Connection = COnar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = contrato
                .Parameters.Add(prm)

                Dim reader As SqlDataReader = .ExecuteReader()
                While reader.Read()
                    Me.TxtNumeroInt.Text = reader.GetValue(0).ToString
                End While
            End With
            COnar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Agenda_Clientes()
        If IdSistema = "LO" Or IdSistema = "YU" Then
            Dim CON As New SqlConnection(MiConexion)
            Dim contador As Integer = 0
            CON.Open()
            'AQUI DEBE DE IR LO DE LA AGENDA
            Dim conlidia As New SqlClient.SqlConnection(MiConexion)
            Dim comando As New SqlClient.SqlCommand
            conlidia.Open()
            With comando
                'Checa_citas2 (@Contrato BIGINT,@Clv_cita bigint,@op int,@error int output)
                .CommandText = "Checa_citas2"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = conlidia
                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(Me.CONTRATOTextBox.Text)
                .Parameters.Add(prm)

                Dim prm2 As New SqlParameter("@Clv_cita", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@op", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@error", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)

                Dim i As Integer = comando.ExecuteNonQuery
                contador = prm4.Value
            End With
            conlidia.Close()
            If contador > 0 Then
                LocBndAgendaClientes = True
                LocGloContratoIni = CLng(Me.CONTRATOTextBox.Text)
                GLOCONTRATOSEL_agenda = CLng(Me.CONTRATOTextBox.Text)
                FrmAgendaRapida.Show()
                'ElseIf contador > 0 Then
                '    Me.Impresion_Contrato()
            End If
        End If
    End Sub

    Private Sub CONSULTARCLIENTEBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        Me.asiganacalle()
        Me.asignaciudad()
        Me.asignacolonia()
        If OpcionCli = "N" Then
            If Len(Trim(Me.NOMBRETextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CalleTextBox.Text) = False Then
                MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
                MsgBox("Se Requiere las Entre Calles ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Numero ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Then
                MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Then
                MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONSULTARCLIENTEBindingSource.EndEdit()
        Me.CONSULTARCLIENTETableAdapter.Connection = CON
        Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
        TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue)
        SP_ConsultaRel_Contratos_Companias(Me.CONTRATOTextBox.Text)
        CON.Close()

    End Sub

    Private Sub FrmClientes_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Try

       
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        Dim Locerror_dir As Integer = 0

        '--No se Que hace
        eEntraUM = True
        eEntraUMB = False

        If GloCmdBanco = 1 Then
            GloCmdBanco = 0
            Me.buscarelaccion()
        End If
        If GloOpPermiso = 0 And GloPermisoCortesia = 1 Then
            Me.ComboBox7.Enabled = True
            Me.ComboBox7.Focus()
        ElseIf GloOpPermiso = 1 And GloPermisoCortesia = 1 Then
            Me.CortesiaCheckBox.Enabled = True
            Me.CortesiaCheckBox.Focus()
        ElseIf GloOpPermiso = 2 And GloPermisoCortesia = 1 Then
            Me.CortesiaCheckBox1.Enabled = True
            Me.CortesiaCheckBox1.Focus()
        ElseIf GloOpPermiso = 3 And GloPermisoCortesia = 1 Then
            Me.CortesiaCheckBox2.Enabled = True
            Me.CortesiaCheckBox2.Focus()
        Else
            If GloTipoUsuario = 40 Then
                Me.ComboBox7.Enabled = True
                Me.CortesiaCheckBox.Enabled = True
                Me.CortesiaCheckBox1.Enabled = True
                Me.CortesiaCheckBox2.Enabled = True
            Else
                    Me.ComboBox7.Enabled = True
                Me.CortesiaCheckBox.Enabled = False
                Me.CortesiaCheckBox1.Enabled = False
                Me.CortesiaCheckBox2.Enabled = False
            End If

        End If
        If bndcambiocable = True Then
            bndcambiocable = False
            frmInternet2.Hide()
            frmInternet2.Show()
            frmctr.Hide()
            frmctr.Crear_Arbol()
            frmctr.Show()
            If (IdSistema = "LO" Or IdSistema = "YU") And (OpcionCli = "C" Or OpcionCli = "M") Then
                DameComboYRenta(CLng(Me.CONTRATOTextBox.Text))
            End If
        End If

        'If GLOMOVNET > 0 And LocBndAct = False Then
        '    LocBndAct = True
        '    If IdSistema = "LO" And (OpcionCli = "C" Or OpcionCli = "M") Then
        '        DameComboYRenta(CLng(Me.CONTRATOTextBox.Text))
        '    End If
        'End If
        'If GLOMOVNET = 0 Then
        '    LocBndAct = False
        'End If


        '--No se Que hace
        'If GloClv_Servicio > 0 And GLOMOVNET = 1 Then
        '    CON.Open()
        '    locclv_servicio = GloClv_Servicio
        '    GloClv_Servicio = 0
        '    Me.Validate()
        '    'eCabModPropio Fué reemplazado por True
        '    Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
        '    Me.CONSULTACLIENTESNETTableAdapter.Insert(Contrato, "P", GloClv_Cablemodem, 0, False, False, 1, "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", GloTipoCablemodem, "", eCabModPropio, LoContratonet)
        '    GloContratonet_Nuevo = LoContratonet
        '    Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
        '    Me.CONSULTACLIENTESNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)
        '    Me.CONSULTACONTNETTableAdapter.Connection = CON
        '    Me.CONSULTACONTNETTableAdapter.Insert(LoContratonet, locclv_servicio, "C", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", True, 0, 0, False, "C", "", True, "", 0, 0, "", "", Me.CortesiaCheckBox2.CheckState, LoClv_Unicanet)
        '    GloClv_UnicaNet_Nuevo = LoClv_Unicanet
        '    Me.CONSULTACONTNETTableAdapter.Connection = CON
        '    Me.CONSULTACONTNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
        '    Me.GUARDARRel_ContNet_UsuariosTableAdapter.Connection = CON
        '    Me.GUARDARRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_ContNet_Usuarios, GloClv_UnicaNet_Nuevo, GloClvUsuario)
        '    'PEriodo
        '    Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
        '    Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, Contrato)
        '    Me.CONSULTARCLIENTETableAdapter.Connection = CON
        '    Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
        '    Me.CREAARBOL()
        '    Me.Panel5.Visible = False
        '    Me.Panel6.Visible = True
        '    Me.VerAparatodelClienteTableAdapter.Connection = CON
        '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(LoContratonet, Long))
        '    Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
        '    Me.CONSULTACLIENTESNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(LoContratonet, Long))
        '    Me.CONSULTACONTNETTableAdapter.Connection = CON
        '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(LoClv_Unicanet, Long)))
        '    Me.CONRel_ContNet_UsuariosTableAdapter.Connection = CON
        '    Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, LoClv_Unicanet)
        '    CON.Close()
        '    Me.TreeView1.ExpandAll()


        '    cmd = New SqlClient.SqlCommand()
        '    CON.Open()
        '    With cmd
        '        .CommandText = "Dame_Mac_CableDeco"
        '        .Connection = CON
        '        .CommandTimeout = 0
        '        .CommandType = CommandType.StoredProcedure

        '        '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
        '        Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
        '        Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
        '        Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
        '        Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

        '        prm.Direction = ParameterDirection.Input
        '        prm1.Direction = ParameterDirection.Input
        '        prm2.Direction = ParameterDirection.Input
        '        prm3.Direction = ParameterDirection.Output

        '        prm.Value = LoContratonet
        '        prm1.Value = 0
        '        prm2.Value = 1
        '        prm3.Value = ""

        '        .Parameters.Add(prm)
        '        .Parameters.Add(prm1)
        '        .Parameters.Add(prm2)
        '        .Parameters.Add(prm3)

        '        Dim i As Integer = cmd.ExecuteNonQuery()

        '        NombreMAC = prm3.Value

        '    End With
        '    CON.Close()

        '    'Aqui stoy
        '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Cablemodem Nuevo: " + NombreMAC, " ", " Se Agrego un Cablemodem Nuevo ", LocClv_Ciudad)


        '    '--Dim lnivel As Integer = 0
        '    'Dim etree As System.Windows.Forms.TreeViewEventArgs = Me.TreeView1.Nodes
        '    'For lnivel = 0 To etree.Node.Level
        '    'Next
        '    GloClv_Servicio = 0
        '    GloClv_Cablemodem = 0
        '    GLOMOVNET = 0

        'ElseIf GloClv_Servicio > 0 And GLOMOVNET = 2 Then
        '    CON.Open()
        '    locclv_servicio = GloClv_Servicio
        '    GloClv_Servicio = 0
        '    LoContratonet = GloContratonet
        '    Me.Validate()
        '    '--Me.CONSULTACLIENTESNETTableAdapter.Insert(Contrato, "P", GloClv_Cablemodem, 0, False, False, 1, "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", 1, "", False, LoContratonet)
        '    '--Me.CONSULTACLIENTESNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)

        '    Me.HABILITACABLEMODEMTableAdapter.Connection = CON
        '    Me.HABILITACABLEMODEMTableAdapter.Fill(Me.NewSofTvDataSet.HABILITACABLEMODEM, New System.Nullable(Of Long)(CType(GloContratonet, Long)))
        '    Me.CONSULTACONTNETTableAdapter.Connection = CON
        '    Me.CONSULTACONTNETTableAdapter.Insert(GloContratonet, locclv_servicio, "C", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", True, 0, 0, False, "C", "", True, "", 0, 0, "", "", Me.CortesiaCheckBox2.CheckState, LoClv_Unicanet)
        '    Me.CONSULTACONTNETTableAdapter.Connection = CON
        '    Me.CONSULTACONTNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
        '    Me.GUARDARRel_ContNet_UsuariosTableAdapter.Connection = CON
        '    Me.GUARDARRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_ContNet_Usuarios, LoClv_Unicanet, GloClvUsuario)
        '    'PEriodo
        '    Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
        '    Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, GloContratonet)
        '    Me.CONSULTARCLIENTETableAdapter.Connection = CON
        '    Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
        '    CON.Close()
        '    Me.CREAARBOL()
        '    Me.Panel5.Visible = False
        '    Me.Panel6.Visible = True
        '    CON.Open()
        '    Me.VerAparatodelClienteTableAdapter.Connection = CON
        '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(LoContratonet, Long))
        '    Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
        '    Me.CONSULTACLIENTESNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(LoContratonet, Long))
        '    Me.CONSULTACONTNETTableAdapter.Connection = CON
        '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(LoClv_Unicanet, Long)))
        '    Me.CONRel_ContNet_UsuariosTableAdapter.Connection = CON
        '    Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, LoClv_Unicanet)
        '    CON.Close()
        '    Me.TreeView1.ExpandAll()

        '    cmd = New SqlClient.SqlCommand()
        '    CON.Open()
        '    With cmd
        '        .CommandText = "Dame_Mac_CableDeco"
        '        .Connection = CON
        '        .CommandTimeout = 0
        '        .CommandType = CommandType.StoredProcedure

        '        '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
        '        Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
        '        Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
        '        Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
        '        Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

        '        prm.Direction = ParameterDirection.Input
        '        prm1.Direction = ParameterDirection.Input
        '        prm2.Direction = ParameterDirection.Input
        '        prm3.Direction = ParameterDirection.Output

        '        prm.Value = LoContratonet
        '        prm1.Value = 0
        '        prm2.Value = 1
        '        prm3.Value = ""

        '        .Parameters.Add(prm)
        '        .Parameters.Add(prm1)
        '        .Parameters.Add(prm2)
        '        .Parameters.Add(prm3)

        '        Dim i As Integer = cmd.ExecuteNonQuery()

        '        NombreMAC = prm3.Value

        '    End With
        '    CON.Close()

        '    'Aqui stoy
        '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Cablemodem Nuevo: " + NombreMAC, " ", "Se Agrego un Cablemodem Nuevo", LocClv_Ciudad)


        '    '--Dim lnivel As Integer = 0
        '    'Dim etree As System.Windows.Forms.TreeViewEventArgs = Me.TreeView1.Nodes
        '    'For lnivel = 0 To etree.Node.Level
        '    'Next
        '    GloClv_Servicio = 0
        '    GloClv_Cablemodem = 0
        '    GLOMOVNET = 0

        'End If
        If bloqueado = 1 Then
            bloqueado = 0
            NUM = 0
            num2 = 0
            CON.Open()
            Me.BuscaBloqueadoTableAdapter.Connection = CON
            Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Contrato, NUM, num2)
            CON.Close()
            If NUM = 0 Then
                Me.Button20.Visible = True
                Me.Button20.Text = "Bloqueo de Cliente" 'aqui no ha hecho nada
            ElseIf num2 = 1 Then
                Me.Button20.Visible = True
                eGloContrato = Me.CONTRATOTextBox.Text
                FrmBloqueo.Show()
                Me.Button20.Text = "Cliente Bloqueado" 'aqui es porque bloqueo el cliente 
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "BloqueodeCliente", " ", "Bloqueo de Cliente", LocClv_Ciudad)
                glopar = "M"
            ElseIf num2 = 0 Then
                Me.Button20.Visible = True
                Me.Button20.Text = "Cliente Bloqueado Anteriormente" ' aqui es porque desbloqueo el cliente 
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "DesbloqueodeCliente", " ", "Desbloqueo de Cliente", LocClv_Ciudad)
                glopar = "M"
            End If

        End If

        If eRobo = True Then
            eRobo = False
            'Eric
            eGloContrato = Me.CONTRATOTextBox.Text
            CON.Open()
            Me.ChecaRoboDeSeñalTableAdapter.Connection = CON
            Me.ChecaRoboDeSeñalTableAdapter.Fill(Me.DataSetEric.ChecaRoboDeSeñal, eGloContrato, eRespuesta)
            CON.Close()
            If eRespuesta = 1 Then
                Me.Button21.BackColor = Color.Red
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "RobodeSeñal", " ", "Robo de Señal", LocClv_Ciudad)
            Else
                Me.Button21.BackColor = Me.Button1.BackColor
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "CancelarRobodeSeñal", " ", "Cancelar Robo de Señal", LocClv_Ciudad)
            End If
            '------------------------------------------------------------------------------------------------
        End If


        'Eric-------------------------------------------
        If eGloDescuento = 1 And eGloTipSerDesc = 1 Then
            eGloDescuento = 0
            eGloTipSerDesc = 0
            eTipSer = 1
            eContrato = Me.CONTRATOTextBox.Text
            FrmRelCteDescuento.Show()
        End If

        If eGloDescuento = 1 And eGloTipSerDesc = 2 Then
            eGloDescuento = 0
            eGloTipSerDesc = 0
            eTipSer = 2
            eClv_UnicaNet = GloClvUnicaNet
            FrmRelCteDescuento.Show()
        End If

        If eGloDescuento = 1 And eGloTipSerDesc = 3 Then
            eGloDescuento = 0
            eGloTipSerDesc = 0
            eTipSer = 3
            eClv_UnicaNetDig = loc_Clv_InicaDig
            FrmRelCteDescuento.Show()
        End If

        If eBndDesc = True And eTipSer = 1 Then
            eBndDesc = False
            BuscaDescTV()
        End If

        If eBndDesc = True And eTipSer = 2 Then
            eBndDesc = False
            BuscaDescNet()
        End If

        If eBndDesc = True And eTipSer = 3 Then
            eBndDesc = False
            BuscaDescDig()
        End If


        'If SoftvMod.VariablesGlobales.BndContratacionCombo = True Then
        '    'La variable la paso a False más abajo
        '    'SoftvMod.VariablesGlobales.BndContratacionCombo = False
        '    If SoftvMod.VariablesGlobales.TieneTv = True Then Tiene_Tv = 1 Else Tiene_Tv = 0
        '    If SoftvMod.VariablesGlobales.TieneInt = True Then Tiene_Internet = 1 Else Tiene_Internet = 0
        '    If SoftvMod.VariablesGlobales.TieneDig = True Then Tiene_Dig = 1 Else Tiene_Dig = 0
        '    buscaCONCLIENTETV()
        '    HABILITA_CTRS()
        '    LocbndContratacionCombo = True
        'End If

        If LocbndContratacionCombo = True Then
            'muestra_telefonia()
            LocbndContratacionCombo = False


            If Tiene_Tel = 1 Then
                globndTel = True
                Equip_Int = False
                Equip_tel = True
                optTel = 5
                GloBndNum = True
                frmctr.Activar()
                muestra_telefonia()
                If Tiene_Tv = 1 Then 'Por si tambien tiene Basico
                    If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                        buscaCONCLIENTETV()
                        damedatostv_2(Me.CONTRATOTextBox.Text)
                        Me.ToolStripButton2.Enabled = True
                        Me.ToolStripButton3.Enabled = True
                        Me.Button29.Enabled = True
                        Me.CONCLIENTETVBindingNavigator.Items(4).Visible = False
                        Me.Panel3.Enabled = True
                        Me.Panel2.Enabled = True
                        Me.TVCONPAGONumericUpDown.Enabled = True
                        Me.TVSINPAGONumericUpDown.Enabled = True
                        FrmTvAdicionales.Show()
                    End If
                End If
                If Me.Button8.Visible = True Then
                    If Me.Button8.Enabled = False Then Me.Button8.Enabled = True
                    If Me.Button8.Text = "&Telefonia" Then
                        Me.Button8.Enabled = False
                    End If
                End If
                If Me.Button7.Visible = True Then
                    If Me.Button7.Enabled = False Then Me.Button7.Enabled = True
                    If Me.Button7.Text = "&Telefonia" Then
                        Me.Button7.Enabled = False
                    End If
                End If
                If Me.Button11.Visible = True Then
                    If Me.Button11.Enabled = False Then Me.Button11.Enabled = True
                    If Me.Button11.Text = "&Telefonia" Then
                        Me.Button11.Enabled = False
                    End If
                End If
                If Me.Button28.Visible = True Then
                    If Me.Button28.Enabled = False Then Me.Button28.Enabled = True
                    If Me.Button28.Text = "&Telefonia" Then
                        Me.Button28.Enabled = False
                    End If
                End If

            ElseIf Tiene_Tv = 1 Then

                If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                    buscaCONCLIENTETV()
                    damedatostv_2(Me.CONTRATOTextBox.Text)
                    Me.ToolStripButton2.Enabled = True
                    Me.ToolStripButton3.Enabled = True
                    Me.Button29.Enabled = True
                    Me.CONCLIENTETVBindingNavigator.Items(4).Visible = False
                    Me.Panel3.Enabled = True
                    Me.Panel2.Enabled = True
                    Me.TVCONPAGONumericUpDown.Enabled = True
                        Me.TVSINPAGONumericUpDown.Enabled = True
                        SP_ConsultaRel_Contratos_Companias(Me.CONTRATOTextBox.Text)
                End If


                If Me.Button8.Visible = True Then
                    If Me.Button8.Enabled = False Then Me.Button8.Enabled = True
                        If Me.Button8.Text.Contains("&Televis") Then
                            Me.Button8.Enabled = False
                        End If
                End If
                If Me.Button7.Visible = True Then
                    If Me.Button7.Enabled = False Then Me.Button7.Enabled = True
                        If Me.Button7.Text.Contains("&Televis") Then
                            Me.Button7.Enabled = False
                        End If
                End If
                If Me.Button11.Visible = True Then
                    If Me.Button11.Enabled = False Then Me.Button11.Enabled = True
                        If Me.Button11.Text.Contains("&Televis") Then
                            Me.Button11.Enabled = False
                        End If
                End If
                If Me.Button28.Visible = True Then
                    If Me.Button28.Enabled = False Then Me.Button28.Enabled = True
                        If Me.Button28.Text.Contains("&Televis") Then
                            Me.Button28.Enabled = False
                        End If
                End If
            End If

            If (IdSistema = "LO" Or IdSistema = "YU") And (OpcionCli = "C" Or OpcionCli = "M") Then
                DameComboYRenta(CLng(Me.CONTRATOTextBox.Text))
            End If

         

            'actualizo los forms de abajo para que c ba lo generado
        End If

        '''''''
            'Dim conexion As New SqlConnection(MiConexion)
            '    conexion.Open()
            '    '  Dim contrato As Integer = CONTRATOTextBox.Text
            '    Dim consulta As New SqlCommand("SELECT  * FROM Recontratacion WHERE Contrato = @contrato", conexion)
            '    Dim par2 As New SqlParameter("@contrato", SqlDbType.BigInt)
            'par2.Direction = ParameterDirection.Input
            '    par2.Value = contrato
            'consulta.Parameters.Add(par2)
            '    Dim contr As Integer = CInt(consulta.ExecuteScalar())
            'Console.WriteLine("contrato es ", contr)
            'conexion.Close()
            ''''''''
            '    If contr > 0 Then
            '        bnGeneraOrden.Enabled = False
            '    End If


            'conexion.Open()
            '    Dim consulta1 As New SqlCommand("SELECT * FROM CLIENTES X WHERE EXISTS (SELECT * FROM ClientesTV WHERE CONTRATO=X.CONTRATO AND STATUS= ('C') AND CONTRATO = @contrato ) OR EXISTS (SELECT * FROM ClientesNet A INNER JOIN ContNet B ON A.ContratoNet =B.ContratoNet WHERE A.Contrato =X.CONTRATO AND B.status   = ('C') AND CONTRATO = @contrato ) OR EXISTS (SELECT * FROM ClientesDig  A INNER JOIN ContDig  B ON A.ContratoNet =B.ContratoNet WHERE A.Contrato =X.CONTRATO AND B.status   = ('C') AND CONTRATO = @contrato) AND EXISTS (SELECT * FROM ClientesTV WHERE CONTRATO =X.CONTRATO AND STATUS = ('C') AND CONTRATO = @contrato) OR EXISTS(SELECT * FROM ClientesNet A INNER JOIN ContNet B ON A.ContratoNet =B.ContratoNet WHERE A.Contrato =X.CONTRATO AND B.status  = ('C') AND CONTRATO = @contrato) OR EXISTS(SELECT * FROM ClientesDig  A INNER JOIN ContDig  B ON A.ContratoNet =B.ContratoNet WHERE A.Contrato =X.CONTRATO AND B.status  = ('C') AND CONTRATO = @contrato)", conexion)
            'Dim par21 As New SqlParameter("@contrato", SqlDbType.BigInt)
            'par21.Direction = ParameterDirection.Input
            '    par21.Value = Contrato
            'consulta1.Parameters.Add(par21)
            '    Dim contr1 As Integer = CInt(consulta1.ExecuteScalar())
            'Console.WriteLine("contrato es ", contr1)
            'conexion.Close()
            '    If contr1 <> Contrato Then
            '        bnGeneraOrden.Enabled = False
            '    End If
            '    conexion.Close()
        '''''''

       

            'If OpcionCli = "N" And eBndEntraDire = True Then

            '    eBndEntraDire = False

            '    If Locclv_session2 = 0 Then
            '        Me.Dame_clv_session_clientesTableAdapter.Connection = CON
            '        Me.Dame_clv_session_clientesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_clv_session_clientes, Locclv_session2)
            '        borra_valida_direccion(Locclv_session2)
            '    End If
            '    Me.Valida_Direccion1TableAdapter.Connection = CON
            '    Me.Valida_Direccion1TableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_Direccion1, Locclv_session2, CLng(Me.COLONIAComboBox.SelectedValue), CLng(Me.CALLEComboBox.SelectedValue), Me.NUMEROTextBox.Text, Locerror_dir)

            '        If Locerror_dir = 1 And Locbnd2clientes = False Then
            '            'If bnddir = False Then
            '            'bnddir = True
            '            Locbndguardar = False
            '            MsgBox("Esta Direccion Ya Esta Asignada a Otro(s) Cliente(s)", MsgBoxStyle.Information)
            '            Dim fRMMD As New FrmMuestraDireccion
            '            'FrmMuestraDireccion.Show()
            '            fRMMD.ShowDialog()

            '            Exit Sub
            '            'End If
            '        End If

            'End If

        If Locbndguardar = True Then
            Locbndguardar = False
            CON.Open()
            Me.Validate()
            Me.CONSULTARCLIENTEBindingSource.EndEdit()
            Me.CONSULTARCLIENTETableAdapter.Connection = CON
            Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
                CON.Close()
                TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue)
                If EsProspecto Then
                    TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + "0"
                    Else
                    TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + ComboBoxCompanias.SelectedValue.ToString
                End If

                ComboBoxCompanias.Enabled = False
                timercompania.Enabled = False
                panelcompania.BackColor = Color.WhiteSmoke
            If Me.SoloInternetCheckBox.Checked = True Then
                    'Me.SoloInternetCheckBox.Enabled = False
            End If
            GUARDARRel_Clientes_TiposClientesGuarda()
            Guarda_No_Int(Me.CONTRATOTextBox.Text, Me.TxtNumeroInt.Text)
            'GUARDA LA PLACA Y TAP DEL CLIENTE (INICIO) *JUANJO*
                uspGuardaPlacaTapCliente(CLng(Me.CONTRATOTextBox.Text), CInt(Me.cmbTap.SelectedValue), Me.txtPlaca.Text, 0)
                NueRelClienteTab(Me.CONTRATOTextBox.Text, Me.cmbTap.Text, "Tap", cmbTap.SelectedValue)
                NueRelClienteTab(Me.CONTRATOTextBox.Text, Me.cmbNap.Text, "Nap", cmbNap.SelectedValue)
                'GUARDA LA PLACA Y TAP DEL CLIENTE (FIN) *JUANJO*

                GuardaEntreCallesTemporales()

                GuardaEstadoYLoDemas(Me.CONTRATOTextBox.Text)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@ClaveElector", SqlDbType.VarChar, tbclvelec.Text, 250)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                BaseII.CreateMyParameter("@idTipoIdentificación", SqlDbType.BigInt, ComboBoxTipoIdenti.SelectedValue)
                BaseII.Inserta("GuardaClaveElector")
                If GuardaMismaElector Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    BaseII.Inserta("GuardaBitacoraElector")
                    GuardaMismaElector = False
                End If
                MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
                guardabitacora()
                'GuardaCuenta()
                BuscaCuenta()
                PreguntaSiQuiereCombo()
                eGuardarCliente = True
                'Guarda registro de direcciones repetidas
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, OpCoincidencias)
                BaseII.Inserta("GuardaRegistroDireccionSimilar")
                'Apellidos(2)
                GuardaClientesApellidos()
                SP_ConsultaRel_Contratos_Companias(CONTRATOTextBox.Text)
                If preguntaFactura Then
                    Dim res = MessageBox.Show("¿Requiere Factura Físcal?", "¡Atención!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                    If res = 1 Then
                        If IsNumeric(Contrato) = True Then
                            If CLng(Contrato) > 0 Then
                                FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
                                FacturacionDigitalSoftv.ClassCFDI.LocClv_Ciudad = ""
                                FacturacionDigitalSoftv.ClassCFDI.OpcionCli = OpcionCli
                                FacturacionDigitalSoftv.ClassCFDI.GloContrato = Contrato
                                Dim frm As New FacturacionDigitalSoftv.FrmDatosFiscales
                                frm.ShowDialog()
                            End If
                        End If
                        preguntaFactura = False
                        'FrmDatosFiscales.requiereFactura = False
                    End If
                End If
            End If
            If Locbndvalcliente = True Then
                Locbndvalcliente = False
                dame_datos()
                CON.Open()
                Me.Valida_servicioTvTableAdapter.Connection = CON
                Me.Valida_servicioTvTableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_servicioTv, Contrato, validacion)
                CON.Close()
                If validacion > 0 Then
                    damedatostv(Contrato)
                End If
                'colorea(Me, Me.Name)
            End If
            '-------------------------------------------------



            'Cambia la vista de la pantalla de clientes cuando se agrega un Combo
            'Por ejemplo, si se agrega un combo que tiene Digital e Internet
            'En lugar de mostrar el Pane de TV se mostrará el de Internet
            If SoftvMod.VariablesGlobales.BndContratacionCombo = True Then
                SoftvMod.VariablesGlobales.BndContratacionCombo = False

                If SoftvMod.VariablesGlobales.TieneInt = True Then
                    BotonInternet()

                End If

                If SoftvMod.VariablesGlobales.TieneDig = True Then
                    BotonDigital()
                    Exit Sub
                End If
            End If
            ''-----------------------------------------------------------------------------


            ''----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            ''14 de Noviembre de 2011
            ''Cuando se trate de un registro nuevo aparece la pantalla de caputura de Nombre
            If OpcionCli = "N" And eBndNombre = True Then

                eBndNombre = False
                If (frmnsb.ShowDialog() = Windows.Forms.DialogResult.OK) Then
                    NOMBRETextBox.Text = frmnsb.txtNombre.Text + " " + frmnsb.txtSegundoNombre.Text + " " + frmnsb.txtApellidoPaterno.Text + " " + frmnsb.txtApellidoMaterno.Text
                    If (OpcionCli = "M") Then
                        GuardaClientesApellidos()
                    End If
                End If

            End If
            ''------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


            ''03 de Diciembre de 2011
            ''Cuando el Cliente tiene cablemodem propio
            'If SoftvMod.VariablesGlobales.BndCablemodemPropio = True Then
            '    SoftvMod.VariablesGlobales.BndCablemodemPropio = False
            '    'GLOMOVNET = 2
            '    'GloClv_Cablemodem = 0
            '    'GloContratonet = Me.ComboBox1.SelectedValue
            '    'GloClv_Servicio = 0
            '    GloClv_TipSer = 2
            '    GLOMOVNET = 1
            '    De_Internet = True
            '    GloClv_Cablemodem = SoftvMod.VariablesGlobales.Clv_Cablemodem
            '    FrmSelServicios.Show()
            'End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub GuardaClientesApellidos()
        Dim fechaN As Date = CDate(frmnsb.dtpFechaNacimiento.Value.ToShortDateString())
        ClienteApellidos.Add(Long.Parse(Me.CONTRATOTextBox.Text), frmnsb.txtNombre.Text, frmnsb.txtSegundoNombre.Text, frmnsb.txtApellidoPaterno.Text, frmnsb.txtApellidoMaterno.Text, fechaN, frmnsb.txtRFC.Text, IIf(frmnsb.RadioButton1.Checked, True, False))

    End Sub

    Private Sub Activa_controles()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim cont, contv, cont2 As Integer
        Me.Button12.Enabled = True
        Me.Panel7.Enabled = True
        CONCLIENTETVBindingNavigator.Enabled = True
        If IsNumeric(Me.ValidaTextBox.Text) = False Then Me.ValidaTextBox.Text = 0
        If IsNumeric(Me.ValidaTextBox1.Text) = False Then Me.ValidaTextBox1.Text = 0
        If Me.ValidaTextBox.Text > 0 Then
            Me.Button4.Enabled = True
        Else
            Me.Button4.Enabled = False
        End If
        If Me.ValidaTextBox1.Text > 0 Then
            Me.Button6.Enabled = True
        Else
            Me.Button6.Enabled = False
        End If
        Me.Button1.Enabled = True
        Me.Button3.Enabled = True
        Me.Button2.Enabled = True

        Me.Panel4.Enabled = True
        'If Me.Button28.Visible = True And IdSistema = "LO" Then
        '    Me.Button8.Enabled = False
        '    Me.Button28.Enabled = True
        'Else
        '    Me.Button8.Enabled = True
        '    If IdSistema = "YU" Then
        '        Me.Button8.Enabled = False
        '        Me.Button7.Enabled = True
        '        Me.Button28.Enabled = True
        '    End If

        'End If
        ''Prueba_Edgar_13/12/2008
        Me.Valida_facturasTableAdapter.Connection = CON
        Me.Valida_facturasTableAdapter.Fill(Me.DataSetarnoldo.valida_facturas, Me.CONTRATOTextBox.Text, cont2)
        If cont2 > 0 Then
            cont2 = 0
            Me.Button1.Enabled = True
        Else
            Me.Button1.Enabled = False
        End If

        If Me.Button8.Text = "IPTV" Or Me.Button8.Text = "&Premium" Then
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, lOC_CONTRATONETDIG, 1, cont)
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 2, contv)
            'If cont > 0 Or contv > 0 Then
            '    Me.Button7.Enabled = True
            'End If
        ElseIf Me.Button8.Text.Contains("&Televis") Then
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, lOC_CONTRATONETDIG, 1, cont)
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 3, contv)
            'If cont > 0 Or contv > 0 Then
            '    Me.Button11.Enabled = True
            '    If IdSistema = "SA" Or IdSistema = "VA" Then
            '        Me.Button8.Enabled = True
            '    End If
            'End If

        End If
        Me.Button8.Enabled = False
        If Me.Button7.Visible = True Then Me.Button7.Enabled = True
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True


        CON.Close()



    End Sub

    Private Sub Desactiva_controles()
        Me.Panel4.Enabled = False
        Me.Button12.Enabled = False
        Me.CONCLIENTETVBindingNavigator.Enabled = False
        Me.Panel7.Enabled = False
        Me.Button7.Enabled = False
        Me.Button8.Enabled = False
        Me.Button11.Enabled = False
        Me.Button28.Enabled = False
    End Sub


    Private Sub FrmClientes_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        Dim CMd As New SqlClient.SqlCommand

        Try



            ' CON.Open()
            GloBnd = True
            If GloGuardarNet = True Then
                'Me.BorraNetPor_NoGRaboTableAdapter.Connection = CON
                'Me.BorraNetPor_NoGRaboTableAdapter.Fill(Me.NewSofTvDataSet.BorraNetPor_NoGRabo, New System.Nullable(Of Long)(CType(GloContratonet_Nuevo, Long)), New System.Nullable(Of Long)(CType(0, Long)))
                GloContratonet_Nuevo = 0
                GloGuardarNet = False
            End If
            If GloGuardarDig = True Then
                'Me.BorraDigPor_NoGRaboTableAdapter.Connection = CON
                'Me.BorraDigPor_NoGRaboTableAdapter.Fill(Me.NewSofTvDataSet.BorraDigPor_NoGRabo, New System.Nullable(Of Long)(CType(GloContratoDig_Nuevo, Long)), New System.Nullable(Of Long)(CType(0, Long)))
                GloGuardarDig = False
            End If

            If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                Genera_Ordenes_Logic(CInt(Me.CONTRATOTextBox.Text))
                Agenda_Clientes()
            End If
            'If IdSistema = "VA" And (OpcionCli = "N" Or OpcionCli = "M") And IsNumeric(Me.CONTRATOTextBox.Text) = True Then
            '    CON.Open()
            '    With CMd
            '        .CommandText = "Determina_Edo_Cuenta"
            '        .CommandTimeout = 0
            '        .CommandType = CommandType.StoredProcedure
            '        .Connection = CON
            '        Dim Prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
            '        Prm.Direction = ParameterDirection.Input
            '        Prm.Value = Me.CONTRATOTextBox.Text
            '        .Parameters.Add(Prm)
            '        Dim i As Integer = CMd.ExecuteNonQuery
            '    End With
            '    CON.Close()
            'End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
            BaseII.CreateMyParameter("@tienePaquete", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("ClienteTienePaquete")
            Dim res = BaseII.dicoPar("@tienePaquete")
            If res = 0 Then
                MsgBox("Existe al menos un Setup Box que NO se le ha asignado un Servicio, complete la acción para salir.")
                e.Cancel = True
            End If
            'Vamos a ver si este señor tiene solo internet, pero no es solo internet para preguntar si quiere ser solo internet
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
            BaseII.CreateMyParameter("@pregunta", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("sp_dimePreguntoSoloInternet")
            If BaseII.dicoPar("@pregunta") = 1 Then
                res = MessageBox.Show("¿El cliente solo tiene servicio de internet, desea asginarlo como Solo Internet?", "¡Atención!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                If res = 1 Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
                    BaseII.CreateMyParameter("@solo", SqlDbType.Int, 1)
                    BaseII.Inserta("sp_guardaSoloInternet")
                    bitsist(GloUsuario, Contrato, "Softv", "Clientes", "Cambio de Solo Internet", "", "Cambio de Cliente Normal a Solo Internet", "AG")
                Else
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        Catch ex As Exception
        Finally
            'CON.Close()
        End Try
        frmctr.WindowState = FormWindowState.Minimized
        frmInternet2.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub GuardaModificame_cablemodem()
        If BanderaCambio = 1 Then
            BanderaCambio = 0
            Dim CON2 As New SqlConnection(MiConexion)
            Dim CMd As New SqlClient.SqlCommand
            CON2.Open()
            With CMd
                .CommandText = "Modificame_Cablemodem"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON2
                Dim Prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = Me.CONTRATOTextBox.Text
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@ContratoNet", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = LocGloContratoNet
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@Mac", SqlDbType.VarChar, 50)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = Me.Label41.Text
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@Tipo_Aparato", SqlDbType.Int)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = 1
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@Respuesta", SqlDbType.VarChar, 250)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = ""
                .Parameters.Add(Prm)

                Dim i As Integer = CMd.ExecuteNonQuery
            End With
            CON2.Close()
        End If
    End Sub


    Private Sub GuardaModificame_cablemodem_Internet()
        If BanderaCambio = 1 Then
            BanderaCambio = 0
            Dim CON2 As New SqlConnection(MiConexion)
            Dim CMd As New SqlClient.SqlCommand
            CON2.Open()
            With CMd
                .CommandText = "Modificame_Cablemodem"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON2
                Dim Prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = Me.CONTRATOTextBox.Text
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@ContratoNet", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = LocGloContratoNet
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@Mac", SqlDbType.VarChar, 50)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = Me.MACCABLEMODEMLabel1.Text
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@Tipo_Aparato", SqlDbType.Int)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = 0
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@Respuesta", SqlDbType.VarChar, 250)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = ""
                .Parameters.Add(Prm)

                Dim i As Integer = CMd.ExecuteNonQuery
            End With
            CON2.Close()
        End If
    End Sub

    'Private Sub HABILITA_CTRS()
    '    ''PROCEDIMIENTO TIPOS DE SERVICIOS 
    '    Dim CONq As New SqlConnection(MiConexion)
    '    CONq.Open()
    '    Dim HAB_TV, cont, conttv As Integer
    '    Dim HAB_TVDIG As Integer
    '    Dim HAB_INTERNET As Integer
    '    Me.DAMESTATUSHABTableAdapter.Connection = CONq
    '    Me.DAMESTATUSHABTableAdapter.Fill(Me.DataSetLidia.DAMESTATUSHAB, 1, HAB_TV)
    '    Me.DAMESTATUSHABTableAdapter.Connection = CONq
    '    Me.DAMESTATUSHABTableAdapter.Fill(Me.DataSetLidia.DAMESTATUSHAB, 2, HAB_INTERNET)
    '    Me.DAMESTATUSHABTableAdapter.Connection = CONq
    '    Me.DAMESTATUSHABTableAdapter.Fill(Me.DataSetLidia.DAMESTATUSHAB, 3, HAB_TVDIG)
    '    CONq.Close()

    '    If HAB_TV = 0 And Me.SoloInternetCheckBox.Checked = False Then
    '        Me.Button8.Visible = True
    '        Me.Button7.Visible = False
    '        Me.Button11.Visible = False
    '        Me.Button8.Text = "&Televisión"
    '        Me.Panel2.Visible = True
    '        Me.Panel7.Visible = False
    '        Me.Panel4.Visible = False
    '        Me.SoloInternetCheckBox.Enabled = False
    '    ElseIf HAB_TVDIG = 0 And Me.SoloInternetCheckBox.Checked = False Then
    '        Me.Button8.Visible = True
    '        Me.Button7.Visible = False
    '        Me.Button11.Visible = False
    '        If IdSistema = "SA" Or IdSistema = "VA" Then
    '            Me.Button8.Text = "&Premium"
    '        Else
    '            Me.Button8.Text = "&Tv Digital"
    '        End If
    '        Me.Panel7.Visible = True
    '        Me.Panel4.Visible = False
    '        Me.SoloInternetCheckBox.Enabled = False
    '        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '            Contrato = 0
    '        Else
    '            Contrato = Me.CONTRATOTextBox.Text
    '        End If
    '        frmctr.MdiParent = Me
    '        frmInternet2.MdiParent = Me
    '        'frmInternet2.Show()
    '        'frmctr.Show()
    '        frmInternet2.Hide()

    '        frmctr.Hide()
    '    ElseIf HAB_INTERNET = 0 And (Me.Button8.Text <> "&Tv Digital" And Me.Button8.Text <> "&Premium") Then
    '        Me.Button8.Visible = True
    '        Me.Button8.Text = "&Internet"
    '        Me.Button7.Visible = False
    '        Me.Panel2.Visible = False
    '        'Me.Panel4.Visible = True
    '        Me.SplitContainer1.Enabled = True
    '        Me.Button11.Visible = False
    '        Me.SoloInternetCheckBox.Enabled = False
    '        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '            Contrato = 0
    '        Else
    '            Contrato = Me.CONTRATOTextBox.Text
    '        End If
    '        frmctr.MdiParent = Me
    '        frmInternet2.MdiParent = Me
    '        'frmInternet2.Show()
    '        'frmctr.Show()
    '        frmInternet2.Hide()

    '        frmctr.Hide()
    '        'frmctr.TreeView1.ExpandAll()
    '    ElseIf HAB_TVDIG = 0 And Me.Button8.Text = "&Televisión" Then
    '        Me.Button7.Visible = True
    '        Me.Button11.Visible = False
    '        If IdSistema = "SA" Or IdSistema = "VA" Then
    '            Me.Button7.Text = "&Premium"
    '        Else
    '            Me.Button7.Text = "&Tv Digital"
    '        End If
    '        If IdSistema = "SA" Or IdSistema = "VA" Then
    '            If BndMini = 0 Then Button8.Enabled = True
    '            'Button11.Enabled = True
    '            If OpcionCli <> "N" Then
    '                If BndMini = 0 Then Me.Button7.Enabled = True
    '                'Else
    '                Button11.Enabled = True
    '            End If
    '        Else
    '            Me.Button7.Enabled = True
    '        End If
    '        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '            Contrato = 0
    '        Else
    '            Contrato = Me.CONTRATOTextBox.Text
    '        End If
    '        frmctr.MdiParent = Me
    '        frmInternet2.MdiParent = Me
    '        frmInternet2.Show()
    '        frmctr.Show()
    '        frmInternet2.Hide()
    '        frmctr.Hide()
    '    ElseIf HAB_INTERNET = 0 And (Me.Button8.Text = "&Tv Digital" Or Me.Button8.Text = "&Televisión" Or Me.Button8.Text = "&Premium") Then
    '        Me.Button7.Visible = True
    '        Me.Button11.Visible = False
    '        Me.Button7.Text = "&Internet"
    '        Dim CONx As New SqlConnection(MiConexion)
    '        CONx.Open()
    '        Me.CONTARCLIENTESTableAdapter.Connection = CONx
    '        Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, Contrato, 3, cont)
    '        Me.CONTARCLIENTESTableAdapter.Connection = CONx
    '        Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 2, conttv)
    '        CONx.Close()
    '        If cont > 0 Or conttv > 0 Then
    '            Me.Button7.Enabled = True
    '        End If
    '        Me.SoloInternetCheckBox.Enabled = True
    '        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '            Contrato = 0
    '        Else
    '            Contrato = Me.CONTRATOTextBox.Text
    '        End If
    '        frmctr.MdiParent = Me
    '        frmInternet2.MdiParent = Me
    '        frmInternet2.Show()
    '        frmctr.Show()
    '        frmInternet2.Hide()
    '        frmctr.Hide()
    '    ElseIf HAB_INTERNET = 0 And (Me.Button7.Text = "&Tv Digital" Or Me.Button7.Text = "&Premium") Then
    '        Me.Button11.Visible = True
    '        Me.Button11.Text = "&Internet"
    '        Me.SoloInternetCheckBox.Enabled = True
    '    ElseIf HAB_INTERNET = 0 And Me.Button8.Text = "&Internet" Then
    '        Me.SoloInternetCheckBox.Enabled = False
    '        Me.Panel2.Hide()
    '        Me.Panel4.Hide()
    '        Me.Panel7.Hide()
    '        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '            Contrato = 0
    '        Else
    '            Contrato = Me.CONTRATOTextBox.Text
    '        End If
    '        frmctr.MdiParent = Me
    '        frmInternet2.MdiParent = Me
    '        frmInternet2.Show()
    '        frmctr.Show()
    '        frmctr.TreeView1.ExpandAll()
    '    End If
    'End Sub



    Private Sub HABILITA_CTRS() 'Prueba
        Dim cont, conttv As Integer
        Dim ContadorBotones As Integer = 4
        Dim Contadorcuantos As Integer = 0
        Dim Loc_HAB_TV As Integer = 1
        Dim Loc_HAB_TVDIG As Integer = 1
        Dim Loc_HAB_INTERNET As Integer = 1
        Dim Loc_HAB_Telefonia As Integer = 1

        Loc_HAB_TV = HAB_TV
        Loc_HAB_TVDIG = HAB_TVDIG
        Loc_HAB_INTERNET = HAB_INTERNET
        Loc_HAB_Telefonia = HAB_Telefonia

        'Vamos a inicializar

        Me.Button8.Visible = False
        Me.Button7.Visible = False
        Me.Button11.Visible = False
        Me.Button28.Visible = False

        If Loc_HAB_TV = 0 Then Contadorcuantos = Contadorcuantos + 1
        If Loc_HAB_TVDIG = 0 Then Contadorcuantos = Contadorcuantos + 1
        If Loc_HAB_INTERNET = 0 Then Contadorcuantos = Contadorcuantos + 1
        If Loc_HAB_Telefonia = 0 Then Contadorcuantos = Contadorcuantos + 1

        'cuales Servicios es Principal
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
            Contrato = 0
        Else
            Contrato = Me.CONTRATOTextBox.Text
        End If
        BndMini = 0
        If Contrato > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Dime_Si_ESMiniBasicoTableAdapter.Connection = CON
            Me.Dime_Si_ESMiniBasicoTableAdapter.Fill(Me.DataSetEDGAR.Dime_Si_ESMiniBasico, New System.Nullable(Of Long)(CType(Contrato, Long)), BndMini)
            CON.Close()
        End If
        'If (IdSistema = "SA" Or IdSistema = "VA") And BndMini = 1 Then
        '    'Si el Cliente tiene un Servicio MiniBasico no Puede Tener Otros Servicios
        '    If Me.Button7.Visible = True Then Me.Button7.Enabled = False
        '    If Me.Button8.Visible = True Then Me.Button8.Enabled = False
        '    If Me.Button11.Visible = True Then Me.Button11.Enabled = False
        '    If Me.Button28.Visible = True Then Me.Button28.Enabled = False
        'End If

        'Primer Boton Button8

        If Me.SoloInternetCheckBox.Checked = False Or Me.SoloInternetCheckBox.Checked = True Then
            'Clienes que NO son de Solointernet
            If Me.SoloInternetCheckBox.Checked = False Then
                Select Case Servicio_Principal
                    Case 1
                        BndEsInternet = False
                        Me.Panel2.Visible = False
                        Me.Panel4.Visible = False
                        Me.Panel7.Visible = False
                        Me.Panel2.Visible = True
                        Me.Button8.Visible = True
                        Me.Button8.Enabled = False
                        Me.Button8.Text = "&Televisión"
                        'Me.SoloInternetCheckBox.Enabled = False
                        frmctr.Hide()
                        'No lo Usas frmTelefonia.Hide()
                        frmInternet2.Hide()
                        frmctr.Hide()
                        frmInternet2.Hide()
                        Loc_HAB_TV = 2
                        Contadorcuantos = Contadorcuantos - 1
                    Case 2
                        Me.Button8.Visible = True
                        Me.Button8.Enabled = False
                        Me.Button8.Text = "&Internet"
                        'Me.Button7.Visible = False
                        'Me.Panel2.Visible = True
                        'Me.Panel2.Visible = False
                        'Me.Panel4.Visible = True
                        Me.SplitContainer1.Enabled = True
                        'Me.Button11.Visible = False
                        'Me.SoloInternetCheckBox.Enabled = False
                        'frmctr.MdiParent = Me
                        'frmInternet2.MdiParent = Me
                        'frmInternet2.Show()
                        'frmctr.Show()
                        BndEsInternet = True
                        frmctr.MdiParent = Me
                        frmInternet2.MdiParent = Me
                        frmctr.WindowState = FormWindowState.Normal
                        frmInternet2.Show()
                        frmctr.Show()
                        frmctr.Boton_Internet()
                        frmctr.TreeView1.ExpandAll()
                        'frmInternet2.Hide()
                        'frmctr.Hide()                    
                        Loc_HAB_INTERNET = 2
                        Contadorcuantos = Contadorcuantos - 1
                    Case 3
                        Me.Button8.Visible = True
                        Me.Button8.Enabled = False
                        If IdSistema = "SA" Or IdSistema = "VA" Then
                            Me.Button8.Text = "&Premium"
                        Else
                            Me.Button8.Text = "IPTV"
                        End If
                        Me.Panel12.Visible = False
                        Me.Panel7.Visible = True
                        Me.Panel4.Visible = False
                        'Me.SoloInternetCheckBox.Enabled = False
                        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
                            Contrato = 0
                        Else
                            Contrato = Me.CONTRATOTextBox.Text
                        End If
                        frmctr.MdiParent = Me
                        frmInternet2.MdiParent = Me
                        'frmInternet2.Show()
                        'frmctr.Show()
                        frmInternet2.Hide()
                        Loc_HAB_TVDIG = 2
                        Contadorcuantos = Contadorcuantos - 1
                    Case 5
                        Me.Button8.Visible = True
                        Me.Button8.Enabled = False
                        Me.Button8.Text = "&Telefonia"
                        Me.Panel2.Visible = False
                        muestra_telefonia()
                        Loc_HAB_Telefonia = 2
                        Contadorcuantos = Contadorcuantos - 1
                End Select
            Else
                '-Solo Internet
                Me.Button8.Visible = True
                Me.Button8.Enabled = False
                Me.Button8.Text = "&Internet"
                'Me.Button7.Visible = False
                'Me.Panel2.Visible = True
                'Me.Panel2.Visible = False
                'Me.Panel4.Visible = True
                Me.SplitContainer1.Enabled = True
                'Me.Button11.Visible = False
                'Me.SoloInternetCheckBox.Enabled = False
                'frmctr.MdiParent = Me
                'frmInternet2.MdiParent = Me
                'frmInternet2.Show()
                'frmctr.Show()
                BndEsInternet = True
                frmctr.MdiParent = Me
                frmInternet2.MdiParent = Me
                frmctr.WindowState = FormWindowState.Normal
                frmInternet2.Show()
                frmctr.Show()
                frmctr.Boton_Internet()
                frmctr.TreeView1.ExpandAll()
                'frmInternet2.Hide()
                'frmctr.Hide()                    
                Loc_HAB_INTERNET = 2
                Contadorcuantos = Contadorcuantos - 1
                If (IdSistema <> "LO" And IdSistema <> "YU") Then Exit Sub
            End If
            For I As Integer = 1 To Contadorcuantos
                If Loc_HAB_TV = 0 Then
                    If I = 1 Then
                        Me.Button7.Visible = True
                        Me.Button7.Enabled = True
                        Me.Button7.Text = "&Televisión"
                    ElseIf I = 2 Then
                        Me.Button11.Visible = True
                        Me.Button11.Enabled = True
                        Me.Button11.Text = "&Televisión"
                    ElseIf I = 3 Then
                        Me.Button28.Visible = True
                        Me.Button28.Enabled = True
                        Me.Button28.Text = "&Televisión"
                    End If
                    'Me.Panel2.Visible = True
                    'Me.Panel7.Visible = False
                    'Me.Panel4.Visible = False
                    'Me.SoloInternetCheckBox.Enabled = False
                    Loc_HAB_TV = 2
                ElseIf Loc_HAB_INTERNET = 0 Then
                    If I = 1 Then
                        Me.Button7.Visible = True
                        Me.Button7.Enabled = True
                        Me.Button7.Text = "&Internet"
                    ElseIf I = 2 Then
                        Me.Button11.Visible = True
                        Me.Button11.Enabled = True
                        Me.Button11.Text = "&Internet"
                    ElseIf I = 3 Then
                        Me.Button28.Visible = True
                        Me.Button28.Enabled = True
                        Me.Button28.Text = "&Internet"
                    End If
                    'Me.Button7.Visible = False
                    'Me.Panel2.Visible = True
                    'Me.Panel2.Visible = False
                    'Me.Panel4.Visible = True
                    'Me.SplitContainer1.Enabled = True
                    'Me.Button11.Visible = False
                    'Me.SoloInternetCheckBox.Enabled = False
                    'frmctr.MdiParent = Me
                    'frmInternet2.MdiParent = Me
                    'frmInternet2.Show()
                    'frmctr.Show()
                    'BndEsInternet = True
                    'frmctr.MdiParent = Me
                    'frmInternet2.MdiParent = Me
                    'frmctr.WindowState = FormWindowState.Normal
                    'frmInternet2.Show()
                    'frmctr.Show()
                    'frmctr.Boton_Internet()
                    'frmctr.TreeView1.ExpandAll()
                    'frmInternet2.Hide()
                    'frmctr.Hide()                    
                    Loc_HAB_INTERNET = 2
                ElseIf Loc_HAB_TVDIG = 0 Then
                    If (IdSistema = "SA" Or IdSistema = "VA") And BndMini = 1 Then
                        ''PAra Vallarta y Sahuayo si es MiniBasico no se hablita los Servicios Premium
                        Loc_HAB_TVDIG = 2
                    Else
                        If I = 1 Then
                            Me.Button7.Visible = True
                            Me.Button7.Enabled = True
                            If IdSistema = "SA" Or IdSistema = "VA" Then
                                Me.Button7.Text = "&Premium"
                            Else
                                Me.Button7.Text = "IPTV"
                            End If
                            'ElseIf I = 2 Then
                            '    Me.Button11.Visible = True
                            '    Me.Button11.Enabled = True
                            '    If IdSistema = "SA" Or IdSistema = "VA" Then
                            '        Me.Button11.Text = "&Premium"
                            '    Else
                            '        Me.Button11.Text = "IPTV"
                            '    End If
                            'ElseIf I = 3 Then
                            '    Me.Button28.Visible = True
                            '    Me.Button28.Enabled = True
                            '    If IdSistema = "SA" Or IdSistema = "VA" Then
                            '        Me.Button28.Text = "&Premium"
                            '    Else
                            '        Me.Button28.Text = "IPTV"
                            '    End If
                        End If
                        'Me.Panel7.Visible = True
                        'Me.Panel4.Visible = False
                        'Me.SoloInternetCheckBox.Enabled = False
                        'If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
                        '    Contrato = 0
                        'Else
                        '    Contrato = Me.CONTRATOTextBox.Text
                        'End If
                        ''frmctr.MdiParent = Me
                        ''frmInternet2.MdiParent = Me
                        '''frmInternet2.Show()
                        '''frmctr.Show()
                        ''frmInternet2.Hide()
                        Loc_HAB_TVDIG = 2
                    End If
                ElseIf Loc_HAB_Telefonia = 0 Then
                    If I = 1 Then
                        Me.Button7.Visible = True
                        Me.Button7.Enabled = True
                        Me.Button7.Text = "&Telefonia"
                    ElseIf I = 2 Then
                        Me.Button11.Visible = True
                        Me.Button11.Enabled = True
                        Me.Button11.Text = "&Telefonia"
                    ElseIf I = 3 Then
                        Me.Button28.Visible = True
                        Me.Button28.Enabled = True
                        Me.Button28.Text = "&Telefonia"
                    End If
                    'Me.Panel2.Visible = False
                    'muestra_telefonia()
                    Loc_HAB_Telefonia = 2
                End If
            Next
        End If

        If (Button11.Text = Button7.Text) Or (Button11.Text = Button8.Text) Then
            Button11.Visible = False
        End If
        'If GloHabilitadosNET = False Then
        '    Me.Button11.Visible = False
        '    SoloInternetCheckBox.Visible = False
        'End If

        'Panel16.Visible = False
        'Button33.Visible = False

    End Sub

    '02/Mayo/2009 Private Sub HABILITA_CTRS()
    '    ''PROCEDIMIENTO TIPOS DE SERVICIOS 
    '    'Dim CONq As New SqlConnection(MiConexion)
    '    'CONq.Open()
    '    Dim cont, conttv As Integer
    '    'Me.DAMESTATUSHABTableAdapter.Connection = CONq
    '    'Me.DAMESTATUSHABTableAdapter.Fill(Me.DataSetLidia.DAMESTATUSHAB, 1, HAB_TV)
    '    'Me.DAMESTATUSHABTableAdapter.Connection = CONq
    '    'Me.DAMESTATUSHABTableAdapter.Fill(Me.DataSetLidia.DAMESTATUSHAB, 2, HAB_INTERNET)
    '    'Me.DAMESTATUSHABTableAdapter.Connection = CONq
    '    'Me.DAMESTATUSHABTableAdapter.Fill(Me.DataSetLidia.DAMESTATUSHAB, 3, HAB_TVDIG)
    '    'Me.DAMESTATUSHABTableAdapter.Connection = CONq
    '    'Me.DAMESTATUSHABTableAdapter.Fill(Me.DataSetLidia.DAMESTATUSHAB, 5, HAB_Telefonia)
    '    'CONq.Close()
    '    For I As Integer = 1 To 5
    '        Select Case I
    '            Case 1 'SI TENGO fON QUE HAGO
    '                If HAB_TV = 0 And Me.SoloInternetCheckBox.Checked = False Then
    '                    Me.Button8.Visible = True
    '                    Me.Button7.Visible = False
    '                    Me.Button11.Visible = False
    '                    Me.Button8.Text = "&Televisión"
    '                    Me.Panel2.Visible = True
    '                    Me.Panel7.Visible = False
    '                    Me.Panel4.Visible = False
    '                    Me.SoloInternetCheckBox.Enabled = False
    '                ElseIf HAB_TVDIG = 0 And Me.SoloInternetCheckBox.Checked = False Then
    '                    Me.Button8.Visible = True
    '                    Me.Button7.Visible = False
    '                    Me.Button11.Visible = False
    '                    If IdSistema = "SA" Or IdSistema = "VA" Then
    '                        Me.Button8.Text = "&Premium"
    '                    Else
    '                        Me.Button8.Text = "&Tv Digital"
    '                    End If
    '                    Me.Panel7.Visible = True
    '                    Me.Panel4.Visible = False
    '                    Me.SoloInternetCheckBox.Enabled = False
    '                    If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '                        Contrato = 0
    '                    Else
    '                        Contrato = Me.CONTRATOTextBox.Text
    '                    End If
    '                    frmctr.MdiParent = Me
    '                    frmInternet2.MdiParent = Me
    '                    'frmInternet2.Show()
    '                    'frmctr.Show()
    '                    frmInternet2.Hide()

    '                    frmctr.Hide()
    '                ElseIf HAB_INTERNET = 0 And (Me.Button8.Text <> "&Tv Digital" And Me.Button8.Text <> "&Premium") Then
    '                    Me.Button8.Visible = True
    '                    Me.Button8.Text = "&Internet"
    '                    Me.Button7.Visible = False
    '                    Me.Panel2.Visible = False
    '                    'Me.Panel4.Visible = True
    '                    Me.SplitContainer1.Enabled = True
    '                    Me.Button11.Visible = False
    '                    Me.SoloInternetCheckBox.Enabled = False
    '                    If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '                        Contrato = 0
    '                    Else
    '                        Contrato = Me.CONTRATOTextBox.Text
    '                    End If
    '                    'frmctr.MdiParent = Me
    '                    'frmInternet2.MdiParent = Me
    '                    'frmInternet2.Show()
    '                    'frmctr.Show()
    '                    BndEsInternet = True
    '                    frmctr.MdiParent = Me
    '                    frmInternet2.MdiParent = Me
    '                    frmctr.WindowState = FormWindowState.Normal
    '                    frmInternet2.Show()
    '                    frmctr.Show()
    '                    frmctr.Boton_Internet()
    '                    frmctr.TreeView1.ExpandAll()
    '                    'frmInternet2.Hide()
    '                    'frmctr.Hide()
    '                    If Me.SoloInternetCheckBox.Checked = True Then
    '                        Exit For
    '                    End If
    '                    'frmctr.TreeView1.ExpandAll()
    '                End If
    '            Case 2 'SI TENGO INTERNET QUE HAGO
    '                If HAB_TVDIG = 0 And Me.Button8.Text = "&Televisión" Then
    '                    Me.Button7.Visible = True
    '                    Me.Button11.Visible = False
    '                    If IdSistema = "SA" Or IdSistema = "VA" Then
    '                        Me.Button7.Text = "&Premium"
    '                    Else
    '                        Me.Button7.Text = "&Tv Digital"
    '                    End If
    '                    If IdSistema = "SA" Or IdSistema = "VA" Then
    '                        If BndMini = 0 Then Button8.Enabled = True
    '                        'Button11.Enabled = True
    '                        If OpcionCli <> "N" Then
    '                            If BndMini = 0 Then Me.Button7.Enabled = True
    '                            'Else
    '                            Button11.Enabled = True
    '                        End If
    '                    Else
    '                        Me.Button7.Enabled = True
    '                    End If
    '                    If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '                        Contrato = 0
    '                    Else
    '                        Contrato = Me.CONTRATOTextBox.Text
    '                    End If
    '                    '    frmctr.MdiParent = Me
    '                    '    frmInternet2.MdiParent = Me
    '                    '    frmInternet2.Show()
    '                    '    frmctr.Show()
    '                    '    frmInternet2.Hide()
    '                    'frmctr.Hide()
    '                    BndEsInternet = True
    '                    frmctr.MdiParent = Me
    '                    frmInternet2.MdiParent = Me
    '                    frmctr.WindowState = FormWindowState.Normal
    '                    frmInternet2.Show()
    '                    frmctr.Show()
    '                    frmctr.Boton_Internet()
    '                    frmctr.TreeView1.ExpandAll()
    '                ElseIf HAB_INTERNET = 0 And (Me.Button8.Text = "&Tv Digital" Or Me.Button8.Text = "&Televisión" Or Me.Button8.Text = "&Premium") Then
    '                    Me.Button7.Visible = True
    '                    Me.Button11.Visible = False
    '                    Me.Button7.Text = "&Internet"
    '                    Dim CONx As New SqlConnection(MiConexion)
    '                    CONx.Open()
    '                    Me.CONTARCLIENTESTableAdapter.Connection = CONx
    '                    Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, Contrato, 3, cont)
    '                    Me.CONTARCLIENTESTableAdapter.Connection = CONx
    '                    Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 2, conttv)
    '                    CONx.Close()
    '                    If cont > 0 Or conttv > 0 Then
    '                        Me.Button7.Enabled = True
    '                    End If
    '                    Me.SoloInternetCheckBox.Enabled = True
    '                    If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '                        Contrato = 0
    '                    Else
    '                        Contrato = Me.CONTRATOTextBox.Text
    '                    End If
    '                    '    frmctr.MdiParent = Me
    '                    '    frmInternet2.MdiParent = Me
    '                    '    frmInternet2.Show()
    '                    '    frmctr.Show()
    '                    '    frmInternet2.Hide()
    '                    'frmctr.Hide()
    '                    BndEsInternet = True
    '                    frmctr.MdiParent = Me
    '                    frmInternet2.MdiParent = Me
    '                    frmctr.WindowState = FormWindowState.Normal
    '                    frmInternet2.Show()
    '                    frmctr.Show()
    '                    frmctr.Boton_Internet()
    '                    frmctr.TreeView1.ExpandAll()
    '                End If
    '            Case 3 ' SI TENGO PREMIUM O DIGITAL
    '                If HAB_INTERNET = 0 And (Me.Button7.Text = "&Tv Digital" Or Me.Button7.Text = "&Premium") Then
    '                    Me.Button11.Visible = True
    '                    Me.Button11.Text = "&Internet"
    '                    Me.SoloInternetCheckBox.Enabled = True

    '                End If
    '                If HAB_INTERNET = 0 And Me.Button8.Text = "&Internet" Then
    '                    Me.SoloInternetCheckBox.Enabled = False
    '                    Me.Panel2.Hide()
    '                    Me.Panel4.Hide()
    '                    Me.Panel7.Hide()
    '                    If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '                        Contrato = 0
    '                    Else
    '                        Contrato = Me.CONTRATOTextBox.Text
    '                    End If
    '                    '    frmctr.MdiParent = Me
    '                    '    frmInternet2.MdiParent = Me
    '                    '    frmInternet2.Show()
    '                    '    frmctr.Show()
    '                    '    frmctr.Boton_Internet()
    '                    'frmctr.TreeView1.ExpandAll()
    '                    BndEsInternet = True
    '                    frmctr.MdiParent = Me
    '                    frmInternet2.MdiParent = Me
    '                    frmctr.WindowState = FormWindowState.Normal
    '                    frmInternet2.Show()
    '                    frmctr.Show()
    '                    frmctr.Boton_Internet()
    '                    frmctr.TreeView1.ExpandAll()
    '                End If
    '            Case 5
    '                If HAB_Telefonia = 0 Then
    '                    Me.Button28.Visible = True
    '                    Me.Button28.Enabled = False
    '                    Me.Button8.Enabled = False
    '                    Me.Button28.Enabled = True
    '                End If
    '        End Select
    '    Next
    '    'If Me.Button28.Visible = True And IdSistema = "LO" Then
    '    '    Me.Button8.Enabled = False
    '    '    Me.Button28.Enabled = True
    '    'End If
    'End Sub


    Private Sub bloqueado1(ByRef contrato As Integer)
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        cone.Open()
        NUM = 0
        num2 = 0
        Me.BuscaBloqueadoTableAdapter.Connection = cone
        Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, contrato, NUM, num2)
        cone.Close()
        If num2 = 1 Then
            eGloContrato = contrato
            FrmBloqueo.Show()
            ' MsgBox("El Cliente " + Me.ContratoTextBox.Text + " Ha Sido Bloqueado por lo que no se Podrá Llevar a cabo la Orden ", MsgBoxStyle.Exclamation)
            Me.Panel1.Enabled = False
            Me.Panel3.Enabled = False
            Me.Panel4.Enabled = False
            Me.Panel6.Enabled = False
            Me.Panel7.Enabled = False
            Me.Panel8.Enabled = False
        End If
    End Sub
    Private Sub PintarFondo(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs)

        Dim GradientePanel As New LinearGradientBrush(New RectangleF(0, 0, ctlMDI.Width, ctlMDI.Height), Color.WhiteSmoke, Color.WhiteSmoke, LinearGradientMode.Vertical)

        e.Graphics.FillRectangle(GradientePanel, New RectangleF(0, 0, ctlMDI.Width, ctlMDI.Height))

    End Sub

    Private Sub FrmClientes_Invalidated(ByVal sender As Object, ByVal e As System.Windows.Forms.InvalidateEventArgs) Handles Me.Invalidated

    End Sub


    Private Sub FrmClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'MsgBox(Today.ToString)

        CheckBoxEsProspecto.Checked = EsProspecto

        If CheckBoxEsProspecto.Checked Then
            Button21.Visible = False
            btnEdoCuenta.Visible = False
            Button1.Visible = False
            Button20.Visible = False
            Button6.Visible = False
            Button4.Visible = False
            Button20.Visible = False
            Me.Text = "Prospecto"
        End If


        locLoad = False
        'SoloInternetCheckBox.Enabled = False
        TVCONPAGONumericUpDown.Minimum = 0
        TVCONPAGONumericUpDown.Maximum = GloTvConpago
        Me.TVSINPAGONumericUpDown.Minimum = 0
        Me.TVSINPAGONumericUpDown.Maximum = GloTvSinPago
        Digital = True
        frmnsb = New FrmNombresSeparadosBuro()
        Asigna_taps()
        ColoreaFrm(Me)
        Dim ctl As Control


        For Each ctl In Me.Controls

            Try

                ctlMDI = CType(ctl, MdiClient)

                ' Asignamos el color de fondo
                ctlMDI.BackColor = Color.AntiqueWhite

                'Aquí asignamos el manejador para pintar el fondo con degradados o lo que
                'queramos. Si solo queremos cambiar el color de fondo no hace falta, ni las funciones siguientes tampoco
                AddHandler ctlMDI.Paint, AddressOf PintarFondo

            Catch ex As InvalidCastException

            End Try

        Next



        frmctr = New FrmCtrl_ServiciosCli
        'frmTelefonia = New FrmClientesTel
        frmInternet2 = New FrmInternet
        Frm1 = New FrmOtrosAparatos
        Frm1.BackColor = Me.BackColor

        Try
            De_Internet = False
            'frmctr.MdiParent = Me
            'frmInternet2.MdiParent = Me
            'frmctr.WindowState = FormWindowState.Normal
            'frmInternet2.Show()
            'frmInternet2.Hide()
            'frmctr.Show()
            'frmctr.Hide()
            'Me.Button28.Visible = False
            Me.Button30.Visible = False
            If IdSistema = "AG" Or IdSistema = "VA" Then
                If IdSistema = "VA" Then
                    Me.Panel11.Visible = True
                End If
                Me.Button29.Visible = True


                frmInternet2.NumericUpDown1.Enabled = False
            ElseIf IdSistema <> "AG" And IdSistema <> "SA" Then
                Me.Panel11.Visible = True
            End If

            If IdSistema = "SA" Then
                Me.Button29.Visible = False
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then

                Me.Button30.Visible = True
                Me.Panel13.Visible = True
                'Me.Button28.Visible = True
                Me.DESGLOSA_IvaCheckBox.Visible = False
                Me.SoloInternetCheckBox.Visible = False
                'Me.Panel2.Visible = True
                'Me.SoloInternetCheckBox.Checked = True
            End If



            Dim CON As New SqlConnection(MiConexion)

            'Banderas que se usan para cuando un contrato tiene el Mismo Nombre ó el Mismo Domicilio
            'La bandera apagada indica que si guardará, en caso cotrario si la bandera está prendida no lo hará
            eBndMismoNombre = False
            If Locbnd2clientes = True Then
                Locbnd2clientes = False
            End If
            If bnddir = True Then
                bnddir = False
            End If

            'Llenar variables para la bitacora del sistema
            'dame_datos()
            'ésta Linea la puso Eric, cualquier cosa, ya sabe...
            BndMini = 0
            eEntraUM = True
            eEntraUMB = False
            CON.Open()
            Me.DAMEFECHADELSERVIDOR_2TableAdapter.Connection = CON
            Me.DAMEFECHADELSERVIDOR_2TableAdapter.Fill(Me.DataSetEdgarRev2.DAMEFECHADELSERVIDOR_2)
            'TODO: esta línea de código carga datos en la tabla 'DataSetEDGAR.MuestraPromotoresTv' Puede moverla o quitarla según sea necesario.
            Me.MuestraPromotoresTvTableAdapter.Connection = CON
            Me.MuestraPromotoresTvTableAdapter.Fill(Me.DataSetEDGAR.MuestraPromotoresTv)
            'TODO: esta línea de código carga datos en la tabla 'DataSetEDGAR.MuestraPromotoresNet' Puede moverla o quitarla según sea necesario.
            Me.MuestraPromotoresNetTableAdapter.Connection = CON
            Me.MuestraPromotoresNetTableAdapter.Fill(Me.DataSetEDGAR.MuestraPromotoresNet)
            CON.Close()
            'TODO: esta línea de código carga datos en la tabla 'DataSetLidia.DameFechaHabilitar' Puede moverla o quitarla según sea necesario.
            'Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar)
            eGuardarCliente = False

            Dim CONT, CONTV As Integer
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRA_A_DIGITAL' Puede moverla o quitarla según sea necesario.
            CON.Open()

            If IdSistema <> "VA" Then
                Me.ComboBox11.Enabled = False
            ElseIf IdSistema = "VA" Then
                Me.MUESTRATABSTableAdapter.Connection = CON
                Me.MUESTRATABSTableAdapter.Fill(Me.DataSetarnoldo.MUESTRATABS, 0)
                Me.CMBLabel10.Text = "Cajas Digitales Asignadas al Cliente:"
                Me.CMBLabel32.Text = "Seleccione la Caja Digital que le va a Asignar el Paquete"
                Me.CMBLabel53.Text = "Status Caja"
                Me.CMBLabel54.Text = "Datos de la Caja Digital"
                Me.ToolStripButton16.Text = "Agregar Caja Digital"
                Me.ToolStripButton15.Text = "Quitar Caja Digital"
            End If

            DameListaTipoIdentificacion()

            Me.MUESTRACatalogoPeriodosCorteTableAdapter.Connection = CON
            Me.MUESTRACatalogoPeriodosCorteTableAdapter.Fill(Me.DataSetEDGAR.MUESTRACatalogoPeriodosCorte, 0)
            Me.MUESTRA_TIPOCLIENTESTableAdapter.Connection = CON
            Me.MUESTRA_TIPOCLIENTESTableAdapter.Fill(Me.DataSetEDGAR.MUESTRA_TIPOCLIENTES, 0)
            locLoad = False
            Me.MUESTRA_A_DIGITALTableAdapter.Connection = CON
            Me.MUESTRA_A_DIGITALTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRA_A_DIGITAL)
            Pantalla = Me.Name
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipoPromocion' Puede moverla o quitarla según sea necesario.
            'Me.MuestraTipoPromocionTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipoPromocion)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraPromotores' Puede moverla o quitarla según sea necesario.
            Me.MuestraPromotoresTableAdapter.Connection = CON
            Me.MuestraPromotoresTableAdapter.Fill(Me.NewSofTvDataSet.MuestraPromotores)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.StatusCableModem' Puede moverla o quitarla según sea necesario.
            Me.StatusCableModemTableAdapter.Connection = CON
            Me.StatusCableModemTableAdapter.Fill(Me.NewSofTvDataSet.StatusCableModem)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.TipoCablemodem' Puede moverla o quitarla según sea necesario.
            Me.TipoCablemodemTableAdapter.Connection = CON
            Me.TipoCablemodemTableAdapter.Fill(Me.NewSofTvDataSet.TipoCablemodem)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerInternet' Puede moverla o quitarla según sea necesario.
            Me.MuestraTipSerInternetTableAdapter.Connection = CON
            Me.MuestraTipSerInternetTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerInternet)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.StatusNet' Puede moverla o quitarla según sea necesario.
            Me.StatusNetTableAdapter.Connection = CON
            Me.StatusNetTableAdapter.Fill(Me.NewSofTvDataSet.StatusNet)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.TiposAparatos' Puede moverla o quitarla según sea necesario.
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.StatusBasico' Puede moverla o quitarla según sea necesario.
            Me.StatusBasicoTableAdapter.Connection = CON
            Me.StatusBasicoTableAdapter.Fill(Me.NewSofTvDataSet.StatusBasico)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraMotivoCancelacion' Puede moverla o quitarla según sea necesario.
            Me.MuestraMotivoCancelacionTableAdapter.Connection = CON
            Me.MuestraMotivoCancelacionTableAdapter.Fill(Me.NewSofTvDataSet.MuestraMotivoCancelacion)

            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTiposServicioTv' Puede moverla o quitarla según sea necesario.
            'Me.MuestraTiposServicioTvTableAdapter.Connection = CON
            'Me.MuestraTiposServicioTvTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTiposServicioTv)
            MuestraTiposServicioTvBase2(GloIdCompania)

            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla según sea necesario.
            'Me.MuestraServiciosTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios, 3)

            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.CIUDADES' Puede moverla o quitarla según sea necesario.
            'Lo comentó jano
            'Me.CIUDADESTableAdapter.Connection = CON
            'Me.CIUDADESTableAdapter.Fill(Me.NewSofTvDataSet.CIUDADES)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.CALLES' Puede moverla o quitarla según sea necesario.
            'Me.MUESTRACALLESTableAdapter.Connection = CON
            'Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)





            CON.Close()
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            'CALLEComboBox.SelectedItem = 1
            'Me.Clv_CalleTextBox.Text = Me.CALLEComboBox.SelectedValue
            'Me.DAMECOLONIA_CALLETableAdapter.Connection = CON2
            'Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.CALLEComboBox.SelectedValue, Integer)))
            CON2.Close()


            ' Me.Clv_ColoniaTextBox.Text = Me.COLONIAComboBox.SelectedValue
            'Me.Clv_CiudadTextBox.Text = Me.CIUDADComboBox.SelectedValue
            Me.MOTCANComboBox.Text = ""
            Me.MOTCANComboBox.SelectedValue = 0
            Me.Bucacontrato(Contrato)
            Me.Panel7.Enabled = True
            Me.Panel2.Enabled = True
            Me.Panel4.Enabled = True
            Me.SplitContainer1.Enabled = True
            Me.SplitContainer2.Enabled = True
            Me.BindingNavigator5.Enabled = True
            Me.BindingNavigator6.Enabled = True
            Me.Button9.Enabled = True
            Me.HABILITA_CTRS()

            'Valida la licencia, si llego al limite y esta en baja, no se puede modificar
            If OpcionCli = "M" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.Int, Contrato)
                BaseII.CreateMyParameter("@aux", ParameterDirection.Output, SqlDbType.Int)
                BaseII.ProcedimientoOutPut("Sp_ValidarCambioPorLicencia")
                If (BaseII.dicoPar("@aux").ToString() = 0) Then
                    If Int(ValidaLicencia()) = 0 Then
                        OpcionCli = "C"
                        MsgBox("El número de contratos ha llegado al limite de la Licencia, por lo que no se podrá modificar el cliente porque sus servicios se encuentran dados de baja. Contacte al proveedor.")
                    End If
                End If
            End If


            If OpcionCli = "N" Then
                'Llena_companias_nuevo()
                ConClientesDigDec(0)
                'CALLEComboBox.SelectedValue = 0
                'CALLEComboBox.SelectedIndex = 1
                SoloInternetCheckBox.Enabled = False
                ComboBoxCompanias.Enabled = True
                CON.Open()
                btnEdoCuenta.Enabled = False
                ComboBoxCompanias.SelectedValue = GloIdCompania
                'Me.CONSULTARCLIENTETableAdapter.Connection = CON
                Me.SoloInternetCheckBox.Tag = "N"
                Me.CONSULTARCLIENTEBindingSource.AddNew()
                Me.CONSULTARCLIENTEBindingNavigator.Enabled = True
                Me.Panel1.Enabled = True
                Me.SoloInternetCheckBox.Enabled = True
                Me.SoloInternetCheckBox.Checked = False
                Me.SoloInternetCheckBox.Tag = ""
                Me.EshotelCheckBox.Checked = False
                Me.DESGLOSA_IvaCheckBox.Checked = False
                Me.Button16.Enabled = False
                Me.Button29.Enabled = True
                'Por default Enabled=False
                'If IdSistema = "SA" Then
                Me.TVSINPAGONumericUpDown.Enabled = True
                Me.TVCONPAGONumericUpDown.Enabled = True
                'Else
                '    Me.TVSINPAGONumericUpDown.Enabled = False
                '    Me.TVCONPAGONumericUpDown.Enabled = False
                'End If
                Me.Button20.Visible = True
                Me.Button20.Text = "Bloqueo de Cliente"
                glopar = "N"
                'Eric
                Me.Button21.Enabled = False
                CON.Close()
                'En Cuernava todos Son de solo Internet
                'If IdSistema = "LO" Then
                ''  Me.SoloInternetCheckBox.Checked = True
                'End If
                Button8.Visible = True
                Button11.Visible = True
                Llena_companias_nuevo()
                LlenaEstados()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@IdCompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                BaseII.CreateMyParameter("@permiteCapturarNuevo", ParameterDirection.Output, SqlDbType.Bit)
                BaseII.ProcedimientoOutPut("sp_permiteCapturarCalles")
                CapturaCalle = BaseII.dicoPar("@permiteCapturarNuevo")
            ElseIf OpcionCli = "C" Then
                SoloInternetCheckBox.Enabled = False
                ComboBoxCompanias.Enabled = False
                Me.CONSULTARCLIENTEBindingNavigator.Enabled = False
                Me.Panel1.Enabled = False
                'Me.Panel7.Enabled = False
                Me.Panel2.Enabled = False
                'Me.Panel4.Enabled = False
                'Llena_companias_contrato()
                Llena_companias_nuevo()
                LlenaEstados()
                Me.SplitContainer1.Panel2.Enabled = False
                Me.SplitContainer1.Panel1.Enabled = True
                Me.SplitContainer2.Panel2.Enabled = False
                Me.SplitContainer2.Panel1.Enabled = True
                Me.Button9.Enabled = False
                Me.Button19.Enabled = False
                Me.BindingNavigator5.Enabled = False
                Me.BindingNavigator6.Enabled = False

                'Eric
                If IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "VA" Then
                    CON.Open()
                    Me.ConRelCtePlacaTableAdapter.Connection = CON
                    Me.ConRelCtePlacaTableAdapter.Fill(Me.DataSetEric.ConRelCtePlaca, Me.CONTRATOTextBox.Text)
                    Me.Label32.Visible = True
                    Me.PlacaTextBox.Visible = True
                    Me.Label32.Visible = True
                    Me.Button21.Visible = True
                    CON.Close()
                ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                    BuscaCuenta()
                End If

                buscarelaccion()
                'ALMACENA EL TAP Y PLACA DEL CLIENTE (INICIO) *JUANJO*
                uspDameTapCliente(CLng(Me.CONTRATOTextBox.Text))
                'ALMACENA EL TAP Y PLACA DEL CLIENTE (FIN) *JUANJO*

                SP_ConsultaRel_Contratos_Companias(Me.CONTRATOTextBox.Text)
                Button8.Visible = True
                Button11.Visible = True
                DameClaveElector()
            ElseIf OpcionCli = "M" Then
                SoloInternetCheckBox.Enabled = True
                ComboBoxCompanias.Enabled = False
                'Llena_companias()
                CON.Open()
                Me.CONSULTARCLIENTEBindingNavigator.Enabled = True
                Me.CONTARCLIENTESTableAdapter.Connection = CON
                Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, Contrato, 6, CONT)
                Me.CONTARCLIENTESTableAdapter.Connection = CON
                Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 7, CONTV)
                CON.Close()
                'Llena_companias_contrato()
                Llena_companias_nuevo()
                LlenaEstados()

                'Las Siguientes 5 Líneas las puso Eric. Cualquier cosa, sobrees de él!

                If Me.ComboBox1.Text.Length > 0 Then
                    Me.Button16.Enabled = True
                Else
                    Me.Button16.Enabled = False
                End If
                'CON.Open()
                'Me.Valida_SiahiOrdSerTableAdapter.Connection = CON
                'Me.Valida_SiahiOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.Valida_SiahiOrdSer, New System.Nullable(Of Long)(CType(Contrato, Long)))
                'Me.Valida_SiahiQuejasTableAdapter.Connection = CON
                'Me.Valida_SiahiQuejasTableAdapter.Fill(Me.NewSofTvDataSet.Valida_SiahiQuejas, New System.Nullable(Of Long)(CType(Contrato, Long)))
                'CON.Close()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@Cont", ParameterDirection.Output, SqlDbType.Int)
                BaseII.ProcedimientoOutPut("Valida_SiahiOrdSerOutput")
                ValidaTextBox.Text = BaseII.dicoPar("@Cont").ToString()


                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@Cont", ParameterDirection.Output, SqlDbType.Int)
                BaseII.ProcedimientoOutPut("Valida_SiahiQuejasOutput")
                ValidaTextBox1.Text = BaseII.dicoPar("@Cont").ToString()

                'Eric
                If IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "VA" Then
                    CON.Open()
                    Me.ConRelCtePlacaTableAdapter.Connection = CON
                    Me.ConRelCtePlacaTableAdapter.Fill(Me.DataSetEric.ConRelCtePlaca, Me.CONTRATOTextBox.Text)
                    Me.Label32.Visible = True
                    Me.PlacaTextBox.Visible = True
                    Me.Label32.Visible = True
                    Me.Button21.Visible = True
                    CON.Close()
                ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                    BuscaCuenta()
                End If
                If GloTipoUsuario = 40 Then
                    'Digital
                    Me.Label41.ReadOnly = False
                    Me.MACCABLEMODEMLabel1.ReadOnly = False
                    Me.TextBox16.ReadOnly = False
                    Me.TextBox15.ReadOnly = False
                    Me.TextBox14.ReadOnly = False
                    Me.TextBox13.ReadOnly = False
                    Me.TextBox12.ReadOnly = False
                    'Me.TextBox10.ReadOnly = False
                    Me.TextBox3.ReadOnly = False
                    Me.TextBox9.ReadOnly = False
                    Me.ComboBox10.Enabled = True
                    Me.TextBox23.ReadOnly = False
                    Me.TextBox32.ReadOnly = False
                    Me.TextBox33.ReadOnly = False
                    Me.ComboBox13.Enabled = True
                    'Ésta lineas la puso Eric
                    Me.Button17.Enabled = False
                    Me.CheckBox1.Enabled = True
                    Me.CheckBox1.Enabled = True
                    Me.TextBox11.ReadOnly = False
                    Me.TextBox3.ReadOnly = False
                    Me.ComboBox9.Enabled = True
                    '-------------------------
                    'INTERNET
                    frmInternet2.Fecha_solicitudTextBox1.ReadOnly = False
                    frmInternet2.Fecha_suspensionTextBox.ReadOnly = False
                    frmInternet2.Fecha_SuspencionTextBox.ReadOnly = False
                    frmInternet2.Fecha_TraspasoTextBox.ReadOnly = False
                    frmInternet2.Fecha_Fuera_AreaTextBox.ReadOnly = False
                    frmInternet2.Fecha_BajaTextBox1.ReadOnly = False
                    frmInternet2.Fecha_bajaTextBox.ReadOnly = False
                    frmInternet2.Fecha_ActivacionTextBox.ReadOnly = False
                    frmInternet2.Fecha_instalacioTextBox.ReadOnly = False
                    frmInternet2.PrimerMensualidadCheckBox.Enabled = True
                    frmInternet2.CortesiaCheckBox2.Enabled = True
                    'frmTelefonia.CortesiaCheckBox.Enabled = True

                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.

                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    frmInternet2.Ultimo_mesTextBox1.ReadOnly = False
                    frmInternet2.TextBox5.ReadOnly = False
                    frmInternet2.Ultimo_anioTextBox1.ReadOnly = False
                    frmInternet2.ComboBox5.Enabled = True
                    frmInternet2.ComboBox2.Enabled = True
                    frmInternet2.FECHA_ULT_PAGOTextBox1.ReadOnly = False
                    frmInternet2.TextBox5.ReadOnly = False
                    frmInternet2.ComboBox6.Enabled = True
                    frmInternet2.Button18.Enabled = True  'Ésta linea la puso Eric
                    frmInternet2.PrimerMensualidadCheckBox.Enabled = True
                    '------------------------
                    'Tv
                    Me.ComboBox16.Enabled = True
                    Me.TextBox2.ReadOnly = False
                    Me.FECHA_ULT_PAGOTextBox.ReadOnly = False
                    Me.PRIMERMENSUALIDACheckBox.Enabled = True
                    Me.ComboBox1.Enabled = True
                    Me.ComboBox18.Enabled = True
                    'Me.ULTIMO_MESTextBox.ReadOnly = False
                    Me.TextBox2.ReadOnly = False
                    Me.ULTIMO_ANIOTextBox.ReadOnly = False
                    Me.FECHA_SOLICITUDTextBox.ReadOnly = False
                    Me.FECHA_INSTTextBox.ReadOnly = False
                    Me.FECHA_CORTETextBox.ReadOnly = False
                    Me.FECHACANCOUTAREATextBox.ReadOnly = False
                    Me.FECHA_CANCELACIOTextBox.ReadOnly = False
                    Me.MOTCANComboBox.Enabled = True
                    Me.TipSerTvComboBox.Enabled = True
                    'Éstas Líneas las puso Eric
                    Me.TVCONPAGONumericUpDown.Enabled = True
                    Me.TVSINPAGONumericUpDown.Enabled = True
                    Me.Button16.Enabled = False
                    Me.CortesiaCheckBox1.Enabled = True
                    '--------------------------
                End If
                If GloTipoUsuario = 21 Then
                    'Me.btnCapturarNombre.Enabled = False
                    'Me.CALLEComboBox.Enabled = False
                    'Me.NUMEROTextBox.Enabled = False
                    'Me.TxtNumeroInt.Enabled = False
                    ''Me.ENTRECALLESTextBox.Enabled = False
                    'Me.COLONIAComboBox.Enabled = False
                    'Me.CIUDADComboBox.Enabled = False
                    'Me.TELEFONOTextBox.Enabled = False
                    'Me.CELULARTextBox.Enabled = False
                    'Me.EmailTextBox.Enabled = False
                    ''Me.txtPlaca.Enabled = False
                    CodigoPostalTextBox.Enabled = True
                End If

                If CONT = 0 And CONTV = 0 Then
                    Me.SoloInternetCheckBox.Enabled = True
                ElseIf CONT > 0 And CONTV > 0 Then
                    'Me.SoloInternetCheckBox.Enabled = False
                End If
                If CONTV = 0 Then
                    Me.SoloInternetCheckBox.Enabled = True
                Else
                    'Me.SoloInternetCheckBox.Enabled = False
                End If
                If NUM = 0 Then
                    Me.Button20.Visible = True
                    Me.Button20.Text = "Bloqueo de Cliente"
                    glopar = "N"
                ElseIf num2 = 1 And NUM <> 0 Then
                    Me.Button20.Visible = True
                    Me.Button20.Text = "Cliente Bloqueado"
                    glopar = "M"
                    Me.Panel1.Enabled = False
                    Me.Panel7.Enabled = False
                    Me.Panel4.Enabled = False
                    Me.CONCLIENTETVBindingNavigator.Enabled = False
                    eGloContrato = Contrato
                    bloqueado = 1
                    'FrmBloqueo.Show()
                    'Dim myProcess As New Process()
                    'Dim myProcessStartInfo As New ProcessStartInfo("C:\exes\softv\reportes\SoftvPPE.exe", "msjbloq" + "," + CStr(Contrato))
                    'myProcess.StartInfo = myProcessStartInfo
                    'myProcess.Start()
                ElseIf num2 = 0 And NUM <> 0 Then
                    Me.Button20.Visible = True
                    Me.Button20.Text = "Cliente Bloqueado Anteriormente"
                    glopar = "M"
                    Me.Panel1.Enabled = True
                Else
                    Me.Panel1.Enabled = True
                End If

                buscarelaccion()
                'ALMACENA EL TAP Y PLACA DEL CLIENTE (INICIO) *JUANJO*
                uspDameTapCliente(CLng(Me.CONTRATOTextBox.Text))
                'ALMACENA EL TAP Y PLACA DEL CLIENTE (FIN) *JUANJO*
                SP_ConsultaRel_Contratos_Companias(Me.CONTRATOTextBox.Text)

                'aqui*****
                If SoloInternetCheckBox.Checked = False Then
                    Button11.Visible = True
                End If
                DameClaveElector()
            End If
            'Try

            '    Dim auxCalle, auxColonia, auxCiudad As Integer
            '    auxCalle = CALLEComboBox.SelectedValue
            '    auxColonia = COLONIAComboBox.SelectedValue
            '    auxCiudad = CIUDADComboBox.SelectedValue
            '    BaseII.limpiaParametros()
            '    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
            '    CALLEComboBox.DataSource = BaseII.ConsultaDT("[MUESTRACALLES_PorCompania]")
            '    'CALLEComboBox.DataSource = BaseII.ConsultaDT("[MUESTRACALLES]")
            '    CALLEComboBox.DisplayMember = "NOMBRE"
            '    CALLEComboBox.ValueMember = "CLV_CALLE"
            '    CALLEComboBox.SelectedValue = auxCalle
            '    COLONIAComboBox.SelectedValue = auxColonia
            '    CIUDADComboBox.SelectedValue = auxCiudad
            'Catch ex As Exception

            'End Try
            If OpcionCli = "M" Or OpcionCli = "C" Then
                validaClienteEnBuro()
                asignaNombres()
                buscaCONCLIENTETV()
                Dim auxClv_ServicioCLiente As Integer
                auxClv_ServicioCLiente = TipSerTvComboBox.SelectedValue
                MuestraTiposServicioTvPorColonia()
                TipSerTvComboBox.SelectedValue = auxClv_ServicioCLiente

                Me.Button29.Enabled = True

                'CHECA ORDENES DE RETIRO DE APARATO PENDIENTES
                ChecaOrdenRetiro(CONTRATOTextBox.Text)

                'OBTIENE LA CLV_TAP DEL CLIENTE (INICIO) *JUANJO*
                uspDameTapCliente(CLng(Me.CONTRATOTextBox.Text))
                'OBTIENE LA CLV_TAP DEL CLIENTE (FIN) *JUANJO*

                'Me.Valida_servicioTvTableAdapter.Connection = CON
                'Me.Valida_servicioTvTableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_servicioTv, Contrato, validacion)
                'If validacion > 0 Then
                '    damedatostv(Contrato)
                'End If

                'MuestraPromocionAntiguedadCliente(CONTRATOTextBox.Text)
                If Me.ComboBox7.SelectedValue > 0 Then
                    clv_tipo_pagobit = Me.ComboBox7.SelectedValue
                End If
                'BaseII.limpiaParametros()
                'BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
                'BaseII.CreateMyParameter("@yaTiene", ParameterDirection.Output, SqlDbType.Int)
                'BaseII.CreateMyParameter("@clabe", ParameterDirection.Output, SqlDbType.VarChar, 50)
                'BaseII.ProcedimientoOutPut("YaTieneClabe")
                'If BaseII.dicoPar("@yaTiene") = 1 Then
                '    Label62.Visible = True
                '    tbClabe.Visible = True
                '    tbClabe.Text = BaseII.dicoPar("@clabe")
                'End If
            End If
            If IsNumeric(Me.CONTRATOTextBox1.Text) = False Then
                Me.CONCLIENTETVBindingNavigator.Items(4).Visible = True
                Me.ToolStripButton2.Enabled = False
                Me.ToolStripButton3.Enabled = False
                Me.Panel3.Enabled = False
            Else
                Me.CONCLIENTETVBindingNavigator.Items(4).Visible = False
                Me.Panel3.Enabled = True
            End If
            'Eric
            CON.Open()
            eGloContrato = Me.CONTRATOTextBox.Text
            Me.ChecaRoboDeSeñalTableAdapter.Connection = CON
            Me.ChecaRoboDeSeñalTableAdapter.Fill(Me.DataSetEric.ChecaRoboDeSeñal, eGloContrato, eRespuesta)
            CON.Close()
            If eRespuesta = 1 Then
                Me.Button21.BackColor = Color.Red
                If IdSistema = "SA" Then
                    FrmRoboDeSeñal.Show()
                End If
            End If
            '------------------------------------------------------------------------------------------------

            If Me.SoloInternetCheckBox.Checked = True And IdSistema = "VA" Then
                If Me.ComboBox7.SelectedValue = 3 Then
                    frmInternet2.Hide()
                    frmInternet2.Show()
                End If
            End If
            'If Me.Button28.Visible = True Then
            '    Me.Button28.Enabled = False
            'End If

            'If opcion = "C" Or opcion = "M" Then
            '    bloqueado1(CInt(Me.CONTRATOTextBox.Text))
            'End If

            If (IdSistema = "LO" Or IdSistema = "YU") And (OpcionCli = "C" Or OpcionCli = "M") Then
                DameComboYRenta(CLng(Me.CONTRATOTextBox.Text))
                If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                    If Me.CONTRATOTextBox.Text > 0 Then
                        Me.BtnEstadoDeCuenta.Visible = True
                    End If
                End If
            End If
            If GloTipoUsuario <> 40 Then
                Me.ComboBox20.Enabled = False
            ElseIf Me.ComboBox20.Enabled = True Then
            End If
            'Muestra_Usuarios(GloClvUnicaNet, 3)

            ''''NUEVA MODIFICACIÓN PARA BLOQUEAR LA SELECCIÓN DE LA MARCA AL AGREGAR UN CLIENTES O SERVICIOS A UN CONTRATO (INICIO)
            Me.AparatosComboBox.Visible = False
            Me.Label55.Visible = False
            ''''NUEVA MODIFICACIÓN PARA BLOQUEAR LA SELECCIÓN DE LA MARCA AL AGREGAR UN CLIENTES O SERVICIOS A UN CONTRATO (FIN)



            zonas2 = Valida_Zonas()
            CheckTarjetaOxxo.Checked = Valida_TarjetaOxxo()

            If zonas2 = True Then
                CheZonas.Visible = True
                CheZonas.Checked = True
                CheZonas.Enabled = False
                'LabelTZ.Visible = True
            Else
                CheZonas.Visible = False
                CheZonas.Checked = False
                CheZonas.Enabled = False
                'LabelTZ.Visible = False
            End If


            If (Button11.Text = Button7.Text) Or (Button11.Text = Button8.Text) Then
                Button11.Visible = False
            End If
            'Cambia la vista de la pantalla de clientes cuando se agrega un Combo
            'Por ejemplo, si se agrega un combo que tiene Digital e Internet
            'En lugar de mostrar el Pane de TV se mostrará el de Internet
            If CONTRATOTextBox.Text.Length > 0 Then
                VALIDATipSerCliente(CONTRATOTextBox.Text)

                'If Tv = True Then
                '    Exit Sub
                'End If

                'If Internet = True And Digital = True Then
                '    BotonInternet()
                '    Exit Sub
                'End If
                If HAB_TVDIG = 1 Then
                    If Button8.Text = "IPTV" Then Button8.Visible = False
                    If Button7.Text = "IPTV" Then Button7.Visible = False
                    If Button11.Text = "IPTV" Then Button11.Visible = False
                    If Button28.Text = "IPTV" Then Button28.Visible = False
                End If

                If Tv = True Then
                    Panel3.Enabled = True
                    ToolStripButton4.Enabled = False
                    ToolStripButton3.Enabled = True
                    ToolStripButton2.Enabled = True
                    'boton_BasicoTV()
                    If Button8.Text = "&Televisión" Then
                        Button8.PerformClick()
                    ElseIf Button7.Text = "&Televisión" Then
                        Button7.PerformClick()
                    ElseIf Button11.Text = "&Televisión" Then
                        Button11.PerformClick()
                    ElseIf Button28.Text = "&Televisión" Then
                        Button28.PerformClick()
                    End If
                    If HAB_TVDIG = 1 Then
                        Digital = True
                        If Button8.Text = "IPTV" Then Button8.Visible = False
                        If Button7.Text = "IPTV" Then Button7.Visible = False
                        If Button11.Text = "IPTV" Then Button11.Visible = False
                        If Button28.Text = "IPTV" Then Button28.Visible = False
                    End If
                    Exit Sub
                End If

                If Internet = True And Digital = True Then
                    BotonInternet()

                    If Digital = True Then
                        If Button8.Text = "IPTV" Then
                            Button8.PerformClick()
                        ElseIf Button7.Text = "IPTV" Then
                            Button7.PerformClick()
                        ElseIf Button11.Text = "IPTV" Then
                            Button11.PerformClick()
                        ElseIf Button28.Text = "&T&V Digital" Then
                            Button28.PerformClick()
                        End If
                    End If
                    Exit Sub
                End If

                If Digital = True Then
                    BotonInternet()
                    BotonDigital()
                    Exit Sub
                End If

            End If
            '-----------------------------------------------------------------------------

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        'If GloHabilitadosNET = False Then
        '    SoloInternetCheckBox.Visible = False
        '    Me.Button11.Visible = False
        '    Me.DESGLOSA_IvaCheckBox.Visible = False
        '    Me.Label41.Enabled = False
        'End If
        'Me.SoloInternetCheckBox.Visible = False


        Digital = True
        If OpcionCli = "N" Or OpcionCli = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)

        CodigoPostalTextBox.Enabled = True

        'Panel16.Visible = False
        'Button33.Visible = False
        If opcion <> "N" Then
            panelcompania.BackColor = Color.LightGray
        End If

        'Panel16.Visible = False
        Button34.Visible = False
        'Button33.Visible = False
        locLoad = True

    End Sub

    Private Sub asignaNombres()
        Dim ca As ClienteApellidos = ClienteApellidos.GetOne(Integer.Parse(Me.CONTRATOTextBox.Text))
        If (Not ca Is Nothing) Then
            frmnsb.txtNombre.Text = ca.Nombre
            frmnsb.txtSegundoNombre.Text = ca.SegundoNombre
            frmnsb.txtApellidoPaterno.Text = ca.Apellido_Paterno
            frmnsb.txtApellidoMaterno.Text = ca.Apellido_Materno
            frmnsb.txtRFC.Text = ca.RFC
            frmnsb.RadioButton1.Checked = ca.EsFisica.Value
            frmnsb.RadioButton2.Checked = Not ca.EsFisica.Value
            If (ca.FechaNacimiento.HasValue) Then
                frmnsb.dtpFechaNacimiento.Value = ca.FechaNacimiento.Value
            End If
        End If

    End Sub

    Private Sub buscarelaccion()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
            Me.CONRel_Clientes_TiposClientesTableAdapter.Connection = CON
            Me.CONRel_Clientes_TiposClientesTableAdapter.Fill(Me.DataSetEDGAR.CONRel_Clientes_TiposClientes, Me.CONTRATOTextBox.Text)
        End If
        CON.Close()
    End Sub

    Private Sub asignacolonia()
        Me.Clv_ColoniaTextBox.Text = Me.COLONIAComboBox.SelectedValue
    End Sub

    Private Sub asiganacalle()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            LocClv_Calletmp = Me.CALLEComboBox.SelectedValue
            Me.Clv_CalleTextBox.Text = Me.CALLEComboBox.SelectedValue
            'Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
            'Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.CALLEComboBox.SelectedValue, Integer)))
            Try
                'Dim auxColonia As Integer
                'auxColonia = COLONIAComboBox.SelectedValue
                'BaseII.limpiaParametros()
                'BaseII.CreateMyParameter("@CLV_CALLE", SqlDbType.Int, CALLEComboBox.SelectedValue)
                'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                'COLONIAComboBox.DataSource = BaseII.ConsultaDT("[DAMECOLONIA_CALLE_COMPANIA]")
                'COLONIAComboBox.DisplayMember = "COLONIA"
                'COLONIAComboBox.ValueMember = "CLV_COLONIA"
                'COLONIAComboBox.SelectedValue = auxColonia
            Catch ex As Exception

            End Try


            'COLONIAComboBox.SelectedValue = 0
            'Me.Clv_ColoniaTextBox.Text = Me.COLONIAComboBox.SelectedValue
            CON.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub asignaciudad()
        Me.Clv_CiudadTextBox.Text = Me.CIUDADComboBox.SelectedValue
    End Sub



    Private Sub Bucacontrato(ByVal txtcontrato As Long)
        If OpcionCli <> "N" Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Try
                Me.CONSULTARCLIENTETableAdapter.Connection = CON
                Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, New System.Nullable(Of Long)(CType(txtcontrato, Long)))
                CON.Close()
                CON.Open()
                Me.BuscaBloqueadoTableAdapter.Connection = CON
                Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Contrato, NUM, num2)

                CON.Close()
                Consulta_No_Int(Me.CONTRATOTextBox.Text)
                If IdSistema <> "AG" And IdSistema <> "SA" Then
                    Apellidos(1)
                End If


                

            Catch ex As System.Exception
                CON.Close()
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub guarda_Cliente()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.asiganacalle()
        Me.asignaciudad()
        Me.asignacolonia()
        If OpcionCli = "N" Then
            If Len(Trim(Me.NOMBRETextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CalleTextBox.Text) = False Then
                MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
                MsgBox("Se Requiere las Entre Calles ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Numero ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Then
                MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Then
                MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        Me.Validate()
        Me.CONSULTARCLIENTEBindingSource.EndEdit()
        Me.CONSULTARCLIENTETableAdapter.Connection = CON
        Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
        TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue)
        CON.Close()

    End Sub

    Public Function valida_direccionOledb(ByVal oclv_session As Long, ByVal oclv_colonia As Long, ByVal oclv_calle As Long, ByVal onumero As String, ByVal oContrato As Long) As Integer
        valida_direccionOledb = 0
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            con.Open()
            With cmd
                .CommandText = "Valida_DireccionNew"
                .Connection = con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = oclv_session
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@clv_colonia", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = oclv_colonia
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@clv_calle", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = oclv_calle
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@numero", SqlDbType.VarChar, 10)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = onumero
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@error", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = ""
                .Parameters.Add(prm4)

                Dim prm5 As New SqlParameter("@contratoNew", SqlDbType.BigInt)
                prm5.Direction = ParameterDirection.Input
                prm5.Value = oContrato
                .Parameters.Add(prm5)

                Dim ia As Integer = cmd.ExecuteNonQuery()
                valida_direccionOledb = prm4.Value
            End With
            con.Close()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

        End Try
    End Function


    Private Sub borra_valida_direccion(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            con.Open()
            With cmd
                .CommandText = "Borra_Valida_direccion"
                .Connection = con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_session
                .Parameters.Add(prm)

                Dim ia As Integer = cmd.ExecuteNonQuery()

            End With
            con.Close()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

        End Try
    End Sub
    Private Sub Apellidos(ByVal opc As Integer)


        Dim conApe As New SqlConnection(MiConexion)
        Dim CmdAp As New SqlCommand
        conApe.Open()
        With CmdAp
            .CommandText = "Administra_Apellidos_Clientes"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conApe

            Dim Pmt As New SqlParameter("@Contrato", SqlDbType.BigInt)
            Pmt.Direction = ParameterDirection.Input
            If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
                Pmt.Value = 0
            Else
                Pmt.Value = Me.CONTRATOTextBox.Text
            End If

            Dim Pmt2 As New SqlParameter("@Nombre", SqlDbType.VarChar, 150)
            Pmt2.Direction = ParameterDirection.InputOutput
            Pmt2.Value = Me.TextBox27.Text

            Dim Pmt3 As New SqlParameter("@ApellidoP", SqlDbType.VarChar, 150)
            Pmt3.Direction = ParameterDirection.InputOutput
            Pmt3.Value = Me.TextBox4.Text

            Dim Pmt4 As New SqlParameter("@ApellidoM", SqlDbType.VarChar, 150)
            Pmt4.Direction = ParameterDirection.InputOutput
            Pmt4.Value = Me.TextBox7.Text

            Dim Pmt5 As New SqlParameter("@opc", SqlDbType.Int)
            Pmt5.Direction = ParameterDirection.Input
            Pmt5.Value = opc

            .Parameters.Add(Pmt)
            .Parameters.Add(Pmt2)
            .Parameters.Add(Pmt3)
            .Parameters.Add(Pmt4)
            .Parameters.Add(Pmt5)

            .ExecuteNonQuery()
            If opc = 1 Then
                Me.TextBox27.Text = Pmt2.Value
                Me.TextBox4.Text = Pmt3.Value
                Me.TextBox7.Text = Pmt4.Value
            End If

        End With
    End Sub

    'Private Sub Checa_si_quiere_combos()

    '    Dim op As Integer = 0
    '    If IdSistema = "LO" Or IdSistema = "YU" Then
    '        op = MsgBox("Desea Contratar Un Combo", MsgBoxStyle.YesNo)
    '        If op = 6 Then 'si Desea El Combo
    '            'Edgar 11Mayo2009
    '            'ChecaTelefonosDisponibles()
    '            'If eResValida = 1 Then
    '            '    MsgBox(eMsgValida, MsgBoxStyle.Information)
    '            '    Exit Sub
    '            'End If
    '            'Edgar 11Mayo2009
    '            LocContratoLog = CLng(Me.CONTRATOTextBox.Text)
    '            FrmContratacionCombo.Show()
    '        End If
    '    End If
    'End Sub
    Dim preguntaFactura As Boolean = False
    Dim GuardaMismaElector As Boolean = False
    Private Sub CONSULTARCLIENTEBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSULTARCLIENTEBindingNavigatorSaveItem.Click
        'If ComboBoxCompanias.SelectedIndex <= 0 Then
        '    MsgBox("Seleccione la compañía.")
        '    Exit Sub
        'End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.asiganacalle()
        'Me.asignaciudad()
        'Me.asignacolonia()
        Dim Locerror_dir As Integer = 0
        'If OpcionCli = "N" Then
        '    preguntaFactura = True
        'End If

        'If IdSistema <> "AG" And IdSistema <> "SA" Then
        '    Me.NOMBRETextBox.Text = Me.TextBox27.Text + " " + Me.TextBox4.Text + " " + Me.TextBox7.Text
        'End If
        Me.Clv_CiudadTextBox.Text = Me.CIUDADComboBox.SelectedValue
        If ComboBoxCompanias.Text = "" Then
            MsgBox("Selecciona una Compañía")
            Exit Sub
        End If
        If ComboBoxCompanias.Items.Count = 0 Then
            MsgBox("Selecciona una Compañía")
            Exit Sub
        End If
        'Me.Clv_ColoniaTextBox.Text = Me.COLONIAComboBox.SelectedValue
        'Me.Clv_CalleTextBox.Text = Me.CALLEComboBox.SelectedValue
        If OpcionCli = "N" Then
            If Len(Trim(Me.NOMBRETextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
                Exit Sub
            End If
            'If IsNumeric(Me.Clv_CalleTextBox.Text) = False Then
            '    MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
            '    Exit Sub
            'End If
            If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
                MsgBox("Se Requiere las Entre Calles ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.tbclvelec.Text)) = 0 Then
                MsgBox("Se Requiere el no. de identificación", MsgBoxStyle.Information)
                Exit Sub
            End If
            If ComboBoxTipoIdenti.SelectedValue = 0 Then
                MsgBox("Se Requiere el tipo de identificación", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.TELEFONOTextBox.Text)) = 0 And Len(Trim(Me.CELULARTextBox.Text)) = 0 Then
                MsgBox("Se requiere por lo menos un número de teléfono", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.EmailTextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Email", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Número ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Or COLONIAComboBox.SelectedValue = Nothing Then
                MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Or COLONIAComboBox.SelectedValue = Nothing Then
                MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                Exit Sub
            End If
            If ComboBoxEstados.SelectedValue = Nothing Then
                MsgBox("Seleccione el Estado", MsgBoxStyle.Information)
                Exit Sub
            End If
            If ComboBoxLocalidad.SelectedValue = Nothing Then
                MsgBox("Seleccione la Localidad", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Me.ComboBoxCompanias.Text = "" Then
                MsgBox("Seleccione la Compañía", MsgBoxStyle.Information)
                Exit Sub
            End If

        End If

        If OpcionCli = "N" Or OpcionCli = "M" Then
            If TELEFONOTextBox.Text.Length > 0 Then
                If TELEFONOTextBox.Text.Length <> 10 Then
                    MsgBox("El teléfono principal debe constar de 10 dígitos.")
                    Exit Sub
                End If
            End If
            If CELULARTextBox.Text.Length > 0 Then
                If CELULARTextBox.Text.Length <> 10 Then
                    MsgBox("El celular debe constar de 10 dígitos.")
                    Exit Sub
                End If
            End If
            If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Or COLONIAComboBox.SelectedValue = Nothing Then
                MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Then
                MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                Exit Sub
            End If
            If ComboBoxEstados.SelectedValue = Nothing Then
                MsgBox("Seleccione el Estado", MsgBoxStyle.Information)
                Exit Sub
            End If
            If ComboBoxLocalidad.SelectedValue = Nothing Then
                MsgBox("Seleccione la Localidad", MsgBoxStyle.Information)
                Exit Sub
            End If
            If CALLEComboBox.SelectedValue = Nothing And CapturaCalle = False Then
                MsgBox("Seleccione una calle válida")
                Exit Sub
            End If
            If Me.CELULARTextBox.Text.Length > 0 Then
                If IsNumeric(Me.CELULARTextBox.Text) = True Then
                    If Me.CELULARTextBox.Text.Length > 13 Then
                        MsgBox("¡El número celular no debe ser mayor a 13 dígitos!", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                Else
                    MsgBox("¡El número celular sólo debe contener caracteres numéricos!", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

        End If

        If Len(Trim(Me.ComboBox7.Text)) = 0 Then
            MsgBox("Seleccione el Tipo de Cobro", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.ComboBox7.SelectedValue) = False Then
            MsgBox("Seleccione el Tipo de Cobro", MsgBoxStyle.Information)
            Exit Sub
        End If
        If CALLEComboBox.SelectedValue = Nothing And CapturaCalle = False Then
            MsgBox("Seleccione una calle válida")
            Exit Sub
        End If
        'If OpcionCli = "N" Then
        '    Dim result As Integer = MessageBox.Show("¿Desea continuar con el contrato en la siguiente compañía?: " + ComboBoxCompanias.Text, "¡Atención!", MessageBoxButtons.YesNoCancel)
        '    If result = DialogResult.Cancel Then
        '        Exit Sub
        '    ElseIf result = DialogResult.No Then
        '        Exit Sub
        '    ElseIf result = DialogResult.Yes Then
        '        'MessageBox.Show("Yes pressed")
        '    End If
        'End If
        If OpcionCli = "N" Then
            'Validación Licencia---------------------------------------------------------------------------
            If Int(ValidaLicencia()) = 0 Then
                Exit Sub
            End If
            Dim res As Integer = ValidaClaveElector(1)
            If res = 1 Then
                Dim result As Integer = MessageBox.Show("Ya existe ese no. de identificación registrado en otro contrato. ¿Desea autorizar el contrato con la misma?", "Autorización", MessageBoxButtons.YesNoCancel)
                If result = DialogResult.Cancel Then
                    Exit Sub
                ElseIf result = DialogResult.No Then
                    Exit Sub
                ElseIf result = DialogResult.Yes Then
                    GuardaMismaElector = True
                End If
            End If


            'Validación Nombre------------------------------------------------------------------------------
            eResValida = 0
            ValidaNombreUsuario(Me.NOMBRETextBox.Text)
            If eResValida = 1 Then
                MsgBox(eMsgValida, MsgBoxStyle.Exclamation)
                eNombre = ""
                eNombre = Me.NOMBRETextBox.Text
                Dim fRMMC As New FrmMuestraContrato
                'FrmMuestraContrato.Show()
                fRMMC.ShowDialog()
                Exit Sub
            Else
                eResValida = 0
                ValidaNombreUsuario2()
                If eResValida = 1 Then
                    MsgBox(eMsgValida, MsgBoxStyle.Exclamation)
                    'eNombre = ""
                    'eNombre = Me.NOMBRETextBox.Text
                    'FrmMuestraContrato.Show()
                    Dim fRMMC As New FrmMuestraContrato
                    'FrmMuestraContrato.Show()
                    fRMMC.ShowDialog()
                    Exit Sub
                End If
            End If
            OpcionCli = "M"
            'Validacion Calles-------------------------------------------------------------------------------
            If Locclv_session2 = 0 Then
                Me.Dame_clv_session_clientesTableAdapter.Connection = CON
                Me.Dame_clv_session_clientesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_clv_session_clientes, Locclv_session2)
                borra_valida_direccion(Locclv_session2)
            End If
            'Me.Valida_Direccion1TableAdapter.Connection = CON
            'Me.Valida_Direccion1TableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_Direccion1, Locclv_session2, CLng(Me.COLONIAComboBox.SelectedValue), CLng(Me.CALLEComboBox.SelectedValue), Me.NUMEROTextBox.Text, Locerror_dir)
            Locerror_dir = valida_direccionOledb(Locclv_session2, CLng(Me.COLONIAComboBox.SelectedValue), CLng(Me.CALLEComboBox.SelectedValue), Me.NUMEROTextBox.Text, CONTRATOTextBox.Text)

            If Locerror_dir = 1 And Locbnd2clientes = False Then
                If bnddir = False Then
                    'bnddir = True
                    MsgBox("Esta Direccion Ya Esta Asignada a Otro(s) Cliente(s)", MsgBoxStyle.Information)
                    FrmMuestraDireccion.Show()
                    If TextBoxNoContrato.Text = "" Then
                        OpcionCli = "N"
                    End If
                    Exit Sub
                ElseIf bnddir = True Then
                    Me.Validate()
                    Me.CONSULTARCLIENTEBindingSource.EndEdit()
                    Me.CONSULTARCLIENTETableAdapter.Connection = CON
                    Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
                    TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue)
                    GuardaEstadoYLoDemas(Me.CONTRATOTextBox.Text)
                    If EsProspecto Then
                        TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + "0"
                    Else
                        TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + ComboBoxCompanias.SelectedValue.ToString
                    End If

                    panelcompania.BackColor = Color.WhiteSmoke
                    ComboBoxCompanias.Enabled = False
                    If Me.SoloInternetCheckBox.Checked = True Then
                        'Me.SoloInternetCheckBox.Enabled = False
                    End If
                    CON.Close()
                    GUARDARRel_Clientes_TiposClientesGuarda()
                    Guarda_No_Int(Me.CONTRATOTextBox.Text, Me.TxtNumeroInt.Text)
                    'GUARDA LA PLACA Y TAP DEL CLIENTE (INICIO) *JUANJO*
                    uspGuardaPlacaTapCliente(CLng(Me.CONTRATOTextBox.Text), CInt(Me.cmbTap.SelectedValue), Me.txtPlaca.Text, 0)
                    NueRelClienteTab(Me.CONTRATOTextBox.Text, Me.cmbTap.Text, "Tap", cmbTap.SelectedValue)
                    NueRelClienteTab(Me.CONTRATOTextBox.Text, Me.cmbNap.Text, "Nap", cmbNap.SelectedValue)
                    'GUARDA LA PLACA Y TAP DEL CLIENTE (FIN) *JUANJO*

                    GuardaEntreCallesTemporales()

                    GuardaEstadoYLoDemas(Me.CONTRATOTextBox.Text)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClaveElector", SqlDbType.VarChar, tbclvelec.Text, 250)
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                    BaseII.CreateMyParameter("@idTipoIdentificación", SqlDbType.BigInt, ComboBoxTipoIdenti.SelectedValue)
                    BaseII.Inserta("GuardaClaveElector")
                    If GuardaMismaElector Then
                        BaseII.limpiaParametros()
                        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                        BaseII.Inserta("GuardaBitacoraElector")
                        GuardaMismaElector = False
                    End If

                    MsgBox("Se ha Guardado con Éxito", MsgBoxStyle.Information)
                    SoloInternetCheckBox.Enabled = True
                    GloIdCompania = ComboBoxCompanias.SelectedValue
                    timercompania.Enabled = False
                    guardabitacora()
                    BuscaCuenta()
                    PreguntaSiQuiereCombo() 'Esto se desprotegio
                    eGuardarCliente = True
                    Me.Button11.Enabled = True
                End If
            ElseIf Locerror_dir = 0 Then
                Me.Validate()
                Me.CONSULTARCLIENTEBindingSource.EndEdit()
                Me.CONSULTARCLIENTETableAdapter.Connection = CON
                Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
                TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue)
                If EsProspecto Then
                    TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + "0"
                Else
                    TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + ComboBoxCompanias.SelectedValue.ToString
                End If

                GuardaEstadoYLoDemas(Me.CONTRATOTextBox.Text)
                ComboBoxCompanias.Enabled = False
                If Me.SoloInternetCheckBox.Checked = True Then
                    'Me.SoloInternetCheckBox.Enabled = False
                End If
                CON.Close()
                GUARDARRel_Clientes_TiposClientesGuarda()
                Guarda_No_Int(Me.CONTRATOTextBox.Text, Me.TxtNumeroInt.Text)
                'GUARDA LA PLACA Y TAP DEL CLIENTE (INICIO) *JUANJO*
                uspGuardaPlacaTapCliente(CLng(Me.CONTRATOTextBox.Text), CInt(Me.cmbTap.SelectedValue), Me.txtPlaca.Text, 0)
                NueRelClienteTab(Me.CONTRATOTextBox.Text, Me.cmbTap.Text, "Tap", cmbTap.SelectedValue)
                NueRelClienteTab(Me.CONTRATOTextBox.Text, Me.cmbNap.Text, "Nap", cmbNap.SelectedValue)
                'GUARDA LA PLACA Y TAP DEL CLIENTE (FIN) *JUANJO*
                GuardaEntreCallesTemporales()
                GuardaEstadoYLoDemas(Me.CONTRATOTextBox.Text)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@ClaveElector", SqlDbType.VarChar, tbclvelec.Text, 250)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                BaseII.CreateMyParameter("@idTipoIdentificación", SqlDbType.BigInt, ComboBoxTipoIdenti.SelectedValue)
                BaseII.Inserta("GuardaClaveElector")
                If GuardaMismaElector Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    BaseII.Inserta("GuardaBitacoraElector")
                    GuardaMismaElector = False
                End If
                MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
                SoloInternetCheckBox.Enabled = True
                GloIdCompania = ComboBoxCompanias.SelectedValue
                panelcompania.BackColor = Color.WhiteSmoke
                timercompania.Enabled = False
                guardabitacora()
                BuscaCuenta()
                PreguntaSiQuiereCombo()
                eGuardarCliente = True
                Me.Button11.Enabled = True
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@ClaveElector", SqlDbType.VarChar, tbclvelec.Text, 250)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                BaseII.CreateMyParameter("@idTipoIdentificación", SqlDbType.BigInt, ComboBoxTipoIdenti.SelectedValue)
                BaseII.Inserta("GuardaClaveElector")
                If GuardaMismaElector Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    BaseII.Inserta("GuardaBitacoraElector")
                    GuardaMismaElector = False
                End If
            ElseIf Locerror_dir = 1 And Locbnd2clientes = True Then
                Me.Validate()
                Me.CONSULTARCLIENTEBindingSource.EndEdit()
                Me.CONSULTARCLIENTETableAdapter.Connection = CON
                Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
                TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue)
                If EsProspecto Then
                    TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + "0"
                Else
                    TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + ComboBoxCompanias.SelectedValue.ToString
                End If

                GuardaEstadoYLoDemas(Me.CONTRATOTextBox.Text)
                ComboBoxCompanias.Enabled = False
                If Me.SoloInternetCheckBox.Checked = True Then
                    'Me.SoloInternetCheckBox.Enabled = False
                End If
                CON.Close()
                GUARDARRel_Clientes_TiposClientesGuarda()
                Guarda_No_Int(Me.CONTRATOTextBox.Text, Me.TxtNumeroInt.Text)
                'GUARDA LA PLACA Y TAP DEL CLIENTE (INICIO) *JUANJO*
                uspGuardaPlacaTapCliente(CLng(Me.CONTRATOTextBox.Text), CInt(Me.cmbTap.SelectedValue), Me.txtPlaca.Text, 0)
                NueRelClienteTab(Me.CONTRATOTextBox.Text, Me.cmbTap.Text, "Tap", cmbTap.SelectedValue)
                NueRelClienteTab(Me.CONTRATOTextBox.Text, Me.cmbNap.Text, "Nap", cmbNap.SelectedValue)
                'GUARDA LA PLACA Y TAP DEL CLIENTE (FIN) *JUANJO*
                GuardaEntreCallesTemporales()
                GuardaEstadoYLoDemas(Me.CONTRATOTextBox.Text)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@ClaveElector", SqlDbType.VarChar, tbclvelec.Text, 250)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                BaseII.CreateMyParameter("@idTipoIdentificación", SqlDbType.BigInt, ComboBoxTipoIdenti.SelectedValue)
                BaseII.Inserta("GuardaClaveElector")
                If GuardaMismaElector Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    BaseII.Inserta("GuardaBitacoraElector")
                    GuardaMismaElector = False
                End If
                MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
                SoloInternetCheckBox.Enabled = True
                GloIdCompania = ComboBoxCompanias.SelectedValue
                timercompania.Enabled = False
                panelcompania.BackColor = Color.WhiteSmoke
                guardabitacora()
                BuscaCuenta()
                PreguntaSiQuiereCombo()
                eGuardarCliente = True
                'TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + ComboBoxCompanias.SelectedValue.ToString
                Me.Button11.Enabled = True
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@ClaveElector", SqlDbType.VarChar, tbclvelec.Text)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                BaseII.CreateMyParameter("@idTipoIdentificación", SqlDbType.BigInt, ComboBoxTipoIdenti.SelectedValue)
                BaseII.Inserta("GuardaClaveElector")
                If GuardaMismaElector Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                    BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                    BaseII.Inserta("GuardaBitacoraElector")
                    GuardaMismaElector = False
                End If
            End If
        Else
            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@ClaveElector", SqlDbType.VarChar, tbclvelec.Text, 250)
            'BaseII.CreateMyParameter("@res", ParameterDirection.Output, SqlDbType.Int, 0)
            'BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            'BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
            'BaseII.ProcedimientoOutPut("ValidaClaveElector")

            'mike
            If Len(Trim(Me.tbclvelec.Text)) = 0 Then
                MsgBox("Falta el no. de identificación.")
                Exit Sub
            End If
            Dim res As Integer = ValidaClaveElector(2)
            If res = 1 Then
                Dim result2 As Integer = MessageBox.Show("Ya existe una ese no de identificación registrado en otro contrato. ¿Desea autorizar el contrato con la misma?", "Autorización", MessageBoxButtons.YesNoCancel)
                If result2 = DialogResult.Cancel Then
                    Exit Sub
                ElseIf result2 = DialogResult.No Then
                    Exit Sub
                ElseIf result2 = DialogResult.Yes Then
                    GuardaMismaElector = True
                End If
            End If

            'PAGOS DIFERIDOS--------------------------------------------------
            'ValidaCambioPagosDif(CONTRATOTextBox.Text, ComboBox7.SelectedValue)
            If eRes = 1 Then
                MsgBox(eMsj, MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            '------------------------------------------------------------------

            Me.Validate()
            Me.CONSULTARCLIENTEBindingSource.EndEdit()
            Me.CONSULTARCLIENTETableAdapter.Connection = CON
            Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
            TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue)
            'TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue)
            If EsProspecto Then
                TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + "0"
            Else
                TextBoxNoContrato.Text = TextBoxNoContrato.Text + "-" + ComboBoxCompanias.SelectedValue.ToString
            End If

            ComboBoxCompanias.Enabled = False
            If Me.SoloInternetCheckBox.Checked = True Then
                'Me.SoloInternetCheckBox.Enabled = False
            End If
            CON.Close()
            GUARDARRel_Clientes_TiposClientesGuarda()
            bndbitacora = True
            guardabitacora()
            Guarda_No_Int(Me.CONTRATOTextBox.Text, Me.TxtNumeroInt.Text)

            'GUARDA LA PLACA Y TAP DEL CLIENTE (INICIO) *JUANJO*
            uspGuardaPlacaTapCliente(CLng(Me.CONTRATOTextBox.Text), CInt(Me.cmbTap.SelectedValue), Me.txtPlaca.Text, 0)
            NueRelClienteTab(Me.CONTRATOTextBox.Text, Me.cmbTap.Text, "Tap", cmbTap.SelectedValue)
            NueRelClienteTab(Me.CONTRATOTextBox.Text, Me.cmbNap.Text, "Nap", cmbNap.SelectedValue)
            'GUARDA LA PLACA Y TAP DEL CLIENTE (FIN) *JUANJO*
            GuardaEntreCallesTemporales()
            GuardaEstadoYLoDemas(Me.CONTRATOTextBox.Text)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClaveElector", SqlDbType.VarChar, tbclvelec.Text, 250)
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
            BaseII.CreateMyParameter("@idTipoIdentificación", SqlDbType.BigInt, ComboBoxTipoIdenti.SelectedValue)
            BaseII.Inserta("GuardaClaveElector")
            If GuardaMismaElector Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                BaseII.Inserta("GuardaBitacoraElector")
                GuardaMismaElector = False
            End If

            MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
            SoloInternetCheckBox.Enabled = True
            GloIdCompania = ComboBoxCompanias.SelectedValue
            timercompania.Enabled = False
            panelcompania.BackColor = Color.WhiteSmoke
            eGuardarCliente = True
            Me.Button29.Enabled = True
            Me.Button11.Enabled = True
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClaveElector", SqlDbType.VarChar, tbclvelec.Text)
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
            BaseII.CreateMyParameter("@idTipoIdentificación", SqlDbType.BigInt, ComboBoxTipoIdenti.SelectedValue)
            BaseII.Inserta("GuardaClaveElector")
            If GuardaMismaElector Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
                BaseII.Inserta("GuardaBitacoraElector")
                GuardaMismaElector = False
            End If
        End If
            'Fin VAlidacion Calles
            CON.Close()
            GuardaClientesApellidos()
            'TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue) 
            ComboBoxCompanias.Enabled = False
            ''Lo Cambiamos a M por que ya se guardo una vez
            OpcionCli = "M"
            If preguntaFactura Then
                Dim res = MessageBox.Show("¿Requiere Factura Físcal?", "¡Atención!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                If res = 1 Then

                    If IsNumeric(Contrato) = True Then
                        If CLng(Contrato) > 0 Then
                            FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
                            FacturacionDigitalSoftv.ClassCFDI.LocClv_Ciudad = ""
                            FacturacionDigitalSoftv.ClassCFDI.OpcionCli = OpcionCli
                            FacturacionDigitalSoftv.ClassCFDI.GloContrato = Contrato
                            Dim frm As New FacturacionDigitalSoftv.FrmDatosFiscales
                            frm.ShowDialog()
                        End If
                    End If
                    preguntaFactura = False
                    'FrmDatosFiscales.requiereFactura = False
                Else
                    preguntaFactura = False
                End If
            End If
            'If OpcionCli = "M" Then
            '    eResValida = 0
            '    ValidaDatosBancarios(CInt(Me.CONTRATOTextBox.Text), CLng(Me.ComboBox7.SelectedValue))
            '    If eResValida = 1 Then
            '        MsgBox(eMsgValida, MsgBoxStyle.Exclamation)
            '    End If
            'End If
            'GuardaCuenta()
            'Valida boton de generacion de ordenes 


            'bnGeneraOrden.Enabled = True
    End Sub

    Private Sub NueRelClienteTab(ByVal Contrato As Long, ByVal Tab As String, ByVal NT As String, ByVal IdTap As Integer)
        If IdTap > 0 Then
            Dim procedimiento As String
            If NT = "Tap" Then
                procedimiento = "GuardarRelClienteTap"
            End If
            If NT = "Nap" Then
                procedimiento = "GuardarRelClienteNap"
            End If
            Dim conexion As New SqlConnection(MiConexion)
            'Dim comando As New SqlCommand("NueRelClienteTab", conexion)
            Dim comando As New SqlCommand(procedimiento, conexion)
            comando.CommandType = CommandType.StoredProcedure

            Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = Contrato
            comando.Parameters.Add(parametro)

            Dim parametro2 As New SqlParameter("@TAP", SqlDbType.VarChar, 50)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = Tab
            comando.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@IDTAP", SqlDbType.VarChar, 50)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = IdTap
            comando.Parameters.Add(parametro3)

            Try
                conexion.Open()
                comando.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Finally
                conexion.Close()
                conexion.Dispose()
            End Try
        End If
    End Sub
    Private Sub GuardaEstadoYLoDemas(ByVal contratointerno As Long)
        Try
            Dim clv_calle As Integer
            If CapturaCalle And CALLEComboBox.SelectedValue = Nothing Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.Int, ComboBoxEstados.SelectedValue)
                BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, CIUDADComboBox.SelectedValue)
                BaseII.CreateMyParameter("@Clv_Localidad", SqlDbType.Int, ComboBoxLocalidad.SelectedValue)
                BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, COLONIAComboBox.SelectedValue)
                BaseII.CreateMyParameter("@calleNombre", SqlDbType.VarChar, CALLEComboBox.Text)
                BaseII.CreateMyParameter("@clv_calle", ParameterDirection.Output, SqlDbType.Int)
                BaseII.ProcedimientoOutPut("sp_guardarCalleNuevaCliente")
                clv_calle = BaseII.dicoPar("@clv_calle")
            Else
                clv_calle = CALLEComboBox.SelectedValue
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, ComboBoxEstados.SelectedValue)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, CIUDADComboBox.SelectedValue)
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, ComboBoxLocalidad.SelectedValue)
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, COLONIAComboBox.SelectedValue)
            BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, clv_calle)
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, contratointerno)
            BaseII.Inserta("GuardaInfoClientes")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dame_datos()
        Try
            Dim comando As New SqlClient.SqlCommand
            Dim con89 As New SqlClient.SqlConnection(MiConexion)
            con89.Open()
            If OpcionCli = "M" Then

                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = con89
                    .CommandText = "CONSULTARCLIENTE_bitacora"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    '@CONTRATO BIGINT,@contrato1 bigint output, @NOMBRE varchar(max) output, @Clv_Calle bigint output,
                    '@NUMERO varchar(max) output, @ENTRECALLES varchar(max) output, @Clv_Colonia bigint output,@CodigoPostal varchar(50) output,
                    ' @TELEFONO varchar(30) output, @CELULAR varchar(50) output, @DESGLOSA_Iva bit output, @SoloInternet bit output, 
                    '@eshotel bit output,@clv_Ciudad int output,@Email varchar(50) output, @clv_sector int output,@Clv_Periodo int output
                    Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@contrato1", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@NOMBRE", SqlDbType.VarChar, 300)
                    Dim prm3 As New SqlParameter("@Clv_Calle", SqlDbType.BigInt)
                    Dim prm4 As New SqlParameter("@NUMERO", SqlDbType.VarChar, 300)
                    Dim prm5 As New SqlParameter("@ENTRECALLES", SqlDbType.VarChar, 300)
                    Dim prm6 As New SqlParameter("@Clv_Colonia", SqlDbType.BigInt)
                    Dim prm7 As New SqlParameter("@CodigoPostal", SqlDbType.VarChar, 50)
                    Dim prm8 As New SqlParameter("@TELEFONO", SqlDbType.VarChar, 30)
                    Dim prm9 As New SqlParameter("@CELULAR", SqlDbType.VarChar, 50)
                    Dim prm10 As New SqlParameter("@DESGLOSA_Iva", SqlDbType.Bit)
                    Dim prm11 As New SqlParameter("@SoloInternet", SqlDbType.Bit)
                    Dim prm12 As New SqlParameter("@eshotel", SqlDbType.Bit)
                    Dim prm13 As New SqlParameter("@clv_Ciudad", SqlDbType.Int)
                    Dim prm14 As New SqlParameter("@Email", SqlDbType.VarChar, 50)
                    Dim prm15 As New SqlParameter("@clv_sector", SqlDbType.Int)
                    Dim prm16 As New SqlParameter("@Clv_Periodo", SqlDbType.Int)



                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm4.Direction = ParameterDirection.Output
                    prm5.Direction = ParameterDirection.Output
                    prm6.Direction = ParameterDirection.Output
                    prm7.Direction = ParameterDirection.Output
                    prm8.Direction = ParameterDirection.Output
                    prm9.Direction = ParameterDirection.Output
                    prm10.Direction = ParameterDirection.Output
                    prm11.Direction = ParameterDirection.Output
                    prm12.Direction = ParameterDirection.Output
                    prm13.Direction = ParameterDirection.Output
                    prm14.Direction = ParameterDirection.Output
                    prm15.Direction = ParameterDirection.Output
                    prm16.Direction = ParameterDirection.Output


                    prm.Value = Contrato
                    prm1.Value = 0
                    prm2.Value = " "
                    prm3.Value = 0
                    prm4.Value = " "
                    prm5.Value = " "
                    prm6.Value = 0
                    prm7.Value = " "
                    prm8.Value = " "
                    prm9.Value = " "
                    prm10.Value = False
                    prm11.Value = False
                    prm12.Value = False
                    prm13.Value = 0
                    prm14.Value = " "
                    prm15.Value = " "
                    prm16.Value = 0

                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    .Parameters.Add(prm10)
                    .Parameters.Add(prm11)
                    .Parameters.Add(prm12)
                    .Parameters.Add(prm13)
                    .Parameters.Add(prm14)
                    .Parameters.Add(prm15)
                    .Parameters.Add(prm16)

                    Dim i As Integer = comando.ExecuteNonQuery()
                    contratobit = prm1.Value
                    nombre_cliente = prm2.Value
                    Clv_Callebit = prm3.Value
                    NUMERO_casabit = prm4.Value
                    ENTRECALLESbit = prm5.Value
                    Clv_Coloniabit = prm6.Value
                    CodigoPostalbit = prm7.Value
                    TELEFONObit = prm8.Value
                    CELULARbit = prm9.Value
                    DESGLOSA_Ivabit = prm10.Value
                    SoloInternetbit = prm11.Value
                    eshotelbit = prm12.Value
                    clv_Ciudadbit = prm13.Value
                    Email = prm14.Value
                    clv_sectorbit = prm15.Value
                    Clv_Periodobit = prm16.Value


                End With

                con89.Close()

            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        'Fin de llenado variables de la bitacora del sistema

    End Sub
    Private Sub damedatosdig2()
        If OpcionCli = "M" Then
            statusTarjeta = Me.ComboBox13.Text
            Activacion = Me.TextBox33.Text
            Suspension = Me.TextBox32.Text

            Baja = Me.TextBox23.Text
            Obs = Me.TextBox29.Text
            If Me.CheckBox2.CheckState = CheckState.Checked Then
                serenta = "True"
            ElseIf Me.CheckBox2.CheckState = CheckState.Unchecked Then
                serenta = "False"
            End If
        End If
    End Sub

    Private Sub damedatostv_2(ByVal contrato As Integer)
        Try

            Dim comando As New SqlClient.SqlCommand
            Dim con95 As New SqlConnection(MiConexion)
            con95.Open()

            status = ""
            LocTVSINPAGO = 0
            LocTVCONPAGO = 0
            ultimo_mes = 0
            ultimo_anio = 0
            FECHA_ULT_PAGO = ""
            fecha_solicitud = ""
            fecha_suspension = ""
            fecha_Fuera_Area = ""
            fecha_instalacio = ""
            fecha_baja = ""
            LocFec_ULT_PAG_ANT = ""
            PrimerMensualidad = False
            LocClv_MOTCAN = 0
            LocPRIMERMESANT = False
            LocClv_TipoServicioTV = 0
            LocpuntosAcumulados = 0
            Obs = ""
            LocTipSerTv = ""
            LocMOTCAN = ""
            Cortesia = False
            factura = ""
            Descuento = 0
            Clv_Vendedor = 0


            comando = New SqlClient.SqlCommand

            With comando
                .Connection = con95
                .CommandText = "CONCLIENTETV_oledb"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@STATUS", SqlDbType.VarChar, 1)
                Dim prm2 As New SqlParameter("@TVSINPAGO", SqlDbType.BigInt)
                Dim prm3 As New SqlParameter("@TVCONPAGO", SqlDbType.BigInt)
                Dim prm4 As New SqlParameter("@ULTIMO_MES", SqlDbType.Int)
                Dim prm5 As New SqlParameter("@ULTIMO_ANIO", SqlDbType.Int)
                Dim prm6 As New SqlParameter("@FECHA_ULT_PAGO", SqlDbType.VarChar, 20)
                Dim prm7 As New SqlParameter("@FECHA_SOLICITUD", SqlDbType.VarChar, 20)
                Dim prm8 As New SqlParameter("@FECHA_CANCELACIO", SqlDbType.VarChar, 20)
                Dim prm9 As New SqlParameter("@FECHACANCOUTAREA", SqlDbType.VarChar, 20)
                Dim prm10 As New SqlParameter("@FECHA_INST", SqlDbType.VarChar, 20)
                Dim prm11 As New SqlParameter("@FECHA_CORTE", SqlDbType.VarChar, 20)
                Dim prm12 As New SqlParameter("@Fec_ULT_PAG_ANT", SqlDbType.VarChar, 20)
                Dim prm13 As New SqlParameter("@PRIMERMENSUALIDA", SqlDbType.Bit)
                Dim prm14 As New SqlParameter("@Clv_MOTCAN", SqlDbType.Int)
                Dim prm15 As New SqlParameter("@PRIMERMESANT", SqlDbType.Bit)
                Dim prm16 As New SqlParameter("@Clv_TipoServicioTV", SqlDbType.Int)
                Dim prm17 As New SqlParameter("@puntosAcumulados", SqlDbType.Int)
                Dim prm18 As New SqlParameter("@obs", SqlDbType.VarChar, 500)
                Dim prm19 As New SqlParameter("@TipSerTv", SqlDbType.VarChar, 10)
                Dim prm20 As New SqlParameter("@MOTCAN", SqlDbType.VarChar, 500)
                Dim prm21 As New SqlParameter("@Cortesia", SqlDbType.Bit)
                Dim prm22 As New SqlParameter("@factura", SqlDbType.VarChar, 50)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Output
                prm2.Direction = ParameterDirection.Output
                prm3.Direction = ParameterDirection.Output
                prm4.Direction = ParameterDirection.Output
                prm5.Direction = ParameterDirection.Output
                prm6.Direction = ParameterDirection.Output
                prm7.Direction = ParameterDirection.Output
                prm8.Direction = ParameterDirection.Output
                prm9.Direction = ParameterDirection.Output
                prm10.Direction = ParameterDirection.Output
                prm11.Direction = ParameterDirection.Output
                prm12.Direction = ParameterDirection.Output
                prm13.Direction = ParameterDirection.Output
                prm14.Direction = ParameterDirection.Output
                prm15.Direction = ParameterDirection.Output
                prm16.Direction = ParameterDirection.Output
                prm17.Direction = ParameterDirection.Output
                prm18.Direction = ParameterDirection.Output
                prm19.Direction = ParameterDirection.Output
                prm20.Direction = ParameterDirection.Output
                prm21.Direction = ParameterDirection.Output
                prm22.Direction = ParameterDirection.Output

                prm.Value = contrato
                prm1.Value = ""
                prm2.Value = 0
                prm3.Value = 0
                prm4.Value = 0
                prm5.Value = 0
                prm6.Value = ""
                prm7.Value = ""
                prm8.Value = ""
                prm9.Value = ""
                prm10.Value = ""
                prm11.Value = ""
                prm12.Value = ""
                prm13.Value = False
                prm14.Value = 0
                prm15.Value = False
                prm16.Value = 0
                prm17.Value = 0
                prm18.Value = ""
                prm19.Value = ""
                prm20.Value = ""
                prm21.Value = False
                prm22.Value = ""



                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                .Parameters.Add(prm5)
                .Parameters.Add(prm6)
                .Parameters.Add(prm7)
                .Parameters.Add(prm8)
                .Parameters.Add(prm9)
                .Parameters.Add(prm10)
                .Parameters.Add(prm11)
                .Parameters.Add(prm12)
                .Parameters.Add(prm13)
                .Parameters.Add(prm14)
                .Parameters.Add(prm15)
                .Parameters.Add(prm16)
                .Parameters.Add(prm17)
                .Parameters.Add(prm18)
                .Parameters.Add(prm19)
                .Parameters.Add(prm20)
                .Parameters.Add(prm21)
                .Parameters.Add(prm22)


                Dim i As Integer = comando.ExecuteNonQuery()
                status = prm1.Value
                LocTVSINPAGO = prm2.Value
                LocTVCONPAGO = prm3.Value
                ultimo_mes = prm4.Value
                ultimo_anio = prm5.Value
                FECHA_ULT_PAGO = prm6.Value
                fecha_solicitud = prm7.Value
                fecha_suspension = prm8.Value
                fecha_Fuera_Area = prm9.Value
                fecha_instalacio = prm10.Value
                fecha_baja = prm11.Value
                LocFec_ULT_PAG_ANT = prm12.Value
                PrimerMensualidad = prm13.Value
                LocClv_MOTCAN = prm14.Value
                LocPRIMERMESANT = prm15.Value
                LocClv_TipoServicioTV = prm16.Value
                LocpuntosAcumulados = prm17.Value
                Obs = prm18.Value
                LocTipSerTv = prm19.Value
                LocMOTCAN = prm20.Value
                Cortesia = prm21.Value
                factura = prm22.Value


            End With
            Descuento = Me.DescuentoLabel2.Text
            'Clv_Vendedor = Me.ComboBox16.SelectedValue
            LocVendedorTv = Me.ComboBox16.Text
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub damedatostv(ByVal contrato As Integer)
        Try
            If OpcionCli = "M" Then
                Dim comando As New SqlClient.SqlCommand
                Dim con95 As New SqlConnection(MiConexion)
                con95.Open()

                status = ""
                LocTVSINPAGO = 0
                LocTVCONPAGO = 0
                ultimo_mes = 0
                ultimo_anio = 0
                FECHA_ULT_PAGO = ""
                fecha_solicitud = ""
                fecha_suspension = ""
                fecha_Fuera_Area = ""
                fecha_instalacio = ""
                fecha_baja = ""
                LocFec_ULT_PAG_ANT = ""
                PrimerMensualidad = False
                LocClv_MOTCAN = 0
                LocPRIMERMESANT = False
                LocClv_TipoServicioTV = 0
                LocpuntosAcumulados = 0
                Obs = ""
                LocTipSerTv = ""
                LocMOTCAN = ""
                Cortesia = False
                factura = ""
                Descuento = 0
                Clv_Vendedor = 0


                comando = New SqlClient.SqlCommand

                With comando
                    .Connection = con95
                    .CommandText = "CONCLIENTETV_oledb"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@STATUS", SqlDbType.VarChar, 1)
                    Dim prm2 As New SqlParameter("@TVSINPAGO", SqlDbType.BigInt)
                    Dim prm3 As New SqlParameter("@TVCONPAGO", SqlDbType.BigInt)
                    Dim prm4 As New SqlParameter("@ULTIMO_MES", SqlDbType.Int)
                    Dim prm5 As New SqlParameter("@ULTIMO_ANIO", SqlDbType.Int)
                    Dim prm6 As New SqlParameter("@FECHA_ULT_PAGO", SqlDbType.VarChar, 20)
                    Dim prm7 As New SqlParameter("@FECHA_SOLICITUD", SqlDbType.VarChar, 20)
                    Dim prm8 As New SqlParameter("@FECHA_CANCELACIO", SqlDbType.VarChar, 20)
                    Dim prm9 As New SqlParameter("@FECHACANCOUTAREA", SqlDbType.VarChar, 20)
                    Dim prm10 As New SqlParameter("@FECHA_INST", SqlDbType.VarChar, 20)
                    Dim prm11 As New SqlParameter("@FECHA_CORTE", SqlDbType.VarChar, 20)
                    Dim prm12 As New SqlParameter("@Fec_ULT_PAG_ANT", SqlDbType.VarChar, 20)
                    Dim prm13 As New SqlParameter("@PRIMERMENSUALIDA", SqlDbType.Bit)
                    Dim prm14 As New SqlParameter("@Clv_MOTCAN", SqlDbType.Int)
                    Dim prm15 As New SqlParameter("@PRIMERMESANT", SqlDbType.Bit)
                    Dim prm16 As New SqlParameter("@Clv_TipoServicioTV", SqlDbType.Int)
                    Dim prm17 As New SqlParameter("@puntosAcumulados", SqlDbType.Int)
                    Dim prm18 As New SqlParameter("@obs", SqlDbType.VarChar, 500)
                    Dim prm19 As New SqlParameter("@TipSerTv", SqlDbType.VarChar, 10)
                    Dim prm20 As New SqlParameter("@MOTCAN", SqlDbType.VarChar, 500)
                    Dim prm21 As New SqlParameter("@Cortesia", SqlDbType.Bit)
                    Dim prm22 As New SqlParameter("@factura", SqlDbType.VarChar, 50)

                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm4.Direction = ParameterDirection.Output
                    prm5.Direction = ParameterDirection.Output
                    prm6.Direction = ParameterDirection.Output
                    prm7.Direction = ParameterDirection.Output
                    prm8.Direction = ParameterDirection.Output
                    prm9.Direction = ParameterDirection.Output
                    prm10.Direction = ParameterDirection.Output
                    prm11.Direction = ParameterDirection.Output
                    prm12.Direction = ParameterDirection.Output
                    prm13.Direction = ParameterDirection.Output
                    prm14.Direction = ParameterDirection.Output
                    prm15.Direction = ParameterDirection.Output
                    prm16.Direction = ParameterDirection.Output
                    prm17.Direction = ParameterDirection.Output
                    prm18.Direction = ParameterDirection.Output
                    prm19.Direction = ParameterDirection.Output
                    prm20.Direction = ParameterDirection.Output
                    prm21.Direction = ParameterDirection.Output
                    prm22.Direction = ParameterDirection.Output

                    prm.Value = contrato
                    prm1.Value = ""
                    prm2.Value = 0
                    prm3.Value = 0
                    prm4.Value = 0
                    prm5.Value = 0
                    prm6.Value = ""
                    prm7.Value = ""
                    prm8.Value = ""
                    prm9.Value = ""
                    prm10.Value = ""
                    prm11.Value = ""
                    prm12.Value = ""
                    prm13.Value = False
                    prm14.Value = 0
                    prm15.Value = False
                    prm16.Value = 0
                    prm17.Value = 0
                    prm18.Value = ""
                    prm19.Value = ""
                    prm20.Value = ""
                    prm21.Value = False
                    prm22.Value = ""



                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    .Parameters.Add(prm10)
                    .Parameters.Add(prm11)
                    .Parameters.Add(prm12)
                    .Parameters.Add(prm13)
                    .Parameters.Add(prm14)
                    .Parameters.Add(prm15)
                    .Parameters.Add(prm16)
                    .Parameters.Add(prm17)
                    .Parameters.Add(prm18)
                    .Parameters.Add(prm19)
                    .Parameters.Add(prm20)
                    .Parameters.Add(prm21)
                    .Parameters.Add(prm22)


                    Dim i As Integer = comando.ExecuteNonQuery()
                    status = prm1.Value
                    LocTVSINPAGO = prm2.Value
                    LocTVCONPAGO = prm3.Value
                    ultimo_mes = prm4.Value
                    ultimo_anio = prm5.Value
                    FECHA_ULT_PAGO = prm6.Value
                    fecha_solicitud = prm7.Value
                    fecha_suspension = prm8.Value
                    fecha_Fuera_Area = prm9.Value
                    fecha_instalacio = prm10.Value
                    fecha_baja = prm11.Value
                    LocFec_ULT_PAG_ANT = prm12.Value
                    PrimerMensualidad = prm13.Value
                    LocClv_MOTCAN = prm14.Value
                    LocPRIMERMESANT = prm15.Value
                    LocClv_TipoServicioTV = prm16.Value
                    LocpuntosAcumulados = prm17.Value
                    Obs = prm18.Value
                    LocTipSerTv = prm19.Value
                    LocMOTCAN = prm20.Value
                    Cortesia = prm21.Value
                    factura = prm22.Value


                End With
                Descuento = Me.DescuentoLabel2.Text
                'Clv_Vendedor = Me.ComboBox16.SelectedValue
                LocVendedorTv = Me.ComboBox16.Text
                SP_ConsultaRel_Contratos_Companias(contrato)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub guardabitacoratv()
        Try



            Dim valida1 As String = Nothing
            Dim valida2 As String = Nothing
            Dim valida3 As String = Nothing
            If OpcionCli = "M" And validacion > 0 Then


                'status = prm1.Value
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Status: ", status, Me.ComboBox1.SelectedValue, LocClv_Ciudad)
                'LocTVSINPAGO = prm2.Value
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Televisiones Sin Pago: ", LocTVSINPAGO, Me.TVSINPAGONumericUpDown.Value, LocClv_Ciudad)
                'LocTVCONPAGO = prm3.Value
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Televisiones Con Pago: ", LocTVCONPAGO, Me.TVCONPAGONumericUpDown.Value, LocClv_Ciudad)
                'ultimo_mes = prm4.Value

                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Ultimo Mes: ", ultimo_mes, Me.ULTIMO_MESTextBox.Text, LocClv_Ciudad)
                'ultimo_anio = prm5.Value
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Ultimo año: ", ultimo_anio, Me.ULTIMO_ANIOTextBox.Text, LocClv_Ciudad)
                'FECHA_ULT_PAGO = prm6.Value
                If FECHA_ULT_PAGO = "01/01/1900" Then FECHA_ULT_PAGO = ""
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Ultimo Pago: ", FECHA_ULT_PAGO, Me.FECHA_ULT_PAGOTextBox.Text, LocClv_Ciudad)
                'fecha_solicitud = prm7.Value
                If fecha_solicitud = "01/01/1900" Then fecha_solicitud = ""
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Solicitud: ", fecha_solicitud, Me.FECHA_SOLICITUDTextBox.Text, LocClv_Ciudad)
                'fecha_suspension = prm8.Value
                If fecha_suspension = "01/01/1900" Then fecha_suspension = ""
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Cancelacion: ", fecha_suspension, Me.FECHA_CANCELACIOTextBox.Text, LocClv_Ciudad)
                'fecha_Fuera_Area = prm9.Value
                If fecha_Fuera_Area = "01/01/1900" Then fecha_Fuera_Area = ""
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Fuera De Area: ", fecha_Fuera_Area, Me.FECHACANCOUTAREATextBox.Text, LocClv_Ciudad)
                'fecha_instalacio = prm10.Value
                If fecha_instalacio = "01/01/1900" Then fecha_instalacio = ""
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Instalacion: ", fecha_instalacio, Me.FECHA_INSTTextBox.Text, LocClv_Ciudad)
                'fecha_baja = prm11.Value
                If fecha_baja = "01/01/1900" Then fecha_baja = ""
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Corte: ", fecha_baja, Me.FECHA_CORTETextBox.Text, LocClv_Ciudad)
                ''LocFec_ULT_PAG_ANT = prm12.Value
                'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", Me.FECHA_CORTETextBox.Name, fecha_baja, Me.FECHA_CORTETextBox.Text, LocClv_Ciudad)
                'PrimerMensualidad = prm13.Value

                If Me.PRIMERMENSUALIDACheckBox.CheckState = CheckState.Checked Then
                    valida1 = "True"
                ElseIf Me.PRIMERMENSUALIDACheckBox.CheckState = CheckState.Unchecked Then
                    valida1 = "False"
                End If
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Primer Mensualidad: ", PrimerMensualidad, valida1, LocClv_Ciudad)

                'LocClv_MOTCAN = prm14.Value
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Motivo de Cancelacion: ", LocClv_MOTCAN, Me.MOTCANComboBox.SelectedValue, LocClv_Ciudad)
                'LocPRIMERMESANT = prm15.Value
                'LocClv_TipoServicioTV = prm16.Value
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Servicio: ", LocClv_TipoServicioTV, Me.TipSerTvComboBox.SelectedValue, LocClv_Ciudad)
                'LocpuntosAcumulados = prm17.Value
                'Obs = prm18.Value
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Observaciones: ", Obs, Me.ObsTextBox.Text, LocClv_Ciudad)
                'LocTipSerTv = prm19.Value
                'LocMOTCAN = prm20.Value
                'Cortesia = prm21.Value
                If Me.CortesiaCheckBox.CheckState = CheckState.Checked Then
                    valida2 = "True"
                ElseIf Me.CortesiaCheckBox.CheckState = CheckState.Unchecked Then
                    valida2 = "False"
                End If
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Cortesia: ", Cortesia, valida2, LocClv_Ciudad)
                'factura = prm22.Value
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Factura: ", factura, Me.FacturaTextBox1.Text, LocClv_Ciudad)
                'Descuento = Me.DescuentoLabel2.Text
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Descuento: ", Descuento, Me.DescuentoLabel2.Text, LocClv_Ciudad)
                'Clv_Vendedor = Me.ComboBox16.SelectedValue
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Vendedor: ", LocVendedorTv, Me.ComboBox16.Text, LocClv_Ciudad)

            End If

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub damedatosdig(ByVal clv_unicanet As Integer)
        Try
            If OpcionCli = "M" Then

                Dim comando As New SqlClient.SqlCommand
                Dim con90 As New SqlConnection(MiConexion)
                con90.Open()

                status = ""
                fecha_solicitud = ""
                fecha_instalacio = ""
                fecha_suspension = ""
                fecha_baja = ""
                fecha_Fuera_Area = ""
                FECHA_ULT_PAGO = ""
                PrimerMensualidad = False
                ultimo_mes = 0
                ultimo_anio = 0
                primerMesAnt = False
                statusAnt = ""
                facturaAnt = ""
                GENERAOSINSTA = False
                factura = ""
                Clv_Vendedor = 0
                Clv_Promocion = 0
                Email = ""
                Obs = ""
                DESCRIPCION = ""
                Cortesia = False



                comando = New SqlClient.SqlCommand

                With comando
                    .Connection = con90
                    .CommandText = "CONSULTACONTDIG_Oledb"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    '@CLV_UNICANET BIGINT, @status varchar(1) output, @fecha_solicitud varchar(20) output, @fecha_instalacio varchar(20) output, 
                    '@fecha_suspension varchar(20) output , @fecha_baja varchar(20) output, @fecha_Fuera_Area varchar(20) output, @FECHA_ULT_PAGO varchar(max) output , 
                    '@PrimerMensualidad bit output, @ultimo_mes int output, @ultimo_anio int output, @primerMesAnt bit output ,
                    ' @statusAnt varchar(50) output, @facturaAnt varchar(50) output, @GENERAOSINSTA bit output, @factura varchar(50) output,
                    ' @Clv_Vendedor int output, @Clv_Promocion int output, @Email varchar(50) output, @Obs varchar(250) output,
                    '@DESCRIPCION varchar(250) output,@Cortesia bit output

                    Dim prm As New SqlParameter("@CLV_UNICANET", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@status", SqlDbType.VarChar, 1)
                    Dim prm2 As New SqlParameter("@fecha_solicitud", SqlDbType.VarChar, 20)
                    Dim prm3 As New SqlParameter("@fecha_instalacio", SqlDbType.VarChar, 20)
                    Dim prm4 As New SqlParameter("@fecha_suspension", SqlDbType.VarChar, 20)
                    Dim prm5 As New SqlParameter("@fecha_baja", SqlDbType.VarChar, 20)
                    Dim prm6 As New SqlParameter("@fecha_Fuera_Area", SqlDbType.VarChar, 20)
                    Dim prm7 As New SqlParameter("@FECHA_ULT_PAGO", SqlDbType.VarChar, 20)
                    Dim prm8 As New SqlParameter("@PrimerMensualidad", SqlDbType.Bit)
                    Dim prm9 As New SqlParameter("@ultimo_mes", SqlDbType.Int)
                    Dim prm10 As New SqlParameter("@ultimo_anio", SqlDbType.Int)
                    Dim prm11 As New SqlParameter("@primerMesAnt", SqlDbType.Bit)
                    Dim prm12 As New SqlParameter("@statusAnt", SqlDbType.VarChar, 50)
                    Dim prm13 As New SqlParameter("@facturaAnt", SqlDbType.VarChar, 50)
                    Dim prm14 As New SqlParameter("@GENERAOSINSTA", SqlDbType.Bit)
                    Dim prm15 As New SqlParameter("@factura", SqlDbType.VarChar, 50)
                    Dim prm16 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
                    Dim prm17 As New SqlParameter("@Clv_Promocion", SqlDbType.Int)
                    Dim prm18 As New SqlParameter("@Email", SqlDbType.VarChar, 50)
                    Dim prm19 As New SqlParameter("@Obs", SqlDbType.VarChar, 250)
                    Dim prm20 As New SqlParameter("@DESCRIPCION", SqlDbType.VarChar, 250)
                    Dim prm21 As New SqlParameter("@Cortesia", SqlDbType.Bit)
                    Dim prm22 As New SqlParameter("@Motivo", SqlDbType.VarChar, 500)


                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm4.Direction = ParameterDirection.Output
                    prm5.Direction = ParameterDirection.Output
                    prm6.Direction = ParameterDirection.Output
                    prm7.Direction = ParameterDirection.Output
                    prm8.Direction = ParameterDirection.Output
                    prm9.Direction = ParameterDirection.Output
                    prm10.Direction = ParameterDirection.Output
                    prm11.Direction = ParameterDirection.Output
                    prm12.Direction = ParameterDirection.Output
                    prm13.Direction = ParameterDirection.Output
                    prm14.Direction = ParameterDirection.Output
                    prm15.Direction = ParameterDirection.Output
                    prm16.Direction = ParameterDirection.Output
                    prm17.Direction = ParameterDirection.Output
                    prm18.Direction = ParameterDirection.Output
                    prm19.Direction = ParameterDirection.Output
                    prm20.Direction = ParameterDirection.Output
                    prm21.Direction = ParameterDirection.Output
                    prm22.Direction = ParameterDirection.Output



                    prm.Value = clv_unicanet
                    prm1.Value = " "
                    prm2.Value = " "
                    prm3.Value = " "
                    prm4.Value = " "
                    prm5.Value = " "
                    prm6.Value = " "
                    prm7.Value = " "
                    prm8.Value = False
                    prm9.Value = 0
                    prm10.Value = 0
                    prm11.Value = False
                    prm12.Value = " "
                    prm13.Value = " "
                    prm14.Value = False
                    prm15.Value = " "
                    prm16.Value = 0
                    prm17.Value = 0
                    prm18.Value = " "
                    prm19.Value = " "
                    prm20.Value = " "
                    prm21.Value = False
                    prm22.Value = " "


                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    .Parameters.Add(prm10)
                    .Parameters.Add(prm11)
                    .Parameters.Add(prm12)
                    .Parameters.Add(prm13)
                    .Parameters.Add(prm14)
                    .Parameters.Add(prm15)
                    .Parameters.Add(prm16)
                    .Parameters.Add(prm17)
                    .Parameters.Add(prm18)
                    .Parameters.Add(prm19)
                    .Parameters.Add(prm20)
                    .Parameters.Add(prm21)
                    .Parameters.Add(prm22)


                    Dim i As Integer = comando.ExecuteNonQuery()
                    status = prm1.Value
                    fecha_solicitud = prm2.Value
                    fecha_instalacio = prm3.Value
                    fecha_suspension = prm4.Value
                    fecha_baja = prm5.Value
                    fecha_Fuera_Area = prm6.Value
                    FECHA_ULT_PAGO = prm7.Value
                    PrimerMensualidad = prm8.Value
                    ultimo_mes = prm9.Value
                    ultimo_anio = prm10.Value
                    primerMesAnt = prm11.Value
                    statusAnt = prm12.Value
                    facturaAnt = prm13.Value
                    GENERAOSINSTA = prm14.Value
                    factura = prm15.Value
                    Clv_Vendedor = prm16.Value
                    Clv_Promocion = prm17.Value
                    Email = prm18.Value
                    Obs = prm19.Value
                    DESCRIPCION = prm20.Value
                    Cortesia = prm21.Value
                    Me.Label48.Text = prm22.Value


                End With
                Descuento = Me.DescuentoLabel1.Text
                con90.Close()

            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub
    Private Sub damedatosnet2()
        If OpcionCli = "M" Then

            Locmarca = Me.MARCALabel1.Text
            LocTipoApar = Me.TIPOAPARATOLabel1.Text
            statusTarjeta = Me.ComboBox2.Text
            LocTipservcabl = Me.ComboBox3.Text
            LocTipoCablemodem = Me.ComboBox4.Text
            Obs = Me.ObsTextBox1.Text
            Activacion = Me.Fecha_ActivacionTextBox.Text
            Suspension = Me.Fecha_suspensionTextBox.Text
            LocTranspaso = Me.Fecha_TraspasoTextBox.Text
            Baja = Me.Fecha_bajaTextBox.Text

            If Me.Ventacablemodem1CheckBox.CheckState = CheckState.Checked Then
                Loc1pago = "True"
            ElseIf Me.Ventacablemodem1CheckBox.CheckState = CheckState.Unchecked Then
                Loc1pago = "False"
            End If

            If Me.Ventacablemodem2CheckBox.CheckState = CheckState.Checked Then
                Loc2pago = "True"
            ElseIf Me.Ventacablemodem2CheckBox.CheckState = CheckState.Unchecked Then
                Loc2pago = "False"
            End If

            If Me.SeRentaCheckBox.CheckState = CheckState.Checked Then
                serenta = "True"
            ElseIf Me.SeRentaCheckBox.CheckState = CheckState.Unchecked Then
                serenta = "False"
            End If

        End If
    End Sub
    Private Sub damedatosnet(ByVal clv_unicanet As Integer)
        Try
            If OpcionCli = "M" Then

                Dim comando As New SqlClient.SqlCommand
                Dim con90 As New SqlConnection(MiConexion)
                con90.Open()

                status = ""
                fecha_solicitud = ""
                fecha_instalacio = ""
                fecha_suspension = ""
                fecha_baja = ""
                fecha_Fuera_Area = ""
                FECHA_ULT_PAGO = ""
                PrimerMensualidad = False
                ultimo_mes = 0
                ultimo_anio = 0
                primerMesAnt = False
                statusAnt = ""
                facturaAnt = ""
                GENERAOSINSTA = False
                factura = ""
                Clv_Vendedor = 0
                Clv_Promocion = 0
                Email = ""
                Obs = ""
                DESCRIPCION = ""
                Cortesia = False



                comando = New SqlClient.SqlCommand

                With comando
                    .Connection = con90
                    .CommandText = "CONSULTACONTNET_Oledb"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    '@CLV_UNICANET BIGINT, @status varchar(1) output, @fecha_solicitud varchar(20) output, @fecha_instalacio varchar(20) output, 
                    '@fecha_suspension varchar(20) output , @fecha_baja varchar(20) output, @fecha_Fuera_Area varchar(20) output, @FECHA_ULT_PAGO varchar(max) output , 
                    '@PrimerMensualidad bit output, @ultimo_mes int output, @ultimo_anio int output, @primerMesAnt bit output ,
                    ' @statusAnt varchar(50) output, @facturaAnt varchar(50) output, @GENERAOSINSTA bit output, @factura varchar(50) output,
                    ' @Clv_Vendedor int output, @Clv_Promocion int output, @Email varchar(50) output, @Obs varchar(250) output,
                    '@DESCRIPCION varchar(250) output,@Cortesia bit output

                    Dim prm As New SqlParameter("@CLV_UNICANET", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@status", SqlDbType.VarChar, 1)
                    Dim prm2 As New SqlParameter("@fecha_solicitud", SqlDbType.VarChar, 20)
                    Dim prm3 As New SqlParameter("@fecha_instalacio", SqlDbType.VarChar, 20)
                    Dim prm4 As New SqlParameter("@fecha_suspension", SqlDbType.VarChar, 20)
                    Dim prm5 As New SqlParameter("@fecha_baja", SqlDbType.VarChar, 20)
                    Dim prm6 As New SqlParameter("@fecha_Fuera_Area", SqlDbType.VarChar, 20)
                    Dim prm7 As New SqlParameter("@FECHA_ULT_PAGO", SqlDbType.VarChar, 20)
                    Dim prm8 As New SqlParameter("@PrimerMensualidad", SqlDbType.Bit)
                    Dim prm9 As New SqlParameter("@ultimo_mes", SqlDbType.Int)
                    Dim prm10 As New SqlParameter("@ultimo_anio", SqlDbType.Int)
                    Dim prm11 As New SqlParameter("@primerMesAnt", SqlDbType.Bit)
                    Dim prm12 As New SqlParameter("@statusAnt", SqlDbType.VarChar, 50)
                    Dim prm13 As New SqlParameter("@facturaAnt", SqlDbType.VarChar, 50)
                    Dim prm14 As New SqlParameter("@GENERAOSINSTA", SqlDbType.Bit)
                    Dim prm15 As New SqlParameter("@factura", SqlDbType.VarChar, 50)
                    Dim prm16 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
                    Dim prm17 As New SqlParameter("@Clv_Promocion", SqlDbType.Int)
                    Dim prm18 As New SqlParameter("@Email", SqlDbType.VarChar, 50)
                    Dim prm19 As New SqlParameter("@Obs", SqlDbType.VarChar, 250)
                    Dim prm20 As New SqlParameter("@DESCRIPCION", SqlDbType.VarChar, 250)
                    Dim prm21 As New SqlParameter("@Cortesia", SqlDbType.Bit)
                    Dim prm22 As New SqlParameter("@motivo", SqlDbType.VarChar, 500)



                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm4.Direction = ParameterDirection.Output
                    prm5.Direction = ParameterDirection.Output
                    prm6.Direction = ParameterDirection.Output
                    prm7.Direction = ParameterDirection.Output
                    prm8.Direction = ParameterDirection.Output
                    prm9.Direction = ParameterDirection.Output
                    prm10.Direction = ParameterDirection.Output
                    prm11.Direction = ParameterDirection.Output
                    prm12.Direction = ParameterDirection.Output
                    prm13.Direction = ParameterDirection.Output
                    prm14.Direction = ParameterDirection.Output
                    prm15.Direction = ParameterDirection.Output
                    prm16.Direction = ParameterDirection.Output
                    prm17.Direction = ParameterDirection.Output
                    prm18.Direction = ParameterDirection.Output
                    prm19.Direction = ParameterDirection.Output
                    prm20.Direction = ParameterDirection.Output
                    prm21.Direction = ParameterDirection.Output
                    prm22.Direction = ParameterDirection.Output



                    prm.Value = clv_unicanet
                    prm1.Value = " "
                    prm2.Value = " "
                    prm3.Value = " "
                    prm4.Value = " "
                    prm5.Value = " "
                    prm6.Value = " "
                    prm7.Value = " "
                    prm8.Value = False
                    prm9.Value = 0
                    prm10.Value = 0
                    prm11.Value = False
                    prm12.Value = " "
                    prm13.Value = " "
                    prm14.Value = False
                    prm15.Value = " "
                    prm16.Value = 0
                    prm17.Value = 0
                    prm18.Value = " "
                    prm19.Value = " "
                    prm20.Value = " "
                    prm21.Value = False
                    prm22.Value = " "

                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    .Parameters.Add(prm10)
                    .Parameters.Add(prm11)
                    .Parameters.Add(prm12)
                    .Parameters.Add(prm13)
                    .Parameters.Add(prm14)
                    .Parameters.Add(prm15)
                    .Parameters.Add(prm16)
                    .Parameters.Add(prm17)
                    .Parameters.Add(prm18)
                    .Parameters.Add(prm19)
                    .Parameters.Add(prm20)
                    .Parameters.Add(prm21)
                    .Parameters.Add(prm22)


                    Dim i As Integer = comando.ExecuteNonQuery()
                    status = prm1.Value
                    fecha_solicitud = prm2.Value
                    fecha_instalacio = prm3.Value
                    fecha_suspension = prm4.Value
                    fecha_baja = prm5.Value
                    fecha_Fuera_Area = prm6.Value
                    FECHA_ULT_PAGO = prm7.Value
                    PrimerMensualidad = prm8.Value
                    ultimo_mes = prm9.Value
                    ultimo_anio = prm10.Value
                    primerMesAnt = prm11.Value
                    statusAnt = prm12.Value
                    facturaAnt = prm13.Value
                    GENERAOSINSTA = prm14.Value
                    factura = prm15.Value
                    Clv_Vendedor = prm16.Value
                    Clv_Promocion = prm17.Value
                    Email = prm18.Value
                    Obs = prm19.Value
                    DESCRIPCION = prm20.Value
                    Cortesia = prm21.Value
                    Me.Label49.Text = prm22.Value

                End With
                Descuento = Me.DescuentoLabel1.Text
                con90.Close()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub guardabitacoradig(ByVal op As Integer)
        Try



            Dim CON10 As New SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand
            If OpcionCli = "M" Then
                Dim valida1 As String = Nothing
                Dim Valida2 As String = Nothing
                Dim Valida3 As String = Nothing
                Dim valida4 As String = Nothing
                Dim valida5 As String = Nothing

                Select Case op
                    Case 0

                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LoContratonet
                            prm1.Value = 0
                            prm2.Value = 3
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Servicio Digital Al Equipo: " + NombreMAC, " ", "Se agrego el Paquete Digital:" & GLoPaqueteAgrega, LocClv_Ciudad)

                        LocGloContratoNet = 0
                    Case 1
                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = 0
                            prm1.Value = loc_Clv_InicaDig
                            prm2.Value = 4
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()


                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino Un Servicio Digital Al Equipo: " + NombreMAC, " ", "Se Elimino el Paquete Digital:" & NombrePaqueteElimino, LocClv_Ciudad)

                    Case 2
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino el Equipo Digital: " + NombrePaqueteElimino, " ", "Se Elimino el Equipo", LocClv_Ciudad)
                    Case 3

                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LoContratonet
                            prm1.Value = 0
                            prm2.Value = 3
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Equipo Digital: " + NombreMAC, " ", "Se Agrego Un Equipo Digital", LocClv_Ciudad)

                    Case 4

                        'ContDig
                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = 0
                            prm1.Value = LocGloClv_unianet
                            prm2.Value = GloOp
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()


                        cmd = New SqlClient.SqlCommand
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Servicio_Cab_Deco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure
                            '@clv_unicanet bigint, @op int,@Paquete varchar(max) output
                            Dim prm As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm2 As New SqlParameter("@Paquete", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Output

                            prm.Value = LocGloClv_unianet
                            prm1.Value = GloOp
                            prm2.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombrePaqueteElimino = prm2.Value

                        End With
                        CON10.Close()


                        'status = prm1.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Status: ", status, Me.ComboBox10.SelectedValue, LocClv_Ciudad)
                        'fecha_solicitud = prm2.Value
                        If fecha_solicitud = "01/01/1900" Then fecha_solicitud = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha de solicitud: ", fecha_solicitud, Me.TextBox16.Text, LocClv_Ciudad)
                        'fecha_instalacio = prm3.Value
                        If fecha_instalacio = "01/01/1900" Then fecha_instalacio = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha de instalacion: ", fecha_instalacio, Me.TextBox15.Text, LocClv_Ciudad)
                        'fecha_suspension = prm4.Value
                        If fecha_suspension = "01/01/1900" Then fecha_suspension = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha de suspension: ", fecha_suspension, Me.TextBox14.Text, LocClv_Ciudad)
                        'fecha_baja = prm5.Value
                        If fecha_baja = "01/01/1900" Then fecha_baja = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Baja: ", fecha_baja, Me.TextBox13.Text, LocClv_Ciudad)
                        'fecha_Fuera_Area = prm6.Value
                        If fecha_Fuera_Area = "01/01/1900" Then fecha_Fuera_Area = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha de Fuera Area: ", fecha_Fuera_Area, Me.TextBox12.Text, LocClv_Ciudad)
                        'FECHA_ULT_PAGO = prm7.Value
                        If FECHA_ULT_PAGO = "01/01/1900" Then FECHA_ULT_PAGO = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Ultimo Pago: ", FECHA_ULT_PAGO, Me.TextBox11.Text, LocClv_Ciudad)
                        'PrimerMensualidad = prm8.Value
                        If Me.CheckBox1.CheckState = CheckState.Checked Then
                            valida1 = "True"
                        ElseIf Me.CheckBox1.CheckState = CheckState.Unchecked Then
                            valida1 = "False"
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Primer Mensualidad: ", PrimerMensualidad, valida1, LocClv_Ciudad)
                        'ultimo_mes = prm9.Value
                        If Me.TextBox3.Text = "" Or IsNumeric(Me.TextBox10.Text) = False Then
                            Valida2 = "0"
                        Else
                            Valida2 = Me.TextBox10.Text
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Ultimo mes: ", ultimo_mes, Valida2, LocClv_Ciudad)
                        'ultimo_anio = prm10.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Ultimo año: ", ultimo_anio, Me.TextBox9.Text, LocClv_Ciudad)
                        'primerMesAnt = prm11.Value
                        'statusAnt = prm12.Value
                        'facturaAnt = prm13.Value
                        'GENERAOSINSTA = prm14.Value
                        'factura = prm15.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Factura: ", factura, Me.TextBox8.Text, LocClv_Ciudad)
                        'Clv_Vendedor = prm16.Value
                        If Me.ComboBox9.Text = "" Then
                            Valida3 = "0"
                        Else
                            Valida3 = CStr(Me.ComboBox9.SelectedValue)
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Vendedor: ", Clv_Vendedor, Valida3, LocClv_Ciudad)
                        'Clv_Promocion = prm17.Value
                        If Me.ComboBox8.Text = "" Then
                            valida4 = "0"
                        Else
                            valida4 = CStr(Me.ComboBox8.SelectedValue)
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Promocion: ", Clv_Promocion, valida4, LocClv_Ciudad)
                        'Email = prm18.Value
                        'Obs = prm19.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Observaciones: ", Obs, Me.TextBox6.Text, LocClv_Ciudad)
                        'DESCRIPCION = prm20.Value
                        'Cortesia = prm21.Value
                        If Me.CortesiaCheckBox.CheckState = CheckState.Checked Then
                            valida5 = "True"
                        ElseIf Me.CortesiaCheckBox.CheckState = CheckState.Unchecked Then
                            valida5 = "False"
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Cortesia: ", Cortesia, valida5, LocClv_Ciudad)
                        'descuento
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Descuento: ", Descuento, Me.DescuentoLabel1.Text, LocClv_Ciudad)
                    Case 5
                        'ClientesDig

                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LocGloContratoNet
                            prm1.Value = 0
                            prm2.Value = GloOp
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        'statusTarjeta = Me.ComboBox13.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Status: ", statusTarjeta, Me.ComboBox13.Text, LocClv_Ciudad)
                        'Activacion = Me.TextBox33.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Fecha de Activacion: ", Activacion, Me.TextBox33.Text, LocClv_Ciudad)
                        'Suspension = Me.TextBox32.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Fecha De Suspension: ", Suspension, Me.TextBox32.Text, LocClv_Ciudad)
                        'Baja = Me.TextBox23.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Fecha  De  Baja: ", Baja, Me.TextBox23.Text, LocClv_Ciudad)
                        'Obs = Me.TextBox29.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Observaciones: ", Obs, Me.TextBox29.Text, LocClv_Ciudad)
                        'serenta
                        If Me.CheckBox2.CheckState = CheckState.Checked Then
                            valida1 = "True"
                        ElseIf Me.CheckBox2.CheckState = CheckState.Unchecked Then
                            valida1 = "False"
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Se Renta: ", serenta, valida1, LocClv_Ciudad)

                End Select
            ElseIf OpcionCli = "N" Then
                Select Case op
                    Case 0
                        LoContratonet = LocGloContratoNet
                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LoContratonet
                            prm1.Value = 0
                            prm2.Value = 3
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Servicio Digital Al Equipo: " + NombreMAC, " ", "Se agrego el Paquete Digital:" & GLoPaqueteAgrega, LocClv_Ciudad)

                    Case 1
                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = 0
                            prm1.Value = loc_Clv_InicaDig
                            prm2.Value = 4
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()


                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino Un Servicio Digital Al Equipo: " + NombreMAC, " ", "Se Elimino el Paquete Digital:" & NombrePaqueteElimino, LocClv_Ciudad)


                    Case 2
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino un Equipo Digital: " + NombrePaqueteElimino, " ", "Se Elimino un Equipo Digital. ", LocClv_Ciudad)
                    Case 3
                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LoContratonet
                            prm1.Value = 0
                            prm2.Value = 3
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Equipo Digital: " + NombreMAC, " ", "Se Agrego Un Nuevo Equipo Digital", LocClv_Ciudad)

                End Select
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub guardabitacoranet(ByVal op As Integer)
        Try



            Dim CON10 As New SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand()

            If OpcionCli = "M" Then
                Dim valida1 As String = Nothing
                Dim Valida2 As String = Nothing
                Dim Valida3 As String = Nothing
                Dim valida4 As String = Nothing
                Dim valida5 As String = Nothing

                Select Case op
                    'Case 0
                    '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Paquete Digital", " ", "Se agrego el Paquete Digital:" & GLoPaqueteAgrega, LocClv_Ciudad)
                    'Case 1
                    '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino un paquete Digital", " ", "Se Elimino el Paquete Digital:" & NombrePaqueteElimino, LocClv_Ciudad)
                    Case 2


                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = 0
                            prm1.Value = LocGloClv_unianet
                            prm2.Value = GloOp
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()


                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino Un Servicio De Internet: " + NombreMAC, " ", "Se Elimino El Servicio de Itnernet : " & NombrePaqueteElimino, LocClv_Ciudad)
                        'Case 3
                        '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Equipo Digital", " ", "Se Agrego Un Equipo Digital Con El ContratoNEt:" & GloContratoDig_Nuevo, LocClv_Ciudad)
                    Case 4
                        'COntNet

                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = 0
                            prm1.Value = LocGloClv_unianet
                            prm2.Value = GloOp
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        cmd = New SqlClient.SqlCommand
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Servicio_Cab_Deco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure
                            '@clv_unicanet bigint, @op int,@Paquete varchar(max) output
                            Dim prm As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm2 As New SqlParameter("@Paquete", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Output

                            prm.Value = LocGloClv_unianet
                            prm1.Value = GloOp
                            prm2.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombrePaqueteElimino = prm2.Value

                        End With
                        CON10.Close()

                        'status = prm1.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Status: ", status, Me.ComboBox5.SelectedValue, LocClv_Ciudad)
                        'fecha_solicitud = prm2.Value
                        If fecha_solicitud = "01/01/1900" Then fecha_solicitud = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De solicitud: ", fecha_solicitud, Me.Fecha_solicitudTextBox1.Text, LocClv_Ciudad)
                        'fecha_instalacio = prm3.Value
                        If fecha_instalacio = "01/01/1900" Then fecha_instalacio = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Instalacion: ", fecha_instalacio, Me.Fecha_instalacioTextBox.Text, LocClv_Ciudad)
                        'fecha_suspension = prm4.Value
                        If fecha_suspension = "01/01/1900" Then fecha_suspension = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Suspension: ", fecha_suspension, Me.Fecha_suspensionTextBox.Text, LocClv_Ciudad)
                        'fecha_baja = prm5.Value
                        If fecha_baja = "01/01/1900" Then fecha_baja = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Baja: ", fecha_baja, Me.Fecha_bajaTextBox.Text, LocClv_Ciudad)
                        'fecha_Fuera_Area = prm6.Value
                        If fecha_Fuera_Area = "01/01/1900" Then fecha_Fuera_Area = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Fuera De Area: ", fecha_Fuera_Area, Me.Fecha_Fuera_AreaTextBox.Text, LocClv_Ciudad)
                        'FECHA_ULT_PAGO = prm7.Value
                        If FECHA_ULT_PAGO = "01/01/1900" Then FECHA_ULT_PAGO = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Ultimo Pago: ", FECHA_ULT_PAGO, Me.FECHA_ULT_PAGOTextBox1.Text, LocClv_Ciudad)
                        'PrimerMensualidad = prm8.Value
                        If Me.PrimerMensualidadCheckBox.CheckState = CheckState.Checked Then
                            valida1 = "True"
                        ElseIf Me.PrimerMensualidadCheckBox.CheckState = CheckState.Unchecked Then
                            valida1 = "False"
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Primer Mensualidad: ", PrimerMensualidad, valida1, LocClv_Ciudad)
                        'ultimo_mes = prm9.Value
                        If Me.Ultimo_mesTextBox1.Text = "0" Or IsNumeric(Me.Ultimo_mesTextBox1.Text) = False Then
                            Valida2 = "0"
                        Else
                            Valida2 = Me.Ultimo_mesTextBox1.Text
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Ultimo mes: ", ultimo_mes, Valida2, LocClv_Ciudad)
                        'ultimo_anio = prm10.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Ultimo año: ", ultimo_anio, Me.Ultimo_anioTextBox1.Text, LocClv_Ciudad)
                        'primerMesAnt = prm11.Value
                        'statusAnt = prm12.Value
                        'facturaAnt = prm13.Value
                        'GENERAOSINSTA = prm14.Value
                        'factura = prm15.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Factura: ", factura, Me.FacturaTextBox.Text, LocClv_Ciudad)
                        'Clv_Vendedor = prm16.Value
                        If Me.ComboBox6.Text = "" Then
                            Valida3 = "0"
                        Else
                            Valida3 = CStr(Me.ComboBox6.SelectedValue)
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Vendedor: ", Clv_Vendedor, Valida3, LocClv_Ciudad)
                        'Clv_Promocion = prm17.Value
                        'If Me.ComboBox8.Text = "" Then
                        '    valida4 = "0"
                        'Else
                        '    valida4 = CStr(Me.ComboBox8.SelectedValue)
                        'End If
                        'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Clv_Promocion" + "-" + NombrePaqueteElimino, Clv_Promocion, valida4, LocClv_Ciudad)
                        'Email = prm18.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Email: ", Email, Me.EmailTextBox1.Text, LocClv_Ciudad)
                        'Obs = prm19.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Observaciones: ", Obs, Me.ObsTextBox2.Text, LocClv_Ciudad)
                        'DESCRIPCION = prm20.Value
                        'Cortesia = prm21.Value
                        If Me.CortesiaCheckBox2.CheckState = CheckState.Checked Then
                            valida5 = "True"
                        ElseIf Me.CortesiaCheckBox2.CheckState = CheckState.Unchecked Then
                            valida5 = "False"
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Cortesia: ", Cortesia, valida5, LocClv_Ciudad)
                        'descuento
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Descuento: ", Descuento, Me.DescuentoLabel3.Text, LocClv_Ciudad)
                    Case 5

                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LocGloContratoNet
                            prm1.Value = 0
                            prm2.Value = GloOp
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()


                        'Locmarca = Me.MARCALabel1.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Marca: ", Locmarca, Me.MARCALabel1.Text, LocClv_Ciudad)
                        'LocTipoApar = Me.TIPOAPARATOLabel1.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Tipo de Aparato: ", LocTipoApar, Me.TIPOAPARATOLabel1.Text, LocClv_Ciudad)
                        'statusTarjeta = Me.ComboBox2.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Status: ", statusTarjeta, Me.ComboBox2.Text, LocClv_Ciudad)
                        'LocTipservcabl = Me.ComboBox3.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Tipo Servicio : ", LocTipservcabl, Me.ComboBox3.Text, LocClv_Ciudad)
                        'LocTipoCablemodem = Me.ComboBox4.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Tipo de Cablemodem: ", LocTipoCablemodem, Me.ComboBox4.Text, LocClv_Ciudad)
                        'Obs = Me.ObsTextBox1.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Observaciones: ", Obs, Me.ObsTextBox1.Text, LocClv_Ciudad)
                        'Activacion = Me.Fecha_ActivacionTextBox.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha De Activacion: ", Activacion, Me.Fecha_ActivacionTextBox.Text, LocClv_Ciudad)
                        'Suspension = Me.Fecha_suspensionTextBox.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha de Supension: ", Suspension, Me.Fecha_suspensionTextBox.Text, LocClv_Ciudad)
                        'LocTranspaso = Me.Fecha_TraspasoTextBox.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha De Transpaso: ", LocTranspaso, Me.Fecha_TraspasoTextBox.Text, LocClv_Ciudad)
                        'Baja = Me.Fecha_bajaTextBox.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha De Baja: ", Baja, Me.Fecha_bajaTextBox.Text, LocClv_Ciudad)

                        '1er Pago
                        If Me.Ventacablemodem1CheckBox.CheckState = CheckState.Checked Then
                            valida1 = "True"
                        ElseIf Me.Ventacablemodem1CheckBox.CheckState = CheckState.Unchecked Then
                            valida1 = "False"
                        End If

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Se Vende El Cablemodem 1er Pago: ", Loc1pago, valida1, LocClv_Ciudad)

                        '2do pago Loc2pago

                        If Me.Ventacablemodem2CheckBox.CheckState = CheckState.Checked Then
                            Valida2 = "True"
                        ElseIf Me.Ventacablemodem2CheckBox.CheckState = CheckState.Unchecked Then
                            Valida2 = "False"
                        End If

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Se Vende El Cablemodem 2do Pago: ", Loc2pago, Valida2, LocClv_Ciudad)

                        'Se Renta serenta
                        If Me.SeRentaCheckBox.CheckState = CheckState.Checked Then
                            Valida3 = "True"
                        ElseIf Me.SeRentaCheckBox.CheckState = CheckState.Unchecked Then
                            Valida3 = "False"
                        End If

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Se Renta El Cablemodem: ", serenta, Valida3, LocClv_Ciudad)
                End Select
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub
    Private Sub guardabitacora()
        Try



            If OpcionCli = "M" Then

                Dim solo_internet As String = Nothing
                Dim desglosa_iva As String = Nothing
                Dim clv_loc As String = Nothing
                'If bndbitacora = True Then
                '    bndbitacora = False
                '    dame_datos()
                'End If


                'Validaciones de nulos
                If IsDBNull(nombre_cliente) = True Then
                    nombre_cliente = ""
                End If
                If IsDBNull(Clv_Callebit) = True Then
                    Clv_Callebit = 0
                End If
                If IsDBNull(NUMERO_casabit) = True Then
                    NUMERO_casabit = ""
                End If
                If IsDBNull(CodigoPostalbit) = True Then
                    CodigoPostalbit = ""
                End If

                If IsDBNull(ENTRECALLESbit) = True Then
                    ENTRECALLESbit = ""
                End If

                If IsDBNull(Clv_Coloniabit) = True Then
                    Clv_Coloniabit = 0
                End If

                If IsDBNull(Clv_Periodobit) = True Then
                    Clv_Periodobit = 0
                End If

                If IsDBNull(clv_Ciudadbit) = True Then
                    clv_Ciudadbit = 0
                End If

                If IsDBNull(clv_sectorbit) = True Then
                    clv_sectorbit = 0
                End If

                'If IsDBNull(Me.ComboBox7.Tag) = True Then
                '    Me.ComboBox7.Tag = 0
                'End If    Tipo de Pago

                If IsDBNull(SoloInternetbit) = True Then
                    SoloInternetbit = 0
                End If

                If IsDBNull(DESGLOSA_Ivabit) = True Then
                    DESGLOSA_Ivabit = 0
                End If

                If IsDBNull(TELEFONObit) = True Then
                    TELEFONObit = ""
                End If
                If IsDBNull(CELULARbit) = True Then
                    CELULARbit = ""
                End If
                If IsDBNull(Email) = True Then
                    Email = ""
                End If

                If Me.SoloInternetCheckBox.CheckState = CheckState.Checked Then
                    solo_internet = "True"
                Else
                    solo_internet = "False"
                End If

                If Me.DESGLOSA_IvaCheckBox.CheckState = CheckState.Checked Then
                    desglosa_iva = "True"
                Else
                    desglosa_iva = "False"
                End If

                If Me.ComboBox11.Text = "" Then
                    clv_loc = "0"
                Else
                    clv_loc = CStr(Me.ComboBox11.SelectedValue)

                End If


                ' fin de validacion nulos
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + " Nombre:", nombre_cliente, Me.NOMBRETextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Calle:", Clv_Callebit, CStr(Me.CALLEComboBox.SelectedValue), LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Numero:", NUMERO_casabit, Me.NUMEROTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Código Postal:", CodigoPostalbit, Me.CodigoPostalTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Entre Calles:", ENTRECALLESbit, Me.ENTRECALLESTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Colonia:", Clv_Coloniabit, CStr(Me.COLONIAComboBox.SelectedValue), LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Periodo de corte:", Clv_Periodobit, Me.ComboBox15.SelectedValue, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Ciudad: ", clv_Ciudadbit, Me.CIUDADComboBox.SelectedValue, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Clave De Localizacion:", clv_sectorbit, clv_loc, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Tipo De Pago:", clv_tipo_pagobit, Me.ComboBox7.SelectedValue, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Solo Internet: ", SoloInternetbit, solo_internet, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Desglosa Iva:", DESGLOSA_Ivabit, desglosa_iva, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Telefono: ", TELEFONObit, Me.TELEFONOTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Celular: ", CELULARbit, Me.CELULARTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Email: ", Email, Me.EmailTextBox.Text, LocClv_Ciudad)

            ElseIf OpcionCli = "N" Then
                If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Capturaron Los Datos Generales Del Cliente", " ", "Se Capturo Los Datos Generales Del Cliente", LocClv_Ciudad)
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub BindingNavigatorAddNewItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub



    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONSULTARCLIENTEBindingSource.CancelEdit()
    End Sub




    Private Sub CALLEComboBox_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CALLEComboBox.SelectedIndexChanged
        'If CALLEComboBox.SelectedValue = 0 Then
        '    Exit Sub
        'End If
        If IsNumeric(Me.CALLEComboBox.SelectedValue) = True Then
            If LocClv_Calletmp <> Me.CALLEComboBox.SelectedValue Then
                Me.asiganacalle()
                BndDClientes = True
                Try
                    If IsNumeric(Contrato) = False Then Contrato = 0
                    Me.uspConsultaTap(Contrato, CInt(Me.CALLEComboBox.SelectedValue), CInt(Me.COLONIAComboBox.SelectedValue))
                    uspDameTapCliente(CLng(Contrato))
                Catch ex As Exception

                End Try
            End If
        End If
        'Llena_companias_nuevo()
    End Sub
    Dim bndServicio As Integer = 0
    Private Sub COLONIAComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles COLONIAComboBox.SelectedIndexChanged
        'If COLONIAComboBox.SelectedValue = 0 Then
        '    Exit Sub
        'End If
        Me.asignacolonia()
        Try
            'Me.uspConsultaTap(Contrato, CInt(Me.CALLEComboBox.SelectedValue), CInt(Me.COLONIAComboBox.SelectedValue))
            'uspDameTapCliente(CLng(Contrato))
            DameSector()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, COLONIAComboBox.SelectedValue)
            CALLEComboBox.DataSource = BaseII.ConsultaDT("MuestraCalleColonia")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, COLONIAComboBox.SelectedValue)
            BaseII.CreateMyParameter("@Clv_Localidad", SqlDbType.Int, ComboBoxLocalidad.SelectedValue)
            BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, CIUDADComboBox.SelectedValue)
            BaseII.CreateMyParameter("@codigoPostal", ParameterDirection.Output, SqlDbType.VarChar, 10)
            BaseII.CreateMyParameter("@tieneservicio", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("muestraCP")
            CodigoPostalTextBox.Text = BaseII.dicoPar("@codigoPostal")
            bndServicio = BaseII.dicoPar("@tieneservicio")
            If CALLEComboBox.Items.Count = 0 Then
                CALLEComboBox.Text = ""
            End If
            If OpcionCli <> "N" And CALLEComboBox.Items.Count > 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@clvopcion", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
                BaseII.ProcedimientoOutPut("DameECCCCliente")
                CALLEComboBox.SelectedValue = BaseII.dicoPar("@clvopcion")
            End If
            If OpcionCli = "N" And CapturaCalle = True Then
                CALLEComboBox.Text = ""
            End If
            GloClvColonia = COLONIAComboBox.SelectedValue
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub DameSector()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, COLONIAComboBox.SelectedValue)
            ComboSector.DataSource = BaseII.ConsultaDT("MuestraSectoresNew")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub CIUDADComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CIUDADComboBox.SelectedIndexChanged
        Me.asignaciudad()
        'Llena_companias_nuevo()
        Try
            If CIUDADComboBox.Items.Count = 0 Then
                Exit Sub
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, CIUDADComboBox.SelectedValue)
            ComboBoxLocalidad.DataSource = BaseII.ConsultaDT("MuestraLocalidadCiudad")
            If ComboBoxLocalidad.Items.Count > 0 And opcion = "N" Then
                ComboBoxLocalidad.SelectedIndex = 0
            End If
            If OpcionCli <> "N" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@clvopcion", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
                BaseII.ProcedimientoOutPut("DameECCCCliente")
                ComboBoxLocalidad.SelectedValue = BaseII.dicoPar("@clvopcion")
            End If
            GloClvCiudad = CIUDADComboBox.SelectedValue
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Esto lo puso Eric
        GloPermisoCortesia = 0
        eResValida = 0
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then Me.CONTRATOTextBox.Text = 0
        ValidaDatosBancarios(CInt(Me.CONTRATOTextBox.Text), CLng(Me.ComboBox7.SelectedValue))
        'Genera_Ordenes_Logic(CInt(Me.CONTRATOTextBox.Text))
        If eResValida = 1 Then
            MsgBox(eMsgValida, MsgBoxStyle.Exclamation)
        End If

        Me.Close()
    End Sub

    Private Sub Genera_Ordenes_Logic(ByVal Contrato As Long)
        If IdSistema = "LO" Or IdSistema = "YU" Then
            If OpcionCli = "N" Or OpcionCli = "M" Then
                Dim con As New SqlConnection(MiConexion)
                Dim cmd As New SqlCommand()
                Try
                    cmd = New SqlCommand()
                    con.Open()
                    With cmd
                        .CommandText = "Genera_Ordenes"
                        .Connection = con
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure

                        Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = Contrato
                        .Parameters.Add(prm)

                        Dim ia As Integer = .ExecuteNonQuery()
                    End With
                    con.Close()
                Catch ex As Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try
            End If
        End If
    End Sub
    Private Sub SoloInternetCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SoloInternetCheckBox.CheckedChanged
        If Me.SoloInternetCheckBox.Checked = True And Me.SoloInternetCheckBox.Enabled = True And Me.SoloInternetCheckBox.Tag <> "N" Then
            If IsNumeric(Me.DigitalTextBox.Text) = False Then Me.DigitalTextBox.Text = 0
            If IsNumeric(Me.BasicoTextBox.Text) = False Then Me.BasicoTextBox.Text = 0

            If Me.DigitalTextBox.Text > 0 Or Me.BasicoTextBox.Text > 0 Then
                'MsgBox("No Puede ser un Cliente de Solo Internet ya tiene otros Servicios ", MsgBoxStyle.Information)

                VALIDASoloInternet(CONTRATOTextBox.Text)
                If eMsj.Length > 0 Then
                    MessageBox.Show(eMsj)
                    Me.SoloInternetCheckBox.Checked = False
                    Exit Sub
                End If

            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
            BaseII.CreateMyParameter("@solo", SqlDbType.Int, SoloInternetCheckBox.CheckState)
            BaseII.Inserta("sp_guardaSoloInternet")
            If SoloInternetCheckBox.Checked Then
                bitsist(GloUsuario, Contrato, "Softv", "Clientes", "Cambio de Solo Internet", "", "Cambio de Cliente Normal a Solo Internet", "AG")
            Else
                bitsist(GloUsuario, Contrato, "Softv", "Clientes", "Cambio de Solo Internet", "", "Cambio de Solo Internet a Cliente Normal", "AG")
            End If
            Me.Button8.Visible = True
            Me.Button8.Text = "&Internet"
            Me.Button7.Visible = False
            Me.Button11.Visible = False

            'Me.Panel2.Visible = False
            'Me.Panel7.Visible = False
            'Me.Panel4.Visible = True

            Me.Panel2.Hide()
            Me.Panel4.Hide()
            Me.Panel7.Hide()
            'Contrato = Me.CONTRATOTextBox.Text
            'frmctr.MdiParent = Me
            'frmctr.WindowState = FormWindowState.Normal
            'frmctr.Show()
            'frmInternet2.Show()
            'frmInternet2.Panel5.Enabled = False
            'frmInternet2.Panel6.Enabled = False
            'frmctr.TreeView1.ExpandAll()

            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmInternet2.Panel5.Enabled = False
            frmInternet2.Panel6.Enabled = False
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Button9.Enabled = True
            'Me.SplitContainer1.Enabled = True
            'Me.Button11.Visible = False
        ElseIf Me.SoloInternetCheckBox.Enabled = False And (Me.Button8.Text.Contains("&Televis") Or Me.Button8.Text = "IPTV" Or Me.Button8.Text = "&Premium") = False And Me.SoloInternetCheckBox.Tag <> "N" Then
            Me.Button8.Visible = True
            Me.Button8.Text = "&Internet"
            Me.Button7.Visible = False
            Me.Button11.Visible = False
            'Me.Panel2.Visible = False
            'Me.Panel7.Visible = False
            'Me.Panel4.Visible = True
            Me.SplitContainer1.Enabled = True
            'Me.Button11.Visible = False
            Me.Panel2.Hide()
            Me.Panel4.Hide()
            Me.Panel7.Hide()
            'Contrato = Me.CONTRATOTextBox.Text
            frmctr.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmctr.Show()
            frmctr.TreeView1.ExpandAll()
        ElseIf Me.SoloInternetCheckBox.Tag <> "N" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
            BaseII.CreateMyParameter("@solo", SqlDbType.Int, SoloInternetCheckBox.CheckState)
            BaseII.Inserta("sp_guardaSoloInternet")
            If SoloInternetCheckBox.Checked Then
                bitsist(GloUsuario, Contrato, "Softv", "Clientes", "Cambio de Solo Internet", "", "Cambio de Cliente Normal a Solo Internet", "AG")
            Else
                bitsist(GloUsuario, Contrato, "Softv", "Clientes", "Cambio de Solo Internet", "", "Cambio de Solo Internet a Cliente Normal", "AG")
            End If
            'Me.SoloInternetCheckBox.Checked
            Me.HABILITA_CTRS()
        End If
    End Sub

    Public Sub CREAARBOL()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''
            'se comento inicio
            'Dim CON2 As New SqlConnection(MiConexion)
            'CON2.Open()
            ''Dim customerOrders As DataSet = New DataSet()
            ''custAdapter.Fill(customerOrders, "Customers")
            '' 
            ''Dim pRow, cRow As DataRow
            ''For Each pRow In customerOrders.Tables("Customers").Rows
            '' Console.WriteLine(pRow("CustomerID").ToString())
            ''Next
            'Me.HaberServicios_CliTableAdapter.Connection = CON2
            'Me.HaberServicios_CliTableAdapter.Fill(Me.NewSofTvDataSet.HaberServicios_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))
            'Me.MUESTRACABLEMODEMSDELCLITableAdapter.Connection = CON2
            'Me.MUESTRACABLEMODEMSDELCLITableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI, New System.Nullable(Of Long)(CType(Contrato, Long)))
            'Dim FilaRow As DataRow
            'Dim FilacontNet As DataRow
            'Me.TreeView1.Nodes.Clear()
            'For Each FilaRow In Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI.Rows

            '    'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
            '    X = 0
            '    Me.TreeView1.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
            '    Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
            '    Me.MUESTRACONTNETTableAdapter.Connection = CON2
            '    Me.MUESTRACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
            '    Me.TreeView1.Nodes(I).ForeColor = Color.Black
            '    For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET.Rows
            '        Me.TreeView1.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & Trim(FilacontNet("STATUS").ToString()))
            '        Me.TreeView1.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
            '        If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
            '            Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Olive
            '        ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
            '            Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Navy
            '        Else
            '            Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Red
            '        End If
            '        X += 1
            '    Next
            '    I += 1
            'Next
            'CON2.Close()
            'se comento fin

            Me.TreeView1.ExpandAll()

            Me.CREAARBOLDIGITAL2()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Function SP_MUESTRADIGITALDELCLI(ByVal oContrato As Long) As DataTable

        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, oContrato)
        dTable = BaseII.ConsultaDT("SP_MUESTRADIGITALDELCLI")

        SP_MUESTRADIGITALDELCLI = dTable

    End Function


    Public Sub CREAARBOLDIGITAL2()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next
            Me.HaberServicios_CliTableAdapter.Connection = CON3
            Me.HaberServicios_CliTableAdapter.Fill(Me.NewSofTvDataSet.HaberServicios_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))
            'Me.MUESTRADIGITALDELCLITableAdapter.Connection = CON3
            'Me.MUESTRADIGITALDELCLITableAdapter.Fill(Me.NewSofTvDataSet.MUESTRADIGITALDELCLI, New System.Nullable(Of Long)(CType(Contrato, Long)))
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView3.Nodes.Clear()
            Dim Dt As New DataTable
            Dt = SP_MUESTRADIGITALDELCLI(Contrato)

            For Each FilaRow In Dt.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                If FilaRow("Tipo").ToString() = "A" Then
                    Me.TreeView3.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                    Me.TreeView3.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString()) & FilaRow("Tipo").ToString()
                ElseIf FilaRow("Tipo").ToString() = "L" Then
                    Me.TreeView3.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                    Me.TreeView3.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString()) & FilaRow("Tipo").ToString()
                Else

                    Me.TreeView3.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                    Me.TreeView3.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString()) & FilaRow("Tipo").ToString()
                    Me.MUESTRACONTDIGTableAdapter.Connection = CON3
                    Me.MUESTRACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                    Me.TreeView3.Nodes(I).ForeColor = Color.Black
                    For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG.Rows
                        Me.TreeView3.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " " & Trim(FilacontNet("STATUS").ToString()))
                        Me.TreeView3.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                        If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                            Me.TreeView3.Nodes(I).Nodes(X).ForeColor = Color.Olive
                        ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                            Me.TreeView3.Nodes(I).Nodes(X).ForeColor = Color.Navy
                        Else
                            Me.TreeView3.Nodes(I).Nodes(X).ForeColor = Color.Red
                        End If
                        X += 1
                    Next
                End If

                I += 1
            Next
            'If IsNumeric(ComboBox14.SelectedValue) = True Then
            'If ComboBox14.SelectedValue > 0 Then
            'Me.MuestraServicios_digitalTableAdapter.Connection = CON3
            'Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
            'uestraServicios_digitalBase2(3, Me.ComboBox14.SelectedValue, GloIdCompania)
            'End If
            'End If
            Me.TreeView3.ExpandAll()
            CON3.Close()

            'CONRelClientesExtensiones(CONTRATOTextBox.Text)

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    'Public Sub CREAARBOLDIGITAL()

    '    Try
    '        Dim I As Integer = 0
    '        Dim X As Integer = 0
    '        ' Assumes that customerConnection is a valid SqlConnection object.
    '        ' Assumes that orderConnection is a valid OleDbConnection object.
    '        'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
    '        '  "SELECT * FROM dbo.Customers", customerConnection)''

    '        'Dim customerOrders As DataSet = New DataSet()
    '        'custAdapter.Fill(customerOrders, "Customers")
    '        ' 
    '        'Dim pRow, cRow As DataRow
    '        'For Each pRow In customerOrders.Tables("Customers").Rows
    '        ' Console.WriteLine(pRow("CustomerID").ToString())
    '        'Next


    '        Me.MUESTRADIGITALDELCLITableAdapter.Fill(Me.NewSofTvDataSet.MUESTRADIGITALDELCLI, New System.Nullable(Of Long)(CType(Contrato, Long)))
    '        Dim FilaRow As DataRow
    '        Dim FilacontNet As DataRow
    '        Me.TreeView2.Nodes.Clear()
    '        For Each FilaRow In Me.NewSofTvDataSet.MUESTRADIGITALDELCLI.Rows

    '            'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
    '            X = 0
    '            Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
    '            Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
    '            Me.MUESTRACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
    '            Me.TreeView2.Nodes(I).ForeColor = Color.Black
    '            For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG.Rows
    '                Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " " & Trim(FilacontNet("STATUS").ToString()))
    '                Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
    '                If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
    '                    Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
    '                ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
    '                    Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
    '                Else
    '                    Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
    '                End If
    '                X += 1
    '            Next
    '            I += 1
    '        Next
    '        Me.TreeView2.ExpandAll()
    '        Me.CREAARBOLDIGITAL2()



    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub buscaCONCLIENTETV()
        Dim cont, ppal As Integer
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONCLIENTETVTableAdapter.Connection = CON
            Me.CONCLIENTETVTableAdapter.Fill(Me.NewSofTvDataSet.CONCLIENTETV, New System.Nullable(Of Long)(CType(Me.CONTRATOTextBox.Text, Long)))
            'Me.Dime_Si_ESMiniBasicoTableAdapter.Connection = CON
            'Me.Dime_Si_ESMiniBasicoTableAdapter.Fill(Me.DataSetEDGAR.Dime_Si_ESMiniBasico, New System.Nullable(Of Long)(CType(Me.CONTRATOTextBox.Text, Long)), BndMini)
            'If (IdSistema = "SA" Or IdSistema = "VA") And BndMini = 1 Then
            '    'Si el Cliente tiene un Servicio MiniBasico no Puede Tener Otros Servicios
            '    If Me.Button7.Visible = True Then Me.Button7.Enabled = False
            '    If Me.Button8.Visible = True Then Me.Button8.Enabled = False
            '    If Me.Button11.Visible = True Then Me.Button11.Enabled = False
            '    If Me.Button28.Visible = True Then Me.Button28.Enabled = False
            'End If
            If (Me.FacturaTextBox1.Text.Trim.Length) > 0 And GloTipoUsuario <> 40 Then
                Me.TipSerTvComboBox.Enabled = False
            End If
            Me.PrimerMesCLIENTESTableAdapter.Connection = CON
            Me.PrimerMesCLIENTESTableAdapter.Fill(Me.DataSetLidia.PrimerMesCLIENTES, Me.CONTRATOTextBox.Text, 2, cont, ppal)
            If cont = 1 Then
                Me.TVCONPAGONumericUpDown.Enabled = False
                Me.TVSINPAGONumericUpDown.Enabled = False
            End If
            Me.CONRel_ClientesTv_UsuariosTableAdapter.Connection = CON
            Me.CONRel_ClientesTv_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ClientesTv_Usuarios, Me.CONTRATOTextBox.Text)
            Me.ConRel_ClientesTv_VendedorTableAdapter.Connection = CON
            Me.ConRel_ClientesTv_VendedorTableAdapter.Fill(Me.DataSetEDGAR.ConRel_ClientesTv_Vendedor, Me.CONTRATOTextBox.Text)
            CON.Close()
            CREAARBOL()

            'Eric-----------------DESCUENTO AL CLIENTE
            BuscaDescTV()
            '---------------------

            MUESTRARel_ClientesTv_Usuarios(CONTRATOTextBox.Text)
            SP_ConsultaRel_Contratos_Companias(CONTRATOTextBox.Text)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub FillToolStripButton_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub hablitaserviciostv()

        Me.CONCLIENTETVBindingSource.AddNew()
        Me.ComboBox16.Text = ""
        Me.ComboBox16.SelectedValue = 0
        Me.ToolStripButton2.Enabled = True
        Me.ToolStripButton3.Enabled = True
        Me.CONTRATOTextBox1.Text = Me.CONTRATOTextBox.Text
        Me.Panel3.Enabled = True
        Me.PRIMERMENSUALIDACheckBox.Checked = True
        Me.STATUSTextBox.Text = "C"
        Me.FECHA_SOLICITUDTextBox.Text = Format(Now, "dd/MMM/yyyy")
        Me.Clv_TipoServicioTVTextBox.Text = 1
        Me.Clv_MOTCANTextBox.Text = 0
        'Me.TipSerTvComboBox.Text = "Basico"
        Me.MOTCANComboBox.SelectedValue = 0
        Me.MOTCANComboBox.Text = "Ninguno"
        If TipSerTvComboBox.Items.Count > 0 Then
            Me.TipSerTvComboBox.SelectedIndex = 0
        End If
        If Me.MOTCANComboBox.Items.Count > 0 Then
            Me.MOTCANComboBox.SelectedIndex = 0
        End If

    End Sub

    Private Sub boton_BasicoTV()
        'Éstas Lines las puso Eric
        If Me.ComboBox1.Text.Length = 0 Then
            Me.TVSINPAGONumericUpDown.Enabled = True
            Me.TVCONPAGONumericUpDown.Enabled = True
        End If
        If Me.ComboBox1.Text.Length = 0 And OpcionCli = "M" Then
            'Me.TVSINPAGONumericUpDown.Enabled = False
            Me.Button16.Enabled = False
        End If


        Dim resp As MsgBoxResult = MsgBoxResult.Cancel
        Me.ToolStripButton4.Enabled = False
        If OpcionCli = "N" Then
            Me.TVCONPAGONumericUpDown.Enabled = True
        End If
        MuestraTiposServicioTvPorColonia()

        If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
            If Me.CONTRATOTextBox.Text > 0 Then
                hablitaserviciostv()
                If Me.TipSerTvComboBox.SelectedValue > 0 Then
                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agregó un Servicio de Televisión", "", "Se Agrego un Servicio de Televisión: " + Me.TipSerTvComboBox.Text, LocClv_Ciudad)
                End If

            Else
                resp = MsgBox("No sean Guardado los Datos Personales del Clientes. ¿ Deseas Guardar ?", MsgBoxStyle.OkCancel)
                If resp = MsgBoxResult.Ok Then
                    guarda_Cliente()
                    hablitaserviciostv()
                    If Me.TipSerTvComboBox.SelectedValue > 0 Then
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agregó un Servicio de Televisión", "", "Se Agrego un Servicio de Televisión: " + Me.TipSerTvComboBox.Text, LocClv_Ciudad)
                    End If

                End If
            End If
        Else
            resp = MsgBox("No sean Guardado los Datos Personales del Clientes. ¿ Deseas Guardar ?", MsgBoxStyle.OkCancel)
            If resp = MsgBoxResult.Ok Then
                guarda_Cliente()
                hablitaserviciostv()
                If Me.TipSerTvComboBox.SelectedValue > 0 Then
                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agregó un Servicio de Televisión", "", "Se Agrego un Servicio de Televisión: " + Me.TipSerTvComboBox.Text, LocClv_Ciudad)
                End If

            End If
        End If

        'Éstas Líneas... así es, también las puso Eric
        Me.CortesiaCheckBox.Checked = False
        If GloTipoUsuario = 40 Then
            Me.CortesiaCheckBox.Enabled = True
        Else
            Me.TextBox2.Enabled = False
            Me.CortesiaCheckBox.Enabled = False
        End If

    End Sub


    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click

        If Not IsNumeric(Me.CONTRATOTextBox.Text) = True Then
            MsgBox("No sean Guardado los Datos Personales del Cliente.")
            Exit Sub
        End If

        If Not Me.CONTRATOTextBox.Text > 0 Then
            MsgBox("No sean Guardado los Datos Personales del Cliente.")
            Exit Sub
        End If

        DIME_SITELYNET(Contrato)
        MUESTRARel_ClientesTv_Usuarios(Contrato)
        ComboBox18.SelectedValue = GloClvUsuario
        If IdSistema = "LO" Or IdSistema = "YU" Then
            If gLONET_1 = 0 And gLOTEL_1 = 0 And CliTiene_Tv = 0 And CliTiene_Dig = 0 Then
                If GloHabilitadosNET = True Then
                    Dim op As Integer = 0

                    op = MsgBox("Desea Contratar Un Combo", MsgBoxStyle.YesNo)
                    If op = 6 Then 'si Desea El Combo
                        LocContratoLog = CLng(Contrato)
                        FrmContratacionCombo.Show()
                    Else
                        boton_BasicoTV()
                    End If
                Else
                    boton_BasicoTV()
                End If

            Else
                boton_BasicoTV()
            End If
        Else
            boton_BasicoTV()
        End If




    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
            If Me.CONTRATOTextBox.Text > 0 Then
                'VALIDA FECHAS==========================================
                If Me.ComboBox1.Text = "Contratado" Then
                    If IsDate(Me.FECHA_SOLICITUDTextBox.Text) = False Then
                        MsgBox("Se Requiere que se Capture la Fecha de Contratación", MsgBoxStyle.Exclamation, "Atención")
                    ElseIf CDate(Me.FECHA_SOLICITUDTextBox.Text) > CDate(Me.TextBox1.Text) Then
                        MsgBox("La Fecha de Contratación no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
                    Else
                        GuardaTv()
                    End If
                End If
                If Me.ComboBox1.Text = "Suspendido" Or Me.ComboBox1.Text = "Desconectado" Then
                    If IsDate(Me.FECHA_CORTETextBox.Text) = False Then
                        MsgBox("Se Requiere que se Capture la Fecha de Suspensión o Desconexión", MsgBoxStyle.Exclamation, "Atención")
                    ElseIf (CDate(Me.FECHA_SOLICITUDTextBox.Text) <= CDate(Me.FECHA_CORTETextBox.Text)) And (CDate(Me.FECHA_CORTETextBox.Text) <= CDate(Me.TextBox1.Text)) Then
                        GuardaTv()
                    Else
                        MsgBox("La Fecha de Suspensión o Desconexión debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
                    End If
                End If
                If Me.ComboBox1.Text = "Instalado" Then
                    If IsDate(Me.FECHA_INSTTextBox.Text) = False Then
                        MsgBox("Se Requiere que se Capture la Fecha de Instalación", MsgBoxStyle.Exclamation, "Atención")
                    ElseIf Me.ULTIMO_MESTextBox.Text = "" Or CInt(Me.ULTIMO_MESTextBox.Text) > 12 Or CInt(Me.ULTIMO_MESTextBox.Text) = 0 Then
                        MsgBox("Se Requiere que Capture el Ùltimo Mes", MsgBoxStyle.Exclamation, "Atención")
                    ElseIf (Me.ULTIMO_ANIOTextBox.Text).Length <> 4 Then
                        MsgBox("Se Requiere que Capture el Último Año", MsgBoxStyle.Exclamation, "Atención")
                    ElseIf (CDate(Me.FECHA_SOLICITUDTextBox.Text) <= CDate(Me.FECHA_INSTTextBox.Text)) And (CDate(Me.FECHA_INSTTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
                        GuardaTv()
                    Else
                        MsgBox("La Fecha de Instalación debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
                    End If
                End If
                If Me.ComboBox1.Text = "Fuera de Area" Then
                    If IsDate(Me.FECHACANCOUTAREATextBox.Text) = False Then
                        MsgBox("Se Requiere que se Capture la Fecha de Fuera de Área", MsgBoxStyle.Exclamation, "Atención")
                    ElseIf (CDate(Me.FECHA_SOLICITUDTextBox.Text) <= CDate(Me.FECHACANCOUTAREATextBox.Text)) And (CDate(Me.FECHACANCOUTAREATextBox.Text) <= CDate(Me.TextBox1.Text)) Then
                        GuardaTv()
                    Else
                        MsgBox("La Fecha de Fuera de Área debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
                    End If
                End If
                If Me.ComboBox1.Text = "Baja" Then
                    If IsDate(Me.FECHA_CANCELACIOTextBox.Text) = False Then
                        MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Information, "Atención")
                    ElseIf (CDate(Me.FECHA_SOLICITUDTextBox.Text) <= CDate(Me.FECHA_CANCELACIOTextBox.Text)) And (CDate(Me.FECHA_CANCELACIOTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
                        GuardaTv()

                    Else
                        MsgBox("La Fecha de Baja debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
                    End If
                End If

                If Me.ComboBox1.Text = "Desconectado Temporal" Then
                    If IsDate(Me.FECHA_CORTETextBox.Text) = False Then
                        MsgBox("Se Requiere que se Capture la Fecha de Corte de Servicio", MsgBoxStyle.Information, "Atención")
                    ElseIf (CDate(Me.FECHA_SOLICITUDTextBox.Text) <= CDate(Me.FECHA_CORTETextBox.Text)) And (CDate(Me.FECHA_CORTETextBox.Text) <= CDate(Me.TextBox1.Text)) Then
                        GuardaTv()

                    Else
                        MsgBox("La Fecha de Baja debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
                    End If
                End If

            Else
                MsgBox("Primero debe Guardar los Datos del Cliente", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Primero debe Guardar los Datos del Cliente", MsgBoxStyle.Information)
        End If
        Actualiza_usuario(CONTRATOTextBox.Text, CInt(Me.ComboBox18.SelectedValue), 1)
        'bnGeneraOrden.Enabled = True
    End Sub

    Private Sub GuardaTv()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        Me.Validate()
        Me.CONCLIENTETVBindingSource.EndEdit()
        Me.CONCLIENTETVTableAdapter.Connection = CON
        Me.CONCLIENTETVTableAdapter.Update(Me.NewSofTvDataSet.CONCLIENTETV)
        Me.HaberServicios_CliTableAdapter.Connection = CON
        Me.HaberServicios_CliTableAdapter.Fill(Me.NewSofTvDataSet.HaberServicios_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))
        Me.GUARDARRel_ClientesTv_UsuariosTableAdapter.Connection = CON
        Me.GUARDARRel_ClientesTv_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_ClientesTv_Usuarios, Me.CONTRATOTextBox.Text, GloClvUsuario)
        Me.Dime_Si_ESMiniBasicoTableAdapter.Connection = CON
        Me.Dime_Si_ESMiniBasicoTableAdapter.Fill(Me.DataSetEDGAR.Dime_Si_ESMiniBasico, New System.Nullable(Of Long)(CType(Me.CONTRATOTextBox.Text, Long)), BndMini)
        If Me.Button7.Text = "&Internet" Then
            Me.Button7.Enabled = True
        End If
        If Me.Button11.Text = "&Internet" Then
            Me.Button11.Enabled = True
            If (IdSistema = "SA" Or IdSistema = "VA") And BndMini = 0 Then
                Me.Button7.Enabled = True
            End If
        End If
        Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
        Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, Me.CONTRATOTextBox.Text)
        Me.CONSULTARCLIENTETableAdapter.Connection = CON
        Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, New System.Nullable(Of Long)(CType(Me.CONTRATOTextBox.Text, Long)))
        Me.Inserta_Rel_cortesia_FechaTableAdapter.Connection = CON
        Me.Inserta_Rel_cortesia_FechaTableAdapter.Fill(Me.Procedimientosarnoldo4.Inserta_Rel_cortesia_Fecha, CLng(Contrato), cortesiatv, 1)
        guardabitacoratv()
        damedatostv(Contrato)
        CON.Close()
        MsgBox("Se ha Guardado los Datos del Servicio de Televisión con Exíto", MsgBoxStyle.Information)
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        'Me.CONCLIENTETVBindingSource.CancelEdit()
        EliminaServicioTV()
        Select Case MsjBorraTV
            Case 0
                MsgBox("Servicio Eliminado Exitosamente", MsgBoxStyle.Information)
            Case 1
                MsgBox("El Servicio No Puede Ser Eliminado", MsgBoxStyle.Information)
        End Select
        buscaCONCLIENTETV()
        Me.ToolStripButton4.Visible = True
        Me.ToolStripButton4.Enabled = True
    End Sub

    Private Sub TipSerTvComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipSerTvComboBox.SelectedIndexChanged
        Me.Clv_TipoServicioTVTextBox.Text = Me.TipSerTvComboBox.SelectedValue
        If IsNumeric(Clv_TipoServicioTVTextBox.Text) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipoServicioTV", SqlDbType.Int, Clv_TipoServicioTVTextBox.Text)
            BaseII.CreateMyParameter("@tvFibra", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("sp_esPorFibraTv")
            BtnMininodo.Visible = CBool(BaseII.dicoPar("@tvFibra").ToString)
        End If
    End Sub

    Private Sub STATUSNOMComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub MOTCANComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MOTCANComboBox.SelectedIndexChanged
        Me.Clv_MOTCANTextBox.Text = Me.MOTCANComboBox.SelectedValue
    End Sub

    Private Sub STATUSNOMComboBox_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.STATUSTextBox.Text = Me.ComboBox1.SelectedValue
    End Sub

    Private Sub STATUSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles STATUSTextBox.TextChanged
        Select Case Me.STATUSTextBox.Text
            Case "C"
                Me.ComboBox1.SelectedIndex = 0
            Case "I"
                Me.ComboBox1.SelectedIndex = 1
            Case "D"
                Me.ComboBox1.SelectedIndex = 2
            Case "S"
                Me.ComboBox1.SelectedIndex = 3
            Case "F"
                Me.ComboBox1.SelectedIndex = 4
            Case "B"
                Me.ComboBox1.SelectedIndex = 5
        End Select
    End Sub

    'Private Sub Clv_TipoServicioTVTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TipoServicioTVTextBox.TextChanged
    ' Me.TipSerTvComboBox.SelectedIndex = 0
    'End Sub

    Private Sub FECHA_INSTTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHA_INSTTextBox.TextChanged
        Try

            If DateValue(Me.FECHA_INSTTextBox.Text) = DateValue("01/01/1900") Then
                Me.FECHA_INSTTextBox.Text = ""

            End If
        Catch
        End Try
    End Sub

    Private Sub FECHA_CORTETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHA_CORTETextBox.TextChanged
        Try
            If DateValue(Me.FECHA_CORTETextBox.Text) = DateValue("01/01/1900") Then
                Me.FECHA_CORTETextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub FECHACANCOUTAREATextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHACANCOUTAREATextBox.TextChanged
        Try
            If DateValue(Me.FECHACANCOUTAREATextBox.Text) = DateValue("01/01/1900") Then
                Me.FECHACANCOUTAREATextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub FECHA_CANCELACIOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHA_CANCELACIOTextBox.TextChanged
        Try
            If DateValue(Me.FECHA_CANCELACIOTextBox.Text) = DateValue("01/01/1900") Then
                Me.FECHA_CANCELACIOTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click

        'Validación para Tuxtla
        If Me.CONTRATOTextBox.Text.Length > 0 Then
            VALIDATipSerCliente(CONTRATOTextBox.Text)
            If Digital = True And Button8.Text.Contains("&Televis") Then
                MessageBox.Show("No se puede agregar Servicio de Televisión Análoga ya que cuenta con Servicio de Televisión Digital.")
                Exit Sub
            End If
            If Tv = True And Button8.Text = "IPTV" Then
                MessageBox.Show("No se puede agregar Servicio de Televisión Digital ya que cuenta con Servicio de Televisión Análoga.")
                Exit Sub
            End If
        End If

        If Me.Button8.Visible = True Then Me.Button8.Enabled = False
        If Me.Button7.Visible = True Then Me.Button7.Enabled = True
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True

        If Me.Button8.Text = "&Internet" Then


            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()

            Me.Panel2.Hide()
            Me.Panel4.Hide()
            Me.Panel7.Hide()

            'If Me.Button28.Enabled = False And Me.Button28.Visible = True And (IdSistema = "LO" Or IdSistema = "YU") Then
            'Me.Button28.Enabled = True
            '  Button8.Enabled = False
            'End If



        ElseIf Me.Button8.Text.Contains("&Televis") Then

            BndEsInternet = False
            Me.Panel2.Visible = True
            Me.Panel4.Visible = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

        ElseIf Me.Button8.Text = "IPTV" Or Me.Button8.Text = "&Premium" Then
            BndEsInternet = False
            Me.Panel2.Visible = False
            Me.Panel4.Visible = False
            Me.Panel7.Visible = True
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

            'If IsNumeric(ComboBox14.SelectedValue) = True Then
            '    If ComboBox14.SelectedValue > 0 Then
            '        Dim CON As New SqlConnection(MiConexion)
            '        CON.Open()
            '        'Me.MuestraServicios_digitalTableAdapter.Connection = CON
            '        'Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
            '        MuestraServicios_digitalBase2(3, Me.ComboBox14.SelectedValue, GloIdCompania)
            '        CON.Close()
            '    End If
            'End If
        ElseIf Me.Button8.Text = "&Telefonia" Then
            muestra_telefonia()
        End If
        'Eric----------
        BuscaDescTV()
        '--------------
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False

        'Dim conexion As New SqlConnection(MiConexion)
        'conexion.Open()
        'Dim consulta1 As New SqlCommand("SELECT CONTRATO FROM ClientesTV WHERE PrimerMensualidad = 0 AND CN.Contrato = @CONTRATO", conexion)
        'Dim par3 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        'par3.Direction = ParameterDirection.Input
        'par3.Value = CONTRATOTextBox.Text
        'consulta1.Parameters.Add(par3)
        'Dim contr1 As String = CStr(consulta1.ExecuteScalar())
        'Console.WriteLine("contrato es ", contr1)
        'If contr1 = CONTRATOTextBox.Text Then
        '    Me.bnGeneraOrden.Enabled = False
        'End If
        'conexion.Close()



    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click

        'Tuxtla
        If Me.CONTRATOTextBox.Text.Length > 0 Then
            VALIDATipSerCliente(CONTRATOTextBox.Text)
            If Tv = True And Button7.Text = "IPTV" Then
                MessageBox.Show("No se puede agregar Servicio de Televisión Digital ya que cuenta con Servicio de Televisión Análoga.")
                Exit Sub
            End If
            If Digital = True And Button7.Text.Contains("&Televis") Then
                MessageBox.Show("No se puede agregar Servicio de Televisión Análoga ya que cuenta con Servicio de Televisión Digital.")
                Exit Sub
            End If
        End If

        If Me.Button8.Visible = True Then Me.Button8.Enabled = True
        If Me.Button7.Visible = True Then Me.Button7.Enabled = False
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True
        Panel10.Visible = False
        panelTipoCaja.Visible = False

        If Me.Button7.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Panel2.Hide()
            Me.Panel4.Hide()
            Me.Panel7.Hide()
            'If Me.Button28.Enabled = False And Me.Button28.Visible = True And (IdSistema = "LO" Or IdSistema = "YU") Then
            '    Me.Button28.Enabled = True
            '    Button8.Enabled = False
            'End If
        ElseIf Me.Button7.Text.Contains("&Televis") Then

            BndEsInternet = False
            Me.Panel2.Visible = True
            Me.Panel4.Visible = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            If OpcionCli = "M" Then
                Panel2.Enabled = True
            End If
            frmInternet2.Hide()

        ElseIf Me.Button7.Text = "IPTV" Or Me.Button7.Text = "&Premium" Then
            BndEsInternet = False
            Me.Panel2.Visible = False
            Me.Panel4.Visible = False
            Me.Panel7.Visible = True
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

            'If IsNumeric(ComboBox14.SelectedValue) = True Then
            '    If ComboBox14.SelectedValue > 0 Then
            '        Dim CON As New SqlConnection(MiConexion)
            '        CON.Open()
            '        'Me.MuestraServicios_digitalTableAdapter.Connection = CON
            '        'Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
            '        MuestraServicios_digitalBase2(3, Me.ComboBox14.SelectedValue, GloIdCompania)
            '        CON.Close()
            '    End If
            'End If
        ElseIf Me.Button7.Text = "&Telefonia" Then
            muestra_telefonia()
        End If
        'Eric----------
        BuscaDescTV()
        '--------------
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False

        'If Me.Button7.Text = "&Internet" Then

        '    BndEsInternet = True
        '    frmctr.MdiParent = Me
        '    frmInternet2.MdiParent = Me
        '    frmctr.WindowState = FormWindowState.Normal
        '    frmInternet2.Show()
        '    frmctr.Show()
        '    frmctr.Boton_Internet()
        '    frmctr.TreeView1.ExpandAll()

        '    Me.Panel2.Hide()
        '    Me.Panel4.Hide()
        '    Me.Panel7.Hide()
        '    '--Controlar el Efecto de los botones de Television y Internet
        '    If Me.Button28.Enabled = False And Me.Button28.Visible = True And IdSistema = "LO" Then
        '        Me.Button28.Enabled = True
        '        Button8.Enabled = False
        '    End If
        '    If Me.Button28.Visible = True And Me.Button8.Visible = True Then
        '        Button8.Enabled = True
        '    End If

        '    Me.Button8.Enabled = True
        '    Me.Button7.Enabled = False
        '    Me.Button28.Enabled = True

        'ElseIf Me.Button7.Text = "&Tv Digital" Or Me.Button7.Text = "&Premium" Then
        '    BndEsInternet = False
        '    Me.Panel2.Visible = False
        '    Me.Panel4.Visible = False
        '    Me.Panel7.Visible = True
        '    frmctr.Hide()
        '    'No lo Usas frmTelefonia.Hide()
        '    frmInternet2.Hide()

        '    If IsNumeric(ComboBox14.SelectedValue) = True Then
        '        If ComboBox14.SelectedValue > 0 Then
        '            Dim CON As New SqlConnection(MiConexion)
        '            CON.Open()
        '            Me.MuestraServicios_digitalTableAdapter.Connection = CON
        '            Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
        '            CON.Close()
        '            BuscaDescDig()
        '        End If
        '    End If
        'End If
        ''Eric----------
        'BuscaDescTV()
        ''--------------
        ''Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        'eEntraUM = True
        'eEntraUMB = False

        'Dim conexion As New SqlConnection(MiConexion)
        'conexion.Open()
        'Dim consulta1 As New SqlCommand("SELECT CONTRATO FROM ClientesNet CN INNER JOIN ContNet  N ON CN.ContratoNet = N.ContratoNet  WHERE PrimerMensualidad = 0 AND CN.Contrato = @CONTRATO", conexion)
        'Dim par3 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        'par3.Direction = ParameterDirection.Input
        'par3.Value = CONTRATOTextBox.Text
        'consulta1.Parameters.Add(par3)
        'Dim contr1 As String = CStr(consulta1.ExecuteScalar())
        'Console.WriteLine("contrato es ", contr1)
        'If contr1 = CONTRATOTextBox.Text Then
        '    Me.bnGeneraOrden.Enabled = False
        'End If
        'conexion.Close()



    End Sub


    Private Sub BotonInternet()
        If Me.Button8.Visible = True Then Me.Button8.Enabled = True
        If Me.Button7.Visible = True Then Me.Button7.Enabled = False
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True


        If Me.Button7.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Panel2.Hide()
            Me.Panel4.Hide()
            Me.Panel7.Hide()
            'If Me.Button28.Enabled = False And Me.Button28.Visible = True And (IdSistema = "LO" Or IdSistema = "YU") Then
            '    Me.Button28.Enabled = True
            '    Button8.Enabled = False
            'End If
        ElseIf Me.Button7.Text.Contains("&Televis") Then

            BndEsInternet = False
            Me.Panel2.Visible = True
            Me.Panel4.Visible = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

        ElseIf Me.Button7.Text = "IPTV" Or Me.Button7.Text = "&Premium" Then
            BndEsInternet = False
            Me.Panel2.Visible = False
            Me.Panel4.Visible = False
            Me.Panel7.Visible = True
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

            'If IsNumeric(ComboBox14.SelectedValue) = True Then
            '    If ComboBox14.SelectedValue > 0 Then
            '        Dim CON As New SqlConnection(MiConexion)
            '        CON.Open()
            '        'Me.MuestraServicios_digitalTableAdapter.Connection = CON
            '        'Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
            '        MuestraServicios_digitalBase2(3, Me.ComboBox14.SelectedValue, GloIdCompania)
            '        CON.Close()
            '    End If
            'End If
        ElseIf Me.Button7.Text = "&Telefonia" Then
            muestra_telefonia()
        End If
        'Eric----------
        BuscaDescTV()
        '--------------
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False

        'If Me.Button7.Text = "&Internet" Then

        '    BndEsInternet = True
        '    frmctr.MdiParent = Me
        '    frmInternet2.MdiParent = Me
        '    frmctr.WindowState = FormWindowState.Normal
        '    frmInternet2.Show()
        '    frmctr.Show()
        '    frmctr.Boton_Internet()
        '    frmctr.TreeView1.ExpandAll()

        '    Me.Panel2.Hide()
        '    Me.Panel4.Hide()
        '    Me.Panel7.Hide()
        '    '--Controlar el Efecto de los botones de Television y Internet
        '    If Me.Button28.Enabled = False And Me.Button28.Visible = True And IdSistema = "LO" Then
        '        Me.Button28.Enabled = True
        '        Button8.Enabled = False
        '    End If
        '    If Me.Button28.Visible = True And Me.Button8.Visible = True Then
        '        Button8.Enabled = True
        '    End If

        '    Me.Button8.Enabled = True
        '    Me.Button7.Enabled = False
        '    Me.Button28.Enabled = True

        'ElseIf Me.Button7.Text = "&Tv Digital" Or Me.Button7.Text = "&Premium" Then
        '    BndEsInternet = False
        '    Me.Panel2.Visible = False
        '    Me.Panel4.Visible = False
        '    Me.Panel7.Visible = True
        '    frmctr.Hide()
        '    'No lo Usas frmTelefonia.Hide()
        '    frmInternet2.Hide()

        '    If IsNumeric(ComboBox14.SelectedValue) = True Then
        '        If ComboBox14.SelectedValue > 0 Then
        '            Dim CON As New SqlConnection(MiConexion)
        '            CON.Open()
        '            Me.MuestraServicios_digitalTableAdapter.Connection = CON
        '            Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
        '            CON.Close()
        '            BuscaDescDig()
        '        End If
        '    End If
        'End If
        ''Eric----------
        'BuscaDescTV()
        ''--------------
        ''Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        'eEntraUM = True
        'eEntraUMB = False



    End Sub

    Private Sub Clv_ColoniaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ColoniaTextBox.TextChanged
        'Try

        '    If IsNumeric(Me.COLONIAComboBox.SelectedValue) = True And Me.COLONIAComboBox.SelectedValue > 0 Then
        '        'Dim CON As New SqlConnection(MiConexion)
        '        'CON.Open()
        '        'Me.MuestraCVECOLCIUTableAdapter.Connection = CON
        '        'Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCVECOLCIU, New System.Nullable(Of Integer)(CType(Me.COLONIAComboBox.SelectedValue, Integer)))
        '        'CIUDADComboBox.SelectedValue = 0
        '        'CON.Close()
        '    End If
        'Catch ex As System.Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try

    End Sub

    Private Sub cargaviewtree()




    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim contv As Integer

        'Try
        ' Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, New System.Nullable(Of Long)(CType(CONTRATOToolStripTextBox.Text, Long)))
        ' Catch ex As System.Exception
        ' System.Windows.Forms.MessageBox.Show(ex.Message)
        ' End Try
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim resp As MsgBoxResult = MsgBoxResult.Cancel

            If GloGuardarNet = True Then
                MsgBox("Primero debe guardar el nuevo cable módem")
                Exit Sub
            End If
            If Contrato > 0 Then
                GLOMOVNET = 0
                GloClv_Cablemodem = 0
                GloClv_Servicio = 0
                GloClv_TipSer = 2
                If IdSistema <> "VA" Then
                    resp = MsgBox("¿ El Cliente cuenta Cablemodem Propio ? ", MsgBoxStyle.YesNoCancel)
                End If
                If resp = MsgBoxResult.Yes Then
                    'Linea de Eric
                    eCabModPropio = False

                    'Comentado el 03 Dic 2011
                    'FrmCabModPropio.Show()
                    SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
                    SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
                    SoftvMod.VariablesGlobales.MiConexion = MiConexion
                    SoftvMod.VariablesGlobales.Contrato = CInt(CONTRATOTextBox.Text)
                    Dim frm As New SoftvMod.Tuxtla.FrmCablemodemPropio
                    frm.Show()

                    Me.CONTARCLIENTESTableAdapter.Connection = CON
                    Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, GloContratoNet2, 0, contv)
                    'If contv = 0 Then
                    '    Me.SoloInternetCheckBox.Enabled = True
                    'Else
                    '    Me.SoloInternetCheckBox.Enabled = False
                    'End If
                ElseIf resp = MsgBoxResult.No Then
                    'Linea de Eric
                    eCabModPropio = True
                    resp = MsgBox("¿ El modem es Alámbrico ? ", MsgBoxStyle.YesNoCancel)
                    If resp = MsgBoxResult.Yes Then
                        GloTipoCablemodem = 1
                        ' Me.ComboBox4.Text = "Alambrico"
                    ElseIf resp = MsgBoxResult.No Then
                        GloTipoCablemodem = 2
                        ' Me.ComboBox4.Text = "Inalambrico"
                    End If

                    GloIdCompania = ComboBoxCompanias.SelectedValue

                    FrmSelServicios.Show()
                    'Me.SoloInternetCheckBox.Enabled = False
                    'Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
                    ''Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, Me.CONTRATOTextBox.Text)
                    'Me.CONSULTARCLIENTETableAdapter.Connection = CON
                    'Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
                    GLOMOVNET = 1
                Else
                    GLOMOVNET = 3
                End If
            Else
                Dim resp2 As MsgBoxResult = MsgBoxResult.Yes
                resp2 = MsgBox("No se han Guardado los Datos del Clientes ¿ Deseas Guardar ? ", MsgBoxStyle.YesNoCancel)
                If resp2 = MsgBoxResult.Yes Then
                    Me.asiganacalle()
                    Me.asignaciudad()
                    Me.asignacolonia()
                    If OpcionCli = "N" Then
                        If Len(Trim(Me.NOMBRETextBox.Text)) = 0 Then
                            MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_CalleTextBox.Text) = False Then
                            MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
                            MsgBox("Se Requiere las Entre Calles ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
                            MsgBox("Se Requiere el Numero ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Then
                            MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Then
                            MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If

                    Me.Validate()
                    Me.CONSULTARCLIENTEBindingSource.EndEdit()
                    Me.CONSULTARCLIENTETableAdapter.Connection = CON
                    Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
                    TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue)
                    '--MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
                End If
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub CONTRATOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTRATOTextBox.TextChanged
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                Contrato = Me.CONTRATOTextBox.Text
                If Contrato > 0 Then
                    Me.HaberServicios_CliTableAdapter.Connection = CON
                    Me.HaberServicios_CliTableAdapter.Fill(Me.NewSofTvDataSet.HaberServicios_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))
                    GloContratoVer = Contrato
                    'Me.Valida_SiahiOrdSerTableAdapter.Connection = CON
                    'Me.Valida_SiahiOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.Valida_SiahiOrdSer, New System.Nullable(Of Long)(CType(Contrato, Long)))
                    'Me.Valida_SiahiQuejasTableAdapter.Connection = CON
                    'Me.Valida_SiahiQuejasTableAdapter.Fill(Me.NewSofTvDataSet.Valida_SiahiQuejas, New System.Nullable(Of Long)(CType(Contrato, Long)))
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
                    BaseII.CreateMyParameter("@Cont", ParameterDirection.Output, SqlDbType.Int)
                    BaseII.ProcedimientoOutPut("Valida_SiahiOrdSerOutput")
                    ValidaTextBox.Text = BaseII.dicoPar("@Cont").ToString()


                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
                    BaseII.CreateMyParameter("@Cont", ParameterDirection.Output, SqlDbType.Int)
                    BaseII.ProcedimientoOutPut("Valida_SiahiQuejasOutput")
                    ValidaTextBox1.Text = BaseII.dicoPar("@Cont").ToString()

                    Me.Activa_controles()
                Else
                    Me.BasicoTextBox.Text = 0
                    Me.InternetTextBox.Text = 0
                    Me.DigitalTextBox.Text = 0
                    GloContratoVer = 0
                    Me.Desactiva_controles()
                End If
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        Dim habilita As String
        Dim contv As Integer
        'Muestra_Usuarios(GloClvUnicaNet, 2)
        Me.Label5.Visible = False
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    NombrePaqueteElimino = e.Node.Text
                    LocGloContratoNet = e.Node.Tag
                    GloOp = 1

                    Panel6.Enabled = True
                    Panel5.Enabled = True
                    Me.Panel6.Visible = False
                    Me.Panel5.Visible = True
                    Me.VerAparatodelClienteTableAdapter.Connection = CON
                    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
                    Me.CONSULTACLIENTESNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    Me.DameFechaHabilitarTableAdapter.Connection = CON
                    Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar, CType(e.Node.Tag, Long), 1, habilita)
                    'Muestra_Usuarios(GloClvUnicaNet, 3)
                    'If habilita <> "01/01/1900" Then
                    '    Me.Label5.Visible = True
                    '    Me.Label5.Text = (" El Servicio es de Prueba y Vence en:" & " " & habilita)
                    '    Me.Label5.BackColor = Color.Yellow
                    '    Me.Label5.ForeColor = Color.Red
                    'End If
                    GloContratoNet2 = e.Node.Tag
                Else
                    Me.VerAparatodelClienteTableAdapter.Connection = CON
                    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                End If
                damedatosnet2()
            Else
                Panel6.Enabled = True
                Panel5.Enabled = True
                Me.Panel5.Visible = False
                Me.Panel6.Visible = True


                If IsNumeric(e.Node.Tag) Then
                    cortesiaint = False
                    GloClvUnicaNet = e.Node.Tag
                    LocGloClv_unianet = GloClvUnicaNet
                    GloOp = 2
                    NombrePaqueteElimino = e.Node.Text
                    damedatosnet(GloClvUnicaNet)
                    Me.CONSULTACONTNETTableAdapter.Connection = CON
                    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    Me.CONRel_ContNet_UsuariosTableAdapter.Connection = CON
                    Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, GloClvUnicaNet)

                    'Eric------------DESCUENTO AL CLIENTE
                    BuscaDescNet()

                    '-----------------------------------

                    'PAGOS DIFERIDOS--------------------
                    MuestraPagosDif(Contrato, 2, GloClvUnicaNet)
                    If eRes = 1 Then
                        lblPagoDifNET.Text = eMsj
                        lblPagoDifNET.Visible = True
                        If eBnd = True Then
                            lblPagoDifNET.ForeColor = Color.Red
                            lblPagoDifNET.BackColor = Color.Gold
                        Else
                            lblPagoDifNET.ForeColor = Color.Black
                            lblPagoDifNET.BackColor = Color.Transparent
                        End If
                    Else
                        lblPagoDifNET.Text = String.Empty
                        lblPagoDifNET.Visible = False
                    End If
                    '-----------------------------------

                Else
                    Me.CONSULTACONTNETTableAdapter.Connection = CON
                    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    Me.CONRel_ContNet_UsuariosTableAdapter.Connection = CON
                    Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, 0)
                End If



            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub



    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        If Me.ComboBox2.Text = "Instalado" Then
            If IsDate(Me.Fecha_ActivacionTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Activación de Manera Correcta", MsgBoxStyle.Information)
            Else
                guardanet2()
            End If
        ElseIf Me.ComboBox2.Text = "Baja" Then
            If IsDate(Me.Fecha_bajaTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Information)
            Else
                guardanet2()
            End If
        ElseIf Me.ComboBox2.Text = "Traspasado" Then
            If IsDate(Me.Fecha_TraspasoTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Activación", MsgBoxStyle.Information)
            Else
                guardanet2()
            End If
        ElseIf Me.ComboBox2.Text.Contains("Por Asignar") Then
            guardanet2()
        End If
        'bnGeneraOrden.Enabled = True
    End Sub
    Private Sub guardanet2()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONSULTACLIENTESNETBindingSource.EndEdit()
        Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
        Me.CONSULTACLIENTESNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)
        guardabitacoranet(5)
        CON.Close()
        GuardaModificame_cablemodem_Internet()
        Me.CREAARBOL()
        MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
        damedatosnet2()
        'bnGeneraOrden.Enabled = True
    End Sub

    Private Sub ToolStripButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton5.Click
        Me.CONSULTACLIENTESNETBindingSource.CancelEdit()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.StatusTextBox1.Text = Me.ComboBox2.SelectedValue

    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        Me.Clv_TipoServicioTextBox.Text = ComboBox3.SelectedValue
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Me.Tipo_CablemodemTextBox.Text = ComboBox4.SelectedValue
    End Sub

    Private Sub ComboBox6_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox6.SelectedIndexChanged
        Me.Clv_VendedorTextBox.Text = Me.ComboBox6.SelectedValue
    End Sub

    'Private Sub ComboBox7_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    'Me.Clv_PromocionTextBox.Text = Me.ComboBox7.SelectedValue
    ' End Sub

    Private Sub ToolStripButton8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton8.Click
        'VALIDACIONES DE FECHAS
        'Guarda servicio internet
        If Me.ComboBox5.Text = "Contratado" Then
            If IsDate(Me.Fecha_solicitudTextBox1.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Contratación", MsgBoxStyle.Information, "Atención")
            ElseIf CDate(Me.Fecha_solicitudTextBox1.Text) > CDate(Me.TextBox1.Text) Then
                MsgBox("La Fecha de Contratación no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Information, "Atención")
            Else
                guardanet()
            End If
        End If
        If Me.ComboBox5.Text = "Suspendido" Or Me.ComboBox5.Text = "Desconectado" Then
            If IsDate(Me.Fecha_suspensionTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Suspensión o Desconexión", MsgBoxStyle.Information, "Atención")
            ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_suspensionTextBox.Text)) And (CDate(Me.Fecha_suspensionTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
                guardanet()
            Else
                MsgBox("La Fecha de Suspensión o Desconexión debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
            End If
        End If
        If Me.ComboBox5.Text = "Instalado" Then
            If IsDate(Me.Fecha_instalacioTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Instalación", MsgBoxStyle.Information, "Atención")
            ElseIf Me.Ultimo_mesTextBox1.Text = "" Or CInt(Me.Ultimo_mesTextBox1.Text) = 0 Or CInt(Me.Ultimo_mesTextBox1.Text) > 12 Then
                MsgBox("Se Requiere que Capture el Último Mes", MsgBoxStyle.Exclamation, "Atención")
            ElseIf (Me.Ultimo_anioTextBox1.Text).Length <> 4 Then
                MsgBox("Se Requiere que Capture el Último Año", MsgBoxStyle.Exclamation, "Atención")
            ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_instalacioTextBox.Text)) And (CDate(Me.Fecha_instalacioTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
                guardanet()
            Else
                MsgBox("La Fecha de Instalación debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")

            End If
        End If
        If Me.ComboBox5.Text = "Fuera de Area" Then
            If IsDate(Me.Fecha_Fuera_AreaTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Fuera de Área", MsgBoxStyle.Exclamation, "Atención")
            ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_Fuera_AreaTextBox.Text)) And (CDate(Me.Fecha_Fuera_AreaTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
                guardanet()
            Else
                MsgBox("La Fecha de Fuera de Área debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
            End If
        End If
        If Me.ComboBox5.Text = "Baja" Then
            MsgBox("Solo se puede cancelar por una orden de servicio o por el cobro de adeudo", vbInformation)
            Exit Sub
            If IsDate(Me.Fecha_bajaTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Exclamation, "Atención")
            ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_bajaTextBox.Text)) And (CDate(Me.Fecha_bajaTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
                guardanet()
            Else
                MsgBox("La Fecha de Baja debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
            End If
        End If
        Actualiza_usuario(GloClvUnicaNet, CInt(Me.ComboBox1.SelectedValue), 3)
        'bnGeneraOrden.Enabled = True
    End Sub
    Private Sub guardanet()


        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONSULTACONTNETBindingSource.EndEdit()
        Me.CONSULTACONTNETTableAdapter.Connection = CON
        Me.CONSULTACONTNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
        Me.Inserta_Rel_cortesia_FechaTableAdapter.Connection = CON
        Me.Inserta_Rel_cortesia_FechaTableAdapter.Fill(Me.Procedimientosarnoldo4.Inserta_Rel_cortesia_Fecha, GloClvUnicaNet, cortesiaint, 2)
        guardabitacoranet(4)
        CON.Close()
        Me.CREAARBOL()
        damedatosnet(GloClvUnicaNet)
        GloGuardarNet = False
        MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
        'bnGeneraOrden.Enabled = True
    End Sub
    Private Sub ToolStripButton7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton7.Click
        Me.CONSULTACONTNETBindingSource.CancelEdit()
    End Sub
    Private Sub Fecha_ActivacionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_ActivacionTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_ActivacionTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_ActivacionTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_SuspencionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_SuspencionTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_SuspencionTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_SuspencionTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_TraspasoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_TraspasoTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_TraspasoTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_TraspasoTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_BajaTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_BajaTextBox1.TextChanged
        Try
            If DateValue(Me.Fecha_BajaTextBox1.Text) = DateValue("01/01/1900") Then
                Me.Fecha_BajaTextBox1.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_solicitudTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_solicitudTextBox1.TextChanged
        Try
            If DateValue(Me.Fecha_solicitudTextBox1.Text) = DateValue("01/01/1900") Then
                Me.Fecha_solicitudTextBox1.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_instalacioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_instalacioTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_instalacioTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_instalacioTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_suspensionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_suspensionTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_suspensionTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_suspensionTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_bajaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_bajaTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_bajaTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_bajaTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_Fuera_AreaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_Fuera_AreaTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_Fuera_AreaTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_Fuera_AreaTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub FECHA_ULT_PAGOTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHA_ULT_PAGOTextBox1.TextChanged
        Try
            If DateValue(Me.FECHA_ULT_PAGOTextBox1.Text) = DateValue("01/01/1900") Then
                Me.FECHA_ULT_PAGOTextBox1.Text = ""
            End If
        Catch
        End Try
    End Sub





    Private Sub Manda_Agrega_Cablemodem()
        'Try
        ' Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, New System.Nullable(Of Long)(CType(CONTRATOToolStripTextBox.Text, Long)))
        ' Catch ex As System.Exception
        ' System.Windows.Forms.MessageBox.Show(ex.Message)
        ' End Try
        Try
            Dim CON As New SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand



            Dim resp As MsgBoxResult = MsgBoxResult.Cancel
            If Contrato > 0 Then
                If GloGuardarDig = True Then
                    MsgBox("Primero asigne el paquete y guarde ", MsgBoxStyle.Information)
                    Exit Sub
                End If
                GLOMOVDIG = 1
                GloClv_Cablemodem = 0
                'Me.BindingNavigator6.Enabled = True
                Dim LOCCLV_CABLEMODEM As Integer = 0
                LOCCLV_CABLEMODEM = GloClv_Cablemodem
                If GLOMOVDIG = 1 Then
                    GloGuardarDig = True
                    If Me.Button8.Text = "IPTV" Or Me.Button8.Text = "&Premium" Then
                        Me.Button7.Enabled = True
                    End If
                    'If Me.Button8.Text = "&Television" Then
                    '    Me.Button7.Enabled = True
                    'End If
                    'Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
                    'Me.CONSULTACLIENTESDIGTableAdapter.Insert(Contrato, "P", LOCCLV_CABLEMODEM, 0, False, False, "01/01/1900", "01/01/1900", "01/01/1900", "", True, LoContratonet)

                    CON.Open()
                    With cmd
                        .CommandText = "NUEVOCLIENTESDIG"
                        .Connection = CON
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        '@Contrato bigint, @Status varchar(1), @Clv_CableModem int, @Clv_Usuario int,
                        ' @ventacablemodem1 bit, @ventacablemodem2 bit,  @Fecha_Activacion datetime, 
                        '@Fecha_Suspencion datetime, @Fecha_Baja datetime,@Obs varchar(150),@SeRenta bit,@CONTRATONET BIGINT OUTPUT

                        Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
                        Dim prm2 As New SqlParameter("@clv_cablemodem", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@clv_usuario", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@ventacablemodem1", SqlDbType.Bit)
                        Dim prm5 As New SqlParameter("@ventacablemodem2", SqlDbType.Bit)
                        Dim prm6 As New SqlParameter("@Fecha_Activacion", SqlDbType.DateTime)
                        Dim prm7 As New SqlParameter("@Fecha_suspencion", SqlDbType.DateTime)
                        Dim prm8 As New SqlParameter("@Fecha_baja", SqlDbType.DateTime)
                        Dim prm9 As New SqlParameter("@Obs", SqlDbType.VarChar, 150)
                        Dim prm10 As New SqlParameter("@SeRenta", SqlDbType.Bit)
                        Dim prm11 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)

                        prm.Direction = ParameterDirection.Input
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm5.Direction = ParameterDirection.Input
                        prm6.Direction = ParameterDirection.Input
                        prm7.Direction = ParameterDirection.Input
                        prm8.Direction = ParameterDirection.Input
                        prm9.Direction = ParameterDirection.Input
                        prm10.Direction = ParameterDirection.Input
                        prm11.Direction = ParameterDirection.Output

                        'Contrato, "P", LOCCLV_CABLEMODEM, 0, False, False, "01/01/1900", "01/01/1900", "01/01/1900",
                        ' "", True, LoContratonet
                        prm.Value = Contrato
                        prm1.Value = "P"
                        prm2.Value = LOCCLV_CABLEMODEM
                        prm3.Value = 0
                        prm4.Value = False
                        prm5.Value = False
                        prm6.Value = "01/01/1900"
                        prm7.Value = "01/01/1900"
                        prm8.Value = "01/01/1900"
                        prm9.Value = ""
                        prm10.Value = True
                        prm11.Value = 0

                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm5)
                        .Parameters.Add(prm6)
                        .Parameters.Add(prm7)
                        .Parameters.Add(prm8)
                        .Parameters.Add(prm9)
                        .Parameters.Add(prm10)
                        .Parameters.Add(prm11)

                        Dim i As Integer = cmd.ExecuteNonQuery()

                        LoContratonet = prm11.Value
                    End With
                    CON.Close()
                    guardabitacoradig(3)


                    GloContratoDig_Nuevo = LoContratonet
                    CON.Open()
                    Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
                    Me.CONSULTACLIENTESDIGTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESDIG)
                    Me.CREAARBOLDIGITAL2()
                    Me.MUESTRA_A_DIGITALTableAdapter.Connection = CON
                    Me.MUESTRA_A_DIGITALTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRA_A_DIGITAL)
                    Muestra_Usuarios(loc_Clv_InicaDig, 3)
                    CON.Close()
                    'Me.SoloInternetCheckBox.Enabled = False
                    'Aqui Wey


                    Me.CONTRATONETTextBox2.Text = 0
                    Me.MACCABLEMODEMTextBox.Text = ""
                    CON.Open()
                    Me.MUESTRADIGITALDELCLI_porAparatoTableAdapter.Connection = CON
                    Me.MUESTRADIGITALDELCLI_porAparatoTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRADIGITALDELCLI_porAparato, New System.Nullable(Of Long)(CType(Contrato, Long)), New System.Nullable(Of Long)(CType(LoContratonet, Long)))
                    CON.Close()
                    'Me.ComboBox14.Text = Me.MACCABLEMODEMTextBox.Text
                    'Me.ComboBox14.SelectedValue = LoContratonet
                    'Me.ComboBox14.FindStringExact(Me.MACCABLEMODEMTextBox.Text, 0)
                    'MsgBox(Me.ContratoNetTextBox.Text, MsgBoxStyle.Information)
                    'MsgBox(Me.ContratoNetTextBox1.Text, MsgBoxStyle.Information)

                    'Me.CONTRATONETTextBox2.Text = 0
                    'Me.MACCABLEMODEMTextBox.Text = ""
                    'LOCCLV_CABLEMODEM = 0
                    'GloClv_Cablemodem = 0

                    NueClientesDigDec(comboBoxDec.SelectedValue, GloContratoDig_Nuevo)


                End If
            Else
                Dim resp2 As MsgBoxResult = MsgBoxResult.Yes
                resp2 = MsgBox("No se han Guardado los Datos del Clientes ¿ Deseas Guardar ? ", MsgBoxStyle.YesNoCancel)
                If resp2 = MsgBoxResult.Yes Then
                    Me.asiganacalle()
                    Me.asignaciudad()
                    Me.asignacolonia()
                    If OpcionCli = "N" Then
                        If Len(Trim(Me.NOMBRETextBox.Text)) = 0 Then
                            MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_CalleTextBox.Text) = False Then
                            MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
                            MsgBox("Se Requiere las Entre Calles ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
                            MsgBox("Se Requiere el Numero ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Then
                            MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Then
                            MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If

                    Me.Validate()
                    Me.CONSULTARCLIENTEBindingSource.EndEdit()
                    CON.Open()
                    Me.CONSULTARCLIENTETableAdapter.Connection = CON
                    Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
                    CON.Close()
                    TextBoxNoContrato.Text = SP_GuardaRel_Contratos_Companias(Me.CONTRATOTextBox.Text, ComboBoxCompanias.SelectedValue)
                    '--MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
                End If
            End If



        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    lOC_CONTRATONETDIG = e.Node.Tag
                Else
                    lOC_CONTRATONETDIG = 0
                End If
            Else
                lOC_CONTRATONETDIG = 0
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click

        'Tuxtla
        If Me.CONTRATOTextBox.Text.Length > 0 Then
            VALIDATipSerCliente(CONTRATOTextBox.Text)
            If Tv = True And Button11.Text = "IPTV" Then
                MessageBox.Show("No se puede agregar Servicio de Televisión Digital ya que cuenta con Servicio de Televisión Análoga.")
                Exit Sub
            End If
        End If

        If Me.Button8.Visible = True Then Me.Button8.Enabled = True
        If Me.Button7.Visible = True Then Me.Button7.Enabled = True
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True


        If Me.Button11.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Panel2.Hide()
            Me.Panel4.Hide()
            Me.Panel7.Hide()
            'If Me.Button28.Enabled = False And Me.Button28.Visible = True And (IdSistema = "LO" Or IdSistema = "YU") Then
            '    Me.Button28.Enabled = True
            '    Button8.Enabled = False
            'End If
        ElseIf Me.Button11.Text.Contains("&Televis") Then

            BndEsInternet = False
            Me.Panel2.Visible = True
            Me.Panel4.Visible = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

        ElseIf Me.Button11.Text = "IPTV" Or Me.Button11.Text = "&Premium" Then
            BndEsInternet = False
            Me.Panel2.Visible = False
            Me.Panel4.Visible = False
            Me.Panel7.Visible = True
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

            'If IsNumeric(ComboBox14.SelectedValue) = True Then
            '    If ComboBox14.SelectedValue > 0 Then
            '        Dim CON As New SqlConnection(MiConexion)
            '        CON.Open()
            '        'Me.MuestraServicios_digitalTableAdapter.Connection = CON
            '        'Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
            '        MuestraServicios_digitalBase2(3, Me.ComboBox14.SelectedValue, GloIdCompania)
            '        CON.Close()
            '    End If
            'End If
        ElseIf Me.Button11.Text = "&Telefonia" Then
            muestra_telefonia()
        End If
        'Eric----------
        BuscaDescTV()
        '--------------
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False

        ''Contrato = Me.CONTRATOTextBox.Text
        'BndEsInternet = True
        'frmctr.MdiParent = Me
        'frmInternet2.MdiParent = Me
        'frmctr.WindowState = FormWindowState.Normal
        'frmInternet2.Show()
        'frmctr.Show()
        'frmctr.TreeView1.ExpandAll()

        'Me.Panel2.Hide()
        'Me.Panel4.Hide()
        'Me.Panel7.Hide()

        ''Eric---------------
        'BuscaDescNet()
        ''-------------------

        ''Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        'eEntraUM = True
        'eEntraUMB = False

        ''Me.Panel7.Visible = False
        ''Me.Panel2.Visible = False
        ''Me.Panel4.Visible = True
        ''If IsNumeric(ComboBox14.SelectedValue) = True Then
        ''    If ComboBox14.SelectedValue > 0 Then
        ''        Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
        ''    End If
        ''End If
        'Dim conexion As New SqlConnection(MiConexion)
        'conexion.Open()
        'Dim consulta1 As New SqlCommand("SELECT CONTRATO FROM ClientesDig CN INNER JOIN ContDig  N ON CN.ContratoNet = N.ContratoNet  WHERE PrimerMensualidad = 1 AND CN.Contrato = @CONTRATO", conexion)
        'Dim par3 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        'par3.Direction = ParameterDirection.Input
        'par3.Value = CONTRATOTextBox.Text
        'consulta1.Parameters.Add(par3)
        'Dim contr1 As String = CStr(consulta1.ExecuteScalar())
        'Console.WriteLine("contrato es ", contr1)
        'If contr1 = CONTRATOTextBox.Text Then
        '    Me.bnGeneraOrden.Enabled = True
        'Else
        '    Me.bnGeneraOrden.Enabled = False
        'End If
        'conexion.Close()




    End Sub

    Private Sub BotonDigital()
        If Me.Button8.Visible = True Then Me.Button8.Enabled = True
        If Me.Button7.Visible = True Then Me.Button7.Enabled = True
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True


        If Me.Button11.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Panel2.Hide()
            Me.Panel4.Hide()
            Me.Panel7.Hide()
            'If Me.Button28.Enabled = False And Me.Button28.Visible = True And (IdSistema = "LO" Or IdSistema = "YU") Then
            '    Me.Button28.Enabled = True
            '    Button8.Enabled = False
            'End If
        ElseIf Me.Button11.Text.Contains("&Televis") Then

            BndEsInternet = False
            Me.Panel2.Visible = True
            Me.Panel4.Visible = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

        ElseIf Me.Button11.Text = "IPTV" Or Me.Button11.Text = "&Premium" Then
            BndEsInternet = False
            Me.Panel2.Visible = False
            Me.Panel4.Visible = False
            Me.Panel7.Visible = True
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

            'If IsNumeric(ComboBox14.SelectedValue) = True Then
            '    If ComboBox14.SelectedValue > 0 Then
            '        Dim CON As New SqlConnection(MiConexion)
            '        CON.Open()
            '        'Me.MuestraServicios_digitalTableAdapter.Connection = CON
            '        'Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
            '        MuestraServicios_digitalBase2(3, Me.ComboBox14.SelectedValue, GloIdCompania)
            '        CON.Close()
            '    End If
            'End If
        ElseIf Me.Button11.Text = "&Telefonia" Then
            muestra_telefonia()
        End If
        'Eric----------
        BuscaDescTV()
        '--------------
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False

        ''Contrato = Me.CONTRATOTextBox.Text
        'BndEsInternet = True
        'frmctr.MdiParent = Me
        'frmInternet2.MdiParent = Me
        'frmctr.WindowState = FormWindowState.Normal
        'frmInternet2.Show()
        'frmctr.Show()
        'frmctr.TreeView1.ExpandAll()

        'Me.Panel2.Hide()
        'Me.Panel4.Hide()
        'Me.Panel7.Hide()

        ''Eric---------------
        'BuscaDescNet()
        ''-------------------

        ''Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        'eEntraUM = True
        'eEntraUMB = False

        'Me.Panel7.Visible = False
        'Me.Panel2.Visible = False
        'Me.Panel4.Visible = True
        'If IsNumeric(ComboBox14.SelectedValue) = True Then
        '    If ComboBox14.SelectedValue > 0 Then
        '        Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
        '    End If
        'End If





    End Sub

    Private Sub ComboBox8_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox8.SelectedIndexChanged
        Me.TextBox21.Text = Me.ComboBox8.SelectedValue
    End Sub

    Private Function DimeSiEsDispositivoMovil() As Boolean
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_UnicaNet", SqlDbType.BigInt, loc_Clv_InicaDig)
        BaseII.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("DimeSiEsDispositivoMovil")
        Return CBool(BaseII.dicoPar("@bnd").ToString)
    End Function

    Private Sub AgregarPaquete()
        Dim CON As New SqlConnection(MiConexion)
        Try


            CON.Open()
            Dim Clv_unicanetDig As Long = 0
            Dim locdigcontratonet As Long = 0
            Dim locdigClv_servicio As Long = 0
            Dim RESP As MsgBoxResult = MsgBoxResult.No
            locdigClv_servicio = Me.ComboBox12.SelectedValue
            locdigcontratonet = CONTRATONETTextBox2.Text
            If locdigClv_servicio > 0 And locdigcontratonet > 0 Then
                eResValida = 0
                Me.ValidaDigitalTableAdapter.Connection = CON
                Me.ValidaDigitalTableAdapter.Fill(Me.NewSofTvDataSet.ValidaDigital, locdigcontratonet, locdigClv_servicio)
                LocGloContratoNet = locdigcontratonet
                ValidaDigital2(locdigcontratonet, locdigClv_servicio)
                If eResValida = 1 Then
                    MsgBox(eMsgValida, MsgBoxStyle.Information)
                    Exit Sub
                End If
                If Me.RespuestaTextBox.Text = 1 Then
                    GloGuardarDig = False
                    Me.CONSULTACONTDIGTableAdapter.Connection = CON
                    Me.CONSULTACONTDIGTableAdapter.Insert(locdigcontratonet, locdigClv_servicio, "C", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", True, 0, 0, False, "", "", True, "", 0, 0, "", "", Me.CortesiaCheckBox1.CheckState, Clv_unicanetDig)
                    'GuardaRelClientesAparatos(Me.CONTRATOTextBox.Text, locdigcontratonet, locdigClv_servicio, Me.AparatosComboBox.SelectedValue)
                    'GuardaRelClientesAparatos(Me.CONTRATOTextBox.Text, locdigcontratonet, locdigClv_servicio, 11)
                    loc_Clv_InicaDig = Clv_unicanetDig

                    If DimeSiEsDispositivoMovil() Then
                        Dim frmDisMovil As FrmDispositivoMovil
                        frmDisMovil = New FrmDispositivoMovil()
                        frmDisMovil.Clv_unicanetDigLoc = loc_Clv_InicaDig
                        frmDisMovil.ShowDialog()
                    End If

                    Me.GUARDARRel_ContDig_UsuariosTableAdapter.Connection = CON
                    Me.GUARDARRel_ContDig_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_ContDig_Usuarios, Clv_unicanetDig, GloClvUsuario)
                    'Me.SoloInternetCheckBox.Enabled = False
                    Me.CREAARBOLDIGITAL2()
                    Me.HaberServicios_CliTableAdapter.Connection = CON
                    Me.HaberServicios_CliTableAdapter.Fill(Me.NewSofTvDataSet.HaberServicios_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))
                    NomDig = ""
                    lOC_CONTRATONETDIG = 0
                    locdigClv_servicio = 0
                    locdigcontratonet = 0
                    Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
                    Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, New System.Nullable(Of Long)(CType(Contrato, Long)))
                    'Me.AsignaPeriodoTableAdapter.Fill(Me.DataSetLidia.AsignaPeriodo, Me.CONTRATOTextBox.Text)
                    'Me.CONSULTARCLIENTETableAdapter.Connection = CON
                    'Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, New System.Nullable(Of Long)(CType(Contrato, Long)))
                    Muestra_Usuarios(loc_Clv_InicaDig, 3)
                    GloGuardarDig = False
                ElseIf Me.RespuestaTextBox.Text = 0 Then
                    MsgBox("El Paquete ya está en la lista ", MsgBoxStyle.Information)
                ElseIf Me.RespuestaTextBox.Text = 4 Then
                    MsgBox("El Aparato Digital está dado de Baja por lo cual no se puede agregar el paquete ", MsgBoxStyle.Information)
                End If
            Else
                MsgBox("Se Requiere que Seleccione el Equipo Digital y También Seleccione el Paquete ", MsgBoxStyle.Information)
            End If

        Catch ex As Exception
        Finally
            CON.Close()
        End Try
    End Sub
    Public Sub Consulta_cliente()
        Dim ConCon As New SqlConnection(MiConexion)
        ConCon.Open()
        Me.CONSULTARCLIENTETableAdapter.Connection = ConCon
        Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
        ConCon.Close()
    End Sub
    Private Sub agregar_Paquete()
        Me.CortesiaCheckBox1.Checked = False
        Me.Panel10.Visible = True
        Me.Panel10.Enabled = True
        Me.Panel8.Enabled = False
        Me.Panel9.Enabled = False

        DimeSiEsPrincipal()
        ConsultaAparatosContratacion()
        MUESTRATipoCaja(GloIdCompania)

    End Sub


    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click

        If GloGuardarDig = True Then
            Dim CON As New SqlConnection(MiConexion)
            Try
                CON.Open()
                Me.BorraDigPor_NoGRaboTableAdapter.Connection = CON
                Me.BorraDigPor_NoGRaboTableAdapter.Fill(Me.NewSofTvDataSet.BorraDigPor_NoGRabo, New System.Nullable(Of Long)(CType(GloContratoDig_Nuevo, Long)), New System.Nullable(Of Long)(CType(0, Long)))

            Catch ex As Exception
            Finally
                If CON.State = ConnectionState.Open Then CON.Close()
            End Try
            GloGuardarDig = False
            Me.CREAARBOLDIGITAL2()
        End If
        Me.Panel10.Visible = False
        Me.Panel10.Enabled = False

    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        'Guuardar paquete digital
        'If IsNumeric(Me.AparatosComboBox.SelectedValue) = False Then
        '    MsgBox("Seleccione la Marca del Aparato que se le asignará al Cliente", MsgBoxStyle.Information)
        '    Exit Sub
        'End If

        'If Me.AparatosComboBox.SelectedValue <= 0 Then
        '    MsgBox("Seleccione la Marca del Aparato que se le asignará al Cliente", MsgBoxStyle.Information)
        '    Exit Sub
        'End If

        'If Me.AparatosComboBox.Text.Length = 0 Then
        '    MsgBox("Seleccione la Marca del Aparato que se le asignará al Cliente", MsgBoxStyle.Information)
        '    Exit Sub
        'End If

        GloGuardarDig = False
        GLoPaqueteAgrega = Me.ComboBox12.Text
        AgregarPaquete()
        ConsultaRelClientesAparatos(lOC_CONTRATONETDIG)
        Me.Panel10.Visible = False
        Me.Panel10.Enabled = False
        Me.Button11.Enabled = True
        'If IsNumeric(ComboBox14.SelectedValue) = True Then
        '    If ComboBox14.SelectedValue > 0 Then
        '        Dim CON As New SqlConnection(MiConexion)
        '        CON.Open()
        '        guardabitacoradig(0)
        '        'Me.MuestraServicios_digitalTableAdapter.Connection = CON
        '        'Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
        '        MuestraServicios_digitalBase2(3, Me.ComboBox14.SelectedValue, GloIdCompania)
        '        CON.Close()
        '    End If
        'End If

    End Sub

    Private Sub Quitar_Equipo()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim cont As Integer
        Dim contv As Integer

        If lOC_CONTRATONETDIG > 0 Then
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, lOC_CONTRATONETDIG, 1, cont)
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 3, contv)
            If cont = 0 And contv = 0 Then
                If Me.Button7.Text = "&Internet" Then
                    Me.SoloInternetCheckBox.Enabled = True
                    Me.Button7.Enabled = False
                End If
                If Me.Button11.Text = "&Internet" Then
                    Me.Button11.Enabled = False
                    Me.SoloInternetCheckBox.Enabled = True
                End If
            End If
            If cont = 0 Then
                'If GloTipoUsuario <> 40 Then
                '    MsgBox("Su perfil de usuario no cuenta con los permisos para realizar el proceso.")
                '    Exit Sub
                'End If
                guardabitacoradig(2)
                Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
                Me.CONSULTACLIENTESDIGTableAdapter.Delete(Contrato, lOC_CONTRATONETDIG)
                GloGuardarDig = False
            ElseIf cont > 0 Then
                MsgBox("Primero Debes Eliminar Los Paquetes ", MsgBoxStyle.Information)
            End If
            Me.CREAARBOLDIGITAL2()
            Me.MUESTRA_A_DIGITALTableAdapter.Connection = CON
            Me.MUESTRA_A_DIGITALTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRA_A_DIGITAL)
        End If
        CON.Close()

    End Sub

    Private Sub ToolStripButton16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton16.Click
        'Agregar SetUpBox
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Error", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ValidaPaqueteAdicional")
        If BaseII.dicoPar("@Error") = 1 Then
            MsgBox("No se puede agregar paquete o caja adicional, primero es necesario pagar la mensualidad del paquete principal", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        'Manda_Agrega_Cablemodem()
        MUESTRATipoCaja(GloIdCompania)
        panelTipoCaja.Visible = True
    End Sub

    Private Sub ToolStripButton15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton15.Click
        Quitar_Equipo()
    End Sub



    Private Sub ToolStripButton14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton14.Click
        'Agregar paquete digital
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Error", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ValidaPaqueteAdicional")
        If BaseII.dicoPar("@Error") = 1 Then
            MsgBox("No se puede agregar paquete o caja adicional, primero es necesario pagar la mensualidad del paquete principal", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        ValidaPagosDif(CONTRATOTextBox.Text)
        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        agregar_Paquete()
        'bnGeneraOrden.Enabled = True
    End Sub

    Private Sub TreeView3_AfterSelect_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView3.AfterSelect
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim habilita As String
        Me.CMB2Label5.Visible = False
        Dim oContratonet As Long = 0
        Dim oTipo As String = ""
        Try
            If e.Node.Level = 0 Then

                oContratonet = Mid(e.Node.Tag, 1, Len(e.Node.Tag) - 1)
                oTipo = Mid(e.Node.Tag, Len(e.Node.Tag), Len(e.Node.Tag))
                If (oTipo = "A" Or oTipo = "L") And IsNumeric(oContratonet) = True Then

                    Frm1.MdiParent = Me
                    Frm1.WindowState = FormWindowState.Normal
                    Frm1.BackColor = Me.BackColor
                    Frm1.Show()
                    'SplitContainer2.Panel2.Hide()
                    Panel9.Hide()
                    Panel8.Hide()
                    'Panel7.Hide()
                    Panel7.Location = New Point(2, 335)
                    Panel7.Size = New Size(438, 347)
                    Frm1.LContratonet = oContratonet
                    Frm1.LTipo = oTipo
                    Frm1.generaOrden(oContratonet, oTipo)
                    Frm1.restringir()
                    'Me.Panel2.Hide()
                    'Me.Panel4.Hide()
                    'Me.Panel7.Hide()
                ElseIf IsNumeric(oContratonet) = True Then

                    Panel7.Location = New Point(2, 335)
                    Panel7.Size = New Size(1010, 347)
                    lOC_CONTRATONETDIG = oContratonet
                    LocGloContratoNet = oContratonet
                    LoContratonet = oContratonet
                    ConClientesDigDec(LocGloContratoNet)


                    'IndiceSel = Me.TreeView3.Nodes.IndexOf 
                    GloOp = 3
                    'NombreMAC = Me.TreeView3.Nodes.Item(IndiceSel).Text
                    'MsgBox(IndiceSel.ToString, MsgBoxStyle.Information)
                    NombrePaqueteElimino = e.Node.Text
                    Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
                    Me.CONSULTACLIENTESDIGTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACLIENTESDIG, Contrato, lOC_CONTRATONETDIG)

                    'Muestra_Usuarios(lOC_CONTRATONETDIG, 3)
                    'Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar)
                    Me.VerAparatodelClientedigTableAdapter.Connection = CON
                    Me.VerAparatodelClientedigTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelClientedig, lOC_CONTRATONETDIG)
                    Muestra_Usuarios(loc_Clv_InicaDig, 3)
                    ConsultaRelClientesAparatos(lOC_CONTRATONETDIG)
                    eUsuario = Me.ComboBox20.Text
                    Me.Panel9.Enabled = True
                    Panel8.Visible = False
                    Panel9.Visible = True
                    Me.DameFechaHabilitarTableAdapter.Connection = CON
                    Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar, lOC_CONTRATONETDIG, 0, habilita)
                    'If habilita <> "01/01/1900" Then
                    '    Me.CMB2Label5.Visible = True
                    '    Me.CMB2Label5.Text = (" El Servicio es de Prueba y Vence en:" & " " & habilita)
                    '    Me.CMB2Label5.BackColor = Color.Yellow
                    '    Me.CMB2Label5.ForeColor = Color.Red
                    'End If
                    'Me.ComboBox14.SelectedValue = lOC_CONTRATONETDIG
                    ''Me.ComboBox14.Text = e.Node.Text
                    'Me.ComboBox14.FindStringExact(e.Node.Text, 0)
                    Me.CONTRATONETTextBox2.Text = 0
                    Me.MACCABLEMODEMTextBox.Text = ""

                    Me.MUESTRADIGITALDELCLI_porAparatoTableAdapter.Connection = CON
                    Me.MUESTRADIGITALDELCLI_porAparatoTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRADIGITALDELCLI_porAparato, New System.Nullable(Of Long)(CType(Contrato, Long)), New System.Nullable(Of Long)(CType(LoContratonet, Long)))

                    MuestraServicios_digitalBase2(3, lOC_CONTRATONETDIG, GloIdCompania)
                    damedatosdig2()
                Else
                    lOC_CONTRATONETDIG = 0
                End If
            ElseIf e.Node.Level = 1 Then
                Panel7.Location = New Point(2, 335)
                Panel7.Size = New Size(1010, 347)
                If IsNumeric(e.Node.Tag) = True Then
                    cortesiadig = False
                    GloOp = 4
                    loc_Clv_InicaDig = e.Node.Tag
                    NombrePaqueteElimino = e.Node.Text
                    LocGloClv_unianet = loc_Clv_InicaDig
                    damedatosdig(loc_Clv_InicaDig)
                    'loc_Clv_InicaDig



                    Me.CONSULTACONTDIGTableAdapter.Connection = CON
                    Me.CONSULTACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTDIG, loc_Clv_InicaDig)
                    Me.CONRel_ContDig_UsuariosTableAdapter.Connection = CON
                    Me.CONRel_ContDig_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContDig_Usuarios, loc_Clv_InicaDig)
                    Muestra_Usuarios(loc_Clv_InicaDig, 3)
                    eUsuario = Me.ComboBox20.Text


                    Me.Panel8.Enabled = True
                    Panel9.Visible = False
                    Panel8.Visible = True

                    If DimeSiEsDispositivoMovil() Then
                        ButtonPassDisMovil.Visible = True
                    Else
                        ButtonPassDisMovil.Visible = False
                    End If
                    'Eric------------------------ DESCUENTO AL CLIENTE
                    BuscaDescDig()
                    '---------------------------------
                    'PAGOS DIFERIDOS-----------------------------------
                    MuestraPagosDif(Contrato, 3, loc_Clv_InicaDig)
                    If eRes = 1 Then
                        lblPagoDifDIG.Text = eMsj
                        lblPagoDifDIG.Visible = True

                        If eBnd = True Then
                            lblPagoDifDIG.ForeColor = Color.Red
                            lblPagoDifDIG.BackColor = Color.Gold
                        Else
                            lblPagoDifDIG.ForeColor = Color.Black
                            lblPagoDifDIG.BackColor = Color.Transparent
                        End If

                    Else
                        lblPagoDifDIG.Text = String.Empty
                        lblPagoDifDIG.Visible = False
                    End If
                    '--------------------------------------------------

                Else
                    loc_Clv_InicaDig = 0
                End If
            Else

                lOC_CONTRATONETDIG = 0
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON.Close()
        End Try


    End Sub

    Private Sub TextBox20_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox20.TextChanged

    End Sub

    Private Sub ComboBox13_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox13.SelectedIndexChanged
        Me.TextBox26.Text = Me.ComboBox13.SelectedValue
    End Sub

    Private Sub ComboBox10_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox10.SelectedIndexChanged
        Me.TextBox20.Text = Me.ComboBox10.SelectedValue
        If Me.ComboBox10.Text = "Contratado" Then
            Me.CheckBox1.Checked = True
        Else
            Me.CheckBox1.Checked = False
        End If
    End Sub

    Private Sub ComboBox9_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox9.SelectedIndexChanged
        Me.TextBox22.Text = Me.ComboBox9.SelectedValue
    End Sub

    Private Sub ToolStripButton10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton10.Click
        'Guarda paquete digital
        If Me.ComboBox10.Text = "Contratado" Then
            If IsDate(Me.TextBox16.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Contratación", MsgBoxStyle.Information, "Atención")
            ElseIf CDate(Me.TextBox16.Text) > CDate(Me.TextBox1.Text) Then
                MsgBox("La Fecha de Contratación no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Information, "Atención")
            Else
                guardadig()
            End If
        End If
        If Me.ComboBox10.Text = "Suspendido" Or Me.ComboBox10.Text = "Desconectado" Then
            If IsDate(Me.TextBox14.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Suspensión o Desconexión", MsgBoxStyle.Information, "Atención")
            ElseIf (CDate(Me.TextBox16.Text) <= CDate(Me.TextBox14.Text)) And (CDate(Me.TextBox14.Text) <= CDate(Me.TextBox1.Text)) Then
                guardadig()
            Else
                MsgBox("La Fecha de Suspensión o Desconexión debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
            End If
        End If
        If Me.ComboBox10.Text = "Instalado" Then
            If IsDate(Me.TextBox15.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Instalación", MsgBoxStyle.Information, "Atención")
            ElseIf Me.TextBox10.Text = "" Or CInt(Me.TextBox10.Text) > 12 Or CInt(Me.TextBox10.Text) = 0 Then
                MsgBox("Se Requiere que Capture el Ùltimo Mes", MsgBoxStyle.Exclamation, "Atención")
            ElseIf (Me.TextBox9.Text).Length <> 4 Then
                MsgBox("Se Requiere que Capture el Último Año", MsgBoxStyle.Exclamation, "Atención")
            ElseIf (CDate(Me.TextBox16.Text) <= CDate(Me.TextBox15.Text)) And (CDate(Me.TextBox15.Text) <= CDate(Me.TextBox1.Text)) Then
                guardadig()
            Else
                MsgBox("La Fecha de Instalación debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
            End If
        End If
        If Me.ComboBox10.Text = "Fuera de Area" Then
            MsgBox("Solo se puede pasar a fuera de area por una orden de servicio ", vbInformation)
            Exit Sub
            If IsDate(Me.TextBox12.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Fuera de Área", MsgBoxStyle.Exclamation, "Atención")
            ElseIf (CDate(Me.TextBox16.Text) <= CDate(Me.TextBox12.Text)) And (CDate(Me.TextBox12.Text) <= CDate(Me.TextBox1.Text)) Then
                guardadig()
            Else
                MsgBox("La Fecha de Fuera de Área debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
            End If
        End If
        If Me.ComboBox10.Text = "Baja" Then
            MsgBox("Solo se puede cancelar por una orden de servicio o por el cobro de adeudo", vbInformation)
            Exit Sub
            If IsDate(Me.TextBox13.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Information, "Atención")
            ElseIf (CDate(Me.TextBox16.Text) <= CDate(Me.TextBox13.Text)) And (CDate(Me.TextBox13.Text) <= CDate(Me.TextBox1.Text)) Then
                guardadig()
            Else
                MsgBox("La Fecha de Baja debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
            End If
        End If

        'Actualiza_usuario(GloClvUnicaNet, CInt(Me.ComboBox19.SelectedValue), 3)
        'bnGeneraOrden.Enabled = True
    End Sub
    Private Sub guardadig()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONSULTACONTDIGBindingSource.EndEdit()
        Me.CONSULTACONTDIGTableAdapter.Connection = CON
        Me.CONSULTACONTDIGTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTDIG)
        Me.Inserta_Rel_cortesia_FechaTableAdapter.Connection = CON
        Me.Inserta_Rel_cortesia_FechaTableAdapter.Fill(Me.Procedimientosarnoldo4.Inserta_Rel_cortesia_Fecha, loc_Clv_InicaDig, cortesiadig, 3)

        guardabitacoradig(4)
        CON.Close()
        Me.CREAARBOLDIGITAL2()
        damedatosdig(loc_Clv_InicaDig)
        Actualiza_usuario(loc_Clv_InicaDig, CLng(Me.ComboBox20.SelectedValue), 3)
        MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
    End Sub

    Private Sub ToolStripButton12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton12.Click
        If Me.ComboBox13.Text = "Instalado" Then
            If IsDate(Me.TextBox33.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Activación", MsgBoxStyle.Information)
            Else
                guardardig2()
            End If
        ElseIf Me.ComboBox13.Text = "Baja" Then
            MsgBox("Solo se puede cancelar por una orden de servicio o por el cobro de adeudo", vbInformation)
            Exit Sub
            If IsDate(Me.TextBox23.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Information)
            Else
                guardardig2()
            End If
        ElseIf Me.ComboBox13.Text = "Traspasado" Then
            If IsDate(Me.TextBox33.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Activación", MsgBoxStyle.Information)
            Else
                guardardig2()
            End If
        ElseIf ComboBox13.Text.Contains("Por Asignar") Then
            guardardig2()
        End If
        'bnGeneraOrden.Enabled = True
    End Sub
    Private Sub guardardig2()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONSULTACLIENTESDIGBindingSource.EndEdit()
        Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
        Me.CONSULTACLIENTESDIGTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESDIG)

        'Me.CONSULTACLIENTESDIGTableAdapter.Update(
        guardabitacoradig(5)
        CON.Close()
        'Me.GuardaModificame_cablemodem()
        Me.CREAARBOLDIGITAL2()
        damedatosdig2()
        Actualiza_usuario(loc_Clv_InicaDig, CLng(Me.ComboBox20.SelectedValue), 3)
        NueClientesDigDec(comboBoxDec.SelectedValue, LocGloContratoNet)
        MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
    End Sub
    Private Sub ToolStripButton9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton9.Click
        Me.CONSULTACONTDIGBindingSource.CancelEdit()
    End Sub

    Private Sub ToolStripButton11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton11.Click
        Me.CONSULTACLIENTESDIGBindingSource.CancelEdit()
    End Sub
    Private Sub ToolStripButton13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton13.Click

        'Quitar paquete
        Dim cont, cont2 As Integer
        Dim PPAL As Boolean
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(loc_Clv_InicaDig) = True Then
            If loc_Clv_InicaDig > 0 Then
                'If GloTipoUsuario <> 40 Then
                '    MsgBox("Su perfil de usuario no cuenta con los permisos para realizar el proceso.")
                '    Exit Sub
                'End If
                If ValidaEliminarServicios(3, loc_Clv_InicaDig) = False Then
                    MsgBox("No se puede eliminar el Servicio.", MsgBoxStyle.Information)
                    Exit Sub
                End If

                Me.PrimerMesCLIENTESTableAdapter.Connection = CON
                Me.PrimerMesCLIENTESTableAdapter.Fill(Me.DataSetLidia.PrimerMesCLIENTES, loc_Clv_InicaDig, 1, cont, PPAL)
                Me.CONTARCLIENTESTableAdapter.Connection = CON
                Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, lOC_CONTRATONETDIG, 4, cont2)
                If cont = 1 Then
                    If PPAL = False Then
                        guardabitacoradig(1)
                        Me.CONSULTACONTDIGTableAdapter.Connection = CON
                        Me.CONSULTACONTDIGTableAdapter.Delete(loc_Clv_InicaDig)
                        loc_Clv_InicaDig = 0
                        Me.CREAARBOL()
                    End If
                    If PPAL = True Then
                        If cont2 > 0 Then
                            MsgBox("No se Puede Eliminar el Paquete Principal Sin Antes Haber Eliminado los Paquetes Adicionales", MsgBoxStyle.Information)
                        ElseIf cont2 = 0 Then
                            guardabitacoradig(1)
                            Me.CONSULTACONTDIGTableAdapter.Connection = CON
                            Me.CONSULTACONTDIGTableAdapter.Delete(loc_Clv_InicaDig)
                            loc_Clv_InicaDig = 0
                            Me.CREAARBOL()
                        End If
                    End If

                ElseIf cont = 0 Then
                    MsgBox("El Paquete No Puede ser Eliminado", MsgBoxStyle.Information)
                ElseIf IsNumeric(loc_Clv_InicaDig) = False Then
                    MsgBox("Seleccione el Paquete que desea quitar ", MsgBoxStyle.Information)
                End If

            ElseIf loc_Clv_InicaDig = 0 Then
                MsgBox("Escoja un Paquete a Eliminar", MsgBoxStyle.Information)
            End If
        End If
        CON.Close()
        'bnGeneraOrden.Enabled = True
    End Sub

    'Private Sub ComboBox14_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    'If IsNumeric(ComboBox14.SelectedValue) = True Then
    '    '    If ComboBox14.SelectedValue > 0 Then
    '    '        Dim CON As New SqlConnection(MiConexion)
    '    '        CON.Open()
    '    '        'Me.MuestraServicios_digitalTableAdapter.Connection = CON
    '    '        'Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
    '    '        MuestraServicios_digitalBase2(3, Me.ComboBox14.SelectedValue, GloIdCompania)
    '    '        CON.Close()
    '    '    End If
    '    'End If
    'End Sub


    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        VerBrwOrdSer.Show()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        VerBRWQUEJAS.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If GloActivarCFD = 0 Then
            FrmDatosFiscales.Show()
        Else
            If IsNumeric(Contrato) = True Then
                If CLng(Contrato) > 0 Then
                    FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
                    FacturacionDigitalSoftv.ClassCFDI.LocClv_Ciudad = ""
                    FacturacionDigitalSoftv.ClassCFDI.OpcionCli = OpcionCli
                    FacturacionDigitalSoftv.ClassCFDI.GloContrato = Contrato
                    Dim frm As New FacturacionDigitalSoftv.FrmDatosFiscales
                    frm.ShowDialog()
                End If
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        FrmDatosBanco.Show()
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        eGloContrato = Me.CONTRATOTextBox.Text
        If IdSistema = "LO" Or IdSistema = "YU" Then
            FrmConsultaCobro2.Show()
        Else
            FrmConsultaCobro.Show()
        End If

    End Sub

    Private Sub FECHA_ULT_PAGOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHA_ULT_PAGOTextBox.TextChanged
        Try
            If DateValue(Me.FECHA_ULT_PAGOTextBox.Text) = DateValue("01/01/1900") Then
                Me.FECHA_ULT_PAGOTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub


    Private Sub BasicoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BasicoTextBox.TextChanged
        If IsNumeric(Me.BasicoTextBox.Text) = True Then
            If Me.BasicoTextBox.Text > 0 Then
                Me.Button11.Enabled = True
            End If
        End If
    End Sub

    Private Sub DigitalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DigitalTextBox.TextChanged
        If IsNumeric(Me.DigitalTextBox.Text) = True Then
            If Me.DigitalTextBox.Text > 0 Then
                Me.Button11.Enabled = True
            End If
        End If
    End Sub

    Private Sub TextBox33_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox33.TextChanged

        If IsDate(Mid(Me.TextBox33.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox33.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox33.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox32_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox32.TextChanged

        If IsDate(Mid(Me.TextBox32.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox32.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox32.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox23_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox23.TextChanged

        If IsDate(Mid(Me.TextBox23.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox23.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox23.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox16_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox16.TextChanged

        If IsDate(Mid(Me.TextBox16.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox16.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox16.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox15_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox15.TextChanged

        If IsDate(Mid(Me.TextBox15.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox15.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox15.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox14_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox14.TextChanged

        If IsDate(Mid(Me.TextBox14.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox14.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox14.Clear()
                End If
            Catch
            End Try
        End If
    End Sub

    Private Sub TextBox13_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox13.TextChanged

        If IsDate(Mid(Me.TextBox13.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox13.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox13.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox12_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox12.TextChanged

        If IsDate(Mid(Me.TextBox12.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox12.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox12.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox11_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox11.TextChanged

        If IsDate(Mid(Me.TextBox11.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox11.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox11.Clear()
                End If
            Catch
            End Try
        End If

    End Sub


    Private Sub NOMBRETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NOMBRETextBox.TextChanged
        BndDClientes = True
    End Sub

    Private Sub GUARDARRel_Clientes_TiposClientesGuarda()
        Try

            If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                If Len(Trim(Me.ComboBox7.Text)) > 0 Then
                    If IsNumeric(Me.ComboBox7.SelectedValue) = True Then
                        Dim CON As New SqlConnection(MiConexion)
                        CON.Open()
                        Me.GUARDARRel_Clientes_TiposClientesTableAdapter.Connection = CON
                        Me.GUARDARRel_Clientes_TiposClientesTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_Clientes_TiposClientes, Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue)
                        CON.Close()
                    End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Contrato) = True And Contrato > 0 Then
            GloOpFacturas = 3
            GloIdCompania = ComboBoxCompanias.SelectedValue
            Bwr_FacturasCancelar.Show()
            'FrmHistorialPago.Show()
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button12_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        If Contrato > 0 Then
            My.Forms.FrmLoginCte.Show()
        Else
            MsgBox("Primero Guarda el Cliente", MsgBoxStyle.Information)
        End If
    End Sub
    Dim locLoad As Boolean = False
    Private Sub ComboBox7_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox7.LostFocus
        GloOpPermiso = 0
        GloPermisoCortesia = 0
        'Me.ComboBox7.Enabled = False
    End Sub

    Private Sub ComboBox7_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox7.SelectedIndexChanged
        If locLoad = True Then
            If ComboBox7.SelectedValue = 3 And GloTipoUsuario <> 40 Then
                ComboBox7.SelectedValue = 1
                MsgBox("No cuenta con los permisos para realizar esta acción.")
                Exit Sub
            End If
            If ComboBox7.SelectedValue = 2 Then
                FrmDatosBanco.Show()
            End If
            'Else
            '    locLoad = True
        End If
        If IsNumeric(Me.ComboBox7.SelectedValue) = True Then eClv_TipoCliente = CInt(Me.ComboBox7.SelectedValue)
        'If IdSistema = "VA" Then
        If IsNumeric(Me.ComboBox7.SelectedValue) = True And Me.ComboBox7.SelectedValue = 3 Then

            Me.Button25.Visible = True
            Me.Button26.Visible = True
            Me.Button27.Visible = True

            If BndEsInternet = True Then
                frmInternet2.Hide()
                frmInternet2.Show()
            End If

        ElseIf IsNumeric(Me.ComboBox7.SelectedValue) = True And Me.ComboBox7.SelectedValue <> 3 Then
            Me.Button25.Visible = False
            Me.Button26.Visible = False
            Me.Button27.Visible = False
        End If

        'End If
    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        GloOpPermiso = 0
        GloPermisoCortesia = 0
        bnd_tipopago = True
        FrmAccesopoUsuario.Show()
    End Sub

    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click
        GloOpPermiso = 1
        GloPermisoCortesia = 0
        FrmAccesopoUsuario.Show()
    End Sub

    Private Sub Button17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button17.Click
        GloOpPermiso = 2
        GloPermisoCortesia = 0
        FrmAccesopoUsuario.Show()
    End Sub

    Private Sub Button18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button18.Click
        GloOpPermiso = 3
        GloPermisoCortesia = 0
        FrmAccesopoUsuario.Show()
    End Sub

    Private Sub CortesiaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox.CheckedChanged
        If Me.CortesiaCheckBox.CheckState = CheckState.Checked Then
            cortesiatv = True
        Else
            cortesiatv = False
        End If
    End Sub

    Private Sub CortesiaCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox.Click
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ChecaRelCteDescuentoTableAdapter.Connection = CON
        Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, Me.CONTRATOTextBox.Text, 1, eRes, eMsg)
        CON.Close()
        If eRes = 1 Then
            MsgBox(eMsg)
            Me.CortesiaCheckBox.Checked = False
        End If
    End Sub

    Private Sub CortesiaCheckBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox.LostFocus
        If GloTipoUsuario = 40 Then
            Me.CortesiaCheckBox.Enabled = True
        Else
            Me.CortesiaCheckBox.Enabled = False
        End If
    End Sub

    Private Sub CortesiaCheckBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox1.Click
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ChecaRelCteDescuentoTableAdapter.Connection = CON
        Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, loc_Clv_InicaDig, 3, eRes, eMsg)
        CON.Close()
        If eRes = 1 Then
            MsgBox(eMsg)
            Me.CortesiaCheckBox1.Checked = False
        End If
    End Sub

    Private Sub CortesiaCheckBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox1.LostFocus
        If GloTipoUsuario = 40 Then
            Me.CortesiaCheckBox1.Enabled = True
        Else
            Me.CortesiaCheckBox1.Enabled = False
        End If
    End Sub

    Private Sub CortesiaCheckBox2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox2.Click
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ChecaRelCteDescuentoTableAdapter.Connection = CON
        Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, GloClvUnicaNet, 2, eRes, eMsg)
        CON.Close()
        If eRes = 1 Then
            MsgBox(eMsg)
            Me.CortesiaCheckBox2.Checked = False
        End If
    End Sub

    Private Sub CortesiaCheckBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox2.LostFocus
        If GloTipoUsuario = 40 Then
            Me.CortesiaCheckBox2.Enabled = True
        Else
            Me.CortesiaCheckBox2.Enabled = False
        End If
    End Sub



    Private Sub Button19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button19.Click
        Dim CONT, cont2 As Integer
        Dim PPAL As Boolean
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.Panel6.Visible = True Then
            Me.PrimerMesCLIENTESTableAdapter.Connection = CON
            Me.PrimerMesCLIENTESTableAdapter.Fill(Me.DataSetLidia.PrimerMesCLIENTES, GloClvUnicaNet, 0, CONT, PPAL)
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, GloContratoNet2, 5, cont2)

            If CONT = 1 Then
                If PPAL = False Then
                    guardabitacoranet(2)
                    Me.CONSULTACONTNETTableAdapter.Connection = CON
                    Me.CONSULTACONTNETTableAdapter.Delete(GloClvUnicaNet)
                    Me.CREAARBOL()
                End If
                If PPAL = True Then
                    If cont2 > 0 Then
                        MsgBox("No se Puede Eliminar el Paquete Principal Sin Antes Haber Eliminado los Paquetes Adicionales", MsgBoxStyle.Information)
                    ElseIf cont2 = 0 Then
                        guardabitacoranet(2)
                        Me.CONSULTACONTNETTableAdapter.Connection = CON
                        Me.CONSULTACONTNETTableAdapter.Delete(GloClvUnicaNet)
                        Me.CREAARBOL()
                    End If
                End If


            ElseIf CONT = 0 Then
                MsgBox("No Se Puede Eliminar El Servicio ", MsgBoxStyle.Information)
            End If
        End If
        If Me.Panel5.Visible = True Then
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, GloContratoNet2, 0, CONT)
            If CONT = 0 Then
                guardabitacoranet(2)
                Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
                Me.CONSULTACLIENTESNETTableAdapter.Delete(Contrato, GloContratoNet2)
            ElseIf CONT > 0 Then
                MsgBox("Primero Debes Eliminar Los Servicios ", MsgBoxStyle.Information)
            End If
            Me.CREAARBOL()
        End If
        CON.Close()
    End Sub


    Private Sub NUMEROTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NUMEROTextBox.TextChanged
        BndDClientes = True
    End Sub

    Private Sub ENTRECALLESTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ENTRECALLESTextBox.TextChanged
        BndDClientes = True
    End Sub

    Private Sub TELEFONOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TELEFONOTextBox.KeyPress
        e.KeyChar = ChrW(ValidaKey(TELEFONOTextBox, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TELEFONOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TELEFONOTextBox.TextChanged
        BndDClientes = True
    End Sub




    Private Sub ObsTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ObsTextBox.TextChanged

    End Sub

    Private Sub Label29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label29.Click

    End Sub

    Private Sub ComboBox5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        If Me.ComboBox5.Text = "Contratado" Then
            Me.PrimerMensualidadCheckBox.Checked = True
        Else
            Me.PrimerMensualidadCheckBox.Checked = False
        End If
    End Sub


    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        eEntraUMB = True
        eEntraUM = False
    End Sub

    Private Sub TextBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.LostFocus
        If Me.TextBox2.Text.Length < 3 Then
            MsgBox("Captura el Mes.", , "Atención")
            Me.TextBox2.Focus()
        End If
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        If eEntraUMB = True Then
            If Me.TextBox2.Text.Length = 3 Then

                If Me.TextBox2.Text = "ENE" Then
                    Me.ULTIMO_MESTextBox.Text = 1
                ElseIf Me.TextBox2.Text = "FEB" Then
                    Me.ULTIMO_MESTextBox.Text = 2
                ElseIf Me.TextBox2.Text = "MAR" Then
                    Me.ULTIMO_MESTextBox.Text = 3
                ElseIf Me.TextBox2.Text = "ABR" Then
                    Me.ULTIMO_MESTextBox.Text = 4
                ElseIf Me.TextBox2.Text = "MAY" Then
                    Me.ULTIMO_MESTextBox.Text = 5
                ElseIf Me.TextBox2.Text = "JUN" Then
                    Me.ULTIMO_MESTextBox.Text = 6
                ElseIf Me.TextBox2.Text = "JUL" Then
                    Me.ULTIMO_MESTextBox.Text = 7
                ElseIf Me.TextBox2.Text = "AGO" Then
                    Me.ULTIMO_MESTextBox.Text = 8
                ElseIf Me.TextBox2.Text = "SEP" Then
                    Me.ULTIMO_MESTextBox.Text = 9
                ElseIf Me.TextBox2.Text = "OCT" Then
                    Me.ULTIMO_MESTextBox.Text = 10
                ElseIf Me.TextBox2.Text = "NOV" Then
                    Me.ULTIMO_MESTextBox.Text = 11
                ElseIf Me.TextBox2.Text = "DIC" Then
                    Me.ULTIMO_MESTextBox.Text = 12
                Else
                    MsgBox("El Mes Tecleado es Incorrecto.", , "Error")
                    Me.TextBox2.Clear()
                End If

            End If
        End If
    End Sub



    Private Sub ULTIMO_MESTextBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ULTIMO_MESTextBox.Click
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub ULTIMO_MESTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ULTIMO_MESTextBox.TextChanged
        If eEntraUM = True Then
            If IsNumeric(ULTIMO_MESTextBox.Text) = True Then
                If Me.ULTIMO_MESTextBox.Text = 1 Then
                    Me.TextBox2.Text = "ENE"
                ElseIf Me.ULTIMO_MESTextBox.Text = 2 Then
                    Me.TextBox2.Text = "FEB"
                ElseIf Me.ULTIMO_MESTextBox.Text = 3 Then
                    Me.TextBox2.Text = "MAR"
                ElseIf Me.ULTIMO_MESTextBox.Text = 4 Then
                    Me.TextBox2.Text = "ABR"
                ElseIf Me.ULTIMO_MESTextBox.Text = 5 Then
                    Me.TextBox2.Text = "MAY"
                ElseIf Me.ULTIMO_MESTextBox.Text = 6 Then
                    Me.TextBox2.Text = "JUN"
                ElseIf Me.ULTIMO_MESTextBox.Text = 7 Then
                    Me.TextBox2.Text = "JUL"
                ElseIf Me.ULTIMO_MESTextBox.Text = 8 Then
                    Me.TextBox2.Text = "AGO"
                ElseIf Me.ULTIMO_MESTextBox.Text = 9 Then
                    Me.TextBox2.Text = "SEP"
                ElseIf Me.ULTIMO_MESTextBox.Text = 10 Then
                    Me.TextBox2.Text = "OCT"
                ElseIf Me.ULTIMO_MESTextBox.Text = 11 Then
                    Me.TextBox2.Text = "NOV"
                ElseIf Me.ULTIMO_MESTextBox.Text = 12 Then
                    Me.TextBox2.Text = "DIC"
                Else
                    Me.TextBox2.Text = "---"
                End If
            End If
        End If
    End Sub


    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        eEntraUMB = True
        eEntraUM = False
    End Sub

    Private Sub TextBox3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox3.LostFocus
        If Me.TextBox3.Text.Length < 3 Then
            MsgBox("Captura el Mes.", , "Atención")
            Me.TextBox3.Focus()
        End If
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged
        If eEntraUMB = True Then
            If Me.TextBox3.Text.Length = 3 Then

                If Me.TextBox3.Text = "ENE" Then
                    Me.TextBox10.Text = 1
                ElseIf Me.TextBox3.Text = "FEB" Then
                    Me.TextBox10.Text = 2
                ElseIf Me.TextBox3.Text = "MAR" Then
                    Me.TextBox10.Text = 3
                ElseIf Me.TextBox3.Text = "ABR" Then
                    Me.TextBox10.Text = 4
                ElseIf Me.TextBox3.Text = "MAY" Then
                    Me.TextBox10.Text = 5
                ElseIf Me.TextBox3.Text = "JUN" Then
                    Me.TextBox10.Text = 6
                ElseIf Me.TextBox3.Text = "JUL" Then
                    Me.TextBox10.Text = 7
                ElseIf Me.TextBox3.Text = "AGO" Then
                    Me.TextBox10.Text = 8
                ElseIf Me.TextBox3.Text = "SEP" Then
                    Me.TextBox10.Text = 9
                ElseIf Me.TextBox3.Text = "OCT" Then
                    Me.TextBox10.Text = 10
                ElseIf Me.TextBox3.Text = "NOV" Then
                    Me.TextBox10.Text = 11
                ElseIf Me.TextBox3.Text = "DIC" Then
                    Me.TextBox10.Text = 12
                Else
                    MsgBox("El Mes Tecleado es Incorrecto.", , "Error")
                    Me.TextBox3.Clear()
                End If

            End If
        End If
    End Sub

    Private Sub TextBox10_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox10.Click
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub TextBox10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox10.TextChanged
        If eEntraUM = True Then
            If IsNumeric(TextBox10.Text) = True Then
                If Me.TextBox10.Text = 1 Then
                    Me.TextBox3.Text = "ENE"
                ElseIf Me.TextBox10.Text = 2 Then
                    Me.TextBox3.Text = "FEB"
                ElseIf Me.TextBox10.Text = 3 Then
                    Me.TextBox3.Text = "MAR"
                ElseIf Me.TextBox10.Text = 4 Then
                    Me.TextBox3.Text = "ABR"
                ElseIf Me.TextBox10.Text = 5 Then
                    Me.TextBox3.Text = "MAY"
                ElseIf Me.TextBox10.Text = 6 Then
                    Me.TextBox3.Text = "JUN"
                ElseIf Me.TextBox10.Text = 7 Then
                    Me.TextBox3.Text = "JUL"
                ElseIf Me.TextBox10.Text = 8 Then
                    Me.TextBox3.Text = "AGO"
                ElseIf Me.TextBox10.Text = 9 Then
                    Me.TextBox3.Text = "SEP"
                ElseIf Me.TextBox10.Text = 10 Then
                    Me.TextBox3.Text = "OCT"
                ElseIf Me.TextBox10.Text = 11 Then
                    Me.TextBox3.Text = "NOV"
                ElseIf Me.TextBox10.Text = 12 Then
                    Me.TextBox3.Text = "DIC"
                Else
                    Me.TextBox3.Text = "---"
                End If
            End If
        End If
    End Sub


    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        eEntraUMB = True
        eEntraUM = False
    End Sub

    Private Sub TextBox5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox5.LostFocus
        If Me.TextBox5.Text.Length < 3 Then
            MsgBox("Captura el Mes.", , "Atención")
            Me.TextBox5.Focus()
        End If
    End Sub

    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        If eEntraUMB = True Then
            If Me.TextBox5.Text.Length = 3 Then

                If Me.TextBox5.Text = "ENE" Then
                    Me.Ultimo_mesTextBox1.Text = 1
                ElseIf Me.TextBox5.Text = "FEB" Then
                    Me.Ultimo_mesTextBox1.Text = 2
                ElseIf Me.TextBox5.Text = "MAR" Then
                    Me.Ultimo_mesTextBox1.Text = 3
                ElseIf Me.TextBox5.Text = "ABR" Then
                    Me.Ultimo_mesTextBox1.Text = 4
                ElseIf Me.TextBox5.Text = "MAY" Then
                    Me.Ultimo_mesTextBox1.Text = 5
                ElseIf Me.TextBox5.Text = "JUN" Then
                    Me.Ultimo_mesTextBox1.Text = 6
                ElseIf Me.TextBox5.Text = "JUL" Then
                    Me.Ultimo_mesTextBox1.Text = 7
                ElseIf Me.TextBox5.Text = "AGO" Then
                    Me.Ultimo_mesTextBox1.Text = 8
                ElseIf Me.TextBox5.Text = "SEP" Then
                    Me.Ultimo_mesTextBox1.Text = 9
                ElseIf Me.TextBox5.Text = "OCT" Then
                    Me.Ultimo_mesTextBox1.Text = 10
                ElseIf Me.TextBox5.Text = "NOV" Then
                    Me.Ultimo_mesTextBox1.Text = 11
                ElseIf Me.TextBox5.Text = "DIC" Then
                    Me.Ultimo_mesTextBox1.Text = 12
                Else
                    MsgBox("El Mes Tecleado es Incorrecto.", , "Error")
                    Me.TextBox5.Clear()
                End If

            End If
        End If
    End Sub

    Private Sub Ultimo_mesTextBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ultimo_mesTextBox1.Click
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub Ultimo_mesTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ultimo_mesTextBox1.TextChanged
        If eEntraUM = True Then
            If IsNumeric(Ultimo_mesTextBox1.Text) = True Then
                If Me.Ultimo_mesTextBox1.Text = 1 Then
                    Me.TextBox5.Text = "ENE"
                ElseIf Me.Ultimo_mesTextBox1.Text = 2 Then
                    Me.TextBox5.Text = "FEB"
                ElseIf Me.Ultimo_mesTextBox1.Text = 3 Then
                    Me.TextBox5.Text = "MAR"
                ElseIf Me.Ultimo_mesTextBox1.Text = 4 Then
                    Me.TextBox5.Text = "ABR"
                ElseIf Me.Ultimo_mesTextBox1.Text = 5 Then
                    Me.TextBox5.Text = "MAY"
                ElseIf Me.Ultimo_mesTextBox1.Text = 6 Then
                    Me.TextBox5.Text = "JUN"
                ElseIf Me.Ultimo_mesTextBox1.Text = 7 Then
                    Me.TextBox5.Text = "JUL"
                ElseIf Me.Ultimo_mesTextBox1.Text = 8 Then
                    Me.TextBox5.Text = "AGO"
                ElseIf Me.Ultimo_mesTextBox1.Text = 9 Then
                    Me.TextBox5.Text = "SEP"
                ElseIf Me.Ultimo_mesTextBox1.Text = 10 Then
                    Me.TextBox5.Text = "OCT"
                ElseIf Me.Ultimo_mesTextBox1.Text = 11 Then
                    Me.TextBox5.Text = "NOV"
                ElseIf Me.Ultimo_mesTextBox1.Text = 12 Then
                    Me.TextBox5.Text = "DIC"
                Else
                    Me.TextBox5.Text = "---"
                End If
            End If
        End If
    End Sub

    Private Sub Button11_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Button11.KeyPress
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub Button7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Button7.KeyPress
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub Button8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Button8.KeyPress
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub PRIMERMENSUALIDACheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PRIMERMENSUALIDACheckBox.CheckedChanged

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub

    Private Sub PrimerMensualidadCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrimerMensualidadCheckBox.CheckedChanged

    End Sub

    Private Sub CortesiaCheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox2.CheckedChanged
        If Me.CortesiaCheckBox2.CheckState = CheckState.Checked Then
            cortesiaint = True
        ElseIf Me.CortesiaCheckBox2.CheckState = CheckState.Unchecked Then
            cortesiaint = False
        End If
    End Sub

    Private Sub Button20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button20.Click
        opcFrm = 12
        bloqueado = 1
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.Contrato = CInt(CONTRATOTextBox.Text)
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub Button21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button21.Click
        opcFrm = 14
        eOpcion = OpcionCli
        eGloContrato = Me.CONTRATOTextBox.Text
        'eRobo = True
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub Button22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button22.Click
        If Me.CortesiaCheckBox.Checked = False Then
            eTipSer = 1
            If eAccesoAdmin = True Then
                eContrato = Me.CONTRATOTextBox.Text
                FrmRelCteDescuento.Show()
            Else
                eGloDescuento = 0
                eGloTipSerDesc = 2
                FrmAccesopoUsuario.Show()
            End If
        Else
            MsgBox("No Puede Aplicar un Descuento, ya que cuenta con una Cortesía", , "Atención")
        End If
    End Sub

    Private Sub Button23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button23.Click
        If Me.CortesiaCheckBox2.Checked = False Then
            eTipSer = 2
            If eAccesoAdmin = True Then
                eClv_UnicaNet = GloClvUnicaNet
                FrmRelCteDescuento.Show()
            Else
                eGloDescuento = 0
                eGloTipSerDesc = 2
                FrmAccesopoUsuario.Show()
            End If
        Else
            MsgBox("No Puede Aplicar un Descuento, ya que cuenta con una Cortesía", , "Atención")
        End If
    End Sub

    Private Sub Button24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button24.Click
        If Me.CortesiaCheckBox1.Checked = False Then
            eTipSer = 3
            If eAccesoAdmin = True Then
                eClv_UnicaNetDig = loc_Clv_InicaDig
                FrmRelCteDescuento.Show()
            Else
                eGloDescuento = 0
                eGloTipSerDesc = 3
                FrmAccesopoUsuario.Show()
            End If
        Else
            MsgBox("No Puede Aplicar un Descuento, ya que cuenta con una Cortesía", , "Atención")
        End If
    End Sub


    Private Sub CortesiaCheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox1.CheckedChanged
        If Me.CortesiaCheckBox1.CheckState = CheckState.Checked Then
            cortesiadig = True
        ElseIf Me.CortesiaCheckBox1.CheckState = CheckState.Unchecked Then
            cortesiadig = False
        End If
    End Sub

    Private Sub BuscaDescTV()
        Dim CON2 As New SqlConnection(MiConexion)
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
            CON2.Open()
            Me.ChecaRelCteDescuentoTableAdapter.Connection = CON2
            Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, Me.CONTRATOTextBox.Text, 1, eRes, eMsg)
            CON2.Close()
            If eRes = 1 Then
                CON2.Open()
                Me.ConRelCteDescuentoTableAdapter.Connection = CON2
                Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, Me.CONTRATOTextBox.Text, 1)
                CON2.Close()
                Me.DescuentoLabel2.Visible = True
                Me.Label40.Visible = True
            Else
                Me.DescuentoLabel2.Visible = False
                Me.Label40.Visible = False
            End If
        End If
    End Sub

    Private Sub BuscaDescNet()
        Dim CON2 As New SqlConnection(MiConexion)
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        If GloClvUnicaNet > 0 Then
            CON2.Open()
            Me.ChecaRelCteDescuentoTableAdapter.Connection = CON2
            Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, GloClvUnicaNet, 2, eRes, eMsg)
            CON2.Close()
            If eRes = 1 Then
                CON2.Open()
                Me.ConRelCteDescuentoTableAdapter.Connection = CON2
                Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, GloClvUnicaNet, 2)
                CON2.Close()
                Me.DescuentoLabel3.Visible = True
                Me.Label44.Visible = True
            Else
                Me.DescuentoLabel3.Visible = False
                Me.Label44.Visible = False
            End If
        End If
    End Sub

    Private Sub BuscaDescDig()
        Dim CON2 As New SqlConnection(MiConexion)
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        If loc_Clv_InicaDig > 0 Then
            CON2.Open()
            Me.ChecaRelCteDescuentoTableAdapter.Connection = CON2
            Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, loc_Clv_InicaDig, 3, eRes, eMsg)
            CON2.Close()
            If eRes = 1 Then
                CON2.Open()
                Me.ConRelCteDescuentoTableAdapter.Connection = CON2
                Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, loc_Clv_InicaDig, 3)
                CON2.Close()
                Me.DescuentoLabel1.Visible = True
                Me.Label39.Visible = True
            Else
                Me.DescuentoLabel1.Visible = False
                Me.Label39.Visible = False
            End If
        End If
    End Sub

    Private Sub Button27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button27.Click
        LServicio = Me.Label12.Text
        LClv_Servicio = CInt(Me.Label45.Text)
        LTip_Ser = 3
        LClvUnica = loc_Clv_InicaDig
        FrmPreciosHoteles.Show()
    End Sub

    Private Sub Button25_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button25.Click
        LClv_Servicio = CInt(Me.TipSerTvComboBox.SelectedValue)
        LServicio = Me.TipSerTvComboBox.Text
        LTXTServ = Me.TipSerTvComboBox.SelectedValue
        LTip_Ser = 1
        LClvUnica = Contrato
        FrmPreciosHoteles.Show()
    End Sub

    Private Sub Button26_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button26.Click
        LClv_Servicio = CInt(Me.Clv_ServicioTextBox.Text)
        LServicio = Me.DESCRIPCIONLabel1.Text
        LTip_Ser = 2
        LClvUnica = GloClvUnicaNet
        FrmPreciosHoteles.Show()
    End Sub


    Private Sub FECHA_SOLICITUDTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHA_SOLICITUDTextBox.TextChanged
        Try
            If DateValue(Me.FECHA_SOLICITUDTextBox.Text) = DateValue("01/01/1900") Then
                Me.FECHA_SOLICITUDTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub


    Private Sub muestra_telefonia()
        'Contrato = Me.CONTRATOTextBox.Text
        'FrmCtrl_ServiciosCli.Show()
        'Me.Button8.Enabled = True
        'Me.Button7.Enabled = True
        'Me.Button28.Enabled = False
        BndEsInternet = True
        frmctr.MdiParent = Me
        frmInternet2.MdiParent = Me
        frmctr.WindowState = FormWindowState.Normal
        frmInternet2.Show()
        frmctr.Show()
        frmctr.Boton_Telefonia()
        frmctr.TreeView1.ExpandAll()
        Me.Panel2.Hide()
        Me.Panel4.Hide()
        Me.Panel7.Hide()
        'If Me.Button8.Enabled = False And Me.Button28.Visible = True And IdSistema = "LO" Then
        ' Me.Button8.Enabled = True
        'Me.Button28.Enabled = False
        'End If

    End Sub

    Private Sub Button28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button28.Click

        If Me.Button8.Visible = True Then Me.Button8.Enabled = True
        If Me.Button7.Visible = True Then Me.Button7.Enabled = True
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = False



        If Me.Button11.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Panel2.Hide()
            Me.Panel4.Hide()
            Me.Panel7.Hide()
            'If Me.Button28.Enabled = False And Me.Button28.Visible = True And (IdSistema = "LO" Or IdSistema = "YU") Then
            '    Me.Button28.Enabled = True
            '    Button8.Enabled = False
            'End If
        ElseIf Me.Button11.Text.Contains("&Televis") Then

            BndEsInternet = False
            Me.Panel2.Visible = True
            Me.Panel4.Visible = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

        ElseIf Me.Button11.Text = "IPTV" Or Me.Button11.Text = "&Premium" Then
            BndEsInternet = False
            Me.Panel2.Visible = False
            Me.Panel4.Visible = False
            Me.Panel7.Visible = True
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

            'If IsNumeric(ComboBox14.SelectedValue) = True Then
            '    If ComboBox14.SelectedValue > 0 Then
            '        Dim CON As New SqlConnection(MiConexion)
            '        CON.Open()
            '        'Me.MuestraServicios_digitalTableAdapter.Connection = CON
            '        'Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
            '        MuestraServicios_digitalBase2(3, Me.ComboBox14.SelectedValue, GloIdCompania)
            '        CON.Close()
            '    End If
            'End If
        ElseIf Me.Button11.Text = "&Telefonia" Then
            muestra_telefonia()
        End If
        'Eric----------
        BuscaDescTV()
        '--------------
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False

    End Sub

    Private Sub Label41_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Label41_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Label41.KeyPress
        BanderaCambio = 1
    End Sub

    Private Sub Label41_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label41.TextChanged

    End Sub

    Private Sub BindingNavigator4_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigator4.RefreshItems

    End Sub

    Private Sub MACCABLEMODEMLabel1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MACCABLEMODEMLabel1.KeyPress
        BanderaCambio = 1
    End Sub

    Private Sub MACCABLEMODEMLabel1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MACCABLEMODEMLabel1.TextChanged

    End Sub

    Private Sub Button29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button29.Click
        FrmReferencias.contratoRef = CLng(Me.CONTRATOTextBox.Text)
        FrmReferencias.contratoRefCom = Me.TextBoxNoContrato.Text
        FrmReferencias.tipoRef = "C"
        FrmReferencias.Show()
    End Sub

    Private Sub ValidaDigital2(ByVal ContratoNet As Long, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaDigital2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ContratoNet
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Servicio
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msg", SqlDbType.Char, 250)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = 0
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eResValida = CInt(parametro3.Value)
            eMsgValida = parametro4.Value
            conexion.Close()
        Catch ex As Exception

            conexion.Close()
            MsgBox(ex.Message)


        End Try



    End Sub



    Private Sub ValidaDatosBancarios(ByVal Contrato As Long, ByVal Clv_TipoCliente As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaDatosBancarios", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipoCliente
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msg", SqlDbType.VarChar, 500)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = ""
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eResValida = CInt(parametro3.Value.ToString)
            eMsgValida = parametro4.Value.ToString
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try



    End Sub

    Private Sub ValidaNombreUsuario(ByVal Nombre As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaNombreUsuario", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Nombre", SqlDbType.VarChar, 250)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Nombre
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = ""
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eResValida = CInt(parametro2.Value.ToString)
            eMsgValida = parametro3.Value.ToString
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try


    End Sub
    'SAUL VALIDA NOMBRE
    Private Sub ValidaNombreUsuario2()
        ' NOMBRETextBox.Text = frmnsb.txtNombre.Text + " " + frmnsb.txtSegundoNombre.Text + " " + frmnsb.txtApellidoPaterno.Text + " " + frmnsb.txtApellidoMaterno.Text
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaNombreUsuario2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Nombre", SqlDbType.VarChar, 250)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = frmnsb.txtNombre.Text.ToString
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@ApellidoP", SqlDbType.VarChar, 250)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = frmnsb.txtApellidoPaterno.Text.ToString
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ApellidoM", SqlDbType.VarChar, 250)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = frmnsb.txtApellidoMaterno.Text.ToString
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Res", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = 0
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro5.Direction = ParameterDirection.Output
        parametro5.Value = ""
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eResValida = CInt(parametro4.Value.ToString)
            eMsgValida = parametro5.Value.ToString
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try


    End Sub
    'SAUL VALIDA NOMBRE(FIN)


    Private Sub CELULARTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CELULARTextBox.KeyPress
        e.KeyChar = ChrW(ValidaKey(Me.CELULARTextBox, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TextBox7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox7.TextChanged

    End Sub

    Private Sub Button30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button30.Click
        FrmRefBan.Show()
    End Sub

    Private Sub BuscaCuenta()
        If IdSistema = "LO" Or IdSistema = "YU" Then
            'If Contrato = Nothing Or Contrato = 0 Then
            '    Contrato = CLng(Me.CONTRATOTextBox.Text)
            'End If
            If OpcionCli = "N" Then
                Contrato = CLng(Me.CONTRATOTextBox.Text)
            End If

            Dim con As New SqlConnection(MiConexion)
            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("CONSULTA_RelCuenta", con)
            cmd.CommandType = CommandType.StoredProcedure

            Dim prm As New SqlParameter( _
                      "@Contrato", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = Contrato
            cmd.Parameters.Add(prm)

            Try
                con.Open()
                reader = cmd.ExecuteReader()
                Using reader
                    While reader.Read
                        Me.TextBox28.Text = reader.GetValue(0)
                    End While
                End Using
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            con.Close()
        End If
    End Sub

    Private Sub GuardaCuenta()
        If OpcionCli <> "N" Then
            Dim con As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("GUARDA_RelCuenta", con)
            cmd.CommandType = CommandType.StoredProcedure

            Dim prm As New SqlParameter( _
                      "@Contrato", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = Contrato
            cmd.Parameters.Add(prm)

            prm = New SqlParameter( _
                     "@Cuenta", SqlDbType.VarChar, 50)
            prm.Direction = ParameterDirection.Input
            prm.Value = Me.TextBox28.Text
            cmd.Parameters.Add(prm)

            Try
                con.Open()
                Dim i As Integer = cmd.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            con.Close()
        End If
    End Sub

    Private Sub Panel11_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel11.Paint

    End Sub

    Private Sub DameComboYRenta(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameComboYRenta", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@DescCombo", SqlDbType.VarChar, 150)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = String.Empty
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            'Me.LabelComboYRenta.Text = parametro2.Value.ToString
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Private Sub BtnEstadoDeCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEstadoDeCuenta.Click
        LocGloContratoIni = Me.CONTRATOTextBox.Text
        LocGloContratoFin = Me.CONTRATOTextBox.Text
        LocbndImpresionEdoCuentaLog = True
        FrmImprimirContrato.Show()
    End Sub

    Private Sub Button31_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button31.Click
        FrmClienteObs.Show()
    End Sub
    Private Sub Muestra_Usuarios(ByVal clv_unicanet As Long, ByVal tipo_serv As Integer)
        If OpcionCli = "C" Then
            Me.ComboBox20.Enabled = False
        End If
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec Muestra_Usuarios ")
        str.Append(CStr(clv_unicanet) & ", ")
        str.Append(CStr(tipo_serv))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.ComboBox20.DataSource = binding
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()
        End Try


    End Sub
    Private Sub Actualiza_usuario(ByVal clv_unicanet As Long, ByVal clv_usuario As Integer, ByVal tipo_serv As Integer)
        If OpcionCli = "M" Then
            Dim con As New SqlConnection(MiConexion)
            Dim str As New StringBuilder

            str.Append("Exec Actualiza_usuarios ")
            str.Append(CStr(clv_unicanet) & ", ")
            str.Append(CStr(clv_usuario) & ", ")
            str.Append(CStr(tipo_serv))

            Dim dataadapter As New SqlDataAdapter(str.ToString, con)
            Dim datatable As New DataTable
            Dim binding As New BindingSource

            Try
                con.Open()
                dataadapter.Fill(datatable)
                binding.DataSource = datatable

                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cambio de Usuario. Clv_TipSer: " & CStr(tipo_serv) & " Clv_UnicaNet: " & CStr(clv_unicanet), eUsuario, Me.ComboBox20.Text, LocClv_Ciudad)
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            Finally
                con.Close()
                con.Dispose()
            End Try
        End If
    End Sub

    Private Sub ComboBox17_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Muestra_Usuarios(GloClvUnicaNet)
    End Sub

    Private Sub FECHA_ULT_PAGOLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ComboBox18_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox18.SelectedIndexChanged
        'Muestra_Usuarios(GloClvUnicaNet)
    End Sub

    Private Sub Panel6_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel6.Paint

    End Sub


    Private Sub btnHisDes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHisDes.Click
        Dim desconexiones As BitacoraSuspendidos = New BitacoraSuspendidos
        desconexiones.Show()
    End Sub

    Private Function ValidaEliminarServicios(ByVal Clv_TipSer As Integer, ByVal Clv_Unicanet As Long) As Boolean
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaEliminarServicios", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_TipSer
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Unicanet", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Unicanet
        comando.Parameters.Add(parametro2)

        Dim parametro4 As New SqlParameter("@contrato", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Contrato
        comando.Parameters.Add(parametro4)

        Dim parametro3 As New SqlParameter("@Bnd", SqlDbType.Bit)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

            Return parametro3.Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Function

    Private Sub Button32_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button32.Click
        If CONTRATOTextBox.Text.ToString.Length > 0 Then
            Dim relacion As FrmRelClienteInfo = New FrmRelClienteInfo
            relacion.inicia(CONTRATOTextBox.Text.ToString)
            relacion.ShowDialog()
        Else
            MsgBox("Se debe estar consultando un cliente para poder agregarle información", MsgBoxStyle.Critical)
        End If
    End Sub

    Private Sub ChecaOrdenRetiro(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaOrdenRetiro", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msj", SqlDbType.VarChar, 250)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = String.Empty
            eRes = CInt(parametro2.Value)
            eMsj = parametro3.Value.ToString
            If eRes = 1 Then FrmMsj.Show()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub MuestraPagosDif(ByVal Contrato As Long, ByVal Clv_TipSer As Integer, ByVal Clv_UnicaNet As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MuestraPagosDif", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Res", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Bnd", SqlDbType.Bit)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = String.Empty
            eBnd = False
            eRes = CInt(parametro4.Value)
            eMsj = parametro5.Value.ToString()
            eBnd = parametro6.Value.ToString()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ValidaCambioPagosDif(ByVal Contrato As Long, ByVal Clv_TipoCliente As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaCambioPagosDif", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipoCliente
        comando.Parameters.Add(parametro2)

        Dim parametro4 As New SqlParameter("@Res", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = String.Empty
            eRes = CInt(parametro4.Value)
            eMsj = parametro5.Value.ToString()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub


    Private Sub ValidaPagosDif(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaPagosDif", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = 0
            eRes = parametro2.Value
            eMsj = parametro3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ConClientesDigDec(ByVal ContratoNet As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC ConClientesDigDec " & CStr(ContratoNet))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            comboBoxDec.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub NueClientesDigDec(ByVal Id As Integer, ByVal ContratoNet As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueClientesDigDec", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Id", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Id
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = ContratoNet
        comando.Parameters.Add(par2)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ConsultaAparatosContratacion()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC ConsultaAparatosContratacion ")
        If Len(Me.ComboBox12.SelectedValue) = 0 Then
            StrSql.Append(CStr(0))
        Else
            StrSql.Append(CStr(Me.ComboBox12.SelectedValue))
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSql.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.AparatosComboBox.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub DimeSiEsPrincipal()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DimeSiEsPrincipal", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CLV_SERVICIO", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        If Len(Me.ComboBox12.SelectedValue) = 0 Then
            PRM1.Value = 0
        Else
            PRM1.Value = Me.ComboBox12.SelectedValue
        End If
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@PRINCIPAL", SqlDbType.Bit)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            If PRM2.Value = True Then
                Me.AparatosComboBox.Visible = True
                Me.Label55.Visible = True
            Else
                Me.AparatosComboBox.Visible = False
                Me.Label55.Visible = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ComboBox12_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox12.SelectedIndexChanged
        DimeSiEsPrincipal()
        ConsultaAparatosContratacion()
    End Sub


    Private Sub GuardaRelClientesAparatos(ByVal ContratoAparato As Integer, ByVal ContratoNetAparato As Integer, ByVal ClvServicioAparato As Integer, ByVal IdArticuloAparato As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("GuardaRelClientesAparatos", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CONTRATO", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = ContratoAparato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@CONTRATONET", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = ContratoNetAparato
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@CLV_SERVICIO", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = ClvServicioAparato
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@IDARTICULO", SqlDbType.Int)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = IdArticuloAparato
        CMD.Parameters.Add(PRM4)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ConsultaRelClientesAparatos(ByVal ContratoNetAparato As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("CosultaRelClientesAparatos", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CONTRATO", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = Me.CONTRATOTextBox.Text
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@ContratoNet", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = ContratoNetAparato
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@MARCA", SqlDbType.VarChar, 250)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            Me.MarcaAparatoTextBox.Text = PRM3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub Label15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnCapturarNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCapturarNombre.Click

        If (frmnsb.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            NOMBRETextBox.Text = frmnsb.txtNombre.Text + " " + frmnsb.txtSegundoNombre.Text + " " + frmnsb.txtApellidoPaterno.Text + " " + frmnsb.txtApellidoMaterno.Text
            If (OpcionCli = "M") Then
                GuardaClientesApellidos()
            End If
        End If

    End Sub

    Private Sub MuestraPromocionAntiguedadCliente(ByVal Contrato As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("MuestraPromocionAntiguedadCliente", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CONTRATO", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = Contrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@Msj", SqlDbType.VarChar, 750)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)



        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            If PRM2.Value.ToString.Length = 0 Then
                CMBlbPromocion.Visible = False
                CMBlbPromocion.Text = ""
            Else
                CMBlbPromocion.Visible = True
                CMBlbPromocion.Text = PRM2.Value.ToString
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub validaClienteEnBuro()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Existe", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
        'Dim dico As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("USP_ClienteListadoBuro")
        'If (CBool(dico("@Existe"))) Then
        '    BtnDetalleBuroDeCredito.Visible = True
        'End If
    End Sub

    Private Sub BtnDetalleBuroDeCredito_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDetalleBuroDeCredito.Click
        If OpcionCli = "M" Or OpcionCli = "C" Then
            Dim frm As New FrmDetalleClienteBuro()
            frm.Contrato = CLng(CONTRATOTextBox.Text)
            frm.ShowDialog()
        End If
    End Sub

    Private Sub bnRetencion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnRetencion.Click
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.Contrato = CInt(CONTRATOTextBox.Text)
        SoftvMod.VariablesGlobales.Clv_Usuario = GloUsuario
        Dim frm As New SoftvMod.FrmClientesRetenciones
        frm.ShowDialog()
    End Sub

    Private Sub bnNivelDeRiesgo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnNivelDeRiesgo.Click
        SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.Contrato = CInt(CONTRATOTextBox.Text)
        Dim frm As New SoftvMod.FrmClientesNivelesDeRiesgo
        frm.ShowDialog()
    End Sub

    Private Sub PreguntaSiQuiereCombo()
        'Temporalmente 
        Exit Sub

        If SoloInternetCheckBox.Checked = True Then Exit Sub
        If MessageBox.Show("¿Deseas contratar un Combo?", "Atención", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then

            GloIdCompania = ComboBoxCompanias.SelectedValue

            SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
            SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
            SoftvMod.VariablesGlobales.MiConexion = MiConexion
            SoftvMod.VariablesGlobales.Contrato = CInt(CONTRATOTextBox.Text)
            SoftvMod.VariablesGlobales.Clv_Usuario = GloUsuario
            SoftvMod.VariablesGlobales.Clv_TipoCliente = CInt(ComboBox7.SelectedValue)
            SoftvMod.VariablesGlobales.IdCompania = CInt(ComboBoxCompanias.SelectedValue)
            SoftvMod.VariablesGlobales.TvsinPago = GloTvSinPago
            SoftvMod.VariablesGlobales.TvconPago = GloTvConpago

            SoftvMod.VariablesGlobales.GloClvEstado = GloClvEstado
            SoftvMod.VariablesGlobales.GloClvCiudad = GloClvCiudad

            'Dim frm As New SoftvMod.Teziutlan.FrmComboPantallaDeClientesTezi
            Dim frm As New SoftvMod.Torreon.FrmComboPantallaDeClientes
            frm.ShowDialog()
            If frm.SiContrataCombo = True Then
                HABILITA_CTRS()
                buscaCONCLIENTETV()
            End If
        End If
    End Sub


    Private Sub VALIDATipSerCliente(ByVal Contrato As Integer)



        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("VALIDATipSerCliente", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CONTRATO", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = Contrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@Tv", SqlDbType.Bit)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@Internet", SqlDbType.Bit)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@Digital", SqlDbType.Bit)
        PRM4.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM4)



        Try
            CON.Open()
            CMD.ExecuteNonQuery()


            Tv = False
            Internet = False
            Digital = False

            Tv = PRM2.Value
            Internet = PRM3.Value
            Digital = PRM4.Value


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub uspConsultaTap(ByVal prmContrato As Long, ByVal prmClvCalle As Integer, ByVal prmClvColonia As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim STR As New StringBuilder

        STR.Append("EXEC uspConsultaTap ")
        STR.Append(CStr(prmContrato) & ", ")
        STR.Append(CStr(prmClvCalle) & ", ")
        STR.Append(CStr(prmClvColonia))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(STR.ToString, CON)

        Try
            CON.Open()
            DA.Fill(DT)
            Me.cmbTap.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvCalle", SqlDbType.Int, prmClvCalle)
            BaseII.CreateMyParameter("@clvColonia", SqlDbType.Int, prmClvColonia)
            cmbTap.DataSource = BaseII.ConsultaDT("ObtieneTapColoniaCalle")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvCalle", SqlDbType.Int, prmClvCalle)
            BaseII.CreateMyParameter("@clvColonia", SqlDbType.Int, prmClvColonia)
            cmbNap.DataSource = BaseII.ConsultaDT("ObtieneNapColoniaCalle")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub uspDameTapCliente(ByVal prmContrato As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("uspDamePlacaTapCliente ", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = prmContrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@clvTap", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@placa", SqlDbType.VarChar, 50)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            'Me.cmbTap.SelectedValue = PRM2.Value
            Me.txtPlaca.Text = PRM3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

        'Llenar tap y nap
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, Convert.ToInt32(Contrato))
        BaseII.CreateMyParameter("@tap", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@nap", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("TieneTapNap")
        Dim tienetap As Integer = Convert.ToInt32(BaseII.dicoPar("@tap"))
        Dim tienenap As Integer = Convert.ToInt32(BaseII.dicoPar("@nap"))

        If tienetap = 1 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, Convert.ToInt32(Contrato))
            BaseII.CreateMyParameter("@Clv_Tap", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("TraerTapCliente")
            If BaseII.dicoPar("@Clv_Tap") > 0 Then
                cmbTap.SelectedValue = BaseII.dicoPar("@Clv_Tap")
            End If
        End If

        If tienenap = 1 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, Convert.ToInt32(Contrato))
            BaseII.CreateMyParameter("@IdTap", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("TraerNapCliente")
            If BaseII.dicoPar("@IdTap") > 0 Then
                cmbNap.SelectedValue = BaseII.dicoPar("@IdTap")
            End If
        End If
    End Sub

    Private Sub ObtieneTapNapNuevo()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvCalle", SqlDbType.Int, CALLEComboBox.SelectedValue)
            BaseII.CreateMyParameter("@clvColonia", SqlDbType.Int, COLONIAComboBox.SelectedValue)
            cmbTap.DataSource = BaseII.ConsultaDT("ObtieneTapColoniaCalle")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvCalle", SqlDbType.Int, CALLEComboBox.SelectedValue)
            BaseII.CreateMyParameter("@clvColonia", SqlDbType.Int, COLONIAComboBox.SelectedValue)
            cmbNap.DataSource = BaseII.ConsultaDT("ObtieneNapColoniaCalle")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub uspGuardaPlacaTapCliente(ByVal prmContrato As Long, ByVal prmClvTap As Integer, ByVal prmPlaca As String, ByVal prmClvOrden As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("uspGuardaPlacaTapCliente", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = prmContrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@clvTap", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = prmClvTap
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@placa", SqlDbType.VarChar, 250)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = prmPlaca
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@clvOrden", SqlDbType.BigInt)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = prmClvOrden
        CMD.Parameters.Add(PRM4)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub MUESTRATipoCaja(ByVal idcompania As Integer)
        'Dim CON As New SqlConnection(MiConexion)
        'Dim STR As New StringBuilder

        'STR.Append("EXEC MUESTRATipoCaja ")

        'Dim DT As New DataTable
        'Dim DA As New SqlDataAdapter(STR.ToString, CON)

        'Try
        '    CON.Open()
        '    DA.Fill(DT)
        '    cbTipoCaja.DataSource = DT
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Information)
        'Finally
        '    CON.Close()
        '    CON.Dispose()
        'End Try
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, idcompania)
            cbTipoCaja.DataSource = BaseII.ConsultaDT("MUESTRATipoCajaBase2")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub NUERelClientesAparatos(ByVal Contrato As Integer, ByVal ContratoNet As Integer, ByVal Clv_Servicio As Integer, ByVal NoArticulo As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("NUERelClientesAparatos", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@Contrato", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = Contrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@ContratoNet", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = ContratoNet
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = Clv_Servicio
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@NoArticulo", SqlDbType.Int)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = NoArticulo
        CMD.Parameters.Add(PRM4)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub bnGuardarTipoCaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnGuardarTipoCaja.Click
        'Guardar SetupBox
        'ValidaPagosDif(CONTRATOTextBox.Text)
        'If eRes = 1 Then
        '    MsgBox(eMsj, MsgBoxStyle.Exclamation)
        '    Exit Sub
        'End If
        If bndServicio = 0 Then
            MsgBox("La colonia no tiene servicios activos.")
            Exit Sub
        End If
        If cbTipoCaja.SelectedValue = 0 Then
            MessageBox.Show("Selecciona Tipo de Caja.")
            Exit Sub
        End If
        Manda_Agrega_Cablemodem()
        NUERelClientesAparatos(CONTRATOTextBox.Text, GloContratoDig_Nuevo, 0, cbTipoCaja.SelectedValue)
        panelTipoCaja.Visible = False
    End Sub

    Private Sub bnCancelarTipoCaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnCancelarTipoCaja.Click
        panelTipoCaja.Visible = False
    End Sub

    Private Sub EliminaServicioTV()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("EliminaServicioTV", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@Contrato", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = Contrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@Msg", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            MsjBorraTV = PRM2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub MUESTRARel_ClientesTv_Usuarios(ByVal Contrato As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        ComboBox18.DataSource = BaseII.ConsultaDT("MUESTRARel_ClientesTv_Usuarios")
    End Sub

    Private Sub VALIDASoloInternet(ByVal Contrato)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("VALIDASoloInternet")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString
    End Sub

    Private Sub Button33_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmRelClientesExtensiones.Show()
    End Sub

    Private Sub CONRelClientesExtensiones(ByVal Contrato As Integer)
        'Dim dTable As New DataTable
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        'dTable = BaseII.ConsultaDT("CONRelClientesExtensiones")
        'nudExtensioesCableadas.Text = 0
        'If dTable.Rows.Count = 0 Then Exit Sub
        'nudExtensioesCableadas.Text = dTable.Rows(0)(0).ToString
    End Sub

    Public Function Valida_Zonas() As Boolean


        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, CONTRATOTextBox.Text)
        BaseII.CreateMyParameter("@Zonas", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("Valida_Zonas")
        Return BaseII.dicoPar("@Zonas")


    End Function

    Public Function Valida_TarjetaOxxo() As Boolean

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
        BaseII.CreateMyParameter("@yaTieneTarjeta", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@bndMensaje", ParameterDirection.Output, SqlDbType.BigInt)
        BaseII.ProcedimientoOutPut("DimeSiYaTieneTarjeta")
        Return BaseII.dicoPar("@yaTieneTarjeta")

    End Function

    'Private Sub bnGeneraOrden_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try

    '        generaOrden(CONTRATOTextBox.Text)


    '    Catch ex As Exception
    '        MessageBox.Show("No se generaron las ordenes correspondientes.")
    '    End Try
    'End Sub
    Private Sub generaOrden(ByVal contrato As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("GeneraOrdenes_Servicios", conexion)
        Dim reader As SqlDataReader

        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0


        Dim parametro3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = contrato
        comando.Parameters.Add(parametro3)


        Dim parametro14 As New SqlParameter("@MSJ", SqlDbType.VarChar, 150)
        parametro14.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro14)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            reader = comando.ExecuteReader

            While (reader.Read())
                eMsj = ""
                eMsj = reader(0).ToString()
                If eMsj.Length > 0 Then
                    MessageBox.Show(eMsj, "¡Atención!")
                End If
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub


    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        If ComboBoxCompanias.SelectedIndex <= 0 Then
            timercompania.Enabled = False
        End If

        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdCompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
            BaseII.CreateMyParameter("@permiteCapturarNuevo", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("sp_permiteCapturarCalles")
            CapturaCalle = BaseII.dicoPar("@permiteCapturarNuevo")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
            ComboBoxEstados.DataSource = BaseII.ConsultaDT("MuestraEstadosCompania")
            GloIdCompania = ComboBoxCompanias.SelectedValue
            MuestraTiposServicioTvBase2(GloIdCompania)
            If OpcionCli <> "N" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@clvopcion", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
                BaseII.ProcedimientoOutPut("DameECCCCliente")
                ComboBoxEstados.SelectedValue = BaseII.dicoPar("@clvopcion")
                'BaseII.limpiaParametros()
                'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                'CALLEComboBox.DataSource = BaseII.ConsultaDT("[MUESTRACALLES_PorCompania]")
                'CALLEComboBox.DisplayMember = "NOMBRE"
                'CALLEComboBox.ValueMember = "CLV_CALLE"
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub LlenaEstados()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
            ComboBoxEstados.DataSource = BaseII.ConsultaDT("MuestraEstadosCompania")
            GloIdCompania = ComboBoxCompanias.SelectedValue
            MuestraTiposServicioTvBase2(GloIdCompania)
            If OpcionCli <> "N" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@clvopcion", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
                BaseII.ProcedimientoOutPut("DameECCCCliente")
                ComboBoxEstados.SelectedValue = BaseII.dicoPar("@clvopcion")
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub CIUDADComboBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CIUDADComboBox.TextChanged
        'Llena_companias_nuevo()
    End Sub

    Private Sub CIUDADComboBox_DisplayMemberChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CIUDADComboBox.DisplayMemberChanged
        'Llena_companias_nuevo()
    End Sub

    Private Sub CIUDADComboBox_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CIUDADComboBox.BindingContextChanged
        'Llena_companias_nuevo()
    End Sub

    Private Sub timercompania_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timercompania.Tick
        If ComboBoxCompanias.SelectedValue <= 0 Then
            If Not ComboBoxCompanias.Focused Then
                ComboBoxCompanias.Focus()

            End If

        Else
            panelcompania.BackColor = Color.WhiteSmoke
            timercompania.Enabled = False
            panelcompania.BackColor = Color.WhiteSmoke
        End If
        If panelcompania.BackColor = Color.WhiteSmoke And (opcion = "M" Or opcion = "C") Then
            panelcompania.BackColor = Color.Yellow
        Else
            panelcompania.BackColor = Color.WhiteSmoke
        End If
    End Sub

    Private Sub MuestraServicios_digitalBase2(ByVal tipser As Integer, ByVal Contratonet As Integer, ByVal idcompania As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_tipser", SqlDbType.Int, tipser)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.Int, Contratonet)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.Int, ComboBoxEstados.SelectedValue)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, CIUDADComboBox.SelectedValue)
        'BaseII.CreateMyParameter("@opcionCliente", SqlDbType.VarChar, OpcionCli, 5)
        ComboBox12.DataSource = BaseII.ConsultaDT("MuestraServicios_digitalBase2")
        If ComboBox12.Items.Count = 0 Then
            ComboBox12.Text = ""
        End If
    End Sub

    Private Sub MuestraTiposServicioTvBase2(ByVal idcompania As Integer)
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 1)
        'TipSerTvComboBox.DataSource = BaseII.ConsultaDT("MuestraTiposServicioTvBase2")
        MuestraTiposServicioTvPorColonia()
        
    End Sub

    Private Sub CELULARTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CELULARTextBox.TextChanged

    End Sub

    Private Sub FrmClientes_MouseCaptureChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseCaptureChanged

    End Sub

    Private Sub btDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button85.Click
        ContratoDocumentos = CONTRATOTextBox.Text
        FrmDocumentosCliente.tipopersona = 1
        FrmDocumentosCliente.ShowDialog()
    End Sub

    Private Sub CONTRATONETTextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTRATONETTextBox2.TextChanged
        Try
            Dim OCONTRATONET As Long = 0
            If IsNumeric(CONTRATONETTextBox2.Text) = False Then OCONTRATONET = 0 Else OCONTRATONET = CONTRATONETTextBox2.Text
            MuestraServicios_digitalBase2(3, OCONTRATONET, 1)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxEstados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxEstados.SelectedIndexChanged
        Try
            If ComboBoxEstados.Items.Count = 0 Then
                Exit Sub
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, ComboBoxEstados.SelectedValue)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
            CIUDADComboBox.DataSource = BaseII.ConsultaDT("MuestraCiudadesEstado")
            If OpcionCli <> "N" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@clvopcion", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
                BaseII.ProcedimientoOutPut("DameECCCCliente")
                CIUDADComboBox.SelectedValue = BaseII.dicoPar("@clvopcion")
            End If
            GloClvEstado = ComboBoxEstados.SelectedValue
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxLocalidad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxLocalidad.SelectedIndexChanged
        Try
            If ComboBoxLocalidad.Items.Count = 0 Then
                Exit Sub
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, ComboBoxLocalidad.SelectedValue)
            COLONIAComboBox.DataSource = BaseII.ConsultaDT("MuestraColoniaLocalidad")
            If COLONIAComboBox.Items.Count > 0 And opcion = "N" Then
                COLONIAComboBox.SelectedIndex = 0
            End If
            If OpcionCli <> "N" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@clvopcion", ParameterDirection.Output, SqlDbType.Int)
                BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
                BaseII.ProcedimientoOutPut("DameECCCCliente")
                COLONIAComboBox.SelectedValue = BaseII.dicoPar("@clvopcion")
            End If
            GloClvLocalidad = ComboBoxLocalidad.SelectedValue
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tbclvelec_TextChanged(sender As System.Object, e As System.EventArgs) Handles tbclvelec.TextChanged

    End Sub

    Private Sub btnEdoCuenta_Click(sender As Object, e As EventArgs) Handles btnEdoCuenta.Click
        If IsNumeric(Me.CONTRATOTextBox.Text) = True And CInt(Me.CONTRATOTextBox.Text) > 0 Then
            Dim lineaPP As String
            Dim lineaTR As String
            Dim lineaC As String
            Dim lineaO As String
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@tieneEdoCuenta", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.CreateMyParameter("@idEstadoCuenta", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
            BaseII.ProcedimientoOutPut("tieneEdoCuenta")
            'Checamos si tiene estado cuenta generado
            If BaseII.dicoPar("@tieneEdoCuenta") Then
                GloIdEstadoCuenta = BaseII.dicoPar("@idEstadoCuenta")
                If GloIdEstadoCuenta > 0 Then
                    'checamos si existe pdf del estado de cuenta
                    Dim exists As Boolean
                    exists = System.IO.File.Exists("\\172.16.126.41\exes\EdoCuentaClientes\EdoCuenta" + Contrato.ToString() + ".pdf")
                    If exists Then
                        'Si tiene lo muestra
                        FrmEdoCuenta.Show()
                    Else
                        'Y si no tiene lo genera y después lo muestra
                        Try
                            BaseII.limpiaParametros()
                            BaseII.CreateMyParameter("@idEstadoCuenta", SqlDbType.Int, GloIdEstadoCuenta)
                            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                            BaseII.CreateMyParameter("@lineaPP", ParameterDirection.Output, SqlDbType.VarChar, 30)
                            BaseII.CreateMyParameter("@lineaTR", ParameterDirection.Output, SqlDbType.VarChar, 30)
                            BaseII.CreateMyParameter("@lineaC", ParameterDirection.Output, SqlDbType.VarChar, 10)
                            BaseII.CreateMyParameter("@lineaO", ParameterDirection.Output, SqlDbType.VarChar, 15)
                            BaseII.ProcedimientoOutPut("DameLineasCaptura")
                            lineaPP = BaseII.dicoPar("@lineaPP")
                            lineaTR = BaseII.dicoPar("@lineaTR")
                            lineaC = BaseII.dicoPar("@lineaC")
                            lineaO = BaseII.dicoPar("@lineaO")
                            Dim imgPP As Byte()
                            imgPP = GeneraCodeBar128Nuevo(lineaPP)
                            Dim imgTR As Byte()
                            imgTR = GeneraCodeBar128Nuevo(lineaTR)
                            Dim imgC As Byte()
                            imgC = GeneraCodeBar128Nuevo(lineaC)
                            Dim imgO As Byte()
                            imgO = GeneraCodeBar128NuevoOxxo(lineaO)
                            'Dim conexion As New SqlConnection(MiConexion)
                            'Dim stringBuilder As New StringBuilder("EXEC ReporteEstadoCuentaNuevo " + GloIdEstadoCuenta.ToString() + ", " + CONTRATOTextBox.Text + ", 1," + GloIdCompania.ToString)
                            'Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
                            Dim dataSet As New DataSet()
                            Dim reportDocument As New ReportDocument
                            Dim dataTable As New DataTable
                            Dim lp As List(Of String) = New List(Of String)
                            lp.Add("EstadoCuenta")
                            lp.Add("DetEstadoCuenta")
                            'lp.Add("ReporteAreaTecnicaQuejas1")
                            BaseII.limpiaParametros()
                            BaseII.CreateMyParameter("@ID", SqlDbType.Int, GloIdEstadoCuenta)
                            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, CONTRATOTextBox.Text)
                            BaseII.CreateMyParameter("@OP", SqlDbType.Int, 1)
                            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                            BaseII.CreateMyParameter("@imgPP", SqlDbType.Image, imgPP)
                            BaseII.CreateMyParameter("@imgTR", SqlDbType.Image, imgTR)
                            BaseII.CreateMyParameter("@imgC", SqlDbType.Image, imgC)
                            BaseII.CreateMyParameter("@imgO", SqlDbType.Image, imgO)
                            dataSet = BaseII.ConsultaDS("ReporteEstadoCuentaNuevo", lp)

                            'dataAdapter.Fill(dataSet)
                            dataTable = MuestraGeneral()
                            dataSet.Tables(0).TableName = "EstadoCuenta"
                            dataSet.Tables(1).TableName = "DetEstadoCuenta"
                            dataSet.Tables.Add(dataTable)
                            If lineaPP.Substring(0, 6) = "440702" Then
                                reportDocument.Load(RutaReportes + "\EstadoCuentaInternet.rpt")
                            Else
                                reportDocument.Load(RutaReportes + "\EstadoCuenta.rpt")
                            End If
                            reportDocument.SetDataSource(dataSet)
                            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, "\\172.16.126.41\exes\EdoCuentaClientes\EdoCuenta" + Contrato.ToString() + ".pdf")
                            FrmEdoCuenta.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                        End Try
                    End If

                    'Try
                    '    BaseII.limpiaParametros()
                    '    BaseII.CreateMyParameter("@idEstadoCuenta", SqlDbType.Int, GloIdEstadoCuenta)
                    '    BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
                    '    BaseII.CreateMyParameter("@lineaPP", ParameterDirection.Output, SqlDbType.VarChar, 30)
                    '    BaseII.CreateMyParameter("@lineaTR", ParameterDirection.Output, SqlDbType.VarChar, 30)
                    '    BaseII.CreateMyParameter("@lineaC", ParameterDirection.Output, SqlDbType.VarChar, 10)
                    '    BaseII.CreateMyParameter("@lineaO", ParameterDirection.Output, SqlDbType.VarChar, 15)
                    '    BaseII.ProcedimientoOutPut("DameLineasCaptura")
                    '    lineaPP = BaseII.dicoPar("@lineaPP")
                    '    lineaTR = BaseII.dicoPar("@lineaTR")
                    '    lineaC = BaseII.dicoPar("@lineaC")
                    '    lineaO = BaseII.dicoPar("@lineaO")
                    '    Dim imgPP As Byte()
                    '    imgPP = GeneraCodeBar128Nuevo(lineaPP)
                    '    Dim imgTR As Byte()
                    '    imgTR = GeneraCodeBar128Nuevo(lineaTR)
                    '    Dim imgC As Byte()
                    '    imgC = GeneraCodeBar128Nuevo(lineaC)
                    '    Dim imgO As Byte()
                    '    imgO = GeneraCodeBar128NuevoOxxo(lineaO)
                    '    'Dim conexion As New SqlConnection(MiConexion)
                    '    'Dim stringBuilder As New StringBuilder("EXEC ReporteEstadoCuentaNuevo " + GloIdEstadoCuenta.ToString() + ", " + CONTRATOTextBox.Text + ", 1," + GloIdCompania.ToString)
                    '    'Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
                    '    Dim dataSet As New DataSet()
                    '    Dim reportDocument As New ReportDocument
                    '    Dim dataTable As New DataTable
                    '    Dim lp As List(Of String) = New List(Of String)
                    '    lp.Add("EstadoCuenta")
                    '    lp.Add("DetEstadoCuenta")
                    '    'lp.Add("ReporteAreaTecnicaQuejas1")
                    '    BaseII.limpiaParametros()
                    '    BaseII.CreateMyParameter("@ID", SqlDbType.Int, GloIdEstadoCuenta)
                    '    BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, CONTRATOTextBox.Text)
                    '    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 1)
                    '    BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                    '    BaseII.CreateMyParameter("@imgPP", SqlDbType.Image, imgPP)
                    '    BaseII.CreateMyParameter("@imgTR", SqlDbType.Image, imgTR)
                    '    BaseII.CreateMyParameter("@imgC", SqlDbType.Image, imgC)
                    '    BaseII.CreateMyParameter("@imgO", SqlDbType.Image, imgO)
                    '    dataSet = BaseII.ConsultaDS("ReporteEstadoCuentaNuevo", lp)

                    '    'dataAdapter.Fill(dataSet)
                    '    dataTable = MuestraGeneral()
                    '    dataSet.Tables(0).TableName = "EstadoCuenta"
                    '    dataSet.Tables(1).TableName = "DetEstadoCuenta"
                    '    dataSet.Tables.Add(dataTable)
                    '    If lineaPP.Substring(0, 6) = "440702" Then
                    '        reportDocument.Load(RutaReportes + "\EstadoCuentaInternet.rpt")
                    '    Else
                    '        reportDocument.Load(RutaReportes + "\EstadoCuenta.rpt")
                    '    End If
                    '    reportDocument.SetDataSource(dataSet)
                    '    'reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, "C:\Users\Jano\Desktop\EdoCuenta" + Contrato.ToString() + ".pdf")
                    '    eOpPPE = 999
                    '    FrmImprimirPPE.CrystalReportViewer1.ReportSource = reportDocument
                    '    FrmImprimirPPE.Show()
                    'Catch ex As Exception
                    '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                    'End Try
                End If
            Else
                MsgBox("El cliente no cuenta con ningún Estado de Cuenta generado.")
            End If
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
    End Sub

    Function MuestraGeneral() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MuestraGeneral")
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable()
        Try
            dataAdapter.Fill(dataTable)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Return dataTable

    End Function

    Public Function GeneraCodeBar128Nuevo(ByVal Data As String) As Byte()
        Dim b As New BarcodeLib.Barcode()
        Dim imgBarCode As Byte()
        Dim barcode As New PictureBox()
        Dim W As Integer = Convert.ToInt32(350)
        Dim H As Integer = Convert.ToInt32(100)
        'Dim Data As [String] = "31000016024201101070637005"
        Dim Align As BarcodeLib.AlignmentPositions = BarcodeLib.AlignmentPositions.CENTER
        Align = BarcodeLib.AlignmentPositions.CENTER
        Dim type As BarcodeLib.TYPE = BarcodeLib.TYPE.UNSPECIFIED
        type = BarcodeLib.TYPE.CODE128
        Try
            If type <> BarcodeLib.TYPE.UNSPECIFIED Then
                b.IncludeLabel = True
                b.Alignment = Align
                b.RotateFlipType = (RotateFlipType.RotateNoneFlipNone)
                b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER
                barcode.Image = b.Encode(type, Data, Color.Black, Color.White, W, H)
            End If
            barcode.Width = barcode.Image.Width
            barcode.Height = barcode.Image.Height
            'File.WriteAllBytes(@"C:\XSD\1.jpg", imgbites);                
            Dim imgbites As Byte() = ImageToByte2(barcode.Image)
            imgBarCode = imgbites
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Return imgBarCode
    End Function

    Public Function GeneraCodeBar128NuevoOxxo(ByVal Data As String) As Byte()
        Dim b As New BarcodeLib.Barcode()
        Dim imgBarCode As Byte()
        Dim barcode As New PictureBox()
        Dim W As Integer = Convert.ToInt32(255)
        Dim H As Integer = Convert.ToInt32(55)
        'Dim Data As [String] = "31000016024201101070637005"
        Dim Align As BarcodeLib.AlignmentPositions = BarcodeLib.AlignmentPositions.CENTER
        Align = BarcodeLib.AlignmentPositions.CENTER
        Dim type As BarcodeLib.TYPE = BarcodeLib.TYPE.UNSPECIFIED
        type = BarcodeLib.TYPE.CODE128
        Try
            If type <> BarcodeLib.TYPE.UNSPECIFIED Then
                b.IncludeLabel = True
                b.Alignment = Align
                b.RotateFlipType = (RotateFlipType.RotateNoneFlipNone)
                b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER
                barcode.Image = b.Encode(type, Data, Color.Black, Color.White, W, H)
            End If
            barcode.Width = barcode.Image.Width
            barcode.Height = barcode.Image.Height
            'File.WriteAllBytes(@"C:\XSD\1.jpg", imgbites);                
            Dim imgbites As Byte() = ImageToByte2(barcode.Image)
            imgBarCode = imgbites
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Return imgBarCode
    End Function

    Public Sub AgregaImgCodeBarEstadoDeCuentaTV(ByVal oClv_Recibo As Integer, ByVal strBarCode As String, ByVal prmop As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()

        Dim comando As New SqlClient.SqlCommand("AgregaImgCodeBarEstadoDeCuentaTV", conexion)

        Try

            'GENERAMOS EL CÓDIGO DE BARRAS DE ESTE ESTADO DE CUENTA
            Dim imgBarCode As Byte()
            imgBarCode = GeneraCodeBar128Nuevo(strBarCode)

            'GUADAMOS EL CÓDIGO DE BARRAS GENERADO
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@Contrato", SqlDbType.BigInt)
            comando.Parameters(0).Direction = ParameterDirection.Input
            comando.Parameters(0).Value = oClv_Recibo

            comando.Parameters.Add("@imgBarCode", SqlDbType.Image)
            comando.Parameters(1).Direction = ParameterDirection.Input
            comando.Parameters(1).Value = imgBarCode

            comando.Parameters.Add("@op", SqlDbType.Int)
            comando.Parameters(2).Direction = ParameterDirection.Input
            comando.Parameters(2).Value = prmop

            comando.ExecuteNonQuery()
            imgBarCode = Nothing

            GC.Collect()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
        End Try

    End Sub

    Private Sub Button33_Click_1(sender As Object, e As EventArgs) Handles Button33.Click
        MsgBox(CALLEComboBox.SelectedValue)
    End Sub

    Public Sub muestraOtrosAparato(ByVal oContratonet As Integer, ByVal oTipo As Char)
        Frm1.MdiParent = Me
        Frm1.WindowState = FormWindowState.Normal
        Frm1.BackColor = Me.BackColor
        Frm1.Show()
        'SplitContainer2.Panel2.Hide()
        Panel9.Hide()
        Panel8.Hide()
        'Panel7.Hide()
        Panel7.Location = New Point(2, 335)
        Panel7.Size = New Size(438, 347)
        Frm1.LContratonet = oContratonet
        Frm1.LTipo = oTipo
        Frm1.generaOrden(oContratonet, oTipo)
        Frm1.restringir()
        'Me.Panel2.Hide()
        'Me.Panel4.Hide()
        'Me.Panel7.Hide()
    End Sub

    Private Sub BindingNavigator5_RefreshItems(sender As Object, e As EventArgs)

    End Sub


    Private Sub CONSULTARCLIENTEBindingNavigator_RefreshItems(sender As Object, e As EventArgs) Handles CONSULTARCLIENTEBindingNavigator.RefreshItems

    End Sub

    Private Sub Button35_Click(sender As Object, e As EventArgs) Handles Button35.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@tieneInternet", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("TieneInternetContrato")
        If BaseII.dicoPar("@tieneInternet") = 1 Then
            FrmAsignaClabe.ShowDialog()
        Else
            MsgBox("Este proceso es exclusivo de clientes con servicio de internet.")
        End If

    End Sub

    Private Sub Button36_Click(sender As Object, e As EventArgs) Handles Button36.Click
        Dim Frm1 As New FrmVendedorContratos
        Frm1.ShowDialog()

    End Sub

    Private Sub Button37_Click(sender As Object, e As EventArgs) Handles ButtonPassDisMovil.Click
        If DimeSiEsDispositivoMovil() Then
            Dim frmDisMovil As FrmDispositivoMovil
            frmDisMovil = New FrmDispositivoMovil()
            frmDisMovil.Clv_unicanetDigLoc = loc_Clv_InicaDig
            frmDisMovil.tbUser.Enabled = False
            frmDisMovil.ShowDialog()
        End If
    End Sub

    Private Sub DameListaTipoIdentificacion()
        'Try
        BaseII.limpiaParametros()
        ComboBoxTipoIdenti.DataSource = BaseII.ConsultaDT("DameListaTipoIdentificacion")
        ComboBoxTipoIdenti.DisplayMember = "tipoIdentificacion"
        ComboBoxTipoIdenti.ValueMember = "id"
        ComboBoxTipoIdenti.SelectedValue = 0
        'Catch ex As Exception

        'End Try
    End Sub

    Private Function ValidaLicencia()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Validado", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("Sp_ValidarLicencia")
        If BaseII.dicoPar("@Msg").ToString().Length() > 1 And OpcionCli = "N" Then
            MsgBox(BaseII.dicoPar("@Msg").ToString())
        End If
        Return BaseII.dicoPar("@Validado").ToString()
    End Function

    Private Function MuestraTiposServicioTvPorColonia()
        If IsNumeric(COLONIAComboBox.SelectedValue) And IsNumeric(CIUDADComboBox.SelectedValue) And IsNumeric(ComboBoxCompanias.SelectedValue) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.BigInt, COLONIAComboBox.SelectedValue)
            BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.BigInt, CIUDADComboBox.SelectedValue)
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.BigInt, ComboBoxLocalidad.SelectedValue)
            TipSerTvComboBox.DataSource = BaseII.ConsultaDT("MuestraTiposServicioTvPorColonia")
            TipSerTvComboBox.ValueMember = "Clv_TipSerTv"
            TipSerTvComboBox.DisplayMember = "TipSerTv"
        End If

    End Function

    Private Sub BtnMininodo_Click(sender As Object, e As EventArgs) Handles BtnMininodo.Click
        GloContrato = CONTRATOTextBox.Text
        FrmDetalleMininodo.ShowDialog()
    End Sub

    Private Sub Button37_Click_1(sender As Object, e As EventArgs) Handles Button37.Click
        GloContrato = CONTRATOTextBox.Text
        FrmDatosProvisionamiento.ShowDialog()
    End Sub

    Private Sub Button38_Click(sender As Object, e As EventArgs) Handles Button38.Click
        If CONTRATOTextBox.Text > 0 Then
            FrmEntreCalles.ContratoEntreCalles = CONTRATOTextBox.Text
            FrmEntreCalles.clv_sesionEntreCalles = 0
        Else
            If ENTRECALLESTextBox.Text.Length = 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.BigInt)
                BaseII.ProcedimientoOutPut("DameClv_Session")
                clv_sesionEntreCallesFrmClientes = CInt(BaseII.dicoPar("@Clv_Session").ToString())
            End If
            FrmEntreCalles.clv_sesionEntreCalles = clv_sesionEntreCallesFrmClientes
            FrmEntreCalles.ContratoEntreCalles = 0
        End If

        FrmEntreCalles.clv_coloniaEntreCalles = COLONIAComboBox.SelectedValue


        FrmEntreCalles.CallePrincipal = CALLEComboBox.SelectedValue
        FrmEntreCalles.Show()
    End Sub

    Private Sub GuardaEntreCallesTemporales()
        If CONTRATOTextBox.Text > 0 And clv_sesionEntreCallesFrmClientes > 0 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, CONTRATOTextBox.Text)
            BaseII.CreateMyParameter("@Clv_sesion", SqlDbType.BigInt, clv_sesionEntreCallesFrmClientes)
            BaseII.Inserta("GuardaEntreCallesTemporales")
        End If
    End Sub



End Class
