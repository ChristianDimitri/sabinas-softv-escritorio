<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelVendedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Aceptar = New System.Windows.Forms.Button()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.Dame_clv_session_ReportesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_session_ReportesTableAdapter = New sofTV.DataSetEricTableAdapters.Dame_clv_session_ReportesTableAdapter()
        Me.ConVentasVendedoresTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConVentasVendedoresTmpTableAdapter = New sofTV.DataSetEricTableAdapters.ConVentasVendedoresTmpTableAdapter()
        Me.ConVentasVendedoresTmpListBox = New System.Windows.Forms.ListBox()
        Me.InsertarVendedorTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarVendedorTmpTableAdapter = New sofTV.DataSetEricTableAdapters.InsertarVendedorTmpTableAdapter()
        Me.BorrarVendedorTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarVendedorTmpTableAdapter = New sofTV.DataSetEricTableAdapters.BorrarVendedorTmpTableAdapter()
        Me.ConVentasVendedoresProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConVentasVendedoresProTableAdapter = New sofTV.DataSetEricTableAdapters.ConVentasVendedoresProTableAdapter()
        Me.ConVentasVendedoresProListBox = New System.Windows.Forms.ListBox()
        Me.Button5 = New System.Windows.Forms.Button()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConVentasVendedoresTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConVentasVendedoresProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(459, 119)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 31)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(459, 158)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(85, 31)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(459, 309)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(85, 31)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(459, 347)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(85, 31)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Aceptar
        '
        Me.Aceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Aceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Aceptar.Location = New System.Drawing.Point(659, 489)
        Me.Aceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Aceptar.Name = "Aceptar"
        Me.Aceptar.Size = New System.Drawing.Size(181, 44)
        Me.Aceptar.TabIndex = 4
        Me.Aceptar.Text = "&ACEPTAR"
        Me.Aceptar.UseVisualStyleBackColor = True
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.EnforceConstraints = False
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_clv_session_ReportesBindingSource
        '
        Me.Dame_clv_session_ReportesBindingSource.DataMember = "Dame_clv_session_Reportes"
        Me.Dame_clv_session_ReportesBindingSource.DataSource = Me.DataSetEric
        '
        'Dame_clv_session_ReportesTableAdapter
        '
        Me.Dame_clv_session_ReportesTableAdapter.ClearBeforeFill = True
        '
        'ConVentasVendedoresTmpBindingSource
        '
        Me.ConVentasVendedoresTmpBindingSource.DataMember = "ConVentasVendedoresTmp"
        Me.ConVentasVendedoresTmpBindingSource.DataSource = Me.DataSetEric
        '
        'ConVentasVendedoresTmpTableAdapter
        '
        Me.ConVentasVendedoresTmpTableAdapter.ClearBeforeFill = True
        '
        'ConVentasVendedoresTmpListBox
        '
        Me.ConVentasVendedoresTmpListBox.DataSource = Me.ConVentasVendedoresTmpBindingSource
        Me.ConVentasVendedoresTmpListBox.DisplayMember = "Nombre"
        Me.ConVentasVendedoresTmpListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConVentasVendedoresTmpListBox.ItemHeight = 18
        Me.ConVentasVendedoresTmpListBox.Location = New System.Drawing.Point(575, 47)
        Me.ConVentasVendedoresTmpListBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ConVentasVendedoresTmpListBox.Name = "ConVentasVendedoresTmpListBox"
        Me.ConVentasVendedoresTmpListBox.Size = New System.Drawing.Size(367, 382)
        Me.ConVentasVendedoresTmpListBox.TabIndex = 7
        Me.ConVentasVendedoresTmpListBox.ValueMember = "Clv_Vendedor"
        '
        'InsertarVendedorTmpBindingSource
        '
        Me.InsertarVendedorTmpBindingSource.DataMember = "InsertarVendedorTmp"
        Me.InsertarVendedorTmpBindingSource.DataSource = Me.DataSetEric
        '
        'InsertarVendedorTmpTableAdapter
        '
        Me.InsertarVendedorTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarVendedorTmpBindingSource
        '
        Me.BorrarVendedorTmpBindingSource.DataMember = "BorrarVendedorTmp"
        Me.BorrarVendedorTmpBindingSource.DataSource = Me.DataSetEric
        '
        'BorrarVendedorTmpTableAdapter
        '
        Me.BorrarVendedorTmpTableAdapter.ClearBeforeFill = True
        '
        'ConVentasVendedoresProBindingSource
        '
        Me.ConVentasVendedoresProBindingSource.DataMember = "ConVentasVendedoresPro"
        Me.ConVentasVendedoresProBindingSource.DataSource = Me.DataSetEric
        '
        'ConVentasVendedoresProTableAdapter
        '
        Me.ConVentasVendedoresProTableAdapter.ClearBeforeFill = True
        '
        'ConVentasVendedoresProListBox
        '
        Me.ConVentasVendedoresProListBox.DataSource = Me.ConVentasVendedoresProBindingSource
        Me.ConVentasVendedoresProListBox.DisplayMember = "Nombre"
        Me.ConVentasVendedoresProListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConVentasVendedoresProListBox.ItemHeight = 18
        Me.ConVentasVendedoresProListBox.Location = New System.Drawing.Point(57, 47)
        Me.ConVentasVendedoresProListBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ConVentasVendedoresProListBox.Name = "ConVentasVendedoresProListBox"
        Me.ConVentasVendedoresProListBox.Size = New System.Drawing.Size(367, 382)
        Me.ConVentasVendedoresProListBox.TabIndex = 8
        Me.ConVentasVendedoresProListBox.ValueMember = "Clv_Vendedor"
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(848, 489)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 9
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'FrmSelVendedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1045, 548)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.ConVentasVendedoresProListBox)
        Me.Controls.Add(Me.ConVentasVendedoresTmpListBox)
        Me.Controls.Add(Me.Aceptar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button3)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelVendedor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Vendedor(es)"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConVentasVendedoresTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConVentasVendedoresProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Aceptar As System.Windows.Forms.Button
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents Dame_clv_session_ReportesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_session_ReportesTableAdapter As sofTV.DataSetEricTableAdapters.Dame_clv_session_ReportesTableAdapter
    Friend WithEvents ConVentasVendedoresTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConVentasVendedoresTmpTableAdapter As sofTV.DataSetEricTableAdapters.ConVentasVendedoresTmpTableAdapter
    Friend WithEvents ConVentasVendedoresTmpListBox As System.Windows.Forms.ListBox
    Friend WithEvents InsertarVendedorTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarVendedorTmpTableAdapter As sofTV.DataSetEricTableAdapters.InsertarVendedorTmpTableAdapter
    Friend WithEvents BorrarVendedorTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarVendedorTmpTableAdapter As sofTV.DataSetEricTableAdapters.BorrarVendedorTmpTableAdapter
    Friend WithEvents ConVentasVendedoresProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConVentasVendedoresProTableAdapter As sofTV.DataSetEricTableAdapters.ConVentasVendedoresProTableAdapter
    Friend WithEvents ConVentasVendedoresProListBox As System.Windows.Forms.ListBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
