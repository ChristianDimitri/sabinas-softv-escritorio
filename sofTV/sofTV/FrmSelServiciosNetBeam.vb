﻿Imports System.Data.SqlClient
Public Class FrmSelServiciosNetBeam

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Try
            If IsNumeric(ComboBoxServicio.SelectedValue) = True And IsNumeric(tBoxLongitud.Text) = True And IsNumeric(tBoxLatitud.Text) = True Then
                'Comentado e insertado para la orden de preregistro 27/02/2017

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Longitud", SqlDbType.Decimal, tBoxLongitud.Text)
                BaseII.CreateMyParameter("@Latitud", SqlDbType.Decimal, tBoxLatitud.Text)
                BaseII.CreateMyParameter("@Cobertura", ParameterDirection.Output, SqlDbType.Bit)
                BaseII.ProcedimientoOutPut("sp_ValidaCoordenadas")
                Dim tieneCobertura As Boolean = BaseII.dicoPar("@Cobertura")

                If tieneCobertura = True Then

                    'And IsNumeric(ComboBoxProveedor.SelectedValue) = True And IsNumeric(ComboBoxBeam.SelectedValue) = True 
                    GloGuardarNet = False
                    If GloClv_TipSer <> 5 Then
                        GloClv_Servicio = ComboBoxServicio.SelectedValue
                        GloId_Beam_Paquete = 0
                        locGloClv_servicioPromocion = GloClv_Servicio
                        GloLongitud = tBoxLongitud.Text
                        GloLatitud = tBoxLatitud.Text
                    Else
                        GloClv_servTel = Me.ComboBoxServicio.SelectedValue
                        locGloClv_servicioPromocion = GloClv_servTel
                    End If

                    'Comentado e insertado para la orden de preregistro 27/02/2017 Fin

                    If GloClv_TipSer = 2 Then
                        If Equip_tel = True Then
                            numtel = True
                        End If
                        GloBndSer = True
                    ElseIf GloClv_TipSer = 5 Then
                        globndTel = True
                    End If
                    Me.Close()
                    frmctr.Activar()
                Else
                    MsgBox("Sin Cobertura", MsgBoxStyle.Information)
                End If

            Else
                MsgBox("Faltan datos por capturar.", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GloClv_Servicio = 0
        GloClv_servTel = 0
        Me.Close()
    End Sub

    Private Sub FrmSelServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        locGloClv_servicioPromocion = 0
        frmctr.MdiParent = FrmClientes
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            MuestraServiciosBase2ContratacionInternet(GloClv_TipSer, GloIdCompania)
            MuestraProveedorBase2()
            If IsNumeric(ComboBoxProveedor.SelectedValue) And IsNumeric(ComboBoxServicio.SelectedValue) Then
                MuestraBeamBase2(ComboBoxProveedor.SelectedValue, ComboBoxServicio.SelectedValue)
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MuestraServiciosBase2ContratacionInternet(ByVal tipser As Integer, ByVal idcompania As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_tipser", SqlDbType.Int, tipser)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@idcompaniaBeam", SqlDbType.Int, GloIdCompania)
        BaseII.CreateMyParameter("@Clv_Estado", SqlDbType.Int, GloClvEstado)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, GloClvCiudad)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.BigInt, GloClvLocalidad)
        BaseII.CreateMyParameter("@clv_colonia", SqlDbType.BigInt, GloClvColonia)
        ComboBoxServicio.DataSource = BaseII.ConsultaDT("MuestraServiciosBase2ContratacionInternet")
    End Sub

    Private Sub MuestraProveedorBase2()
        BaseII.limpiaParametros()
        ComboBoxProveedor.DataSource = BaseII.ConsultaDT("sp_dameProveedoresInternet")
        ComboBoxProveedor.DisplayMember = "nombre"
        ComboBoxProveedor.ValueMember = "idProveedor"

    End Sub

    Private Sub MuestraBeamBase2(ByVal idProveedor As Integer, ByVal clv_servicio As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", SqlDbType.BigInt, GloIdCompania)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, ComboBoxServicio.SelectedValue)
        ComboBoxBeam.Text = ""
        ComboBoxBeam.DataSource = BaseII.ConsultaDT("sp_dameBeamPorServicio")

    End Sub
    Private Sub ComboBoxProveedor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxProveedor.SelectedIndexChanged
        If IsNumeric(ComboBoxProveedor.SelectedValue) And IsNumeric(ComboBoxServicio.SelectedValue) Then
            MuestraBeamBase2(ComboBoxProveedor.SelectedValue, ComboBoxServicio.SelectedValue)
        End If
    End Sub

    Private Sub ComboBoxServicio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxServicio.SelectedIndexChanged
        If IsNumeric(ComboBoxProveedor.SelectedValue) And IsNumeric(ComboBoxServicio.SelectedValue) Then
            MuestraBeamBase2(ComboBoxProveedor.SelectedValue, ComboBoxServicio.SelectedValue)
        End If
    End Sub

    Private Sub Label6_Click(sender As System.Object, e As System.EventArgs) Handles Label6.Click

    End Sub
End Class