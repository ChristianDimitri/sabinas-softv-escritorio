﻿Public Class BrwRedes

    Private Sub BrwRedes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaGrid()

    End Sub

    Public Sub llenaGrid()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdRed", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
        dgvRedes.DataSource = BaseII.ConsultaDT("GetList_CatalogoRed")
        '  dgvRedes.CurrentCell = Nothing ' Ninguna celda seleccionada 
    End Sub

    Private Sub BuscaRedBtn_Click(sender As Object, e As EventArgs) Handles BuscaRedBtn.Click
        buscar()
    End Sub


    Private Sub buscar()
        Dim red As String
        red = buscaRedTxt.Text

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdRed", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@IpRed", SqlDbType.VarChar, red.ToString())

        dgvRedes.DataSource = BaseII.ConsultaDT("BuscarIp_CatalogoRed")

        buscaRedTxt.Text = ""
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub


    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click

        OpcAccion = "C"
        seleccionarRed()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click

        OpcAccion = "M"
        seleccionarRed()
    End Sub

  

    Private Sub seleccionarRed()
        Dim rowActual As Long
        iD_redSelec = 0

        If dgvRedes.SelectedRows.Count = 1 Then
            rowActual = dgvRedes.SelectedRows(0).Index
        End If

        If dgvRedes.SelectedRows.Count = 1 Then
            iD_redSelec = dgvRedes.Rows(rowActual).Cells(0).Value
            FrmRedes.Show()
        Else
            MessageBox.Show("Seleccione una Red")
        End If

    End Sub


    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        OpcAccion = "N"
        FrmRedes.Show()
        '  seleccionarRed()
    End Sub

    Private Sub buscaRedTxt_TextChanged(sender As Object, e As EventArgs) Handles buscaRedTxt.TextChanged

    End Sub

    Private Sub buscaRedTxt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles buscaRedTxt.KeyPress

        If Asc(e.KeyChar) = 13 Then
            buscar()
        End If

    End Sub


End Class