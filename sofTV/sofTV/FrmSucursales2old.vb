﻿Imports System.Data.SqlClient
Imports Softv.BAL
Imports System.Collections.Generic
Public Class FrmSucursales2old
    Public RFC As String = Nothing
    Public LocIdCompania As Integer = 0
    Private Sub FrmSucursales2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'Llena_Combo()
        'If GloActivarCFD = 1 Then
        '    ''no se usa ya SP_Mizar_MuestraRFC()
        '    Label6.Visible = True
        '    ComboBox1.Visible = True
        '    RFC_CFD()
        'End If

        If opcion = "N" Then
            Me.CONSUCURSALESBindingSource.AddNew()
            Panel1.Enabled = True
            Llena_Companias()
        ElseIf opcion = "C" Then
            Clv_SucursalTextBox.Text = gloClave.ToString
            Panel1.Enabled = False
            BUSCA(gloClave)
            consultaDatosGeneralesSucursal(gloClave, 0)
            Llena_Companias()
            damecompaniasucursal()
            llenaSucursal()
        ElseIf opcion = "M" Then
            Clv_SucursalTextBox.Text = gloClave.ToString
            Panel1.Enabled = True
            BUSCA(gloClave)
            consultaDatosGeneralesSucursal(gloClave, 0)
            Llena_Companias()
            damecompaniasucursal()
            If NombreTextBox.Text = "Puntos Web" Then
                SerieTextBox1.ReadOnly = True
                No_folioTextBox.ReadOnly = True
            End If
            llenaSucursal()
            ComboBoxCompanias.Enabled = False
        End If
    End Sub

    Private Sub llenaSucursal()
        Try
            Dim comando As New SqlCommand()
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            comando.Connection = conexion
            comando.CommandText = "exec CONSUCURSALES " + gloClave.ToString
            Dim reader As SqlDataReader = comando.ExecuteReader()
            reader.Read()
            Clv_SucursalTextBox.Text = reader(0).ToString
            NombreTextBox.Text = reader(1).ToString
            IPTextBox.Text = reader(2).ToString
            ImpresoraTextBox.Text = reader(3).ToString
            Clv_EquivalenteTextBox.Text = reader(4).ToString
            SerieTextBox.Text = reader(5).ToString
            UltimoFolioUsadoTextBox.Text = reader(6).ToString
            reader.Close()
            conexion.Close()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub damecompaniasucursal()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
        BaseII.ProcedimientoOutPut("DameCompaniaSucursal")
        Dim res As Integer = BaseII.dicoPar("@idcompania")
        If res = 0 Then
            ComboBoxCompanias.SelectedValue = 0
        Else
            ComboBoxCompanias.SelectedValue = res
        End If
        'Parte de serial
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
        BaseII.CreateMyParameter("@serie", ParameterDirection.Output, SqlDbType.VarChar, 15)
        BaseII.ProcedimientoOutPut("DameSerieSucursal")
        SerieTextBox.Text = BaseII.dicoPar("@serie")
    End Sub
    Private Sub ModificaCompaniaSucursal()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
        BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
        BaseII.Inserta("ModificaCompaniaSucursal")
    End Sub
    Private Sub Llena_Companias()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"
            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            End If
            GloIdCompania = 0
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub RFC_CFD()
        Dim conn As New SqlConnection(MiConexion)
        Try

            conn.Open()
            Dim comando As New SqlClient.SqlCommand("Dame_Serire_Folio_RFC", conn)
            comando.CommandType = CommandType.StoredProcedure
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            'Agrego el parámetro
            'Adaptador.SelectCommand.Parameters.Add("@RFC", SqlDbType.VarChar, 50).Value = eRFC

            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "Dame_Serire_Folio_RFC")
            Bs.DataSource = Dataset.Tables("Dame_Serire_Folio_RFC")

            'dgvResultadosClasificacion.DataSource = Bs
            ComboBox1.DataSource = Bs
            ComboBox1.DisplayMember = "Serie_Folio"
            ComboBox1.ValueMember = "Clave"

            'If ComboBox1.Items.Count > 0 Then
            '    ComboBox1.SelectedIndex = 0
            'End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conn.Close()
        End Try
    End Sub
    Public Sub Guarda_Rel_Ciudad_Ciudad(ByVal eClv_Sucursal As Integer, ByVal eClv_Ciudad As Integer, ByVal eClave_SerieFolio As String, ByVal eMatriz As Boolean, ByVal oId_Compania As Long, ByVal oRfc As String)

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Insetra_Rel_Sucursal_Ciudad"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm1 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                prm1.Value = eClv_Sucursal
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@Clave_SerieFolio", SqlDbType.VarChar, 50)
                prm3.Value = eClave_SerieFolio
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Matriz", SqlDbType.Bit)
                prm4.Value = eMatriz
                .Parameters.Add(prm4)



                Dim i As Integer = .ExecuteNonQuery()
                'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con éxito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With
            CON80.Close()
        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Sub
    Private Sub Muestra_Rel_Ciudad_Ciudad(ByVal eClv_Sucursal As Integer)

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                '.CommandText = "Muestra_Rel_Sucursal_Ciudad"
                '.CommandType = CommandType.StoredProcedure
                '.Connection = CON80
                '.CommandTimeout = 0
                .CommandText = "DimeSiMatriz"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0
                Dim prm2 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                prm2.Value = eClv_Sucursal
                .Parameters.Add(prm2)


                'Dim prm3 As New SqlParameter("@Clave_SerieFolio", SqlDbType.VarChar, 50)
                'prm3.Direction = ParameterDirection.Output
                'prm3.Value = 0
                '.Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Matriz", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)


                Dim i As Integer = .ExecuteNonQuery()
                'Ciudadcmb.Items.Add(prm1.Value)
                'Ciudadcmb.SelectedItem = prm1.Value
                'Dame_RFC_Por_Ciudad(prm1.Value)
                'RFC_CFD(RFC)
                '
                If GloActivarCFD = 1 Then
                    Dim CON1 As New SqlClient.SqlConnection(MiConexion)
                    Dim CMD1 As New SqlClient.SqlCommand()
                    CMD1 = New SqlClient.SqlCommand()
                    CON1.Open()
                    With CMD1
                        .CommandText = "Muestra_Rel_Sucursal_Ciudad"
                        .CommandType = CommandType.StoredProcedure
                        .Connection = CON80
                        .CommandTimeout = 0

                        Dim prm21 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                        prm21.Value = eClv_Sucursal
                        .Parameters.Add(prm21)


                        Dim prm31 As New SqlParameter("@Clave_SerieFolio", SqlDbType.VarChar, 50)
                        prm31.Direction = ParameterDirection.Output
                        prm31.Value = 0
                        .Parameters.Add(prm31)

                        Dim prm41 As New SqlParameter("@Matriz", SqlDbType.Bit)
                        prm41.Direction = ParameterDirection.Output
                        prm41.Value = 0
                        .Parameters.Add(prm41)


                        Dim j As Integer = .ExecuteNonQuery()
                        ComboBox1.SelectedValue = prm31.Value

                    End With

                End If

                MatrizChck.CheckState = CheckState.Unchecked

                If prm4.Value = 1 Then MatrizChck.CheckState = CheckState.Checked

                'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con éxito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With

        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            'System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Sub

    Public Function Valida_Serie(ByVal LocSerie As String, LocClv_Sucursal As Integer) As Integer

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Valida_Serie = 0
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Valida_Serie"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm2 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                prm2.Value = LocClv_Sucursal
                .Parameters.Add(prm2)

                Dim prm1 As New SqlParameter("@SERIE", SqlDbType.VarChar, 10)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = LocSerie
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@Bnd", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim i As Integer = .ExecuteNonQuery()
                Valida_Serie = prm3.Value
            End With

        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Function

    Public Function Valida_Sucursal_Matriz(ByVal eClv_Sucursal As Integer, ByVal eClv_Ciudad As Integer) As Integer

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Valida_Sucursal_Matriz = 0
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Valida_Sucursal_Matriz"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm2 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
                prm2.Value = eClv_Sucursal
                .Parameters.Add(prm2)

                Dim prm1 As New SqlParameter("@idcompania", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = ComboBoxCompanias.SelectedValue
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@Bnd", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim i As Integer = .ExecuteNonQuery()
                Valida_Sucursal_Matriz = prm3.Value
            End With

        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
        Return Valida_Sucursal_Matriz
    End Function
    Private Sub Dame_RFC_Por_Ciudad(ByVal eClv_Ciudad As Integer)

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        RFC = ""
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Dame_RFC_Por_Ciudad"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm2 As New SqlParameter("@Clv_Ciudad", SqlDbType.Int)
                prm2.Value = eClv_Ciudad
                .Parameters.Add(prm2)

                Dim prm1 As New SqlParameter("@RFC", SqlDbType.VarChar, 50)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)



                Dim i As Integer = .ExecuteNonQuery()
                RFC = prm1.Value

                'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con éxito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With

        Catch ex As Exception
            If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON80.Close()
        End Try
    End Sub

    Private Sub CONSUCURSALESBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSUCURSALESBindingNavigatorSaveItem.Click
        If IsNumeric(Ciudadcombo.SelectedItem) = False And Ciudadcombo.Items.Count > 0 Then
            MsgBox("Seleccione una Ciudad de la Lista ", vbInformation)
            Exit Sub
        End If
        If Ciudadcombo.SelectedItem <= 0 And Ciudadcombo.Items.Count > 0 Then
            MsgBox("Seleccione una Ciudad de la Lista ", vbInformation)
            Exit Sub
        End If
        'If GloActivarCFD = 1 Then
        '    If ComboBox1.SelectedIndex < 0 Then
        '        MsgBox("Seleccione la Series y Folio de la Lista ", vbInformation)
        '        Exit Sub
        '    End If
        'End If
        If SerieTextBox.Text.Length > 5 Then
            MsgBox("La Serie no puede exceder de 5 caracteres.")
            Exit Sub
        End If
        If MatrizChck.CheckState = CheckState.Checked Then
            If Valida_Sucursal_Matriz(Clv_SucursalTextBox.Text, Ciudadcombo.SelectedItem) = 1 Then
                'MatrizChck.CheckState = CheckState.Unchecked
                MsgBox("Ya existe una sucursal que es la Matríz de la Plaza seleccionado ", vbInformation)
                Exit Sub
            End If
        End If
        If IsNumeric(Clv_SucursalTextBox.Text) = False Then Clv_SucursalTextBox.Text = 0
        If Valida_Serie(SerieTextBox.Text, Clv_SucursalTextBox.Text) = 1 Then
            'MatrizChck.CheckState = CheckState.Unchecked
            MsgBox("Ya existe una serie ", vbInformation)
            Exit Sub
        End If

        If Me.txtCalle.Text.Length = 0 And Me.txtCalle.Enabled = True Then
            MsgBox("¡Capture la Calle de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtNumero.Text.Length = 0 And Me.txtNumero.Enabled = True Then
            MsgBox("¡Capture el Número de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtColonia.Text.Length = 0 And Me.txtColonia.Enabled = True Then
            MsgBox("¡Capture el Barrio de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtCP.Text.Length = 0 And txtCP.Enabled = True And Not IsNumeric(txtCP.Text) Then
            MsgBox("¡Capture el CP de la Sucursal!. Recuerde que debe ser un valor numérico" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtMunicipio.Text.Length = 0 And txtMunicipio.Enabled = True Then
            MsgBox("¡Capture el Municipio de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.txtCiudad.Text.Length = 0 And txtCiudad.Enabled = True Then
            MsgBox("¡Capture la Ciudad de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
            Exit Sub

        End If

        Dim locerror As Integer
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.No_folioTextBox.Text) = False Then Me.No_folioTextBox.Text = 0
            'Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
            'Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)
            locerror = 0
            If locerror = 0 Then
                If IsNumeric(Me.UltimoFolioUsadoTextBox.Text) = False Then Me.UltimoFolioUsadoTextBox.Text = 0
                Me.Validate()
                Me.CONSUCURSALESBindingSource.EndEdit()
                'Me.CONSUCURSALESTableAdapter.Connection = CON
                'Me.CONSUCURSALESTableAdapter.Update(Me.NewSofTvDataSet.CONSUCURSALES)
                If opcion = "M" Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
                    BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, NombreTextBox.Text)
                    BaseII.CreateMyParameter("@IP", SqlDbType.VarChar, IPTextBox.Text)
                    BaseII.CreateMyParameter("@Impresora", SqlDbType.VarChar, ImpresoraTextBox.Text)
                    BaseII.CreateMyParameter("@Clv_Equivalente", SqlDbType.VarChar, Clv_EquivalenteTextBox.Text)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, SerieTextBox.Text)
                    BaseII.CreateMyParameter("@UltimoFolioUsado", SqlDbType.BigInt, UltimoFolioUsadoTextBox.Text)
                    BaseII.Inserta("MODSUCURSALES")
                End If
                If opcion = "N" Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Clv_Sucursal", ParameterDirection.Output, SqlDbType.Int)
                    BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, NombreTextBox.Text)
                    BaseII.CreateMyParameter("@IP", SqlDbType.VarChar, IPTextBox.Text)
                    BaseII.CreateMyParameter("@Impresora", SqlDbType.VarChar, ImpresoraTextBox.Text)
                    BaseII.CreateMyParameter("@Clv_Equivalente", SqlDbType.VarChar, Clv_EquivalenteTextBox.Text)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, SerieTextBox.Text)
                    BaseII.CreateMyParameter("@UltimoFolioUsado", SqlDbType.BigInt, UltimoFolioUsadoTextBox.Text)
                    BaseII.ProcedimientoOutPut("NUESUCURSALES")
                    Clv_SucursalTextBox.Text = BaseII.dicoPar("@Clv_Sucursal")

                End If
                Me.Inserta_impresora_sucursalTableAdapter1.Connection = CON
                Me.Inserta_impresora_sucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.inserta_impresora_sucursal, CInt(Me.Clv_SucursalTextBox.Text), Me.Impresora_ContratosTextBox.Text, Me.Impresora_TarjetasTextBox.Text, Me.TextBox1.Text)
                'Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
                'Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@serie", SqlDbType.VarChar, SerieTextBox.Text)
                BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
                BaseII.Inserta("InsertaSerieSucursal")
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.Int, Clv_SucursalTextBox.Text)
                If MatrizChck.Checked = True Then
                    BaseII.CreateMyParameter("@matriz", SqlDbType.Int, 1)
                Else
                    BaseII.CreateMyParameter("@matriz", SqlDbType.Int, 2)
                End If
                BaseII.Inserta("InsertaMatrizSucursal")
                If MatrizChck.Checked = True Then
                    Me.txtCalle.Text = ""
                    Me.txtNumero.Text = ""
                    Me.txtColonia.Text = ""
                    Me.txtCP.Text = 0
                    Me.txtMunicipio.Text = ""
                    Me.txtCiudad.Text = ""
                    Me.txtTelefono.Text = ""
                    insertaDatosGeneralesSucursal(Clv_SucursalTextBox.Text, Me.txtCalle.Text, Me.txtNumero.Text, Me.txtColonia.Text, CInt(Me.txtCP.Text), Me.txtMunicipio.Text, _
                                              Me.txtCiudad.Text, Me.txtTelefono.Text)
                ElseIf MatrizChck.Checked = False Then
                    insertaDatosGeneralesSucursal(Clv_SucursalTextBox.Text, Me.txtCalle.Text, Me.txtNumero.Text, Me.txtColonia.Text, CInt(Me.txtCP.Text), Me.txtMunicipio.Text, _
                                              Me.txtCiudad.Text, Me.txtTelefono.Text)
                End If
                'If IsNumeric(Ciudadcmb.SelectedValue) = True Then
                '    If Ciudadcmb.SelectedValue > 0 Then
                'If MatrizChck.CheckState = CheckState.Checked Then
                LocIdCompania = 0
                'Guarda_Rel_Ciudad_Ciudad(Clv_SucursalTextBox.Text, gloClave, ComboBox1.SelectedValue, MatrizChck.Checked, LocIdCompania, "")
                ModificaCompaniaSucursal()
                'Else
                '    LocIdCompania = 0
                '    If ComboBoxRFC.SelectedIndex > 0 Then
                '        If IsNumeric(ComboBoxRFC.SelectedValue) = True Then
                '            LocIdCompania = ComboBoxRFC.SelectedValue
                '        End If
                '    End If
                '    Guarda_Rel_Ciudad_Ciudad(Clv_SucursalTextBox.Text, Ciudadcmb.SelectedValue, ComboBox1.SelectedValue, False, LocIdCompania)
                'End If

                '    End If
                'End If
                bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Creación Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
                MsgBox(mensaje5)
                GloBnd = True
                CON.Close()
                Me.Close()
            Else
                MsgBox("La Serie de Factura Global ya Existe no se puede Guardar", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            'MsgBox("La Serie ya Existe no se Puede Guardar")
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'Me.CONSUCURSALESTableAdapter.Connection = CON
            'Me.CONSUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.CONSUCURSALES, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            Me.Consulta_Impresora_SucursalTableAdapter1.Connection = CON
            Me.Consulta_Impresora_SucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.Consulta_Impresora_Sucursal, CLAVE)
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Connection = CON
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Consulta_Generales_FacturasGlobales, CLAVE)
            CON.Close()

            Muestra_Rel_Ciudad_Ciudad(CLAVE)
            'If (pos) > 0 Then
            '    Ciudadcmb.SelectedValue = pos
            'End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Clv_SucursalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SucursalTextBox.TextChanged
        gloClave = Me.Clv_SucursalTextBox.Text
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONSUCURSALESTableAdapter.Connection = CON
        Me.CONSUCURSALESTableAdapter.Delete(gloClave)
        Me.Borra_Impresora_SucursalesTableAdapter.Connection = CON
        Me.Borra_Impresora_SucursalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Impresora_Sucursales, gloClave)
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Connection = CON
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Generales_FacturasGlobales, gloClave)
        bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Eliminó Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
        CON.Close()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub
    Private Sub insertaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmCalle As String, ByVal prmNumero As String, ByVal prmColonia As String, ByVal prmCp As Integer, _
                                              ByVal prmMunicipio As String, ByVal prmCiudad As String, ByVal prmTelefono As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, prmCalle, 250)
            BaseII.CreateMyParameter("@numero", SqlDbType.VarChar, prmNumero, 100)
            BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, prmColonia, 250)
            BaseII.CreateMyParameter("@cp", SqlDbType.Int, prmCp)
            BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, prmMunicipio, 250)
            BaseII.CreateMyParameter("@ciudad", SqlDbType.VarChar, prmCiudad, 250)
            BaseII.CreateMyParameter("@telefono", SqlDbType.VarChar, prmTelefono, 250)
            'Nuevos datos
            BaseII.CreateMyParameter("@horario", SqlDbType.VarChar, tbHorario.Text)
            BaseII.CreateMyParameter("@referencia", SqlDbType.VarChar, tbReferencia.Text)
            BaseII.CreateMyParameter("@contacto", SqlDbType.VarChar, tbContacto.Text)
            BaseII.CreateMyParameter("@email", SqlDbType.VarChar, tbEmail.Text)
            BaseII.Inserta("uspInsertaTblRelSucursalDatosGenerales")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub consultaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmClvFactura As Integer)

        Dim dtDatosGenerales As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
        BaseII.CreateMyParameter("@clvFactura", SqlDbType.Int, prmClvFactura)
        dtDatosGenerales = BaseII.ConsultaDT("uspConsultaTblRelSucursalDatosGenerales")


        If dtDatosGenerales.Rows.Count > 0 Then
            Me.txtCalle.Text = dtDatosGenerales.Rows(0)("calle").ToString
            Me.txtNumero.Text = dtDatosGenerales.Rows(0)("numero").ToString
            Me.txtColonia.Text = dtDatosGenerales.Rows(0)("colonia").ToString
            Me.txtCP.Text = dtDatosGenerales.Rows(0)("cp").ToString
            Me.txtMunicipio.Text = dtDatosGenerales.Rows(0)("municipio").ToString
            Me.txtCiudad.Text = dtDatosGenerales.Rows(0)("ciudad").ToString
            Me.txtTelefono.Text = dtDatosGenerales.Rows(0)("telefono").ToString
            tbHorario.Text = dtDatosGenerales.Rows(0)("horario").ToString
            tbContacto.Text = dtDatosGenerales.Rows(0)("contacto").ToString
            tbEmail.Text = dtDatosGenerales.Rows(0)("email").ToString
            tbReferencia.Text = dtDatosGenerales.Rows(0)("referenciabancaria").ToString
        End If
    End Sub

    Private Sub MatrizChck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MatrizChck.CheckedChanged
        If MatrizChck.Checked = True Then
            GroupBox1.Enabled = False
        ElseIf MatrizChck.Checked = False Then
            GroupBox1.Enabled = True
        End If
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Llena_Ciudades()
    End Sub
    Private Sub Llena_Ciudades()

    End Sub

    Private Sub txtCP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCP.KeyPress
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtTelefono_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefono.KeyPress
        Dim val As Integer
        val = AscW(e.KeyChar)
        'Validación enteros
        If (val >= 48 And val <= 57) Or val = 8 Or val = 13 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
End Class