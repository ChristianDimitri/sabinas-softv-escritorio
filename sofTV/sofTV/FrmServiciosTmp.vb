﻿Public Class FrmServiciosTmp

    Private Sub FrmServiciosTmp_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        llenar_lb(0)
        llenar_lb(1)

    End Sub

    Private Sub llenar_lb(opcion As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CType(opcion, Integer))
        BaseII.CreateMyParameter("@IdSession", SqlDbType.BigInt, CType(IdSessionRep, Long))
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, CType(identificador, Integer))

        If opcion = 0 Then
            lbIzquierda.DataSource = BaseII.ConsultaDT("CONtbl_ServiciosTmp")
        Else
            lbDerecha.DataSource = BaseII.ConsultaDT("CONtbl_ServiciosTmp")
        End If


    End Sub

    Private Sub button1_Click(sender As Object, e As EventArgs) Handles button1.Click

        Nue(0, lbIzquierda.SelectedValue)
        llenar_lb(0)
        llenar_lb(1)
    End Sub

    Private Sub button2_Click(sender As Object, e As EventArgs) Handles button2.Click
        Nue(1, 0)
        llenar_lb(0)
        llenar_lb(1)
    End Sub

    Private Sub button3_Click(sender As Object, e As EventArgs) Handles button3.Click
        Bor(0, lbDerecha.SelectedValue)
        llenar_lb(0)
        llenar_lb(1)
    End Sub

    Private Sub button4_Click(sender As Object, e As EventArgs) Handles button4.Click
        Bor(1, 0)
        llenar_lb(0)
        llenar_lb(1)
    End Sub


    Private Sub Nue(opcion As Integer, Clv_Servicio As Integer)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CType(opcion, Integer))
        BaseII.CreateMyParameter("@IdSession", SqlDbType.BigInt, CType(IdSessionRep, Long))
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, CType(Clv_Servicio, Integer))
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, CType(identificador, Integer))
        BaseII.ConsultaDT("NUEtbl_ServiciosTmp")

    End Sub


    Private Sub Bor(opcion As Integer, Clv_Servicio As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CType(opcion, Integer))
        BaseII.CreateMyParameter("@IdSession", SqlDbType.BigInt, CType(IdSessionRep, Long))
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, CType(Clv_Servicio, Integer))
        BaseII.ConsultaDT("BORtbl_ServiciosTmp")
    End Sub


    Private Sub bnAceptar_Click(sender As Object, e As EventArgs) Handles bnAceptar.Click
        If lbDerecha.Items.Count = 0 Then
            MessageBox.Show("Selecciona al menos un elemento.")
        End If

        If opRep = 8 Then
            FrmImprimeRepRecontratacion.Show()
            Me.Close()
        End If



    End Sub
End Class