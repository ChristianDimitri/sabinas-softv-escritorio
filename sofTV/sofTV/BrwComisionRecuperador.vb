﻿Public Class BrwComisionRecuperador

    Private Sub BrwComisionRecuperador_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        MuestraRangosComisionRecuperador()
    End Sub

    Private Sub BrwComisionRecuperador_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        MuestraRangosComisionRecuperador()

        If Me.DataGridView1.RowCount > 0 Then
            idRanRec = Me.DataGridView1.SelectedCells(0).Value.ToString
        Else
            idRanRec = 0
        End If
    End Sub

    Private Sub MuestraRangosComisionRecuperador()

        BaseII.limpiaParametros()
        DataGridView1.DataSource = BaseII.ConsultaDT("Usp_MuestraRangosComisionRecuperador")

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        opRanRec = "N"
        FrmComisionRecuperador.Show()
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If idRanRec > 0 Then
            opRanRec = "C"
            FrmComisionRecuperador.Show()
        Else
            MsgBox("Seleccione alguna comisión", MsgBoxStyle.Information)
        End If
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If idRanRec > 0 Then
            opRanRec = "M"
            FrmComisionRecuperador.Show()
        Else
            MsgBox("Seleccione alguna comisión", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If Me.DataGridView1.RowCount > 0 Then
            idRanRec = Me.DataGridView1.SelectedCells(0).Value.ToString
        Else
            idRanRec = 0
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, idRanRec)
        BaseII.Inserta("Usp_BorRangosComisionRecuperador")

        MsgBox("Borrado con exito")

        MuestraRangosComisionRecuperador()

    End Sub


End Class