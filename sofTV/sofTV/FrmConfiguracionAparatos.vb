﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Public Class FrmConfiguracionAparatos

    Private Sub FrmConfiguracionAparatos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaServiciosAparatos()
        If GloClv_TipSer = 2 Then
            DataGridView1.ReadOnly = True
        End If
    End Sub

    Private Function llenaServiciosAparatos()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)

        Dim DS As New DataSet
        DS.Clear()
        Dim listatablas As New List(Of String)
        listatablas.Add("SinAsignar")
        listatablas.Add("Asignada")

        DS = BaseII.ConsultaDS("DameRelServiciosAparatos", listatablas)

        ComboBox1.DataSource = DS.Tables.Item("SinAsignar")
        ComboBox1.DisplayMember = "concepto"
        ComboBox1.ValueMember = "TipoAparato"

        DataGridView1.DataSource = DS.Tables.Item("Asignada")
    End Function



    Private Sub BtnAgregar_Click(sender As Object, e As EventArgs) Handles BtnAgregar.Click
        Try
            If Not ComboBox1.SelectedValue.ToString.Length > 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)
        BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, ComboBox1.SelectedValue, 5)
        BaseII.Inserta("insertaRelServiciosAparatos")
        llenaServiciosAparatos()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If Not DataGridView1.SelectedRows(0).Cells("TipoAparato").Value.ToString().Length > 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)
        BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, DataGridView1.SelectedRows(0).Cells("TipoAparato").Value.ToString(), 5)
        BaseII.Inserta("DeleteRelServiciosAparatos")
        llenaServiciosAparatos()
    End Sub





    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub DataGridView1_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)
            BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, DataGridView1.Rows(e.RowIndex).Cells(0).Value, 5)
            BaseII.CreateMyParameter("@comando", SqlDbType.Bit, CInt(DataGridView1.Rows(e.RowIndex).Cells(2).Value))
            BaseII.Inserta("UpdateRelServiciosAparatos")
        Catch ex As System.Exception
        End Try
    End Sub
End Class