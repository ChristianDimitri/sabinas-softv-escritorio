﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.IO
Public Class FrmImprimeRepRecontratacion


    Private Sub FrmImprimeRepRecontratacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FrmImprimeReporte()
    End Sub

    Private Sub FrmImprimeReporte()

        Try
            Dim fechas As String = Nothing
            Dim customersByCityReport As ReportDocument = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            reportPath = RutaReportes + "\REPORTERecontratacion.rpt"
            '    mySelectFormula = "Agenda de Actividades Del Técnico"

            Dim dSet As New DataSet
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, CDate(fechaInicial))
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, CDate(fechaFinal))
            BaseII.CreateMyParameter("@IDSESSION", SqlDbType.BigInt, CType(IdSessionRep, Long))
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, CInt(identificador))

            Dim listatablas As New List(Of String)
            listatablas.Add("REPORTERecontratacion")
            dSet = BaseII.ConsultaDS("REPORTERecontratacion", listatablas)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dSet)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" + GloEmpresaPRincipal + "'"    'GloEmpresa 
            ' customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" + " del " + fechaInicial.ToShortDateString() + " al " + fechaFinal.ToShortDateString() + "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class