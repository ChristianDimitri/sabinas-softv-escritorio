﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCompania
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label23 As System.Windows.Forms.Label
        Dim Label24 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label26 As System.Windows.Forms.Label
        Dim Label27 As System.Windows.Forms.Label
        Dim Label28 As System.Windows.Forms.Label
        Dim Label29 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCompania))
        Me.CONSERVICIOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.tbidcompania = New System.Windows.Forms.TextBox()
        Me.tbrazonsocial = New System.Windows.Forms.TextBox()
        Me.tbrfc = New System.Windows.Forms.TextBox()
        Me.tbcalle = New System.Windows.Forms.TextBox()
        Me.tbexterior = New System.Windows.Forms.TextBox()
        Me.tbinterior = New System.Windows.Forms.TextBox()
        Me.tbcolonia = New System.Windows.Forms.TextBox()
        Me.tbentrecalles = New System.Windows.Forms.TextBox()
        Me.tblocalidad = New System.Windows.Forms.TextBox()
        Me.tbmunicipio = New System.Windows.Forms.TextBox()
        Me.tbestado = New System.Windows.Forms.TextBox()
        Me.tbpais = New System.Windows.Forms.TextBox()
        Me.tbcodigop = New System.Windows.Forms.TextBox()
        Me.tbtelefono = New System.Windows.Forms.TextBox()
        Me.tbfax = New System.Windows.Forms.TextBox()
        Me.tbemail = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ComboBoxEstado = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgvciudades = New System.Windows.Forms.DataGridView()
        Me.IdCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComboBoxCiudades = New System.Windows.Forms.ComboBox()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.ComboBoxPlazas = New System.Windows.Forms.ComboBox()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.tbcontacto = New System.Windows.Forms.TextBox()
        Me.TabDirecciones = New System.Windows.Forms.TabControl()
        Me.tabDireccion = New System.Windows.Forms.TabPage()
        Me.TabDireccionAlm = New System.Windows.Forms.TabPage()
        Me.tbCalleAlm = New System.Windows.Forms.TextBox()
        Me.tbCPAlm = New System.Windows.Forms.TextBox()
        Me.tbPaisAlm = New System.Windows.Forms.TextBox()
        Me.tbExtAlm = New System.Windows.Forms.TextBox()
        Me.tbEstAlm = New System.Windows.Forms.TextBox()
        Me.tbIntAlm = New System.Windows.Forms.TextBox()
        Me.tbMunALm = New System.Windows.Forms.TextBox()
        Me.tbColAlm = New System.Windows.Forms.TextBox()
        Me.tbLocAlm = New System.Windows.Forms.TextBox()
        Me.tbEntreCallesAlm = New System.Windows.Forms.TextBox()
        Clv_TxtLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label21 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label23 = New System.Windows.Forms.Label()
        Label24 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label26 = New System.Windows.Forms.Label()
        Label27 = New System.Windows.Forms.Label()
        Label28 = New System.Windows.Forms.Label()
        Label29 = New System.Windows.Forms.Label()
        CType(Me.CONSERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONSERVICIOSBindingNavigator.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvciudades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabDirecciones.SuspendLayout()
        Me.tabDireccion.SuspendLayout()
        Me.TabDireccionAlm.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TxtLabel.Location = New System.Drawing.Point(100, 49)
        Clv_TxtLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(81, 18)
        Clv_TxtLabel.TabIndex = 14
        Clv_TxtLabel.Text = "ID Plaza :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(107, 134)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(78, 18)
        Label1.TabIndex = 15
        Label1.Text = "Nombre :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(599, 177)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(52, 18)
        Label2.TabIndex = 16
        Label2.Text = "RFC :"
        Label2.Visible = False
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(96, 30)
        Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(56, 18)
        Label3.TabIndex = 17
        Label3.Text = "Calle :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(0, 97)
        Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(142, 18)
        Label4.TabIndex = 18
        Label4.Text = "Número Exterior :"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(313, 97)
        Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(136, 18)
        Label5.TabIndex = 19
        Label5.Text = "Número Interior :"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(43, 60)
        Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(107, 18)
        Label6.TabIndex = 20
        Label6.Text = "Entre calles :"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(76, 137)
        Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(76, 18)
        Label7.TabIndex = 21
        Label7.Text = "Colonia :"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(628, 27)
        Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(90, 18)
        Label8.TabIndex = 22
        Label8.Text = "Localidad :"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(628, 105)
        Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(90, 18)
        Label10.TabIndex = 24
        Label10.Text = "Municipio :"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(653, 70)
        Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(71, 18)
        Label11.TabIndex = 25
        Label11.Text = "Estado :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Label12.Location = New System.Drawing.Point(675, 148)
        Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(51, 18)
        Label12.TabIndex = 26
        Label12.Text = "País :"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(23, 172)
        Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(125, 18)
        Label13.TabIndex = 27
        Label13.Text = "Código Postal :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(100, 414)
        Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(84, 18)
        Label14.TabIndex = 28
        Label14.Text = "Teléfono :"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(456, 414)
        Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(45, 18)
        Label15.TabIndex = 29
        Label15.Text = "Fax :"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(769, 414)
        Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(60, 18)
        Label16.TabIndex = 30
        Label16.Text = "Email :"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(432, 27)
        Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(88, 18)
        Label9.TabIndex = 101
        Label9.Text = "Ciudades :"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Label17.Location = New System.Drawing.Point(73, 90)
        Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(105, 18)
        Label17.TabIndex = 49
        Label17.Text = "Distribuidor :"
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Label18.Location = New System.Drawing.Point(32, 27)
        Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(80, 18)
        Label18.TabIndex = 106
        Label18.Text = "Estados :"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.LightSlateGray
        Label19.Location = New System.Drawing.Point(101, 447)
        Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(82, 18)
        Label19.TabIndex = 51
        Label19.Text = "Contacto:"
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Label20.Location = New System.Drawing.Point(111, 27)
        Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(56, 18)
        Label20.TabIndex = 38
        Label20.Text = "Calle :"
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label21.ForeColor = System.Drawing.Color.LightSlateGray
        Label21.Location = New System.Drawing.Point(15, 95)
        Label21.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(142, 18)
        Label21.TabIndex = 39
        Label21.Text = "Número Exterior :"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label22.ForeColor = System.Drawing.Color.LightSlateGray
        Label22.Location = New System.Drawing.Point(328, 95)
        Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(136, 18)
        Label22.TabIndex = 40
        Label22.Text = "Número Interior :"
        '
        'Label23
        '
        Label23.AutoSize = True
        Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label23.ForeColor = System.Drawing.Color.LightSlateGray
        Label23.Location = New System.Drawing.Point(57, 58)
        Label23.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label23.Name = "Label23"
        Label23.Size = New System.Drawing.Size(107, 18)
        Label23.TabIndex = 41
        Label23.Text = "Entre calles :"
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.ForeColor = System.Drawing.Color.LightSlateGray
        Label24.Location = New System.Drawing.Point(91, 134)
        Label24.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(76, 18)
        Label24.TabIndex = 42
        Label24.Text = "Colonia :"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.ForeColor = System.Drawing.Color.LightSlateGray
        Label25.Location = New System.Drawing.Point(643, 25)
        Label25.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(90, 18)
        Label25.TabIndex = 43
        Label25.Text = "Localidad :"
        '
        'Label26
        '
        Label26.AutoSize = True
        Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label26.ForeColor = System.Drawing.Color.LightSlateGray
        Label26.Location = New System.Drawing.Point(643, 102)
        Label26.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label26.Name = "Label26"
        Label26.Size = New System.Drawing.Size(90, 18)
        Label26.TabIndex = 44
        Label26.Text = "Municipio :"
        '
        'Label27
        '
        Label27.AutoSize = True
        Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label27.ForeColor = System.Drawing.Color.LightSlateGray
        Label27.Location = New System.Drawing.Point(668, 68)
        Label27.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label27.Name = "Label27"
        Label27.Size = New System.Drawing.Size(71, 18)
        Label27.TabIndex = 45
        Label27.Text = "Estado :"
        '
        'Label28
        '
        Label28.AutoSize = True
        Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label28.ForeColor = System.Drawing.Color.LightSlateGray
        Label28.Location = New System.Drawing.Point(689, 145)
        Label28.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label28.Name = "Label28"
        Label28.Size = New System.Drawing.Size(51, 18)
        Label28.TabIndex = 46
        Label28.Text = "País :"
        '
        'Label29
        '
        Label29.AutoSize = True
        Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label29.ForeColor = System.Drawing.Color.LightSlateGray
        Label29.Location = New System.Drawing.Point(37, 170)
        Label29.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label29.Name = "Label29"
        Label29.Size = New System.Drawing.Size(125, 18)
        Label29.TabIndex = 47
        Label29.Text = "Código Postal :"
        '
        'CONSERVICIOSBindingNavigator
        '
        Me.CONSERVICIOSBindingNavigator.AddNewItem = Nothing
        Me.CONSERVICIOSBindingNavigator.CountItem = Nothing
        Me.CONSERVICIOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONSERVICIOSBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONSERVICIOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONSERVICIOSBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.CONSERVICIOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONSERVICIOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MoveLastItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MoveNextItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONSERVICIOSBindingNavigator.Name = "CONSERVICIOSBindingNavigator"
        Me.CONSERVICIOSBindingNavigator.PositionItem = Nothing
        Me.CONSERVICIOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONSERVICIOSBindingNavigator.Size = New System.Drawing.Size(1167, 28)
        Me.CONSERVICIOSBindingNavigator.TabIndex = 13
        Me.CONSERVICIOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(122, 25)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(105, 25)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONSERVICIOSBindingNavigatorSaveItem
        '
        Me.CONSERVICIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSERVICIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSERVICIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Name = "CONSERVICIOSBindingNavigatorSaveItem"
        Me.CONSERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 25)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'tbidcompania
        '
        Me.tbidcompania.BackColor = System.Drawing.Color.White
        Me.tbidcompania.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbidcompania.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbidcompania.Enabled = False
        Me.tbidcompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbidcompania.Location = New System.Drawing.Point(200, 47)
        Me.tbidcompania.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbidcompania.MaxLength = 5
        Me.tbidcompania.Name = "tbidcompania"
        Me.tbidcompania.Size = New System.Drawing.Size(133, 24)
        Me.tbidcompania.TabIndex = 0
        Me.tbidcompania.Text = "0"
        '
        'tbrazonsocial
        '
        Me.tbrazonsocial.BackColor = System.Drawing.Color.White
        Me.tbrazonsocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbrazonsocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrazonsocial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrazonsocial.Location = New System.Drawing.Point(200, 127)
        Me.tbrazonsocial.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbrazonsocial.MaxLength = 255
        Me.tbrazonsocial.Name = "tbrazonsocial"
        Me.tbrazonsocial.Size = New System.Drawing.Size(429, 24)
        Me.tbrazonsocial.TabIndex = 2
        '
        'tbrfc
        '
        Me.tbrfc.BackColor = System.Drawing.Color.White
        Me.tbrfc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbrfc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrfc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrfc.Location = New System.Drawing.Point(600, 169)
        Me.tbrfc.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbrfc.MaxLength = 15
        Me.tbrfc.Name = "tbrfc"
        Me.tbrfc.Size = New System.Drawing.Size(229, 24)
        Me.tbrfc.TabIndex = 2
        Me.tbrfc.Visible = False
        '
        'tbcalle
        '
        Me.tbcalle.BackColor = System.Drawing.Color.White
        Me.tbcalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcalle.Location = New System.Drawing.Point(169, 22)
        Me.tbcalle.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbcalle.MaxLength = 255
        Me.tbcalle.Name = "tbcalle"
        Me.tbcalle.Size = New System.Drawing.Size(429, 24)
        Me.tbcalle.TabIndex = 0
        '
        'tbexterior
        '
        Me.tbexterior.BackColor = System.Drawing.Color.White
        Me.tbexterior.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbexterior.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbexterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbexterior.Location = New System.Drawing.Point(168, 90)
        Me.tbexterior.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbexterior.MaxLength = 15
        Me.tbexterior.Name = "tbexterior"
        Me.tbexterior.Size = New System.Drawing.Size(105, 24)
        Me.tbexterior.TabIndex = 2
        '
        'tbinterior
        '
        Me.tbinterior.BackColor = System.Drawing.Color.White
        Me.tbinterior.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbinterior.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbinterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbinterior.Location = New System.Drawing.Point(496, 90)
        Me.tbinterior.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbinterior.MaxLength = 15
        Me.tbinterior.Name = "tbinterior"
        Me.tbinterior.Size = New System.Drawing.Size(105, 24)
        Me.tbinterior.TabIndex = 3
        '
        'tbcolonia
        '
        Me.tbcolonia.BackColor = System.Drawing.Color.White
        Me.tbcolonia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcolonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcolonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcolonia.Location = New System.Drawing.Point(169, 129)
        Me.tbcolonia.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbcolonia.MaxLength = 255
        Me.tbcolonia.Name = "tbcolonia"
        Me.tbcolonia.Size = New System.Drawing.Size(429, 24)
        Me.tbcolonia.TabIndex = 4
        '
        'tbentrecalles
        '
        Me.tbentrecalles.BackColor = System.Drawing.Color.White
        Me.tbentrecalles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbentrecalles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbentrecalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbentrecalles.Location = New System.Drawing.Point(172, 55)
        Me.tbentrecalles.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbentrecalles.MaxLength = 255
        Me.tbentrecalles.Name = "tbentrecalles"
        Me.tbentrecalles.Size = New System.Drawing.Size(429, 24)
        Me.tbentrecalles.TabIndex = 1
        '
        'tblocalidad
        '
        Me.tblocalidad.BackColor = System.Drawing.Color.White
        Me.tblocalidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tblocalidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tblocalidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblocalidad.Location = New System.Drawing.Point(740, 22)
        Me.tblocalidad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tblocalidad.MaxLength = 255
        Me.tblocalidad.Name = "tblocalidad"
        Me.tblocalidad.Size = New System.Drawing.Size(246, 24)
        Me.tblocalidad.TabIndex = 6
        '
        'tbmunicipio
        '
        Me.tbmunicipio.BackColor = System.Drawing.Color.White
        Me.tbmunicipio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbmunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbmunicipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbmunicipio.Location = New System.Drawing.Point(740, 101)
        Me.tbmunicipio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbmunicipio.MaxLength = 255
        Me.tbmunicipio.Name = "tbmunicipio"
        Me.tbmunicipio.Size = New System.Drawing.Size(246, 24)
        Me.tbmunicipio.TabIndex = 8
        '
        'tbestado
        '
        Me.tbestado.BackColor = System.Drawing.Color.White
        Me.tbestado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbestado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbestado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbestado.Location = New System.Drawing.Point(740, 62)
        Me.tbestado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbestado.MaxLength = 255
        Me.tbestado.Name = "tbestado"
        Me.tbestado.Size = New System.Drawing.Size(246, 24)
        Me.tbestado.TabIndex = 7
        '
        'tbpais
        '
        Me.tbpais.BackColor = System.Drawing.Color.White
        Me.tbpais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbpais.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbpais.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbpais.Location = New System.Drawing.Point(740, 140)
        Me.tbpais.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbpais.MaxLength = 255
        Me.tbpais.Name = "tbpais"
        Me.tbpais.Size = New System.Drawing.Size(246, 24)
        Me.tbpais.TabIndex = 9
        '
        'tbcodigop
        '
        Me.tbcodigop.BackColor = System.Drawing.Color.White
        Me.tbcodigop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcodigop.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcodigop.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcodigop.Location = New System.Drawing.Point(169, 165)
        Me.tbcodigop.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbcodigop.MaxLength = 5
        Me.tbcodigop.Name = "tbcodigop"
        Me.tbcodigop.Size = New System.Drawing.Size(246, 24)
        Me.tbcodigop.TabIndex = 5
        '
        'tbtelefono
        '
        Me.tbtelefono.BackColor = System.Drawing.Color.White
        Me.tbtelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbtelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbtelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbtelefono.Location = New System.Drawing.Point(201, 406)
        Me.tbtelefono.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbtelefono.MaxLength = 255
        Me.tbtelefono.Name = "tbtelefono"
        Me.tbtelefono.Size = New System.Drawing.Size(246, 24)
        Me.tbtelefono.TabIndex = 4
        '
        'tbfax
        '
        Me.tbfax.BackColor = System.Drawing.Color.White
        Me.tbfax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbfax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbfax.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbfax.Location = New System.Drawing.Point(515, 406)
        Me.tbfax.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbfax.MaxLength = 255
        Me.tbfax.Name = "tbfax"
        Me.tbfax.Size = New System.Drawing.Size(246, 24)
        Me.tbfax.TabIndex = 5
        '
        'tbemail
        '
        Me.tbemail.BackColor = System.Drawing.Color.White
        Me.tbemail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbemail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbemail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbemail.Location = New System.Drawing.Point(843, 407)
        Me.tbemail.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbemail.MaxLength = 255
        Me.tbemail.Name = "tbemail"
        Me.tbemail.Size = New System.Drawing.Size(246, 24)
        Me.tbemail.TabIndex = 6
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(932, 854)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 17
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Label18)
        Me.GroupBox1.Controls.Add(Me.ComboBoxEstado)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.dgvciudades)
        Me.GroupBox1.Controls.Add(Label9)
        Me.GroupBox1.Controls.Add(Me.ComboBoxCiudades)
        Me.GroupBox1.Controls.Add(Me.tbrfc)
        Me.GroupBox1.Controls.Add(Label2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(63, 474)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(1051, 375)
        Me.GroupBox1.TabIndex = 48
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Estado y ciudad/municipio de la plaza"
        '
        'ComboBoxEstado
        '
        Me.ComboBoxEstado.DisplayMember = "Nombre"
        Me.ComboBoxEstado.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxEstado.FormattingEnabled = True
        Me.ComboBoxEstado.Location = New System.Drawing.Point(36, 49)
        Me.ComboBoxEstado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxEstado.Name = "ComboBoxEstado"
        Me.ComboBoxEstado.Size = New System.Drawing.Size(391, 30)
        Me.ComboBoxEstado.TabIndex = 0
        Me.ComboBoxEstado.ValueMember = "Clv_Estado"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(863, 91)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(135, 34)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "ELIMINAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(863, 49)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(135, 34)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "AGREGAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'dgvciudades
        '
        Me.dgvciudades.AllowUserToAddRows = False
        Me.dgvciudades.AllowUserToDeleteRows = False
        Me.dgvciudades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvciudades.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdCompania, Me.estado, Me.Nombre, Me.Clv_Estado})
        Me.dgvciudades.Location = New System.Drawing.Point(36, 91)
        Me.dgvciudades.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvciudades.Name = "dgvciudades"
        Me.dgvciudades.ReadOnly = True
        Me.dgvciudades.RowHeadersVisible = False
        Me.dgvciudades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvciudades.Size = New System.Drawing.Size(807, 274)
        Me.dgvciudades.TabIndex = 102
        '
        'IdCompania
        '
        Me.IdCompania.DataPropertyName = "Clv_Ciudad"
        Me.IdCompania.HeaderText = "Clv_Ciudad"
        Me.IdCompania.Name = "IdCompania"
        Me.IdCompania.ReadOnly = True
        Me.IdCompania.Visible = False
        '
        'estado
        '
        Me.estado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.estado.DataPropertyName = "Estado"
        Me.estado.HeaderText = "Estado"
        Me.estado.Name = "estado"
        Me.estado.ReadOnly = True
        '
        'Nombre
        '
        Me.Nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Ciudad"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        '
        'Clv_Estado
        '
        Me.Clv_Estado.DataPropertyName = "Clv_Estado"
        Me.Clv_Estado.HeaderText = "Clv_Estado"
        Me.Clv_Estado.Name = "Clv_Estado"
        Me.Clv_Estado.ReadOnly = True
        Me.Clv_Estado.Visible = False
        '
        'ComboBoxCiudades
        '
        Me.ComboBoxCiudades.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCiudades.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCiudades.FormattingEnabled = True
        Me.ComboBoxCiudades.Location = New System.Drawing.Point(436, 49)
        Me.ComboBoxCiudades.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxCiudades.Name = "ComboBoxCiudades"
        Me.ComboBoxCiudades.Size = New System.Drawing.Size(405, 30)
        Me.ComboBoxCiudades.TabIndex = 1
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'ComboBoxPlazas
        '
        Me.ComboBoxPlazas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxPlazas.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxPlazas.FormattingEnabled = True
        Me.ComboBoxPlazas.Location = New System.Drawing.Point(200, 84)
        Me.ComboBoxPlazas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxPlazas.Name = "ComboBoxPlazas"
        Me.ComboBoxPlazas.Size = New System.Drawing.Size(428, 30)
        Me.ComboBoxPlazas.TabIndex = 1
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'tbcontacto
        '
        Me.tbcontacto.BackColor = System.Drawing.Color.White
        Me.tbcontacto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbcontacto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcontacto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcontacto.Location = New System.Drawing.Point(200, 441)
        Me.tbcontacto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbcontacto.MaxLength = 255
        Me.tbcontacto.Name = "tbcontacto"
        Me.tbcontacto.Size = New System.Drawing.Size(613, 24)
        Me.tbcontacto.TabIndex = 7
        '
        'TabDirecciones
        '
        Me.TabDirecciones.Controls.Add(Me.tabDireccion)
        Me.TabDirecciones.Controls.Add(Me.TabDireccionAlm)
        Me.TabDirecciones.Location = New System.Drawing.Point(63, 162)
        Me.TabDirecciones.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabDirecciones.Name = "TabDirecciones"
        Me.TabDirecciones.SelectedIndex = 0
        Me.TabDirecciones.Size = New System.Drawing.Size(1027, 239)
        Me.TabDirecciones.TabIndex = 3
        '
        'tabDireccion
        '
        Me.tabDireccion.Controls.Add(Me.tbcalle)
        Me.tabDireccion.Controls.Add(Label3)
        Me.tabDireccion.Controls.Add(Label4)
        Me.tabDireccion.Controls.Add(Label5)
        Me.tabDireccion.Controls.Add(Label6)
        Me.tabDireccion.Controls.Add(Label7)
        Me.tabDireccion.Controls.Add(Label8)
        Me.tabDireccion.Controls.Add(Label10)
        Me.tabDireccion.Controls.Add(Label11)
        Me.tabDireccion.Controls.Add(Label12)
        Me.tabDireccion.Controls.Add(Me.tbcodigop)
        Me.tabDireccion.Controls.Add(Label13)
        Me.tabDireccion.Controls.Add(Me.tbpais)
        Me.tabDireccion.Controls.Add(Me.tbexterior)
        Me.tabDireccion.Controls.Add(Me.tbestado)
        Me.tabDireccion.Controls.Add(Me.tbinterior)
        Me.tabDireccion.Controls.Add(Me.tbmunicipio)
        Me.tabDireccion.Controls.Add(Me.tbcolonia)
        Me.tabDireccion.Controls.Add(Me.tblocalidad)
        Me.tabDireccion.Controls.Add(Me.tbentrecalles)
        Me.tabDireccion.Location = New System.Drawing.Point(4, 25)
        Me.tabDireccion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tabDireccion.Name = "tabDireccion"
        Me.tabDireccion.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tabDireccion.Size = New System.Drawing.Size(1019, 210)
        Me.tabDireccion.TabIndex = 0
        Me.tabDireccion.Text = "Dirección Plaza"
        Me.tabDireccion.UseVisualStyleBackColor = True
        '
        'TabDireccionAlm
        '
        Me.TabDireccionAlm.Controls.Add(Me.tbCalleAlm)
        Me.TabDireccionAlm.Controls.Add(Label20)
        Me.TabDireccionAlm.Controls.Add(Label21)
        Me.TabDireccionAlm.Controls.Add(Label22)
        Me.TabDireccionAlm.Controls.Add(Label23)
        Me.TabDireccionAlm.Controls.Add(Label24)
        Me.TabDireccionAlm.Controls.Add(Label25)
        Me.TabDireccionAlm.Controls.Add(Label26)
        Me.TabDireccionAlm.Controls.Add(Label27)
        Me.TabDireccionAlm.Controls.Add(Label28)
        Me.TabDireccionAlm.Controls.Add(Me.tbCPAlm)
        Me.TabDireccionAlm.Controls.Add(Label29)
        Me.TabDireccionAlm.Controls.Add(Me.tbPaisAlm)
        Me.TabDireccionAlm.Controls.Add(Me.tbExtAlm)
        Me.TabDireccionAlm.Controls.Add(Me.tbEstAlm)
        Me.TabDireccionAlm.Controls.Add(Me.tbIntAlm)
        Me.TabDireccionAlm.Controls.Add(Me.tbMunALm)
        Me.TabDireccionAlm.Controls.Add(Me.tbColAlm)
        Me.TabDireccionAlm.Controls.Add(Me.tbLocAlm)
        Me.TabDireccionAlm.Controls.Add(Me.tbEntreCallesAlm)
        Me.TabDireccionAlm.Location = New System.Drawing.Point(4, 25)
        Me.TabDireccionAlm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabDireccionAlm.Name = "TabDireccionAlm"
        Me.TabDireccionAlm.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabDireccionAlm.Size = New System.Drawing.Size(1019, 210)
        Me.TabDireccionAlm.TabIndex = 1
        Me.TabDireccionAlm.Text = "Dirección Almacén"
        Me.TabDireccionAlm.UseVisualStyleBackColor = True
        '
        'tbCalleAlm
        '
        Me.tbCalleAlm.BackColor = System.Drawing.Color.White
        Me.tbCalleAlm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbCalleAlm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCalleAlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCalleAlm.Location = New System.Drawing.Point(184, 20)
        Me.tbCalleAlm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCalleAlm.MaxLength = 255
        Me.tbCalleAlm.Name = "tbCalleAlm"
        Me.tbCalleAlm.Size = New System.Drawing.Size(429, 24)
        Me.tbCalleAlm.TabIndex = 0
        '
        'tbCPAlm
        '
        Me.tbCPAlm.BackColor = System.Drawing.Color.White
        Me.tbCPAlm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbCPAlm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCPAlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCPAlm.Location = New System.Drawing.Point(184, 162)
        Me.tbCPAlm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCPAlm.MaxLength = 5
        Me.tbCPAlm.Name = "tbCPAlm"
        Me.tbCPAlm.Size = New System.Drawing.Size(246, 24)
        Me.tbCPAlm.TabIndex = 5
        '
        'tbPaisAlm
        '
        Me.tbPaisAlm.BackColor = System.Drawing.Color.White
        Me.tbPaisAlm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbPaisAlm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbPaisAlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPaisAlm.Location = New System.Drawing.Point(755, 138)
        Me.tbPaisAlm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbPaisAlm.MaxLength = 255
        Me.tbPaisAlm.Name = "tbPaisAlm"
        Me.tbPaisAlm.Size = New System.Drawing.Size(246, 24)
        Me.tbPaisAlm.TabIndex = 9
        '
        'tbExtAlm
        '
        Me.tbExtAlm.BackColor = System.Drawing.Color.White
        Me.tbExtAlm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbExtAlm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbExtAlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbExtAlm.Location = New System.Drawing.Point(183, 87)
        Me.tbExtAlm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbExtAlm.MaxLength = 15
        Me.tbExtAlm.Name = "tbExtAlm"
        Me.tbExtAlm.Size = New System.Drawing.Size(105, 24)
        Me.tbExtAlm.TabIndex = 2
        '
        'tbEstAlm
        '
        Me.tbEstAlm.BackColor = System.Drawing.Color.White
        Me.tbEstAlm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbEstAlm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbEstAlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEstAlm.Location = New System.Drawing.Point(755, 59)
        Me.tbEstAlm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbEstAlm.MaxLength = 255
        Me.tbEstAlm.Name = "tbEstAlm"
        Me.tbEstAlm.Size = New System.Drawing.Size(246, 24)
        Me.tbEstAlm.TabIndex = 7
        '
        'tbIntAlm
        '
        Me.tbIntAlm.BackColor = System.Drawing.Color.White
        Me.tbIntAlm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbIntAlm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbIntAlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIntAlm.Location = New System.Drawing.Point(511, 87)
        Me.tbIntAlm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbIntAlm.MaxLength = 15
        Me.tbIntAlm.Name = "tbIntAlm"
        Me.tbIntAlm.Size = New System.Drawing.Size(105, 24)
        Me.tbIntAlm.TabIndex = 3
        '
        'tbMunALm
        '
        Me.tbMunALm.BackColor = System.Drawing.Color.White
        Me.tbMunALm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbMunALm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbMunALm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMunALm.Location = New System.Drawing.Point(755, 98)
        Me.tbMunALm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbMunALm.MaxLength = 255
        Me.tbMunALm.Name = "tbMunALm"
        Me.tbMunALm.Size = New System.Drawing.Size(246, 24)
        Me.tbMunALm.TabIndex = 8
        '
        'tbColAlm
        '
        Me.tbColAlm.BackColor = System.Drawing.Color.White
        Me.tbColAlm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbColAlm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbColAlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbColAlm.Location = New System.Drawing.Point(184, 127)
        Me.tbColAlm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbColAlm.MaxLength = 255
        Me.tbColAlm.Name = "tbColAlm"
        Me.tbColAlm.Size = New System.Drawing.Size(429, 24)
        Me.tbColAlm.TabIndex = 4
        '
        'tbLocAlm
        '
        Me.tbLocAlm.BackColor = System.Drawing.Color.White
        Me.tbLocAlm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbLocAlm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbLocAlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLocAlm.Location = New System.Drawing.Point(755, 20)
        Me.tbLocAlm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbLocAlm.MaxLength = 255
        Me.tbLocAlm.Name = "tbLocAlm"
        Me.tbLocAlm.Size = New System.Drawing.Size(246, 24)
        Me.tbLocAlm.TabIndex = 6
        '
        'tbEntreCallesAlm
        '
        Me.tbEntreCallesAlm.BackColor = System.Drawing.Color.White
        Me.tbEntreCallesAlm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbEntreCallesAlm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbEntreCallesAlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEntreCallesAlm.Location = New System.Drawing.Point(187, 53)
        Me.tbEntreCallesAlm.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbEntreCallesAlm.MaxLength = 255
        Me.tbEntreCallesAlm.Name = "tbEntreCallesAlm"
        Me.tbEntreCallesAlm.Size = New System.Drawing.Size(429, 24)
        Me.tbEntreCallesAlm.TabIndex = 1
        '
        'FrmCompania
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1167, 898)
        Me.Controls.Add(Me.TabDirecciones)
        Me.Controls.Add(Me.tbcontacto)
        Me.Controls.Add(Label19)
        Me.Controls.Add(Me.ComboBoxPlazas)
        Me.Controls.Add(Label17)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.tbemail)
        Me.Controls.Add(Me.tbfax)
        Me.Controls.Add(Me.tbtelefono)
        Me.Controls.Add(Me.tbrazonsocial)
        Me.Controls.Add(Me.tbidcompania)
        Me.Controls.Add(Label16)
        Me.Controls.Add(Label15)
        Me.Controls.Add(Label14)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Clv_TxtLabel)
        Me.Controls.Add(Me.CONSERVICIOSBindingNavigator)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmCompania"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Plaza"
        CType(Me.CONSERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONSERVICIOSBindingNavigator.ResumeLayout(False)
        Me.CONSERVICIOSBindingNavigator.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvciudades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabDirecciones.ResumeLayout(False)
        Me.tabDireccion.ResumeLayout(False)
        Me.tabDireccion.PerformLayout()
        Me.TabDireccionAlm.ResumeLayout(False)
        Me.TabDireccionAlm.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CONSERVICIOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbidcompania As System.Windows.Forms.TextBox
    Friend WithEvents tbrazonsocial As System.Windows.Forms.TextBox
    Friend WithEvents tbrfc As System.Windows.Forms.TextBox
    Friend WithEvents tbcalle As System.Windows.Forms.TextBox
    Friend WithEvents tbexterior As System.Windows.Forms.TextBox
    Friend WithEvents tbinterior As System.Windows.Forms.TextBox
    Friend WithEvents tbcolonia As System.Windows.Forms.TextBox
    Friend WithEvents tbentrecalles As System.Windows.Forms.TextBox
    Friend WithEvents tblocalidad As System.Windows.Forms.TextBox
    Friend WithEvents tbmunicipio As System.Windows.Forms.TextBox
    Friend WithEvents tbestado As System.Windows.Forms.TextBox
    Friend WithEvents tbpais As System.Windows.Forms.TextBox
    Friend WithEvents tbcodigop As System.Windows.Forms.TextBox
    Friend WithEvents tbtelefono As System.Windows.Forms.TextBox
    Friend WithEvents tbfax As System.Windows.Forms.TextBox
    Friend WithEvents tbemail As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents dgvciudades As System.Windows.Forms.DataGridView
    Friend WithEvents ComboBoxCiudades As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ComboBoxPlazas As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxEstado As System.Windows.Forms.ComboBox
    Friend WithEvents IdCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents tbcontacto As System.Windows.Forms.TextBox
    Friend WithEvents TabDirecciones As System.Windows.Forms.TabControl
    Friend WithEvents tabDireccion As System.Windows.Forms.TabPage
    Friend WithEvents TabDireccionAlm As System.Windows.Forms.TabPage
    Friend WithEvents tbCalleAlm As System.Windows.Forms.TextBox
    Friend WithEvents tbCPAlm As System.Windows.Forms.TextBox
    Friend WithEvents tbPaisAlm As System.Windows.Forms.TextBox
    Friend WithEvents tbExtAlm As System.Windows.Forms.TextBox
    Friend WithEvents tbEstAlm As System.Windows.Forms.TextBox
    Friend WithEvents tbIntAlm As System.Windows.Forms.TextBox
    Friend WithEvents tbMunALm As System.Windows.Forms.TextBox
    Friend WithEvents tbColAlm As System.Windows.Forms.TextBox
    Friend WithEvents tbLocAlm As System.Windows.Forms.TextBox
    Friend WithEvents tbEntreCallesAlm As System.Windows.Forms.TextBox
End Class
