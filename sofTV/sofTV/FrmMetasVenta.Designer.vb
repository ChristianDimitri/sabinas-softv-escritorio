﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMetasVenta
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBoxBonoPlatino = New System.Windows.Forms.TextBox()
        Me.TextBoxMetaPlatino = New System.Windows.Forms.TextBox()
        Me.TextBoxTelemarketing = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxCambaceo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.eliminar = New System.Windows.Forms.ToolStripButton()
        Me.guardar = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TextBoxMetaSucursal = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvrelsector = New System.Windows.Forms.DataGridView()
        Me.Clv_Sucursal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Meta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_SectorTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.GroupBox1.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvrelsector, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBoxBonoPlatino)
        Me.GroupBox1.Controls.Add(Me.TextBoxMetaPlatino)
        Me.GroupBox1.Controls.Add(Me.TextBoxTelemarketing)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBoxCambaceo)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.BindingNavigator1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(606, 231)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Metas Generales"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(157, 133)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 16)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Bono Platino:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(119, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(138, 16)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Meta Bono Platino:"
        '
        'TextBoxBonoPlatino
        '
        Me.TextBoxBonoPlatino.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxBonoPlatino.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxBonoPlatino.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxBonoPlatino.Location = New System.Drawing.Point(278, 129)
        Me.TextBoxBonoPlatino.Name = "TextBoxBonoPlatino"
        Me.TextBoxBonoPlatino.Size = New System.Drawing.Size(127, 24)
        Me.TextBoxBonoPlatino.TabIndex = 11
        '
        'TextBoxMetaPlatino
        '
        Me.TextBoxMetaPlatino.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxMetaPlatino.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxMetaPlatino.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxMetaPlatino.Location = New System.Drawing.Point(278, 99)
        Me.TextBoxMetaPlatino.Name = "TextBoxMetaPlatino"
        Me.TextBoxMetaPlatino.Size = New System.Drawing.Size(127, 24)
        Me.TextBoxMetaPlatino.TabIndex = 10
        '
        'TextBoxTelemarketing
        '
        Me.TextBoxTelemarketing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxTelemarketing.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxTelemarketing.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTelemarketing.Location = New System.Drawing.Point(278, 186)
        Me.TextBoxTelemarketing.Name = "TextBoxTelemarketing"
        Me.TextBoxTelemarketing.Size = New System.Drawing.Size(127, 24)
        Me.TextBoxTelemarketing.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(141, 190)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Telemarketing:"
        '
        'TextBoxCambaceo
        '
        Me.TextBoxCambaceo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxCambaceo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxCambaceo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxCambaceo.Location = New System.Drawing.Point(278, 69)
        Me.TextBoxCambaceo.Name = "TextBoxCambaceo"
        Me.TextBoxCambaceo.Size = New System.Drawing.Size(127, 24)
        Me.TextBoxCambaceo.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(166, 73)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 16)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Cambaceo:"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.eliminar, Me.guardar})
        Me.BindingNavigator1.Location = New System.Drawing.Point(3, 18)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(600, 25)
        Me.BindingNavigator1.TabIndex = 2
        Me.BindingNavigator1.TabStop = True
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'eliminar
        '
        Me.eliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.eliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.eliminar.Name = "eliminar"
        Me.eliminar.Size = New System.Drawing.Size(74, 22)
        Me.eliminar.Text = "&ELIMINAR"
        Me.eliminar.Visible = False
        '
        'guardar
        '
        Me.guardar.Name = "guardar"
        Me.guardar.Size = New System.Drawing.Size(75, 22)
        Me.guardar.Text = "&GUARDAR"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBoxMetaSucursal)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.dgvrelsector)
        Me.GroupBox2.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.GroupBox2.Controls.Add(Me.Clv_SectorTextBox)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 249)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(606, 388)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Metas Sucursal"
        '
        'TextBoxMetaSucursal
        '
        Me.TextBoxMetaSucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxMetaSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxMetaSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxMetaSucursal.Location = New System.Drawing.Point(269, 39)
        Me.TextBoxMetaSucursal.Name = "TextBoxMetaSucursal"
        Me.TextBoxMetaSucursal.Size = New System.Drawing.Size(127, 24)
        Me.TextBoxMetaSucursal.TabIndex = 12
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(217, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 16)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Meta:"
        '
        'dgvrelsector
        '
        Me.dgvrelsector.AllowUserToAddRows = False
        Me.dgvrelsector.AllowUserToDeleteRows = False
        Me.dgvrelsector.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvrelsector.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvrelsector.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Sucursal, Me.Nombre, Me.Meta})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvrelsector.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvrelsector.Location = New System.Drawing.Point(92, 83)
        Me.dgvrelsector.Name = "dgvrelsector"
        Me.dgvrelsector.ReadOnly = True
        Me.dgvrelsector.RowHeadersVisible = False
        Me.dgvrelsector.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvrelsector.Size = New System.Drawing.Size(401, 291)
        Me.dgvrelsector.TabIndex = 7
        Me.dgvrelsector.TabStop = False
        '
        'Clv_Sucursal
        '
        Me.Clv_Sucursal.DataPropertyName = "Clv_Sucursal"
        Me.Clv_Sucursal.HeaderText = "Clv_Sucursal"
        Me.Clv_Sucursal.Name = "Clv_Sucursal"
        Me.Clv_Sucursal.ReadOnly = True
        Me.Clv_Sucursal.Visible = False
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 265
        '
        'Meta
        '
        Me.Meta.DataPropertyName = "Meta"
        Me.Meta.HeaderText = "Meta"
        Me.Meta.Name = "Meta"
        Me.Meta.ReadOnly = True
        Me.Meta.Width = 130
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(236, 149)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.ReadOnly = True
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_ColoniaTextBox.TabIndex = 11
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'Clv_SectorTextBox
        '
        Me.Clv_SectorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_SectorTextBox.Location = New System.Drawing.Point(220, 150)
        Me.Clv_SectorTextBox.Name = "Clv_SectorTextBox"
        Me.Clv_SectorTextBox.ReadOnly = True
        Me.Clv_SectorTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_SectorTextBox.TabIndex = 1
        Me.Clv_SectorTextBox.TabStop = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(509, 43)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(91, 33)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Modificar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(509, 84)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(91, 33)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Aceptar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmMetasVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(635, 642)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmMetasVenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Metas de Venta"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvrelsector, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents eliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents guardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvrelsector As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SectorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents TextBoxCambaceo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBoxTelemarketing As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBoxMetaSucursal As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Clv_Sucursal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Meta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBoxBonoPlatino As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxMetaPlatino As System.Windows.Forms.TextBox
End Class


