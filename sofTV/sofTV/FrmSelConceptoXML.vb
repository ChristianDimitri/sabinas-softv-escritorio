﻿Public Class FrmSelConceptoXml

    Private Sub FrmSelConceptoXml_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'Por si es uno
        'DocPlazas.DocumentElement.RemoveAll()
        If GloOpFiltrosXML = "DetalleOrdenes" Then
            Me.Text = "Selección Trabajos"
            Label1.Text = "Selecciona los Trabajos"
            CargarElementosTrabajoXML()
        End If
        If GloOpFiltrosXML = "QuejasConcetoDetallado" Then
            Me.Text = "Selección Problema"
            Label1.Text = "Selecciona los Problemas"
            CargarElementosProblemaXML()
        End If
        If GloOpFiltrosXML = "QuejasPorIdentificador" Then
            Me.Text = "Selección Identificacion de Usuario"
            Label1.Text = "Selecciona los Identificadores de Usuario"
            CargarElementosIdentificacionXML()
        End If
        llenalistboxs()
        ' If PasarTodoXML Then
        'llevamealotro()
        'Me.Close()
        ' If
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocConceptos.SelectNodes("//CONCEPTO")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clave") = loquehay.SelectedValue Then
                elem.SetAttribute("Seleccion", 1)
            End If
        Next
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocConceptos.SelectNodes("//CONCEPTO")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clave") = seleccion.SelectedValue Then
                elem.SetAttribute("Seleccion", 0)
            End If
        Next
        llenalistboxs()

    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocConceptos.SelectNodes("//CONCEPTO")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        llenalistboxs()

    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocConceptos.SelectNodes("//CONCEPTO")
        For Each elem As Xml.XmlElement In elementos

            elem.SetAttribute("Seleccion", 0)

        Next
        llenalistboxs()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        EntradasSelCiudad = 0
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        If GloOpFiltrosXML = "DetalleOrdenes" Then
            FrmImprimirPPE.Show()
        End If
        If GloOpFiltrosXML = "QuejasConcetoDetallado" Then
            FrmImprimirPPE.Show()
        End If
        If GloOpFiltrosXML = "QuejasPorIdentificador" Then
            eOpPPE = 17
            FrmSelFechasPPE.Show()
        End If
        Me.Close()
    End Sub
    Private Sub llevamealotro()
        Dim elementos = DocConceptos.SelectNodes("//CONCEPTO")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        If GloOpFiltrosXML = "DetalleOrdenes" Then
            FrmImprimirPPE.Show()
        End If
        If GloOpFiltrosXML = "QuejasConcetoDetallado" Then
            FrmImprimirPPE.Show()
        End If
        If GloOpFiltrosXML = "QuejasPorIdentificador" Then
            eOpPPE = 17
            FrmSelFechasPPE.Show()
        End If
    End Sub

    Private Sub llenalistboxs()
        Dim dt As New DataTable
        dt.Columns.Add("Clave", Type.GetType("System.String"))
        dt.Columns.Add("Nombre", Type.GetType("System.String"))

        Dim dt2 As New DataTable
        dt2.Columns.Add("Clave", Type.GetType("System.String"))
        dt2.Columns.Add("Nombre", Type.GetType("System.String"))

        Dim elementos = DocConceptos.SelectNodes("//CONCEPTO")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Seleccion") = 0 Then
                dt.Rows.Add(elem.GetAttribute("Clave"), elem.GetAttribute("Nombre"))
            End If
            If elem.GetAttribute("Seleccion") = 1 Then
                dt2.Rows.Add(elem.GetAttribute("Clave"), elem.GetAttribute("Nombre"))
            End If
        Next
        loquehay.DataSource = New BindingSource(dt, Nothing)
        seleccion.DataSource = New BindingSource(dt2, Nothing)
    End Sub

End Class