﻿Imports System.Data.SqlClient
Public Class FrmCancelacionRetiro

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                GloIdCompania = ComboBoxCompanias.SelectedValue
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmCancelacionRetiro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Llena_companias()
        busca(0)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
        lbNombre.BackColor = lbCalle.BackColor
        lbNombre.ForeColor = lbCalle.ForeColor
    End Sub

    Private Sub busca(ByVal op As Integer)
        Try
            Dim sTATUS As String = "P"
            Dim autom As Boolean = False

            If rbPendientes.Checked = True Then
                sTATUS = "P"
            ElseIf rbEjecutadas.Checked = True Then
                sTATUS = "E"
            End If
            Select Case op
                Case 0 'Al principio todas
                    BuscaSeparado(0, 0, "", "", "", op, 0, "")
                Case 1 'Contrato
                    If Len(Trim(tbContrato.Text)) > 0 Then
                        Dim arr() = tbContrato.Text.Split("-")
                        BaseII.limpiaParametros()
                        BaseII.CreateMyParameter("@contratoCompania", SqlDbType.BigInt, arr(0).ToString)
                        BaseII.CreateMyParameter("@idCompania", SqlDbType.BigInt, arr(1).ToString)
                        BaseII.CreateMyParameter("@contrato", ParameterDirection.Output, SqlDbType.Int)
                        BaseII.ProcedimientoOutPut("sp_dameContrato")
                        BuscaSeparado(0, BaseII.dicoPar("@contrato"), "", "", "", op, 0, "")
                    Else
                        MsgBox("No se puede realizar la búsqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                Case 2 'Nombre y apellidos
                    If Len(Trim(tbNombre.Text)) > 0 Or Len(Trim(tbApellido1.Text)) > 0 Or Len(Trim(tbApellido2.Text)) > 0 Then

                        BuscaSeparado(0, 0, tbNombre.Text, tbApellido1.Text, tbApellido2.Text, op, 0, "")
                    Else
                        MsgBox("No se puede realizar la búsqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                Case 4 'Por compania
                    BuscaSeparado(0, 0, "", "", "", op, ComboBoxCompanias.SelectedValue, "")
                Case 5 'Por orden
                    If IsNumeric(tbOrden.Text) = True Then
                        BuscaSeparado(tbOrden.Text, 0, "", "", "", op, 0, "")
                    Else
                        MsgBox("No se puede realizar la búsqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                Case 6 'Por status
                    If rbEjecutadas.Checked Then
                        BuscaSeparado(0, 0, "", "", "", op, 0, "E")
                    Else
                        BuscaSeparado(0, 0, "", "", "", op, 0, "P")
                    End If
            End Select
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BuscaSeparado(ByVal Clv_Orden As Long, ByVal Contrato As Integer, ByVal NOMBRE As String, ByVal ApePaterno As String, _
                             ByVal ApeMaterno As String, ByVal OP As Integer, oIdCompania As Integer, ByVal status As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlDataAdapter()
        Dim stb As String = ""
        Dim tarjeta As String = ""
        Dim consulta As String = Nothing
        Try 'Op=4 BuscaSeparado(0, 0, 0, "", "", "", "", "", 49, autom, Me.cmbColonias.SelectedValue, ComboBoxCompanias.SelectedValue)
            CON.Open()
            If Len(NOMBRE) = 0 Then
                NOMBRE = "''"
            ElseIf Len(NOMBRE) > 0 Then
                NOMBRE = "'" + NOMBRE + "'"
            End If
            If Len(ApePaterno) = 0 Then
                ApePaterno = "''"
            ElseIf Len(ApePaterno) > 0 Then
                ApePaterno = "'" + ApePaterno + "'"
            End If
            If Len(ApeMaterno) = 0 Then
                ApeMaterno = "''"
            ElseIf Len(ApeMaterno) > 0 Then
                ApeMaterno = "'" + ApeMaterno + "'"
            End If
            'If Len(TxtSetUpBox.Text) = 0 Then
            '    stb = "''"
            'ElseIf Len(TxtSetUpBox.Text) > 0 Then
            '    stb = "'" + TxtSetUpBox.Text + "'"
            'End If
            consulta = "Exec BuscaOrdSerRetiro 0," + CStr(Clv_Orden) + "," + CStr(Contrato) + "," + CStr(NOMBRE) + "," + CStr(ApePaterno) + "," + CStr(ApeMaterno) + "," + _
                        CStr(OP) + "," + CStr(oIdCompania) + "," + GloClvUsuario.ToString + ", '" + status + "'"

            CMD = New SqlDataAdapter(consulta, CON)

            Dim dt As New DataTable
            Dim BS As New BindingSource

            CMD.Fill(dt)
            BS.DataSource = dt
            Me.dgvOrdenes.DataSource = BS.DataSource

            CON.Close()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dgvOrdenes_SelectionChanged(sender As Object, e As EventArgs) Handles dgvOrdenes.SelectionChanged
        Try
            lbOrden.Text = dgvOrdenes.SelectedCells(0).Value.ToString
            lbContrato.Text = dgvOrdenes.SelectedCells(2).Value.ToString
            lbNombre.Text = dgvOrdenes.SelectedCells(3).Value.ToString + " " + dgvOrdenes.SelectedCells(4).Value.ToString + " " + dgvOrdenes.SelectedCells(5).Value.ToString
            lbCalle.Text = dgvOrdenes.SelectedCells(6).Value.ToString
            lbNumero.Text = dgvOrdenes.SelectedCells(7).Value.ToString
            Dim I As Integer = 0
            Dim X As Integer = 0
            If IsNumeric(lbOrden.Text) = True Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, lbOrden.Text)
                Dim dt As DataTable
                dt = BaseII.ConsultaDT("Dame_DetOrdSer")
                Dim FilaRow As DataRow
                Me.TreeView1.Nodes.Clear()
                For Each FilaRow In dt.Rows
                    Me.TreeView1.Nodes.Add(Trim(FilaRow("descripcion").ToString()))
                    I += 1
                Next
                Me.TreeView1.ExpandAll()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        busca(2)
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        busca(1)
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        busca(5)
    End Sub

    Private Sub tbOrden_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbOrden.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(5)
        End If
    End Sub

    Private Sub tbContrato_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbContrato.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub

    Private Sub tbNombre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbNombre.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(2)
        End If
    End Sub

    Private Sub tbApellido1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbApellido1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(2)
        End If
    End Sub

    Private Sub tbApellido2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbApellido2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(2)
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If dgvOrdenes.Rows.Count = 0 Then
            MsgBox("No hay ordenes por cancelar")
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, lbOrden.Text)
            BaseII.CreateMyParameter("@valida", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("ValidaCancelacionOrden")
            If BaseII.dicoPar("@valida") = 0 Then
                MsgBox("Los aparatos de la orden ya han sido devueltos al almacén o el cliente ha pasado al proceso de recontratación, ya no se puede cancelar")
                Exit Sub
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, lbOrden.Text)
            BaseII.Inserta("CancelaOrdenRetiro")
            busca(0)
            MsgBox("Orden cancelada con éxito")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        busca(4)
    End Sub

    Private Sub rbPendientes_CheckedChanged(sender As Object, e As EventArgs) Handles rbPendientes.CheckedChanged
        busca(6)
    End Sub

    Private Sub rbEjecutadas_CheckedChanged(sender As Object, e As EventArgs) Handles rbEjecutadas.CheckedChanged
        busca(6)
    End Sub
End Class