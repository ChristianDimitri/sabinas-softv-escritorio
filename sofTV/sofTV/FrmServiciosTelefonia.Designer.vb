<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmServiciosTelefonia
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim PrecioLabel As System.Windows.Forms.Label
        Dim Clv_ServicioLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Puntos3Label As System.Windows.Forms.Label
        Dim Puntos6Label As System.Windows.Forms.Label
        Dim Puntos11Label As System.Windows.Forms.Label
        Dim Punto_Pronto_PagoLabel As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label24 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim CostoLabel As System.Windows.Forms.Label
        Dim Clv_ServicioDigitalLabel As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmServiciosTelefonia))
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.NumericUpDown5 = New System.Windows.Forms.NumericUpDown()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.ConServiciosTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.CMBTextBox7 = New System.Windows.Forms.TextBox()
        Me.NumericUpDown8 = New System.Windows.Forms.NumericUpDown()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.CMBTextBox4 = New System.Windows.Forms.TextBox()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.Muestra_Dias_FinalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetyahve = New sofTV.DataSetyahve()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Muestra_Dias_inicialBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBTextBoxDias = New System.Windows.Forms.TextBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CMBTextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown7 = New System.Windows.Forms.NumericUpDown()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TxtBoxCosto = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CMBTextBox9 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraServiciosDigitalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConServiciosTelBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ConServiciosTelBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.Clv_TipoClienteTextBox = New System.Windows.Forms.TextBox()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipoClientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.CMBTextBox3 = New System.Windows.Forms.TextBox()
        Me.Puntos3NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Puntos11NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Punto_Pronto_PagoNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Puntos6NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.CMBSale_en_CarteraLabel = New System.Windows.Forms.Label()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.CONSERVICIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TxtTextBox = New System.Windows.Forms.TextBox()
        Me.Es_PrincipalCheckBox = New System.Windows.Forms.CheckBox()
        Me.Sale_en_CarteraCheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CMBTextBox28 = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.CONRel_Trabajos_NoCobroMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBGenera_OrdenLabel = New System.Windows.Forms.Label()
        Me.CMBAplicanComLabel = New System.Windows.Forms.Label()
        Me.Sale_en_CarteraCheckBox = New System.Windows.Forms.CheckBox()
        Me.Genera_OrdenCheckBox = New System.Windows.Forms.CheckBox()
        Me.CMBEs_PrincipalLabel = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PrecioTextBox = New System.Windows.Forms.TextBox()
        Me.CMBPanel4 = New System.Windows.Forms.Panel()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CMBTextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox()
        Me.CLV_SERVICIOTextBox1 = New System.Windows.Forms.TextBox()
        Me.TxtClv_Session = New System.Windows.Forms.TextBox()
        Me.CLV_LLAVETextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CONSERVICIOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CONSERVICIOSTableAdapter = New sofTV.DataSetLidia2TableAdapters.CONSERVICIOSTableAdapter()
        Me.MuestraTipoClientesTableAdapter = New sofTV.DataSetLidia2TableAdapters.MuestraTipoClientesTableAdapter()
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter = New sofTV.DataSetEDGARTableAdapters.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter()
        Me.CONRel_Trabajos_NoCobroMensualTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONRel_Trabajos_NoCobroMensualTableAdapter()
        Me.ConServiciosTelTableAdapter = New sofTV.DataSetLidia2TableAdapters.ConServiciosTelTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter = New sofTV.DataSetyahveTableAdapters.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter()
        Me.Borrar_Rel_ServiciosDigitales_Tel_TMPBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borrar_Rel_ServiciosDigitales_Tel_TMPTableAdapter = New sofTV.DataSetyahveTableAdapters.Borrar_Rel_ServiciosDigitales_Tel_TMPTableAdapter()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.CostoTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioDigitalTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipocobroTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioTextBox2 = New System.Windows.Forms.TextBox()
        Me.Clv_SessionTextBox = New System.Windows.Forms.TextBox()
        Me.Clave_IdTextBox = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Muestra_Dias_inicialTableAdapter = New sofTV.DataSetyahveTableAdapters.Muestra_Dias_inicialTableAdapter()
        Me.Muestra_Dias_FinalTableAdapter = New sofTV.DataSetyahveTableAdapters.Muestra_Dias_FinalTableAdapter()
        PrecioLabel = New System.Windows.Forms.Label()
        Clv_ServicioLabel = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        Clv_TxtLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Puntos3Label = New System.Windows.Forms.Label()
        Puntos6Label = New System.Windows.Forms.Label()
        Puntos11Label = New System.Windows.Forms.Label()
        Punto_Pronto_PagoLabel = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label21 = New System.Windows.Forms.Label()
        Label24 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        CostoLabel = New System.Windows.Forms.Label()
        Clv_ServicioDigitalLabel = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel21.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.ConServiciosTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.Muestra_Dias_FinalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetyahve, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Dias_inicialBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServiciosDigitalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConServiciosTelBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConServiciosTelBindingNavigator.SuspendLayout()
        Me.Panel15.SuspendLayout()
        CType(Me.MuestraTipoClientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.Puntos3NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Puntos11NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Punto_Pronto_PagoNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Puntos6NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel18.SuspendLayout()
        CType(Me.CONRel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.CMBPanel4.SuspendLayout()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONSERVICIOSBindingNavigator.SuspendLayout()
        CType(Me.Borrar_Rel_ServiciosDigitales_Tel_TMPBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'PrecioLabel
        '
        PrecioLabel.AutoSize = True
        PrecioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PrecioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PrecioLabel.Location = New System.Drawing.Point(9, 11)
        PrecioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        PrecioLabel.Name = "PrecioLabel"
        PrecioLabel.Size = New System.Drawing.Size(62, 18)
        PrecioLabel.TabIndex = 300
        PrecioLabel.Text = "Precio:"
        '
        'Clv_ServicioLabel
        '
        Clv_ServicioLabel.AutoSize = True
        Clv_ServicioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_ServicioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_ServicioLabel.Location = New System.Drawing.Point(101, 43)
        Clv_ServicioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_ServicioLabel.Name = "Clv_ServicioLabel"
        Clv_ServicioLabel.Size = New System.Drawing.Size(103, 18)
        Clv_ServicioLabel.TabIndex = 409
        Clv_ServicioLabel.Text = "Clv Servicio:"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(60, 79)
        DescripcionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(108, 18)
        DescripcionLabel.TabIndex = 416
        DescripcionLabel.Text = "Descripción :"
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TxtLabel.Location = New System.Drawing.Point(115, 38)
        Clv_TxtLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(60, 18)
        Clv_TxtLabel.TabIndex = 419
        Clv_TxtLabel.Text = "Clave :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(640, 204)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(161, 18)
        Label1.TabIndex = 429
        Label1.Text = "Llamadas Incluidas :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(407, 76)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(112, 18)
        Label2.TabIndex = 430
        Label2.Text = "Mensualidad :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(32, 94)
        Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(150, 18)
        Label3.TabIndex = 433
        Label3.Text = "Minutos Incluidos :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(83, 43)
        Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(150, 18)
        Label4.TabIndex = 434
        Label4.Text = "Minutos Incluidos :"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(32, 148)
        Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(228, 18)
        Label5.TabIndex = 439
        Label5.Text = "Costo por Minuto  Adicional :"
        Label5.Visible = False
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(41, 170)
        Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(181, 18)
        Label7.TabIndex = 437
        Label7.Text = "Extensiones Incluidas :"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(869, 204)
        Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(235, 18)
        Label9.TabIndex = 438
        Label9.Text = "Costo por Llamada Adicional :"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(361, 47)
        Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(223, 18)
        Label10.TabIndex = 445
        Label10.Text = "Costo por Minuto Adicional :"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(224, 76)
        Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(115, 18)
        Label11.TabIndex = 631
        Label11.Text = "Contratación :"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(4, 2)
        Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(75, 18)
        Label8.TabIndex = 23
        Label8.Text = "Trabajo :"
        '
        'Puntos3Label
        '
        Puntos3Label.AutoSize = True
        Puntos3Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Puntos3Label.ForeColor = System.Drawing.Color.LightSlateGray
        Puntos3Label.Location = New System.Drawing.Point(68, 96)
        Puntos3Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Puntos3Label.Name = "Puntos3Label"
        Puntos3Label.Size = New System.Drawing.Size(163, 18)
        Puntos3Label.TabIndex = 629
        Puntos3Label.Text = "Puntos 3 a 5 Meses:"
        '
        'Puntos6Label
        '
        Puntos6Label.AutoSize = True
        Puntos6Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Puntos6Label.ForeColor = System.Drawing.Color.LightSlateGray
        Puntos6Label.Location = New System.Drawing.Point(52, 129)
        Puntos6Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Puntos6Label.Name = "Puntos6Label"
        Puntos6Label.Size = New System.Drawing.Size(177, 18)
        Puntos6Label.TabIndex = 630
        Puntos6Label.Text = "Puntos 6 a 11 Meses :"
        '
        'Puntos11Label
        '
        Puntos11Label.AutoSize = True
        Puntos11Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Puntos11Label.ForeColor = System.Drawing.Color.LightSlateGray
        Puntos11Label.Location = New System.Drawing.Point(32, 160)
        Puntos11Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Puntos11Label.Name = "Puntos11Label"
        Puntos11Label.Size = New System.Drawing.Size(196, 18)
        Puntos11Label.TabIndex = 631
        Puntos11Label.Text = "Puntos 12 o Mas Meses:"
        '
        'Punto_Pronto_PagoLabel
        '
        Punto_Pronto_PagoLabel.AutoSize = True
        Punto_Pronto_PagoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Punto_Pronto_PagoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Punto_Pronto_PagoLabel.Location = New System.Drawing.Point(75, 60)
        Punto_Pronto_PagoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Punto_Pronto_PagoLabel.Name = "Punto_Pronto_PagoLabel"
        Punto_Pronto_PagoLabel.Size = New System.Drawing.Size(157, 18)
        Punto_Pronto_PagoLabel.TabIndex = 632
        Punto_Pronto_PagoLabel.Text = "Punto Pronto Pago:"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(639, 94)
        Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(150, 18)
        Label16.TabIndex = 642
        Label16.Text = "Minutos Incluidos :"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Label17.Location = New System.Drawing.Point(639, 148)
        Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(223, 18)
        Label17.TabIndex = 645
        Label17.Text = "Costo por Minuto Adicional :"
        Label17.Visible = False
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Label18.Location = New System.Drawing.Point(45, 76)
        Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(99, 18)
        Label18.TabIndex = 647
        Label18.Text = "Instalación :"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.LightSlateGray
        Label19.Location = New System.Drawing.Point(287, 170)
        Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(239, 18)
        Label19.TabIndex = 649
        Label19.Text = "Costo de Extensión Adicional :"
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Label20.Location = New System.Drawing.Point(860, 69)
        Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(235, 18)
        Label20.TabIndex = 651
        Label20.Text = "Costo por Llamada Adicional :"
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label21.ForeColor = System.Drawing.Color.LightSlateGray
        Label21.Location = New System.Drawing.Point(640, 69)
        Label21.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(161, 18)
        Label21.TabIndex = 654
        Label21.Text = "Llamadas Incluidas :"
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.ForeColor = System.Drawing.Color.LightSlateGray
        Label24.Location = New System.Drawing.Point(36, 127)
        Label24.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(105, 18)
        Label24.TabIndex = 656
        Label24.Text = "Inalámbrico :"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.ForeColor = System.Drawing.Color.LightSlateGray
        Label25.Location = New System.Drawing.Point(376, 916)
        Label25.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(93, 18)
        Label25.TabIndex = 658
        Label25.Text = "Alámbrico :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Label12.Location = New System.Drawing.Point(28, 23)
        Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(131, 18)
        Label12.TabIndex = 630
        Label12.Text = "Servicio Digital :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(576, 26)
        Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(64, 18)
        Label14.TabIndex = 631
        Label14.Text = "Costo :"
        '
        'CostoLabel
        '
        CostoLabel.AutoSize = True
        CostoLabel.Location = New System.Drawing.Point(19, 117)
        CostoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CostoLabel.Name = "CostoLabel"
        CostoLabel.Size = New System.Drawing.Size(48, 17)
        CostoLabel.TabIndex = 646
        CostoLabel.Text = "Costo:"
        '
        'Clv_ServicioDigitalLabel
        '
        Clv_ServicioDigitalLabel.AutoSize = True
        Clv_ServicioDigitalLabel.Location = New System.Drawing.Point(19, 98)
        Clv_ServicioDigitalLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_ServicioDigitalLabel.Name = "Clv_ServicioDigitalLabel"
        Clv_ServicioDigitalLabel.Size = New System.Drawing.Size(128, 17)
        Clv_ServicioDigitalLabel.TabIndex = 642
        Clv_ServicioDigitalLabel.Text = "Clv Servicio Digital:"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(303, 233)
        Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(91, 18)
        Label15.TabIndex = 668
        Label15.Text = "Día Inicial :"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label22.ForeColor = System.Drawing.Color.LightSlateGray
        Label22.Location = New System.Drawing.Point(597, 233)
        Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(84, 18)
        Label22.TabIndex = 669
        Label22.Text = "Día Final :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(4, 114)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(155, 18)
        Me.Label6.TabIndex = 430
        Me.Label6.Text = "Número de Lineas :"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.NumericUpDown5)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.TabControl1)
        Me.Panel1.Controls.Add(Me.CMBSale_en_CarteraLabel)
        Me.Panel1.Controls.Add(Clv_TxtLabel)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Panel1.Controls.Add(Me.Clv_TxtTextBox)
        Me.Panel1.Controls.Add(Me.Es_PrincipalCheckBox)
        Me.Panel1.Controls.Add(Me.Sale_en_CarteraCheckBox1)
        Me.Panel1.Controls.Add(Me.CMBTextBox28)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.CMBEs_PrincipalLabel)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.CMBPanel4)
        Me.Panel1.Location = New System.Drawing.Point(31, 39)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1308, 895)
        Me.Panel1.TabIndex = 0
        '
        'NumericUpDown5
        '
        Me.NumericUpDown5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown5.Location = New System.Drawing.Point(184, 114)
        Me.NumericUpDown5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown5.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown5.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown5.Name = "NumericUpDown5"
        Me.NumericUpDown5.Size = New System.Drawing.Size(77, 23)
        Me.NumericUpDown5.TabIndex = 431
        Me.NumericUpDown5.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(27, 199)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1252, 587)
        Me.TabControl1.TabIndex = 429
        '
        'TabPage1
        '
        Me.TabPage1.AutoScroll = True
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.Panel9)
        Me.TabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage1.Location = New System.Drawing.Point(4, 34)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage1.Size = New System.Drawing.Size(1244, 549)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Detalle del Servicio"
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.Panel21)
        Me.Panel9.Controls.Add(Me.TabControl2)
        Me.Panel9.Controls.Add(Me.ConServiciosTelBindingNavigator)
        Me.Panel9.Controls.Add(Me.Clv_TipoClienteTextBox)
        Me.Panel9.Controls.Add(Me.Panel15)
        Me.Panel9.Controls.Add(Me.TextBox5)
        Me.Panel9.Location = New System.Drawing.Point(7, 11)
        Me.Panel9.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(1215, 528)
        Me.Panel9.TabIndex = 0
        '
        'Panel21
        '
        Me.Panel21.Controls.Add(Me.CheckBox6)
        Me.Panel21.Enabled = False
        Me.Panel21.Location = New System.Drawing.Point(19, 90)
        Me.Panel21.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(357, 31)
        Me.Panel21.TabIndex = 639
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox6.ForeColor = System.Drawing.Color.Red
        Me.CheckBox6.Location = New System.Drawing.Point(8, 6)
        Me.CheckBox6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(286, 21)
        Me.CheckBox6.TabIndex = 622
        Me.CheckBox6.Text = "Aplicar a Todos los Tipos de Cobro"
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage3)
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Controls.Add(Me.TabPage5)
        Me.TabControl2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl2.Location = New System.Drawing.Point(19, 128)
        Me.TabControl2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(1163, 361)
        Me.TabControl2.TabIndex = 638
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage3.Controls.Add(Me.CheckBox2)
        Me.TabPage3.Controls.Add(Me.CheckBox1)
        Me.TabPage3.Controls.Add(Me.TextBox13)
        Me.TabPage3.Controls.Add(Label20)
        Me.TabPage3.Controls.Add(Me.CMBTextBox7)
        Me.TabPage3.Controls.Add(Me.NumericUpDown8)
        Me.TabPage3.Controls.Add(Label7)
        Me.TabPage3.Controls.Add(Me.TextBox14)
        Me.TabPage3.Controls.Add(Label11)
        Me.TabPage3.Controls.Add(Label21)
        Me.TabPage3.Controls.Add(Me.NumericUpDown2)
        Me.TabPage3.Controls.Add(Me.TextBox3)
        Me.TabPage3.Controls.Add(Me.TextBox8)
        Me.TabPage3.Controls.Add(Label2)
        Me.TabPage3.Controls.Add(Me.TextBox11)
        Me.TabPage3.Controls.Add(Label18)
        Me.TabPage3.Controls.Add(Me.TextBox12)
        Me.TabPage3.Controls.Add(Label19)
        Me.TabPage3.Controls.Add(Me.CMBTextBox4)
        Me.TabPage3.Controls.Add(Label1)
        Me.TabPage3.Controls.Add(Label9)
        Me.TabPage3.Controls.Add(Me.NumericUpDown1)
        Me.TabPage3.Controls.Add(Me.TextBox1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 29)
        Me.TabPage3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage3.Size = New System.Drawing.Size(1155, 328)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "Costos"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.ConServiciosTelBindingSource, "Ilimitadas_Frac", True))
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBox2.Location = New System.Drawing.Point(732, 106)
        Me.CheckBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(118, 22)
        Me.CheckBox2.TabIndex = 662
        Me.CheckBox2.Text = "ILIMITADAS"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'ConServiciosTelBindingSource
        '
        Me.ConServiciosTelBindingSource.DataMember = "ConServiciosTel"
        Me.ConServiciosTelBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.ConServiciosTelBindingSource, "Ilimitadas_Locales", True))
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBox1.Location = New System.Drawing.Point(732, 240)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(118, 22)
        Me.CheckBox1.TabIndex = 661
        Me.CheckBox1.Text = "ILIMITADAS"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'TextBox13
        '
        Me.TextBox13.BackColor = System.Drawing.Color.DarkOrange
        Me.TextBox13.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox13.ForeColor = System.Drawing.Color.White
        Me.TextBox13.Location = New System.Drawing.Point(596, 31)
        Me.TextBox13.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.ReadOnly = True
        Me.TextBox13.Size = New System.Drawing.Size(513, 19)
        Me.TextBox13.TabIndex = 653
        Me.TextBox13.TabStop = False
        Me.TextBox13.Text = "Llamadas a Mismo Fraccionamiento"
        Me.TextBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CMBTextBox7
        '
        Me.CMBTextBox7.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox7.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox7.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox7.Location = New System.Drawing.Point(41, 31)
        Me.CMBTextBox7.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTextBox7.Name = "CMBTextBox7"
        Me.CMBTextBox7.ReadOnly = True
        Me.CMBTextBox7.Size = New System.Drawing.Size(513, 19)
        Me.CMBTextBox7.TabIndex = 629
        Me.CMBTextBox7.TabStop = False
        Me.CMBTextBox7.Text = "Costos"
        Me.CMBTextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'NumericUpDown8
        '
        Me.NumericUpDown8.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConServiciosTelBindingSource, "Minutos_Fraccionamiento", True))
        Me.NumericUpDown8.Location = New System.Drawing.Point(624, 105)
        Me.NumericUpDown8.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown8.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.NumericUpDown8.Name = "NumericUpDown8"
        Me.NumericUpDown8.Size = New System.Drawing.Size(84, 26)
        Me.NumericUpDown8.TabIndex = 655
        '
        'TextBox14
        '
        Me.TextBox14.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox14.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Costo_fraccionamiento", True))
        Me.TextBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox14.Location = New System.Drawing.Point(915, 107)
        Me.TextBox14.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.ReadOnly = True
        Me.TextBox14.Size = New System.Drawing.Size(138, 23)
        Me.TextBox14.TabIndex = 652
        Me.TextBox14.TabStop = False
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConServiciosTelBindingSource, "Extensiones_SinCosto", True))
        Me.NumericUpDown2.Location = New System.Drawing.Point(88, 202)
        Me.NumericUpDown2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(84, 26)
        Me.NumericUpDown2.TabIndex = 442
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Mensualidad", True))
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox3.Location = New System.Drawing.Point(416, 113)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(138, 23)
        Me.TextBox3.TabIndex = 435
        Me.TextBox3.TabStop = False
        '
        'TextBox8
        '
        Me.TextBox8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox8.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Precio_Contratacion", True))
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox8.Location = New System.Drawing.Point(239, 113)
        Me.TextBox8.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(138, 23)
        Me.TextBox8.TabIndex = 630
        Me.TextBox8.TabStop = False
        '
        'TextBox11
        '
        Me.TextBox11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Costo_Instalacion", True))
        Me.TextBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox11.Location = New System.Drawing.Point(60, 113)
        Me.TextBox11.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.ReadOnly = True
        Me.TextBox11.Size = New System.Drawing.Size(138, 23)
        Me.TextBox11.TabIndex = 648
        Me.TextBox11.TabStop = False
        '
        'TextBox12
        '
        Me.TextBox12.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox12.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Costo_Extension_Adic", True))
        Me.TextBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox12.Location = New System.Drawing.Point(349, 204)
        Me.TextBox12.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.ReadOnly = True
        Me.TextBox12.Size = New System.Drawing.Size(138, 23)
        Me.TextBox12.TabIndex = 650
        Me.TextBox12.TabStop = False
        '
        'CMBTextBox4
        '
        Me.CMBTextBox4.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox4.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox4.Location = New System.Drawing.Point(596, 170)
        Me.CMBTextBox4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTextBox4.Name = "CMBTextBox4"
        Me.CMBTextBox4.ReadOnly = True
        Me.CMBTextBox4.Size = New System.Drawing.Size(513, 19)
        Me.CMBTextBox4.TabIndex = 626
        Me.CMBTextBox4.TabStop = False
        Me.CMBTextBox4.Text = "Llamadas Locales"
        Me.CMBTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConServiciosTelBindingSource, "Llamadas_Locales", True))
        Me.NumericUpDown1.Location = New System.Drawing.Point(624, 239)
        Me.NumericUpDown1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(84, 26)
        Me.NumericUpDown1.TabIndex = 441
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Costo_Lamada_Adic", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox1.Location = New System.Drawing.Point(915, 241)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(138, 23)
        Me.TextBox1.TabIndex = 431
        Me.TextBox1.TabStop = False
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage4.Controls.Add(Label22)
        Me.TabPage4.Controls.Add(Label15)
        Me.TabPage4.Controls.Add(Me.ComboBox3)
        Me.TabPage4.Controls.Add(Me.ComboBox2)
        Me.TabPage4.Controls.Add(Me.CMBTextBoxDias)
        Me.TabPage4.Controls.Add(Me.CheckBox3)
        Me.TabPage4.Controls.Add(Me.CheckBox4)
        Me.TabPage4.Controls.Add(Me.CMBTextBox5)
        Me.TabPage4.Controls.Add(Me.TextBox2)
        Me.TabPage4.Controls.Add(Me.NumericUpDown3)
        Me.TabPage4.Controls.Add(Label3)
        Me.TabPage4.Controls.Add(Label5)
        Me.TabPage4.Controls.Add(Me.NumericUpDown7)
        Me.TabPage4.Controls.Add(Me.TextBox7)
        Me.TabPage4.Controls.Add(Label16)
        Me.TabPage4.Controls.Add(Me.TextBox6)
        Me.TabPage4.Controls.Add(Label17)
        Me.TabPage4.Location = New System.Drawing.Point(4, 29)
        Me.TabPage4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage4.Size = New System.Drawing.Size(1155, 328)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "Larga Distancia"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'ComboBox3
        '
        Me.ComboBox3.DataSource = Me.Muestra_Dias_FinalBindingSource
        Me.ComboBox3.DisplayMember = "Dia"
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(703, 229)
        Me.ComboBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(160, 28)
        Me.ComboBox3.TabIndex = 667
        Me.ComboBox3.ValueMember = "Clv_Dia"
        '
        'Muestra_Dias_FinalBindingSource
        '
        Me.Muestra_Dias_FinalBindingSource.DataMember = "Muestra_Dias_Final"
        Me.Muestra_Dias_FinalBindingSource.DataSource = Me.DataSetyahve
        '
        'DataSetyahve
        '
        Me.DataSetyahve.DataSetName = "DataSetyahve"
        Me.DataSetyahve.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.Muestra_Dias_inicialBindingSource
        Me.ComboBox2.DisplayMember = "Dia"
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(416, 229)
        Me.ComboBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(160, 28)
        Me.ComboBox2.TabIndex = 666
        Me.ComboBox2.ValueMember = "Clv_Dia"
        '
        'Muestra_Dias_inicialBindingSource
        '
        Me.Muestra_Dias_inicialBindingSource.DataMember = "Muestra_Dias_inicial"
        Me.Muestra_Dias_inicialBindingSource.DataSource = Me.DataSetyahve
        '
        'CMBTextBoxDias
        '
        Me.CMBTextBoxDias.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBoxDias.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBoxDias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBoxDias.ForeColor = System.Drawing.Color.White
        Me.CMBTextBoxDias.Location = New System.Drawing.Point(284, 188)
        Me.CMBTextBoxDias.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTextBoxDias.Name = "CMBTextBoxDias"
        Me.CMBTextBoxDias.ReadOnly = True
        Me.CMBTextBoxDias.Size = New System.Drawing.Size(599, 19)
        Me.CMBTextBoxDias.TabIndex = 665
        Me.CMBTextBoxDias.TabStop = False
        Me.CMBTextBoxDias.Text = "Dias en los que Aplica"
        Me.CMBTextBoxDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Checked = True
        Me.CheckBox3.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox3.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.ConServiciosTelBindingSource, "Ilimitadas_LDN", True))
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBox3.Location = New System.Drawing.Point(347, 92)
        Me.CheckBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(118, 22)
        Me.CheckBox3.TabIndex = 664
        Me.CheckBox3.Text = "ILIMITADAS"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.Checked = True
        Me.CheckBox4.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox4.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.ConServiciosTelBindingSource, "Ilimitadas_EuaCan", True))
        Me.CheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBox4.Location = New System.Drawing.Point(949, 92)
        Me.CheckBox4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(118, 22)
        Me.CheckBox4.TabIndex = 663
        Me.CheckBox4.Text = "ILIMITADAS"
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'CMBTextBox5
        '
        Me.CMBTextBox5.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox5.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox5.Location = New System.Drawing.Point(28, 52)
        Me.CMBTextBox5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTextBox5.Name = "CMBTextBox5"
        Me.CMBTextBox5.ReadOnly = True
        Me.CMBTextBox5.Size = New System.Drawing.Size(513, 19)
        Me.CMBTextBox5.TabIndex = 627
        Me.CMBTextBox5.TabStop = False
        Me.CMBTextBox5.Text = "Llamadas De Larga Distancia Nacional"
        Me.CMBTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Costo_Min_LDN_Adic", True))
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox2.Location = New System.Drawing.Point(295, 148)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(138, 23)
        Me.TextBox2.TabIndex = 432
        Me.TextBox2.TabStop = False
        Me.TextBox2.Visible = False
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConServiciosTelBindingSource, "Minutos_LDN", True))
        Me.NumericUpDown3.Location = New System.Drawing.Point(201, 91)
        Me.NumericUpDown3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown3.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(84, 26)
        Me.NumericUpDown3.TabIndex = 443
        '
        'NumericUpDown7
        '
        Me.NumericUpDown7.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConServiciosTelBindingSource, "Minutos_EUA_Can", True))
        Me.NumericUpDown7.Location = New System.Drawing.Point(811, 91)
        Me.NumericUpDown7.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown7.Maximum = New Decimal(New Integer() {100000000, 0, 0, 0})
        Me.NumericUpDown7.Name = "NumericUpDown7"
        Me.NumericUpDown7.Size = New System.Drawing.Size(84, 26)
        Me.NumericUpDown7.TabIndex = 644
        '
        'TextBox7
        '
        Me.TextBox7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Costo_Min_EUA_Can", True))
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox7.Location = New System.Drawing.Point(896, 148)
        Me.TextBox7.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(138, 23)
        Me.TextBox7.TabIndex = 643
        Me.TextBox7.TabStop = False
        Me.TextBox7.Visible = False
        '
        'TextBox6
        '
        Me.TextBox6.BackColor = System.Drawing.Color.DarkOrange
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox6.ForeColor = System.Drawing.Color.White
        Me.TextBox6.Location = New System.Drawing.Point(609, 52)
        Me.TextBox6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(513, 19)
        Me.TextBox6.TabIndex = 646
        Me.TextBox6.TabStop = False
        Me.TextBox6.Text = "Llamadas a Estados Unidos Y Cánada"
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TabPage5
        '
        Me.TabPage5.AutoScroll = True
        Me.TabPage5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage5.Controls.Add(Me.Panel7)
        Me.TabPage5.Controls.Add(Me.TxtBoxCosto)
        Me.TabPage5.Controls.Add(Me.Button2)
        Me.TabPage5.Controls.Add(Me.Button1)
        Me.TabPage5.Controls.Add(Label14)
        Me.TabPage5.Controls.Add(Label12)
        Me.TabPage5.Controls.Add(Me.CMBTextBox9)
        Me.TabPage5.Controls.Add(Me.ComboBox1)
        Me.TabPage5.Location = New System.Drawing.Point(4, 29)
        Me.TabPage5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1155, 328)
        Me.TabPage5.TabIndex = 2
        Me.TabPage5.Text = "Servicios Digitales"
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView)
        Me.Panel7.Location = New System.Drawing.Point(171, 117)
        Me.Panel7.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(615, 165)
        Me.Panel7.TabIndex = 635
        '
        'Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView
        '
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.AllowUserToAddRows = False
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.AllowUserToDeleteRows = False
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.AutoGenerateColumns = False
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7})
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.DataSource = Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.Location = New System.Drawing.Point(-177, 4)
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.Name = "Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView"
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.ReadOnly = True
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.Size = New System.Drawing.Size(792, 158)
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.TabIndex = 634
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clave_Id"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clave_Id"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_Session"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_Session"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Clv_Tipocobro"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Clv_Tipocobro"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Clv_ServicioDigital"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Clv_ServicioDigital"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Nombre"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 300
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Costo"
        DataGridViewCellStyle1.Format = "C2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewTextBoxColumn7.HeaderText = "Costo"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 150
        '
        'Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource
        '
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource.DataMember = "Consultar_Rel_ServiciosDigitales_Tel_TMP"
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource.DataSource = Me.DataSetyahve
        '
        'TxtBoxCosto
        '
        Me.TxtBoxCosto.Location = New System.Drawing.Point(652, 22)
        Me.TxtBoxCosto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtBoxCosto.Name = "TxtBoxCosto"
        Me.TxtBoxCosto.ReadOnly = True
        Me.TxtBoxCosto.Size = New System.Drawing.Size(132, 26)
        Me.TxtBoxCosto.TabIndex = 634
        '
        'Button2
        '
        Me.Button2.Enabled = False
        Me.Button2.Location = New System.Drawing.Point(812, 59)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(100, 31)
        Me.Button2.TabIndex = 633
        Me.Button2.Text = "&Quitar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Location = New System.Drawing.Point(811, 20)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 32)
        Me.Button1.TabIndex = 632
        Me.Button1.Text = "&Agregar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CMBTextBox9
        '
        Me.CMBTextBox9.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox9.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox9.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox9.Location = New System.Drawing.Point(171, 95)
        Me.CMBTextBox9.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTextBox9.Name = "CMBTextBox9"
        Me.CMBTextBox9.ReadOnly = True
        Me.CMBTextBox9.Size = New System.Drawing.Size(615, 19)
        Me.CMBTextBox9.TabIndex = 628
        Me.CMBTextBox9.TabStop = False
        Me.CMBTextBox9.Text = "Servicios Digitales"
        Me.CMBTextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MuestraServiciosDigitalesBindingSource
        Me.ComboBox1.DisplayMember = "Nombre"
        Me.ComboBox1.Enabled = False
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(185, 20)
        Me.ComboBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(375, 28)
        Me.ComboBox1.TabIndex = 0
        Me.ComboBox1.ValueMember = "Clv_ServicioDigital"
        '
        'MuestraServiciosDigitalesBindingSource
        '
        Me.MuestraServiciosDigitalesBindingSource.DataMember = "Muestra_ServiciosDigitales"
        Me.MuestraServiciosDigitalesBindingSource.DataSource = Me.DataSetyahve
        '
        'ConServiciosTelBindingNavigator
        '
        Me.ConServiciosTelBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.ConServiciosTelBindingNavigator.BindingSource = Me.ConServiciosTelBindingSource
        Me.ConServiciosTelBindingNavigator.CountItem = Nothing
        Me.ConServiciosTelBindingNavigator.DeleteItem = Me.ToolStripButton2
        Me.ConServiciosTelBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConServiciosTelBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ConServiciosTelBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton2, Me.ToolStripButton3, Me.ConServiciosTelBindingNavigatorSaveItem, Me.ToolStripButton4, Me.BindingNavigatorAddNewItem})
        Me.ConServiciosTelBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConServiciosTelBindingNavigator.MoveFirstItem = Nothing
        Me.ConServiciosTelBindingNavigator.MoveLastItem = Nothing
        Me.ConServiciosTelBindingNavigator.MoveNextItem = Nothing
        Me.ConServiciosTelBindingNavigator.MovePreviousItem = Nothing
        Me.ConServiciosTelBindingNavigator.Name = "ConServiciosTelBindingNavigator"
        Me.ConServiciosTelBindingNavigator.PositionItem = Nothing
        Me.ConServiciosTelBindingNavigator.Size = New System.Drawing.Size(1215, 28)
        Me.ConServiciosTelBindingNavigator.TabIndex = 637
        Me.ConServiciosTelBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(160, 25)
        Me.BindingNavigatorAddNewItem.Text = "Agregar nuevo"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton2.Size = New System.Drawing.Size(103, 25)
        Me.ToolStripButton2.Text = "Eliminar"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton3.Enabled = False
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.WhiteSmoke
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(87, 25)
        Me.ToolStripButton3.Text = "Cancelar"
        '
        'ConServiciosTelBindingNavigatorSaveItem
        '
        Me.ConServiciosTelBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ConServiciosTelBindingNavigatorSaveItem.Enabled = False
        Me.ConServiciosTelBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConServiciosTelBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConServiciosTelBindingNavigatorSaveItem.Name = "ConServiciosTelBindingNavigatorSaveItem"
        Me.ConServiciosTelBindingNavigatorSaveItem.Size = New System.Drawing.Size(156, 25)
        Me.ConServiciosTelBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.WhiteSmoke
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(94, 25)
        Me.ToolStripButton4.Text = "Modificar"
        '
        'Clv_TipoClienteTextBox
        '
        Me.Clv_TipoClienteTextBox.BackColor = System.Drawing.Color.Gainsboro
        Me.Clv_TipoClienteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipoClienteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Clv_tipocliente", True))
        Me.Clv_TipoClienteTextBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Clv_TipoClienteTextBox.Location = New System.Drawing.Point(40, 4)
        Me.Clv_TipoClienteTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TipoClienteTextBox.Name = "Clv_TipoClienteTextBox"
        Me.Clv_TipoClienteTextBox.Size = New System.Drawing.Size(133, 17)
        Me.Clv_TipoClienteTextBox.TabIndex = 621
        '
        'Panel15
        '
        Me.Panel15.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel15.Controls.Add(Me.ComboBox4)
        Me.Panel15.Controls.Add(Me.Label13)
        Me.Panel15.Location = New System.Drawing.Point(17, 33)
        Me.Panel15.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(568, 49)
        Me.Panel15.TabIndex = 9
        Me.Panel15.TabStop = True
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.MuestraTipoClientesBindingSource
        Me.ComboBox4.DisplayMember = "DESCRIPCION"
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.ForeColor = System.Drawing.Color.Blue
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(128, 9)
        Me.ComboBox4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(403, 32)
        Me.ComboBox4.TabIndex = 8
        Me.ComboBox4.ValueMember = "CLV_TIPOCLIENTE"
        '
        'MuestraTipoClientesBindingSource
        '
        Me.MuestraTipoClientesBindingSource.DataMember = "MuestraTipoClientes"
        Me.MuestraTipoClientesBindingSource.DataSource = Me.DataSetLidia2
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Navy
        Me.Label13.Location = New System.Drawing.Point(7, 14)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(103, 18)
        Me.Label13.TabIndex = 305
        Me.Label13.Text = "Tipo Cobro :"
        '
        'TextBox5
        '
        Me.TextBox5.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Clv_servicio", True))
        Me.TextBox5.Location = New System.Drawing.Point(308, 0)
        Me.TextBox5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(84, 24)
        Me.TextBox5.TabIndex = 430
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage2.Controls.Add(Me.Panel5)
        Me.TabPage2.Location = New System.Drawing.Point(4, 34)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage2.Size = New System.Drawing.Size(1244, 549)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Puntos de Descuento"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.CMBTextBox3)
        Me.Panel5.Controls.Add(Puntos3Label)
        Me.Panel5.Controls.Add(Me.Puntos3NumericUpDown)
        Me.Panel5.Controls.Add(Punto_Pronto_PagoLabel)
        Me.Panel5.Controls.Add(Me.Puntos11NumericUpDown)
        Me.Panel5.Controls.Add(Puntos11Label)
        Me.Panel5.Controls.Add(Me.Punto_Pronto_PagoNumericUpDown)
        Me.Panel5.Controls.Add(Puntos6Label)
        Me.Panel5.Controls.Add(Me.Puntos6NumericUpDown)
        Me.Panel5.Location = New System.Drawing.Point(4, 4)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1211, 494)
        Me.Panel5.TabIndex = 0
        '
        'CMBTextBox3
        '
        Me.CMBTextBox3.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox3.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox3.Location = New System.Drawing.Point(23, 21)
        Me.CMBTextBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTextBox3.Multiline = True
        Me.CMBTextBox3.Name = "CMBTextBox3"
        Me.CMBTextBox3.ReadOnly = True
        Me.CMBTextBox3.Size = New System.Drawing.Size(373, 32)
        Me.CMBTextBox3.TabIndex = 633
        Me.CMBTextBox3.TabStop = False
        Me.CMBTextBox3.Text = "Descuento por Pronto Pago y Pago Adelantado :"
        Me.CMBTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Puntos3NumericUpDown
        '
        Me.Puntos3NumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Puntos3NumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Puntos3NumericUpDown.Location = New System.Drawing.Point(259, 94)
        Me.Puntos3NumericUpDown.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Puntos3NumericUpDown.Name = "Puntos3NumericUpDown"
        Me.Puntos3NumericUpDown.Size = New System.Drawing.Size(76, 24)
        Me.Puntos3NumericUpDown.TabIndex = 626
        '
        'Puntos11NumericUpDown
        '
        Me.Puntos11NumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Puntos11NumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Puntos11NumericUpDown.Location = New System.Drawing.Point(259, 160)
        Me.Puntos11NumericUpDown.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Puntos11NumericUpDown.Name = "Puntos11NumericUpDown"
        Me.Puntos11NumericUpDown.Size = New System.Drawing.Size(76, 24)
        Me.Puntos11NumericUpDown.TabIndex = 628
        '
        'Punto_Pronto_PagoNumericUpDown
        '
        Me.Punto_Pronto_PagoNumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Punto_Pronto_PagoNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Punto_Pronto_PagoNumericUpDown.Location = New System.Drawing.Point(259, 60)
        Me.Punto_Pronto_PagoNumericUpDown.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Punto_Pronto_PagoNumericUpDown.Name = "Punto_Pronto_PagoNumericUpDown"
        Me.Punto_Pronto_PagoNumericUpDown.Size = New System.Drawing.Size(76, 24)
        Me.Punto_Pronto_PagoNumericUpDown.TabIndex = 625
        '
        'Puntos6NumericUpDown
        '
        Me.Puntos6NumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Puntos6NumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Puntos6NumericUpDown.Location = New System.Drawing.Point(259, 127)
        Me.Puntos6NumericUpDown.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Puntos6NumericUpDown.Name = "Puntos6NumericUpDown"
        Me.Puntos6NumericUpDown.Size = New System.Drawing.Size(76, 24)
        Me.Puntos6NumericUpDown.TabIndex = 627
        '
        'CMBSale_en_CarteraLabel
        '
        Me.CMBSale_en_CarteraLabel.AutoSize = True
        Me.CMBSale_en_CarteraLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBSale_en_CarteraLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBSale_en_CarteraLabel.Location = New System.Drawing.Point(1024, 38)
        Me.CMBSale_en_CarteraLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBSale_en_CarteraLabel.Name = "CMBSale_en_CarteraLabel"
        Me.CMBSale_en_CarteraLabel.Size = New System.Drawing.Size(178, 25)
        Me.CMBSale_en_CarteraLabel.TabIndex = 421
        Me.CMBSale_en_CarteraLabel.Text = "Cobro Mensual  :"
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BackColor = System.Drawing.Color.White
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(184, 76)
        Me.DescripcionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DescripcionTextBox.MaxLength = 150
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(598, 24)
        Me.DescripcionTextBox.TabIndex = 411
        '
        'CONSERVICIOSBindingSource
        '
        Me.CONSERVICIOSBindingSource.DataMember = "CONSERVICIOS"
        Me.CONSERVICIOSBindingSource.DataSource = Me.DataSetLidia2
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipSerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipSerTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TipSerTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(536, 85)
        Me.Clv_TipSerTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(139, 17)
        Me.Clv_TipSerTextBox.TabIndex = 424
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'Clv_TxtTextBox
        '
        Me.Clv_TxtTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_TxtTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_TxtTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_TxtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Clv_Txt", True))
        Me.Clv_TxtTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtTextBox.Location = New System.Drawing.Point(184, 34)
        Me.Clv_TxtTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TxtTextBox.MaxLength = 5
        Me.Clv_TxtTextBox.Name = "Clv_TxtTextBox"
        Me.Clv_TxtTextBox.Size = New System.Drawing.Size(133, 24)
        Me.Clv_TxtTextBox.TabIndex = 410
        '
        'Es_PrincipalCheckBox
        '
        Me.Es_PrincipalCheckBox.Checked = True
        Me.Es_PrincipalCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Es_PrincipalCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSERVICIOSBindingSource, "Es_Principal", True))
        Me.Es_PrincipalCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Es_PrincipalCheckBox.Location = New System.Drawing.Point(1241, 74)
        Me.Es_PrincipalCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Es_PrincipalCheckBox.Name = "Es_PrincipalCheckBox"
        Me.Es_PrincipalCheckBox.Size = New System.Drawing.Size(33, 21)
        Me.Es_PrincipalCheckBox.TabIndex = 417
        '
        'Sale_en_CarteraCheckBox1
        '
        Me.Sale_en_CarteraCheckBox1.Checked = True
        Me.Sale_en_CarteraCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Sale_en_CarteraCheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.CONSERVICIOSBindingSource, "Sale_en_Cartera", True))
        Me.Sale_en_CarteraCheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Sale_en_CarteraCheckBox1.Location = New System.Drawing.Point(1241, 37)
        Me.Sale_en_CarteraCheckBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Sale_en_CarteraCheckBox1.Name = "Sale_en_CarteraCheckBox1"
        Me.Sale_en_CarteraCheckBox1.Size = New System.Drawing.Size(31, 30)
        Me.Sale_en_CarteraCheckBox1.TabIndex = 414
        '
        'CMBTextBox28
        '
        Me.CMBTextBox28.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox28.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox28.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox28.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox28.Location = New System.Drawing.Point(951, 6)
        Me.CMBTextBox28.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTextBox28.Name = "CMBTextBox28"
        Me.CMBTextBox28.ReadOnly = True
        Me.CMBTextBox28.Size = New System.Drawing.Size(331, 23)
        Me.CMBTextBox28.TabIndex = 427
        Me.CMBTextBox28.TabStop = False
        Me.CMBTextBox28.Text = "Es Servicio :"
        Me.CMBTextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel18)
        Me.Panel3.Controls.Add(Me.CMBGenera_OrdenLabel)
        Me.Panel3.Controls.Add(Me.CMBAplicanComLabel)
        Me.Panel3.Controls.Add(Me.Sale_en_CarteraCheckBox)
        Me.Panel3.Controls.Add(Me.Genera_OrdenCheckBox)
        Me.Panel3.Location = New System.Drawing.Point(536, 114)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(747, 65)
        Me.Panel3.TabIndex = 418
        Me.Panel3.TabStop = True
        '
        'Panel18
        '
        Me.Panel18.Controls.Add(Label8)
        Me.Panel18.Controls.Add(Me.ComboBox5)
        Me.Panel18.Location = New System.Drawing.Point(7, 34)
        Me.Panel18.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(508, 27)
        Me.Panel18.TabIndex = 303
        Me.Panel18.Visible = False
        '
        'ComboBox5
        '
        Me.ComboBox5.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONRel_Trabajos_NoCobroMensualBindingSource, "Clv_Trabajo", True))
        Me.ComboBox5.DataSource = Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource
        Me.ComboBox5.DisplayMember = "Descripcion"
        Me.ComboBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(97, 1)
        Me.ComboBox5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(409, 26)
        Me.ComboBox5.TabIndex = 7
        Me.ComboBox5.ValueMember = "Clv_Trabajo"
        '
        'CONRel_Trabajos_NoCobroMensualBindingSource
        '
        Me.CONRel_Trabajos_NoCobroMensualBindingSource.DataMember = "CONRel_Trabajos_NoCobroMensual"
        Me.CONRel_Trabajos_NoCobroMensualBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRATRABAJOS_NOCOBROMENSUALBindingSource
        '
        Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource.DataMember = "MUESTRATRABAJOS_NOCOBROMENSUAL"
        Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource.DataSource = Me.DataSetEDGAR
        '
        'CMBGenera_OrdenLabel
        '
        Me.CMBGenera_OrdenLabel.AutoSize = True
        Me.CMBGenera_OrdenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBGenera_OrdenLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBGenera_OrdenLabel.Location = New System.Drawing.Point(529, 32)
        Me.CMBGenera_OrdenLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBGenera_OrdenLabel.Name = "CMBGenera_OrdenLabel"
        Me.CMBGenera_OrdenLabel.Size = New System.Drawing.Size(162, 25)
        Me.CMBGenera_OrdenLabel.TabIndex = 301
        Me.CMBGenera_OrdenLabel.Text = "Genera Orden :"
        '
        'CMBAplicanComLabel
        '
        Me.CMBAplicanComLabel.AutoSize = True
        Me.CMBAplicanComLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBAplicanComLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBAplicanComLabel.Location = New System.Drawing.Point(499, 6)
        Me.CMBAplicanComLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBAplicanComLabel.Name = "CMBAplicanComLabel"
        Me.CMBAplicanComLabel.Size = New System.Drawing.Size(193, 25)
        Me.CMBAplicanComLabel.TabIndex = 8
        Me.CMBAplicanComLabel.Text = "Aplican Comisión :"
        '
        'Sale_en_CarteraCheckBox
        '
        Me.Sale_en_CarteraCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSERVICIOSBindingSource, "AplicanCom", True))
        Me.Sale_en_CarteraCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Sale_en_CarteraCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Sale_en_CarteraCheckBox.Location = New System.Drawing.Point(715, 6)
        Me.Sale_en_CarteraCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Sale_en_CarteraCheckBox.Name = "Sale_en_CarteraCheckBox"
        Me.Sale_en_CarteraCheckBox.Size = New System.Drawing.Size(28, 30)
        Me.Sale_en_CarteraCheckBox.TabIndex = 5
        '
        'Genera_OrdenCheckBox
        '
        Me.Genera_OrdenCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSERVICIOSBindingSource, "Genera_Orden", True))
        Me.Genera_OrdenCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Genera_OrdenCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Genera_OrdenCheckBox.Location = New System.Drawing.Point(715, 31)
        Me.Genera_OrdenCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Genera_OrdenCheckBox.Name = "Genera_OrdenCheckBox"
        Me.Genera_OrdenCheckBox.Size = New System.Drawing.Size(28, 30)
        Me.Genera_OrdenCheckBox.TabIndex = 6
        '
        'CMBEs_PrincipalLabel
        '
        Me.CMBEs_PrincipalLabel.AutoSize = True
        Me.CMBEs_PrincipalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBEs_PrincipalLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBEs_PrincipalLabel.Location = New System.Drawing.Point(1093, 70)
        Me.CMBEs_PrincipalLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBEs_PrincipalLabel.Name = "CMBEs_PrincipalLabel"
        Me.CMBEs_PrincipalLabel.Size = New System.Drawing.Size(114, 25)
        Me.CMBEs_PrincipalLabel.TabIndex = 426
        Me.CMBEs_PrincipalLabel.Text = " Principal :"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.PrecioTextBox)
        Me.Panel2.Controls.Add(PrecioLabel)
        Me.Panel2.Location = New System.Drawing.Point(57, 143)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(364, 38)
        Me.Panel2.TabIndex = 413
        Me.Panel2.TabStop = True
        '
        'PrecioTextBox
        '
        Me.PrecioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PrecioTextBox.CausesValidation = False
        Me.PrecioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Precio", True))
        Me.PrecioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrecioTextBox.Location = New System.Drawing.Point(93, 7)
        Me.PrecioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PrecioTextBox.Name = "PrecioTextBox"
        Me.PrecioTextBox.Size = New System.Drawing.Size(186, 24)
        Me.PrecioTextBox.TabIndex = 2
        '
        'CMBPanel4
        '
        Me.CMBPanel4.AutoScroll = True
        Me.CMBPanel4.Controls.Add(Me.CheckBox5)
        Me.CMBPanel4.Controls.Add(Me.CMBTextBox6)
        Me.CMBPanel4.Controls.Add(Me.TextBox17)
        Me.CMBPanel4.Controls.Add(Me.TextBox18)
        Me.CMBPanel4.Controls.Add(Me.NumericUpDown4)
        Me.CMBPanel4.Controls.Add(Label24)
        Me.CMBPanel4.Controls.Add(Label4)
        Me.CMBPanel4.Controls.Add(Label10)
        Me.CMBPanel4.Controls.Add(Me.TextBox4)
        Me.CMBPanel4.Controls.Add(Clv_ServicioLabel)
        Me.CMBPanel4.Enabled = False
        Me.CMBPanel4.Location = New System.Drawing.Point(392, 817)
        Me.CMBPanel4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBPanel4.Name = "CMBPanel4"
        Me.CMBPanel4.Size = New System.Drawing.Size(643, 161)
        Me.CMBPanel4.TabIndex = 636
        Me.CMBPanel4.Visible = False
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.Checked = True
        Me.CheckBox5.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox5.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.ConServiciosTelBindingSource, "Ilimitadas_LDI", True))
        Me.CheckBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBox5.Location = New System.Drawing.Point(165, 74)
        Me.CheckBox5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(118, 22)
        Me.CheckBox5.TabIndex = 665
        Me.CheckBox5.Text = "ILIMITADAS"
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'CMBTextBox6
        '
        Me.CMBTextBox6.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox6.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox6.Location = New System.Drawing.Point(40, 21)
        Me.CMBTextBox6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBTextBox6.Name = "CMBTextBox6"
        Me.CMBTextBox6.ReadOnly = True
        Me.CMBTextBox6.Size = New System.Drawing.Size(513, 19)
        Me.CMBTextBox6.TabIndex = 628
        Me.CMBTextBox6.TabStop = False
        Me.CMBTextBox6.Text = "Llamadas De Larga Distancia Internacional"
        Me.CMBTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox17
        '
        Me.TextBox17.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox17.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Costo_Inalambrico", True))
        Me.TextBox17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox17.Location = New System.Drawing.Point(179, 127)
        Me.TextBox17.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.ReadOnly = True
        Me.TextBox17.Size = New System.Drawing.Size(138, 23)
        Me.TextBox17.TabIndex = 657
        Me.TextBox17.TabStop = False
        '
        'TextBox18
        '
        Me.TextBox18.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox18.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Costo_Alambrico", True))
        Me.TextBox18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox18.Location = New System.Drawing.Point(396, 127)
        Me.TextBox18.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.ReadOnly = True
        Me.TextBox18.Size = New System.Drawing.Size(138, 23)
        Me.TextBox18.TabIndex = 659
        Me.TextBox18.TabStop = False
        '
        'NumericUpDown4
        '
        Me.NumericUpDown4.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConServiciosTelBindingSource, "Minutos_LDI", True))
        Me.NumericUpDown4.Location = New System.Drawing.Point(40, 73)
        Me.NumericUpDown4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown4.Name = "NumericUpDown4"
        Me.NumericUpDown4.Size = New System.Drawing.Size(84, 22)
        Me.NumericUpDown4.TabIndex = 444
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConServiciosTelBindingSource, "Costo_Min_LDI_Adic", True))
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextBox4.Location = New System.Drawing.Point(396, 75)
        Me.TextBox4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(138, 23)
        Me.TextBox4.TabIndex = 436
        Me.TextBox4.TabStop = False
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_ServicioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ServicioTextBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(24, 15)
        Me.Clv_ServicioTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.ReadOnly = True
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(138, 24)
        Me.Clv_ServicioTextBox.TabIndex = 422
        Me.Clv_ServicioTextBox.TabStop = False
        '
        'CLV_SERVICIOTextBox1
        '
        Me.CLV_SERVICIOTextBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CLV_SERVICIOTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CLV_SERVICIOTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Clv_TipSer", True))
        Me.CLV_SERVICIOTextBox1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CLV_SERVICIOTextBox1.Location = New System.Drawing.Point(24, 17)
        Me.CLV_SERVICIOTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CLV_SERVICIOTextBox1.Name = "CLV_SERVICIOTextBox1"
        Me.CLV_SERVICIOTextBox1.ReadOnly = True
        Me.CLV_SERVICIOTextBox1.Size = New System.Drawing.Size(138, 22)
        Me.CLV_SERVICIOTextBox1.TabIndex = 425
        Me.CLV_SERVICIOTextBox1.TabStop = False
        '
        'TxtClv_Session
        '
        Me.TxtClv_Session.Location = New System.Drawing.Point(24, 16)
        Me.TxtClv_Session.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtClv_Session.Name = "TxtClv_Session"
        Me.TxtClv_Session.Size = New System.Drawing.Size(132, 22)
        Me.TxtClv_Session.TabIndex = 629
        '
        'CLV_LLAVETextBox
        '
        Me.CLV_LLAVETextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CLV_LLAVETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CLV_LLAVETextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.CLV_LLAVETextBox.Location = New System.Drawing.Point(31, 916)
        Me.CLV_LLAVETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CLV_LLAVETextBox.Name = "CLV_LLAVETextBox"
        Me.CLV_LLAVETextBox.Size = New System.Drawing.Size(138, 22)
        Me.CLV_LLAVETextBox.TabIndex = 423
        Me.CLV_LLAVETextBox.TabStop = False
        Me.CLV_LLAVETextBox.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(1129, 857)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONSERVICIOSBindingNavigator
        '
        Me.CONSERVICIOSBindingNavigator.AddNewItem = Nothing
        Me.CONSERVICIOSBindingNavigator.BindingSource = Me.CONSERVICIOSBindingSource
        Me.CONSERVICIOSBindingNavigator.CountItem = Nothing
        Me.CONSERVICIOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONSERVICIOSBindingNavigator.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSERVICIOSBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONSERVICIOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONSERVICIOSBindingNavigatorSaveItem})
        Me.CONSERVICIOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONSERVICIOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MoveLastItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MoveNextItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONSERVICIOSBindingNavigator.Name = "CONSERVICIOSBindingNavigator"
        Me.CONSERVICIOSBindingNavigator.PositionItem = Nothing
        Me.CONSERVICIOSBindingNavigator.Size = New System.Drawing.Size(1350, 30)
        Me.CONSERVICIOSBindingNavigator.TabIndex = 6
        Me.CONSERVICIOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(113, 27)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.White
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(96, 27)
        Me.ToolStripButton1.Text = "Cancelar"
        '
        'CONSERVICIOSBindingNavigatorSaveItem
        '
        Me.CONSERVICIOSBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CONSERVICIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSERVICIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Name = "CONSERVICIOSBindingNavigatorSaveItem"
        Me.CONSERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(110, 27)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Text = "Guardar"
        '
        'CONSERVICIOSTableAdapter
        '
        Me.CONSERVICIOSTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipoClientesTableAdapter
        '
        Me.MuestraTipoClientesTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter
        '
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.ClearBeforeFill = True
        '
        'CONRel_Trabajos_NoCobroMensualTableAdapter
        '
        Me.CONRel_Trabajos_NoCobroMensualTableAdapter.ClearBeforeFill = True
        '
        'ConServiciosTelTableAdapter
        '
        Me.ConServiciosTelTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter
        '
        Me.Muestra_ServiciosDigitalesTableAdapter.ClearBeforeFill = True
        '
        'Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter
        '
        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.ClearBeforeFill = True
        '
        'Borrar_Rel_ServiciosDigitales_Tel_TMPBindingSource
        '
        Me.Borrar_Rel_ServiciosDigitales_Tel_TMPBindingSource.DataMember = "Borrar_Rel_ServiciosDigitales_Tel_TMP"
        Me.Borrar_Rel_ServiciosDigitales_Tel_TMPBindingSource.DataSource = Me.DataSetyahve
        '
        'Borrar_Rel_ServiciosDigitales_Tel_TMPTableAdapter
        '
        Me.Borrar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.ClearBeforeFill = True
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.CLV_SERVICIOTextBox1)
        Me.Panel6.Controls.Add(Me.TxtClv_Session)
        Me.Panel6.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Panel6.Location = New System.Drawing.Point(16, 943)
        Me.Panel6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(296, 58)
        Me.Panel6.TabIndex = 649
        '
        'CostoTextBox
        '
        Me.CostoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource, "Costo", True))
        Me.CostoTextBox.Location = New System.Drawing.Point(219, 113)
        Me.CostoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CostoTextBox.Name = "CostoTextBox"
        Me.CostoTextBox.Size = New System.Drawing.Size(132, 22)
        Me.CostoTextBox.TabIndex = 647
        '
        'NombreTextBox
        '
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource, "Nombre", True))
        Me.NombreTextBox.Location = New System.Drawing.Point(163, 4)
        Me.NombreTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(132, 22)
        Me.NombreTextBox.TabIndex = 645
        '
        'Clv_ServicioDigitalTextBox
        '
        Me.Clv_ServicioDigitalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource, "Clv_ServicioDigital", True))
        Me.Clv_ServicioDigitalTextBox.Location = New System.Drawing.Point(163, 41)
        Me.Clv_ServicioDigitalTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ServicioDigitalTextBox.Name = "Clv_ServicioDigitalTextBox"
        Me.Clv_ServicioDigitalTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clv_ServicioDigitalTextBox.TabIndex = 643
        '
        'Clv_TipocobroTextBox
        '
        Me.Clv_TipocobroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource, "Clv_Tipocobro", True))
        Me.Clv_TipocobroTextBox.Location = New System.Drawing.Point(20, 41)
        Me.Clv_TipocobroTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TipocobroTextBox.Name = "Clv_TipocobroTextBox"
        Me.Clv_TipocobroTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clv_TipocobroTextBox.TabIndex = 641
        '
        'Clv_ServicioTextBox2
        '
        Me.Clv_ServicioTextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox2.Location = New System.Drawing.Point(12, 9)
        Me.Clv_ServicioTextBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ServicioTextBox2.Name = "Clv_ServicioTextBox2"
        Me.Clv_ServicioTextBox2.Size = New System.Drawing.Size(132, 22)
        Me.Clv_ServicioTextBox2.TabIndex = 639
        '
        'Clv_SessionTextBox
        '
        Me.Clv_SessionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource, "Clv_Session", True))
        Me.Clv_SessionTextBox.Location = New System.Drawing.Point(12, 9)
        Me.Clv_SessionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_SessionTextBox.Name = "Clv_SessionTextBox"
        Me.Clv_SessionTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clv_SessionTextBox.TabIndex = 637
        '
        'Clave_IdTextBox
        '
        Me.Clave_IdTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource, "Clave_Id", True))
        Me.Clave_IdTextBox.Location = New System.Drawing.Point(163, 9)
        Me.Clave_IdTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clave_IdTextBox.Name = "Clave_IdTextBox"
        Me.Clave_IdTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clave_IdTextBox.TabIndex = 635
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Clave_IdTextBox)
        Me.Panel4.Controls.Add(Me.Clv_SessionTextBox)
        Me.Panel4.Controls.Add(Me.Clv_ServicioTextBox2)
        Me.Panel4.Controls.Add(Me.Clv_TipocobroTextBox)
        Me.Panel4.Controls.Add(Clv_ServicioDigitalLabel)
        Me.Panel4.Controls.Add(Me.Clv_ServicioDigitalTextBox)
        Me.Panel4.Controls.Add(Me.NombreTextBox)
        Me.Panel4.Controls.Add(CostoLabel)
        Me.Panel4.Controls.Add(Me.CostoTextBox)
        Me.Panel4.Location = New System.Drawing.Point(16, 1008)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(307, 69)
        Me.Panel4.TabIndex = 648
        '
        'Muestra_Dias_inicialTableAdapter
        '
        Me.Muestra_Dias_inicialTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Dias_FinalTableAdapter
        '
        Me.Muestra_Dias_FinalTableAdapter.ClearBeforeFill = True
        '
        'FrmServiciosTelefonia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1371, 903)
        Me.Controls.Add(Me.CONSERVICIOSBindingNavigator)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Label25)
        Me.Controls.Add(Me.CLV_LLAVETextBox)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel4)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmServiciosTelefonia"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Servicios"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel21.ResumeLayout(False)
        Me.Panel21.PerformLayout()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.ConServiciosTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.Muestra_Dias_FinalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetyahve, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Dias_inicialBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        CType(Me.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServiciosDigitalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConServiciosTelBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConServiciosTelBindingNavigator.ResumeLayout(False)
        Me.ConServiciosTelBindingNavigator.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        CType(Me.MuestraTipoClientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.Puntos3NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Puntos11NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Punto_Pronto_PagoNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Puntos6NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        CType(Me.CONRel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATRABAJOS_NOCOBROMENSUALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.CMBPanel4.ResumeLayout(False)
        Me.CMBPanel4.PerformLayout()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONSERVICIOSBindingNavigator.ResumeLayout(False)
        Me.CONSERVICIOSBindingNavigator.PerformLayout()
        CType(Me.Borrar_Rel_ServiciosDigitales_Tel_TMPBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBSale_en_CarteraLabel As System.Windows.Forms.Label
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TxtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Es_PrincipalCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Sale_en_CarteraCheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CLV_LLAVETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents CLV_SERVICIOTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBGenera_OrdenLabel As System.Windows.Forms.Label
    Friend WithEvents CMBEs_PrincipalLabel As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents PrecioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents NumericUpDown4 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents CMBTextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel18 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBAplicanComLabel As System.Windows.Forms.Label
    Friend WithEvents Sale_en_CarteraCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Genera_OrdenCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents CMBTextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Puntos11NumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Puntos6NumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Puntos3NumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Punto_Pronto_PagoNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents CONSERVICIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSERVICIOSTableAdapter As sofTV.DataSetLidia2TableAdapters.CONSERVICIOSTableAdapter
    Friend WithEvents CONSERVICIOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents MuestraTipoClientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipoClientesTableAdapter As sofTV.DataSetLidia2TableAdapters.MuestraTipoClientesTableAdapter
    Friend WithEvents CMBPanel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Clv_TipoClienteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents MUESTRATRABAJOS_NOCOBROMENSUALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter As sofTV.DataSetEDGARTableAdapters.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter
    Friend WithEvents CONRel_Trabajos_NoCobroMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRel_Trabajos_NoCobroMensualTableAdapter As sofTV.DataSetEDGARTableAdapters.CONRel_Trabajos_NoCobroMensualTableAdapter
    Friend WithEvents NumericUpDown7 As System.Windows.Forms.NumericUpDown
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents ConServiciosTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConServiciosTelTableAdapter As sofTV.DataSetLidia2TableAdapters.ConServiciosTelTableAdapter
    Friend WithEvents ConServiciosTelBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConServiciosTelBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents NumericUpDown8 As System.Windows.Forms.NumericUpDown
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents NumericUpDown5 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Panel21 As System.Windows.Forms.Panel
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DataSetyahve As sofTV.DataSetyahve
    Friend WithEvents MuestraServiciosDigitalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBTextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TxtClv_Session As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TxtBoxCosto As System.Windows.Forms.TextBox
    Friend WithEvents Consultar_Rel_ServiciosDigitales_Tel_TMPBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter As sofTV.DataSetyahveTableAdapters.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter
    Friend WithEvents Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Borrar_Rel_ServiciosDigitales_Tel_TMPBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borrar_Rel_ServiciosDigitales_Tel_TMPTableAdapter As sofTV.DataSetyahveTableAdapters.Borrar_Rel_ServiciosDigitales_Tel_TMPTableAdapter
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents CostoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioDigitalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipocobroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SessionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave_IdTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CMBTextBoxDias As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_Dias_inicialBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Dias_inicialTableAdapter As sofTV.DataSetyahveTableAdapters.Muestra_Dias_inicialTableAdapter
    Friend WithEvents Muestra_Dias_FinalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Dias_FinalTableAdapter As sofTV.DataSetyahveTableAdapters.Muestra_Dias_FinalTableAdapter
End Class
