Imports System.Data.SqlClient
Public Class FrmSelServRep

    Private Sub FrmSelServRep_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
   
    End Sub

    Private Sub FrmSelServRep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ListBox1.Visible = True
        If Recordatorio = 1 Then
            GloClv_tipser2 = 0
        End If
        'Me.ElegirServicioTableAdapter.Fill(Me.DataSetarnoldo.ElegirServicio, 1, "", "", GloClv_tipser2)
        If LocOp = 30 Or LocOp = 80 Or eOpVentas = 80 Then
            Me.MuestraSelecciona_TipoServicioTmpNUEVOTableAdapter.Connection = CON
            Me.MuestraSelecciona_TipoServicioTmpNUEVOTableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TipoServicioTmpNUEVO, LocClv_session, 1000)
        Else
            Me.MuestraSelecciona_TipoServicioTmpNUEVOTableAdapter.Connection = CON
            'Me.MuestraSelecciona_TipoServicioTmpNUEVOTableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_TipoServicioTmpNUEVO, LocClv_session, GloClv_tipser2)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            BaseII.CreateMyParameter("@Clv_tip_Serv", SqlDbType.BigInt, GloClv_tipser2)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.Inserta("MuestraSelecciona_TipoServicioTmpNUEVO")
        End If
        Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.Connection = CON
        'Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_ServiciosTmpCONSULTA, LocClv_session)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        ListBox1.DataSource = BaseII.ConsultaDT("MuestraSelecciona_ServiciosTmpCONSULTA")
        CON.Close()
        'Me.ListBox2.Text = ""
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Dim x As Integer = 0
        'Dim y As Integer
        'x = Me.ListBox2.Items.Count()
        'If x > 0 Then
        '    For y = 0 To (x - 1)
        '        Me.ListBox2.SelectedIndex = y
        '        If (Me.ListBox1.Text = Me.ListBox2.Text) Then
        '            MsgBox("El Servicio ya esta en la lista", MsgBoxStyle.Information)
        '            'Else
        '            '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '            '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        '        End If
        '    Next
        '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        'Else
        '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        'End If

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        Me.Insertauno_Seleccion_ServiciostmpTableAdapter.Connection = CON
        Me.Insertauno_Seleccion_ServiciostmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_Serviciostmp, LocClv_session, CStr(Me.ListBox1.SelectedValue))
        Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.Connection = CON
        'Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_ServiciosTmpCONSULTA, LocClv_session)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        ListBox1.DataSource = BaseII.ConsultaDT("MuestraSelecciona_ServiciosTmpCONSULTA")
        Me.MuestraSeleccion_ServiciosCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_ServiciosCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_ServiciosCONSULTA, LocClv_session)
        CON.Close()


    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If LocOp = 80 Then
            Borra_Tablas_Reporte_mensualidades(LocClv_session)
        End If
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Dim x As Integer
        'Dim y As Integer
        'x = Me.ListBox1.Items.Count()
        'If Me.ListBox2.Items.Count() > 0 Then
        '    MsgBox("Primero Borre los Servicios Seleccionados", MsgBoxStyle.Information)
        'Else
        '    Me.ListBox1.SelectedIndex = 0
        '    For y = 1 To x
        '        Me.ListBox1.SelectedIndex = (y - 1)
        '        Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '        Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        '    Next
        'End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTOSelecciona_ServiciosTableAdapter.Connection = CON
        Me.InsertaTOSelecciona_ServiciosTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTOSelecciona_Servicios, LocClv_session)
        Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.Connection = CON
        'Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_ServiciosTmpCONSULTA, LocClv_session)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        ListBox1.DataSource = BaseII.ConsultaDT("MuestraSelecciona_ServiciosTmpCONSULTA")
        Me.MuestraSeleccion_ServiciosCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_ServiciosCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_ServiciosCONSULTA, LocClv_session)
        CON.Close()


    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Dim x As Integer
        'Dim y As Integer
        'x = Me.ListBox2.Items.Count()
        'For y = 0 To (x - 1)
        '    Me.ListBox2.Items.RemoveAt(0)
        '    Me.ListBox3.Items.RemoveAt(0)
        'Next
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTOSelecciona_ServiciosTmpTableAdapter.Connection = CON
        Me.InsertaTOSelecciona_ServiciosTmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTOSelecciona_ServiciosTmp, LocClv_session)
        Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.Connection = CON
        'Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_ServiciosTmpCONSULTA, LocClv_session)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        ListBox1.DataSource = BaseII.ConsultaDT("MuestraSelecciona_ServiciosTmpCONSULTA")
        Me.MuestraSeleccion_ServiciosCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_ServiciosCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_ServiciosCONSULTA, LocClv_session)
        CON.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If (Me.ListBox2.SelectedIndex <> -1) Then
        '    Me.ListBox3.SelectedIndex = Me.ListBox2.SelectedIndex
        '    Me.ListBox2.Items.RemoveAt(Me.ListBox2.SelectedIndex)
        '    Me.ListBox3.Items.RemoveAt(Me.ListBox3.SelectedIndex)
        'Else
        '    MsgBox("Selecciona primero un valor a borrar")
        'End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Insertauno_Seleccion_ServiciosTableAdapter.Connection = CON
        Me.Insertauno_Seleccion_ServiciosTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_Servicios, LocClv_session, CStr(Me.ListBox2.SelectedValue))
        Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.Connection = CON
        'Me.MuestraSelecciona_ServiciosTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_ServiciosTmpCONSULTA, LocClv_session)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        ListBox1.DataSource = BaseII.ConsultaDT("MuestraSelecciona_ServiciosTmpCONSULTA")
        Me.MuestraSeleccion_ServiciosCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_ServiciosCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_ServiciosCONSULTA, LocClv_session)
        CON.Close()

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim x As Integer = 0
        x = Me.ListBox2.Items.Count()
        'z = Me.ListBox1.Items.Count()
        'For y = 0 To (x - 1)
        '    Me.ListBox3.SelectedIndex = y
        '    Me.LLena_Tabla_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.LLena_Tabla_Servicios, LocClv_session, Me.ListBox3.Text)
        'Next
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If x = 0 Then
            MsgBox("Seleccione Al Menos Un Servicio", MsgBoxStyle.Information)
            Exit Sub
        ElseIf x > 0 Then
            If varfrmselcompania = "normalrecordatorios" Then
                FrmSelPeriodo.Show()
                Me.Close()
                Exit Sub
            End If
            If LocOp = 3 Or LocOp = 4 Or LocOp = 5 Or LocOp = 6 Or LocOp = 7 Or LocOp = 9 Or LocOp = 10 Or LocOp = 11 Or LocOp = 30 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr2 = "Varios Servicios"
                Else
                    LocDescr2 = Me.ListBox2.Text
                    'My.Forms.FrmSelEstado.Show()
                End If
                'My.Forms.FrmSelColonias_Rep.Show()
                'My.Forms.FrmSelCiudad.Show()
                FrmSelColonia_Rep21.Show()
            End If
            If LocOp = 1 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr2 = "Varios Servicios"
                    'bndReportC = True
                Else
                    LocDescr2 = Me.ListBox2.Text
                    'bndReportC = True

                    'My.Forms.FrmSelEstado.Show()
                End If
                'My.Forms.FrmSelColonias_Rep.Show()
                'My.Forms.FrmSelCiudad.Show()
                FrmSelColonia_Rep21.Show()
            End If
            If LocOp = 20 Or LocOp = 22 Or LocOp = 23 Or LocOp = 25 Or LocOp = 35 Then
                'FrmSelCiudad.Show()
                FrmSelColonia_Rep21.Show()
            End If
            If LocOp = 80 Then
                bndimportemens = True
                FrmImprimirContrato.Show()
                Me.Close()
            End If
            If LocOp = 45 Then
                FrmSelColonia_Rep21.Show()
            End If
            If eOpVentas = 80 Then
                FrmImprimirComision.Show()
            End If
        End If
        Me.Separa_ServiciosTableAdapter.Connection = CON
        Me.Separa_ServiciosTableAdapter.Fill(Me.ProcedimientosArnoldo2.Separa_Servicios, LocClv_session)
        CON.Close()
        Me.Close()
    End Sub

End Class