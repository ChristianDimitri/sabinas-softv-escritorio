﻿Imports System.Data.SqlClient
Public Class BrwPromocionesNew

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        ClvPromocion = 0
        FrmPromocionesNew.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        opcion = "C"
        ClvPromocion = DataGridView1.SelectedCells(0).Value
        FrmPromocionesNew.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        opcion = "M"
        ClvPromocion = DataGridView1.SelectedCells(0).Value
        FrmPromocionesNew.Show()
    End Sub

    Private Sub BrwPromocionesNew_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Llena_grid("")
    End Sub

    Private Sub BrwPromocionesNew_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Llena_grid("")
    End Sub

    Private Sub Llena_grid(ByVal nombre As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, nombre)
            DataGridView1.DataSource = BaseII.ConsultaDT("BrwPromociones")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If TextBox2.Text.Trim = "" Then
            Exit Sub
        End If
        Llena_grid(TextBox2.Text.Trim)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Dim comando As New SqlCommand()
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            comando.Connection = conexion
            comando.CommandText = "exec DameDatosPromocion " + DataGridView1.SelectedCells(0).Value.ToString
            Dim reader As SqlDataReader = comando.ExecuteReader()
            reader.Read()
            lbPromocion.Text = reader(1).ToString
            lbContratacion.Text = "$ " + reader(2).ToString
            lbMensualidad.Text = "$ " + reader(3).ToString
            lbPlazo.Text = reader(4).ToString
            reader.Close()
            conexion.Close()
        Catch ex As Exception

        End Try
    End Sub
End Class