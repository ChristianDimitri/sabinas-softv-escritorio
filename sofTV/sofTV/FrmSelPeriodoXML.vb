﻿Imports System.Data.SqlClient
Public Class FrmSelPeriodoXML

    Private Sub FrmSelPeriodoXML_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CargarElementosPeriodosXML()
        llenalistboxs()
        If loquehay.Items.Count = 1 Or PasarTodoXML Then
            llevamealotro()
            Me.Close()
        End If
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPeriodos.SelectNodes("//PERIODO")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Periodo") = loquehay.SelectedValue Then
                elem.SetAttribute("Seleccion", 1)
            End If
        Next
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPeriodos.SelectNodes("//PERIODO")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Periodo") = seleccion.SelectedValue Then
                elem.SetAttribute("Seleccion", 0)
            End If
        Next
        llenalistboxs()

    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPeriodos.SelectNodes("//PERIODO")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        llenalistboxs()

    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPeriodos.SelectNodes("//PERIODO")
        For Each elem As Xml.XmlElement In elementos

            elem.SetAttribute("Seleccion", 0)

        Next
        llenalistboxs()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        EntradasSelCiudad = 0
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Select Case LocOp
            Case 1
                bndReportC = True
            Case 5
                bndReport2 = True
            Case 3
                If bnd_Canc_Sin_Mens = False Then
                    bndfechareport = True
                ElseIf bnd_Canc_Sin_Mens = True Then
                    bnd_Canc_Sin_Mens = False
                    bnd_Canc_Sin_Mens_buena = True
                End If
            Case 6
                GloBndEtiqueta = True
            Case 7
                If GloOpEtiqueta <> "1" Then
                    If IdSistema = "SA" And (GloOpEtiqueta = "3" Or GloOpEtiqueta = "4" Or GloOpEtiqueta = "1") Then
                        bndAvisos2 = True
                    Else
                        FrmSelRecord.Show()
                    End If
                ElseIf GloOpEtiqueta = "1" And Recordatorio = 0 Then
                    FrmTelsi.Show()
                    'Else
                    '    bndAvisos2 = True
                    'End If
                ElseIf GloOpEtiqueta = "1" And Recordatorio = 1 Then
                    FrmImprimirContrato.Show()
                    Me.Close()
                    Exit Sub
                End If

            Case 8
                GloBndSelBanco = True
            Case 9
                bndReportA = True
            Case 10
                LocServicios = True
            Case 20
                'If bec_bnd = False Then
                Locreportcity = True
                'ElseIf bec_bnd = True Then

                'End If
            Case 21
                bndfechareport2 = True
            Case 22
                GloBndSelBanco = True
            Case 25
                Locreportcity = True
            Case 30
                LocBndRepMix = True
                LocGloOpRep = 2
                FrmImprimirFac.Show()
            Case 35
                'Locbndrepcontspago = True
                FrmSelFechas.Show()
            Case 90
                'LReportecombo1 = True
                'FrmImprimirContrato.Show()
                FrmOpcionRepCombos.Show()
            Case 45
                'Reporte Contrataciones principales
                bndfechareport = True
        End Select
        If LEdo_Cuenta = True Or LEdo_Cuenta2 = True Then
            ' bec_bnd = False
            FrmImprimirContrato.Show()
            'LocreportEstado = True
        End If
        If GlovienedeCartera = 1 Then
            GlovienedeCartera = 0
            GloPeriodoCartera = 1
            execar = True
        End If
        If op = "0" And GloClv_tipser2 = 1 And LocOp <> 4 Then
            FrmOrdenEjePen.Show()
        End If
        If GloOpFiltrosXML = "VariosDesconectados" Then
            'FrmTelsi.Show()
            If bnd1 = True Then
                FrmSelUltimoMes.Show()
            Else
                bndReport = True
            End If
        End If
        If GloOpFiltrosXML = "VariosAlCorriente" Then
            'FrmTelSi
            bndReport2 = True
        End If
        If GloOpFiltrosXML = "VariosContrataciones" Then
            FrmSelFechas.Show()
        End If
        If GloOpFiltrosXML = "VariosCortesia" Then
            bndReportC = True
        End If
        If GloOpFiltrosXML = "VariosCiudad" Then
            FrmSelEstado.Show()
        End If
        If GloOpFiltrosXML = "MensajesVarios" Then
            FrmSelEstado_Mensajes.Show()
        End If
        If GloOpFiltrosXML = "EnviarSMS" Then
            FrmSms.Show()
        End If
        If GloOpFiltrosXML = "Carteras" Then
            execar = True
        End If
        If GloOpFiltrosXML = "CarteraDistribuidor" Then
            execar = True
        End If
        Me.Close()
    End Sub
    Private Sub llevamealotro()
        Dim elementos = DocPeriodos.SelectNodes("//PERIODO")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next

        If GloOpFiltrosXML = "VariosDesconectados" Then
            If bnd1 = True Then
                FrmSelUltimoMes.Show()
            Else
                bndReport = True
            End If
        End If
        If GloOpFiltrosXML = "VariosAlCorriente" Then
            'FrmTelSi
            bndReport2 = True
        End If
        If GloOpFiltrosXML = "VariosContrataciones" Then
            FrmSelFechas.Show()
        End If
        If GloOpFiltrosXML = "VariosCortesia" Then
            bndReportC = True
        End If
        If GloOpFiltrosXML = "VariosCiudad" Then
            FrmSelEstado.Show()
        End If
        If GloOpFiltrosXML = "MensajesVarios" Then
            FrmSelEstado_Mensajes.Show()
        End If
        If GloOpFiltrosXML = "EnviarSMS" Then
            FrmSms.Show()
        End If
        If GloOpFiltrosXML = "Carteras" Then
            execar = True
        End If
        If GloOpFiltrosXML = "CarteraDistribuidor" Then
            execar = True
        End If
    End Sub

    Private Sub llenalistboxs()
        Dim dt As New DataTable
        dt.Columns.Add("Clv_Periodo", Type.GetType("System.String"))
        dt.Columns.Add("Descripcion", Type.GetType("System.String"))

        Dim dt2 As New DataTable
        dt2.Columns.Add("Clv_Periodo", Type.GetType("System.String"))
        dt2.Columns.Add("Descripcion", Type.GetType("System.String"))

        Dim elementos = DocPeriodos.SelectNodes("//PERIODO")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Seleccion") = 0 Then
                dt.Rows.Add(elem.GetAttribute("Clv_Periodo"), elem.GetAttribute("Descripcion"))
            End If
            If elem.GetAttribute("Seleccion") = 1 Then
                dt2.Rows.Add(elem.GetAttribute("Clv_Periodo"), elem.GetAttribute("Descripcion"))
            End If
        Next
        loquehay.DataSource = New BindingSource(dt, Nothing)
        seleccion.DataSource = New BindingSource(dt2, Nothing)
    End Sub
End Class