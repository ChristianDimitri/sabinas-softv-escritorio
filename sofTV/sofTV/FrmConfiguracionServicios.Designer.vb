﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfiguracionServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBoxServicios = New System.Windows.Forms.ComboBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.BtnAgregar = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ComboBoxTipSer = New System.Windows.Forms.ComboBox()
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.DataSetEDGARTableAdapters.MuestraTipSerPrincipalTableAdapter()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.id_compania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NomCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NomEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Ciudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NomCiudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cbPlaza = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(609, 105)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 15)
        Me.Label1.TabIndex = 110
        Me.Label1.Text = "Servicio:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(609, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 15)
        Me.Label2.TabIndex = 116
        Me.Label2.Text = "Tipo Servicio:"
        Me.Label2.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(46, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(292, 20)
        Me.Label3.TabIndex = 122
        Me.Label3.Text = "Áreas de presencia del distribuidor:"
        '
        'ComboBoxServicios
        '
        Me.ComboBoxServicios.DisplayMember = "Nombre"
        Me.ComboBoxServicios.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxServicios.FormattingEnabled = True
        Me.ComboBoxServicios.Location = New System.Drawing.Point(612, 123)
        Me.ComboBoxServicios.Name = "ComboBoxServicios"
        Me.ComboBoxServicios.Size = New System.Drawing.Size(218, 26)
        Me.ComboBoxServicios.TabIndex = 109
        Me.ComboBoxServicios.ValueMember = "Clv_Estado"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id_compania, Me.NomCompania, Me.Clv_Estado, Me.NomEstado, Me.Clv_Ciudad, Me.NomCiudad})
        Me.DataGridView1.Location = New System.Drawing.Point(47, 96)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(536, 450)
        Me.DataGridView1.TabIndex = 111
        '
        'BtnAgregar
        '
        Me.BtnAgregar.BackColor = System.Drawing.Color.Orange
        Me.BtnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAgregar.ForeColor = System.Drawing.Color.Black
        Me.BtnAgregar.Location = New System.Drawing.Point(866, 38)
        Me.BtnAgregar.Name = "BtnAgregar"
        Me.BtnAgregar.Size = New System.Drawing.Size(120, 31)
        Me.BtnAgregar.TabIndex = 113
        Me.BtnAgregar.Text = "Agregar"
        Me.BtnAgregar.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Orange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(866, 157)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(120, 36)
        Me.Button1.TabIndex = 114
        Me.Button1.Text = "Eliminar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ComboBoxTipSer
        '
        Me.ComboBoxTipSer.DataSource = Me.MuestraTipSerPrincipalBindingSource
        Me.ComboBoxTipSer.DisplayMember = "Concepto"
        Me.ComboBoxTipSer.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBoxTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxTipSer.FormattingEnabled = True
        Me.ComboBoxTipSer.Location = New System.Drawing.Point(612, 66)
        Me.ComboBoxTipSer.Name = "ComboBoxTipSer"
        Me.ComboBoxTipSer.Size = New System.Drawing.Size(218, 24)
        Me.ComboBoxTipSer.TabIndex = 117
        Me.ComboBoxTipSer.ValueMember = "Clv_TipSerPrincipal"
        Me.ComboBoxTipSer.Visible = False
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(866, 73)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 77)
        Me.Button2.TabIndex = 118
        Me.Button2.Text = "Agregar servicio a todas las plazas"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(866, 199)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(120, 79)
        Me.Button3.TabIndex = 119
        Me.Button3.Text = "Eliminar servicio de todas las plazas"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Orange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(866, 510)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(120, 36)
        Me.Button4.TabIndex = 120
        Me.Button4.Text = "Salir"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Servicio, Me.Descripcion})
        Me.DataGridView2.Location = New System.Drawing.Point(612, 163)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.RowHeadersVisible = False
        Me.DataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView2.Size = New System.Drawing.Size(218, 383)
        Me.DataGridView2.TabIndex = 121
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.ReadOnly = True
        Me.Clv_Servicio.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Servicio"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 215
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Orange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(866, 369)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(120, 79)
        Me.Button5.TabIndex = 123
        Me.Button5.Text = "Agregar todos los servicios  a la plaza"
        Me.Button5.UseVisualStyleBackColor = False
        Me.Button5.Visible = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Orange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(866, 284)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(120, 79)
        Me.Button6.TabIndex = 124
        Me.Button6.Text = "Eliminar todos los servicios de la plaza"
        Me.Button6.UseVisualStyleBackColor = False
        Me.Button6.Visible = False
        '
        'id_compania
        '
        Me.id_compania.DataPropertyName = "id_compania"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.id_compania.DefaultCellStyle = DataGridViewCellStyle5
        Me.id_compania.HeaderText = "id_compania"
        Me.id_compania.Name = "id_compania"
        Me.id_compania.ReadOnly = True
        Me.id_compania.Visible = False
        '
        'NomCompania
        '
        Me.NomCompania.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.NomCompania.DataPropertyName = "NomCompania"
        Me.NomCompania.HeaderText = "Plaza"
        Me.NomCompania.Name = "NomCompania"
        Me.NomCompania.ReadOnly = True
        '
        'Clv_Estado
        '
        Me.Clv_Estado.DataPropertyName = "Clv_Estado"
        Me.Clv_Estado.HeaderText = "Clv_Estado"
        Me.Clv_Estado.Name = "Clv_Estado"
        Me.Clv_Estado.ReadOnly = True
        Me.Clv_Estado.Visible = False
        '
        'NomEstado
        '
        Me.NomEstado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.NomEstado.DataPropertyName = "NomEstado"
        Me.NomEstado.HeaderText = "Estado"
        Me.NomEstado.Name = "NomEstado"
        Me.NomEstado.ReadOnly = True
        '
        'Clv_Ciudad
        '
        Me.Clv_Ciudad.DataPropertyName = "Clv_Ciudad"
        Me.Clv_Ciudad.HeaderText = "Clv_Ciudad"
        Me.Clv_Ciudad.Name = "Clv_Ciudad"
        Me.Clv_Ciudad.ReadOnly = True
        Me.Clv_Ciudad.Visible = False
        '
        'NomCiudad
        '
        Me.NomCiudad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.NomCiudad.DataPropertyName = "NomCiudad"
        Me.NomCiudad.HeaderText = "Municipio"
        Me.NomCiudad.Name = "NomCiudad"
        Me.NomCiudad.ReadOnly = True
        '
        'cbPlaza
        '
        Me.cbPlaza.DisplayMember = "Nombre"
        Me.cbPlaza.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbPlaza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPlaza.FormattingEnabled = True
        Me.cbPlaza.Location = New System.Drawing.Point(28, 34)
        Me.cbPlaza.Name = "cbPlaza"
        Me.cbPlaza.Size = New System.Drawing.Size(434, 24)
        Me.cbPlaza.TabIndex = 126
        Me.cbPlaza.ValueMember = "clv_plaza"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(25, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(106, 20)
        Me.Label4.TabIndex = 125
        Me.Label4.Text = "Distribuidor:"
        '
        'FrmConfiguracionServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 589)
        Me.Controls.Add(Me.cbPlaza)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DataGridView2)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ComboBoxTipSer)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.BtnAgregar)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxServicios)
        Me.Name = "FrmConfiguracionServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configuración de Servicios por Distribuidor"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ComboBoxServicios As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents BtnAgregar As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ComboBoxTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.DataSetEDGARTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents id_compania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NomCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NomEstado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Ciudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NomCiudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cbPlaza As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
