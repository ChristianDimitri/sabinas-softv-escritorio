﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwIPs
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.buscaRedTxt = New System.Windows.Forms.TextBox()
        Me.BuscaRedBtn = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvIps = New System.Windows.Forms.DataGridView()
        Me.IdIP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IP_1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IP_2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IP_3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IP_4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IP_ALL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimaFechaAsignacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimaFechaLiberacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimoClienteAsignado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdRed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        CType(Me.dgvIps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'buscaRedTxt
        '
        Me.buscaRedTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buscaRedTxt.Location = New System.Drawing.Point(48, 21)
        Me.buscaRedTxt.Name = "buscaRedTxt"
        Me.buscaRedTxt.Size = New System.Drawing.Size(165, 21)
        Me.buscaRedTxt.TabIndex = 57
        '
        'BuscaRedBtn
        '
        Me.BuscaRedBtn.BackColor = System.Drawing.Color.DarkOrange
        Me.BuscaRedBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BuscaRedBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BuscaRedBtn.ForeColor = System.Drawing.Color.Black
        Me.BuscaRedBtn.Location = New System.Drawing.Point(219, 19)
        Me.BuscaRedBtn.Name = "BuscaRedBtn"
        Me.BuscaRedBtn.Size = New System.Drawing.Size(88, 23)
        Me.BuscaRedBtn.TabIndex = 55
        Me.BuscaRedBtn.Text = "&Buscar"
        Me.BuscaRedBtn.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 15)
        Me.Label3.TabIndex = 56
        Me.Label3.Text = "IP :"
        '
        'dgvIps
        '
        Me.dgvIps.AllowUserToAddRows = False
        Me.dgvIps.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIps.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvIps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvIps.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdIP, Me.IP_1, Me.IP_2, Me.IP_3, Me.IP_4, Me.IP_ALL, Me.STATUS, Me.UltimaFechaAsignacion, Me.UltimaFechaLiberacion, Me.UltimoClienteAsignado, Me.IdRed})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvIps.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvIps.Location = New System.Drawing.Point(12, 56)
        Me.dgvIps.MultiSelect = False
        Me.dgvIps.Name = "dgvIps"
        Me.dgvIps.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvIps.Size = New System.Drawing.Size(680, 484)
        Me.dgvIps.TabIndex = 54
        '
        'IdIP
        '
        Me.IdIP.DataPropertyName = "IdIP"
        Me.IdIP.FillWeight = 90.0!
        Me.IdIP.HeaderText = "Id"
        Me.IdIP.Name = "IdIP"
        Me.IdIP.Width = 80
        '
        'IP_1
        '
        Me.IP_1.DataPropertyName = "IP_1"
        Me.IP_1.HeaderText = "IP_1"
        Me.IP_1.Name = "IP_1"
        Me.IP_1.Visible = False
        '
        'IP_2
        '
        Me.IP_2.DataPropertyName = "IP_2"
        Me.IP_2.HeaderText = "IP_2"
        Me.IP_2.Name = "IP_2"
        Me.IP_2.Visible = False
        '
        'IP_3
        '
        Me.IP_3.DataPropertyName = "IP_3"
        Me.IP_3.HeaderText = "IP_3"
        Me.IP_3.Name = "IP_3"
        Me.IP_3.Visible = False
        '
        'IP_4
        '
        Me.IP_4.DataPropertyName = "IP_4"
        Me.IP_4.HeaderText = "IP_4"
        Me.IP_4.Name = "IP_4"
        Me.IP_4.Visible = False
        '
        'IP_ALL
        '
        Me.IP_ALL.DataPropertyName = "IP_ALL"
        Me.IP_ALL.FillWeight = 145.0!
        Me.IP_ALL.HeaderText = "IP"
        Me.IP_ALL.Name = "IP_ALL"
        Me.IP_ALL.Width = 145
        '
        'STATUS
        '
        Me.STATUS.DataPropertyName = "STATUS"
        Me.STATUS.FillWeight = 80.0!
        Me.STATUS.HeaderText = "Status"
        Me.STATUS.Name = "STATUS"
        Me.STATUS.Width = 80
        '
        'UltimaFechaAsignacion
        '
        Me.UltimaFechaAsignacion.DataPropertyName = "UltimaFechaAsignacion"
        Me.UltimaFechaAsignacion.HeaderText = "Última Fecha de Asignación"
        Me.UltimaFechaAsignacion.Name = "UltimaFechaAsignacion"
        '
        'UltimaFechaLiberacion
        '
        Me.UltimaFechaLiberacion.DataPropertyName = "UltimaFechaLiberacion"
        Me.UltimaFechaLiberacion.HeaderText = "Última Fecha de Liberación"
        Me.UltimaFechaLiberacion.Name = "UltimaFechaLiberacion"
        '
        'UltimoClienteAsignado
        '
        Me.UltimoClienteAsignado.DataPropertyName = "UltimoClienteAsignado"
        Me.UltimoClienteAsignado.FillWeight = 120.0!
        Me.UltimoClienteAsignado.HeaderText = "Último Cliente Asignado"
        Me.UltimoClienteAsignado.Name = "UltimoClienteAsignado"
        Me.UltimoClienteAsignado.Width = 120
        '
        'IdRed
        '
        Me.IdRed.DataPropertyName = "IdRed"
        Me.IdRed.HeaderText = "IdRed"
        Me.IdRed.Name = "IdRed"
        Me.IdRed.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(703, 504)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(120, 36)
        Me.Button1.TabIndex = 61
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(703, 98)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 36)
        Me.Button2.TabIndex = 60
        Me.Button2.Text = "&MODIFICAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Orange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(703, 56)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(120, 36)
        Me.Button6.TabIndex = 59
        Me.Button6.Text = "&CONSULTAR"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'BrwIPs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(834, 552)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.buscaRedTxt)
        Me.Controls.Add(Me.BuscaRedBtn)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dgvIps)
        Me.MaximizeBox = False
        Me.Name = "BrwIPs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de IP"
        CType(Me.dgvIps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents buscaRedTxt As System.Windows.Forms.TextBox
    Friend WithEvents BuscaRedBtn As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvIps As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents IdIP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IP_1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IP_2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IP_3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IP_4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IP_ALL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimaFechaAsignacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimaFechaLiberacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimoClienteAsignado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdRed As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
