﻿Imports System.Data.SqlClient
Public Class FrmUsuarios
    Private clv_usuario As String = Nothing
    Private nombre As String = Nothing
    Private domicilio As String = Nothing
    Private colonia As String = Nothing
    Private activa As String = Nothing
    Private pasaporte As String = Nothing
    Private tipousuario As String = Nothing
    Private email As String = Nothing
    Private fechaingreso As String = Nothing
    Private fechabaja As String = Nothing
    Private CATV As String = Nothing
    Private facturacion As String = Nothing
    Private GrupoVentas As String = Nothing
    Private eClv_Grupo As Integer = 0
    Private FECHA As Date
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, gloClave)
            BaseII.CreateMyParameter("@clvplaza", SqlDbType.Int, ComboBoxPlaza.SelectedValue)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_FrmUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            Else
                ComboBoxCompanias.Text = ""
            End If

            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Llena_ciudades()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, gloClave)
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("Muestra_Ciudad_FrmUsuario")
            ComboBoxCiudad.DisplayMember = "nombre"
            ComboBoxCiudad.ValueMember = "clv_ciudad"
            If ComboBoxCiudad.Items.Count > 0 Then
                ComboBoxCiudad.SelectedIndex = 0
            Else
                ComboBoxCiudad.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llena_dgvcompanias()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        dgvcompania.DataSource = BaseII.ConsultaDT("AgregaEliminaRelCompaniaUsuario")
    End Sub
    Private Sub llena_dgvciudades()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        dgvciudad.DataSource = BaseII.ConsultaDT("AgregaEliminaRelCiudadUsuario")
    End Sub
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                clv_usuario = Me.Clv_UsuarioTextBox.Text
                nombre = Me.NombreTextBox.Text
                domicilio = Me.DomicilioTextBox.Text
                colonia = Me.ColoniaTextBox.Text
                If Me.ActivoCheckBox.CheckState = CheckState.Checked Then
                    activa = "True"
                Else
                    activa = "False"
                End If
                pasaporte = Me.PasaporteTextBox.Text
                tipousuario = Me.ComboBox1.Text
                email = Me.EmailTextBox.Text
                fechaingreso = Me.FechaIngresoMaskedTextBox.Text
                fechabaja = Me.FechaSalidaMaskedTextBox.Text
                If Me.CATVCheckBox.CheckState = CheckState.Checked Then
                    CATV = "True"
                Else
                    CATV = "False"
                End If
                If Me.FacturacionCheckBox.CheckState = CheckState.Checked Then
                    facturacion = "True"
                Else
                    facturacion = "False"
                End If
                GrupoVentas = Me.GrupoComboBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardadatosbitacora(ByVal op As Integer)
        Try
            Dim validacion1 As String = Nothing
            Dim validacion2 As String = Nothing
            Dim validacion3 As String = Nothing
            Select Case op
                Case 0
                    If opcion = "M" Then
                        'clv_usuario = Me.Clv_UsuarioTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.Clv_UsuarioTextBox.Name, clv_usuario, Me.Clv_UsuarioTextBox.Text, LocClv_Ciudad)
                        'nombre = Me.NombreTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.NombreTextBox.Name, nombre, Me.NombreTextBox.Text, LocClv_Ciudad)
                        'domicilio = Me.DomicilioTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.DomicilioTextBox.Name, domicilio, Me.DomicilioTextBox.Text, LocClv_Ciudad)
                        'colonia = Me.ColoniaTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.ColoniaTextBox.Name, colonia, Me.ColoniaTextBox.Text, LocClv_Ciudad)
                        'Activo
                        If Me.ActivoCheckBox.CheckState = CheckState.Checked Then
                            'activa = "True"
                            validacion1 = "True"
                        Else
                            'activa = "False"
                            validacion1 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / usuarioactivo", activa, validacion1, LocClv_Ciudad)
                        'pasaporte = Me.PasaporteTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.PasaporteTextBox.Name, pasaporte, Me.PasaporteTextBox.Text, LocClv_Ciudad)
                        'tipousuario = Me.ComboBox1.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / Tipousuario", tipousuario, Me.ComboBox1.Text, LocClv_Ciudad)
                        'email = Me.EmailTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.EmailTextBox.Name, email, Me.EmailTextBox.Text, LocClv_Ciudad)
                        'fechaingreso = Me.FechaIngresoMaskedTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.FechaIngresoMaskedTextBox.Name, fechaingreso, Me.FechaIngresoMaskedTextBox.Text, LocClv_Ciudad)
                        'fechabaja = Me.FechaSalidaMaskedTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.FechaSalidaMaskedTextBox.Name, fechabaja, Me.FechaSalidaMaskedTextBox.Text, LocClv_Ciudad)
                        'CATV
                        If Me.CATVCheckBox.CheckState = CheckState.Checked Then
                            'CATV = "True"
                            validacion2 = "True"
                        Else
                            'CATV = "False"
                            validacion2 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / Controldeclientes", CATV, validacion2, LocClv_Ciudad)

                        'Facturacion
                        If Me.FacturacionCheckBox.CheckState = CheckState.Checked Then
                            'facturacion = "True"
                            validacion3 = "True"
                        Else
                            'facturacion = "False"
                            validacion3 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / Facturacion", facturacion, validacion3, LocClv_Ciudad)
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / Grupo Ventas", GrupoVentas, Me.GrupoComboBox.Text, LocClv_Ciudad)

                    ElseIf opcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Agrego un nuevo Usuario", "", "Agrego Un Nuevo Usuario: " + Me.Clv_UsuarioTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Un Usuario", "", "Clave Usuario: " + CStr(Me.ClaveLabel1.Text), LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONUSUARIOSTableAdapter.Connection = CON
            Me.CONUSUARIOSTableAdapter.Fill(Me.NewSofTvDataSet.CONUSUARIOS, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            CON.Close()

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clave", SqlDbType.Int, CLAVE)
            BaseII.CreateMyParameter("@RecibeMensaje", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@NotaDeCredito", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@clv_identificacion", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@RecibeMensajeDocumento", ParameterDirection.Output, SqlDbType.Int)

            BaseII.ProcedimientoOutPut("Muestra_AceptarMensajes")
            CheckBox3.Checked = CBool(BaseII.dicoPar("@RecibeMensaje"))
            CheckBoxNotadecredito.Checked = CBool(BaseII.dicoPar("@NotaDeCredito"))
            CheckBoxRecibeAlerta.Checked = CBool(BaseII.dicoPar("@RecibeMensajeDocumento"))
            cbIdentificacion.SelectedValue = BaseII.dicoPar("@clv_identificacion")
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        If opcion = "N" Then
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONUSUARIOSTableAdapter.Connection = CON
        Me.CONUSUARIOSTableAdapter.Delete(gloClave)
        If UCase(Me.ComboBox1.Text) = UCase("Control Total") Then
            Me.ConRelUsuarioEmailTableAdapter.Connection = CON
            Me.ConRelUsuarioEmailTableAdapter.Delete(Me.Clv_UsuarioTextBox.Text)
        End If
        guardadatosbitacora(1)
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub

    Private Sub CONUSUARIOSBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONUSUARIOSBindingNavigatorSaveItem.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Validate()
            Me.CONUSUARIOSBindingSource.EndEdit()
            Me.CONUSUARIOSTableAdapter.Connection = CON
            Me.CONUSUARIOSTableAdapter.Update(Me.NewSofTvDataSet.CONUSUARIOS)
            If UCase(Me.ComboBox1.Text) = UCase("Control Total") Then
                Me.ConRelUsuarioEmailTableAdapter.Connection = CON
                Me.ConRelUsuarioEmailTableAdapter.Insert(Me.Clv_UsuarioTextBox.Text, Me.EmailTextBox.Text)
            End If
            Me.NueRelUsuarioGrupoVentasTableAdapter.Connection = CON
            Me.NueRelUsuarioGrupoVentasTableAdapter.Fill(Me.DataSetEric2.NueRelUsuarioGrupoVentas, Me.Clv_UsuarioTextBox.Text, CInt(Me.GrupoComboBox.SelectedValue))
            MsgBox(mensaje5)
            guardadatosbitacora(0)
            GloBnd = True
            CON.Close()



            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clave", SqlDbType.Int, CInt(ClaveLabel1.Text))
            If CheckBox3.Checked Then
                BaseII.CreateMyParameter("@RecibeMensaje", SqlDbType.Int, 1)
            Else
                BaseII.CreateMyParameter("@RecibeMensaje", SqlDbType.Int, 0)
            End If
            If CheckBoxNotadecredito.Checked Then
                BaseII.CreateMyParameter("@NotaDeCredito", SqlDbType.Int, 1)
            Else
                BaseII.CreateMyParameter("@NotaDeCredito", SqlDbType.Int, 0)
            End If
            If cbIdentificacion.SelectedValue = Nothing Then
                BaseII.CreateMyParameter("@clv_identificacion", SqlDbType.Int, 0)
            Else
                BaseII.CreateMyParameter("@clv_identificacion", SqlDbType.Int, cbIdentificacion.SelectedValue)
            End If
            If CheckBoxRecibeAlerta.Checked Then
                BaseII.CreateMyParameter("@RecibeMensajeDocumento", SqlDbType.Int, 1)
            Else
                BaseII.CreateMyParameter("@RecibeMensajeDocumento", SqlDbType.Int, 0)
            End If
            BaseII.Inserta("Inserta_AceptarMensajes")

            If opcion = "N" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clvusuario", SqlDbType.VarChar, Clv_UsuarioTextBox.Text)
                BaseII.CreateMyParameter("@gloclave", ParameterDirection.Output, SqlDbType.Int)
                BaseII.ProcedimientoOutPut("DameClaveNuevoUsuario")
                gloClave = BaseII.dicoPar("@gloclave")
                ComboBoxPlaza.Enabled = True
                ComboBoxCompanias.Enabled = True
                dgvcompania.Enabled = True
                btnPlazaAdic.Enabled = True
                Llena_Plaza()
                Llena_companias()
                llena_dgvcompanias()
                
                opcion = "M"
            Else
                Me.Close()
            End If


        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            System.Windows.Forms.MessageBox.Show("La Clave del Usuario ya exíste., Teclee Otra por favor")
        End Try
    End Sub

    Private Sub FrmUsuarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRATIPOUSUARIOS' Puede moverla o quitarla según sea necesario.
        'Me.MUESTRATIPOUSUARIOSTableAdapter.Connection = CON
        'Me.MUESTRATIPOUSUARIOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATIPOUSUARIOS)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@bndSuper", SqlDbType.Bit, bndSuper)
        ComboBox1.DataSource = BaseII.ConsultaDT("MUESTRATIPOUSUARIOS")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_identificacion", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, "")
        cbIdentificacion.DataSource = BaseII.ConsultaDT("ConsultaIdentificacionUsuario")
        If opcion = "N" Then
            Me.CONUSUARIOSBindingSource.AddNew()
            ASIGNAfeCHA()
            Me.FechaIngresoMaskedTextBox.Text = Me.FECHA
            Panel1.Enabled = True
            BuscaRelUsuarioGrupoVentas()
            Button1.Enabled = False
            Button2.Enabled = False
            ComboBoxPlaza.Enabled = False
            ComboBoxCompanias.Enabled = False
            dgvcompania.Enabled = False
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
            If UCase(Me.ComboBox1.Text) = UCase("Control Total") Then
                Me.ConRelUsuarioEmailTableAdapter.Connection = CON
                Me.ConRelUsuarioEmailTableAdapter.Fill(Me.DataSetEric.ConRelUsuarioEmail, Me.Clv_UsuarioTextBox.Text)
                Me.Label1.Visible = True
                Me.EmailTextBox.Visible = True
            Else
                Me.EmailTextBox.Visible = False
                Me.Label1.Visible = False
            End If
            BuscaRelUsuarioGrupoVentas()
            Llena_Plaza()
            Llena_companias()
            llena_dgvcompanias()
            Llena_ciudades()
            llena_dgvciudades()
            Button1.Enabled = False
            Button2.Enabled = False
            btnPlazaAdic.Enabled = True
            ComboBoxCompanias.Enabled = False
            dgvcompania.Enabled = False
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
            If UCase(Me.ComboBox1.Text) = UCase("Control Total") Then
                Me.ConRelUsuarioEmailTableAdapter.Connection = CON
                Me.ConRelUsuarioEmailTableAdapter.Fill(Me.DataSetEric.ConRelUsuarioEmail, Me.Clv_UsuarioTextBox.Text)
                Me.Label1.Visible = True
                Me.EmailTextBox.Visible = True

            Else
                Me.EmailTextBox.Visible = False
                Me.Label1.Visible = False
            End If
            BuscaRelUsuarioGrupoVentas()
            damedatosbitacora()
            Llena_Plaza()
            Llena_companias()
            llena_dgvcompanias()
            Llena_ciudades()
            llena_dgvciudades()
            Button1.Enabled = True
            Button2.Enabled = True
            btnPlazaAdic.Enabled = True
        End If
        CON.Close()
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        If GloTipoUsuario <> 40 And GloTipoUsuario <> 51 And GloTipoUsuario <> 65 Then
            ComboBoxCompanias.Visible = False
            Button1.Visible = False
            dgvcompania.Visible = False
            Button2.Visible = False
            Label3.Visible = False
            Label4.Visible = False
            CheckBoxNotadecredito.Visible = False
        End If
        If GloTipoUsuario <> 40 Then
            cbIdentificacion.Enabled = False
        End If
    End Sub
    Private Sub Llena_Plaza()
        BaseII.limpiaParametros()
        ComboBoxPlaza.DataSource = BaseII.ConsultaDT("Muestra_PlazasFrmUsuario")
        If ComboBoxPlaza.Items.Count <> 0 Then
            ComboBoxPlaza.SelectedIndex = 0
        End If
    End Sub
    Private Sub BuscaRelUsuarioGrupoVentas()
        Dim CON2 As New SqlConnection(MiConexion)
        If opcion = "N" Then
            CON2.Open()
            Me.ConGrupoVentasTableAdapter.Connection = CON2
            Me.ConGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConGrupoVentas, 0, 2, GloClvUsuario)
            CON2.Close()
        ElseIf opcion = "C" Or opcion = "M" Then
            CON2.Open()
            Me.ConRelUsuarioGrupoVentasTableAdapter.Connection = CON2
            Me.ConRelUsuarioGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConRelUsuarioGrupoVentas, Me.Clv_UsuarioTextBox.Text, eClv_Grupo)
            CON2.Close()
            CON2.Open()
            Me.ConGrupoVentasTableAdapter.Connection = CON2
            Me.ConGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConGrupoVentas, CInt(eClv_Grupo), 1, GloClvUsuario)
            CON2.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click, Button8.Click
        Me.Close()
    End Sub

    Private Sub ClaveLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClaveLabel1.Click
        gloClave = Me.ClaveLabel1.Text
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONUSUARIOSBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ASIGNAfeCHA()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            FECHA = Today
            Me.DAMEFECHADELSERVIDORTableAdapter.Connection = CON
            Me.DAMEFECHADELSERVIDORTableAdapter.Fill(Me.NewSofTvDataSet.DAMEFECHADELSERVIDOR, FECHA)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Clv_TipoUsuarioTextBox.Text = Me.ComboBox1.SelectedValue
        If UCase(Me.ComboBox1.Text) = UCase("Control Total") Then
            Me.ConRelUsuarioEmailTableAdapter.Connection = CON
            Me.ConRelUsuarioEmailTableAdapter.Fill(Me.DataSetEric.ConRelUsuarioEmail, Me.Clv_UsuarioTextBox.Text)
            Me.Label1.Visible = True
            Me.EmailTextBox.Visible = True
        Else
            Me.EmailTextBox.Visible = False
            Me.Label1.Visible = False
        End If
        CON.Close()
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        'MsgBox(e.KeyChar & " " & Asc(e.KeyChar) & " " & Asc(LCase(e.KeyChar)))
        e.KeyChar = Chr((ValidaKey(Me.NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub NombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreTextBox.TextChanged

    End Sub

    Private Sub DomicilioTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DomicilioTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.DomicilioTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub DomicilioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DomicilioTextBox.TextChanged

    End Sub

    Private Sub ColoniaTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ColoniaTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.ColoniaTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub ColoniaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ColoniaTextBox.TextChanged

    End Sub


    
    
    
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click, Button1.Click
        If ComboBoxCompanias.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
        BaseII.Inserta("AgregaEliminaRelCompaniaUsuario")
        llena_dgvcompanias()
        Llena_companias()
        Llena_ciudades()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click, Button6.Click
        If dgvcompania.Rows.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, dgvcompania.SelectedCells(0).Value)
        BaseII.Inserta("AgregaEliminaRelCompaniaUsuario")
        llena_dgvcompanias()
        Llena_companias()
        Llena_ciudades()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If ComboBoxCiudad.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, ComboBoxCiudad.SelectedValue)
        BaseII.Inserta("AgregaEliminaRelCiudadUsuario")
        llena_dgvciudades()
        Llena_ciudades()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If dgvciudad.Rows.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, dgvciudad.SelectedCells(0).Value)
        BaseII.Inserta("AgregaEliminaRelCiudadUsuario")
        llena_dgvciudades()
        Llena_ciudades()
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub ComboBoxPlaza_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxPlaza.SelectedIndexChanged
        Try
            Llena_companias()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub

    Private Sub FrmUsuarios_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If opcion = "N" Then
            Exit Sub
        End If
        If dgvcompania.Rows.Count = 0 Then
            e.Cancel = True
            MsgBox("El usuario no se ha relacionado con ninguna compañía, agregue alguna.")
            'Dim conexion As New SqlConnection(MiConexion)
            'Dim comando As New SqlCommand()
            'conexion.Open()
            'comando.Connection = conexion
            'comando.CommandText = "select top 1 id_compania from companias order by id_compania asc"
            'Dim companiadefault As Integer = comando.ExecuteScalar()
            'conexion.Close()
            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            'BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, gloClave)
            'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, companiadefault)
            'BaseII.Inserta("AgregaEliminaRelCompaniaUsuario")
        End If
    End Sub

    Private Sub Clv_TipoUsuarioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TipoUsuarioTextBox.TextChanged
        Try
            If Clv_TipoUsuarioTextBox.Text = 53 Then
                CheckBox3.Checked = False
                CheckBox3.Visible = False
                Label12.Visible = False
            Else
                CheckBox3.Visible = True
                Label12.Visible = True
            End If
        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub CheckBox3_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox3.CheckedChanged

    End Sub

    Private Sub CONUSUARIOSBindingNavigator_RefreshItems(sender As Object, e As EventArgs) Handles CONUSUARIOSBindingNavigator.RefreshItems

    End Sub

    Private Sub Label12_Click(sender As Object, e As EventArgs) Handles Label12.Click

    End Sub

    Private Sub Clv_UsuarioTextBox_TextChanged(sender As Object, e As EventArgs) Handles Clv_UsuarioTextBox.TextChanged

    End Sub

    Private Sub CheckBox4_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBoxRecibeAlerta.CheckedChanged

    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles btnPlazaAdic.Click
        FrmUsuarioPlazaAdic.Show()
    End Sub
End Class