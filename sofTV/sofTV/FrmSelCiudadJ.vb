﻿Imports System.Data.SqlClient
Public Class FrmSelCiudadJ

    Private Sub FrmSelCiudadJ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If EntradasSelCiudad = 0 Then
            EntradasSelCiudad = 1
            Me.Close()
            FrmSelPlaza.Show()
            Exit Sub
        End If
        If EntradasSelCiudad = 1 Then
            EntradasSelCiudad = 0
        End If
        'Por si es uno
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
        BaseII.CreateMyParameter("@numero", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameNumCiudadesUsuario")
        Dim numciudades As Integer = BaseII.dicoPar("@numero")
        If numciudades = 1 Then
            'Dim conexion As New SqlConnection(MiConexion)
            'conexion.Open()
            'Dim comando As New SqlCommand()
            'comando.Connection = conexion
            'comando.CommandText = " exec DameIdentificador"
            'identificador = comando.ExecuteScalar()
            'conexion.Close()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@claveusuario", SqlDbType.Int, GloClvUsuario)
            BaseII.Inserta("InsertaSeleccionCiudadtbl")
            llevamealotro()
            Exit Sub
        End If
        'Fin Por si es uno
        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, GloClvUsuario)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCiudad")
        
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCiudad")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCiudad")
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCiudad")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCiudad")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCiudad")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCiudad")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        bgMovCartera = False
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count > 0 Then
            'If varfrmselcompania = "movimientoscartera" Then
            '    bgMovCartera = True
            '    Me.Close()
            'Else
            '    FrmSelCompaniaCartera.Show()
            '    Me.Close()
            'End If
            'If varfrmselcompania.Equals("cartera") Then
            '    If BndDesPagAde = True Then
            '        BndDesPagAde = False
            '        FrmSelFechaDesPagAde.Show()
            '    Else
            '        'If IdSistema = "AG" Then
            '        '    FrmSelPeriodoCartera.Show()
            '        'Else
            '        'execar = True
            '        'End If
            '        FrmSelPeriodo.Show()
            '    End If
            'End If

            'If varfrmselcompania = "opcion1varios" Then
            '    FrmSelServRep.Show()
            'End If
            'If varfrmselcompania = "normal" Then
            '    FrmSelColoniaJ.Show()
            'ElseIf varfrmselcompania = "hastacolonias" Then
            '    FrmSelColoniaJ.Show()
            'ElseIf varfrmselcompania = "hastacompania1" Then
            '    FrmSelUsuariosE.Show()
            'ElseIf varfrmselcompania = "hastacompania2" Then
            '    FrmSelTrabajosE.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselvendedor" Then
            '    FrmSelVendedor.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
            '    FrmSelUsuario.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
            '    FrmSelUsuariosVentas.Show()
            'ElseIf varfrmselcompania = "hastacompaniasreportefolios" Then
            '    FormReporteFolios.Show()
            'ElseIf varfrmselcompania = "hastacompaniasresventas" Then
            '    FrmSelFechasPPE.Show()
            'ElseIf varfrmselcompania = "hastacompaniaselsucursal" Then
            '    FrmSelSucursal.Show()
            'ElseIf varfrmselcompania = "hastacompaniaimprimircomision" Then
            '    FrmImprimirComision.Show()
            'ElseIf varfrmselcompania = "hastacompaniacumpleanos" Then
            '    FrmRepCumpleanosDeLosClientes.Show()
            'ElseIf varfrmselcompania = "hastacompaniapruebaint" Then
            '    FrmRepPruebaInternet.Show()
            'ElseIf varfrmselcompania = "normalrecordatorios" Then
            '    FrmSelColoniaJ.Show()
            'ElseIf varfrmselcompania = "hastacompaniamensualidades" Then
            '    FrmImprimirComision.Show()
            'ElseIf varfrmselcompania = "hastacompaniareprecontratacion" Then
            '    SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
            '    SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
            '    SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
            '    SoftvMod.VariablesGlobales.MiConexion = MiConexion
            '    SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
            '    SoftvMod.VariablesGlobales.IdCompania = identificador
            '    Dim frm As New SoftvMod.FrmRepRecontratacion
            '    frm.ShowDialog()
            'ElseIf varfrmselcompania = "movimientoscartera" Then
            '    FrmSelCompaniaCartera.Show()
            'ElseIf varfrmselcompania = "pendientesderealizar" Then
            '    FrmImprimirComision.Show()
            'ElseIf varfrmselcompania = "resumenordenes" Then
            '    FrmFiltroReporteResumen.Show()
            'ElseIf varfrmselcompania = "graficassucursal" Then
            '    FrmSelSucursal.Show()

            'ElseIf varfrmselcompania = "graficasventas" Then
            '    FrmSelVendedor.Show()
            'ElseIf varfrmselcompania = "graficasdosopciones" Then
            '    FrmSelSucursal.Show()
            'End If
            FrmSelLocalidad.VieneDeIdentificador = 1
            FrmSelLocalidad.Show()
            Me.Close()
        End If
    End Sub
    Private Sub llevamealotro()
        'If varfrmselcompania = "movimientoscartera" Then
        '    bgMovCartera = True
        '    Me.Close()
        'Else
        '    FrmSelCompaniaCartera.Show()
        '    Me.Close()
        'End If
        'If varfrmselcompania.Equals("cartera") Then
        '    If BndDesPagAde = True Then
        '        BndDesPagAde = False
        '        FrmSelFechaDesPagAde.Show()
        '    Else
        '        'If IdSistema = "AG" Then
        '        '    FrmSelPeriodoCartera.Show()
        '        'Else
        '        'execar = True
        '        'End If
        '        FrmSelPeriodo.Show()
        '    End If
        'End If

        'If varfrmselcompania = "opcion1varios" Then
        '    FrmSelServRep.Show()
        'End If
        'If varfrmselcompania = "normal" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacolonias" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacompania1" Then
        '    FrmSelUsuariosE.Show()
        'ElseIf varfrmselcompania = "hastacompania2" Then
        '    FrmSelTrabajosE.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselvendedor" Then
        '    FrmSelVendedor.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselusuario" Then
        '    FrmSelUsuario.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselusuarioventas" Then
        '    FrmSelUsuariosVentas.Show()
        'ElseIf varfrmselcompania = "hastacompaniasreportefolios" Then
        '    FormReporteFolios.Show()
        'ElseIf varfrmselcompania = "hastacompaniasresventas" Then
        '    FrmSelFechasPPE.Show()
        'ElseIf varfrmselcompania = "hastacompaniaselsucursal" Then
        '    FrmSelSucursal.Show()
        'ElseIf varfrmselcompania = "hastacompaniaimprimircomision" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "hastacompaniacumpleanos" Then
        '    FrmRepCumpleanosDeLosClientes.Show()
        'ElseIf varfrmselcompania = "hastacompaniapruebaint" Then
        '    FrmRepPruebaInternet.Show()
        'ElseIf varfrmselcompania = "normalrecordatorios" Then
        '    FrmSelColoniaJ.Show()
        'ElseIf varfrmselcompania = "hastacompaniamensualidades" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "hastacompaniareprecontratacion" Then
        '    SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        '    SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        '    SoftvMod.VariablesGlobales.GloEmpresa = GloEmpresa
        '    SoftvMod.VariablesGlobales.MiConexion = MiConexion
        '    SoftvMod.VariablesGlobales.RutaReportes = RutaReportes
        '    SoftvMod.VariablesGlobales.IdCompania = identificador
        '    Dim frm As New SoftvMod.FrmRepRecontratacion
        '    frm.Show()
        'ElseIf varfrmselcompania = "movimientoscartera" Then
        '    FrmSelCompaniaCartera.Show()
        'ElseIf varfrmselcompania = "pendientesderealizar" Then
        '    FrmImprimirComision.Show()
        'ElseIf varfrmselcompania = "resumenordenes" Then
        '    FrmFiltroReporteResumen.Show()
        'ElseIf varfrmselcompania = "graficassucursal" Then
        '    FrmSelSucursal.Show()

        'ElseIf varfrmselcompania = "graficasventas" Then
        '    FrmSelVendedor.Show()
        'ElseIf varfrmselcompania = "graficasdosopciones" Then
        '    FrmSelSucursal.Show()
        'End If
        FrmSelLocalidad.VieneDeIdentificador = 1
        FrmSelLocalidad.Show()
        Me.Close()
    End Sub
End Class