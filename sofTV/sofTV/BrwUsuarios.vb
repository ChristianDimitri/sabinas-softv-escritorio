Imports System.Data.SqlClient
Public Class BrwUsuarios
    Private Sub Llena_companias()

        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"


            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmUsuarios.Show()
    End Sub

    Private Sub consultar()
        If IsNumeric(Me.ClaveLabel1.Text) = True Then
            gloClave = Me.ClaveLabel1.Text
        End If
        If gloClave > 0 Then
            opcion = "C"
            gloClave = Me.ClaveLabel1.Text
            FrmUsuarios.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If IsNumeric(Me.ClaveLabel1.Text) = True Then
            gloClave = Me.ClaveLabel1.Text
        End If
        If gloClave > 0 Then
            opcion = "M"
            gloClave = Me.ClaveLabel1.Text

            FrmUsuarios.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            modificar()
        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If bndSuper = False Then
                op = op + 3
            End If
            If op = 0 Then
                If Len(Trim(Me.TextBox1.Text)) > 0 Then
                    Me.BUSCAUSUARIOSTableAdapter.Connection = CON
                    Me.BUSCAUSUARIOSTableAdapter.Fill(Me.DataSetEric.BUSCAUSUARIOS, Me.TextBox1.Text, "", New System.Nullable(Of Integer)(CType(op, Integer)), GloIdCompania)
                Else
                    MsgBox("No se puede realizar la b�squeda con datos en blanco", MsgBoxStyle.Information)
                End If

            ElseIf op = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSCAUSUARIOSTableAdapter.Connection = CON
                    Me.BUSCAUSUARIOSTableAdapter.Fill(Me.DataSetEric.BUSCAUSUARIOS, "", Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(op, Integer)), GloIdCompania)
                Else
                    MsgBox("No se puede realizar la b�squeda con datos en blanco", MsgBoxStyle.Information)
                End If
            Else
                Me.BUSCAUSUARIOSTableAdapter.Connection = CON
                Me.BUSCAUSUARIOSTableAdapter.Fill(Me.DataSetEric.BUSCAUSUARIOS, "", "", New System.Nullable(Of Integer)(CType(op, Integer)), GloIdCompania)
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub BrwUsuarios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Busca(2)
        End If
    End Sub


    Private Sub BrwUsuarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        Llena_companias()
        GloIdCompania = ComboBoxCompanias.SelectedValue
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Busca(2)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub


    Private Sub ClaveLabel1_TextChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClaveLabel1.TextChanged
        If IsNumeric(Me.ClaveLabel1.Text) = True Then
            gloClave = Me.ClaveLabel1.Text
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            Busca(2)
        Catch ex As Exception

        End Try
    End Sub
End Class