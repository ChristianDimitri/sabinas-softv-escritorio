<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMetasCarteras
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMetasCarteras))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.AnioComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraAniosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MesComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraMesesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ConMetasCarterasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConMetasCarterasTableAdapter = New sofTV.DataSetEric2TableAdapters.ConMetasCarterasTableAdapter()
        Me.ConMetasCarterasBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ConMetasCarterasBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ConMetasCarterasDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MuestraMesesTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraMesesTableAdapter()
        Me.MuestraAniosTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraAniosTableAdapter()
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter()
        ConceptoLabel = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraAniosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraMesesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMetasCarterasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMetasCarterasBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConMetasCarterasBindingNavigator.SuspendLayout()
        CType(Me.ConMetasCarterasDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Location = New System.Drawing.Point(32, 32)
        ConceptoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(140, 18)
        ConceptoLabel.TabIndex = 9
        ConceptoLabel.Text = "Tipo de Servicio :"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(595, 626)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 44)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(ConceptoLabel)
        Me.GroupBox1.Controls.Add(Me.ConceptoComboBox)
        Me.GroupBox1.Controls.Add(Me.AnioComboBox)
        Me.GroupBox1.Controls.Add(Me.MesComboBox)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(16, 68)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(760, 107)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Selecciona"
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(36, 54)
        Me.ConceptoComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(260, 26)
        Me.ConceptoComboBox.TabIndex = 1
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'AnioComboBox
        '
        Me.AnioComboBox.DataSource = Me.MuestraAniosBindingSource
        Me.AnioComboBox.DisplayMember = "Anio"
        Me.AnioComboBox.FormattingEnabled = True
        Me.AnioComboBox.Location = New System.Drawing.Point(560, 54)
        Me.AnioComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.AnioComboBox.Name = "AnioComboBox"
        Me.AnioComboBox.Size = New System.Drawing.Size(160, 26)
        Me.AnioComboBox.TabIndex = 3
        Me.AnioComboBox.ValueMember = "Anio"
        '
        'MuestraAniosBindingSource
        '
        Me.MuestraAniosBindingSource.DataMember = "MuestraAnios"
        Me.MuestraAniosBindingSource.DataSource = Me.DataSetEric2
        '
        'MesComboBox
        '
        Me.MesComboBox.DataSource = Me.MuestraMesesBindingSource
        Me.MesComboBox.DisplayMember = "Mes"
        Me.MesComboBox.FormattingEnabled = True
        Me.MesComboBox.Location = New System.Drawing.Point(359, 54)
        Me.MesComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MesComboBox.Name = "MesComboBox"
        Me.MesComboBox.Size = New System.Drawing.Size(160, 26)
        Me.MesComboBox.TabIndex = 2
        Me.MesComboBox.ValueMember = "Clv_Mes"
        '
        'MuestraMesesBindingSource
        '
        Me.MuestraMesesBindingSource.DataMember = "MuestraMeses"
        Me.MuestraMesesBindingSource.DataSource = Me.DataSetEric2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(556, 32)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 18)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Año :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(355, 32)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 18)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Mes :"
        '
        'ConMetasCarterasBindingSource
        '
        Me.ConMetasCarterasBindingSource.DataMember = "ConMetasCarteras"
        Me.ConMetasCarterasBindingSource.DataSource = Me.DataSetEric2
        '
        'ConMetasCarterasTableAdapter
        '
        Me.ConMetasCarterasTableAdapter.ClearBeforeFill = True
        '
        'ConMetasCarterasBindingNavigator
        '
        Me.ConMetasCarterasBindingNavigator.AddNewItem = Nothing
        Me.ConMetasCarterasBindingNavigator.BindingSource = Me.ConMetasCarterasBindingSource
        Me.ConMetasCarterasBindingNavigator.CountItem = Nothing
        Me.ConMetasCarterasBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ConMetasCarterasBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConMetasCarterasBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ConMetasCarterasBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConMetasCarterasBindingNavigatorSaveItem})
        Me.ConMetasCarterasBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConMetasCarterasBindingNavigator.MoveFirstItem = Nothing
        Me.ConMetasCarterasBindingNavigator.MoveLastItem = Nothing
        Me.ConMetasCarterasBindingNavigator.MoveNextItem = Nothing
        Me.ConMetasCarterasBindingNavigator.MovePreviousItem = Nothing
        Me.ConMetasCarterasBindingNavigator.Name = "ConMetasCarterasBindingNavigator"
        Me.ConMetasCarterasBindingNavigator.PositionItem = Nothing
        Me.ConMetasCarterasBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConMetasCarterasBindingNavigator.Size = New System.Drawing.Size(803, 27)
        Me.ConMetasCarterasBindingNavigator.TabIndex = 5
        Me.ConMetasCarterasBindingNavigator.TabStop = True
        Me.ConMetasCarterasBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(177, 24)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINA MEDIDOR"
        '
        'ConMetasCarterasBindingNavigatorSaveItem
        '
        Me.ConMetasCarterasBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConMetasCarterasBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConMetasCarterasBindingNavigatorSaveItem.Name = "ConMetasCarterasBindingNavigatorSaveItem"
        Me.ConMetasCarterasBindingNavigatorSaveItem.Size = New System.Drawing.Size(174, 24)
        Me.ConMetasCarterasBindingNavigatorSaveItem.Text = "&GUARDA MEDIDOR"
        '
        'ConMetasCarterasDataGridView
        '
        Me.ConMetasCarterasDataGridView.AutoGenerateColumns = False
        Me.ConMetasCarterasDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConMetasCarterasDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConMetasCarterasDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8})
        Me.ConMetasCarterasDataGridView.DataSource = Me.ConMetasCarterasBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConMetasCarterasDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConMetasCarterasDataGridView.Location = New System.Drawing.Point(16, 213)
        Me.ConMetasCarterasDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ConMetasCarterasDataGridView.Name = "ConMetasCarterasDataGridView"
        Me.ConMetasCarterasDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.ConMetasCarterasDataGridView.Size = New System.Drawing.Size(760, 385)
        Me.ConMetasCarterasDataGridView.TabIndex = 4
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_Concepto"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clv_Concepto"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_TipSer"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_TipSer"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Concepto"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Concepto"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 220
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "TipSer"
        Me.DataGridViewTextBoxColumn4.HeaderText = "TipSer"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Mes"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Mes"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Anio"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Anio"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Cantidad"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Cantidad"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Fecha"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 150
        '
        'MuestraMesesTableAdapter
        '
        Me.MuestraMesesTableAdapter.ClearBeforeFill = True
        '
        'MuestraAniosTableAdapter
        '
        Me.MuestraAniosTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'FrmMetasCarteras
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(803, 695)
        Me.Controls.Add(Me.ConMetasCarterasDataGridView)
        Me.Controls.Add(Me.ConMetasCarterasBindingNavigator)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmMetasCarteras"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Medidor de Cartera"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraAniosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraMesesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMetasCarterasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMetasCarterasBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConMetasCarterasBindingNavigator.ResumeLayout(False)
        Me.ConMetasCarterasBindingNavigator.PerformLayout()
        CType(Me.ConMetasCarterasDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label

    'Private Sub ConMetasCarterasBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConMetasCarterasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConMetasCarterasTableAdapter As sofTV.DataSetEric2TableAdapters.ConMetasCarterasTableAdapter
    Friend WithEvents ConMetasCarterasBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMetasCarterasBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMetasCarterasDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents MuestraMesesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraMesesTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraMesesTableAdapter
    Friend WithEvents MesComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraAniosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraAniosTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraAniosTableAdapter
    Friend WithEvents AnioComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
