﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConceptosPoliza2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tpDebe = New System.Windows.Forms.TabPage()
        Me.btnAgregarDebe = New System.Windows.Forms.Button()
        Me.tbCuentaDebe = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBoxPlazas = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBoxSucursales = New System.Windows.Forms.ComboBox()
        Me.tpHaber = New System.Windows.Forms.TabPage()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnAgregarHaber = New System.Windows.Forms.Button()
        Me.tbCuentaHaber = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ComboBoxConcepto = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ComboBoxServicio = New System.Windows.Forms.ComboBox()
        Me.tpImpuestos = New System.Windows.Forms.TabPage()
        Me.ComboBoxImpuesto = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbDescripcionImpuestos = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnAgregarImpuestos = New System.Windows.Forms.Button()
        Me.tbCuentaImpuestos = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tpOtrasSucursales = New System.Windows.Forms.TabPage()
        Me.btnAgregarSucursalOtros = New System.Windows.Forms.Button()
        Me.tbCuentaSucursalOtros = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ComboBoxSucursalOtros = New System.Windows.Forms.ComboBox()
        Me.dgvConceptos = New System.Windows.Forms.DataGridView()
        Me.Clv_Conceptos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Plaza = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cuenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.tpDebe.SuspendLayout()
        Me.tpHaber.SuspendLayout()
        Me.tpImpuestos.SuspendLayout()
        Me.tpOtrasSucursales.SuspendLayout()
        CType(Me.dgvConceptos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 11)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(197, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cuentas Contables"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tpDebe)
        Me.TabControl1.Controls.Add(Me.tpHaber)
        Me.TabControl1.Controls.Add(Me.tpImpuestos)
        Me.TabControl1.Controls.Add(Me.tpOtrasSucursales)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(16, 52)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(705, 244)
        Me.TabControl1.TabIndex = 1
        '
        'tpDebe
        '
        Me.tpDebe.Controls.Add(Me.btnAgregarDebe)
        Me.tpDebe.Controls.Add(Me.tbCuentaDebe)
        Me.tpDebe.Controls.Add(Me.Label4)
        Me.tpDebe.Controls.Add(Me.Label3)
        Me.tpDebe.Controls.Add(Me.ComboBoxPlazas)
        Me.tpDebe.Controls.Add(Me.Label2)
        Me.tpDebe.Controls.Add(Me.ComboBoxSucursales)
        Me.tpDebe.Location = New System.Drawing.Point(4, 29)
        Me.tpDebe.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tpDebe.Name = "tpDebe"
        Me.tpDebe.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tpDebe.Size = New System.Drawing.Size(697, 211)
        Me.tpDebe.TabIndex = 0
        Me.tpDebe.Text = "Cuentas Debe"
        Me.tpDebe.UseVisualStyleBackColor = True
        '
        'btnAgregarDebe
        '
        Me.btnAgregarDebe.Location = New System.Drawing.Point(424, 129)
        Me.btnAgregarDebe.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregarDebe.Name = "btnAgregarDebe"
        Me.btnAgregarDebe.Size = New System.Drawing.Size(100, 33)
        Me.btnAgregarDebe.TabIndex = 6
        Me.btnAgregarDebe.Text = "Agregar"
        Me.btnAgregarDebe.UseVisualStyleBackColor = True
        '
        'tbCuentaDebe
        '
        Me.tbCuentaDebe.Location = New System.Drawing.Point(176, 95)
        Me.tbCuentaDebe.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCuentaDebe.Name = "tbCuentaDebe"
        Me.tbCuentaDebe.Size = New System.Drawing.Size(347, 26)
        Me.tbCuentaDebe.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 105)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 20)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Cuenta:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 31)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(101, 20)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Distribuidor:"
        '
        'ComboBoxPlazas
        '
        Me.ComboBoxPlazas.DisplayMember = "Nombre"
        Me.ComboBoxPlazas.FormattingEnabled = True
        Me.ComboBoxPlazas.Location = New System.Drawing.Point(176, 21)
        Me.ComboBoxPlazas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxPlazas.Name = "ComboBoxPlazas"
        Me.ComboBoxPlazas.Size = New System.Drawing.Size(347, 28)
        Me.ComboBoxPlazas.TabIndex = 2
        Me.ComboBoxPlazas.ValueMember = "clv_plaza"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 68)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Sucursal:"
        '
        'ComboBoxSucursales
        '
        Me.ComboBoxSucursales.DisplayMember = "Nombre"
        Me.ComboBoxSucursales.FormattingEnabled = True
        Me.ComboBoxSucursales.Location = New System.Drawing.Point(176, 58)
        Me.ComboBoxSucursales.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxSucursales.Name = "ComboBoxSucursales"
        Me.ComboBoxSucursales.Size = New System.Drawing.Size(347, 28)
        Me.ComboBoxSucursales.TabIndex = 0
        Me.ComboBoxSucursales.ValueMember = "Clv_Sucursal"
        '
        'tpHaber
        '
        Me.tpHaber.Controls.Add(Me.Label8)
        Me.tpHaber.Controls.Add(Me.btnAgregarHaber)
        Me.tpHaber.Controls.Add(Me.tbCuentaHaber)
        Me.tpHaber.Controls.Add(Me.Label7)
        Me.tpHaber.Controls.Add(Me.Label6)
        Me.tpHaber.Controls.Add(Me.ComboBoxConcepto)
        Me.tpHaber.Controls.Add(Me.Label5)
        Me.tpHaber.Controls.Add(Me.ComboBoxServicio)
        Me.tpHaber.Location = New System.Drawing.Point(4, 29)
        Me.tpHaber.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tpHaber.Name = "tpHaber"
        Me.tpHaber.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tpHaber.Size = New System.Drawing.Size(697, 211)
        Me.tpHaber.TabIndex = 1
        Me.tpHaber.Text = "Cuentas Haber"
        Me.tpHaber.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(415, 102)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(81, 20)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "-XXX-099"
        '
        'btnAgregarHaber
        '
        Me.btnAgregarHaber.Location = New System.Drawing.Point(436, 133)
        Me.btnAgregarHaber.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregarHaber.Name = "btnAgregarHaber"
        Me.btnAgregarHaber.Size = New System.Drawing.Size(100, 33)
        Me.btnAgregarHaber.TabIndex = 8
        Me.btnAgregarHaber.Text = "Agregar"
        Me.btnAgregarHaber.UseVisualStyleBackColor = True
        '
        'tbCuentaHaber
        '
        Me.tbCuentaHaber.Location = New System.Drawing.Point(135, 98)
        Me.tbCuentaHaber.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCuentaHaber.Name = "tbCuentaHaber"
        Me.tbCuentaHaber.Size = New System.Drawing.Size(275, 26)
        Me.tbCuentaHaber.TabIndex = 7
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(27, 106)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(67, 20)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Cuenta:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(27, 71)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 20)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Concepto:"
        '
        'ComboBoxConcepto
        '
        Me.ComboBoxConcepto.DisplayMember = "Descripcion"
        Me.ComboBoxConcepto.FormattingEnabled = True
        Me.ComboBoxConcepto.Location = New System.Drawing.Point(135, 62)
        Me.ComboBoxConcepto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxConcepto.Name = "ComboBoxConcepto"
        Me.ComboBoxConcepto.Size = New System.Drawing.Size(400, 28)
        Me.ComboBoxConcepto.TabIndex = 2
        Me.ComboBoxConcepto.ValueMember = "Clave"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(27, 34)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 20)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Servicio:"
        '
        'ComboBoxServicio
        '
        Me.ComboBoxServicio.DisplayMember = "Descripcion"
        Me.ComboBoxServicio.FormattingEnabled = True
        Me.ComboBoxServicio.Location = New System.Drawing.Point(135, 25)
        Me.ComboBoxServicio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxServicio.Name = "ComboBoxServicio"
        Me.ComboBoxServicio.Size = New System.Drawing.Size(400, 28)
        Me.ComboBoxServicio.TabIndex = 0
        Me.ComboBoxServicio.ValueMember = "Clv_Servicio"
        '
        'tpImpuestos
        '
        Me.tpImpuestos.Controls.Add(Me.ComboBoxImpuesto)
        Me.tpImpuestos.Controls.Add(Me.Label11)
        Me.tpImpuestos.Controls.Add(Me.tbDescripcionImpuestos)
        Me.tpImpuestos.Controls.Add(Me.Label10)
        Me.tpImpuestos.Controls.Add(Me.btnAgregarImpuestos)
        Me.tpImpuestos.Controls.Add(Me.tbCuentaImpuestos)
        Me.tpImpuestos.Controls.Add(Me.Label9)
        Me.tpImpuestos.Location = New System.Drawing.Point(4, 29)
        Me.tpImpuestos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tpImpuestos.Name = "tpImpuestos"
        Me.tpImpuestos.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tpImpuestos.Size = New System.Drawing.Size(697, 211)
        Me.tpImpuestos.TabIndex = 2
        Me.tpImpuestos.Text = "Cuentas Impuestos"
        Me.tpImpuestos.UseVisualStyleBackColor = True
        '
        'ComboBoxImpuesto
        '
        Me.ComboBoxImpuesto.FormattingEnabled = True
        Me.ComboBoxImpuesto.Items.AddRange(New Object() {"IVA", "IEPS"})
        Me.ComboBoxImpuesto.Location = New System.Drawing.Point(137, 17)
        Me.ComboBoxImpuesto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxImpuesto.Name = "ComboBoxImpuesto"
        Me.ComboBoxImpuesto.Size = New System.Drawing.Size(363, 28)
        Me.ComboBoxImpuesto.TabIndex = 14
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(29, 27)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(46, 20)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Tipo:"
        '
        'tbDescripcionImpuestos
        '
        Me.tbDescripcionImpuestos.Location = New System.Drawing.Point(137, 54)
        Me.tbDescripcionImpuestos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbDescripcionImpuestos.Name = "tbDescripcionImpuestos"
        Me.tbDescripcionImpuestos.Size = New System.Drawing.Size(363, 26)
        Me.tbDescripcionImpuestos.TabIndex = 12
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(29, 62)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(104, 20)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "Descripción:"
        '
        'btnAgregarImpuestos
        '
        Me.btnAgregarImpuestos.Location = New System.Drawing.Point(401, 129)
        Me.btnAgregarImpuestos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregarImpuestos.Name = "btnAgregarImpuestos"
        Me.btnAgregarImpuestos.Size = New System.Drawing.Size(100, 33)
        Me.btnAgregarImpuestos.TabIndex = 10
        Me.btnAgregarImpuestos.Text = "Agregar"
        Me.btnAgregarImpuestos.UseVisualStyleBackColor = True
        '
        'tbCuentaImpuestos
        '
        Me.tbCuentaImpuestos.Location = New System.Drawing.Point(137, 95)
        Me.tbCuentaImpuestos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCuentaImpuestos.Name = "tbCuentaImpuestos"
        Me.tbCuentaImpuestos.Size = New System.Drawing.Size(363, 26)
        Me.tbCuentaImpuestos.TabIndex = 9
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(29, 98)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 20)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Cuenta:"
        '
        'tpOtrasSucursales
        '
        Me.tpOtrasSucursales.Controls.Add(Me.btnAgregarSucursalOtros)
        Me.tpOtrasSucursales.Controls.Add(Me.tbCuentaSucursalOtros)
        Me.tpOtrasSucursales.Controls.Add(Me.Label12)
        Me.tpOtrasSucursales.Controls.Add(Me.Label13)
        Me.tpOtrasSucursales.Controls.Add(Me.ComboBoxSucursalOtros)
        Me.tpOtrasSucursales.Location = New System.Drawing.Point(4, 29)
        Me.tpOtrasSucursales.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tpOtrasSucursales.Name = "tpOtrasSucursales"
        Me.tpOtrasSucursales.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tpOtrasSucursales.Size = New System.Drawing.Size(697, 211)
        Me.tpOtrasSucursales.TabIndex = 3
        Me.tpOtrasSucursales.Text = "Cuentas Otras Sucursales"
        Me.tpOtrasSucursales.UseVisualStyleBackColor = True
        '
        'btnAgregarSucursalOtros
        '
        Me.btnAgregarSucursalOtros.Location = New System.Drawing.Point(436, 95)
        Me.btnAgregarSucursalOtros.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregarSucursalOtros.Name = "btnAgregarSucursalOtros"
        Me.btnAgregarSucursalOtros.Size = New System.Drawing.Size(100, 33)
        Me.btnAgregarSucursalOtros.TabIndex = 11
        Me.btnAgregarSucursalOtros.Text = "Agregar"
        Me.btnAgregarSucursalOtros.UseVisualStyleBackColor = True
        '
        'tbCuentaSucursalOtros
        '
        Me.tbCuentaSucursalOtros.Location = New System.Drawing.Point(188, 60)
        Me.tbCuentaSucursalOtros.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCuentaSucursalOtros.Name = "tbCuentaSucursalOtros"
        Me.tbCuentaSucursalOtros.Size = New System.Drawing.Size(347, 26)
        Me.tbCuentaSucursalOtros.TabIndex = 10
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(25, 70)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 20)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "Cuenta:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(25, 33)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 20)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "Sucursal:"
        '
        'ComboBoxSucursalOtros
        '
        Me.ComboBoxSucursalOtros.DisplayMember = "Nombre"
        Me.ComboBoxSucursalOtros.FormattingEnabled = True
        Me.ComboBoxSucursalOtros.Location = New System.Drawing.Point(188, 23)
        Me.ComboBoxSucursalOtros.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxSucursalOtros.Name = "ComboBoxSucursalOtros"
        Me.ComboBoxSucursalOtros.Size = New System.Drawing.Size(347, 28)
        Me.ComboBoxSucursalOtros.TabIndex = 7
        Me.ComboBoxSucursalOtros.ValueMember = "Clv_Sucursal"
        '
        'dgvConceptos
        '
        Me.dgvConceptos.AllowUserToAddRows = False
        Me.dgvConceptos.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvConceptos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvConceptos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvConceptos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Conceptos, Me.Clv_Plaza, Me.Descripcion, Me.Cuenta, Me.Clave})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvConceptos.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvConceptos.Location = New System.Drawing.Point(16, 303)
        Me.dgvConceptos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvConceptos.Name = "dgvConceptos"
        Me.dgvConceptos.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvConceptos.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvConceptos.RowHeadersVisible = False
        Me.dgvConceptos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvConceptos.Size = New System.Drawing.Size(813, 356)
        Me.dgvConceptos.TabIndex = 2
        '
        'Clv_Conceptos
        '
        Me.Clv_Conceptos.DataPropertyName = "Clv_Concepto"
        Me.Clv_Conceptos.HeaderText = "Clv_Concepto"
        Me.Clv_Conceptos.Name = "Clv_Conceptos"
        Me.Clv_Conceptos.ReadOnly = True
        Me.Clv_Conceptos.Visible = False
        '
        'Clv_Plaza
        '
        Me.Clv_Plaza.DataPropertyName = "Clv_Plaza"
        Me.Clv_Plaza.HeaderText = "Clv_Plaza"
        Me.Clv_Plaza.Name = "Clv_Plaza"
        Me.Clv_Plaza.ReadOnly = True
        Me.Clv_Plaza.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'Cuenta
        '
        Me.Cuenta.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Cuenta.DataPropertyName = "Cuenta"
        Me.Cuenta.HeaderText = "Cuenta"
        Me.Cuenta.Name = "Cuenta"
        Me.Cuenta.ReadOnly = True
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "Clave"
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.ReadOnly = True
        Me.Clave.Visible = False
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(729, 262)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(100, 33)
        Me.btnEliminar.TabIndex = 7
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(729, 666)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(100, 33)
        Me.btnSalir.TabIndex = 8
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'FrmConceptosPoliza2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(849, 714)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.dgvConceptos)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmConceptosPoliza2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cuentas Contables para Pólizas"
        Me.TabControl1.ResumeLayout(False)
        Me.tpDebe.ResumeLayout(False)
        Me.tpDebe.PerformLayout()
        Me.tpHaber.ResumeLayout(False)
        Me.tpHaber.PerformLayout()
        Me.tpImpuestos.ResumeLayout(False)
        Me.tpImpuestos.PerformLayout()
        Me.tpOtrasSucursales.ResumeLayout(False)
        Me.tpOtrasSucursales.PerformLayout()
        CType(Me.dgvConceptos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpDebe As System.Windows.Forms.TabPage
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxPlazas As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxSucursales As System.Windows.Forms.ComboBox
    Friend WithEvents tpHaber As System.Windows.Forms.TabPage
    Friend WithEvents dgvConceptos As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregarDebe As System.Windows.Forms.Button
    Friend WithEvents tbCuentaDebe As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Clv_Conceptos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Plaza As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cuenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAgregarHaber As System.Windows.Forms.Button
    Friend WithEvents tbCuentaHaber As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxConcepto As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxServicio As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tpImpuestos As System.Windows.Forms.TabPage
    Friend WithEvents ComboBoxImpuesto As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbDescripcionImpuestos As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnAgregarImpuestos As System.Windows.Forms.Button
    Friend WithEvents tbCuentaImpuestos As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tpOtrasSucursales As System.Windows.Forms.TabPage
    Friend WithEvents btnAgregarSucursalOtros As System.Windows.Forms.Button
    Friend WithEvents tbCuentaSucursalOtros As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxSucursalOtros As System.Windows.Forms.ComboBox
End Class
