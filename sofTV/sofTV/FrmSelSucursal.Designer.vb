<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelSucursal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.Dame_clv_session_ReportesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_session_ReportesTableAdapter = New sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter()
        Me.ConSucursalesProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSucursalesProTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSucursalesProTableAdapter()
        Me.NombreListBox = New System.Windows.Forms.ListBox()
        Me.ConSucursalesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSucursalesTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSucursalesTmpTableAdapter()
        Me.NombreListBox1 = New System.Windows.Forms.ListBox()
        Me.InsertarSucursalesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarSucursalesTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarSucursalesTmpTableAdapter()
        Me.BorrarSucursalesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarSucursalesTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.BorrarSucursalesTmpTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Aceptar = New System.Windows.Forms.Button()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSucursalesProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_clv_session_ReportesBindingSource
        '
        Me.Dame_clv_session_ReportesBindingSource.DataMember = "Dame_clv_session_Reportes"
        Me.Dame_clv_session_ReportesBindingSource.DataSource = Me.DataSetEric2
        '
        'Dame_clv_session_ReportesTableAdapter
        '
        Me.Dame_clv_session_ReportesTableAdapter.ClearBeforeFill = True
        '
        'ConSucursalesProBindingSource
        '
        Me.ConSucursalesProBindingSource.DataMember = "ConSucursalesPro"
        Me.ConSucursalesProBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSucursalesProTableAdapter
        '
        Me.ConSucursalesProTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox
        '
        Me.NombreListBox.DataSource = Me.ConSucursalesProBindingSource
        Me.NombreListBox.DisplayMember = "Nombre"
        Me.NombreListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreListBox.FormattingEnabled = True
        Me.NombreListBox.ItemHeight = 20
        Me.NombreListBox.Location = New System.Drawing.Point(57, 47)
        Me.NombreListBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreListBox.Name = "NombreListBox"
        Me.NombreListBox.Size = New System.Drawing.Size(367, 384)
        Me.NombreListBox.TabIndex = 2
        Me.NombreListBox.ValueMember = "Clv_Sucursal"
        '
        'ConSucursalesTmpBindingSource
        '
        Me.ConSucursalesTmpBindingSource.DataMember = "ConSucursalesTmp"
        Me.ConSucursalesTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSucursalesTmpTableAdapter
        '
        Me.ConSucursalesTmpTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox1
        '
        Me.NombreListBox1.DataSource = Me.ConSucursalesTmpBindingSource
        Me.NombreListBox1.DisplayMember = "Nombre"
        Me.NombreListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreListBox1.FormattingEnabled = True
        Me.NombreListBox1.ItemHeight = 20
        Me.NombreListBox1.Location = New System.Drawing.Point(575, 47)
        Me.NombreListBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreListBox1.Name = "NombreListBox1"
        Me.NombreListBox1.Size = New System.Drawing.Size(367, 384)
        Me.NombreListBox1.TabIndex = 4
        Me.NombreListBox1.ValueMember = "Clv_Sucursal"
        '
        'InsertarSucursalesTmpBindingSource
        '
        Me.InsertarSucursalesTmpBindingSource.DataMember = "InsertarSucursalesTmp"
        Me.InsertarSucursalesTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarSucursalesTmpTableAdapter
        '
        Me.InsertarSucursalesTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarSucursalesTmpBindingSource
        '
        Me.BorrarSucursalesTmpBindingSource.DataMember = "BorrarSucursalesTmp"
        Me.BorrarSucursalesTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'BorrarSucursalesTmpTableAdapter
        '
        Me.BorrarSucursalesTmpTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(459, 119)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 31)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(459, 158)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(85, 31)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(459, 309)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(85, 31)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(459, 347)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(85, 31)
        Me.Button4.TabIndex = 8
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Aceptar
        '
        Me.Aceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Aceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Aceptar.Location = New System.Drawing.Point(848, 489)
        Me.Aceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Aceptar.Name = "Aceptar"
        Me.Aceptar.Size = New System.Drawing.Size(181, 44)
        Me.Aceptar.TabIndex = 9
        Me.Aceptar.Text = "&ACEPTAR"
        Me.Aceptar.UseVisualStyleBackColor = True
        '
        'FrmSelSucursal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1045, 548)
        Me.Controls.Add(Me.Aceptar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.NombreListBox1)
        Me.Controls.Add(Me.NombreListBox)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelSucursal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Sucursal(es)"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSucursalesProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents Dame_clv_session_ReportesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_session_ReportesTableAdapter As sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter
    Friend WithEvents ConSucursalesProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSucursalesProTableAdapter As sofTV.DataSetEric2TableAdapters.ConSucursalesProTableAdapter
    Friend WithEvents NombreListBox As System.Windows.Forms.ListBox
    Friend WithEvents ConSucursalesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSucursalesTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConSucursalesTmpTableAdapter
    Friend WithEvents NombreListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents InsertarSucursalesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarSucursalesTmpTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarSucursalesTmpTableAdapter
    Friend WithEvents BorrarSucursalesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarSucursalesTmpTableAdapter As sofTV.DataSetEric2TableAdapters.BorrarSucursalesTmpTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Aceptar As System.Windows.Forms.Button
End Class
