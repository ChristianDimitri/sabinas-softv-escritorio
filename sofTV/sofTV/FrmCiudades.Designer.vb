<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCiudades
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_CiudadLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCiudades))
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CONCIUDADESBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONCIUDADESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.CONCIUDADESBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ComboBoxEstados = New System.Windows.Forms.ComboBox()
        Me.Clv_CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.CONCIUDADESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCIUDADESTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgvcompania = New System.Windows.Forms.DataGridView()
        Me.IdCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Razon_Social = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComboBoxEstado = New System.Windows.Forms.ComboBox()
        Clv_CiudadLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.CONCIUDADESBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCIUDADESBindingNavigator.SuspendLayout()
        CType(Me.CONCIUDADESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvcompania, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_CiudadLabel
        '
        Clv_CiudadLabel.AutoSize = True
        Clv_CiudadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CiudadLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_CiudadLabel.Location = New System.Drawing.Point(120, 64)
        Clv_CiudadLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_CiudadLabel.Name = "Clv_CiudadLabel"
        Clv_CiudadLabel.Size = New System.Drawing.Size(65, 18)
        Clv_CiudadLabel.TabIndex = 0
        Clv_CiudadLabel.Text = "Clave : "
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(104, 107)
        NombreLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(78, 18)
        NombreLabel.TabIndex = 2
        NombreLabel.Text = "Nombre :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(56, 62)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(71, 18)
        Label1.TabIndex = 101
        Label1.Text = "Estado :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(104, 156)
        Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(80, 18)
        Label2.TabIndex = 105
        Label2.Text = "Estados :"
        Label2.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(661, 378)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 41)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Controls.Add(Me.CONCIUDADESBindingNavigator)
        Me.Panel1.Controls.Add(Me.ComboBoxEstados)
        Me.Panel1.Controls.Add(Clv_CiudadLabel)
        Me.Panel1.Controls.Add(Me.Clv_CiudadTextBox)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(884, 159)
        Me.Panel1.TabIndex = 18
        '
        'CONCIUDADESBindingNavigator
        '
        Me.CONCIUDADESBindingNavigator.AddNewItem = Nothing
        Me.CONCIUDADESBindingNavigator.BindingSource = Me.CONCIUDADESBindingSource
        Me.CONCIUDADESBindingNavigator.CountItem = Nothing
        Me.CONCIUDADESBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONCIUDADESBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CONCIUDADESBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorSeparator2, Me.BindingNavigatorDeleteItem, Me.CONCIUDADESBindingNavigatorSaveItem})
        Me.CONCIUDADESBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCIUDADESBindingNavigator.MoveFirstItem = Nothing
        Me.CONCIUDADESBindingNavigator.MoveLastItem = Nothing
        Me.CONCIUDADESBindingNavigator.MoveNextItem = Nothing
        Me.CONCIUDADESBindingNavigator.MovePreviousItem = Nothing
        Me.CONCIUDADESBindingNavigator.Name = "CONCIUDADESBindingNavigator"
        Me.CONCIUDADESBindingNavigator.PositionItem = Nothing
        Me.CONCIUDADESBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONCIUDADESBindingNavigator.Size = New System.Drawing.Size(884, 28)
        Me.CONCIUDADESBindingNavigator.TabIndex = 1
        Me.CONCIUDADESBindingNavigator.TabStop = True
        Me.CONCIUDADESBindingNavigator.Text = "BindingNavigator1"
        '
        'CONCIUDADESBindingSource
        '
        Me.CONCIUDADESBindingSource.DataMember = "CONCIUDADES"
        Me.CONCIUDADESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(122, 25)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(105, 25)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 28)
        '
        'CONCIUDADESBindingNavigatorSaveItem
        '
        Me.CONCIUDADESBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCIUDADESBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCIUDADESBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCIUDADESBindingNavigatorSaveItem.Name = "CONCIUDADESBindingNavigatorSaveItem"
        Me.CONCIUDADESBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 25)
        Me.CONCIUDADESBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ComboBoxEstados
        '
        Me.ComboBoxEstados.DisplayMember = "Nombre"
        Me.ComboBoxEstados.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxEstados.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxEstados.FormattingEnabled = True
        Me.ComboBoxEstados.Location = New System.Drawing.Point(200, 143)
        Me.ComboBoxEstados.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxEstados.Name = "ComboBoxEstados"
        Me.ComboBoxEstados.Size = New System.Drawing.Size(436, 30)
        Me.ComboBoxEstados.TabIndex = 104
        Me.ComboBoxEstados.ValueMember = "Clv_Estado"
        Me.ComboBoxEstados.Visible = False
        '
        'Clv_CiudadTextBox
        '
        Me.Clv_CiudadTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_CiudadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCIUDADESBindingSource, "Clv_Ciudad", True))
        Me.Clv_CiudadTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CiudadTextBox.Location = New System.Drawing.Point(200, 62)
        Me.Clv_CiudadTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_CiudadTextBox.Name = "Clv_CiudadTextBox"
        Me.Clv_CiudadTextBox.ReadOnly = True
        Me.Clv_CiudadTextBox.Size = New System.Drawing.Size(133, 24)
        Me.Clv_CiudadTextBox.TabIndex = 1
        Me.Clv_CiudadTextBox.TabStop = False
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCIUDADESBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(200, 105)
        Me.NombreTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(475, 24)
        Me.NombreTextBox.TabIndex = 0
        '
        'CONCIUDADESTableAdapter
        '
        Me.CONCIUDADESTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(Me.dgvcompania)
        Me.GroupBox1.Controls.Add(Label1)
        Me.GroupBox1.Controls.Add(Me.ComboBoxEstado)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 176)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(851, 426)
        Me.GroupBox1.TabIndex = 19
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Relación Estado - Ciudad"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(608, 110)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 41)
        Me.Button2.TabIndex = 103
        Me.Button2.Text = "ELIMINAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(608, 42)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 41)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "AGREGAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'dgvcompania
        '
        Me.dgvcompania.AllowUserToAddRows = False
        Me.dgvcompania.AllowUserToDeleteRows = False
        Me.dgvcompania.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvcompania.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdCompania, Me.Razon_Social})
        Me.dgvcompania.Location = New System.Drawing.Point(108, 110)
        Me.dgvcompania.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvcompania.Name = "dgvcompania"
        Me.dgvcompania.ReadOnly = True
        Me.dgvcompania.RowHeadersVisible = False
        Me.dgvcompania.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvcompania.Size = New System.Drawing.Size(492, 274)
        Me.dgvcompania.TabIndex = 102
        '
        'IdCompania
        '
        Me.IdCompania.DataPropertyName = "Clv_Estado"
        Me.IdCompania.HeaderText = "idcompania"
        Me.IdCompania.Name = "IdCompania"
        Me.IdCompania.ReadOnly = True
        Me.IdCompania.Visible = False
        '
        'Razon_Social
        '
        Me.Razon_Social.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Razon_Social.DataPropertyName = "Nombre"
        Me.Razon_Social.HeaderText = "Nombre"
        Me.Razon_Social.Name = "Razon_Social"
        Me.Razon_Social.ReadOnly = True
        '
        'ComboBoxEstado
        '
        Me.ComboBoxEstado.DisplayMember = "Nombre"
        Me.ComboBoxEstado.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxEstado.FormattingEnabled = True
        Me.ComboBoxEstado.Location = New System.Drawing.Point(143, 48)
        Me.ComboBoxEstado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxEstado.Name = "ComboBoxEstado"
        Me.ComboBoxEstado.Size = New System.Drawing.Size(436, 30)
        Me.ComboBoxEstado.TabIndex = 100
        Me.ComboBoxEstado.ValueMember = "Clv_Estado"
        '
        'FrmCiudades
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(884, 625)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmCiudades"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Ciudades"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONCIUDADESBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCIUDADESBindingNavigator.ResumeLayout(False)
        Me.CONCIUDADESBindingNavigator.PerformLayout()
        CType(Me.CONCIUDADESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvcompania, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONCIUDADESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCIUDADESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCIUDADESTableAdapter
    Friend WithEvents Clv_CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONCIUDADESBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CONCIUDADESBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents dgvcompania As System.Windows.Forms.DataGridView
    Friend WithEvents ComboBoxEstado As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxEstados As System.Windows.Forms.ComboBox
    Friend WithEvents IdCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Razon_Social As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
