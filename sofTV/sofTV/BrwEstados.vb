﻿Public Class BrwEstados

    Private Sub BrwEstados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        busca(0, "")
        NombreTextBox.BackColor = Button1.BackColor
        NombreTextBox.ForeColor = Button1.ForeColor
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
    Private Sub busca(ByVal opcion As Integer, ByVal nombre As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, opcion)
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, nombre)
        DataGridView1.DataSource = BaseII.ConsultaDT("BuscaEstados")
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            If DataGridView1.Rows.Count > 0 Then
                Clv_EstadoLabel.Text = DataGridView1.SelectedCells(0).Value.ToString
                NombreTextBox.Text = DataGridView1.SelectedCells(1).Value.ToString
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public entraActivate As Integer = 0
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If TextBox2.Text = "" Then
            Exit Sub
        End If
        busca(1, TextBox2.Text)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        OpcionEstado = "N"
        FrmEstados.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        OpcionEstado = "C"
        GloEstado = DataGridView1.SelectedCells(0).Value.ToString
        FrmEstados.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        OpcionEstado = "M"
        GloEstado = DataGridView1.SelectedCells(0).Value.ToString
        FrmEstados.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwEstados_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If entraActivate = 1 Then
            entraActivate = 0
            busca(0, "")
        End If
    End Sub
End Class