Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine


Public Class FrmCargarReporte

    Private customersByCityReport As New ReportDocument
    Private op As String = Nothing
    Private Titulo As String = Nothing


    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        'customersByCityReport = New ReportDocument
        Try

        
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        ConnectionInfo.ServerName = GloServerName
        ConnectionInfo.DatabaseName = GloDatabaseName
        ConnectionInfo.UserID = GloUserID
        ConnectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing
        'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCarga.rpt"
        reportPath = RutaReportes.ToString + "\ReporteCarga.rpt"
        customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport
        SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub


    Private Sub CrystalReportViewer1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CrystalReportViewer1.Load
        Dim op As String = Nothing
        Dim Titulo As String = Nothing
        ConfigureCrystalReports(op, Titulo)
    End Sub
End Class