<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwEncargadosEmails
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_EncargadoLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConEncargadosEmailsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConEncargadosEmailsTableAdapter = New sofTV.DataSetEricTableAdapters.ConEncargadosEmailsTableAdapter()
        Me.ConEncargadosEmailsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Clv_EncargadoTextBox = New System.Windows.Forms.TextBox()
        Clv_EncargadoLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConEncargadosEmailsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConEncargadosEmailsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_EncargadoLabel
        '
        Clv_EncargadoLabel.AutoSize = True
        Clv_EncargadoLabel.Location = New System.Drawing.Point(220, 213)
        Clv_EncargadoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_EncargadoLabel.Name = "Clv_EncargadoLabel"
        Clv_EncargadoLabel.Size = New System.Drawing.Size(104, 17)
        Clv_EncargadoLabel.TabIndex = 11
        Clv_EncargadoLabel.Text = "Clv Encargado:"
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.EnforceConstraints = False
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConEncargadosEmailsBindingSource
        '
        Me.ConEncargadosEmailsBindingSource.DataMember = "ConEncargadosEmails"
        Me.ConEncargadosEmailsBindingSource.DataSource = Me.DataSetEric
        '
        'ConEncargadosEmailsTableAdapter
        '
        Me.ConEncargadosEmailsTableAdapter.ClearBeforeFill = True
        '
        'ConEncargadosEmailsDataGridView
        '
        Me.ConEncargadosEmailsDataGridView.AllowUserToAddRows = False
        Me.ConEncargadosEmailsDataGridView.AllowUserToDeleteRows = False
        Me.ConEncargadosEmailsDataGridView.AutoGenerateColumns = False
        Me.ConEncargadosEmailsDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConEncargadosEmailsDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConEncargadosEmailsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3})
        Me.ConEncargadosEmailsDataGridView.DataSource = Me.ConEncargadosEmailsBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConEncargadosEmailsDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConEncargadosEmailsDataGridView.Location = New System.Drawing.Point(16, 15)
        Me.ConEncargadosEmailsDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ConEncargadosEmailsDataGridView.Name = "ConEncargadosEmailsDataGridView"
        Me.ConEncargadosEmailsDataGridView.ReadOnly = True
        Me.ConEncargadosEmailsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConEncargadosEmailsDataGridView.Size = New System.Drawing.Size(748, 481)
        Me.ConEncargadosEmailsDataGridView.TabIndex = 2
        Me.ConEncargadosEmailsDataGridView.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_Encargado"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clv_Encargado"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Nombre"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Email"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 300
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(805, 15)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 44)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&NUEVO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(805, 66)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 44)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "&MODIFICAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(805, 452)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(181, 44)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Clv_EncargadoTextBox
        '
        Me.Clv_EncargadoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConEncargadosEmailsBindingSource, "Clv_Encargado", True))
        Me.Clv_EncargadoTextBox.Location = New System.Drawing.Point(224, 233)
        Me.Clv_EncargadoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_EncargadoTextBox.Name = "Clv_EncargadoTextBox"
        Me.Clv_EncargadoTextBox.ReadOnly = True
        Me.Clv_EncargadoTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clv_EncargadoTextBox.TabIndex = 12
        Me.Clv_EncargadoTextBox.TabStop = False
        '
        'BrwEncargadosEmails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1008, 518)
        Me.Controls.Add(Me.ConEncargadosEmailsDataGridView)
        Me.Controls.Add(Clv_EncargadoLabel)
        Me.Controls.Add(Me.Clv_EncargadoTextBox)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwEncargadosEmails"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte de Fallas de Interface"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConEncargadosEmailsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConEncargadosEmailsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConEncargadosEmailsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConEncargadosEmailsTableAdapter As sofTV.DataSetEricTableAdapters.ConEncargadosEmailsTableAdapter
    Friend WithEvents ConEncargadosEmailsDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Clv_EncargadoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
