Public Class FrmProcesamiento_Llamadas

    Private Sub FrmProcesamiento_Llamadas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Bnprocesa = True Then
            Bnprocesa = False
            Me.TextBox1.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
        End If
    End Sub

    Private Sub FrmProcesamiento_Llamadas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim con As New SqlClient.SqlConnection(MiConexion)
        Try

            con.Open()
            Me.Muestra_PeriodosCobroTableAdapter.Connection = con
            Me.Muestra_PeriodosCobroTableAdapter.Fill(Me.DataSetyahve.Muestra_PeriodosCobro)
            con.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

        colorea(Me, Me.Name)
        'busca(0)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GLOCONTRATOSEL = 0
        Bnprocesa = True
        FrmSelCliente.Show()
    End Sub
End Class