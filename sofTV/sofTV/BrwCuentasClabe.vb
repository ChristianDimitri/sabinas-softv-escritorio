﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic

Imports NPOI.HSSF.UserModel
Imports NPOI.SS.UserModel
Imports NPOI.SS.Util
Imports NPOI.HSSF.Util
Imports NPOI.POIFS.FileSystem
Imports NPOI.HPSF
Imports NPOI.XSSF.UserModel
Imports NPOI.XSSF.Util
Imports System.Text
Imports System.IO
Imports System.Web.Security
Public Class BrwCuentasClabe

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwCuentasClabe_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        NombreTextBox.BackColor = Button4.BackColor
        NombreTextBox.ForeColor = Button4.ForeColor
        busca()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub busca()
        Try
            BaseII.limpiaParametros()
            DataGridView1.DataSource = BaseII.ConsultaDT("ObtieneTodosClabe")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If tbClabe.Text.Trim = "" Then
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clabe", SqlDbType.VarChar, tbClabe.Text)
            BaseII.CreateMyParameter("@contratoCompania", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@idCompania", SqlDbType.Int, 0)
            DataGridView1.DataSource = BaseII.ConsultaDT("BuscaClabe")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If tbContrato.Text.Trim = "" Then
            Exit Sub
        End If
        Try
            Dim arr = tbContrato.Text.Trim.Split("-")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clabe", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@contratoCompania", SqlDbType.BigInt, arr(0))
            BaseII.CreateMyParameter("@idCompania", SqlDbType.Int, arr(1))
            DataGridView1.DataSource = BaseII.ConsultaDT("BuscaClabe")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tbClabe_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbClabe.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If tbClabe.Text.Trim = "" Then
                Exit Sub
            End If
            Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clabe", SqlDbType.VarChar, tbClabe.Text)
                BaseII.CreateMyParameter("@contratoCompania", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@idCompania", SqlDbType.Int, 0)
                DataGridView1.DataSource = BaseII.ConsultaDT("BuscaClabe")
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub tbContrato_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbContrato.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If tbContrato.Text.Trim = "" Then
                Exit Sub
            End If
            Try
                Dim arr = tbContrato.Text.Trim.Split("-")
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clabe", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@contratoCompania", SqlDbType.BigInt, arr(0))
                BaseII.CreateMyParameter("@idCompania", SqlDbType.Int, arr(1))
                DataGridView1.DataSource = BaseII.ConsultaDT("BuscaClabe")
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Clv_LocalidadLabel.Text = DataGridView1.SelectedCells(0).Value.ToString
            NombreTextBox.Text = DataGridView1.SelectedCells(1).Value.ToString
            Label6.Text = DataGridView1.SelectedCells(2).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            opcion = "N"
            gloClave = 0
            FrmCuentaClabe.Show()
            Locbndactualiza = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            opcion = "C"
            gloClave = DataGridView1.SelectedCells(0).Value
            FrmCuentaClabe.Show()
            Locbndactualiza = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Try
            opcion = "M"
            gloClave = DataGridView1.SelectedCells(0).Value
            FrmCuentaClabe.Show()
            Locbndactualiza = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BrwCuentasClabe_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        If Locbndactualiza = True Then
            Locbndactualiza = False
            busca()
        End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Try
            Dim fd As OpenFileDialog = New OpenFileDialog()
            Dim strFileName As String
            Dim clabe As String
            Dim id As String
            fd.Title = "Selecciona el Archivo Layout"
            fd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
            fd.FilterIndex = 2
            fd.RestoreDirectory = True
            Dim mensajeErrores As String = "Problema en fila(s): "
            If fd.ShowDialog() = DialogResult.OK Then
                strFileName = fd.FileName
                If fd.FileName.Contains(".xlsx") Then
                    Dim wb As XSSFWorkbook
                    Using file As New FileStream(fd.FileName, FileMode.Open, FileAccess.Read)
                        wb = New XSSFWorkbook(file)
                    End Using
                    Dim sheet As ISheet = wb.GetSheetAt(0)
                    For row As Integer = 1 To sheet.LastRowNum Step 1
                        Try
                            clabe = sheet.GetRow(row).GetCell(4).StringCellValue
                            id = sheet.GetRow(row).GetCell(3).NumericCellValue
                            BaseII.limpiaParametros()
                            BaseII.CreateMyParameter("@id", SqlDbType.Int, id)
                            BaseII.CreateMyParameter("@clabe", SqlDbType.VarChar, clabe)
                            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
                            BaseII.CreateMyParameter("@op", SqlDbType.VarChar, "N")
                            BaseII.ProcedimientoOutPut("GuardaClabe")
                            If BaseII.dicoPar("@error") = 1 Then
                                If mensajeErrores = "Problema en fila(s): " Then
                                    mensajeErrores = mensajeErrores + row.ToString
                                Else
                                    mensajeErrores = mensajeErrores + ", " + row.ToString
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    Next
                    If mensajeErrores <> "Problema en fila(s): " Then
                        MsgBox(mensajeErrores + "." + vbNewLine + "El Id o la Clabe ya se encontraban registradas en el sistema.")
                    End If
                    busca()
                ElseIf fd.FileName.Contains(".xls") Then
                    Dim wb As HSSFWorkbook
                    Using file As New FileStream(fd.FileName, FileMode.Open, FileAccess.Read)
                        wb = New HSSFWorkbook(file)
                    End Using
                    Dim sheet As ISheet = wb.GetSheetAt(0)
                    For row As Integer = 1 To sheet.LastRowNum Step 1
                        Try
                            clabe = sheet.GetRow(row).GetCell(4).StringCellValue
                            id = sheet.GetRow(row).GetCell(3).NumericCellValue
                            BaseII.limpiaParametros()
                            BaseII.CreateMyParameter("@id", SqlDbType.Int, id)
                            BaseII.CreateMyParameter("@clabe", SqlDbType.VarChar, clabe)
                            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
                            BaseII.CreateMyParameter("@op", SqlDbType.VarChar, "N")
                            BaseII.ProcedimientoOutPut("GuardaClabe")
                            If BaseII.dicoPar("@error") = 1 Then
                                If mensajeErrores = "Problema en fila(s): " Then
                                    mensajeErrores = mensajeErrores + row.ToString
                                Else
                                    mensajeErrores = mensajeErrores + ", " + row.ToString
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    Next
                    If mensajeErrores <> "Problema en fila(s): " Then
                        MsgBox(mensajeErrores + "." + vbNewLine + "El Id o la Clabe ya se encontraban registradas en el sistema.")
                    End If
                    busca()
                Else
                    MsgBox("Formato de archivo no válido.")
                End If
            End If
        Catch ex As Exception
            MsgBox("¡Ups! Algo salió mal..." + vbNewLine + "Advertencia: Las columnas deben coincidir con el patrón.")
        End Try
    End Sub
End Class