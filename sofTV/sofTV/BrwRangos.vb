Imports System.Data.SqlClient
Public Class BrwRangos

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
                GloIdCompania = ComboBoxCompanias.SelectedValue
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmRangos.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.MuestraCatalogoDeRangosDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eCveRango = Me.CveRangoTextBox.Text
            eRangoInferior = Me.RangoInferiorTextBox.Text
            eRangoSuperior = Me.RangoSuperiorTextBox.Text
            GloIdCompania = Me.ComboBoxCompanias.SelectedValue
            FrmRangos.Show()
        Else
            MsgBox("No Existen Registros para Modificar.", , "Error")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub BrwRangos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try

            Me.MuestraCatalogoDeRangosTableAdapter.Connection = CON
            Me.MuestraCatalogoDeRangosTableAdapter.Fill(Me.DataSetEDGAR.MuestraCatalogoDeRangos, 0, ComboBoxCompanias.SelectedValue)
        Catch ex As Exception

        End Try
        CON.Close()

    End Sub

    Private Sub BrwRangos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Llena_companias()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try

            Me.MuestraCatalogoDeRangosTableAdapter.Connection = CON
            Me.MuestraCatalogoDeRangosTableAdapter.Fill(Me.DataSetEDGAR.MuestraCatalogoDeRangos, 0, ComboBoxCompanias.SelectedValue)
        Catch ex As Exception

        End Try
        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try

            Me.MuestraCatalogoDeRangosTableAdapter.Connection = CON
            Me.MuestraCatalogoDeRangosTableAdapter.Fill(Me.DataSetEDGAR.MuestraCatalogoDeRangos, 0, ComboBoxCompanias.SelectedValue)
        Catch ex As Exception

        End Try
        
        CON.Close()
    End Sub
End Class