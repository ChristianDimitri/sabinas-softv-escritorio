﻿Public Class FrmEscalamiento

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If IsNumeric(NoTicket.Text) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_QUEJA", SqlDbType.BigInt, gloClave)
            BaseII.CreateMyParameter("@Clv_usuario", SqlDbType.BigInt, GloClvUsuario)
            BaseII.CreateMyParameter("@NoTicket", SqlDbType.BigInt, NoTicket.Text)
            BaseII.CreateMyParameter("@idEscalamiento", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("sp_escalarQueja")
            TextBox1.Text = BaseII.dicoPar("@idEscalamiento").ToString
            If CBool(BaseII.dicoPar("@error").ToString) = False Then
                MsgBox("Guardado correctamente", MsgBoxStyle.Information)
                FrmQueja.dameNivel()
            Else
                MsgBox("El No. de ticket ya existe", MsgBoxStyle.Critical)
            End If
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmEscalamiento_Load(sender As Object, e As EventArgs) Handles Me.Load
        If GloNivelQueja = 2 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_QUEJA", SqlDbType.BigInt, gloClave)            
            BaseII.CreateMyParameter("@NoTicket", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@idEscalamiento", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("sp_DameNoTicketQueja")
            'Button1.Enabled = False
            NoTicket.Text = BaseII.dicoPar("@NoTicket").ToString
            TextBox1.Text = BaseII.dicoPar("@idEscalamiento").ToString
        End If
    End Sub
End Class