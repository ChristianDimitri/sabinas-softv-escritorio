﻿Imports System.Data.SqlClient
Imports System.Diagnostics
Public Class FrmOtrosAparatos

    Public LContratonet As Long = 0
    Public LTipo As String = ""
    Private latDecimal As String
    Private longDecimal As String


    Public Sub GuardaAparatos(oContratonet As Long, oTipo As String, oStatus As String, oObs As String, oFechaActivacion As String, oFechaSuspencion As String, oFechaBaja As String)
        '@Contratonet bigint,
        '@Tipo Varchar(1),
        '@Status VARCHAR(1)='',
        '@Obs VARCHAR(400)='',
        '@FechaActivacion VARCHAR(10)='',
        '@FechaSuspencion VARCHAR(10)='',
        '@FechaBaja VARCHAR(10)='',
        '@SeRenta BIT=0
        If CMBoxStatus.SelectedIndex < 0 Then
            MsgBox("Seleccione el Status del Aparato por Favor")
            Exit Sub
        End If

        If CMBoxStatus.SelectedValue = "I" Then
            Try
                If IsDate(CDate(TBInstalacion.Text)) = False Then
                    MsgBox("Capture una Fecha de Activación validad por favor")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Capture una Fecha de Activación validad por favor")
                Exit Sub
            End Try
        End If
        If CMBoxStatus.SelectedValue = "S" Then
            Try
                If IsDate(CDate(TBSuspension.Text)) = False Then
                    MsgBox("Capture una Fecha de Suspención validad por favor")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Capture una Fecha de Suspención validad por favor")
                Exit Sub
            End Try
        End If
        If CMBoxStatus.SelectedValue = "B" Then
            Try

                If IsDate(CDate(TBBaja.Text)) = False Then
                    MsgBox("Capture una Fecha de Cancelación validad por favor")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Capture una Fecha de Cancelación validad por favor")
                Exit Sub
            End Try
        End If

        If TBInstalacion.TextLength > 0 Then
            Try
                If IsDate(CDate(TBInstalacion.Text)) = False Then
                    MsgBox("Capture una Fecha de Activación validad por favor")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Capture una Fecha de Activación validad por favor")
                Exit Sub
            End Try
        End If
        If TBSuspension.TextLength > 0 Then
            Try
                If IsDate(CDate(TBSuspension.Text)) = False Then
                    MsgBox("Capture una Fecha de Suspención validad por favor")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Capture una Fecha de Suspención validad por favor")
                Exit Sub
            End Try
        End If
        If TBBaja.TextLength > 0 Then
            Try

                If IsDate(CDate(TBBaja.Text)) = False Then
                    MsgBox("Capture una Fecha de Cancelación validad por favor")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Capture una Fecha de Cancelación validad por favor")
                Exit Sub
            End Try
        End If


        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, oTipo, 1)
        BaseII.CreateMyParameter("@Status", SqlDbType.VarChar, oStatus, 1)
        BaseII.CreateMyParameter("@Obs", SqlDbType.VarChar, oObs, 400)
        BaseII.CreateMyParameter("@FechaActivacion", SqlDbType.VarChar, oFechaActivacion, 10)
        BaseII.CreateMyParameter("@FechaSuspencion", SqlDbType.VarChar, oFechaSuspencion, 10)
        BaseII.CreateMyParameter("@FechaBaja", SqlDbType.VarChar, oFechaBaja, 10)
        If CBSeRenta.CheckState = CheckState.Checked Then
            BaseII.CreateMyParameter("@SeRenta", SqlDbType.Bit, 1)
        Else
            BaseII.CreateMyParameter("@SeRenta", SqlDbType.Bit, 0)
        End If
        BaseII.Inserta("SP_GuardaDatosAparto")

        If CMBoxStatus.SelectedValue <> CMBoxStatus.Tag Then
            bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Aparato: " + LblMac.Text + " Status: ", CMBoxStatus.Tag, CMBoxStatus.SelectedValue, LocClv_Ciudad)
        End If
        If TBObs.Text <> TBObs.Tag Then
            bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Aparato: " + LblMac.Text + " Obs: ", TBObs.Tag, TBObs.Text, LocClv_Ciudad)
        End If

        If TBInstalacion.Text <> TBInstalacion.Tag Then
            bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Aparato: " + LblMac.Text + " Fecha Activación: ", TBInstalacion.Tag, TBInstalacion.Text, LocClv_Ciudad)
        End If

        If TBSuspension.Text <> TBSuspension.Tag Then
            bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Aparato: " + LblMac.Text + " Fecha Suspención: ", TBSuspension.Tag, TBSuspension.Text, LocClv_Ciudad)
        End If
        If TBBaja.Text <> TBBaja.Tag Then
            bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Aparato: " + LblMac.Text + " Fecha Cancelación: ", TBBaja.Tag, TBBaja.Text, LocClv_Ciudad)
        End If
        Try
            If CBSeRenta.CheckState <> CBool(CBSeRenta.Tag) Then
                bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Aparato: " + LblMac.Text + " Se Renta: ", CBSeRenta.Tag, CBSeRenta.CheckState.ToString, LocClv_Ciudad)
            End If
        Catch ex As Exception

        End Try
        

        MsgBox("Guardado Correctamente")
    End Sub

    Private Sub Llena_Aparatos()
        BaseII.limpiaParametros()
        CMBoxStatus.DataSource = BaseII.ConsultaDT("SP_StatusCableModem")
        CMBoxStatus.DisplayMember = "Concepto"
        CMBoxStatus.ValueMember = "Clv_StatusCableModem"
    End Sub

    Public Sub generaOrden(ByVal oContratonet As Integer, oStatus As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_VerAparatodelClientedig", conexion)
        Dim reader As SqlDataReader

        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0


        Dim parametro3 As New SqlParameter("@CONTRATONET", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = oContratonet
        comando.Parameters.Add(parametro3)


        Dim parametro14 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
        parametro14.Direction = ParameterDirection.Input
        parametro14.Value = oStatus
        comando.Parameters.Add(parametro14)
        'Coordenadas de la Antena de Internet
        If oStatus = "C" Then
            PanelUbicacion.Visible = True
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, LContratonet)
            BaseII.CreateMyParameter("@latitud", ParameterDirection.Output, SqlDbType.SmallInt)
            BaseII.CreateMyParameter("@longitud", ParameterDirection.Output, SqlDbType.SmallInt)
            BaseII.ProcedimientoOutPut("sp_dameCoordenadasCliente")

            latDecimal = BaseII.dicoPar("@latitud").ToString()
            longDecimal = BaseII.dicoPar("@longitud").ToString()

            TbLatitud.Text = BaseII.dicoPar("@latitud").ToString()
            TbLongitud.Text = BaseII.dicoPar("@longitud").ToString()

        Else
            ' TbLongitud.Text = Str(Math.Round((latDecimal - Math.Truncate(latDecimal)), 5) * 60)
            PanelUbicacion.Visible = False
        End If

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            reader = comando.ExecuteReader

            While (reader.Read())
                'Mac
                LblMac.Text = reader(1).ToString()
                LblMac.Tag = reader(1).ToString()
                'Marca
                TBMarcaAparato.Text = reader(2).ToString()
                TBMarcaAparato.Tag = reader(2).ToString()
                'Tipo Aparato
                CMBLabelAparato.Text = "Datos " & reader(3).ToString() & " :"

                'Status
                CMBoxStatus.SelectedValue = reader(4).ToString()
                CMBoxStatus.Tag = reader(4).ToString()
                'Obs
                TBObs.Text = reader(5).ToString()
                TBObs.Tag = reader(5).ToString()
                'FechaActivacion
                TBInstalacion.Text = reader(6).ToString()
                TBInstalacion.Tag = reader(6).ToString()
                'FechaSuspension
                TBSuspension.Text = reader(7).ToString()
                TBSuspension.Tag = reader(7).ToString()
                'FechaBaja
                TBBaja.Text = reader(8).ToString()
                TBBaja.Tag = reader(8).ToString()
                'SeRenta
                If CBool(reader(9).ToString()) = True Then
                    CBSeRenta.Checked = True
                Else
                    CBSeRenta.Checked = False
                End If
                CBSeRenta.Tag = reader(9).ToString()

            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Public Sub restringir()

        If GloTipoUsuario <> 40 Then
            BindingNavigator4.Enabled = False
            CMBoxStatus.Enabled = False
            TBObs.Enabled = False
            TBInstalacion.Enabled = False
            TBSuspension.Enabled = False
            TBBaja.Enabled = False
            CBSeRenta.Enabled = False


        Else
            BindingNavigator4.Enabled = True
            CMBoxStatus.Enabled = True
            TBObs.Enabled = True
            TBInstalacion.Enabled = True
            TBSuspension.Enabled = True
            TBBaja.Enabled = True
            CBSeRenta.Enabled = True


            TBObs.ReadOnly = False
            TBInstalacion.ReadOnly = False
            TBSuspension.ReadOnly = False
            TBBaja.ReadOnly = False

        End If

    End Sub

    Private Sub FrmOtrosAparatos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.BackColor = FrmClientes.BackColor
        Llena_Aparatos()
        restringir()
        'If OpcionCli = "N" Or OpcionCli = "M" Then
        '    UspDesactivaBotones(Me, Me.Name)

        'End If
        'UspGuardaFormularios(Me.Name, Me.Text)
        'UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub ToolStripButton12_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton12.Click
        GuardaAparatos(LContratonet, LTipo, CMBoxStatus.SelectedValue, TBObs.Text, TBInstalacion.Text, TBSuspension.Text, TBBaja.Text)
    End Sub

    Private Sub Button35_Click(sender As Object, e As EventArgs) Handles ButtonMap.Click
        'Dim latitud As String
        'Dim longitud As String
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@ContratoAnt", SqlDbType.BigInt, LContratonet)
        'BaseII.CreateMyParameter("@latitud", ParameterDirection.Output, SqlDbType.Float)
        'BaseII.CreateMyParameter("@longitud", ParameterDirection.Output, SqlDbType.Float)
        'BaseII.ProcedimientoOutPut("sp_dameCoordenadasCliente")
        'latitud = BaseII.dicoPar("@latitud").ToString()
        'longitud = BaseII.dicoPar("@longitud").ToString()
        If TbLatitud.Text <> "0" And TbLongitud.Text <> "0" Then
            Dim sInfo As ProcessStartInfo = New ProcessStartInfo("https://www.google.com.mx/maps/place//@" + Str(latDecimal).Trim + "," + Str(longDecimal).Trim + ",17z/data=!4m2!3m1!1s0x0:0x0")
            Process.Start(sInfo)
        End If
        
    End Sub
End Class