﻿Public Class FrmTipServTmp

    '  Dim identif As Integer = 0

    Private Sub llenar_lb(opcion As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CType(opcion, Integer))
        BaseII.CreateMyParameter("@IdSession", SqlDbType.BigInt, CType(IdSessionRep, Long))

        If opcion = 0 Then
            lbIzquierda.DataSource = BaseII.ConsultaDT("CONtbl_TipSerTmp")
        Else
            lbDerecha.DataSource = BaseII.ConsultaDT("CONtbl_TipSerTmp")
        End If


    End Sub

    Private Sub button1_Click(sender As Object, e As EventArgs) Handles button1.Click

        Nue(0, lbIzquierda.SelectedValue)
        llenar_lb(0)
        llenar_lb(1)
    End Sub

    Private Sub button2_Click(sender As Object, e As EventArgs) Handles button2.Click
        Nue(1, 0)
        llenar_lb(0)
        llenar_lb(1)
    End Sub

    Private Sub button3_Click(sender As Object, e As EventArgs) Handles button3.Click
        Bor(0, lbDerecha.SelectedValue)
        llenar_lb(0)
        llenar_lb(1)
    End Sub

    Private Sub button4_Click(sender As Object, e As EventArgs) Handles button4.Click
        Bor(1, 0)
        llenar_lb(0)
        llenar_lb(1)
    End Sub

    Private Sub Nue(opcion As Integer, Clv_TipSer As Integer)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CType(opcion, Integer))
        BaseII.CreateMyParameter("@IdSession", SqlDbType.BigInt, CType(IdSessionRep, Long))
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CType(Clv_TipSer, Integer))
        BaseII.ConsultaDT("NUEtbl_TipSerTmp")

    End Sub


    Private Sub Bor(opcion As Integer, Clv_TipSer As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CType(opcion, Integer))
        BaseII.CreateMyParameter("@IdSession", SqlDbType.BigInt, CType(IdSessionRep, Long))
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CType(Clv_TipSer, Integer))
        BaseII.ConsultaDT("BORtbl_TipSerTmp")
    End Sub


    Private Sub bnAceptar_Click(sender As Object, e As EventArgs) Handles bnAceptar.Click
        If lbDerecha.Items.Count = 0 Then
            MessageBox.Show("Selecciona al menos un elemento.")
        End If

        If opRep = 8 Then
            FrmServiciosTmp.Show()
            Me.Close()
        End If



    End Sub

    Private Sub FrmTipServTmp_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        llenar_lb(0)
        llenar_lb(1)
    End Sub

End Class