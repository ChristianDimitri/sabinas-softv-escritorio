﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCoordenadasCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBoxCoordenadas = New System.Windows.Forms.GroupBox()
        Me.tbLongDecimal = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbLatDecimal = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.GroupBoxCoordenadas.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBoxCoordenadas
        '
        Me.GroupBoxCoordenadas.Controls.Add(Me.tbLongDecimal)
        Me.GroupBoxCoordenadas.Controls.Add(Me.Label8)
        Me.GroupBoxCoordenadas.Controls.Add(Me.tbLatDecimal)
        Me.GroupBoxCoordenadas.Controls.Add(Me.Label9)
        Me.GroupBoxCoordenadas.Controls.Add(Me.Button2)
        Me.GroupBoxCoordenadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBoxCoordenadas.Location = New System.Drawing.Point(5, 3)
        Me.GroupBoxCoordenadas.Name = "GroupBoxCoordenadas"
        Me.GroupBoxCoordenadas.Size = New System.Drawing.Size(359, 166)
        Me.GroupBoxCoordenadas.TabIndex = 52
        Me.GroupBoxCoordenadas.TabStop = False
        Me.GroupBoxCoordenadas.Text = "Coordenadas"
        '
        'tbLongDecimal
        '
        Me.tbLongDecimal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbLongDecimal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbLongDecimal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLongDecimal.Location = New System.Drawing.Point(107, 64)
        Me.tbLongDecimal.Multiline = True
        Me.tbLongDecimal.Name = "tbLongDecimal"
        Me.tbLongDecimal.Size = New System.Drawing.Size(219, 26)
        Me.tbLongDecimal.TabIndex = 35
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(13, 66)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 15)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "Longitud: "
        '
        'tbLatDecimal
        '
        Me.tbLatDecimal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbLatDecimal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbLatDecimal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLatDecimal.Location = New System.Drawing.Point(107, 29)
        Me.tbLatDecimal.Multiline = True
        Me.tbLatDecimal.Name = "tbLatDecimal"
        Me.tbLatDecimal.Size = New System.Drawing.Size(219, 26)
        Me.tbLatDecimal.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(15, 31)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 15)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Latitud: "
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(30, 110)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(137, 40)
        Me.Button2.TabIndex = 34
        Me.Button2.Text = "Guardar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(195, 111)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 40)
        Me.Button5.TabIndex = 50
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'FrmCoordenadasCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(366, 181)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.GroupBoxCoordenadas)
        Me.Name = "FrmCoordenadasCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Coordenadas del Cliente"
        Me.GroupBoxCoordenadas.ResumeLayout(False)
        Me.GroupBoxCoordenadas.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBoxCoordenadas As System.Windows.Forms.GroupBox
    Friend WithEvents tbLongDecimal As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbLatDecimal As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
