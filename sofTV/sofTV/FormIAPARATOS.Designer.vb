﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormIAPARATOS
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.BtnAceptar = New System.Windows.Forms.Button()
        Me.ComboBoxPorAsignar = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBoxAparatosDisponibles = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblEstadoAparato = New System.Windows.Forms.Label()
        Me.CMBoxEstadoAparato = New System.Windows.Forms.ComboBox()
        Me.ComboBoxCables = New System.Windows.Forms.ComboBox()
        Me.LabCables = New System.Windows.Forms.Label()
        Me.ComboBoxControlRemoto = New System.Windows.Forms.ComboBox()
        Me.LabControlRemoto = New System.Windows.Forms.Label()
        Me.CMBPanelAccesorios = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBoxTipoAparato = New System.Windows.Forms.ComboBox()
        Me.LbTipoAparato = New System.Windows.Forms.Label()
        Me.cbNodo = New System.Windows.Forms.ComboBox()
        Me.cbEquipo815 = New System.Windows.Forms.ComboBox()
        Me.LabelNodo = New System.Windows.Forms.Label()
        Me.LabelEquipo815 = New System.Windows.Forms.Label()
        Me.TextBoxWan = New System.Windows.Forms.TextBox()
        Me.LabelWan = New System.Windows.Forms.Label()
        Me.CMBPanelAccesorios.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(308, 259)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 32
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'BtnAceptar
        '
        Me.BtnAceptar.BackColor = System.Drawing.SystemColors.Control
        Me.BtnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAceptar.ForeColor = System.Drawing.Color.Black
        Me.BtnAceptar.Location = New System.Drawing.Point(166, 259)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(136, 33)
        Me.BtnAceptar.TabIndex = 41
        Me.BtnAceptar.Text = "&ACEPTAR"
        Me.BtnAceptar.UseVisualStyleBackColor = False
        '
        'ComboBoxPorAsignar
        '
        Me.ComboBoxPorAsignar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxPorAsignar.FormattingEnabled = True
        Me.ComboBoxPorAsignar.Location = New System.Drawing.Point(14, 52)
        Me.ComboBoxPorAsignar.Name = "ComboBoxPorAsignar"
        Me.ComboBoxPorAsignar.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxPorAsignar.TabIndex = 40
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(11, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(166, 16)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Aparatos por asignar  :"
        '
        'ComboBoxAparatosDisponibles
        '
        Me.ComboBoxAparatosDisponibles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxAparatosDisponibles.FormattingEnabled = True
        Me.ComboBoxAparatosDisponibles.Location = New System.Drawing.Point(14, 133)
        Me.ComboBoxAparatosDisponibles.Name = "ComboBoxAparatosDisponibles"
        Me.ComboBoxAparatosDisponibles.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxAparatosDisponibles.TabIndex = 43
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(12, 114)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(178, 16)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "Seleccione el Aparato   :"
        '
        'LblEstadoAparato
        '
        Me.LblEstadoAparato.AutoSize = True
        Me.LblEstadoAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEstadoAparato.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LblEstadoAparato.Location = New System.Drawing.Point(15, 168)
        Me.LblEstadoAparato.Name = "LblEstadoAparato"
        Me.LblEstadoAparato.Size = New System.Drawing.Size(257, 16)
        Me.LblEstadoAparato.TabIndex = 44
        Me.LblEstadoAparato.Text = "Seleccione el Estado del Aparato   :"
        Me.LblEstadoAparato.Visible = False
        '
        'CMBoxEstadoAparato
        '
        Me.CMBoxEstadoAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBoxEstadoAparato.FormattingEnabled = True
        Me.CMBoxEstadoAparato.Location = New System.Drawing.Point(274, 165)
        Me.CMBoxEstadoAparato.Name = "CMBoxEstadoAparato"
        Me.CMBoxEstadoAparato.Size = New System.Drawing.Size(170, 24)
        Me.CMBoxEstadoAparato.TabIndex = 45
        '
        'ComboBoxCables
        '
        Me.ComboBoxCables.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCables.FormattingEnabled = True
        Me.ComboBoxCables.Location = New System.Drawing.Point(16, 39)
        Me.ComboBoxCables.Name = "ComboBoxCables"
        Me.ComboBoxCables.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxCables.TabIndex = 47
        '
        'LabCables
        '
        Me.LabCables.AutoSize = True
        Me.LabCables.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabCables.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabCables.Location = New System.Drawing.Point(8, 20)
        Me.LabCables.Name = "LabCables"
        Me.LabCables.Size = New System.Drawing.Size(160, 16)
        Me.LabCables.TabIndex = 46
        Me.LabCables.Text = "Seleccione el Cable  :"
        '
        'ComboBoxControlRemoto
        '
        Me.ComboBoxControlRemoto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxControlRemoto.FormattingEnabled = True
        Me.ComboBoxControlRemoto.Location = New System.Drawing.Point(16, 85)
        Me.ComboBoxControlRemoto.Name = "ComboBoxControlRemoto"
        Me.ComboBoxControlRemoto.Size = New System.Drawing.Size(430, 24)
        Me.ComboBoxControlRemoto.TabIndex = 49
        '
        'LabControlRemoto
        '
        Me.LabControlRemoto.AutoSize = True
        Me.LabControlRemoto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabControlRemoto.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabControlRemoto.Location = New System.Drawing.Point(9, 66)
        Me.LabControlRemoto.Name = "LabControlRemoto"
        Me.LabControlRemoto.Size = New System.Drawing.Size(226, 16)
        Me.LabControlRemoto.TabIndex = 48
        Me.LabControlRemoto.Text = "Seleccione el Control Remoto  :"
        '
        'CMBPanelAccesorios
        '
        Me.CMBPanelAccesorios.Controls.Add(Me.Label2)
        Me.CMBPanelAccesorios.Controls.Add(Me.LabCables)
        Me.CMBPanelAccesorios.Controls.Add(Me.ComboBoxCables)
        Me.CMBPanelAccesorios.Controls.Add(Me.ComboBoxControlRemoto)
        Me.CMBPanelAccesorios.Controls.Add(Me.LabControlRemoto)
        Me.CMBPanelAccesorios.Location = New System.Drawing.Point(12, 342)
        Me.CMBPanelAccesorios.Name = "CMBPanelAccesorios"
        Me.CMBPanelAccesorios.Size = New System.Drawing.Size(460, 126)
        Me.CMBPanelAccesorios.TabIndex = 50
        Me.CMBPanelAccesorios.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 20)
        Me.Label2.TabIndex = 50
        Me.Label2.Text = "Accesorios"
        '
        'ComboBoxTipoAparato
        '
        Me.ComboBoxTipoAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxTipoAparato.FormattingEnabled = True
        Me.ComboBoxTipoAparato.Location = New System.Drawing.Point(274, 87)
        Me.ComboBoxTipoAparato.Name = "ComboBoxTipoAparato"
        Me.ComboBoxTipoAparato.Size = New System.Drawing.Size(170, 24)
        Me.ComboBoxTipoAparato.TabIndex = 52
        Me.ComboBoxTipoAparato.Visible = False
        '
        'LbTipoAparato
        '
        Me.LbTipoAparato.AutoSize = True
        Me.LbTipoAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbTipoAparato.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LbTipoAparato.Location = New System.Drawing.Point(11, 89)
        Me.LbTipoAparato.Name = "LbTipoAparato"
        Me.LbTipoAparato.Size = New System.Drawing.Size(213, 16)
        Me.LbTipoAparato.TabIndex = 51
        Me.LbTipoAparato.Text = "Seleccione el tipo de aparato"
        Me.LbTipoAparato.Visible = False
        '
        'cbNodo
        '
        Me.cbNodo.DisplayMember = "Nombre"
        Me.cbNodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cbNodo.FormattingEnabled = True
        Me.cbNodo.Location = New System.Drawing.Point(166, 229)
        Me.cbNodo.Name = "cbNodo"
        Me.cbNodo.Size = New System.Drawing.Size(278, 24)
        Me.cbNodo.TabIndex = 54
        Me.cbNodo.ValueMember = "idNodo"
        Me.cbNodo.Visible = False
        '
        'cbEquipo815
        '
        Me.cbEquipo815.DisplayMember = "Nombre"
        Me.cbEquipo815.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cbEquipo815.FormattingEnabled = True
        Me.cbEquipo815.Location = New System.Drawing.Point(33, 278)
        Me.cbEquipo815.Name = "cbEquipo815"
        Me.cbEquipo815.Size = New System.Drawing.Size(91, 24)
        Me.cbEquipo815.TabIndex = 56
        Me.cbEquipo815.ValueMember = "idEquipo"
        Me.cbEquipo815.Visible = False
        '
        'LabelNodo
        '
        Me.LabelNodo.AutoSize = True
        Me.LabelNodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelNodo.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelNodo.Location = New System.Drawing.Point(25, 229)
        Me.LabelNodo.Name = "LabelNodo"
        Me.LabelNodo.Size = New System.Drawing.Size(99, 16)
        Me.LabelNodo.TabIndex = 57
        Me.LabelNodo.Text = "Nodo de red:"
        Me.LabelNodo.Visible = False
        '
        'LabelEquipo815
        '
        Me.LabelEquipo815.AutoSize = True
        Me.LabelEquipo815.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelEquipo815.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelEquipo815.Location = New System.Drawing.Point(20, 259)
        Me.LabelEquipo815.Name = "LabelEquipo815"
        Me.LabelEquipo815.Size = New System.Drawing.Size(137, 16)
        Me.LabelEquipo815.TabIndex = 58
        Me.LabelEquipo815.Text = "Equipo del cliente:"
        Me.LabelEquipo815.Visible = False
        '
        'TextBoxWan
        '
        Me.TextBoxWan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBoxWan.Location = New System.Drawing.Point(274, 195)
        Me.TextBoxWan.Name = "TextBoxWan"
        Me.TextBoxWan.Size = New System.Drawing.Size(170, 22)
        Me.TextBoxWan.TabIndex = 60
        Me.TextBoxWan.Visible = False
        '
        'LabelWan
        '
        Me.LabelWan.AutoSize = True
        Me.LabelWan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelWan.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelWan.Location = New System.Drawing.Point(16, 201)
        Me.LabelWan.Name = "LabelWan"
        Me.LabelWan.Size = New System.Drawing.Size(153, 16)
        Me.LabelWan.TabIndex = 59
        Me.LabelWan.Text = "Dirección MAC WAN:"
        Me.LabelWan.Visible = False
        '
        'FormIAPARATOS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(467, 308)
        Me.Controls.Add(Me.TextBoxWan)
        Me.Controls.Add(Me.LabelWan)
        Me.Controls.Add(Me.LabelEquipo815)
        Me.Controls.Add(Me.LabelNodo)
        Me.Controls.Add(Me.cbEquipo815)
        Me.Controls.Add(Me.cbNodo)
        Me.Controls.Add(Me.ComboBoxTipoAparato)
        Me.Controls.Add(Me.LbTipoAparato)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.CMBPanelAccesorios)
        Me.Controls.Add(Me.CMBoxEstadoAparato)
        Me.Controls.Add(Me.LblEstadoAparato)
        Me.Controls.Add(Me.ComboBoxAparatosDisponibles)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxPorAsignar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormIAPARATOS"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TopMost = True
        Me.CMBPanelAccesorios.ResumeLayout(False)
        Me.CMBPanelAccesorios.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents BtnAceptar As System.Windows.Forms.Button
    Friend WithEvents ComboBoxPorAsignar As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxAparatosDisponibles As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LblEstadoAparato As System.Windows.Forms.Label
    Friend WithEvents CMBoxEstadoAparato As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxCables As System.Windows.Forms.ComboBox
    Friend WithEvents LabCables As System.Windows.Forms.Label
    Friend WithEvents ComboBoxControlRemoto As System.Windows.Forms.ComboBox
    Friend WithEvents LabControlRemoto As System.Windows.Forms.Label
    Friend WithEvents CMBPanelAccesorios As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxTipoAparato As System.Windows.Forms.ComboBox
    Friend WithEvents LbTipoAparato As System.Windows.Forms.Label
    Friend WithEvents cbNodo As System.Windows.Forms.ComboBox
    Friend WithEvents cbEquipo815 As System.Windows.Forms.ComboBox
    Friend WithEvents LabelNodo As System.Windows.Forms.Label
    Friend WithEvents LabelEquipo815 As System.Windows.Forms.Label
    Friend WithEvents TextBoxWan As System.Windows.Forms.TextBox
    Friend WithEvents LabelWan As System.Windows.Forms.Label
End Class
