Imports System.Data.SqlClient
Public Class BrwDescuentoCombo
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"
            'ComboBoxCiudades.Text = ""
            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0
            End If
            'GloIdCompania = 0
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ComboBoxCompanias.SelectedValue = 0 Then
            MsgBox("Selecciona una Compa��a")
            Exit Sub
        End If
        ' Dim CON As New SqlConnection(MiConexion)
        eOpcion = "N"
        GloIdCompania = ComboBoxCompanias.SelectedValue
        FrmDescuentoCombo.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConDescuentoComboDataGridView.RowCount = 0 Then
            MsgBox("Selecciona un Combo a Modificar.")
            Exit Sub
        End If
        eOpcion = "M"
        eNombreCombo = Me.ComboTextBox.Text
        FrmDescuentoCombo.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub BrwDescuentoCombo_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Try
            If eBndCombo = True Then
                eBndCombo = False
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.ConDescuentoComboTableAdapter.Connection = CON
                'Me.ConDescuentoComboTableAdapter.Fill(Me.DataSetEric2.ConDescuentoCombo, "", 0)
                llenagridcombo("", 0)
                CON.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BrwDescuentoCombo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Llena_companias()
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConDescuentoComboTableAdapter.Connection = CON
            'Me.ConDescuentoComboTableAdapter.Fill(Me.DataSetEric2.ConDescuentoCombo, "", 0)
            GloIdCompania = ComboBoxCompanias.SelectedValue
            llenagridcombo("", 0)
            CON.Close()
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
            UspDesactivaBotones(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub llenagridcombo(ByVal descripcion As String, ByVal op As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, descripcion)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            ConDescuentoComboDataGridView.DataSource = BaseII.ConsultaDT("ConDescuentoCombo")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If Me.ConDescuentoComboDataGridView.RowCount = 0 Then
            MsgBox("Selecciona un Combo a Consultar.")
            Exit Sub
        End If
        eOpcion = "C"
        eNombreCombo = Me.ComboTextBox.Text
        FrmDescuentoCombo.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try
            If Me.TextBox1.Text.Length = 0 Then
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConDescuentoComboTableAdapter.Connection = CON
            'Me.ConDescuentoComboTableAdapter.Fill(Me.DataSetEric2.ConDescuentoCombo, Me.TextBox1.Text, 1)
            llenagridcombo(TextBox1.Text, 1)
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyData = Keys.Enter Then
            Try
                If Me.TextBox1.Text.Length = 0 Then
                    Exit Sub
                End If
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.ConDescuentoComboTableAdapter.Connection = CON
                'Me.ConDescuentoComboTableAdapter.Fill(Me.DataSetEric2.ConDescuentoCombo, Me.TextBox1.Text, 1)
                llenagridcombo(TextBox1.Text, 1)
                CON.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.ConDescuentoComboTableAdapter.Connection = CON2
            'Me.ConDescuentoComboTableAdapter.Fill(Me.DataSetEric2.ConDescuentoCombo, "", 0)
            llenagridcombo("", 0)
            CON2.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConDescuentoComboDataGridView_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConDescuentoComboDataGridView.SelectionChanged
        Try
            ComboTextBox.Text = ConDescuentoComboDataGridView.SelectedCells(0).Value
        Catch ex As Exception

        End Try
    End Sub
End Class