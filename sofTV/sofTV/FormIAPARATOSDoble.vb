﻿Public Class FormIAPARATOSDoble

    Private Sub FormIAPARATOSDoble_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Llena_AparatosporAsignar1(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, 0, gloClv_Orden, GloDetClave)
        Llena_AparatosporAsignar2(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, 0, gloClv_Orden, GloDetClave)
        Llena_EstadoAparato(GLOTRABAJO)

        Llena_AparatosporDisponibles(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
    End Sub
    Private Sub Llena_AparatosporAsignar1(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, "N", 10)

            ComboBoxPorAsignar.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxPorAsignar.DisplayMember = "Descripcion"
            ComboBoxPorAsignar.ValueMember = "ContratoAnt"

            If ComboBoxPorAsignar.Items.Count > 0 Then
                ComboBoxPorAsignar.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_AparatosporAsignar2(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, "S", 10)

            ComboBoxPorAsignar2.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxPorAsignar2.DisplayMember = "Descripcion"
            ComboBoxPorAsignar2.ValueMember = "ContratoAnt"

            If ComboBoxPorAsignar2.Items.Count > 0 Then
                ComboBoxPorAsignar2.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_EstadoAparato(oTrabajo As String)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            If oTrabajo = "CAPRO" Then
                CMBoxEstadoAparato.DataSource = BaseII.ConsultaDT("SP_StatusAparatosRobados")
            Else
                CMBoxEstadoAparato.DataSource = BaseII.ConsultaDT("SP_StatusAparatos")
                CMBoxEstadoAparato2.DataSource = BaseII.ConsultaDT("SP_StatusAparatos")
            End If

            CMBoxEstadoAparato.DisplayMember = "Concepto"
            CMBoxEstadoAparato.ValueMember = "Clv_StatusCableModem"

            CMBoxEstadoAparato2.DisplayMember = "Concepto"
            CMBoxEstadoAparato2.ValueMember = "Clv_StatusCableModem"

            If CMBoxEstadoAparato.Items.Count > 0 Then
                CMBoxEstadoAparato.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""


        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_AparatosporDisponibles(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, "F", 10)
            ComboBoxAparatosDisponibles.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxAparatosDisponibles.DisplayMember = "Descripcion"
            ComboBoxAparatosDisponibles.ValueMember = "ContratoAnt"

            If ComboBoxAparatosDisponibles.Items.Count > 0 Then
                ComboBoxAparatosDisponibles.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If GLOTRABAJO = "CAPSG" Then
            If ComboBoxPorAsignar.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Cambiar", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If

            If CMBoxEstadoAparato.SelectedIndex = -1 Then
                MsgBox("Seleccione el Estado del Aparato a Cambiar ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If

            If ComboBoxPorAsignar2.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Cambiar", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If

            If CMBoxEstadoAparato2.SelectedIndex = -1 Then
                MsgBox("Seleccione el Estado del Aparato a Cambiar ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If

            If ComboBoxAparatosDisponibles.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato nuevo ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If            
        End If

        If TextBoxWan.Text.Length = 0 Then
            MsgBox("Falta la dirección MAC WAN", MsgBoxStyle.Information, "Información")
            Exit Sub
        End If

        If TextBoxWan.Text.Length <> 12 Then
            MsgBox("Formato incorrecto de la dirección MAC WAN", MsgBoxStyle.Information, "Información")
            Exit Sub
        End If

        If TextBoxWan.Text = ComboBoxAparatosDisponibles.Text Then
            MsgBox("La dirección MAC LAN y MAC WAN no pueder ser iguales", MsgBoxStyle.Information, "Información")
            Exit Sub
        End If

        If Not validaWAN() Then
            MsgBox("Ya existe esa MAC WAN registrada con otro equipo", MsgBoxStyle.Information, "Información")
            Exit Sub
        End If


        SP_GuardaIAPARATOS(GloDetClave, GLOTRABAJO, gloClv_Orden, ComboBoxPorAsignar.SelectedValue, ComboBoxAparatosDisponibles.SelectedValue, opcion, CMBoxEstadoAparato.SelectedValue)
        SP_GuardaIAPARATOS(GloDetClave, GLOTRABAJO, gloClv_Orden, ComboBoxPorAsignar2.SelectedValue, ComboBoxAparatosDisponibles.SelectedValue, opcion, CMBoxEstadoAparato.SelectedValue)
        'If GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPRO" Then
        '    SP_GuardaIAPARATOS(GloDetClave, "ECABL", gloClv_Orden, ComboBoxPorAsignar.SelectedValue, ComboBoxCables.SelectedValue, opcion & "CA", "")
        '    SP_GuardaIAPARATOS(GloDetClave, "ECONT", gloClv_Orden, ComboBoxPorAsignar.SelectedValue, ComboBoxControlRemoto.SelectedValue, opcion & "CO", "")
        'End If


        SP_GuardaMacWan(GloDetClave, gloClv_Orden, ComboBoxAparatosDisponibles.SelectedValue, ComboBoxAparatosDisponibles.Text, TextBoxWan.Text)
        Me.Close()

    End Sub

    Private Sub SP_GuardaIAPARATOS(oClave As Long, oTrabajo As String, oClv_Orden As Long, oContratonet As Long, oClv_Aparato As Long, oOpcion As String, oStatus As String)
        '@Clave bigint=0,@Trabajo varchar(10)='',@Clv_Orden bigint=0,@Contratonet Bigint=0,@Clv_Aparato bigint=0,@Opcion varchar(10)=''
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Trabajo", SqlDbType.VarChar, oTrabajo, 10)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.VarChar, oOpcion, 10)
        BaseII.CreateMyParameter("@Status", SqlDbType.VarChar, oStatus, 1)
        BaseII.Inserta("SP_GuardaIAPARATOS")
    End Sub

    Private Sub SP_GuardaMacWan(oClave As Long, oClv_Orden As Long, oClv_Aparato As Long, oMacLan As String, oMacWan As String)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
            BaseII.CreateMyParameter("@MacLan", SqlDbType.VarChar, oMacLan, 50)
            BaseII.CreateMyParameter("@MacWan", SqlDbType.VarChar, oMacWan, 50)
            BaseII.Inserta("SP_GuardaMacWan")

    End Sub

    Private Function validaWAN() As Boolean
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, ComboBoxAparatosDisponibles.SelectedValue)
        BaseII.CreateMyParameter("@MacWan", SqlDbType.VarChar, TextBoxWan.Text, 50)
        BaseII.CreateMyParameter("@correcta", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("validaWAN")
        Return CBool(BaseII.dicoPar("@correcta").ToString)
    End Function

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Function Aparato815() As Boolean
        Throw New NotImplementedException
    End Function

End Class